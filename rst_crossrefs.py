import html
import pypandoc
import re


def add_crossrefs(rst_code, list_of_functions):
    """Add AIMMS Function Reference cross-references to RST code

    Parameters
    ----------
    rst_code : str
        RST code.
    list_of_functions : list
        List of functions in the AIMMS Function Reference (drawn from Intersphinx)

    """
    # Iterate over table directives
    for table_obj in re.finditer(r'(\.\. table::(?: .*)?(?:\n {3}:\w+:.*)*\s{2,})(?:\t.*\n)+', rst_code):
        table_rst = table_obj.group(0)
        table_rst_preamble = table_obj.group(1)
        # If table directive contains code element(s)
        if re.search(r'``.+?``', table_rst):
            # Define placeholders
            crossrefs_rst = {}
            maths_rst = {}
            # Convert RST table to HTML table
            table_html = pypandoc.convert_text(table_rst, 'html', format='rst', extra_args=['--mathjax'])
            # Iterate over code elements
            for code_obj in re.finditer(r'<code>(.+?)</code>', table_html):
                if code_obj.group(1) in list_of_functions:
                    crossref_rst = r':any:`' + code_obj.group(1) + r'`'
                    if len(crossrefs_rst) < 10:
                        text_dummy = (len(crossref_rst) - 2) * 'C' + '0' + str(len(crossrefs_rst))
                    else:
                        text_dummy = (len(crossref_rst) - 2) * 'C' + str(len(crossrefs_rst))
                    # Store RST reference and text dummy
                    crossrefs_rst[text_dummy] = crossref_rst
                    # Replace code element
                    table_html = table_html.replace(code_obj.group(0), text_dummy)
            # Iterate over math elements
            for math_obj in re.finditer(r'<span class=\"math inline\">\\\((.+?)\\\)</span>', table_html):
                math_rst = r':math:`' + html.unescape(math_obj.group(1)) + r'`'
                if len(maths_rst) < 10:
                    text_dummy = (len(math_rst) - 2) * 'M' + '0' + str(len(maths_rst))
                else:
                    text_dummy = (len(math_rst) - 2) * 'M' + str(len(maths_rst))
                # Store RST math element and text dummy
                maths_rst[text_dummy] = math_rst
                # Replace math element
                table_html = table_html.replace(math_obj.group(0), text_dummy)
            # Convert HTML table back to RST table
            table_rst = pypandoc.convert_text(table_html, 'rst', format='html', extra_args=['--wrap=none'])
            if r'.. table::' not in table_rst:
                table_rst = table_rst_preamble + '\t' + table_rst.replace('\r\n', '\n\t')
            # Replace dummies
            for n in range(2):
                if n == 0:
                    dict_rst = crossrefs_rst
                else:
                    dict_rst = maths_rst
                for dummy, rst_element in dict_rst.items():
                    table_rst = table_rst.replace(dummy, rst_element)
            # Replace original table directive
            rst_code = rst_code.replace(table_obj.group(0), table_rst)

    # Iterate over code elements
    for code_obj in re.finditer(r'``(.+?)``', rst_code):
        if code_obj.group(1) in list_of_functions:
            crossref_rst = r':any:`' + code_obj.group(1) + r'`'
            # Replace code element
            rst_code = rst_code.replace(code_obj.group(0), crossref_rst)

    # Character replacements
    rst_code = rst_code.replace('\r\n', '\n')

    return rst_code

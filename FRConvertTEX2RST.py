import pypandoc
import re
import string


def convert(file, toc, doc):
  
    # Read code from file
    with open(file, 'r', encoding='latin-1') as f:
        function_code = f.read()

    # FUNCTION TITLE

    # Define placeholder for function title
    function_title = []
    # Define function title TeX-commands
    function_title_commands = [r'\FunctionRef', r'\PredefRef', r'\ProcedureRef', r'\SFunctionRef', r'\SuffixRef']
    # Get function title
    for command in function_title_commands:
        if command in function_code:
            # Define object
            if command + '[' in function_code:
                function_title_object = re.search(re.escape(command) + r'\[((.|\n)*)\]' + r'{.*?}', function_code)
            else:
                function_title_object = re.search(re.escape(command) + r'{.*?}', function_code)
            # Extract text element
            text_element = function_code[function_title_object.start():function_title_object.end()]
            # Define function title
            function_title = text_element.split('{')[1].split('}')[0].replace(' ', '_')
            if command == r'\SuffixRef':
                function_title = '.' + function_title
            if '\n' in text_element:
                function_code = function_code.replace(text_element, text_element.replace('\n', ''))
            if ' ' in text_element:
                function_code = function_code.replace(text_element, text_element.replace(' ', '_'))
            break

    # FUNCTION ARGUMENTS

    # Define placeholder to collect arguments
    function_arguments = []
    if r'\begin{Arguments}' in function_code:
        # Define environment
        arguments_env_object = re.search(r'(?s)\\begin{Arguments}.*?\\end{Arguments}', function_code)
        arguments_env = function_code[arguments_env_object.start():arguments_env_object.end()]
        if r'\item[' in arguments_env:
            arguments_objects = re.finditer(r'\\item\s{0,1}\[[^\]\[]*(?:\[[^\]\[]*\][^\]\[]*)*\]', function_code)
            for arguments_object in arguments_objects:
                function_argument = function_code[arguments_object.start():arguments_object.end()].replace(r'\item [', r'\item[').split(r'\item[')[1][:-1]
                if '(optional)' in function_argument:
                    function_argument = '[' + function_argument.replace('(optional)', '').strip() + ']'
                function_arguments.append(function_argument)

    # REMOVE COMMENTS

    # Split function code at new lines
    function_code_elements = function_code.split('\n')
    # Define list variable
    comments = []
    inputs = []
    # Iterate over code elements to find lines to remove
    for element in function_code_elements:
        # Comment in TeX starts with '%'
        if element and element[0] == '%':
            # Add comment to list
            comments.append(element)
        elif element and element[0:6] == r'\input':
            # Add comment to list
            inputs.append(element)
    # If there are comments in the function code
    if comments:
        for comment in comments:
            # Remove comments
            function_code = function_code.replace(comment, '')
    if inputs:
        for element in inputs:
            # Remove comments
            function_code = function_code.replace(element, '')

    # REMOVE COMMANDS W/O ARGUMENT

    noargument_commands = [r'\bigskip', r'\medskip', r'\smallskip', r'\newpage', r'\nonumber', r'\pagebreak', r'\pbsr']
    for command in noargument_commands:
        if command in function_code:
            function_code = function_code.replace(command, '')

    # REMOVE COMMANDS W/ ONE ARGUMENT

    argument_commands = [r'\AIMMSlink{', r'\index{', r'\IndexRef{', r'\input{']
    for command in argument_commands:
        if command in function_code:
            # Define object
            command_object = re.search(re.escape(command) + r'[^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)
            while command_object:
                # Extract text element
                text_element = function_code[command_object.start():command_object.end()]
                # Remove text element
                function_code = function_code.replace(text_element, '')
                # Redefine object
                command_object = re.search(re.escape(command) + r'[^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)

    # REMOVE COMMANDS W/ ONE ARGUMENT, BUT KEEP ARGUMENT

    keepargument_commands = [r'\mathit{']
    for command in keepargument_commands:
        if command in function_code:
            # Define object
            command_object = re.search(re.escape(command) + r'[^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)
            while command_object:
                # Extract text element
                text_element = function_code[command_object.start():command_object.end()]
                # Extract argument
                argument = text_element.split(command)[1][:-1]
                # Replace text element by argument
                function_code = function_code.replace(text_element, argument)
                # Redefine object
                command_object = re.search(re.escape(command) + r'[^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)

    # REPLACEMENTS

    # Define placeholders
    pdf_toc_element = []
    pdf_text_elements = []
    crossrefs_rst = []
    numrefs_rst = []
    eqrefs_rst = []
    hyperlinks = []
    hypertargets = []
    example_envs = []

    if r'{\sf\em ' in function_code:
        # Replace '{\sf\em ' with '{\em '
        function_code = function_code.replace(r'{\sf\em ', r'{\em ')

    if r'{\em' in function_code:
        # Pre-processing
        function_code = function_code.replace(r'{\em{}', r'{\em ').replace(r'{\em' + '\n', r'{\em ')
        # Replace '{\em ' with '\textit{'
        function_code = function_code.replace(r'{\em ', r'\textit{')

    if r'{\it' in function_code:
        # Pre-processing
        function_code = function_code.replace(r'{\it{}', r'{\it ').replace(r'{\it' + '\n', r'{\it ')
        # Replace '{\it ' with '\textit{'
        function_code = function_code.replace(r'{\it ', r'\textit{')

    if r'{\tt' in function_code:
        # Pre-processing
        function_code = function_code.replace(r'{\tt{}', r'{\tt ').replace(r'{\tt' + '\n', r'{\tt ')
        # Replace '{\tt ' with '\texttt{'
        function_code = function_code.replace(r'{\tt ', r'\texttt{')

    if r'\aimms' in function_code:
        # Pre-processing
        function_code = function_code.replace(r'{\aimms}', r'\aimms').replace(r'\aimms~', r'\aimms ')
        # Replace '\aimms' with 'AIMMS'
        function_code = function_code.replace(r'\aimms', 'AIMMS')

    if r'\gmp' in function_code:
        # Pre-processing
        function_code = function_code.replace(r'{\gmp}', r'\gmp').replace(r'\gmp~', r'\gmp ')
        # Replace '\gmp' with 'GMP'
        function_code = function_code.replace(r'\gmp', 'GMP')

    if r'\medspace' in function_code:
        # Replace '\medspace' with '\mspace{4mu}'
        function_code = function_code.replace(r'\medspace', r'\mspace{4mu}')

    if r'\item [' in function_code:
        # Replace '\item [' with '\item['
        function_code = function_code.replace(r'\item [', r'\item[')

    if '{table}' in function_code:
        # Replace '{table}' with '{aimmstable}'
        function_code = function_code.replace('{table}', '{aimmstable}')

    if r'\url{www' in function_code:
        # Replace '\url{www' with '\url{http://www'
        function_code = function_code.replace(r'\url{www', r'\url{http://www')

    if r' (optional)]' in function_code:
        # Replace ' (optional)]' with '_(optional)]'
        function_code = function_code.replace(r' (optional)]', r'_(optional)]')

    if r'\Examples' in function_code:
        # Replace '\Examples' with '\Example'
        function_code = function_code.replace(r'\Examples', r'\Example')

    if '{alignat}' in function_code:
        # Replace '{alignat}' with '{align}'
        function_code = function_code.replace('{alignat}', '{align}')

    if '{alignat*}' in function_code:
        # Replace '{alignat*}' with '{align}'
        function_code = function_code.replace('{alignat*}', '{align}')

    if '{align*}' in function_code:
        # Replace '{align*}' with '{align}'
        function_code = function_code.replace('{align*}', '{align}')

    if 'aimmsfigure' in function_code:
        # Replace '{aimmsfigure}' with '{figure}'
        function_code = function_code.replace('aimmsfigure', 'figure')

    if r'\break' in function_code:
        # Replace '\break' with '\\'
        function_code = function_code.replace(r'\break', r'\\')

    if r'\newline' in function_code:
        # Replace '\newline' with '\\'
        function_code = function_code.replace(r'\newline', r'\\')

    if r'\char`\\' in function_code:
        function_code = function_code.replace(r'\char`\\', '\\')

    if 'refdcb' in function_code:
        # Replace '\refdcb'
        function_code = function_code.replace('\\refdcb\\', r'\refdcb').replace(r'\refdcb', 'refdcb')

    if 'refsecn' in function_code:
        # Replace '\refsecn'
        function_code = function_code.replace('\\refsecn\\', r'\refsecn').replace(r'\refsecn', 'refsecn')

    if 'refdepr' in function_code:
        # Replace '\refdepr'
        function_code = function_code.replace('\\refdepr\\', r'\refdepr').replace(r'\refdepr', 'refdepr')

    if 'refinveq' in function_code:
        # Replace '\refinveq'
        function_code = function_code.replace('\\refinveq\\', r'\refinveq').replace(r'\refinveq', 'refinveq')

    if 'refsecdisc' in function_code:
        # Replace '\refsecdisc'
        function_code = function_code.replace('\\refsecdisc\\', r'\refsecdisc').replace(r'\refsecdisc', 'refsecdisc')

    if 'refsec1' in function_code:
        # Replace '\refsec1'
        function_code = function_code.replace('\\refsec1\\', r'\refsec1').replace(r'\refsec1', 'refsec1')

    if '~\\' in function_code:
        # Replace '~\'
        function_code = function_code.replace('~\\', ' \\')

    if '{align}' in function_code:
        # Remove any arguments from 'align' environment
        align_object = re.search(r'begin{align}{\d+}', function_code)
        while align_object:
            # Extract text element
            text_element = function_code[align_object.start():align_object.end()]
            # Replace text element
            function_code = function_code.replace(text_element, 'begin{align}')
            # Search for objects
            align_object = re.search(r'begin{align}{\d+}', function_code)

    if 'figscale' in function_code:
        # Replace 'figscale[]' with 'includegraphics'
        figscale_object = re.search(r'figscale\[\S+\]', function_code)
        while figscale_object:
            # Extract text element
            text_element = function_code[figscale_object.start():figscale_object.end()]
            # Replace text element
            function_code = function_code.replace(text_element, 'includegraphics')
            # Search for objects
            figscale_object = re.search(r'figscale\[\S+\]', function_code)

    if r'{\sc ' in function_code:
        # Replace '{\sc ...}'
        sc_object = re.search(r'{\\sc [^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)
        while sc_object:
            # Extract text element
            text_element = function_code[sc_object.start():sc_object.end()]
            # Extract and uppercase text
            text = text_element.split(r'{\sc ')[1][:-1].upper()
            # Replace text element by extracted text
            function_code = function_code.replace(text_element, text)
            # Search for objects
            sc_object = re.search(r'{\\sc [^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)

    if r'\hyperlink' in function_code:
        # Replace '\hyperlink'
        hyperlink_count = 0
        hyperlink_object = re.search(r'\\hyperlink{[^}{]*}{[^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)
        while hyperlink_object:
            # Add increment
            hyperlink_count += 1
            # Extract text element
            text_element = function_code[hyperlink_object.start():hyperlink_object.end()]
            # Extract label and text
            label = text_element.split(r'\hyperlink{')[1].split('}{')[0]
            text = text_element.split(r'\hyperlink{')[1].split('}{')[1][:-1]
            # Save label and text
            hyperlinks.append({'label': label, 'text': text})
            # Replace text element by plain-text 'dummy'
            if not function_code[hyperlink_object.start() - 1].isspace() and not function_code[hyperlink_object.end() + 1].isspace():
                function_code = function_code.replace(text_element, ' hyperlink' + str(hyperlink_count) + ' ')
            elif not function_code[hyperlink_object.start() - 1].isspace():
                function_code = function_code.replace(text_element, ' hyperlink' + str(hyperlink_count))
            elif not function_code[hyperlink_object.end() + 1].isspace():
                function_code = function_code.replace(text_element, 'hyperlink' + str(hyperlink_count) + ' ')
            else:
                function_code = function_code.replace(text_element, 'hyperlink' + str(hyperlink_count))
            # Search for objects
            hyperlink_object = re.search(r'\\hyperlink{[^}{]*}{[^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)

    if r'\hypertarget' in function_code:
        # Replace '\hypertarget'
        hypertarget_count = 0
        hypertarget_object = re.search(r'\\hypertarget{[^}{]*(?:{[^}{]*}[^}{]*)*}{[^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)
        while hypertarget_object:
            # Add increment
            hypertarget_count += 1
            # Extract text element
            text_element = function_code[hypertarget_object.start():hypertarget_object.end()]
            # Extract label and text
            label = text_element.split(r'\hypertarget{')[1].split('}{')[0]
            text = text_element.split(r'\hypertarget{')[1].split('}{')[1][:-1]
            # Save label and text
            hypertargets.append({'label': label, 'text': text})
            # Replace text element by plain-text 'dummy'
            if not function_code[hypertarget_object.start() - 1].isspace() and not function_code[hypertarget_object.end() + 1].isspace():
                function_code = function_code.replace(text_element, ' hypertarget' + str(hypertarget_count) + ' ')
            elif not function_code[hypertarget_object.start() - 1].isspace():
                function_code = function_code.replace(text_element, ' hypertarget' + str(hypertarget_count))
            elif not function_code[hypertarget_object.end() + 1].isspace():
                function_code = function_code.replace(text_element, 'hypertarget' + str(hypertarget_count) + ' ')
            else:
                function_code = function_code.replace(text_element, 'hypertarget' + str(hypertarget_count))
            # Search for objects
            hypertarget_object = re.search(r'\\hypertarget{[^}{]*(?:{[^}{]*}[^}{]*)*}{[^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)

    if r'\begin{enumerate}' in function_code:
        # Find all enumerate environments
        enumerate_objects = re.finditer(r'(?s)\\begin{enumerate}.*?\\end{enumerate}', function_code)
        # Iterate over enumerate environments in reverse order
        for enumerate_object in reversed(list(enumerate_objects)):
            # Define environment
            enumerate_env = function_code[enumerate_object.start():enumerate_object.end()]
            # Extract items
            enumerate_items = enumerate_env.split(r'\begin{enumerate}')[1].split(r'\end{enumerate}')[0].strip().split('\n')
            # If all items contain an argument
            if all(r'\item[' in item for item in enumerate_items):
                # Change the original enumerate environment to a description environment
                function_code = function_code.replace(enumerate_env, enumerate_env.replace('enumerate', 'description'))

    if '%' in function_code:
        # Find indices '%'
        percent_indices = [i for i, char in enumerate(function_code) if char == '%']
        # Iterate over indices in reverse order
        for index in sorted(percent_indices, reverse=True):
            # If there is no '\' before '%'
            if function_code[index - 1] != '\\':
                # Replace '%' with '\%'
                function_code = ''.join([function_code[:index], r'\%', function_code[index + 1:]])

    # REPLACE TEX CROSS-REFERENCES BY RST CROSS-REFERENCES

    reference_commands = [r'\FRef{', r'\PRef{', r'\PreRef{', r'\SufRef{']
    for command in reference_commands:
        if command in function_code:
            # Define object
            command_object = re.search(re.escape(command) + r'[^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)
            while command_object:
                # Extract text element
                text_element = function_code[command_object.start():command_object.end()]
                # Extract label cross-reference
                crossref_label = text_element.split(command)[1][:-1]
                # Define .RST cross-reference
                if command == r'\PRef{':
                    crossref_rst = ':aimms:procedure:`' + crossref_label + '`'
                elif command == r'\PreRef{':
                    crossref_rst = ':aimms:set:`' + crossref_label + '`'
                elif command == r'\SufRef{':
                    crossref_label = '.' + crossref_label
                    crossref_rst = ':ref:`' + crossref_label + '`'
                else:
                    crossref_rst = ':aimms:func:`' + crossref_label + '`'
                # Store .RST cross-reference
                crossrefs_rst.append(crossref_rst)
                # Replace text element by plain-text 'dummy'
                function_code = function_code.replace(text_element, 'CrossRef' + str(len(crossrefs_rst)))
                # Redefine object
                command_object = re.search(re.escape(command) + r'[^}{]*(?:{[^}{]*}[^}{]*)*}', function_code)

    if r'\ref{' in function_code or r'\pageref{' in function_code:

        # Split function code at 'space'
        function_code_elements = function_code.split(' ')

        # Define reference types
        ref_types = [r'\ref{table', r'\ref{figure', r'\ref{eq', r'\ref{LR', r'\ref{UG', r'\pageref{']

        # Find indices of elements containing '\ref' or '\pageref'
        ref_indices = [i for i, element in enumerate(function_code_elements) if r'\ref{' in element or r'\pageref{' in element]

        # Pre-processing
        element_modification = False
        for index in ref_indices:
            if '\n' in function_code_elements[index]:
                function_code_elements[index] = function_code_elements[index].replace('\n', ' ')
                element_modification = True
        # If element is modified
        if element_modification:
            # Reassemble code
            function_code = ' '.join(function_code_elements)
            # Resplit function code at 'space'
            function_code_elements = function_code.split(' ')
            # Redefine indices of elements containing '\ref' or '\pageref'
            ref_indices = [i for i, element in enumerate(function_code_elements) if r'\ref{' in element or r'\pageref{' in element]

        # Iterate over found indices
        for i, index in enumerate(ref_indices):

            # Define element
            ref_element = function_code_elements[index]

            # Determine type of reference
            ref_type = r'\ref{'
            for ref_type in ref_types:
                if ref_type in ref_element:
                    break
                else:
                    ref_type = r'\ref{'

            if 'table' in ref_type or 'figure' in ref_type:

                # Extract label
                numref_label = ref_element.split(r'\ref{')[1].split('}')[0]
                # Define .RST label
                numref_rst = ':numref:`' + numref_label + '`'
                # Store .RST cross-reference
                numrefs_rst.append(numref_rst)
                # Define temporary plain-text 'dummy' cross-reference to be replaced at the end by .RST cross-reference
                numref_dummy = 'NumRef' + str(len(numrefs_rst)) + '}'.join(ref_element.split(r'\ref{')[1].split('}')[1:])
                # Replace element by plain-text 'dummy'
                function_code_elements[index] = re.sub(re.escape(ref_type) + r'\S*}\S*', numref_dummy, ref_element)

            elif 'eq' in ref_type:

                # Extract label
                eqref_label = ref_element.split(r'\ref{')[1].split('}')[0]
                # Define .RST label
                eqref_rst = ':eq:`' + eqref_label + '`'
                # Store .RST cross-reference
                eqrefs_rst.append(eqref_rst)
                # Define temporary plain-text 'dummy' cross-reference to be replaced at the end by .RST cross-reference
                eqref_dummy = ref_element.split(r'\ref{')[0] + 'EqRef' + str(len(eqrefs_rst)) + '}'.join(ref_element.split(r'\ref{')[1].split('}')[1:])
                # Replace element by plain-text 'dummy'
                function_code_elements[index] = re.sub(re.escape(ref_type) + r'\S*}\S*', eqref_dummy, ref_element)

            elif 'LR' in ref_type or 'UG' in ref_type or 'pageref' in ref_type:

                # Check if 'function_title' exists
                if function_title:

                    # Find function in table of contents of Function Reference PDF
                    if not pdf_toc_element:
                        for i, element in enumerate(toc):
                            if function_title == element[1]:
                                pdf_toc_element = i
                                break
                    # If function was found in table of contents
                    if pdf_toc_element:
                        if not pdf_text_elements:
                            # Find page number of function
                            first_page_number = toc[pdf_toc_element][2] - 1
                            # Find number of pages dedicated to function
                            number_of_pages = toc[pdf_toc_element + 1][2] - 1 - first_page_number
                            # Iterate over pages
                            for i in range(number_of_pages):
                                # Add increment
                                page_number = first_page_number + i
                                # Extract text, split into elements and add elements to placeholder
                                pdf_text_elements = pdf_text_elements + doc[page_number].getText().split()

                        # Extract code elements before and after '\ref' or '\pageref' element
                        if not index - 1 < 0:
                            # Code element before '\ref' or '\pageref' element
                            pre_index = index - 1
                            pre_crossref = function_code_elements[pre_index].strip()
                            while not pre_crossref:
                                pre_index -= 1
                                if not pre_index < 0:
                                    pre_crossref = function_code_elements[pre_index].strip()
                                else:
                                    pre_crossref = ''
                                    break
                            # Check if element is a command
                            if pre_crossref[0:1] == '\\' or pre_crossref[0:2] == '{\\':
                                pre_crossref = 'Command'
                        else:
                            # If there is no element before '\ref' or '\pageref' element
                            pre_crossref = ''
                        if not index + 1 > len(function_code_elements) - 1:
                            # Code element after '\ref' or '\pageref' element
                            post_index = index + 1
                            post_crossref = function_code_elements[post_index].strip()
                            while not post_crossref:
                                post_index += 1
                                if not post_index > len(function_code_elements) - 1:
                                    post_crossref = function_code_elements[post_index].strip()
                                else:
                                    post_crossref = ''
                                    break
                            # Check if element is a command
                            if post_crossref[0:1] == '\\' or post_crossref[0:2] == '{\\':
                                post_crossref = 'Command'
                        else:
                            # If there is no element after '\ref' or '\pageref' element
                            post_crossref = ''

                        # Post-processing
                        if '\n' in pre_crossref:
                            pre_crossref = pre_crossref.split('\n')[-1]
                        if '\n' in post_crossref:
                            post_crossref = post_crossref.split('\n')[0]

                        # Iterate over text elements
                        i_start = 0
                        for i, element in enumerate(pdf_text_elements[i_start:]):

                            # Text element before text element of concern
                            if i + i_start == 0:
                                pre_element = ''
                            else:
                                pre_element = pdf_text_elements[i + i_start - 1]
                            # Text element after text element of concern
                            if i + i_start == len(pdf_text_elements) - 1:
                                post_element = ''
                            else:
                                post_element = pdf_text_elements[i + i_start + 1]

                            # Check relation code elements vs. text elements
                            if (pre_crossref == 'Command' and post_element.lower() == post_crossref.lower()) or (
                                    post_crossref == 'Command' and pre_element.lower() == pre_crossref.lower()) or (
                                    pre_element.lower() == pre_crossref.lower() and post_element.lower() == post_crossref.lower()):
                                # Modify element
                                mod_element = element.translate(str.maketrans('', '', string.punctuation))
                                # Check element
                                if mod_element.isdigit() or sum(char.isalpha() for char in mod_element) == 1:
                                    function_code_elements[index] = re.sub(re.escape(ref_type) + r'\S*}\S*', element, ref_element)
                                    break

            else:

                # Extract label
                crossref_label = ref_element.split(r'\ref{')[1].split('}')[0]
                # Define .RST label
                crossref_rst = ':ref:`' + crossref_label + '`'
                # Store .RST cross-reference
                crossrefs_rst.append(crossref_rst)
                # Define temporary plain-text 'dummy' cross-reference to be replaced at the end by .RST cross-reference
                crossref_dummy = ref_element.split(r'\ref{')[0] + 'CrossRef' + str(len(crossrefs_rst)) + '}'.join(ref_element.split(r'\ref{')[1].split('}')[1:])
                # Replace element by plain-text 'dummy'
                function_code_elements[index] = re.sub(re.escape(ref_type) + r'\S*}\S*', crossref_dummy, ref_element)

        # Reassemble code
        function_code = ' '.join(function_code_elements)

    # CONVERT TEX-COMMANDS AND TEX-ENVIRONMENTS NOT RECOGNISED BY PANDOC TO RST

    # Split function code into separate elements
    function_code_elements = function_code.split()

    # Iterate over function title commands
    for command in function_title_commands:
        if command in function_code:
            # Find all indices of elements containing the command
            function_title_indices = [i for i, element in enumerate(function_code_elements) if command in element]
            # Iterate over indices
            for index in function_title_indices:
                # Define .RST function title
                function_title_rst = function_title + '\n' + len(function_title) * '='
                # Define .RST function title label
                function_label_rst = '.. _' + function_title + ':'
                # Define .RST function in domain
                if command == r'\ProcedureRef':
                    function_domain_rst = '.. aimms:procedure:: ' + function_title + '(' + ', '.join(function_arguments) + ')'
                elif command == r'\PredefRef':
                    function_domain_rst = '.. aimms:set:: ' + function_title
                elif command == r'\SuffixRef':
                    function_domain_rst = ''
                else:
                    function_domain_rst = '.. aimms:function:: ' + function_title + '(' + ', '.join(function_arguments) + ')'
                if ', [' in function_domain_rst:
                    function_domain_rst = function_domain_rst.replace(', [', '[, ')
                # Combine .RST function title and label
                if function_domain_rst:
                    function_label_title_rst = function_domain_rst + '\n\n' + function_label_rst + '\n\n' + function_title_rst + '\n\n'
                else:
                    function_label_title_rst = function_label_rst + '\n\n' + function_title_rst + '\n\n'
                # Replace element
                function_code_elements[index] = function_label_title_rst

    if r'\begin{prototype}' in function_code:

        # Define prototype environment preamble
        prototype_environment_preamble = '\n' + '.. code-block:: aimms' + '\n'
        # Number of prototype environments
        n_prototype_environments = function_code.count(r'\begin{prototype}')

        # Find begin and end indices for each prototype environment
        prototype_begin_indices = [i for i, element in enumerate(function_code_elements) if r'\begin{prototype}' in element]
        prototype_end_indices = [i for i, element in enumerate(function_code_elements) if r'\end{prototype}' in element]

        # Split function code
        function_code_prototype_split = function_code.split(r'\begin{prototype}')

        # Iterate over 'prototype'-environments in reverse order
        for i in range(n_prototype_environments):
            # Extract prototype content
            prototype_content = function_code_prototype_split[-1 - i].split(r'\end{prototype}')[0].replace('\n', '\n' + 4 * ' ')
            # Assemble .RST elements
            prototype_environment = prototype_environment_preamble + prototype_content + '\n'
            # Modify function_code_elements
            function_code_elements[prototype_begin_indices[-1 - i]] = prototype_environment
            del function_code_elements[prototype_begin_indices[-1 - i] + 1:prototype_end_indices[-1 - i] + 1]

    if r'\begin{example}' in function_code:

        # Define example environment preamble
        example_environment_preamble = '\n' + '.. code-block:: aimms' + '\n'
        # Number of example environments
        n_example_environments = function_code.count(r'\begin{example}')

        # Find begin and end indices for each example environment
        example_begin_indices = [i for i, element in enumerate(function_code_elements) if r'\begin{example}' in element]
        example_end_indices = [i for i, element in enumerate(function_code_elements) if r'\end{example}' in element]

        # Split function code
        function_code_example_split = function_code.split(r'\begin{example}')

        # Iterate over 'example'-environments in reverse order
        for i in range(n_example_environments):
            # Extract example content
            example_content = function_code_example_split[-1 - i].split(r'\end{example}')[0].replace('\n', '\n' + 4 * ' ')
            # Assemble .RST elements
            example_environment = example_environment_preamble + example_content + '\n'
            # Store .RST environment
            example_envs.append(example_environment)
            # Define temporary plain-text 'dummy' to be replaced at the end by .RST environment
            example_dummy = 'ExampleEnv' + str(len(example_envs))
            # Modify function_code_elements
            function_code_elements[example_begin_indices[-1 - i]] = example_dummy
            del function_code_elements[example_begin_indices[-1 - i] + 1:example_end_indices[-1 - i] + 1]

    if r'\begin{aimmstable}' in function_code:

        # Number of table environments
        n_table_environments = function_code.count(r'\begin{aimmstable}')

        # Find begin and end indices for each table environment
        table_begin_indices = [i for i, element in enumerate(function_code_elements) if r'\begin{aimmstable}' in element]
        table_end_indices = [i for i, element in enumerate(function_code_elements) if r'\end{aimmstable}' in element]

        # Split function code
        function_code_table_split = function_code.split(r'\begin{aimmstable}')

        # Iterate over found table environments in reverse order
        for i in range(n_table_environments):
            # Predefine variables
            table_environment_rst = ''
            table_caption_rst = ''
            table_label = ''
            # Extract table content
            table_content = function_code_table_split[-1 - i].split(r'\end{aimmstable}')[0]
            # If the table has a caption, extract caption
            if r'\caption' in table_content:
                table_caption_elements = table_content.split(r'\caption{')[1].split('}')
                for j, element in enumerate(table_caption_elements):
                    if '{' not in element:
                        table_caption = '}'.join(table_caption_elements[0:j + 1])
                        table_caption_rst = pypandoc.convert_text(table_caption, 'rst', format='latex').replace('\r', '').replace('\n', ' ')
                        # Remove caption from table content
                        table_content = table_content.replace(r'\caption{' + table_caption + '}', '')
                        break
            # If the table has a label, extract label
            if r'\label' in table_content:
                table_label = table_content.split(r'\label{')[1].split('}')[0]
                # Remove label from table content
                table_content = table_content.replace(r'\label{' + table_label + '}', '')
            # If there is a tabular environment in the table environment
            if r'\begin{tabular}' in table_content:
                # Define tabular environment
                tabular_environment = r'\begin{tabular}' + table_content.split(r'\begin{tabular}')[1].split(r'\end{tabular}')[0] + r'\end{tabular}'
                # Extract (possible) part before tabular environment
                pre_tabular_environment = table_content.split(r'\begin{tabular}')[0]
                # Extract (possible) part after tabular environment
                post_tabular_environment = table_content.split(r'\end{tabular}')[1]
                # Convert environments to .RST
                tabular_environment_rst = pypandoc.convert_text(tabular_environment, 'rst', format='latex')
                pre_tabular_environment_rst = pypandoc.convert_text(pre_tabular_environment, 'rst', format='latex')
                post_tabular_environment_rst = pypandoc.convert_text(post_tabular_environment, 'rst', format='latex')
                # Add caption or label to .RST preamble if applicable
                if table_caption_rst and not table_label:
                    # Define preamble
                    table_environment_preamble = '\n' + '.. table:: ' + table_caption_rst + '\n\n'
                elif table_caption_rst or table_label:
                    # Define preamble
                    table_environment_preamble = '\n' + '.. _' + table_label + ':' + '\n\n' + '.. table:: ' + table_caption_rst + '\n\n'
                else:
                    # Define preamble
                    table_environment_preamble = '\n' + '.. table:: ' + '\n\n'
                # Assemble .RST elements
                table_environment_rst = pre_tabular_environment_rst + '\n' + table_environment_preamble + 4 * ' ' + tabular_environment_rst.replace(
                    '\n', '\n' + 4 * ' ') + '\n' + post_tabular_environment_rst + '\n'
            # It there is no tabular environment in the table environment
            else:
                # Check if there is a code-block in the table environment
                for element in function_code_elements[table_begin_indices[-1 - i] + 1:table_end_indices[-1 - i]]:
                    if 'ExampleEnv' in element:
                        example_object = re.search(r'\bExampleEnv\d+\b', element)
                        example_envs_index = int(element[example_object.start():example_object.end()].split('ExampleEnv')[1]) - 1
                        if '.. code-block:: aimms' in example_envs[example_envs_index]:
                            if table_caption_rst and table_label:
                                # Redefine code-block preamble
                                code_block_preamble_redefined = '.. code-block:: aimms' + '\n' + '   :caption: ' + table_caption_rst + '\n' + '   :name: ' + table_label
                            elif table_caption_rst:
                                # Redefine code-block preamble
                                code_block_preamble_redefined = '.. code-block:: aimms' + '\n' + '   :caption: ' + table_caption_rst
                            elif table_label:
                                # Redefine code-block preamble
                                code_block_preamble_redefined = '.. code-block:: aimms' + '\n' + '   :name: ' + table_label
                            else:
                                code_block_preamble_redefined = '.. code-block:: aimms'
                            # Replace old code-block preamble and define environment
                            table_environment_rst = example_envs[example_envs_index].replace('.. code-block:: aimms', code_block_preamble_redefined)
                            break

                # If none of the above
                if not table_environment_rst:
                    table_environment_rst = pypandoc.convert_text(table_content, 'rst', format='latex')

            # Modify function_code_elements
            function_code_elements[table_begin_indices[-1 - i]] = table_environment_rst
            del function_code_elements[table_begin_indices[-1 - i] + 1:table_end_indices[-1 - i] + 1]

    # SECTION HANDLING

    # Define section elements
    section_elements = [r'\begin{Arguments}', r'\Datatype', r'\Definition', r'\Dimension', r'\Equation', r'\Example',
                        r'\FunctionPrototype', r'\MathematicalFormulation', r'\Remarks', r'\Returns', r'\SeeAlso',
                        r'\Updatability']

    # Find the start and end indices of the section elements
    sections_indices = []
    for section_element in section_elements:
        # If section element is in function code
        if section_element in function_code:
            # Find start index of section (assuming section is only once present)
            index_start_section = min([i for i, element in enumerate(function_code_elements) if section_element in element])
            # Find end index of section
            if section_element == r'\begin{Arguments}':
                index_end_section = min([i for i, element in enumerate(function_code_elements) if r'\end{Arguments}' in element and i > index_start_section])
            else:
                index_end_section_options = [i for i, element in enumerate(function_code_elements) if any(x in element for x in section_elements) and i > index_start_section]
                if index_end_section_options:
                    index_end_section = min(index_end_section_options)
                else:
                    index_end_section = len(function_code_elements)
            # Combine start and end indices of the section in a tuple
            indices_section = (index_start_section, index_end_section)
            # Add tuple to list
            sections_indices.append(indices_section)

    # Sort sections based on starting index from high to low
    sections_indices.sort(key=lambda tup: tup[0], reverse=True)

    # Iterate over tuples
    for section_indices in sections_indices:

        # Extract start and end indices
        index_start_section = section_indices[0]
        index_end_section = section_indices[1]

        # Analyse function code element at start index section

        if r'\begin{Arguments}' in function_code_elements[index_start_section]:

            # Convert 'Arguments'-environment to .RST

            # Define section
            arguments_section = '\n' + 'Arguments' + '\n' + 9 * '-' + '\n\n'
            # Extract arguments from function code
            arguments_elements = function_code.split(r'\begin{Arguments}')[1].split(r'\end{Arguments}')[0].replace('\n', ' ').split()
            # Find indices arguments
            arguments_indices = [i for i, element in enumerate(arguments_elements) if r'\item[' in element]
            # Iterate over arguments
            for i, index in enumerate(arguments_indices):
                # Argument name
                argument_name = arguments_elements[index].split('[')[1].split(']')[0]
                # Argument description
                index_start_description = index + 1
                if i < len(arguments_indices) - 1:
                    index_end_description = arguments_indices[i + 1]
                else:
                    index_end_description = None
                # Define and convert argument description
                argument_description_tex = ' '.join(arguments_elements[index_start_description:index_end_description])
                argument_description_rst = pypandoc.convert_text(argument_description_tex, 'rst', format='latex').replace('\r', '')
                # Modify section
                arguments_section = arguments_section + 4 * ' ' + '*' + argument_name + '*' + '\n' + 8 * ' ' + argument_description_rst.replace('\n', '\n' + 8 * ' ') + '\n'
            # Final modification section
            arguments_section = arguments_section + '\n'
            # Modify function_code_elements
            function_code_elements[index_start_section] = arguments_section
            del function_code_elements[index_start_section + 1:index_end_section + 1]

        else:
            # Define section title
            if r'\Datatype' in function_code_elements[index_start_section]:
                section_title = 'Datatype'
            elif r'\Definition' in function_code_elements[index_start_section]:
                section_title = 'Definition'
            elif r'\Dimension' in function_code_elements[index_start_section]:
                section_title = 'Dimension'
            elif r'\Equation' in function_code_elements[index_start_section]:
                section_title = 'Equation'
            elif r'\Example' in function_code_elements[index_start_section]:
                section_title = 'Example'
            elif r'\FunctionPrototype' in function_code_elements[index_start_section]:
                section_title = 'Function Prototype'
            elif r'\MathematicalFormulation' in function_code_elements[index_start_section]:
                section_title = 'Mathematical Formulation'
            elif r'\Returns' in function_code_elements[index_start_section]:
                section_title = 'Return Value'
            elif r'\Updatability' in function_code_elements[index_start_section]:
                section_title = 'Updatability'
            else:
                section_title = 'Section Title'

            # Define section preamble
            if r'\Remarks' in function_code_elements[index_start_section]:
                section_preamble = '\n' + '.. note::' + '\n'
            elif r'\SeeAlso' in function_code_elements[index_start_section]:
                section_preamble = '\n' + '.. seealso::' + '\n'
            else:
                section_preamble = '\n' + section_title + '\n' + len(section_title) * '-' + '\n'

            # Define and convert section content
            section_code_elements = function_code_elements[index_start_section + 1:index_end_section]
            # Define variables
            untouched_parts_section = []
            start_part_section = []
            for index, element in enumerate(section_code_elements):
                if '\n' not in element:
                    if start_part_section == []:
                        start_part_section = index
                elif '\n' in element and start_part_section != []:
                    end_part_section = index
                    untouched_parts_section.append([start_part_section, end_part_section])
                    start_part_section = []
            if start_part_section != []:
                end_part_section = len(section_code_elements)
                untouched_parts_section.append([start_part_section, end_part_section])
            # If there are untouched parts in section
            if untouched_parts_section:
                # Iterate over untouched parts
                for part in reversed(untouched_parts_section):
                    part_content_tex_elements = section_code_elements[part[0]:part[1]]
                    for index, element in enumerate(part_content_tex_elements):
                        if r'\includegraphics' in element:
                            part_content_tex_elements[index] = element.replace(r'\includegraphics', '\n\n' + r'\includegraphics').replace('}', '}\n\n')
                    part_content_tex = ' '.join(part_content_tex_elements)
                    part_content_rst = pypandoc.convert_text(part_content_tex, 'rst', format='latex')
                    part_rst = '\n' + part_content_rst + '\n'
                    section_code_elements[part[0]] = part_rst
                    del section_code_elements[part[0] + 1:part[1]]

            # Define section content in .RST
            section_content_rst = ' '.join(section_code_elements)

            # Add indentation
            section_content_rst_elements = section_content_rst.replace('\r\n', '\n').split('\n')
            for i, element in enumerate(section_content_rst_elements):
                section_content_rst_elements[i] = 4 * ' ' + element
            section_content_rst = '\n'.join(section_content_rst_elements)

            # Assemble .RST elements
            section = section_preamble + section_content_rst + '\n'

            # Modify function_code_elements
            function_code_elements[index_start_section] = section
            del function_code_elements[index_start_section + 1:index_end_section]

    # CONVERT REMAINING TEX-CODE TO RST USING PANDOC

    # Define variables
    untouched_parts = []
    start_part = []
    # Find untouched parts in the TeX-code (recognised by '\n' not being present)
    for index, element in enumerate(function_code_elements):
        if '\n' not in element:
            if start_part == []:
                start_part = index
        elif '\n' in element and start_part != []:
            end_part = index
            untouched_parts.append([start_part, end_part])
            start_part = []
    if start_part != []:
        end_part_section = len(function_code_elements)
        untouched_parts.append([start_part, end_part_section])
    # If there are untouched parts
    if untouched_parts:
        # Iterate over untouched parts
        for part in reversed(untouched_parts):
            part_content_tex = ' '.join(function_code_elements[part[0]:part[1]])
            if r'\subsection' in part_content_tex and r'\section' not in part_content_tex:
                part_content_tex = part_content_tex.replace(r'\subsection', r'\subsubsection')
            if r'\section' in part_content_tex and r'\chapter' not in part_content_tex:
                part_content_tex = part_content_tex.replace(r'\section', r'\subsection')
            part_content_rst = pypandoc.convert_text(part_content_tex, 'rst', format='latex')
            part_rst = '\n' + part_content_rst + '\n'
            function_code_elements[part[0]] = part_rst
            del function_code_elements[part[0] + 1:part[1]]
    # Reassemble code
    function_code = ' '.join(function_code_elements)

    # FINALLY

    # Check if there are cross-references
    if crossrefs_rst:
        # Replace plain-text 'dummy' cross-reference
        for i, crossref_rst in enumerate(crossrefs_rst, 1):
            function_code = re.sub(r'\bCrossRef' + str(i) + r'\b', crossref_rst, function_code)
    if numrefs_rst:
        # Replace plain-text 'dummy' cross-reference
        for i, numref_rst in enumerate(numrefs_rst, 1):
            function_code = re.sub(r'\bNumRef' + str(i) + r'\b', numref_rst, function_code)
    if eqrefs_rst:
        # Replace plain-text 'dummy' cross-reference
        for i, eqref_rst in enumerate(eqrefs_rst, 1):
            function_code = re.sub(r'\bEqRef' + str(i) + r'\b', eqref_rst, function_code)
        # Remove any unnecessary parentheses
        eq_object = re.search(r'\(:eq:`\S+`\)', function_code)
        while eq_object:
            # Extract text element
            text_element = function_code[eq_object.start():eq_object.end()]
            # Replace text element
            function_code = function_code.replace(text_element, text_element.split('(')[1].split(')')[0])
            # Search for objects
            eq_object = re.search(r'\(:eq:`\S+`\)', function_code)

    # Check if there are hyperlinks
    if hyperlinks:
        # Iterate over hyperlinks
        for i, hyperlink in enumerate(hyperlinks, 1):
            # Extract hyperlink label and text
            label = hyperlink['label']
            text = pypandoc.convert_text(hyperlink['text'], 'rst', format='latex')
            # Replace plain-text 'dummy' hyperlinks
            function_code = re.sub(r'\bhyperlink' + str(i) + r'\b', ':ref:`' + text.strip() + '<' + label + '>`', function_code)

    # Check if there are hypertargets
    if hypertargets:
        # Iterate over hypertargets
        for i, hypertarget in enumerate(hypertargets, 1):
            # Extract hypertarget label and text
            label = hypertarget['label']
            text = pypandoc.convert_text(hypertarget['text'], 'rst', format='latex')
            # Replace plain-text 'dummy' hypertargets
            function_code = re.sub(r'\bhypertarget' + str(i) + r'\b', '.. _' + label + ':\n\n' + text.strip(), function_code)

    # Check if there are example environments
    if example_envs:
        # Split function code at new lines
        function_code_elements = function_code.split('\n')
        # Iterate over example environments
        for i, example_env in enumerate(example_envs, 1):
            indent = ''
            for element in function_code_elements:
                example_object = re.search(r'\bExampleEnv' + str(i) + r'\b', element)
                if example_object:
                    # Define indent
                    indent = (len(element) - len(element.lstrip())) * ' '
                    if indent:
                        # Add indent to each line in environment
                        example_env = example_env.split('\n')
                        for j, line in enumerate(example_env[1:], 1):
                            example_env[j] = indent + line
                        example_env = '\n'.join(example_env)
            # Replace plain-text 'dummy'
            function_code = re.sub(r'\bExampleEnv' + str(i) + r'\s', '\n' + example_env + '\n' + indent, function_code)

    # Replace non-breaking spaces with a regular space
    function_code = function_code.replace('\xa0', ' ')

    # Replace '\r\n' by '\n'
    function_code = function_code.replace('\r\n', '\n')

    # Replace '’' and '‘' by '''
    function_code = function_code.replace('’', '\'').replace('‘', '\'')

    # Replace '“' and '”' by ''
    function_code = function_code.replace('“', '\"').replace('”', '\"')

    # Replace '–' by '--'
    function_code = function_code.replace('–', '--')

    # Replace '…' by '...'
    function_code = function_code.replace('…', '...')

    # Replace ' (optional)' with '_(optional)'
    function_code = function_code.replace(r'_(optional)', r' (optional)')

    # Replace monospaced reference to function by .RST function reference
    if '.. aimms:procedure:: ' in function_code:
        function_code = function_code.replace(r'``' + function_title + r'``', r':aimms:procedure:`' + function_title + r'`')
    elif '.. aimms:set:: ' in function_code:
        function_code = function_code.replace(r'``' + function_title + r'``', r':aimms:set:`' + function_title + r'`')
    elif '.. aimms:function:: ' in function_code:
        function_code = function_code.replace(r'``' + function_title + r'``', r':aimms:func:`' + function_title + r'`')

    # Add links
    if 'refdcb' in function_code:
        function_code = function_code.replace('refdcb', 'Day count basis :ref:`methods<ff.dcb>`.')
    if 'refsecn' in function_code:
        function_code = function_code.replace('refsecn', 'General :ref:`equations<ff.sec.coupn>` for securities with multiple coupons.')
    if 'refdepr' in function_code:
        function_code = function_code.replace('refdepr', 'General equations for computing :ref:`depreciations<FF.depreq>`.')
    if 'refinveq' in function_code:
        function_code = function_code.replace('refinveq', 'General :ref:`equations<FF.inveq>` for investments with constant, periodic payments.')
    if 'refsecdisc' in function_code:
        function_code = function_code.replace('refsecdisc', 'General :ref:`equations<ff.sec.disc>` for discounted securities.')
    if 'refsec1' in function_code:
        function_code = function_code.replace('refsec1', 'General :ref:`equations<ff.sec.coup1>` for securities with one coupon.')
    if 'Language Reference' in function_code:
        function_code = function_code.replace('Language Reference', '`Language Reference <https://documentation.aimms.com/_downloads/AIMMS_ref.pdf>`__')
    if 'User\'s Guide' in function_code:
        function_code = function_code.replace('User\'s Guide', '`User\'s Guide <https://documentation.aimms.com/_downloads/AIMMS_user.pdf>`__')

    # Check if there are math environments that need labelling
    if '.. math::' in function_code:
        # Split function code at new lines
        function_code_elements = function_code.split('\n')
        # Iterate over elements
        for i, element in enumerate(function_code_elements):
            if '.. math::' in element:
                # Define indent
                indent = element.split('.. math::')[0] + 3 * ' '
                if r'\label{' in element:
                    # Extract label
                    eq_label = element.split(r'\label{')[1].split('}')[0]
                    # Remove label from element
                    element = element.replace(r'\label{' + eq_label + '}', '')
                    # Replace element
                    function_code_elements[i] = element + '\n' + indent + ':label: ' + eq_label
                else:
                    # Define length indent
                    len_indent = len(indent)
                    for ii, subelement in enumerate(function_code_elements[i + 1:], 1):
                        if subelement[0:len_indent] == indent and r'\label{' in subelement:
                            # Extract label
                            eq_label = subelement.split(r'\label{')[1].split('}')[0]
                            # Remove label from subelement
                            subelement = subelement.replace(r'\label{' + eq_label + '}', '')
                            # Replace subelement
                            function_code_elements[i + ii] = subelement
                            # Define new math environment preamble including label
                            math_label_preamble = '.. math::' + '\n' + indent + ':label: ' + eq_label
                            # Replace element
                            function_code_elements[i] = element.replace('.. math::', math_label_preamble)
                            # Exit for-loop
                            break
        # Reassemble code
        function_code = '\n'.join(function_code_elements)

    # Remove redundant whitespace from code
    function_code_elements = function_code.split('\n')
    empty_element_indices = []
    for i, element in enumerate(function_code_elements):
        if not element:
            empty_element_indices.append(i)
        elif element.isspace():
            empty_element_indices.append(i)
            function_code_elements[i] = element.strip()
    if empty_element_indices:
        group_indices = []
        for index in reversed(empty_element_indices):
            if not group_indices:
                group_indices.append(index)
            elif group_indices[-1] - index == 1:
                group_indices.append(index)
            elif len(group_indices) > 1:
                group_indices.remove(min(group_indices))
                for group_index in group_indices:
                    del function_code_elements[group_index]
                group_indices = [index]
            else:
                group_indices = [index]
    # Reassemble code
    function_code = '\n'.join(function_code_elements)

    return function_code


def write_output(output, function_code):
    out = open(output, 'w')
    out.write(function_code)
    out.close()


def print_output(function_code):
    print(function_code)

\section{Overview of operator efficiency}\label{section:sparse.dense-ops}

\paragraph{Operator efficiency}

In this section you will find an overview of the efficiency of all unary, binary and
iterative operators in
AIMMS.

\paragraph{Unary operators and functions}

The unary operators and functions presented in
Table~\ref{table:sparseness.unary} are divided in two groups: sparse and
dense.
\begin{itemize}
    \item {\em sparse}: Here, when the argument is 0.0, the result is 0.0.  The result needs to be computed only for those
          tuples for which the argument has a non-zero value.
    \item {\em dense}: Here, when the argument is 0.0, the result is not equal to 0.0.  The results of
          all possible tuples need to be computed.
\end{itemize}
\begin{aimmstable}
\operindex{-} \operindex{NOT} \funcindex{Factorial} \funcindex{Sin}
\funcindex{Cos} \funcindex{Tan} \funcindex{Sinh} \funcindex{Cosh}
\funcindex{Tanh} \funcindex{ArcSin} \funcindex{ArcCos} \funcindex{ArcTan}
\funcindex{ArcSinh} \funcindex{ArcCosh} \funcindex{ArcTanh} \funcindex{Ceil}
\funcindex{Floor} \funcindex{Round} \funcindex{Trunc}
\begin{tabular}{|ll|ll|}
\hline\hline
         \multicolumn{2}{|c|}{\bf sparse}        & \multicolumn{2}{c|}{\bf dense}           \\
\hline
         \verb|-| & {\tt Sinh}     & {\tt NOT}        & {\tt Cos}                    \\
         {\tt Sin}      & {\tt Tanh}     & {\tt Cos}        & {\tt Cosh}                   \\
         {\tt Tan}      & {\tt ArcSin}   & {\tt Exp}        & {\tt ArcCos}                 \\
         {\tt Round}    & {\tt ArcTan}   & {\tt Log}        & {\tt ArcCosh}                \\
         {\tt Floor}    & {\tt ArcSinh}  & {\tt Log10}      & {\tt Factorial}              \\
         {\tt Ceil}     & {\tt ArcTanh}  &                  & \\
         {\tt Trunc}    & {\tt Sqr}      &                  & \\
         {\tt Sqrt}     &                &                  & \\
\hline\hline
\end{tabular}
\caption{Sparsen and dense unary operators and functions}
\label{table:sparseness.unary}
\end{aimmstable}

\paragraph{Binary operators}

The binary operators presented in Table~\ref{table:sparseness.binary} can be
divided in three groups:
\begin{itemize}
    \item {\em intersection sparse}: Here, when either of the arguments is 0.0, the result is 0.0.
          The result of only those tuples need to be computed where both arguments are not equal to 0.0.
          This corresponds to taking the intersection of the set of tuples for which the arguments are defined.
    \item {\em union sparse}: Here, when both arguments are 0.0, the result is 0.0.
          The result of only those tuples need to be computed where at least one of the arguments is not equal to 0.0.
          This corresponds to taking the union of the set of tuples for which the arguments are defined.
    \item {\em dense}: Here, when both arguments are 0.0, the result is not equal to 0.0.  In this case,
          the expression needs to be evaluated for all possible combinations of values of the indices, unless
          these combinations are limited by a sparse operator elsewhere in the same expression.
          This corresponds to taking the Cartesian product of the ranges of the indices.
\end{itemize}
%See also Chapter~\ref{chap:sparse} for a further explanation.
\begin{aimmstable}
\operindex{+} \operindex{-} \operindex{*} \operindex{/} \operindex{\char`\^}
\operindex{=} \operindex{<>} \operindex{<=} \operindex{<} \operindex{>}
\operindex{>=} \operindex{\$} \operindex{ONLYIF} \operindex{AND} \operindex{OR}
\operindex{XOR} \operindex{Permutation} \operindex{Combination}
\begin{tabular}{|l|l|l|}
\hline\hline
         {\bf intersection} & {\bf union} & {\bf dense}             \\
\hline
         \verb|*|           & \verb|+|    & \verb|^|                \\
         {\tt \$}           & \verb|-|    & \verb|/|                \\
         {\tt ONLYIF}       & {\tt <>}    & {\tt =}                 \\
         {\tt AND}          & {\tt <}     & {\tt <=}                \\
                            & {\tt >}     & {\tt >=}                \\
                            & {\tt OR}    & {\tt Permutation}       \\
                            & {\tt XOR}   & {\tt Combination}       \\
\hline\hline
\end{tabular}
\caption{Sparseness of binary operators} \label{table:sparseness.binary}
\end{aimmstable}

\paragraph{Iterative operators}

The iterative operators presented in Table~\ref{table:sparseness.iterative} are
divided in three groups as follows:
\begin{itemize}
    \item {\em sparse}        A value 0.0 of an argument does not influence the result and can safely be ignored.
                              The iterative operator only considers existing entries of its argument.
    \item {\em almost sparse} A second 0.0 in the argument does not influence the result.  The execution starts in a dense
                              meaning that the iterative operator considers all possible tuples.  However,  after a first
                              0.0 has been encountered, execution continues in a sparse manner.
    \item {\em dense}         A value 0.0 in the argument influences the result.  The iterative operator considers
                              all possible combinations.
\end{itemize}
\begin{aimmstable}
\iterindex{Count} \iterindex{Sum} \iterindex{Prod} \logiterindex{Exists}
\logiterindex{ForAll} \iterindex{Max} \iterindex{Min} \eliterindex{ArgMax}
\eliterindex{ArgMin} \statiterindex{Mean} \statiterindex{GeometricMean}
\statiterindex{HarmonicMean} \statiterindex{GeometricMean}
\statiterindex{RootMeanSquare} \statiterindex{Median}
\statiterindex{SampleDeviation} \statiterindex{PopulationDeviation}
\statiterindex{Skewness} \statiterindex{Kurtosis}
\begin{tabular}{|l|l|ll|}
\hline\hline
         {\bf sparse} & {\bf almost sparse} & \multicolumn{2}{c|}{\bf dense}                 \\
\hline
         {\tt Sum}          & {\tt Max}     & {\tt Mean}           & {\tt SampleDeviation}              \\
         {\tt Prod}         & {\tt Min}     & {\tt GeometricMean}  & {\tt PopulationDeviation}          \\
         {\tt Exists}       & {\tt ArgMax}  & {\tt HarmonicMean}   & {\tt Skewness}                     \\
         {\tt Forall}       & {\tt ArgMin}  & {\tt RootMeanSquare} & {\tt Kurtosis}                     \\
         {\tt Count}        &               & {\tt Median}         & {\tt RankCorrelation}              \\
\hline\hline
\end{tabular}
\caption{Sparseness of iterative operators} \label{table:sparseness.iterative}
\end{aimmstable}







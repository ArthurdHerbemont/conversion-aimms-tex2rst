\section{Modifying the sparsity}\label{sec:sparse.modify}
\label{sec:sparse.modifier.binary}

\paragraph{Questions}
Now that we've glanced at the execution engine's inner workings, you may be
wondering about the following questions.
\begin{itemize}
    \item Does sparse execution influence the results of a model?
    \item Does AIMMS have sparse versions of operators that are dense by nature?
\end{itemize}

\paragraph{Sparse execution is correct}

Sparse execution never changes the results of your model. AIMMS only
applies sparse intersection or sparse union when it is applicable. It does not
in any way influence the results of your model compared to simply considering
all the possible combinations of the running indices, but only the efficiency with which
these results are obtained.

\paragraph{Sparsity modifiers}

AIMMS does support sparse versions of some dense operators, but this time the sparse
versions will in general lead to different results. Adding {\tt \$}
characters to dense operators  modify these operators to sparse ones.
That is why we call the {\tt \$} characters added to these operators {\em
sparsity modifiers}.

\paragraph{Left and right operands}

Sparsity modifiers may be added to the left-hand side of a dense operator, to
the right-hand side, or to both. It causes the operator only to return a
non-zero result if the associated operand(s) are non-zero. Such a change to an
operator may, however, change its results in a way you may, or may not, want.

\paragraph{A first example: the  {\tt /\$} operator}

Let us now consider a few examples where such a modification is applicable.  The
first example of using a sparsity modifier is in the efficient guarding against
division by zero errors. Without the use of sparsity modifiers, we can
accomplish this as follows.
\begin{example}
        ! Leave A(i,j) zero when C(i,j)+D(i,j) is zero in order to
        ! avoid division by zero errors.
        ! This is accomplished by repeating the denominator in the condition.
        A(i,j) := ( B(i,j) / (C(i,j)+D(i,j)) ) $ (C(i,j)+D(i,j)) ;
\end{example}
In the example, we only divide by {\tt C(i)+D(i)} if this sum is non-zero.  Note
that this subexpression is actually computed twice.  AIMMS provides a
notational convenience in the form of {\tt \$} sparsity modifiers as follows.
\begin{example}
        ! Leave A(i,j) zero when C(i,j)+D(i,j) is zero in order to
        ! avoid division by zero errors.
        ! This is accomplished by using the /$ division operator
        ! which sparsely skips 0.0's.
        A(i,j) := B(i,j) /$ (C(i,j)+D(i,j)) ;
\end{example}
The {\tt /\$} operator is defined as the {\tt /} operator except when the right
hand side is 0.0.  In that case, the {\tt \$} sparsity modifier defines it as
0.0.  An added advantage is that the sub-expression {\tt C(i)+D(i)} is only
computed once.

\paragraph{The merge operator~{\tt :=\$}}

A second example is in the merging of new results in a set of existing
results. Without the use of a sparsity modifier you can accomplish this as
follows.
\begin{example}
        ! Only overwrite elements of E(i,j) when the result
        ! F(i,j) + G(i,j) is non-zero.
        ! This is accomplished by repeating the RHS of the
        ! assignment as a domain condition.
        E((i,j) | F(i,j)+G(i,j)) := F(i,j)+G(i,j) ;
\end{example}
Using the {\tt \$} sparsity modifier this can be equivalently obtained as
follows.
\begin{example}
        ! Only overwrite elements of E(i,j) when the result
        ! F(i,j) + G(i,j) is non-zero.
        ! This is accomplished by using the $ sparsity
        ! modifier on the assignment operator:
        E(i,j) :=$ F(i,j)+G(i,j) ;
\end{example}


\paragraph{Where allowed?}

Table~\ref{table:sparse.binary-sparsity} summarizes the operators to which the
{\tt \$} sparsity modifier can be applied, and whether it can be applied to the
left-hand side operand, to the right-hand side operand, or to both.

\begin{aimmstable}
\operindex{+}\operindex{-}\operindex{*}\operindex{/}\operindex{\char`\^}
\operindex{=}\operindex{<>}\operindex{<=}\operindex{<}\operindex{>}
\operindex{>=}\operindex{:=}\operindex{+=}\operindex{-=}\operindex{*=}
\operindex{/=}\operindex{\$}\operindex{ONLYIF}\operindex{AND}\operindex{OR}
\operindex{XOR}
\begin{tabular}{|c|c|c|}
\hline\hline {\bf Operator} &
        \multicolumn{2}{c|}{{\bf Sparsity modifier allowed}} \\
        \cline{2-3}
         & {\bf {\tt \$} left} & {\bf {\tt \$} right}   \\
\hline
        \verb|^|                & yes & yes   \\
        \verb|*|                & no  & no    \\
        \verb|/|                & no  & yes   \\
        \verb|+|, \verb|-|      & no  & no    \\
\hline
        {\tt =}, {\tt <>}, {\tt <},  & yes & yes   \\
        {\tt <=}, {\tt >}, {\tt >=}  & &   \\
\hline
        {\tt :=}                   & yes & yes   \\
        {\tt +=}, {\tt -=}         & yes & no    \\
        {\tt *=}, {\tt /=}, \verb|^=| & yes & yes   \\
\hline
    {\tt \$}, {\tt ONLYIF}     & no  & no   \\
        {\tt AND}, {\tt OR}, {\tt XOR} & &   \\
\hline\hline
\end{tabular}
\caption{Sparsity modifiers of binary
operators}\label{table:sparse.binary-sparsity}
\end{aimmstable}


\paragraph{Modifying iterative operators}

In addition to modifying the behavior of binary operators, the {\tt \$}
sparsity modifier can also be applied to iterative operators. The
effect in this case is that the iterative operator in the presence of a
{\tt \$} modifier will only be applied to tuples for which the expression
yields a non-zero value.

\paragraph{Example: the {\tt Min\$} operator}

The third and final example of the {\tt \$} sparsity modifier provided here is on the {\tt
Min} operator. Suppose you want to find the smallest non-zero distance between a
particular node and other nodes. This can be modeled as follows:
\begin{example}
        ! Find the smallest non-zero distance:
        MinimalDistance(i) := Min(j | Distance(i,j), Distance(i,j));
\end{example}
The 'non-zero' restriction is taken care of by repeating the argument of the
{\tt Min} operator in its domain condition.   By using the {\tt \$} sparsity
modifier we can shorten the above as follows:
\begin{example}
        ! Find the smallest non-zero distance:
        MinimalDistance(i) := Min$(j, Distance(i,j));
\end{example}

\paragraph{Where allowed?}

Table~\ref{table:sparse.iterative-sparsity} summarizes the iterative operators
to which the {\tt \$} sparsity modifier can be applied.

\begin{aimmstable}
\index{logical iterative operator} \index{iterative operator!logical}
\logiterindex{ForAll}\index{statistical iterative operator} \index{iterative
operator!statistical} \iterindex{Sum}\iterindex{Prod}\iterindex{Count}
\iterindex{Min}\iterindex{Max} \setiterindex{Sort}\setiterindex{Intersection}
\setiterindex{Union}\setiterindex{NBest}
\eliterindex{First}\eliterindex{Last}\eliterindex{Nth}
\eliterindex{ArgMin}\eliterindex{ArgMax}
\begin{tabular}{|c|c|}
\hline\hline {\bf Iterative operator} & \multicolumn{1}{c|}{{\bf Sparsity
modifier
allowed}} \\
\cline{2-2}
                                        & {\tt \$} added  \\
\hline
{\tt Sort}, {\tt NBest}                 & yes       \\
{\tt Intersection},                     & yes       \\
{\tt First}, {\tt Last}, {\tt Nth}      & no        \\
{\tt ArgMin}, {\tt ArgMax}              & yes       \\
{\tt Sum}, {\tt Union}                  & no        \\
{\tt Prod}                              & yes       \\
{\tt Min}, {\tt Max}                    & yes       \\
\hline
Statistical operators                   & yes       \\
(see also page~\pageref{table:expr.stat-iter}) & \\
\hline
{\tt ForAll}                            & no        \\
Other logical operators                 & no         \\
(see also page~\pageref{table:expr.logic-iter})   & \\
\hline\hline
\end{tabular}
\caption{Sparsity modifiers of iterative
operators}\label{table:sparse.iterative-sparsity}
\end{aimmstable}


\paragraph{Usage of sparsity modifiers}

To conclude, we can say that the {\tt \$} sparsity modifier is notationally a
convenience which you may or may not like. In the end it is up to you 
whether you use it or not. You decide this by weighing its advantage and
disadvantages. Our view on this is discussed briefly below.


\paragraph{Advantages}
Using sparsity modifiers has the following advantages.
    \begin{itemize}
        \item It enables a more compact notation.  In the examples above, the domain condition
              is replaced by a strategically placed {\tt \$} sparsity modifier thereby reducing
              the overall expression.  Many models have with multiple line
              subexpressions and with these the reduction is not insignificant.
        \item It is more efficient.  There are usually abundant zeros in a model. 
              You want them ignored so that the corresponding
              entries do not appear in the results.  In addition, you want them to
              be ignored as quickly as possible: so as not to waste any computation time on them.
    \end{itemize}

\paragraph{Disadvantages}

As with any new notation it takes time to get used to it.
This holds both for you as a modeler and also for the people
you want to communicate your model to.  In order to alleviate this disadvantage
you may want to add a few brief comments on the modified operators you use such as
``{\tt :=\$ operator used here to merge the result into the existing data}''.


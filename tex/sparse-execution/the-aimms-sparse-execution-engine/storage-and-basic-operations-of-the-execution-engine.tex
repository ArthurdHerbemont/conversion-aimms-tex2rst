\section{Storage and basic operations of the execution engine}\label{sec:sparse.basic}


In this section we present, in a step-by-step manner, the operations that,
when combined, build up the AIMMS sparse execution engine. The data storage
method with which these operations work is called an {\em ordered view}.

\paragraph{Ordered view}

The AIMMS execution engine stores the data according to the concept of an
ordered view.  An ordered view is an ordered, sparse collection of the
non-default elements of an identifier. The order is the lexicographical order
of the indices of that identifier.  Because of this order:
\begin{itemize}
    \item   the non-default elements of the identifier can be visited
            in a lexicographic order one at a time, and
    \item   a particular tuple can be found efficiently using values for the indices.
\end{itemize}

\paragraph{Running example}

The running example, used in this section and presented below, contains the two
parameters {\tt A(i,j)} and {\tt B(i,j)}, where {\tt i} and {\tt j} are indices
in a set {\tt S} containing the elements {\tt \{a1..a5\}}. The default values of
these parameters are 0.0, and they contain the following data:

\begin{example}
        A(i,j) := data table          B(i,j) := data table
             a1 a2 a3 a4 a5                a1 a2 a3 a4 a5
          !  -- -- -- -- --             !  -- -- -- -- --
          a1     2        5             a1     3        2
          a2  2     3  2                a2
          a3                            a3  5     1  2
          a4  4                         a4  4
          a5                  ;         a5                  ;
\end{example}

The ordered views of {\tt A} and {\tt B} are presented in the composite tables
below:

\begin{example}
        Composite table:              Composite table:
             i  j A                        i  j B
          ! -- -- -                     ! -- -- -
            a1 a2 2                       a1 a2 3
            a1 a5 5                       a1 a5 2
            a2 a1 2                       a3 a1 5
            a2 a3 3                       a3 a3 1
            a2 a4 2                       a3 a4 2
            a4 a1 4 ;                     a4 a1 4 ;
\end{example}

\paragraph{Like an index in relational databases}

There is nothing really new here; an ordered view corresponds to an relational
table in database terminology, with a (database) index on the primary keys~{\tt
i} and {\tt j}. A characteristic of both representations is that they can be
easily searched given explicit values for {\tt i} and {\tt j}.

\paragraph{Basic operations}

In the following sections, we will classify the algebraic operations in AIMMS
according to their behavior in the AIMMS sparse execution engine, and
discuss the effects of combining multiple operations or changing the natural
index order.

\subsection{The \tttext{+} operator: union behavior}\label{subsec:sparse.basic.plus}

\paragraph{First statement}
The first statement in the running example is the simple addition of the
matching elements resulting in parameter {\tt C(i,j)}:

\begin{example}
        C(i,j) := A(i,j) + B(i,j);
\end{example}

\paragraph{Merging rows}

As illustrated in Figure~\ref{fig:sparse.union}, this statement can be executed
in a sparse manner by merging the ordered views of {\tt A} and {\tt B} and
adding the values as one progresses.

%\guidefig{sparsity-union}
\begin{aimmsfigure}
\psset{xunit=0.8cm,yunit=0.4cm}
\begin{pspicture}(7,0)(10,12)
\rput[r](4,11){\tt ~i~~j~A}   \rput[l](8,11){\tt ~i~~j~B} \rput[r](4,10){\tt
--~--~-}   \rput[l](8,10){\tt --~--~-} \rput[r](4,9) {\tt a1~a2~2}
\rput[l](8,9) {\tt a1~a2~3} \rput[r](4,8) {\tt a1~a5~5}   \rput[l](8,8) {\tt
a1~a5~2} \rput[r](4,7) {\tt a2~a1~2} \rput[r](4,6) {\tt a2~a3~3} \rput[r](4,5)
{\tt a2~a4~2}
                              \rput[l](8,4) {\tt a3~a1~5}
                              \rput[l](8,3) {\tt a3~a3~1}
                              \rput[l](8,2) {\tt a3~a4~2}
\rput[r](4,1) {\tt a4~a1~4}   \rput[l](8,1) {\tt a4~a1~4}
\psline{<->}(4.5,9)(7.5,9) \psline{<->}(4.5,8)(7.5,8)
\psline{<->}(4.5,7)(7.5,7) \psline{<->}(4.5,6)(7.5,6)
\psline{<->}(4.5,5)(7.5,5) \psline{<->}(4.5,4)(7.5,4)
\psline{<->}(4.5,3)(7.5,3) \psline{<->}(4.5,2)(7.5,2)
\psline{<->}(4.5,1)(7.5,1)
\end{pspicture}
\caption{Sparse execution of the {\tt +} operator}\label{fig:sparse.union}
\end{aimmsfigure}


\paragraph{Union behavior}

In this figure, each arrow represents a computed result.  The behavior of the
{\tt +}~operator is referred to as \emph{sparse union} behavior: the union of rows
from {\tt A} and {\tt B} is taken to form the rows of {\tt C} and it is sparse
because we do not need to consider those tuples {\tt (i,j)} for which {\tt A(i,j)}
and {\tt B(i,j)} are both 0.0.

\paragraph{Similar operators}

Other operators, such as {\tt OR}, {\tt XOR}, {\tt <}, {\tt >} and {\tt <>}
have a similar behavior. They can also be implemented using the union of 
rows and performing the appropriate operation.


\subsection{The \tttext{*} operator: intersection behavior}\label{subsec:sparse.basic.mult}

\paragraph{Second statement}
The second statement in the running example is the simple multiplication of the
matching elements resulting in parameter {\tt D(i,j)}:

\begin{example}
        D(i,j) := A(i,j) * B(i,j);
\end{example}

\paragraph{Matching rows}

This statement can be executed in a sparse manner by intersecting the ordered
views of {\tt A} and {\tt B} and multiplying the corresponding values.
Intersection is sufficient because only for those tuples {\tt (i,j)} for which both
{\tt A(i,j)} and {\tt B(i,j)} are non-zero, will a non-zero be computed.  This is
illustrated in the Figure~\ref{fig:sparse.intersection}

%\guidefig{sparsity-intersection}
\begin{aimmsfigure}
\psset{xunit=0.8cm,yunit=0.4cm}
\begin{pspicture}(7,0)(10,12)
\rput[r](4,11){\tt ~i~~j~A}   \rput[l](8,11){\tt ~i~~j~B} \rput[r](4,10){\tt
--~--~-}   \rput[l](8,10){\tt --~--~-} \rput[r](4,9) {\tt a1~a2~2}
\rput[l](8,9) {\tt a1~a2~3} \rput[l](10,9){\scriptsize Match; store result}
\rput[r](4,8) {\tt a1~a5~5}   \rput[l](8,8) {\tt a1~a5~2}
\rput[l](10,8){\scriptsize Match; store result} \rput[r](4,7) {\tt a2~a1~2}
\rput[r](4,6) {\tt a2~a3~3} \rput[r](4,5) {\tt a2~a4~2}
                              \rput[l](8,4) {\tt a3~a1~5} \rput[lt](10,4.33){\parbox{4cm}{\scriptsize First a mismatch: search the left ordered view as represented by the dashed
                             arrow; thereafter search the right ordered view; followed by the final match}}
                              \rput[l](8,3) {\tt a3~a3~1}
                              \rput[l](8,2) {\tt a3~a4~2}
\rput[r](4,1) {\tt a4~a1~4}   \rput[l](8,1) {\tt a4~a1~4}
\psline{<->}(4.5,9)(7.5,9) \psline{<->}(4.5,8)(7.5,8)
\psline[linestyle=dashed]{<-}(4.5,1)(7.5,4) \psline{<->}(4.5,1)(7.5,1)
\end{pspicture}
\caption{Sparse execution of the {\tt *}
operator}\label{fig:sparse.intersection}
\end{aimmsfigure}



\paragraph{Intersection behavior}

Note that the ordered views of both {\tt A} and {\tt B} are searchable and,
thus, finding the matching elements can be efficiently implemented.  We call
this behavior \emph{sparse intersection} behavior. Because only matching rows
need to be considered, sparse intersection operators are much more efficient
than sparse union operators.

\paragraph{Similar operators}

Other operators, such as the {\tt AND} and {\tt \$} operators, exhibit similar
behavior. They can also be implemented using the intersection of the rows and
performing the appropriate operation.

\subsection{The \tttext{=} operator: dense behavior}\label{subsec:sparse.basic.equal}

\paragraph{Third statement}

The third statement in the running example checks whether corresponding
values are equal.

\begin{example}
        E(i,j) := (A(i,j) = B(i,j));
\end{example}

\paragraph{Comparing values}

This statement is admittedly somewhat artificial.  However, such conditions are
frequently part of larger expressions and must be considered.  The key
observation is that the comparison {\tt 0.0 = 0.0} evaluates to true. In
AIMMS the value 'true' is represented by the numerical value 1.0.
Therefore, the result of {\tt E(i,j)} is:

\paragraph{}
\begin{example}
        E(i,j) := data table
             a1 a2 a3 a4 a5
          !  -- -- -- -- --
          a1  1     1  1
          a2     1        1
          a3     1        1
          a4  1  1  1  1  1
          a5  1  1  1  1  1   ;
\end{example}

\paragraph{Dense behavior}

Given that the comparison of two zeros also results in a non-zero, all
possible combinations of {\tt (i,j)} have to be considered. Therefore, this
operation exhibits {\em dense} behavior, i.e.\ the operation cannot be
performed in a sparse manner. Dense operators have the worst possible
efficiency.

\paragraph{Similar operators}

Other operators, such as {\tt /}, {\tt **}, {\tt <=} and {\tt =>} demonstrate
similar behavior. They also need to be implemented by considering all the
possibilities and evaluating as one progresses.

\paragraph{Beware!}

Increasing the number of indices, or increasing the size of the sets will make
the number of rows to be considered in such operations grow rapidly.
Large-dimensional dense operations are a potential cause of performance glitches
in an application.


\subsection{Behavior of combined operations}\label{subsec:sparse.basic.combining}

\paragraph{Fourth statement}
The fourth statement is a variation of the third statement:

\begin{example}
        EP(i,j) := ( A(i,j) = B(i,j) ) $ A(i,j);
\end{example}

\paragraph{Speeding up}

Although the operation {\tt =} remains dense, the entire right hand side of the
assignment statement is limited to only those tuples {\tt (i,j)} for which {\tt
A(i,j)} is non-zero. This is known as a domain condition on the expression. The
net effect on the expression is that this condition  speeds up  efficient
behavior by moving from dense to sparse behavior. The result of this fourth assignment
is:

\begin{example}
        EP(i,j) := data table
             a1 a2 a3 a4 a5
          !  -- -- -- -- --
          a1
          a2
          a3
          a4  1
          a5                  ;
\end{example}

\paragraph{Preventing dense behavior}

If your model contains a statement that performs badly due to a dense
operation, using a domain condition can remedy the problem. Often, it is
possible to formulate a domain condition that does not alter the result of the
computation, but which does allow AIMMS to execute the statement in a sparse
manner.

\subsection{Summation}\label{subsec:sparse.basic.summation}


\paragraph{Fifth statement}

The fifth statement, as detailed below, is a step towards the sixth statement
and illustrates a language construct where sparse evaluation is
straightforward. This fifth statement is a simple aggregation of the parameter
{\tt A(i,j)} in a parameter {\tt AI(i)}:

\begin{example}
        AI(i) := Sum( j, A(i,j) );
\end{example}

This operation is illustrated in Figure~\ref{fig:sparse.aggr-i}.

%\guidefig{sparsity-agg-i}
\begin{aimmsfigure}
\psset{xunit=0.8cm,yunit=0.4cm}
\begin{pspicture}(7,0)(10,9)
\rput[r](4,8){\tt ~i~~j~A} \rput[r](4,7){\tt --~--~-} \rput[r](4,6){\tt
a1~a2~2} \rput[r](4,5){\tt a1~a5~5}
\rput[l](4.2,5.5){\scaleboxto(0.25,0.66){\}}} \rput[l](4.7,5.5){\tt 7}
\rput[r](4,4){\tt a2~a1~2} \rput[r](4,3){\tt a2~a3~3} \rput[r](4,2){\tt
a2~a4~2} \rput[l](4.2,3){\scaleboxto(0.25,1.00){\}}}   \rput[l](4.7,3){\tt 7}
\rput[r](4,1){\tt a4~a1~4} \rput[l](4.2,1){\scaleboxto(0.25,0.30){\}}}
\rput[l](4.7,1){\tt 4}
\end{pspicture}
\caption{Sparse execution of the {\tt Sum} operator}\label{fig:sparse.aggr-i}
\end{aimmsfigure}


\paragraph{Running indices and identifier indices match}

Each pairing represents a group of values corresponding to a particular value
of~{\tt i}.  As the elements in a group are adjacent in this ordered view, the
result of {\tt AI} can be computed in a single pass over the ordered view of
{\tt A}.  The order of the running indices in the statement is {\tt [i,j]}. The
first running index {\tt i} is already part of the left hand side of the
assignment, and {\tt j} is added to this list as part of the sum.

\paragraph{Single pass is sufficient}

Because the order of the running indices matches the order of the indices in
the identifier {\tt A(i,j)}, the results of the sum can be computed in a single
pass over the ordered view of {\tt A(i,j)}.

\subsection{Reordered views}\label{subsec:sparse.basic.reordered-views}

\paragraph{Sixth statement}
The sixth statement is a small variation to the fifth statement above. This
sixth statement is an aggregation of the parameter {\tt A} in a parameter
{\tt AJ(j)}:

\begin{example}
    AJ(j) := Sum( i, A(i,j) );
\end{example}

\paragraph{Non-matching index order}

This time, the elements that belong to the same group {\tt j} are not adjacent in the
ordered view of {\tt A} as the order of the indices in this statement is {\tt
[j,i]} which does not match the order of the indices in {\tt A(i,j)}.

\paragraph{Reordered views}

In order to regain adjacency of the elements in the same group, AIMMS
maintains other views of the parameter {\tt A} known as {\em reordered views}.  A
reordered view of an ordered view is a lexicographic order of the elements such
that the order of the indices in the identifier matches the order of the
running indices. A reordered view, and the grouping according to this view, are
illustrated in Figure~\ref{fig:sparse.aggr-j}.

%\guidefig{sparsity-agg-j}
\begin{aimmsfigure}
\psset{xunit=0.8cm,yunit=0.4cm}
\begin{pspicture}(7,0)(10,9)
\rput[r](4,8){\tt ~j~~i~A} \rput[r](4,7){\tt --~--~-} \rput[r](4,6){\tt
a1~a2~2} \rput[r](4,5){\tt a1~a4~4} \rput[r](4,4){\tt a2~a1~2}
\rput[r](4,3){\tt a3~a2~3} \rput[r](4,2){\tt a4~a2~2} \rput[r](4,1){\tt
a5~a1~5} \rput[l](4.5,5.5){\scaleboxto(0.25,0.60){\}}} \rput[l](5,5.5){\tt 6}
\rput[l](4.5,4){\scaleboxto(0.25,0.30){\}}}   \rput[l](5,4){\tt 2}
\rput[l](4.5,3){\scaleboxto(0.25,0.30){\}}}   \rput[l](5,3){\tt 3}
\rput[l](4.5,2){\scaleboxto(0.25,0.30){\}}}   \rput[l](5,2){\tt 2}
\rput[l](4.5,1){\scaleboxto(0.25,0.30){\}}}   \rput[l](5,1){\tt 4}
\end{pspicture}
\caption{Sparse execution of the reordered {\tt Sum}
operator}\label{fig:sparse.aggr-j}
\end{aimmsfigure}

\paragraph{Single pass is sufficient}

Again, each pairing represents a group of values corresponding to a particular
value of~{\tt j}. As the elements in a group are adjacent in this reordered
view, the results of {\tt AJ} can be computed by a single pass over this
reordered view of {\tt A}.  AIMMS generates and maintains reordered views on
an as needs basis. They do, however, take up memory.




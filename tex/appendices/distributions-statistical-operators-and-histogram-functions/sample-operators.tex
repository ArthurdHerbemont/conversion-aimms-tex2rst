\section{Sample operators}\label{app:sample.stat}
\index{statistical iterative operator} \index{iterative operator!statistical}


\paragraph{Sample operators}

The statistical sample operators discussed in this section can help you to
analyze the results of an experiment. The following operators are available in
AIMMS:
\begin{itemize}
\item the {\tt Mean} operator,
\item the {\tt GeometricMean} operator,
\item the {\tt HarmonicMean} operator,
\item the {\tt RootMeanSquare} operator,
\item the {\tt Median} operator,
\item the {\tt SampleDeviation} operator,
\item the {\tt PopulationDeviation} operator,
\item the {\tt Skewness} operator,
\item the {\tt Kurtosis} operator,
\item the {\tt Correlation} operator, and
\item the {\tt RankCorrelation} operator.
\end{itemize}

\paragraph{Associated units}

The results of the {\tt Skewness}, {\tt Kurtosis}, {\tt Correlation}  and {\tt
RankCorrelation} operator are unitless. The results of the other sample
operators listed above should have the same unit of measurement as the
expression on which the statistical computation is performed. Whenever your
model contains one or more {\tt QUANTITY} declarations, AIMMS will perform a
unit consistency check on arguments of the statistical operators and their
result.

\paragraph{Mean}
\statiterindex{Mean}\AIMMSlink{mean}
%\statiterindex{Average}\AIMMSlink{average}
\statiterindex{GeometricMean}\AIMMSlink{geometricmean}
\statiterindex{HarmonicMean}\AIMMSlink{harmonicmean}
\statiterindex{RootMeanSquare}\AIMMSlink{rootmeansquare}

The following mean computation methods are supported: (arithmetic) mean or
average, geometric mean, harmonic mean and root mean square (RMS). The first
method is well known and has the property that it is an unbiased estimate of
the expectation of a distribution. The geometric mean is defined as the $N$-th
root of the product of $N$ values. The harmonic mean is the reciprocal of the
arithmetic mean of the reciprocals. The root mean square is defined as the
square root of the arithmetic mean of the squares. It is mostly used for
averaging the measurements of a physical process.

\StatIter{0ex} {{\tt Mean}({\sf\em domain},{\sf\em expression}\/)} {\frac{1}{n}
\sum_{i=1}^{n} x_i}
%
\StatIter{0ex} {{\tt GeometricMean}({\sf\em domain},{\sf\em expression}\/)}
{\sqrt[{\displaystyle n}]{\prod_{i=1}^{n} x_i}}
%
\StatIter{0ex} {{\tt HarmonicMean}({\sf\em domain},{\sf\em expression}\/)}
{\frac{{\displaystyle n}}{{\displaystyle \sum_{i=1}^{n} \frac{1}{x_i}}}}
%
\StatIter{0ex} {{\tt RootMeanSquare}({\sf\em domain},{\sf\em expression}\/)}
{\sqrt{\frac{1}{n} \sum_{i=1}^{n} x_{i}^{2}}}

\paragraph{Median}
\statiterindex{Median}\AIMMSlink{median}

The median is the middle value of a sorted group of values. In case of an odd
number of values the median is equal to the middle value. If the number of
values is even, the median is the mean of the two middle values.

\StatIter{0ex} {{\tt Median}({\sf\em domain},{\sf\em expression}\/)}
{\Varr{median} =
 \left\{ \begin{array}{ll}
 x_{\frac{N + 1}{2}} & \; \; \mbox{if} \; N \; \mbox{is odd} \\
 \frac{1}{2} \left( x_{\frac{N}{2}} + x_{\frac{N + 2}{2}} \right)&
 \;\; \mbox{if} \; N \; \mbox{is even}
 \end{array} \right\}
}

\paragraph{Standard deviation}
\statiterindex{SampleDeviation}\AIMMSlink{sampledeviation}
\statiterindex{PopulationDeviation}\AIMMSlink{populationdeviation}

The standard deviation is a measure of dispersion about the mean. It is defined
as the root mean square of the distance of a set of values from the mean. There
are two kinds of standard deviation: the standard deviation of a sample of a
population, also known as $\sigma_{n-1}$ or $s$, and the standard deviation of
a population, which is denoted by $\sigma_n$. The relation between these two
standard deviations is that the first kind is an unbiased estimate of the
second kind. This implies that for large $n$ $\sigma_{n-1} \approx \sigma_n$.
The standard deviation of an sample of a population can be computed by means of

%
\StatIter{2ex} {{\tt SampleDeviation}({\sf\em domain},{\sf\em expression}\/)}
{\sqrt{ \frac{1}{n-1} \left( \sum_{i=1}^{n} x_{i}^{2} -
 \frac{1}{n} {\left( \sum_{i=1}^{n} x_i \right)}^2 \right)}}
%
whereas the standard deviation of a population can be determined by
%
\StatIter{0ex} {{\tt PopulationDeviation}({\sf\em domain},{\sf\em
expression}\/)} {\sqrt{ \frac{1}{n} \left( \sum_{i=1}^{n} x_{i}^{2} -
 \frac{1}{n} {\left( \sum_{i=1}^{n} x_i \right)}^2 \right)}}

\paragraph{Skewness}
\statiterindex{Skewness}\AIMMSlink{skewness}

The skewness is a measure of the symmetry of a distribution. Two kinds of
skewness are distinguished: positive and negative. A positive skewness means
that the tail of the distribution curve on the right side of the central
maximum is longer than the tail on the left side (skewed "to the right"). A
distribution is said to have a negative skewness if the tail on the left side
of the central maximum is longer than the tail on the right side (skewed "to
the left").
%
%\erase{
%The skewness is a measure of the symmetry of a distribution. A positive
%skewness means that the largest part of the distribution area lies to
%the left of the mean of the distribution (skewed "to the right").
%A negative skewness implies that the largest part of the distribution
%area lies to the right of the mean of the distribution
%(skewed "to the left").}
%
In general one can say that a skewness value greater than $1$ of less than $-1$
indicates a highly skewed distribution. Whenever the value is between $0.5$ and
$1$ or $-0.5$ and $-1$, the distribution is considered to be moderately skewed.
A value between $-0.5$ and $0.5$ indicates that the distribution is fairly
symmetrical.

\StatIter{2ex} {{\tt Skewness}({\sf\em domain},{\sf\em expression}\/)}
{\frac{{\displaystyle \sum_{i=1}^{n} (x_i - \mu)^3}}
      {{\displaystyle \sigma_{n-1}^3}}}
%
where $\mu$ denotes the mean and $\sigma_{n-1}$ denotes the standard deviation.

\paragraph{Kurtosis}
\statiterindex{Kurtosis}\AIMMSlink{kurtosis}

The kurtosis coefficient is a measure for the peakedness of a distribution. If
a distribution is fairly peaked, it will have a high kurtosis coefficient. On
the other hand, a low kurtosis coefficient indicates that a distribution has a
flat peak. It is common practice to use the kurtosis coefficient of the
standard Normal distribution, equal to 3, as a standard of reference.
Distributions which have a kurtosis coefficient less than 3 are considered to
be platykurtic (meaning flat), whereas distributions with a kurtosis
coefficient greater than 3 are leptokurtic (meaning peaked). Be aware that in
literature also an alternative definition of kurtosis is used in which 3 is
subtracted from the formula used here.

\StatIter{2ex} {{\tt Kurtosis}({\sf\em domain},{\sf\em expression}\/)}
{\frac{{\displaystyle \sum_{i=1}^{n} (x_i - \mu)^4}}
      {{\displaystyle \sigma_{n-1}^4}}}
%
where $\mu$ denotes the mean and $\sigma_{n-1}$ denotes the standard deviation.

\paragraph{Correlation coefficient}
\statiterindex{Correlation}\AIMMSlink{correlation}

The correlation coefficient is a measurement for the relationship between two
variables. Two variables are positive correlated with each other when the
correlation coefficient lies between 0 and 1. If the correlation coefficient
lies between $-1$ and 0, the variables are negative correlated. In case the
correlation coefficient is 0, the variables are considered to be unrelated to
one another.
%
Positive correlation means that if one variable increases, the other variable
increases also. Negative correlation means that if one variable increases, the
other variable decreases.

\StatIter{0ex} {{\tt Correlation}({\sf\em domain},{\sf\em x\_expression},
                   {\sf\em y\_expression}\/)}
{\frac{ { \displaystyle
          n \sum_{i=1}^{n} x_i y_i - \sum_{i=1}^{n} x_i
          \sum_{i=1}^{n} y_i } }
      { { \displaystyle
          \sqrt{
          \left( n \sum_{i=1}^{n} x_{i}^{2} -
          {\left( \sum_{i=1}^{n} x_i \right)}^2 \right)
          \left( n \sum_{i=1}^{n} y_{i}^{2} -
          {\left( \sum_{i=1}^{n} y_i \right)}^2 \right)} } } }

\paragraph{Rank correlation}
\statiterindex{RankCorrelation}\AIMMSlink{rankcorrelation}

If one wants to determine the relationship between two variables, but their
distributions are not equal or the precision of the data is not trusted, one
can use the rank correlation coefficient to determine their relationship. In
order to compute the rank correlation coefficient the data is ranked by their
value using the numbers $1,2,\ldots,n$. These rank numbers are used to compute
the rank correlation coefficient.

\StatIter{0ex} {{\tt RankCorrelation}({\sf\em domain},{\sf\em x\_expression},
                         {\sf\em y\_expression}\/)}
{1 - \frac{{\displaystyle 6
            \sum_{i=1}^{n} {\left( \mbox{Rank}(x_i) - \mbox{Rank}(y_i)
                           \right)}^2 }}
          {{\displaystyle n(n^2 - 1)}}}



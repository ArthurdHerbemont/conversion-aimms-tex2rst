\section{Distribution operators}\label{app:distribution.stat}

\paragraph{Distribution operators}

The distribution operators discussed in this section can help you to analyze
the results of an experiment. For example, it is expected that the sample mean
of a sequence of observations gets closer to the mean of the distribution that
was used during the observations as the number of observations increases. To
compute statistics over a sample, you can use the sample operators discussed in
Section~\ref{app:sample.stat} or you can use the histogram functions that are
explained in Section~\ref{LRsec:gui.histogram} of the Language Reference. The
following distribution operators are available in AIMMS:

\begin{itemize}
\item the {\tt DistributionCumulative({\sf\em distr,x}\/)} operator,
\item the {\tt DistributionInverseCumulative({\sf\em distr,$\alpha$})} operator,
\item the {\tt DistributionDensity({\sf\em distr,x}\/)} operator,
\item the {\tt DistributionInverseDensity({\sf\em distr,$\alpha$})} operator,
\item the {\tt DistributionMean({\sf\em distr})} operator,
\item the {\tt DistributionDeviation({\sf\em distr})} operator,
\item the {\tt DistributionVariance({\sf\em distr})} operator,
\item the {\tt DistributionSkewness({\sf\em distr})} operator, and
\item the {\tt DistributionKurtosis({\sf\em distr})} operator.
\end{itemize}

\paragraph{Cumulative distributions $\ldots$}

{\tt DistributionCumulative({\sf\em distr,x})} computes the probability that a
random variable $X$ drawn from the distribution {\sf\em distr} is less or equal
than $x$. Its inverse, {\tt DistributionInverseCumulative({\sf\em
distr},$\alpha$)}, computes the smallest $x$ such that the probability that a
variable $X$ is greater than or equal to $x$ does not exceed $\alpha$.

\paragraph{$\ldots$ and their derivatives}

The {\tt Distribution\-Density({\sf\em distr,x}\/)} expresses the expected
density around $x$ of sample points drawn from a {\sf\em distr} distribution
and is in fact the derivative of {\tt Distribution\-Cumulative({\sf\em
distr,x}\/)}. The {\tt Distribution\-Inverse\-Density({\sf\em distr},$\alpha$)}
is the derivative of {\tt Distribution\-Inverse\-Cumulative({\sf\em
distr},$\alpha$)}. Given a random variable $X$, the {\tt
Distribution\-Inverse\-Density} can be used to answer the question of how much
a given value $x$ should be increased such that the probability $P(X \leq x)$
is increased with $\alpha$ (for small values of $\alpha$).
%This can be expressed as follows
%\begin{eqnarray*}
%d x & = & \frac{\partial x}{\partial \alpha} \cdot d \alpha \\
%    & = & \biggl( \bigl( \; \text{\tt DistributionInverseCumulative}(\text{\sf\em distr},\alpha + d \alpha ) \\[-.8em]
%    &   & \quad -\text{\tt DistributionInverseCumulative}(\text{\sf\em distr},\alpha) \bigr) \big/
%           {\partial \alpha} \; \biggr) \cdot d \alpha \\
%    & = & \text{\tt DistributionInverseDensity}(\text{\sf\em distr},\alpha) \cdot d \alpha
%\end{eqnarray*}

\paragraph{$\ldots$ for discrete distributions}

For continuous distributions {\sf\em distr}, $\alpha \in [0,1]$, and $x = {\tt DistributionInverseCumulative}(distr,\alpha)$, it holds that
\begin{eqnarray*}
    {\tt DistributionDensity}(distr,x) & = \partial \alpha / \partial x \\
    {\tt DistributionInverseDensity}(distr,\alpha) & = \partial x / \partial \alpha
\end{eqnarray*}
Note that the above two relations make it possible to express {\tt
Distribution\-Inverse\-Density} in terms of {\tt Distribution\-Density}.
Through this relation the {\tt Distribution\-Inverse\-Density} is also defined
for discrete distributions.

%\begin{multline*}
%\text{{\tt DistributionInverseDensity({\sf\em distr},$\alpha$)}} = \\
%1/\text{\tt DistributionDensity({\sf\em distr},DistributionInverseCumulative({\sf\em distr},$\alpha$))}
%\end{multline*}

%{\tt DistributionInverseDensity(distr,$\alpha$)} = 1 / {\tt DistributionDensity(distr, InverseCumulativeDistribution(distr,$\alpha$))}

\paragraph{Distribution statistics}

The operators {\tt DistributionMean}, {\tt DistributionDeviation}, {\tt
DistributionVariance}, {\tt DistributionSkewness} and {\tt
DistributionKurtosis} provide the mean, standard deviation, variance, skewness
and kurtosis of a given distribution. Note that the values computed using the
sample operators converges to the values computed using the corresponding
distribution operators as the size of the sample increases (the law of large
numbers).


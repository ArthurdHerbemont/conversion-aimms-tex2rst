\section{Continuous distributions}\label{app:distribution.cont}

\paragraph{Continuous distributions}

In this section we discuss the set of continuous distributions available in
AIMMS.

\paragraph{}

The three distributions with both lower and upper bound are
\begin{itemize}
\item the {\tt Uniform} distribution,
\item the {\tt Triangular} distribution, and
\item the {\tt Beta} distribution.
\end{itemize}
The five distributions with only a lower bound are
\begin{itemize}
\item the {\tt LogNormal} distribution,
\item the {\tt Exponential} distribution,
\item the {\tt Gamma} distribution,
\item the {\tt Weibull} distribution, and
\item the {\tt Pareto} distribution.
\end{itemize}
The three unbounded distributions are
\begin{itemize}
\item the {\tt Normal} distribution,
\item the {\tt Logistic} distribution, and
\item the {\tt Extreme Value} distribution.
\end{itemize}

\paragraph{Parameters of continuous distributions}
\index{distribution!location parameter} \index{distribution!scale parameter}
\index{distribution!shape parameter}

Every parameter of a continuous distributions can be characterized as either a
{\em shape} parameter $\beta$, a {\em location} parameter $l$, or a {\em scale}
parameter $s$. While the presence and meaning of a shape parameter is usually
distribution-dependent, location and scale parameters find their origin in the
common transformation

\begin{displaymath}
    x \mapsto \frac{x-l}{s}
\end{displaymath}

to shift and stretch a given distribution. By choosing $l=0$ and $s=1$ the
standard form of a distribution is obtained. If a certain distribution has $n$
shape parameters ($n \geq 0$), these shape parameters will be passed as the
first $n$ parameters to AIMMS. The shape parameters are then followed by two
optional parameters, with default values 0 and 1 respectively. For
double-bounded distributions these two optional parameters can be interpreted
as a lower and upper bound (the value of the location parameter $l$ for these
distributions is equal to the lower bound and the value of the scale parameter
$s$ is equal to the difference between the upper and lower bound). For
single-bounded distributions the bound value is often used as the location
parameter $l$. In this section, whenever the location parameter can be
interpreted as a mean value or whenever the scale parameter can be interpreted
as the deviation of a distribution, these more meaningful names are used to
refer to the parameters. Note that the {\tt LogNormal}, {\tt Gamma} and {\tt
Exponential} distributions are distributions that will mostly be used with
location parameter equal to 0.

\paragraph{Transformation to standard form}

When transforming a distribution to standard form, distribution operators
change. Section~\ref{app:scaleoperator.stat} (scaling of statistical operators)
gives the relationships between distribution operators working on random
variables $X(l,s)$ and $X(0,1)$.
%For the distribution density function $f(x;l,s)$,
% the cumulative distribution function $F(x;l,s)$,
% the inverse cumulative distribution function $F^{-1}(\alpha;l,s)$,
% a random variable $X(l,s)$,
% its mean $\mu(X(l,s))$ and
% variance $\sigma^2(X(l,s))$ one can deduce the relationships

\paragraph{Units of measurement}

When a random variable representing some real-life quantity with a given unit
of measurement (see also Chapter~\ref{chap:units}) is distributed according to
a particular distribution, some parameters of that distribution are also
naturally expressed in terms of this same unit while other parameters are
expected to be unitless. In particular, the location and scale parameters of a
distribution are measured in the same unit of measurement as the corresponding
random variable, while shape parameters (within AIMMS) are implemented as
unitless parameters.

\paragraph{Unit notation in this appendix}

When you use a distribution function, AIMMS will perform a unit consistency
check on its parameters and result, whenever your model contains one or more
{\tt QUANTITY} declarations. In the description of the continuous distributions
below, the expected units of the distribution parameters are denoted in square
brackets. Throughout the sequel, [$x$] denotes that the parameter should have
the same unit of measurement as the random variable $X$ and [--] denotes that a
parameter should be unitless.

\paragraph{A commonly used distribution}

In practice, the {\tt Normal} distribution is used quite frequently. Such
widespread use is due to a number of pleasant properties:

\begin{itemize}
\item the {\tt Normal} distribution has no shape parameters and is symmetrical,
\item random values are more likely as they are closer to the mean value,
\item it can be directly evaluated for any given mean and standard deviation
      because it is fully specified through the mean and standard
      deviation parameter,
\item it can be used as a good approximation for distributions on a finite
      interval, because its probability density is declining fast enough (when moving away from
      the mean),
\item the mean and sum of any number of uncorrelated {\tt Normal} distributions
are
      {\tt Normal} distributed themselves, and thus have the same shape, and
\item the mean and sum of a large number of uncorrelated distributions are
      always approximately {\tt Normal} distributed.
\end{itemize}

\paragraph{Distributions for double bounded variables}

For random variables that have a known lower and upper bound, AIMMS provides
three continuous distributions on a finite interval: the {\tt Uniform}, {\tt
Triang\-ular} and {\tt Beta} distribution. The {\tt Uniform} (no shape
parameters) and {\tt Triangular} (one shape parameter) distributions should be
sufficient for most experiments. For all remaining experiments, the user might
consider the highly configurable {\tt Beta} (two shape parameters)
distribution.

\paragraph{Distributions for single bounded variables}

When your random variable only has a single bound, you should first check
whether the {\tt Gamma} distribution can be used or whether the {\tt Normal}
distribution is accurate enough. The {\tt LogNormal} distribution should be
considered if the most likely value is near but not at the bound. The {\tt
Weibull} or {\tt Gamma} distribution ($\beta>1$), or even the {\tt
ExtremeValue} distribution are alternatives, while the {\tt Weibull} or {\tt
Gamma} distribution ($\beta \leq 1$) or {\tt Pareto} distribution should be
considered if the bound is the most likely value.

\paragraph{The {\tt Gamma} distribution}

The {\tt Gamma} (and as a special case thereof the {\tt Exponential})
distribution is widely used for its special meaning. It answers the question:
how long does it take for a success to occur, when you only know the average
number of occurrences (like in the {\tt Poisson} distribution). The {\tt
Exponential} distribution gives the time to the first occurrence, and its
generalization, the {\tt Gamma}($\beta$) distribution gives the time to the
$\beta$-th occurrence. Note that the sum of a {\tt Gamma}($\beta_1,l_1,s$) and
{\tt Gamma}($\beta_2,l_2,s$) distribution has a {\tt
Gamma}($\beta_1+\beta_2,l_1+l_2,s$) distribution.

\paragraph{The {\tt LogNormal} distribution}

If you assume the logarithm of a variable to be {\tt Normal} distributed, the
variable itself is {\tt LogNormal}-distributed. As a result, it can be shown
that the chance of an outcome in the interval $[x \!\cdot\! c_1,x \!\cdot\!
c_2]$ is equal to the chance of an outcome in the interval $[x/c_2,x/c_1]$ for
some $x$. This might be a reasonable assumption in price developments, for
example.


% \\The {\tt Weibull}, {\tt
%Logistic}, {\tt Pareto} and {\tt Extreme Value} distribution are also available
%in AIMMS. They are found useful in special cases.
%
%distribution, derived from the {\tt Normal} distribution. If you assume the
%logarithm of a variable to be {\tt Normal} distributed, the
%variable itself is {\tt LogNormal}-distributed. For {\tt
%LogNormal} distributed variables, the chance of halving from some value
%is equal to the chance of doubling. The {\tt Weibull} or {\tt Gamma}
%distribution ($\beta>1$) ,or even the {\tt ExtremeValue} distribution are
%alternatives, while one should consider the {\tt Weibull} or {\tt Gamma}
%distribution ($\beta<=1$) or {\tt Pareto} distribution if the bound is the most
%likely value.
%
%
%\paragraph{Other continuous distributions}
%

\paragraph{The
{\tt Uniform} distribution \\
%
\psset{xunit=0.33cm,yunit=0.25cm}
\begin{pspicture}(-3,0)(3,6)
    \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \psline(-3,0)(-2,0)(-2,4)(2,4)(2,0)(3,0)}
%      \psline(-3,0)(-2,0
%      \psline(-2,0)(-2,4)
%      \psplot[plotpoints=100,plotstyle=curve]{-2.0}{2.0}{4}
%      \psline(2,4)(2,0)
%      \psline(2,0)(3,0)}
    \psline{-}(-3,0)(3,0)
\end{pspicture}
} \dfuncindex{Uniform} \AIMMSlink{uniform}

The {\tt Uniform}(\Varr{min},\Varr{max}) distribution: \DistributionExt {{\it
min} [$x$], {\it max} [$x$]} {\Varr{min} < \Varr{max}} {\{ x \Cond \Varr{min}
\leq x \leq \Varr{max} \}} {Standard density}{f_{(0,1)}(x) = 1} {1/2} {1/12}

In the {\tt Uniform} distribution all values of the random variable occur
between a fixed minimum and a fixed maximum with equal likelihood. It is quite
common to use the {\tt Uniform} distribution when you have little knowledge
about an uncertain parameter in your model except that its value has to lie
anywhere within fixed bounds. For instance, after talking to a few appraisers
you might conclude that their single appraisals of your property vary anywhere
between a fixed pessimistic and a fixed optimistic value.

\paragraph{The
{\tt Triangular} distribution \\
%
\psset{xunit=0.33cm,yunit=0.25cm}
\begin{pspicture}(0,0)(6,8)
    \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \psplot[plotpoints=100,plotstyle=curve]{1.0}{4.0}{ x 1 sub 6 div 12 mul}
      \psplot[plotpoints=100,plotstyle=curve]{4.0}{5.0}{ 5 x sub 2 div 12 mul}}
    \psline{-}(0,0)(6,0)
\end{pspicture}
} \dfuncindex{Triangular} \AIMMSlink{triangular}

The {\tt Triangular}($\beta$,\Varr{min},\Varr{max}) distribution:
\DistributionRem { shape $\beta$ [$-$],{\it min} [$x$], {\it max} [$x$]}
{\Varr{min} <  \Varr{max }, \; 0 < \beta < 1} {\{ x \Cond \Varr{min} \leq x
\leq \Varr{max} \} } {Standard density} {f_{(\beta,0,1)}(x) = \begin{cases}
   2 x    /   \beta  & \text{for $0 \leq x \leq \beta$}  \\
   2 (1-x)/(1-\beta) & \text{for $\beta<x \leq1$}
\end{cases}}
{(\beta+1)/3} {(1-\beta+\beta^2)/18 } {The shape parameter $\beta$ indicates
the position of the peak in relation to the range, i.e.\
 $\beta = \frac{\Varr{peak}-\Varr{min}}{\Varr{max}-\Varr{min}}$}

% \\
%  \mbox{where the shape } \beta = \frac{\Varr{Peak}-\Varr{Mi}}{\Varr{Ma}-\Varr{Mi}} \in [0,1]}

In the {\tt Triangular} distribution all values of the random variable occur
between a fixed minimum and a fixed maximum, but not with equal likelihood as
in the Uniform distribution. Instead, there is a most likely value, and its
position is not necessarily in the middle of the interval. It is quite common
to use the {\tt Triangular} distribution when you have little knowledge about
an uncertain parameter in your model except that its value has to lie anywhere
within fixed bounds and that there is a most likely value. For instance, assume
that a few appraisers each quote an optimistic as well as a pessimistic value
of your property. Summarizing their input you might conclude that their quotes
provide not only a well-defined interval but also an indication of the most
likely value of your property.

%\newpage

\paragraph[0.8]{The
{\tt Beta} distribution\\
%
\psset{xunit=2cm,yunit=1cm}
\begin{pspicture}(-0.1,0)(1,2.0)
   \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \psplot[plotpoints=100,plotstyle=curve]{0.0}{1.0}%
       {x 4 exp 1 x sub mul 21 mul}
   }
   \psline{-}(-0.1,0)(1.1,0)
\rput(0.6,-0.3){\scriptsize {\tt Beta}(4,2)}
\end{pspicture}
\begin{pspicture}(-0.1,0)(1,2.0)
   \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \psline{-}(0.01,0)(0.01,1.7)
      \psplot[plotpoints=100,plotstyle=curve]{0.01}{1.0}%
       {1 x sub 0.5 exp x -0.1 exp mul 0.9 mul}
   }
   \psline{-}(-0.1,0)(1.1,0)
\rput(0.5,-0.3){\scriptsize {\tt Beta}(0.9,1.5)}
\end{pspicture}
} \dfuncindex{Beta} \AIMMSlink{beta}

The {\tt Beta}($\alpha$,$\beta$,\Varr{min},\Varr{max}) distribution:
\DistributionRem {shape $\alpha$ [--], shape $\beta$ [--], \Varr{min} [$x$],
\Varr{max} [$x$]} {\alpha > 0, \beta > 0, \Varr{min} < \Varr{max} } {\{x \Cond
\Varr{min} < x < \Varr{max} \}} {Standard density}{ f_{(\alpha,\beta,0,1)}(x)
= \frac{1}{B(\alpha,\beta)} x^{\alpha - 1} (1-x)^{\beta - 1}, \; \text{where $B(\alpha,\beta)$ is the Beta function}
} { \alpha/(\alpha+\beta)} {
\alpha\beta(\alpha+\beta)^{-2}(\alpha+\beta+1)^{-1} } {${\tt
Beta}(1,1,\Varr{min},\Varr{max})={\tt Uniform}(\Varr{min},\Varr{max})$}

The {\tt Beta} distribution is a very flexible distribution whose two shape
parameters allow for a good approximation of almost any distribution on a
finite interval. The distribution can be made symmetrical, positively skewed,
negatively skewed, etc. It has been used to describe empirical data and predict
the random behavior of percentages and fractions. Note that for $\alpha<1$ a
singularity occurs at $x=\text{\Varr{min}}$ and for $\beta<1$ at
$x=\text{\Varr{max}}$.


\paragraph{The
{\tt LogNormal} distribution\\
%
\psset{xunit=0.2cm,yunit=5.26cm}
\begin{pspicture}(0,0)(10,0.38)
   \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \psplot[plotpoints=200,plotstyle=curve]{0.2}{10.0}%
       {2.71828 x ln 1 sub 2 exp neg 2 mul exp 1.25331 x mul div}
   }
   \psline{-}(0,0)(10,0)
\end{pspicture}
} \dfuncindex{LogNormal} \AIMMSlink{lognormal}

%Whenever an uncertain variable must be nonnegative and most of its
%values lie near the minimum value, you should consider the {\tt
%Lognormal} distribution  as its distribution.
%The assumption made when  using this distribution (with location parameter valued zero) is
%that the chance of halving from an expected value $\mu$ is equal to the chance of doubling.
%In that case, the logarithm of the variable is symmetrical around its mean.\\
The {\tt LogNormal}($\beta$,\Varr{min},\Varr{s}) distribution:
\DistributionExt {shape $\beta$ [--], lowerbound \Varr{min} [$x$] and scale $s$
[$x$]} {\beta > 0 \; \mbox{and} \; s > 0} {\{ x \Cond \Varr{min} < x < \PLUSINF
\}} {Standard density} {f_{(\beta,0,1)}(x) = \frac{1}
                               { \sqrt{2 \pi} x \ln(\beta^2+1) }
                           e^{ \frac{ -(\ln(x^2(\beta^2+1)) }
                                    {2 \ln(\beta^2+1) } }}
{1} {\beta^2}

If you assume the logarithm of the variable to be {\tt
Normal}($\mu,\sigma$)-distributed, then the variable itself is {\tt
LogNormal}($\sqrt{e^{\sigma^2}\!\! - \!\! 1},0,e^{\mu - \sigma^2/2}
$)-distributed. This parameterization is used for its simple expressions for
mean and variance. A typical example is formed by real estate prices and stock
prices. They all cannot drop below zero, but they can grow to be very high.
However, most values tend to stay within a particular range. You usually can
form some expected value of a real estate price or a stock price, and estimate
the standard deviation of the prices on the basis of historical data.

\paragraph{The
{\tt Exponential} distribution\\
%
\psset{xunit=0.33cm,yunit=0.25cm}
\begin{pspicture}(-0.5,0)(5.5,8)
    \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \moveto(0,0)
      \psline(0,7)
      \psplot[plotpoints=100,plotstyle=curve]{0.0}{5.5}%
        {2.71828 x neg exp 7 mul}}
    \psline(-0.5,0)(5.5,0)
\end{pspicture}
} \dfuncindex{Exponential} \AIMMSlink{exponential}


The {\tt Exponential}(\Varr{min},$s$) distribution: \DistributionRem
{lowerbound \Varr{min} [$x$] and scale $s$ [$x$]} {s > 0} {\{ x \Cond
\Varr{min} \leq x < \PLUSINF \}} {Standard density}{f_{(0,1)}(x) = \lambda
e^{-x}} {1} {1}
{ {\tt Exponential} (\Varr{min}, $s$) = {\tt Gamma} (1, \Varr{min}, $s$)\\
  {\tt Exponential} (\Varr{min}, $s$) = {\tt Weibull} (1, \Varr{min}, $s$)}

Assume that you are observing a sequence of independent events with a constant
chance of occurring in time, with s being the average time between occurrences.
(in accordance with the {\tt Poisson} distribution)  The {\tt
Exponential}($0,s$) distribution gives answer to the question: how long a time
do you need to wait until you observe the first occurrence of an event. Typical
examples are time between failures of equipment, and time between arrivals of
customers at a service desk (bank, hospital, etc.).

\paragraph{The
{\tt Gamma} distribution\\
%
\psset{xunit=0.133cm,yunit=10cm}
\begin{pspicture}(0,0)(15,0.2)
   \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \psplot[plotpoints=200,plotstyle=curve]{0.0}{15.0}%
       {x 4 exp 2.71828 x neg exp mul 26.0 div}
   }
   \psline{-}(0,0)(15,0)
\end{pspicture}
} \dfuncindex{Gamma} \AIMMSlink{gamma}

The {\tt Gamma}($\beta$,\Varr{min},$s$) distribution: \DistributionExt {shape
$\beta$ [--], lowerbound \Varr{min} [$x$] and scale $s$ [$x$]} {s > 0 \;
\mbox{and} \; \beta > 0} {\{x \Cond \Varr{min} < x < \PLUSINF\}}
{Standard density}{ f_{(\beta,0,1)}(x) = x^{\beta - 1}  e^{-x} / {\Gamma ( \beta )} \\
  \mbox{where} \; \Gamma ( \beta ) \; \mbox{is the Gamma function}
} {\beta} {\beta} The {\tt Gamma} distribution gives answer to the question:
how long a time do you need to wait until you observe the $\beta$-th occurrence
of an event (instead of the first occurrence as in the {\tt Exponential}
distribution). Note that it is possible to use non-integer values for $\beta$
and a location parameter. In these cases there is no natural interpretation of
the distribution and for $\beta<1$ a singularity exists at $x=\Varr{min}$, so
one should be very careful in using the {\tt Gamma} distribution this way.

\paragraph{The
{\tt Weibull} distribution\\
%
\psset{xunit=0.57cm,yunit=0.25cm}
\begin{pspicture}(-0.5,0)(3.0,8)
    \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \psplot[plotpoints=100,plotstyle=curve]{0.0}{3.0}%
       {2.71828 x x neg mul exp x mul 13 mul}}
    \psline(-0.3,0)(3.0,0)
\end{pspicture}
} \dfuncindex{Weibull} \AIMMSlink{weibull}


The {\tt Weibull}($\beta$,\Varr{min},$s$) distribution: \DistributionExt {shape
$\beta$ [--], lowerbound \Varr{min} [$x$] and scale $s$ [$x$] } {\beta > 0 \;
\mbox{and} \; s > 0} {\{x \Cond \Varr{min} \leq x < \PLUSINF\}} {Standard
density}{f_{(\beta,0,1)}(x) = \beta x^{\beta - 1} e^{-x^\beta} } {
\Gamma(1+1/\beta)} { \Gamma(1+2/\beta)-\Gamma^2(1+1/\beta)} The {\tt Weibull}
distribution is another generalization of the {\tt Exponential} distribution.
It has been successfully used to describe failure time in reliability studies,
and the breaking strengths of items in quality control testing. By using a
value of the shape parameter that is less than 1, the {\tt Weibull}
distribution becomes steeply declining and could be of interest to a
manufacturer testing failures of items during their initial period of use. Note
that in that case there is a singularity at $x=\Varr{min}$.

\paragraph{The
{\tt Pareto} distribution\\
%
\psset{xunit=0.25cm,yunit=0.25cm}
\begin{pspicture}(0.0,0)(9.0,8)
    \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \moveto(1,0)
      \psline(1,7)
      \psplot[plotpoints=200,plotstyle=curve]{1.0}{9.0}%
       {1 x x mul div 7 mul}}
    \psline(0.0,0)(9.0,0)
\end{pspicture}
} \dfuncindex{Pareto} \AIMMSlink{pareto}

The {\tt Pareto}($\beta$,$l$,$s$) distribution: \DistributionExt {shape $\beta$
[--], location $l$ [$x$] and scale $s$ [$x$]} {s > 0 \; \mbox{and} \; \beta >
0} {\{ x \Cond l+s < x < \PLUSINF \}} {Standard density}{f_{(\beta,0,1)}(x) =
\beta / x^{\beta + 1}} {\mbox{for } \beta>1:\; \beta/(\beta-1), \PLUSINF \text{
otherwise}} {\mbox{for } \beta>2:\; \beta(\beta-1)^{-2}(\beta-2)^{-1}, \PLUSINF
\text{ otherwise}} The {\tt Pareto} distribution has been used to describe the
sizes of such phenomena as human population, companies, incomes, stock
fluctuations, etc.
%Note that mean and variance become infinite for small $\beta$.

\paragraph{The
{\tt Normal} distribution \\
%
\psset{xunit=0.2cm,yunit=1.8cm}
\begin{pspicture}(-5,0)(5,1.1)
    \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \psplot[plotpoints=100,plotstyle=curve]{-5.0}{5.0}{2.71728 x x neg mul 4 div exp}}
    \psline{-}(-5,0)(5,0)
\end{pspicture}
} \dfuncindex{Normal} \AIMMSlink{normal}

%In the {\tt Normal} distribution the mean value of the random variable
%is the most likely. In addition, the random variable is more likely to
%be near the mean value than far away, and its value could as likely be
%above the mean as below.
The {\tt Normal}($\mu$,$\sigma$) distribution: \DistributionRem {Mean $\mu$
[$x$] and standard deviation $\sigma$ [$x$]} {\sigma > 0} {\{ x \Cond \MININF <
x < \PLUSINF \}} {Standard density}{f_{(0,1)}(x) = e^{-x^2/2}/\sqrt{2 \pi}} {0}
{1} {Location $\mu$, scale $\sigma$}

The {\tt Normal} distribution is frequently used in practical applications as
it describes many phenomena observed in real life. Typical examples are
attributes such as length, IQ, etc. Note that while the values in these
examples are naturally bounded, a close fit between such data values and
normally distributed values is quite common in practice, because the likelihood
of extreme values away from the mean is essentially zero in the {\tt Normal}
distribution.


\paragraph{The
{\tt Logistic} distribution\\
%
\psset{xunit=0.125cm,yunit=0.25cm}
\begin{pspicture}(-8.0,0)(8.0,8)
    \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \psplot[plotpoints=100,plotstyle=curve]{-8.0}{8.0}%
       {2.71828 x neg exp 2.71828 x neg exp 1 add 2 exp div 28 mul}}
    \psline(-8.0,0)(8.0,0)
\end{pspicture}
} \dfuncindex{Logistic} \AIMMSlink{logistic}

The {\tt Logistic}($\mu$,$s$) distribution: \DistributionExt {mean $\mu$ [$x$]
and scale $s$ [$x$]} {s > 0} {\{x \Cond \MININF < x < \PLUSINF \}} {Standard
density}{f_{(0,1)}(x) = ( e^x + e^{-x} + 2 )^{-1} } {0} {\pi^2/3} The {\tt
Logistic} distribution has been used to describe growth of a population over
time, chemical reactions, and similar processes. Extreme values are more common
than in the somewhat similar {\tt Normal} distribution

%\newpage

\paragraph{The
{\tt Extreme Value} distribution\\
%
\psset{xunit=0.2cm,yunit=0.25cm}
\begin{pspicture}(-5.0,0)(5.0,8)
    \pscustom[fillstyle=solid,fillcolor=lightgray]{
      \psplot[plotpoints=200,plotstyle=curve]{-5.0}{5.0}%
       {2.71828 x 1 sub exp 2.71828 2.71828 x 1 sub exp neg exp mul 19 mul}}
    \psline(-5.0,0)(5.0,0)
\end{pspicture}
} \dfuncindex{ExtremeValue} \AIMMSlink{extremevalue}

The {\tt Extreme Value}($l$,$s$) distribution: \DistributionExt {Location $l$
[$x$] and scale $s$ [$x$]} {s > 0} {\{ x \Cond \MININF < x < \PLUSINF \}}
{Standard density}{f_{(0,1)}(x) = e^x e^{-e^x} } {\gamma=0.5772\dots\mbox{
(Euler's constant)}} {\pi^2/6} {\tt Extreme Value} distributions have been used
to describe the largest values of phenomena observed over time: water levels,
rainfall, etc. Other applications include material strength, construction
design or any other application in which extreme values are of interest. In
literature the {\tt Extreme Value} distribution that is provided by AIMMS is
known as a type 1 Gumbel distribution.


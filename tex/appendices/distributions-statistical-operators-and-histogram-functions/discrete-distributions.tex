\section{Discrete distributions}\label{app:distribution.discrete}
\index{distribution function} \index{function!distribution}

\paragraph{Discrete distributions}

We start our discussion with the discrete distributions available in AIMMS.
They are
\begin{itemize}
\item the {\tt Binomial} distribution,
\item the {\tt HyperGeometric} distribution,
\item the {\tt Poisson} distribution,
\item the {\tt NegativeBinomial} distribution, and
\item the {\tt Geometric} distribution.
\end{itemize}

\paragraph{Discrete distributions describing successes}

The {\tt Binomial}, {\tt HyperGeometric} and {\tt Poisson} distributions
describe the number of times that a particular outcome (referred to as
"success") occurs. In the {\tt Binomial} distribution, the underlying
assumption is a fixed number of trials and a constant likelihood of success. In
the {\tt HyperGeometric} distribution, the underlying assumption is "sampling
without replacement": A fixed number of trials are taken from a population.
Each element of this population denotes a success or failure and cannot occur
more than once. In the {\tt Poisson} distribution the number of trials is not
fixed. Instead we assume that successes occur independently of each other and
with equal chance for all intervals with the same duration.

\paragraph{Distributions describing trials}

The {\tt NegativeBinomial} distribution describes the number of failures
before a specified number of successes have occurred. It assumes a constant
chance of success for each trial, so it is linked to the {\tt Binomial}
distribution. Similarly, the distribution linked to {\tt Poisson} distribution
that describes the amount of time until a certain number of successes have
occurred is known as the {\tt Gamma} distribution and is discussed in
Section~\ref{app:distribution.cont}. The {\tt NegativeBinomial} distribution
is a special case of the {\tt Geometric} distribution and describes the number
of failures before the first success occurs. Similarly, the {\tt Exponetial}
distribution is a special case of the {\tt Gamma} distribution and describes
the amount of time until the first occurrence.

\paragraph{Discrete distributions overview}

Table~\ref{table:app.discr_distributions} shows the relation between the discrete
distributions. The continuous {\tt Exponential} and {\tt Gamma} distribution
naturally fit in this table as they represent the distribution of the time it
takes before the first/$n$-th occurrence (given the average time between two
consecutive occurrences).

\begin{table}
\begin{center}
\begin{tabular}{|l|l|l|l|}
\hline\hline
& with replacement & without replacement & independent occurrences at random moments \\
\hline
example & throwing dice & drawing cards & serving customers \\
\hline
\# trials until first success / time until first occurrence & {\tt Geometric} & not supported in AIMMS & {\tt Exponential} (continuous) \\
\hline
\# trials until $n$-th success / time until $n$-th occurrence & {\tt NegativeBinomial} & not supported in AIMMS & {\tt Gamma} (continuous) \\
\hline
\# successes in fixed \# trials / \# successes in fixed time & {\tt Binomial} & {\tt HyperGeometric} & {\tt Poisson} \\
\hline\hline
\end{tabular}
\end{center}
\caption{Overview of discrete distributions in AIMMS}
\label{table:app.discr_distributions}
\end{table}

% time to the first success has a
%{\tt Gamma} distribution with its own name: the {\tt Exponential} distribution.
%A special case of is the {\tt Geometric} distribution, it describes the ,
%  Equivalent distribution for hypergeometric:
%  {Formula}{P(X = i) = \binom{Np}{r-1}\binom{N(p-1)}{i}/ (r+i) \binom{N}{r+i}
%  $$ P(X = i) = \frac{ \binom{Np}{r-1} \binom{N(p-1)}{i} }{ (r+i) \binom{N}{r+i} }$$

\paragraph{
{\tt Binomial} distribution\\
%
\psset{xunit=0.2cm,yunit=7.6cm}
\begin{pspicture}(-0.5,0)(10.5,0.26)
    \pscustom[linewidth=2pt]{
    \psdiscr{0.0025}{0}{0.009670321}
    \psdiscr{0.0025}{1}{0.05256755}
    \psdiscr{0.0025}{2}{0.133352558}
    \psdiscr{0.0025}{3}{0.209415598}
    \psdiscr{0.0025}{4}{0.227675268}
    \psdiscr{0.0025}{5}{0.181519846}
    \psdiscr{0.0025}{6}{0.109637328}
    \psdiscr{0.0025}{7}{0.051084422}
    \psdiscr{0.0025}{8}{0.018512883}
    \psdiscr{0.0025}{9}{0.005218133}
%    \psdiscr{0.0025}{10}{0.001134624}
    \moveto(-0.5,0)
    }
    \psline{-}(-0.5,0)(10.5,0)
\end{pspicture}
} \dfuncindex{Binomial} \AIMMSlink{binomial}

The {\tt Binomial}$(p,n)$ distribution: \DistributionRem {Probability of
success $p$ and number of trials $n$} { \mbox{integer} \; n > 0 \; \mbox{and}
\; 0 < p < 1} {\{i \Cond i = 0, 1, \ldots, n \}} {Formula}{P(X=i) =
\binom{n}{i} p^i (1-p)^{n-i}} {np} {np(1-p)} {{\tt Binomial}$(p,n)$ = {\tt
HyperGeometric}$(p,n,\infty)$}

A typical example for this distribution is the number of defectives in a batch
of manufactured products where a fixed percentage was found to be defective in
previously produced batches. Another example is the number of persons in a
group voting yes instead of no, where the probability of yes has been
determined on the basis of a sample.

\paragraph{
{\tt Hyper\-Geo\-metric} distribution\\
%
\psset{xunit=0.2cm,yunit=5.15cm}
\begin{pspicture}(-0.5,0)(10.5,0.38)
    \pscustom[linewidth=2pt]{
%    \psdiscr{0.003}{0}{0.00000541254}
%    \psdiscr{0.003}{1}{0.000541254}
    \psdiscr{0.003}{2}{0.010960402}
    \psdiscr{0.003}{3}{0.077940635}
    \psdiscr{0.003}{4}{0.238693195}
    \psdiscr{0.003}{5}{0.343718201}
    \psdiscr{0.003}{6}{0.238693195}
    \psdiscr{0.003}{7}{0.077940635}
    \psdiscr{0.003}{8}{0.010960402}
%    \psdiscr{0.003}{9}{0.000541254}
%    \psdiscr{0.003}{10}{0.00000541254}
    \moveto(-0.5,0)
    }
    \psline{-}(-0.5,0)(10.5,0)
\end{pspicture}
} \dfuncindex{HyperGeometric} \AIMMSlink{hypergeometric}

The {\tt HyperGeometric}$(p,n,N)$ distribution: \DistributionExt {Known initial
probability of success $p$, number of trials $n$ and
 population size $N$}
{ \mbox{integer} \; n,N: 0 < n \leq N, \; \mbox{and $p \in \frac{1}{N},
\frac{2}{N}, \ldots, \frac{N - 1}{N}$}
   }
{\{ i \Cond i = 0, 1, \ldots, n \}} {Formula}{P(X = i) = \frac{ \binom{N p}{i}
\binom{N (1-p)}{n - i} }{ \binom{N}{n} } } {np}
{np(1-p)\mbox{$\frac{N-n}{N-1}$}}

%In the {\tt HyperGeometric} distribution there is always a finite
%number of elements (referred to as a population) and a sample size
%representing a portion of the population. In addition, there is an
%initial probability of success.
As an example of this distribution, consider a set of 1000 books of which 30
are faulty When considering an order containing 50 books from this set, the
{\tt HyperGeometric}(0.03,50,1000) distribution shows the probability of
observing $i \; (i = 0, 1, \ldots,n)$ faulty books in this subset.

\paragraph{
{\tt Poisson} distribution\\
%
\psset{xunit=0.2cm,yunit=8cm}
\begin{pspicture}(-0.5,0)(10.5,0.25)
    \pscustom[linewidth=2pt]{
    \psdiscr{0.001}{0.0}{0.030197383}
    \psdiscr{0.001}{1.0}{0.105690842}
    \psdiscr{0.001}{2.0}{0.184958973}
    \psdiscr{0.001}{3.0}{0.215785469}
    \psdiscr{0.002}{4.0}{0.188812285}
    \psdiscr{0.001}{5.0}{0.1321686}
    \psdiscr{0.001}{6.0}{0.07709835}
    \psdiscr{0.001}{7.0}{0.038549175}
    \psdiscr{0.001}{8.0}{0.016865264}
    \psdiscr{0.001}{9.0}{0.006558714}
%    \psdiscr{0.001}{10.0}{0.00229555}
    \moveto(-0.5,0)
    }
    \psline{-}(-0.5,0)(10.5,0)
\end{pspicture}
} \dfuncindex{Poisson} \AIMMSlink{poisson}

The {\tt Poisson}$(\lambda)$ distribution: \DistributionRem {Average number of
occurrences $\lambda$} {\lambda > 0} {\{ i \Cond i = 0, 1, \ldots \}}
{Formula}{P(X = i) = \frac{\lambda^i}{i!} e^{-\lambda}} {\lambda} {\lambda}
{{\tt Poisson}$(\lambda)= \lim_{p \downarrow 0}${\tt Binomial}$(p,\lambda/p)$}

The {\tt Poisson} distribution should be used when there is an constant chance
of a 'success' over time or (as an approximation) when there are many
occurrences with a very small individual chance of 'success'. Typical examples
are the number of visitors in a day, the number of errors in a document, the
number of defects in a large batch, the number of telephone calls in a minute,
etc.

\paragraph{
{\tt NegativeBinomial} distribution\\
%
\psset{xunit=0.2cm,yunit=9.2cm}
\begin{pspicture}(-0.5,0)(10.5,0.21)
    \pscustom[linewidth=2pt]{
    \psdiscr{0.00205}{0}{0.07776}
    \psdiscr{0.00205}{1}{0.15552}
    \psdiscr{0.00205}{2}{0.186624}
    \psdiscr{0.00205}{3}{0.1741824}
    \psdiscr{0.00205}{4}{0.13934592}
    \psdiscr{0.00205}{5}{0.100329062}
    \psdiscr{0.00205}{6}{0.066886042}
    \psdiscr{0.00205}{7}{0.042042655}
    \psdiscr{0.00205}{8}{0.025225593}
    \psdiscr{0.00205}{9}{0.014574787}
    \psdiscr{0.00205}{10}{0.008161881}
    \moveto(-0.5,0)
    }
    \psline{-}(-0.5,0)(10.5,0)
\end{pspicture}
} \dfuncindex{NegativeBinomial} \AIMMSlink{negativebinomial}

The {\tt NegativeBinomial}$(p,r)$ distribution: \DistributionExt {Success
probability $p$ and number of successes $r$} {0 < p < 1 \; \mbox{and} \; r = 1,
2, \ldots} {\{ i \Cond i = 0, 1, \ldots \}} {Formula} {P(X = i) = \binom{r + i
- 1}{i} p^r (1-p)^i} {r/p-r} {r(1-p)/p^2}

Whenever there is a repetition of the same activity, and you are interested in
observing the $r$-th occurrence of a particular outcome, then the {\tt NegativeBinomial} distribution might be applicable. A typical situation is going from
door-to-door until you have made $r$ sales, where the probability of making a
sale has been determined on the basis of previous experience. Note that the
{\tt NegativeBinomial} distribution describes the number of {\em failures}
before the $r$-th success. The distribution of the number of {\em trials} $i$
before the $r$-th success is given by $P_{\tt NegativeBinomial(p,r)}(X=i-r)$.

\paragraph{
{\tt Geometric} distribution\\
%
\psset{xunit=0.2cm,yunit=7.9cm}
\begin{pspicture}(-0.5,0)(10.5,0.29)
    \pscustom[linewidth=2pt]{
    \psdiscr{0.00175}{0}{0.25}
    \psdiscr{0.00175}{1}{0.1875}
    \psdiscr{0.00175}{2}{0.140625}
    \psdiscr{0.00175}{3}{0.10546875}
    \psdiscr{0.00225}{4}{0.079101563}
    \psdiscr{0.00175}{5}{0.059326172}
    \psdiscr{0.00175}{6}{0.044494629}
    \psdiscr{0.00175}{7}{0.033370972}
    \psdiscr{0.00175}{8}{0.025028229}
    \psdiscr{0.00175}{9}{0.018771172}
    \psdiscr{0.00225}{10}{0.014078379}
    \moveto(-0.5,0)
    }
    \psline{-}(-0.5,0)(10.5,0)
\end{pspicture}
} \dfuncindex{Geometric} \AIMMSlink{geometric}

The {\tt Geometric}$(p)$ distribution: \DistributionRem {Probability of success
$p$} {0 < p < 1} {\{i \Cond i = 0, 1, \ldots \}} {Formula}{P(X = i) = (1 - p)^i
p} {1/p-1} {(1-p)/p^2} {{\tt Geometric}$(p)$ = {\tt NegativeBinomial}$(p,1)$}

The {\tt Geometric} distribution is a special case of the {\tt
NegativeBinomial} distribution. So it can be used for the same type of problems
(the number of visited doors before the first sale). Another example is an oil
company drilling wells until a producing well is found, where the probability
of success is based on measurements around the site and comparing them with
measurements from other similar sites.


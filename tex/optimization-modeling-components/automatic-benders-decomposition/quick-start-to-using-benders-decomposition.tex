\section{Quick start to using Benders' decomposition} \label{sec:benders.quickstart}

\paragraph{System module}

The system module with the name {\tt GMP Benders Decomposition} implements the Benders' decomposition algorithm.
You can add this module to your project using the {\bf Install System Module} command in the AIMMS {\bf Settings} menu.
This module contains three procedures that can be called, each implementing a different algorithm.

\paragraph{Classic algorithm}

The procedure {\tt Do\-Benders\-Decomposition\-Classic} inside this module implements the
classic version of the Benders' decomposition algorithm, in which the master problem and the
subproblem are solved in an alternating sequence.

\paragraph{Modern algorithm}

The procedure {\tt Do\-Benders\-Decomposition\-Single\-MIP} inside the module implements the
modern approach for MIP problems which solves only a single MIP problem; the subproblem is
solved whenever the MIP solver finds a solution for the master problem (using callbacks).

\paragraph{Two phase algorithm}

The procedure {\tt Do\-Benders\-Decomposition\-Two\-Phase} inside the module implements a
two phase algorithm for MIP problems. In the first phase it solves the relaxed problem (in
which the integer variables become continuous) using the classic Benders decomposition algorithm.
The Benders' cuts found in the first phase are then added to the master problem in the second
phase after which the MIP problem is solved using either the classic or modern approach
of the Benders decomposition algorithm.

\paragraph{The procedure {\tt Do\-Benders\-Decomposition\-Classic}}

The procedure {\tt DoBendersDecompositionClassic} has two input arguments:
\begin{enumerate}
\item {\tt MyGMP}, an element parameter with range {\tt All\-Ge\-ne\-ra\-ted\-Ma\-the\-ma\-ti\-cal\-Pro\-grams}, and
\item {\tt MyMasterVariables}, a subset of the predefined set {\tt AllVariables} defining the variables
in the master problem.
\end{enumerate}

For a MIP problem, the integer variables typically become the variables of the master problem,
although it is possible to also include continuous variables in the set of master problem variables.
The {\tt Do\-Benders\-Decomposition\-Classic} procedure is called as follows:

\begin{example}
    generatedMP := GMP::Instance::Generate( SymbolicMP );
    
    GMPBenders::DoBendersDecompositionClassic( generatedMP, AllIntegerVariables );
\end{example}

Here {\tt SymbolicMP} is the symbolic mathematical program containing the MIP model, and
{\tt generatedMP} is an element parameter in the predefined set
{\tt All\-Generated\-Mathematical\-Programs}. {\tt GMPBenders} is the prefix of the
Benders' module. The implementation of this procedure will be discussed in
Section~\ref{sec:benders.textbook.alg}.

\paragraph{The procedure {\tt Do\-Benders\-Decomposition\-Single\-MIP}}

The procedure {\tt Do\-Benders\-Decomposition\-Single\-MIP} has the same input arguments
as the procedure {\tt Do\-Benders\-Decomposition\-Classic}. The {\tt Do\-Benders\-Decomposition\-Single\-MIP}
procedure is called as follows:

\begin{example}
    generatedMP := GMP::Instance::Generate( SymbolicMP );
    
    GMPBenders::DoBendersDecompositionSingleMIP( generatedMP, AllIntegerVariables );
\end{example}

This procedure can only be used if the original problem contains some integer variables. The
implementation of this procedure will be discussed in Section~\ref{sec:benders.modern.impl}.

\paragraph{The procedure {\tt Do\-Benders\-Decomposition\-Two\-Phase}}

The procedure {\tt Do\-Benders\-Decomposition\-Two\-Phase} has one additional argument
compared to the procedure {\tt Do\-Benders\-Decomposition\-Classic}. Namely, the third argument
{\tt Use\-Single\-MIP} is used to indicate whether the second phase should use the
classic algorithm (value 0) or the modern algorithm (value 1). The procedure is called as follows
if the modern algorithm should be used:

\begin{example}
    generatedMP := GMP::Instance::Generate( SymbolicMP );
    
    GMPBenders::DoBendersDecompositionTwoPhase( generatedMP, AllIntegerVariables, 1 );
\end{example}

This procedure should only be used if the original problem contains some integer variables. The
implementation of this procedure will be discussed in Section~\ref{sec:benders.twophase.impl}.

\paragraph{Combining procedure}

To make it easier for you to switch between the three algorithms, 
the module also implements the procedure {\tt Do\-Benders\-Decomposition} that calls one of the
three procedures above based on the Benders' mode. The first two arguments of this procedure
are the same as before, namely {\tt MyGMP} and {\tt My\-Master\-Variables}. The third argument,
{\tt BendersMode}, is an element parameter that defines the Benders' mode and can take
value {\tt 'Classic'}, {\tt 'Modern'}, {\tt 'Two\-Phase\-Classic'} or {\tt 'Two\-Phase\-Modern'}.
The procedure is called as follows if the two phase algorithm should be used with the
modern algorithm for the second phase:

\begin{example}
    generatedMP := GMP::Instance::Generate( SymbolicMP );
    
    GMPBenders::DoBendersDecomposition( generatedMP, AllIntegerVariables,
                                        'TwoPhaseModern' );
\end{example}

If the problem contains no integer variables then only mode {\tt 'Classic'} can be used.

\paragraph{Control parameters}

The Benders' module defines several parameters that influence the Benders' decomposition algorithm.
These parameters have a similar functionality as options of a solver, e.g., {\sc Cplex}. The most
important parameters, with their default setting, are shown in Table~\ref{table:benders.controlparam}.
\begin{aimmstable}
\begin{tabular}{|l|l|l|l|}
\hline\hline
{\bf Parameter} &  {\bf Default} &  {\bf Range} &  {\bf Subsection} \\
\hline
{\tt BendersOptimalityTolerance}     & 1e-6  & [0,1]        & \\
{\tt IterationLimit}                 & 1e7   & \{1,maxint\} & \\
{\tt TimeLimit}                      & 1e9   & [0,inf)      & \\
{\tt CreateStatusFile}               & 0     & \{0,1\}      & \\
{\tt UseDual}                        & 0     & \{0,1\}      & \ref{sec:benders.primaldual.sub} \\
{\tt FeasibilityOnly}                & 1     & \{0,1\}      & \ref{sec:benders.feas.prob} \\
{\tt NormalizationType}              & 1     & \{0,1\}      & \ref{sec:benders.normalization} \\
{\tt UseMinMaxForFeasibilityProblem} & 1     & \{0,1\}      & \ref{sec:benders.feas.mode} \\
{\tt AddTighteningConstraints}       & 1     & \{0,1\}      & \ref{sec:benders.tight.cons} \\
{\tt UseStartingPointForMaster}      & 0     & \{0,1\}      & \ref{sec:benders.start.point} \\
{\tt UsePresolver}                   & 0     & \{0,1\}      & \ref{sec:benders.aimms.presolver} \\
\hline\hline
\end{tabular}
\caption{Control parameters in the Benders' module}\label{table:benders.controlparam}
\end{aimmstable}
The parameters that are not self-explanatory are explained in Section~\ref{sec:benders.control.par};
the last column in the table refers to the subsection that discusses the corresponding parameter.

\paragraph{Optimality tolerance}

The optimality tolerance, as controlled by the parameter {\tt Benders\-Optimality\-Tolerance},
guarantees that a solution returned by the Benders' decomposition algorithm lies within a certain
percentage of the optimal solution.

\paragraph{Solver options}

The parameters {\tt Benders\-Optimality\-Tolerance}, {\tt Iteration\-Limit} and {\tt Time\-Limit}
are used by the classic algorithm (and the first phase of the two phase algorithm). For
the modern algorithm, the corresponding general solver options,
{\tt MIP\_relative\_optimality\_tolerance}, {\tt iteration\_limit} and {\tt time\_limit}
re\-spec\-tive\-ly, are used.


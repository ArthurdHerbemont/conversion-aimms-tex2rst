\section{Benders' decomposition - Textbook algorithm} \label{sec:benders.textbook.alg}

\paragraph{Master problem}

The basic Benders' decomposition algorithm as explained in several textbooks (e.g., \cite{bib:NW88}, \cite{bib:Ma99})
works as follows. After introducing an artificial variable $\eta = d^Ty$, the master problem relaxation
becomes:

\begin{model}
\direction{Minimize:}
\begin{alignat*}{4}
   \modelline{}{c^Tx + \eta}{}{}
\end{alignat*}
\subjecttomath
\begin{alignat*}{4}
   \modelline{}{A x}{\leq b}{}\\
   \modelline{}{\eta}{\geq \overline{\eta}}{}\\
   \modelline{}{x}{\in \mathbb{Z}^n_+}{}
\end{alignat*}
\end{model}

Here $\overline{\eta}$ is a lower bound on the variable $\eta$ that AIMMS will automatically derive. For example,
if the vector $d$ is nonnegative then we know that 0 is a lower bound on $d^Ty$ since we assumed that the variable
$y$ is nonnegative, and therefore we can take $\overline{\eta} = 0$. We assume that the master problem is bounded.

\paragraph{Subproblem}

After solving the master problem we obtain an optimal solution, denoted by $(x^*,\eta^*)$ with $x^*$ integer.
This solution is fixed in the subproblem which we denote by $PS(x^*)$:

\begin{model}
\direction{Minimize:}
\begin{alignat*}{4}
   \modelline{}{d^Ty}{}{}
\end{alignat*}
\subjecttomath
\begin{alignat*}{4}
   \modelline{}{Q y}{\leq r - Tx^*}{}\\
   \modelline{}{y}{\in \mathbb{R}^m_+}{}
\end{alignat*}
\end{model}

Note that this subproblem is a linear programming problem in which the continuous variable $y$ is the only
variable.

\paragraph{Dual subproblem}

Textbooks that explain Benders' decomposition often use the dual of this subproblem because duality theory
plays an important role, and the Benders' optimality and feasibility cuts can be expressed using the variables
of the dual problem. The dual of the subproblem $PS(x^*)$ is given by:

\begin{model}
\direction{Maximize:}
\begin{alignat*}{4}
   \modelline{}{r - \pi^T(Tx^*)}{}{}
\end{alignat*}
\subjecttomath
\begin{alignat*}{4}
   \modelline{}{\pi^TQ}{\geq d^T}{}\\
   \modelline{}{\pi}{\geq 0}{}
\end{alignat*}
\end{model}

We denote this problem by $DS(x^*)$.

\paragraph{Optimality cut}

If this subproblem is feasible, let $z^*$ denote the optimal objective value and $\overline{\pi}$ an optimal solution
of $DS(x^*)$. If $z^* \leq \eta^*$ then the current solution $(x^*,\eta^*)$ is a feasible and optimal
solution of our original problem, and the Benders' decomposition algorithm only needs to solve
$PS(x^*)$ to obtain optimal values for variable $y$. If $z^* > \eta^*$ then the Benders' optimality cut
$\eta \geq \overline{\pi}^T (r - Tx)$ is added to the master problem and the algorithm continues by solving the
master problem again.

\paragraph{Feasibility cut}

If the dual subproblem is unbounded, implying that the primal subproblem is infeasible, then an
unbounded extreme ray $\overline{\pi}$ is selected and the Benders' feasibility cut
$\overline{\pi}^T (r - Tx) \leq 0$ is added to the master problem. Modern solvers like {\sc Cplex}
and {\sc Gurobi} can provide an unbounded extreme ray in case a LP problem is unbounded. After adding
the feasibility cut the Benders' decomposition algorithm continues by solving the master problem.


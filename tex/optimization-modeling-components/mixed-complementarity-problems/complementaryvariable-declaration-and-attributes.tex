\section{\tttext{ComplementaryVariable} declaration and attributes}\label{sec:compl.variable}

\paragraph{Complementar\-ity~variables}
\declindex{ComplementaryVariable} \AIMMSlink{complementarity_variable}

To support you in formulating a complementarity model, AIMMS provides a
special type of variable, the {\tt ComplementaryVariable}. The attributes of
a complementarity variable allow you to declare an (indexed) class of variables
in a complementarity model along with their associated constraints. The
attributes of a {\tt ComplementaryVariable} are listed in
Table~\ref{table:compl.attr-compl}.

\paragraph{Automatic sanity checks}

By construction, this new variable type automatically ensures that every
variable in a complementarity model is associated with a single constraint.
Also, when AIMMS detects that the total number of (finite) bounds on both
the complementarity variable and its associated constraint is not equal to two
(as required above), a compilation error will result. Thus, {\tt
ComplementaryVariable} will help to reduce the most common declaration
errors for this type of model.
\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}               & {\bf See also} \\
                &                                    & {\bf page} \\
\hline \verb|IndexDomain|   & {\em index-domain}                  &
                \pageref{attr:par.index-domain},
                \pageref{attr:var.index-domain}, \pageref{attr:var.index-domain}\\
\verb|Range|    & {\em range}                           & \pageref{attr:var.range}\\
\verb|Unit|     &{\em  unit-valued expression}          &
                \pageref{attr:par.unit}, \pageref{attr:var.unit}\\
\verb|Text|         & {\em string     }                 &
\pageref{attr:prelim.text}, \pageref{attr:par.text}\\
\verb|Comment|      & {\em comment string }             & \pageref{attr:prelim.comment}\\
\verb|Complement|   & {\em expression}                  & \pageref{attr:var.constr.definition}\\
\verb|NonvarStatus| & {\em reference}                  & \pageref{attr:var.nonvar}\\
\verb|Property|  & {\tt NoSave}, {\tt Complement}      & \\
\hline\hline
\end{tabular}
\caption{{\tt ComplementaryVariable}
attributes}\label{table:compl.attr-compl}
\end{aimmstable}

\paragraph{The {\tt IndexDomain} attribute}
\declattrindex{complementarity variable}{IndexDomain}
\AIMMSlink{complementarity_variable.index_domain}

Through the {\tt IndexDomain} attribute of a complementarity variable you can
specify domain of tuples for which you want AIMMS to generate a variable and
its associated constraint. During generation, AIMMS will only generate a
variable for all tuples that satisfy all domain restrictions that you have
imposed on the domain.

\paragraph{The {\tt Range} attribute}
\declattrindex{complementarity variable}{Range}
\AIMMSlink{complementarity_variable.range}

In the {\tt Range} attribute you can specify the lower and upper bound of a
complementarity variable, in a similar manner for ordinary {\tt Variables} (see
also Section~\ref{sec:var.var}). During generation, AIMMS will perform a
runtime check, for every individual tuple in the index domain, whether the
number of finite bounds specified here, plus the number of finite bounds in the
constraint specified in the {\tt Complement} attribute, exactly equals two.

\paragraph{The {\tt Complement} attribute}
\declattrindex{complementarity variable}{Complement}
\AIMMSlink{complementarity_variable.complement}

The {\tt Complement} attribute allows you to specify the constraint that must
be associated with the complementarity variable at hand. With $f(x,\dots)$ a
general nonlinear function, the following types of expressions are allowed
\begin{itemize}
\item     $\phantom{a\leq{}}f(x,\dots)\geq a$ \qquad (variable must have a single-sided {\tt Range}),
\item     $\phantom{a\leq{}}f(x,\dots)\leq a$ \qquad (variable must have a single-sided {\tt Range}),
\item     $a \leq f(x,\dots) \leq b$ \qquad (variable must be free),
\item     $\phantom{a\leq{}}f(x,\dots)= a$ \qquad (variable must be free), or
\item     $\phantom{a\leq{}}f(x,\dots)\phantom{{}\leq b}$ \qquad (variable must be bounded).
\end{itemize}
In addition, the {\tt Complement} attribute can refer to an existing
{\tt Constraint} in your model, which then should hold a definition as one of
the cases above. The {\tt Complement} attribute can also hold a scalar element 
parameter into the set {\tt AllConstraints}, which offers the possibility to assign 
different constraints to the complementarity variable in sequential solves.

\paragraph{Constraint listing}

In the constraint listing, the constraints associated with a
complementarity variable will be listed with a generated name consisting of the
name of the {\tt ComplementarityVariable} with an additional suffix ``{\tt
\_complement}''.

\paragraph{The {\tt NonvarStatus} attribute}
\declattrindex{complementarity variable}{NonvarStatus}
\AIMMSlink{complementarity_variable.nonvar_status}

With the {\tt NonvarStatus} attribute you can indicate for which tuples you
want AIMMS to consider the complementarity variable as a parameter, i.e.\
with the lower and upper bound set equal to the level value prior to solving
the model (see also Section~\ref{sec:var.var.solver-attr}). From the mixed
complementarity condition it follows that the function in the corresponding
constraint is then allowed to assume arbitrary values, whence there is no
strict need to generate the variable and constraint for the solver.

\paragraph{Positive and negative values}

The value of the {\tt NonvarStatus} attribute must be an expression in some or
all of the indices in the index list of the variable, allowing you to change
the nonvariable status of individual elements or groups of elements at once.
When the {\tt NonvarStatus} assumes a positive value, AIMMS will not
generate the variable and its associated constraint. For negative values, the
variable and constraint will be generated, but reduces to the second special
case of the mixed complementarity condition
\[
    \hat{x}_i = x_i - x_i^0 = 0 \quad \text{and}\quad f_i(x) \text{ is "free"},
\]
i.e.\ the function in the constraint will be allowed to assume arbitrary
values.

\paragraph{The {\tt Unit} attribute}
\declattrindex{complementarity variable}{Unit}
\AIMMSlink{complementarity_variable.unit}

Providing a {\tt Unit} for a complementarity variable will help you in a number
of ways.
\begin{itemize}
\item  AIMMS will help you to check the consistency of all the
constraints and assignments in your model (including the expression in the {\tt
Complement} attribute), and
\item AIMMS will use the units  to scale the model that is sent to the solver.
\end{itemize}
Proper scaling of a model will generally result in a more accurate and robust
solution process. You can find more information on the definition and use of
units to scale mathematical programs in Chapter~\ref{chap:units}.

\paragraph{The {\tt Property} attribute}
\declattrindex{complementarity variable}{Property}
\AIMMSlink{complementarity_variable.property} \suffindex{complementarity
variable}{Complement}

Complementarity variables support the properties {\tt NoSave} and {\tt
Complement}. With the property {\tt NoSave} you indicate that you do not want
to store data associated with this variable in a case. The {\tt Complement}
property indicates that you are interested in the level values of the
constraint defined in the {\tt Complement} attribute. When this property is
set, AIMMS will make the level value of this constraint available through
the {\tt .Complement} suffix of the complementarity variable at hand.

\paragraph{Example}

The declaration of the complementarity variable {\tt MembraneHeight} expresses
a complementarity condition for the height of a membrane in a rectangular
$(x,y)$-grid, with a uniform external force acting on each cell in the grid.
\begin{example}
ComplementaryVariable MembraneHeight {
    IndexDomain  : (x,y);
    Range        : [MembraneLowerBound(x,y), MembraneUpperBound(x,y)];
    Complement   : {
        4*MembraneHeight(x,y)
        - MembraneHeight(x+1,y) - MembraneHeight(x-1,y)
        - MembraneHeight(x,y+1) - MembraneHeight(x,y-1)
        - CellForce
    }
}
\end{example}
The complementarity condition expresses that either the membrane reaches one
its given bounds (for instance, an obstacle placed in the way of the membrane),
or the external force on the cell must be equal to the internal forces acting
on the cell caused by differences in height with neighboring cells.

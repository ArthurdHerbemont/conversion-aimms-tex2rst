\section{\tttext{Node} declaration and attributes}\label{sec:net.node}

\paragraph{Node attributes}
\declindex{Node} \AIMMSlink{node}

Each node in a network has a number of associated incoming and outgoing flows.
Unless stated otherwise, these flows should be in balance. Based on the flows
specified in the model, AIMMS will automatically generate a balancing
constraint for every node. The possible attributes of a {\tt Node} declaration
are given in Table~\ref{table:net.attr-node}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}               & {\bf See also} \\
                &                                    & {\bf page} \\
\hline \verb|IndexDomain|   & {\em index-domain}                  &
                \pageref{attr:par.index-domain},
                \pageref{attr:var.index-domain}, \pageref{attr:var.constr.index-domain}\\
\verb|Unit|     &{\em  unit-valued expression}          &
                \pageref{attr:par.unit}, \pageref{attr:var.unit}\\
\verb|Text|         & {\em string     }                 &
\pageref{attr:prelim.text}, \pageref{attr:par.text}\\
\verb|Comment|      & {\em comment string }             & \pageref{attr:prelim.comment}\\
\verb|Definition|   & {\em expression}                  & \pageref{attr:var.constr.definition}\\
\verb|Property|  & {\tt NoSave}, {\tt Sos1}, {\tt Sos2},       &
                \pageref{attr:par.property},
                \pageref{attr:var.property},
         \pageref{attr:var.constr.property} \\
         & {\tt Level}, {\tt Bound}, {\tt ShadowPrice}, & \\ &
         {\tt RightHandSideRange}, {\tt ShadowPriceRange} &\\
\hline\hline
\end{tabular}
\caption{{\tt Node} attributes}\label{table:net.attr-node}
\end{aimmstable}

\paragraph{Nodes are like constraints}%
\declattrindex{node}{IndexDomain} \declattrindex{node}{Unit}
\declattrindex{node}{Definition} \declattrindex{node}{Property}
\AIMMSlink{node.index_domain} \AIMMSlink{node.unit} \AIMMSlink{node.definition}
\AIMMSlink{node.property}

Nodes are a special kind of constraint. Therefore, the remarks in
Section~\ref{sec:var.constr} that apply to the attributes of constraints are
also valid for nodes. The only difference between constraints and nodes is that
in the definition attribute of a node you can use one of the keywords
\verb|NetInflow| and \verb|NetOutflow|.

\paragraph{{\tt NetInflow} and {\tt NetOutflow}}
\keyindex{NetInflow} \keyindex{NetOutflow} \AIMMSlink{netinflow}
\AIMMSlink{netoutflow}

The keywords \verb|NetInflow| and \verb|NetOutflow| denote the net input or net
output flow for the node. The expressions represented by \verb|NetInflow| and
\verb|NetOutflow| are computed by AIMMS on the basis of all arcs that depart
from and arrive at the declared node. Since these keywords are opposites, you
should choose the keyword that makes most sense for a particular node.

\paragraph{Example}

The following two {\tt Node} declarations show natural applications of the
keywords {\tt NetInflow} and {\tt NetOutflow}.
\begin{example}
Node CustomerDemandNode {
    IndexDomain  : (j in Customers, p in Products);
    Definition   : {
        NetInflow >= ProductDemanded(j,p)
    }
}
\end{example}
\pagebreak
\begin{example}
Node DepotStockSupplyNode {
    IndexDomain  : (i in Depots, p in Products);
    Definition   : {
        NetOutflow <= StockAvailable(i,p) + ProductImport(i,p)
    }
}
\end{example}%
The declaration of {\tt CustomerDemandNode(c,p)} only involves network flows,
while the flow balance of {\tt DepotStockSupplyNode(d,p)} also uses a variable
{\tt Product\-Import(d,p)}.

\section{\tttext{Arc} declaration and attributes}\label{sec:net.arc}

\paragraph{Arc attributes}
\declindex{Arc} \AIMMSlink{arc}

Arcs are used to represent the possible flows between nodes in a network. From
these flows, balancing constraints can be generated by AIMMS for every node
in the network. The possible attributes of an arc are given in
Table~\ref{table:net.attr-arc}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}               & {\bf See also} \\
                &                                & {\bf page} \\
\hline \verb|IndexDomain|   & {\em index-domain}                  &
                \pageref{attr:par.index-domain}\\
\verb|Range|    & {\em range}                          & \pageref{attr:var.range}\\
\verb|Default|  &{\em  constant-expression}            &
\pageref{attr:par.default}, \pageref{attr:var.default}\\
\verb|From|     & {\em node-reference}                   & \\
\verb|FromMultiplier|  & {\em expression}              & \\
\verb|To|       & {\em node-reference}                   & \\
\verb|ToMultiplier|    & {\em expression}              & \\
\verb|Cost|     & {\em expression}              & \\
\verb|Unit|     &{\em  unit-valued expression}         & \pageref{attr:var.unit}\\
\verb|Priority| & {\em expression}              & \pageref{attr:var.priority}\\
\verb|NonvarStatus|   & {\em expression}              & \pageref{attr:var.nonvar}\\
\verb|RelaxStatus|    & {\em expression}           & \pageref{attr:var.relax}\\
\verb|Property|     &{\tt NoSave}, {\em numeric-storage-property}, &
\pageref{attr:set.property}, \pageref{attr:par.property}, \pageref{attr:var.property}\\
           & {\tt Inline}, {\tt SemiContinuous}, {\tt
           ReducedCost}, & \\ & {\tt ValueRange}, {\tt
           CoefficientRange} &\\
\verb|Text|         & {\em string}                     &
           \pageref{attr:prelim.text}, \pageref{attr:par.text}\\
\verb|Comment|      & {\em comment string }            & \pageref{attr:prelim.comment}\\
\hline\hline
\end{tabular}
\caption{{\tt Arc} attributes}\label{table:net.attr-arc}
\end{aimmstable}

\paragraph{Arcs are like variables}
\declattrindex{arc}{IndexDomain} \declattrindex{arc}{Range}
\declattrindex{arc}{Default} \declattrindex{arc}{Unit}
\declattrindex{arc}{Priority} \declattrindex{arc}{NonvarStatus}
\declattrindex{arc}{RelaxStatus} \declattrindex{arc}{Property}
\AIMMSlink{arc.index_domain} \AIMMSlink{arc.range} \AIMMSlink{arc.default}
\AIMMSlink{arc.unit} \AIMMSlink{arc.priority} \AIMMSlink{arc.nonvar_status}
\AIMMSlink{arc.relax_status} \AIMMSlink{arc.property}

Arcs play the role of variables in a network problem, but have some extra
attributes compared to ordinary variables, namely the {\tt From}, {\tt To},
{\tt FromMultiplier}, {\tt ToMultiplier}, and {\tt Cost}
attributes. Arcs do not have a {\tt Definition} attribute because they are
implicitly defined by the {\tt From} and {\tt To} attributes.

\paragraph{The {\tt From} and {\tt To} attributes}\herelabel{attr:net.arc.from}\label{attr:net.arc.to}
\declattrindex{arc}{From} \declattrindex{arc}{To} \AIMMSlink{arc.from}
\AIMMSlink{arc.to}

For each arc, the \verb|From| attribute is used to specify the starting node,
and the {\tt To} attribute to specify the end node. The value of both
attributes must be a reference to a declared node.

\paragraph{The {\tt Multiplier} attributes}\herelabel{attr:net.arc.multiplier}
\declattrindex{arc}{FromMultiplier} \declattrindex{arc}{ToMultiplier}
\AIMMSlink{arc.from_multiplier} \AIMMSlink{arc.to_multiplier}

With the {\tt FromMultiplier} and {\tt ToMultiplier} attributes
you can specify whether the flow along an arc has a gain or loss factor. Their
value must be an expression defined over some or all of the indices of the
index domain of the arc. The result of the expression must be positive. If you
do not specify a {\tt Multiplier} attribute, AIMMS assumes a default of one.
Network problems with non unit-valued {\tt Multiplier}s are called {\em
generalized networks}.

\paragraph{{\tt FromMultiplier} and {\tt ToMultiplier}}

The {\tt FromMultiplier} is the conversion factor of the flow at the source
node, while the {\tt ToMultiplier} is the conversion factor at the destination
node. Having both multipliers offers you the freedom to specify the network in
its most natural way.

\paragraph{The {\tt Cost} attribute}\herelabel{attr:net.arc.cost}
\declattrindex{arc}{Cost} \AIMMSlink{arc.cost} \keyindex{FlowCost}

You can use the {\tt Cost} attribute to specify the cost associated with the
transport of one unit of flow across the arc. Its value is used in the
computation of the special variable {\tt FlowCost}, which is the accumulated
cost over all arcs. In the computation of the {\tt FlowCost} variable the
component of an arc is computed as the product of the unit cost and the level
value of the flow.

\paragraph{Graphically illustrated}

In the presence of {\tt FromMultiplier} and {\tt ToMultipliers}, the drawing in
Figure~\ref{fig:net.flow} illustrates
\begin{itemize}
\item the level value of the flow,
\item its associated cost component in the predefined {\tt FlowCost} variable, and
\item the flows as they enter into the flow balances at the source and
destination nodes (denoted by SBF and DBF, respectively).
\end{itemize}
\begin{aimmsfigure}
\psset{dimen=middle,xunit=0.65cm,yunit=0.65cm}
\begin{pspicture}(3,2.5)(15,7.5)
  \cnode*(4.5,5){5pt}{node_i}\rput(4.5,5.75){Node $i$}
  \cnode*(15.5,5){5pt}{node_j}\rput(15.5,5.75){Node $j$}
  \pnode(4,5){start}
  \pnode(16,5){end}
  \pnode(5.25,5){from}
  \pnode(14.75,5){to}
  \pnode(5.25,3.1){from_expr}
  \pnode(14.75,3.1){to_expr}
  \rput(5.75,2.9){{SBF = \tt Flow(i,j)/[FromMultiplier]}}
  \rput(14.25,2.9){{DBF = \tt Flow(i,j)*[ToMultiplier]}}
  \ncline[nodesep=4pt]{->}{from}{from_expr}
  \ncline[nodesep=4pt]{->}{to}{to_expr}
  \ncline{->}{node_i}{node_j}\rput(10,6.5){Level = {\tt Flow(i,j)}}
    \rput(10,6){Cost = {\tt Flow(i,j)*[Cost]}}
%  \ncline{->}{start}{node_i}
%  \ncline{->}{node_j}{end}
  \psline[linestyle=dashed]{-}(6,4)(6,6)
  \psline[linestyle=dashed]{-}(14,4)(14,6)
%  \psline[linestyle=dashed]{-}(3,5)(4,5)
%  \psline[linestyle=dashed]{-}(16,5)(17,5)
  \end{pspicture}
\caption{Flow levels and cost from node $i$ to node $j$}\label{fig:net.flow}
\end{aimmsfigure}

\paragraph{Semi-continuous arcs}
\declpropindex{arc}{SemiContinuous}

You can only use the {\tt SemiContinuous} property for arcs if you use an LP
solver to find the solution. If you use the pure network solver integrated in
AIMMS, AIMMS will issue an error message.

\paragraph{Example}

Using the declaration of nodes from the previous section, an example of a valid
arc declaration is given by
\begin{example}
Arc Transport {
    IndexDomain  :  (i,j,p) | Distance(i,j);
    Range        :  nonnegative;
    From         :  DepotStockSupplyNode(i,p);
    To           :  CustomerDemandNode(j,p);
    Cost         :  UnitTransportCost(i,j);
}
\end{example}
Note that this arc declaration declares flows between nodes {\tt i} and {\tt j}
for multiple products {\tt p}.

\section{Introduction to the \GMP~library}\label{sec:gmp.intro}

\paragraph{Introduction}

With every {\tt MathematicalProgram} declared as part of your model, the
{\gmp} library allows you to associate
\begin{itemize}
\item one or more {\em \underline{G}enerated \underline{M}ath \underline{P}rogram instances} (GMPs),
\end{itemize}
and with each GMP
\begin{itemize}
\item a conceptual matrix of coefficients that can be manipulated,
\item a repository of initial, intermediate or final solutions, and
\item a pool of local or remote solver sessions.
\end{itemize}
Figure~\ref{fig:gmp.concepts} illustrates the interrelationship between
symbolic mathematical programs and the concepts of the {\GMP} library, as well
as the main properties that can be associated with each of them.
\begin{aimmsfigure}
  \psset{xunit=1cm,yunit=1cm,framesep=0.2cm}
  \begin{pspicture}(2,-4)(11,11)
    \rput[l](1,9.7){\psframebox{\parbox[b]{8cm}{\small
      \vspace*{1mm}
      \begin{itemize}
        \item symbolic variables
        \item symbolic constraints
      \end{itemize}
      \vspace*{1mm}
    }}}
    \rput[l](1,10.7){\sc Symbolic MP}
    \rput[r](9.5,10.7){$\in$ {\tt AllMathematicalPrograms}}
    \rput[l](3,6){\psframebox{\parbox[b]{8cm}{\small
      \begin{itemize}
        \item generated columns
        \item generated rows
        \item generated matrix coefficients
        \item mapping to symbolic variables and constraints
      \end{itemize}
    }}}
    \rput[l](3,7.4){\sc Matrix}
    \rput[l](3,0.7){\psframe(8.46,3.3)}
    \rput[l](3,4.25){\sc Solution repository}
    \rput[r](11.5,4.25){$\subseteq$ {\tt Integers}}
    \rput[l](3.4,2.1){\psframebox[linecolor=gray]{\parbox[b]{3.5cm}{\scriptsize
      \vspace*{-2mm}
      \begin{itemize}
        \item solution status
        \item level values
        \item $\left[\text{basis information}\right]$
        \item $\left[\text{marginals}\right]$
        \item $\ldots$
      \end{itemize}
      \vspace*{-2mm}
    }}}
    \rput[r](7.4,3.45){\small \sc Solution~1}
    \rput[l](7.7,2.1){\psframebox[linecolor=gray]{\parbox[b]{1.5cm}{\scriptsize
      \vspace*{-2mm}
      \begin{itemize}
        \item $\ldots$
        \item $\ldots$
        \item $\ldots$
        \item $\ldots$
        \item $\ldots$
      \end{itemize}
      \vspace*{-2mm}
    }}}
    \rput[r](9.65,3.45){\small \sc Solution~2}
    \rput[l](10.3,2.2){\bf\large\ldots}
    \rput[l](3,-2.55){\psframe(8.46,2.4)}
    \rput[l](3,0.1){\sc Solver session pool}
    \rput[r](11.5,0.1){$\subseteq$ {\tt AllSolverSessions}}
    \rput[l](3.4,-1.6){\psframebox[linecolor=gray]{\parbox[b]{4.5cm}{\scriptsize
      \vspace*{-2.5mm}
      \begin{itemize}
        \item solver process specification
        \item solver option settings
      \end{itemize}
      \vspace*{-2.5mm}
    }}}
    \rput[r](8.4,-0.7){\small \sc Solver session~1}
    \rput[l](9.5,-1.5){\bf\large\ldots}
    \psframe[linecolor=gray](2.5,-3)(11.9,8)
    \rput[l](2.5,8.25){\sc Generated MP}
    \rput[r](11.9,8.25){$\in$ {\tt AllGeneratedMathematicalPrograms}}
    \psline[linewidth=2pt]{->}(1.7,8.9)(1.7,7.5)(2.5,7.5)
    \psline[linewidth=2pt]{-}(1.7,7.5)(1.7,-2)
    \psline[linewidth=2pt,linestyle=dotted](1.7,-2)(1.7,-3)
  \end{pspicture}
\caption{Concepts associated with a GMP} \label{fig:gmp.concepts}
\end{aimmsfigure}

\paragraph{Generated mathematical program instances}
\index{mathematical program!generated instance} \index{generated mathematical
program instance}

For every {\tt MathematicalProgram} declaration in your model, modifications
in the index sets and input data referenced in constraints and variable
definitions may give rise to completely different instances of the coefficient
matrix when the mathematical program at hand is being generated.

\paragraph{An example: indexed instances}

An illustrative example of such differing instances occurs when the constraints
and variables of a symbolic mathematical program are indexed over a subset of
some other superset. If you let the subset contain a single element of the
superset, the generated instances will be completely different for each element
of the superset. The effect of changing the contents of the subset in this
manner, would almost compare to having an indexed {\tt MathematicalProgram}
{\em declaration} (which AIMMS does not support). In the worked example of
Section~\ref{sec:gmp.examples.indexed} you will see, however, how you can
obtain an indexed collection of generated mathematical program {\em instances}
using the {\gmp} library.

\paragraph{Need for multiple instances}

With the standard {\tt SOLVE} statement (see Section~\ref{sec:mp.solve}) you
only have access to a single generated mathematical program instance for every
symbolic mathematical program, namely the instance associated with the last
call to the {\tt SOLVE} statement for that particular mathematical program.
This effectively eliminates the capability to efficiently implement an
algorithm which requires the interaction between two or more generated
instances of the same symbolic mathematical program. For this reason, the
{\gmp} library allows you to maintain and work with a collection of generated
mathematical program instances simultaneously.

\paragraph{Matrix manipulation}
\index{matrix manipulation}

The {\gmp} library also allows you to manipulate the rows, columns and
coefficients of the matrix of a mathematical program instance once it has been
generated. If the number of modifications is relatively small, manipulating the
matrix directly will save a considerable amount of time compared to letting
AIMMS completely regenerate the matrix again through the standard {\tt
SOLVE} statement. You can use matrix manipulation, for instance
\begin{itemize}
\item to quickly add columns, and adapt the existing rows of the matrix accordingly, in a column
generation scheme, or
\item to dynamically add cuts to a mixed integer linear program.
\end{itemize}

\paragraph{Keeping multiple solutions}

With the standard {\tt SOLVE} statement, you only have access to a single
solution of a mathematical program, namely the one stored in the symbolic
variables and constraints that make up the mathematical program. There are,
however, many situations where it would be convenient to have access to a
repository of solutions. A solution repository can be used, for instance
\begin{itemize}
\item to store a collection of starting solutions for a NLP or MINLP problem.
Solving the problem, in either a serial or parallel manner, with each of these
starting solutions may help you find a better solution than by simply solving
the problem with only a single starting solution.
\item during the solution process of a mixed integer program, if you are
interested in other integer solutions than the final solution returned by the
solver. You can use the solution repository to store a fixed size collection of
the best incumbent solutions returned by the solver during the solution
process.
\end{itemize}

\paragraph{Solution repository}
\index{solution repository}

The {\gmp} library comes with a solution repository for each generated
mathematical program instance, and offers a number of functions to easily
transfer a solution from and to either
\begin{itemize}
\item the data of the variables and constraints that make up the associated mathematical program in your model, or
\item any solver session (explained below) associated with the generated mathematical program instance.
\end{itemize}
In fact, in the {\gmp} library there is no direct solution/starting point
transfer between a solver and the model, but such transfer always takes place
through the solution repository.

\paragraph{Solver session pool}
\index{solver session}

The final concept that is part of the {\gmp} library is that of solver
sessions. In principle, the {\gmp} library is prepared to allow a generated
mathematical program instance to keep a pool of associated solver sessions,
each possibly set up with a different solver, or with different solver
settings, and to be run either locally or remotely.

\paragraph{When useful}

Using multiple solver session it becomes possible, for
example, to let the same (or another) solver with different solver settings solve a
mixed integer program instance in parallel, and pass tighter bound information
found by one solver session to the other sessions by means of a callback
implemented in your model.

\paragraph{{\tt GMP} namespace}

To prevent naming conflicts, all functions and procedure in the {\gmp} library
are member of the predefined {\tt GMP} namespace. The {\tt GMP} namespace is
further partitioned into the namespaces
\begin{itemize}
\item {\tt GMP::Instance},
\item {\tt GMP::Row},
\item {\tt GMP::Column},
\item {\tt GMP::Coefficient},
\item {\tt GMP::Event},
\item {\tt GMP::QuadraticCoefficient},
\item {\tt GMP::Solution},
\item {\tt GMP::SolverSession},
\item {\tt GMP::Stochastic},
\item {\tt GMP::Robust},
\item {\tt GMP::Benders},
\item {\tt GMP::Linearization}, and
\item {\tt GMP::ProgressWindow}.
\end{itemize}
In the following sections we will discuss the procedures and functions
contained in each of these namespaces.

\paragraph{Return values}

When using the {\gmp} library, it may be particularly important to check for
any kind of error conditions that can occur. To help you catch such errors, the
procedures and functions in the {\tt GMP} namespace either return
\begin{itemize}
\item a 1 when successful, or 0 otherwise (for procedures), or
\item a non-empty element in one of the {\gmp}-related
predefined sets when successful, or the empty element otherwise (for
functions).
\end{itemize}
Note that, for the sake of brevity, most of the examples in this chapter do not
perform error checking of any kind.
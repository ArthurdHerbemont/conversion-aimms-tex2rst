\section{Customizing the progress window}\label{sec:gmp.progress}

\paragraph{Customizing the progress window}

When you are using the {\gmp} library to implement a customized algorithm for a
particular problem or problem class, you can use the procedures in the {\tt
GMP::ProgressWindow} namespace to customize the contents of the AIMMS
Progress Window. This allows you to provide customized feedback to the end-user
regarding the progress of the overall solution algorithm, or to provide simultaneous progress information about multiple solver session executing in parallel.

\paragraph{Customizing progress}

The procedures and functions of the {\tt GMP::ProgressWindow} namespace are
listed in Table~\ref{table:gmp.progress}. They allow you to modify every aspect
of the solver part of the AIMMS progress window.
\begin{aimmstable}
\procindexns{GMP::ProgressWindow}{DisplaySolver}
\procindexns{GMP::ProgressWindow}{DisplayLine}
\procindexns{GMP::ProgressWindow}{DisplayProgramStatus}
\procindexns{GMP::ProgressWindow}{DisplaySolverStatus}
\procindexns{GMP::ProgressWindow}{DeleteCategory}
\procindexns{GMP::ProgressWindow}{FreezeLine}
\procindexns{GMP::ProgressWindow}{Transfer}
\procindexns{GMP::ProgressWindow}{UnfreezeLine}

\AIMMSlink{GMP::ProgressWindow::DisplaySolver}
\AIMMSlink{GMP::ProgressWindow::DisplayLine}
\AIMMSlink{GMP::ProgressWindow::DisplayProgramStatus}
\AIMMSlink{GMP::ProgressWindow::DisplaySolverStatus}
\AIMMSlink{GMP::ProgressWindow::DeleteCategory}
\AIMMSlink{GMP::ProgressWindow::FreezeLine}
\AIMMSlink{GMP::ProgressWindow::Transfer}
\AIMMSlink{GMP::ProgressWindow::UnfreezeLine}
\size{9}{11.5pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
{\tt DisplaySolver}({\em name}[, {\em Category}])\\
{\tt DisplayLine}({\em lineNr}, {\em title}, {\em value}[, {\em Category}])\\
{\tt DisplayProgramStatus}({\em status}[, {\em Category}][, {\em lineNo}])\\
{\tt DisplaySolverStatus}({\em status}[, {\em Category}][, {\em lineNo}])\\
\hline
{\tt FreezeLine}({\em lineNo}, {\em totalFreeze}[, {\em Category}])\\
{\tt UnfreezeLine}({\em lineNo}[, {\em Category}])\\
\hline
{\tt DeleteCategory}({\em Category})\\
{\tt Transfer}({\em Category}, {\em solverSession})\\
\hline\hline
\end{tabular}
\caption{Procedures and functions in {\tt GMP::ProgressWindow}
namespace}\label{table:gmp.progress}
\end{aimmstable}

\paragraph{Creating a new progress category}

When your model executes multiple solver sessions in parallel, you can request AIMMS to create a new progress {\em category} to display separate solver progress for each solver session in a separate area of the progress window.
Using the function {\tt GMP::Instance::CreateProgressCategory}, you can create a new progress category for a specific mathematical program instance that will subsequently be used to display solver progress for {\em all} solver sessions associated with that mathematical program instance. Alternatively, you can create a per-session category to display separate solver progress for every single solver session using the function {\tt GMP::SolverSession::CreateProgressCategory}.
The procedure {\tt GMP::ProgressWindow::Transfer} allows you to share a progress category among several solver sessions.
Through the function {\tt GMP::ProgressWindow::De\-le\-te\-Category} you can delete progress categories created by either function.

\paragraph{Freezing category content}

Through the functions {\tt GMP::ProgressWindow::FreezeLine} and {\tt GMP::Progress\-Win\-dow::UnfreezeLine} you can instruct AIMMS to stop and start updating particular areas of the solver progress area associated with the progress category.

\paragraph{Displaying custom content}

When you are writing a custom algorithm you can use the progress window to display custom progress information supplied by you using the functions
\begin{itemize}
\item {\tt GMP::ProgressWindow::DisplaySolver},
\item {\tt GMP::ProgressWindow::DisplayLine},
\item {\tt GMP::ProgressWindow::DisplayProgramStatus}, and
\item {\tt GMP::ProgressWindow::DisplaySolverStatus}.
\end{itemize}
When your custom algorithm consists of a sequence of solves, you can use these functions, for instance, to display custom progress information for the overall algorithm, possibly in combination with regular progress for the underlying solves in a separate category.

\paragraph{Example of use}

An example of the usage of the {\tt GMP::ProgressWindow} can be found in the AIMMS module containing the {\sc gmp} Outer Approximation algorithm discussed in
Section~\ref{sec:aoa.impl}. In this module, the contents of the AIMMS progress window is adapted for the AIMMS Outer Approximation solver.


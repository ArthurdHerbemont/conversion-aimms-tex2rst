\section{Matrix manipulation procedures}\label{sec:gmp.matrix}
\index{matrix manipulation} \index{mathematical program!matrix manipulation}

\paragraph{Matrix manipulation}

The matrix manipulation procedures in the {\gmp} library allow you to implement
efficient algorithms for generated mathematical program instances which require
only slight modifications of the matrix associated with the mathematical
program instance during successive runs. These procedures operate directly on
the coefficient matrix underlying the mathematical program, and thus avoid the
constraint-generation process normally initiated by the {\tt SOLVE} statement
after input data has been modified.

\paragraph{This section}

Prior to discussing the individual matrix manipulation procedures, the
following section will provide some motivation  when and when not to use matrix
manipulation.

\subsection{When to use matrix manipulation}

\paragraph{When to use matrix manipulation}
\index{use of!matrix manipulation}

Even though AIMMS offers a library of matrix manipulation procedures, you
should not use them blindly. As explained below, it is important to distinguish
between manual and automatic input data changes inside an AIMMS application.
Your decision whether or not to use the matrix manipulation procedures
described in this section, should depend on this distinction.

\paragraph{Manual data input \dots}

Consider an end-user of an AIMMS application who, after having looked at the
results of a mathematical program, wants to make changes in the input data and
then look again at the new solution of the mathematical program. The effect of
the data changes on the input to the solver cannot be predicted in advance.
Even a single data change could lead to multiple changes in the input to the
solver, and could also cause a change in the number of constraints and
variables inside the particular mathematical program.

\paragraph{\dots requires structure recognition}

As a result, AIMMS has to determine whether or not the structure of the
underlying mathematical program has changed. Only then can AIMMS decide
whether the value of existing coefficients can be overwritten, or whether a new
and structurally different data set has to be provided to the solver. This
structure recognition step is time consuming, and cannot be avoided in the
absence of any further information concerning the changes in input data.

\paragraph{Automatic data input \dots}

Whenever input data are changed inside an AIMMS procedure, their effect on
the input to the solver can usually be determined in advance. This effect may
be nontrivial, in which case it is not worth the effort to establish the
consequences. Rather, letting AIMMS perform the required structure
recognition step through the regular {\tt SOLVE} statement before passing new
information to the solver seems to be a better remedy. There are several
instances, however, in which the effect of data changes on the solver input
data is easy to determine.

\paragraph{\dots may reflect particular structure}

Consider, for instance, automatic data changes that have a one-to-one
correspondence with values in the underlying mathematical program. In these
instances, the incidence of variables in constraints is not modified, and only
the replacement values of some coefficients need to be supplied to the
particular solver. Other examples include automatic data changes that could
create new values for particular variable-constraint combinations, or that
could even cause new constraints or variables to be added to the input of the
solver. In all these instances, the exact effects on the input of the solver
can easily be determined in advance, and there is no need to let AIMMS
perform of the computationally intensive structure recognition step of the {\tt
SOLVE} statement before passing new information to the solver.

\paragraph{Restrictions on usage}

The above effects of data input modifications on the input to the solver are
straightforward to implement with linear and quadratic mathematical programs,
because the underlying data structures are matrices with rows, columns and
nonzero elements. The input data structures for nonlinear mathematical programs
are essentially nonlinear expressions. Modifications of the type discussed in
the previous paragraph are not easily passed onto these nonlinear data
structures. For this reason, the efficient updating of solver input has been
confined to
\begin{itemize}
\item linear and quadratic constraints, and
\item coefficients of nonlinear constraints with respect to variables that only occur
linearly in that constraint.
\end{itemize}

\paragraph{Regeneration of nonlinear constraints}

Whenever the input data of a nonlinear expression in a nonlinear constraint has
changed, it is not possible anymore to change the nonlinear expression used by
the solver directly to reflect the data change. You can still request AIMMS
to regenerate the entire row, which will then use the updated inputs. You
should note, however, that any modifications to the linear part of the
regenerated constraint are lost after the constraint has been regenerated.

\paragraph{Scalar arguments only}

All matrix procedures listed in
Tables~\ref{table:gmp.coefficient}--\ref{table:gmp.column} have scalar-valued
arguments. The {\em row} argument should always be 
\begin{itemize}
\item a scalar reference to an existing constraint name in your model, or
\item a row number which is an integer in the range $\{ 0 .. m-1 \}$ whereby $m$ is the number of rows.
\end{itemize}
The {\em column} argument should always be
\begin{itemize}
\item a scalar reference to an existing variable name in your model, or
\item a column number which is an integer in the range $\{ 0 .. n-1 \}$ whereby $n$ is the number of columns. 
\end{itemize}

%Thus, any
%indices used in the expressions for the {\em row}, {\em column} and {\em value}
%arguments should be bound prior to calling any of these procedures.

\paragraph{Mathematical program instance required}
\statindex{SOLVE}

Before you can apply any of the procedures of
Tables~\ref{table:gmp.coefficient}--\ref{table:gmp.column}, you must first
create a mathematical program instance using any of the functions for this
purpose discussed in Section~\ref{sec:gmp.instance}. Either of these methods
will set up the initial row-column matrix required by the matrix manipulation
procedures. Also, any row or column referenced in the matrix manipulation
procedures must either have been generated during the initial generation step,
or must have been generated later on by a call to the procedures {\tt
GMP::Row::Add}, or {\tt GMP::Column::Add}, respectively.

\subsection{Coefficient modification procedures}\label{sec:gmp.matrix.coefficient}

\paragraph{Coefficient modification procedures}

The procedures and functions of the {\tt GMP::Coefficient} namespace are listed
in Table~\ref{table:gmp.coefficient} and take care of the modification of
coefficients in the matrix and objective of a generated mathematical program
instance.
\begin{aimmstable}
\funcindexns{GMP::Coefficient}{Get} \procindexns{GMP::Coefficient}{Set}
\procindexns{GMP::Coefficient}{SetMulti}
\funcindexns{GMP::Coefficient}{GetQuadratic}
\procindexns{GMP::Coefficient}{SetQuadratic} \AIMMSlink{GMP::Coefficient::Get}
\AIMMSlink{GMP::Coefficient::Set} \AIMMSlink{GMP::Coefficient::SetMulti}
\AIMMSlink{GMP::Coefficient::GetQuadratic} \AIMMSlink{GMP::Coefficient::SetQuadratic}
\size{9}{11.5pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
{\tt Get}({\em GMP}, {\em row}, {\em column})\\
{\tt Set}({\em GMP}, {\em row}, {\em column}, {\em value})\\
{\tt SetMulti}({\em GMP}, {\em binding}, {\em row}, {\em column}, {\em value})\\
{\tt GetQuadratic}({\em GMP}, {\em column1}, {\em column2})\\
{\tt SetQuadratic}({\em GMP}, {\em column1}, {\em column2}, {\em value})\\
\hline\hline
\end{tabular}
\caption{Procedures and functions in {\tt GMP::Coefficient}
namespace}\label{table:gmp.coefficient}
\end{aimmstable}

\paragraph{Modifying coefficients}

You can instruct AIMMS to modify any particular coefficient in a matrix by
specifying the corresponding row and column (in AIMMS notation), together
with the new value of that coefficient, as arguments of the procedure {\tt
GMP::Coeffici\-ent::Set}. This procedure can also be used when a value for the
coefficient does not exist prior to calling the procedure. If many coeffients
for a row or column have to be changed then it is more efficient to use
{\tt GMP::Coef\-fi\-ci\-ent::Set\-Multi}.

%\paragraph{Tags are optional}
%\index{argument!tag}
%
%The argument names listed in Table~\ref{table:gmp.coefficient} can also be used
%as tags in any call to one of the procedures listed. For instance, the
%following two calls are valid and identical.
%\begin{example}
%    GMP::Coefficient::Set( LinearizedProgram, ObjectiveRow, x(j), ObjCoeff(j));
%
%    GMP::Coefficient::Set( GMP    : LinearizedProgram,
%                           row    : ObjectiveRow,
%                           column : x(j),
%                           value  : ObjCoeff(j) );
%\end{example}
%Because the arguments to all matrix manipulation procedures should be scalar,
%both calls to {\tt GMP::Coefficient::Set} should be surrounded by a {\tt FOR}
%statement that binds the index {\tt j}, when called within a procedure of your
%model.

\paragraph{Quadratic coefficients}

For quadratic mathematical programs, you can modify the quadratic objective
coefficients by applying the function {\tt GMP::Coefficient::SetQuadratic} to
the objective row. For every two columns $x_1$ and $x_2$ you can specify the
modified coefficient $c_{12}$ if $c_{12}x_1x_2$ is to be part of the quadratic
objective.

\subsection{Quadratic coefficient modification procedures}\label{sec:gmp.matrix.quadratic}

\paragraph{Quadratic coefficient modification procedures}

The procedures and functions of the {\tt GMP::QuadraticCoefficient} namespace
are listed in Table~\ref{table:gmp.quadratic} and take care of the modification
of coefficients of quadratic rows in the matrix other than the objective of a
generated mathematical program instance.
\begin{aimmstable}
\funcindexns{GMP::QuadraticCoefficient}{Get}
\procindexns{GMP::QuadraticCoefficient}{Set}
\AIMMSlink{GMP::QuadraticCoefficient::Get}
\AIMMSlink{GMP::QuadraticCoefficient::Set}
\size{9}{11.5pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
{\tt Get}({\em GMP}, {\em row}, {\em column1}, {\em column2})\\
{\tt Set}({\em GMP}, {\em row}, {\em column1}, {\em column2}, {\em value})\\
\hline\hline
\end{tabular}
\caption{Procedures and functions in {\tt GMP::QuadraticCoefficient}
namespace}\label{table:gmp.quadratic}
\end{aimmstable}

\paragraph{Modifying coefficients}

You can instruct AIMMS to modify any particular quadratic coefficient in a
matrix by specifying the corresponding row and columns (in AIMMS notation),
together with the new value of that coefficient, as arguments of the procedure
{\tt GMP::QuadraticCoeffici\-ent::Set}. This procedure can also be used when a
value for the quadratic coefficient does not exist prior to calling the
procedure.

\subsection{Row modification procedures}\label{sec:gmp.matrix.row}

\paragraph{Row modification procedures}

The procedures and functions of the {\tt GMP::Row} namespace are listed in
Table~\ref{table:gmp.row} and take care of the modification of properties of
existing rows and the creation of new rows.
\begin{aimmstable}
\procindexns{GMP::Row}{Add} \procindexns{GMP::Row}{Delete}
\procindexns{GMP::Row}{Activate} \procindexns{GMP::Row}{Deactivate}
\procindexns{GMP::Row}{Generate} \funcindexns{GMP::Row}{GetLeftHandSide}
\procindexns{GMP::Row}{SetLeftHandSide}
\funcindexns{GMP::Row}{GetRightHandSide}
\procindexns{GMP::Row}{SetRightHandSide}
\procindexns{GMP::Row}{SetRightHandSideMulti} \funcindexns{GMP::Row}{GetType}
\procindexns{GMP::Row}{SetType}
\procindexns{GMP::Row}{DeleteIndicatorCondition}
\procindexns{GMP::Row}{GetIndicatorColumn}
\procindexns{GMP::Row}{GetIndicatorCondition}
\procindexns{GMP::Row}{SetIndicatorCondition}
\procindexns{GMP::Row}{GetConvex}
\procindexns{GMP::Row}{GetRelaxationOnly}
\procindexns{GMP::Row}{SetConvex}
\procindexns{GMP::Row}{SetRelaxationOnly}
\funcindexns{GMP::Row}{GetStatus}
\procindexns{GMP::Row}{SetPoolType}
\procindexns{GMP::Row}{SetPoolTypeMulti}
\AIMMSlink{GMP::Row::Add}
\AIMMSlink{GMP::Row::Delete} \AIMMSlink{GMP::Row::Activate}
\AIMMSlink{GMP::Row::Deactivate} \AIMMSlink{GMP::Row::Generate}
\AIMMSlink{GMP::Row::GetLeftHandSide} \AIMMSlink{GMP::Row::SetLeftHandSide}
\AIMMSlink{GMP::Row::GetRightHandSide} \AIMMSlink{GMP::Row::SetRightHandSide}
\AIMMSlink{GMP::Row::SetRightHandSideMulti}
\AIMMSlink{GMP::Row::GetType} \AIMMSlink{GMP::Row::SetType}
\AIMMSlink{GMP::Row::DeleteIndicatorCondition}
\AIMMSlink{GMP::Row::GetIndicatorColumn}
\AIMMSlink{GMP::Row::GetIndicatorCondition}
\AIMMSlink{GMP::Row::SetIndicatorCondition}
\AIMMSlink{GMP::Row::GetConvex}
\AIMMSlink{GMP::Row::GetRelaxationOnly}
\AIMMSlink{GMP::Row::SetConvex}
\AIMMSlink{GMP::Row::SetRelaxationOnly}
\AIMMSlink{GMP::Row::GetStatus}
\AIMMSlink{GMP::Row::SetPoolType}
\AIMMSlink{GMP::Row::SetPoolTypeMulti}
\size{9}{11.5pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
{\tt Add}({\em GMP}, {\em row})\\
{\tt Delete}({\em GMP}, {\em row})\\
{\tt Activate}({\em GMP}, {\em row})\\
{\tt Deactivate}({\em GMP}, {\em row})\\
\hline
{\tt Generate}({\em GMP}, {\em row})\\
\hline
{\tt GetLeftHandSide}({\em GMP}, {\em row})\\
{\tt SetLeftHandSide}({\em GMP}, {\em row}, {\em value})\\
{\tt GetRightHandSide}({\em GMP}, {\em row})\\
{\tt SetRightHandSide}({\em GMP}, {\em row}, {\em value})\\
{\tt SetRightHandSideMulti}({\em GMP}, {\em binding}, {\em row}, {\em value})\\
\hline
{\tt GetType}({\em GMP}, {\em row}) $\to$ {\tt AllRowTypes}\\
{\tt SetType}({\em GMP}, {\em row}, {\em type})\\
{\tt GetStatus}({\em GMP}, {\em row}) $\to$ {\tt AllRowColumnStatuses}\\
\hline
{\tt DeleteIndicatorCondition}({\em GMP}, {\em row})\\
{\tt GetIndicatorColumn}({\em GMP}, {\em row})\\
{\tt GetIndicatorCondition}({\em GMP}, {\em row})\\
{\tt SetIndicatorCondition}({\em GMP}, {\em row}, {\em column}, {\em value})\\
\hline
{\tt GetConvex}({\em GMP}, {\em row})\\
{\tt GetRelaxationOnly}({\em GMP}, {\em row})\\
{\tt SetConvex}({\em GMP}, {\em row}, {\em value})\\
{\tt SetRelaxationOnly}({\em GMP}, {\em row}, {\em value})\\
\hline
{\tt SetPoolType}({\em GMP}, {\em row}, {\em value}[, {\em mode}])\\
{\tt SetPoolTypeMulti}({\em GMP}, {\em binding}, {\em row}, {\em value}, {\em mode})\\
\hline\hline
\end{tabular}
\caption{Procedures and functions in {\tt GMP::Row}
namespace}\label{table:gmp.row}
\end{aimmstable}

\paragraph{Row types}

The row type refers to one of the four possibilities
\begin{itemize}
\item \verb|'<='|,
\item \verb|'='|,
\item \verb|'>='|, and
\item {\tt 'ranged'}
\end{itemize}
You are free to change this type for each row. Deactivating and subsequently
reactivating a row are instructions to the solver to ignore the row as part of
the underlying mathematical program and then reconsider the row again as an
active row.

\paragraph{Row generation}

When you add a new row to a matrix using {\tt GMP::Row::Add}, the newly added
row will initially only have any zero coefficients, regardless of whether the
corresponding AIMMS constraint had a definition or not. Through the
procedure {\tt GMP::Row::Generate} you can tell AIMMS to discard the current
contents of a row in the matrix, and insert the coefficients as they follow
from the definition of the corresponding constraint in your model.

\paragraph{Indicator conditions}
\index{constraint!indicator}
\index{indicator constraint}

When you are using the {\sc Cplex}, {\sc Gurobi} or {\sc Odh-Cplex} solver, you can declaratively specify indicator constraints through the {\tt IndicatorConstraint} property of a constraint declaration (see Section~\ref{sec:var.constr.indicator}). You can also set and delete indicator constraints programmatically for a given {\em GMP} using the functions {\tt GMP::Row::Set\-Indicator\-Condition} and {\tt GMP::Row::Delete\-Indicator\-Condition}

\paragraph{Lazy and cut pool constraints}
\index{constraint!lazy}
\index{lazy constraint}
\index{constraint!user cut}
\index{user cut pool}

When you are using the {\sc Cplex}, {\sc Gurobi} or {\sc Odh-Cplex} solver, you can declaratively specify constraints to be part of a pool of lazy constraints or cuts through the {\tt Include\-In\-Lazy\-Constraint\-Pool} and {\tt Include\-In\-Cut\-Pool} properties of a constraint declaration respectively (see Section~\ref{sec:var.constr.indicator}). You can also specify lazy and cut pool constraints programmatically for a given {\em GMP} using the function {\tt GMP::Row::Set\-Pool\-Type}.
(If the pool type has to be set for many rows then it is more efficient to use {\tt GMP::Row::Set\-Pool\-Type\-Multi}.)

\paragraph{Convex and relaxation-only constraints}
\suffindex{constraint}{Convex}\suffindex{constraint}{RelaxationOnly}

Through the {\tt .Convex} and {\tt .RelaxationOnly} suffices of constraints you can set special constraint properties for the {\sc Baron} global optimization solver (see also Section~\ref{sec:var.constr.glob-suff}). For a given {\em GMP} you can also set these constraint properties programmatically using the {\tt GMP::Row::SetConvex} and {\tt GMP::Row::SetRelax\-ation\-Only} functions.


\subsection{Column modification procedures}\label{sec:gmp.matrix.column}

The procedures and functions of the {\tt GMP::Column} namespace are listed in
Table~\ref{table:gmp.column} and take care of the modification of properties of
existing columns and the creation of new columns.

\begin{aimmstable}
\procindexns{GMP::Column}{Add} \procindexns{GMP::Column}{Delete}
\procindexns{GMP::Column}{Freeze} \procindexns{GMP::Column}{FreezeMulti}
\procindexns{GMP::Column}{Unfreeze} \procindexns{GMP::Column}{UnfreezeMulti}
\procindexns{GMP::Column}{SetDecomposition} \procindexns{GMP::Column}{SetDecompositionMulti}
\funcindexns{GMP::Column}{GetLowerBound}
\procindexns{GMP::Column}{SetLowerBound} \procindexns{GMP::Column}{SetLowerBoundMulti}
\funcindexns{GMP::Column}{GetUpperBound}
\procindexns{GMP::Column}{SetUpperBound} \procindexns{GMP::Column}{SetUpperBoundMulti}
\funcindexns{GMP::Column}{GetType}
\procindexns{GMP::Column}{SetType} \procindexns{GMP::Column}{SetAsObjective}
\procindexns{GMP::Column}{SetAsMultiObjective}
\funcindexns{GMP::Column}{GetStatus}
\AIMMSlink{GMP::Column::Add} \AIMMSlink{GMP::Column::Delete}
\AIMMSlink{GMP::Column::Freeze} \AIMMSlink{GMP::Column::FreezeMulti}
\AIMMSlink{GMP::Column::Unfreeze} \AIMMSlink{GMP::Column::UnfreezeMulti}
\AIMMSlink{GMP::Column::SetDecomposition} \AIMMSlink{GMP::Column::SetDecompositionMulti}
\AIMMSlink{GMP::Column::GetLowerBound}
\AIMMSlink{GMP::Column::SetLowerBound} \AIMMSlink{GMP::Column::SetLowerBoundMulti}
\AIMMSlink{GMP::Column::GetUpperBound}
\AIMMSlink{GMP::Column::SetUpperBound} \AIMMSlink{GMP::Column::SetUpperBoundMulti}
\AIMMSlink{GMP::Column::GetType} \AIMMSlink{GMP::Column::SetType}
\AIMMSlink{GMP::Column::SetAsObjective} \AIMMSlink{GMP::Column::GetStatus}
\AIMMSlink{GMP::Column::SetAsMultiObjective}
\size{9}{11.5pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
{\tt Add}({\em GMP}, {\em column})\\
{\tt Delete}({\em GMP}, {\em column})\\
{\tt Freeze}({\em GMP}, {\em column}, {\em value})\\
{\tt FreezeMulti}({\em GMP}, {\em binding}, {\em column}, {\em value})\\
{\tt Unfreeze}({\em GMP}, {\em column})\\
{\tt UnfreezeMulti}({\em GMP}, {\em binding}, {\em column})\\
\hline
{\tt GetLowerBound}({\em GMP}, {\em column})\\
{\tt SetLowerBound}({\em GMP}, {\em column}, {\em value})\\
{\tt SetLowerBoundMulti}({\em GMP}, {\em binding}, {\em column}, {\em value})\\
{\tt GetUpperBound}({\em GMP}, {\em column})\\
{\tt SetUpperBound}({\em GMP}, {\em column}, {\em value})\\
{\tt SetUpperBoundMulti}({\em GMP}, {\em binding}, {\em column}, {\em value})\\
\hline
{\tt GetType}({\em GMP}, {\em column}) $\to$ {\tt AllColumnTypes}\\
{\tt SetType}({\em GMP}, {\em column}, {\em type})\\
{\tt GetStatus}({\em GMP}, {\em column}) $\to$ {\tt AllRowColumnStatuses}\\
\hline
{\tt SetDecomposition}({\em GMP}, {\em column}, {\em value})\\
{\tt SetDecompositionMulti}({\em GMP}, {\em binding}, {\em column}, {\em value})\\
\hline
{\tt SetAsObjective}({\em GMP}, {\em column})\\
{\tt SetAsMultiObjective}({\em GMP}, {\em column}, {\em priority}, {\em weight})\\
\hline\hline
\end{tabular}
\caption{Procedures and functions in {\tt GMP::Column}
namespace}\label{table:gmp.column}
\end{aimmstable}

\paragraph{Column types}

The column type refers to one of the three possibilities
\begin{itemize}
\item {\tt 'integer'},
\item {\tt 'continuous'}, and
\item {\tt 'semi-continuous'}.
\end{itemize}
You are free to specify a different type for each column. For newly added
columns, AIMMS will (initially) use the lower bound, upper bound and column
type as specified in the declaration of the (symbolic) variable associated with
the added column. Freezing a column and subsequently unfreezing it are
instructions to the solver to fix the corresponding variable to its current
value, and then free it again by letting it vary between its bounds.

\paragraph{Multi variants}

If you want to change the data of many columns belonging to some variable then
it is more efficient to use the multi variant of a procedure. A multi variant
is available for freezing and unfreezing columns, and for setting the lower and
upper bounds.

\paragraph{Changing the objective column}

If you want to implement the procedures for reaching primal or dual uniqueness
as described in Section~\ref{sec:gmp.instance.dual}, you can use the procedure
\begin{itemize}
\item {\tt GMP::Column::SetAsObjective}
\end{itemize}
to change the objective function used by either the primal or dual mathematical
program instance that you want to solve for a second time. Notice that the
defining constraint for this variable should be
\begin{itemize}
\item part of the original mathematical program formulation for which
AIMMS has generated a mathematical program instance, or
\item added later on to the primal or dual generated mathematical program
instance using the {\tt GMP::Row::Add} procedure, where the row definition is
generated by AIMMS through the {\tt GMP::Row::Generate} procedure or
constructed explicitly through several calls to the {\tt GMP::Coefficient::Set}
procedure.
\end{itemize}

\subsection{Modifying an extended math program instance}\label{sec:matrix.extended}
\index{dual mathematical program!matrix manipulation}\index{matrix
manipulation!dual mathematical program} \index{mathematical
program!manipulating dual}

\suffindex{variable}{ExtendedVariable}\suffindex{variable}{ExtendedConstraint}
\suffindex{constraint}{ExtendedVariable}\suffindex{constraint}{ExtendedConstraint}
\suffindex{mathematical program}{ExtendedConstraint}\suffindex{mathematical
program}{ExtendedVariable}

\paragraph{Extended math program instances}

To use the matrix manipulation routines of the {\gmp} library, you must be able
to associate every row and column of the matrix of the math program instance
you want to manipulate with a symbolic constraint or variable within your
model. However, some routines in the {\gmp} library generate rows and columns
that cannot be directly associated with specific symbolic constraints and
variables in your model. Examples of such routines are:
\begin{itemize}\hfuzz 0.5pt
\item the {\tt GMP::Instance::CreateDual} procedure, which may generate additional variables in the dual formulation for
bounded variables and ranged constraints in the primal formulation (see also
Section~\ref{sec:gmp.instance.dual}),
\item the {\tt GMP::Linearization::Add} and {\tt GMP::Linearization::AddSingle} procedu\-res, which add
linearizations of nonlinear constraints to a specific math program instance
(see also Section~\ref{sec:gmp.lin}), and
\item the {\tt GMP::Instance::AddIntegerEliminationRows} procedure.
\end{itemize}
The rows and columns generated by these procedures can, however, be {\em
indirectly} associated with symbolic constraints, variables or mathematical
programs, as will be explained below.

\paragraph{Extended suffices}

To support the use of the matrix manipulation routines in conjunction with rows
and columns generated by AIMMS that can only be indirectly associated with
symbolic identifers in the model, AIMMS provides the following suffices
which allow you to do so:
\begin{itemize}
\item {\tt .ExtendedVariable}, and
\item {\tt .ExtendedConstraint}.
\end{itemize}
These suffices are supported for {\tt Variables}, {\tt Constraints} and {\tt
MathematicalPro\-grams}. They behave like variables and constraints, which
implies that it is possible to refer to the {\tt .ReducedCost} and {\tt
.ShadowPrice} suffices of these extended suffices to get hold of their
sensitivity information.

\paragraph{Suffix dimensions}
\presetindex{AllGMPExtensions}

Each of the suffices listed above has one additional dimension compared to the
dimension of the original identifier, over the predefined set {\tt
AllGMPExtensions}. For example, assuming that  {\tt ae} is an index into the
set {\tt AllGMPExtensions},
\begin{itemize}
\item if {\tt z(i,j)} is a variable or constrain, the {\tt .ExtendedVariable} suffix will have indices {\tt
z.ExtendedVariable(ae,i,j)},
\item if {\tt mp} is a mathematical program, the {\tt .ExtendedConstraint} suffix will have indices {\tt
mp.ExtendedConstraint(ae)}.
\end{itemize}
Each of the procedures listed above, will add elements to the set {\tt
AllGMPExten\-sions} as necessary. The names of the precise elements added to
the set is explained below in more detail.

\paragraph{Suffices generated by {\tt CreateDual}}

The procedure {\tt GMP::Instance::CreateDual} will add the following elements
to the set {\tt AllGMPExtensions}:
\begin{itemize}
\item {\tt DualObjective}, {\tt DualDefinition}, {\tt DualUpperBound}, {\tt DualLowerBound}.
\end{itemize}
In addition, it will generate the following extended variables and constraints
\begin{itemize}
\item For the mathematical program {\tt mp} at hand
\begin{itemize}
\item the variable {\tt mp.ExtendedVariable('DualDefinition')},
\item the constraint {\tt mp.ExtendedConstraint('DualObjective')}.
\end{itemize}
\item For every ranged constraint {\tt c(i)}
\begin{itemize}
\item the constraint {\tt c.ExtendedConstraint('DualLowerBound',i)},
\item the constraint {\tt c.ExtendedConstraint('DualUpperBound' i)}.
\end{itemize}
\item For every bounded variable {\tt x(i)} in $[l_i,u_i]$
\begin{itemize}
\item the constraint {\tt x.ExtendedConstraint('DualLowerBound',i)}\\ (if $l_i\neq 0,-\infty$),
\item the constraint {\tt x.ExtendedConstraint('DualUpperBound' i)}\\ (if $u_i\neq 0, \infty$).
\end{itemize}
\end{itemize}

\paragraph{Modifying the dual math program}

Using the matrix manipulation procedures, you can modify the matrix or
objective associated with a dual mathematical program instance created by
calling the procedure {\tt GMP::Instance::CreateDual}. Below you will find how
you can access the rows and columns of a dual mathematical program instance
created by AIMMS.

\paragraph{Row and column names}

For each procedure in the {\tt GMP::Coefficient}, {\tt GMP::Row} and {\tt
GMP::Column} name\-spaces you must refer to a scalar constraint and/or variable
reference from your symbolic model. For the dual formulation, you must
\begin{itemize}
\item use the symbolic primal constraint name, to refer to the dual shadow price variable
associated with that constraint in the dual mathematical program instance, and
\item use the symbolic primal variable name, to refer to the dual constraint
associated with that variable in the dual mathematical program instance.
\end{itemize}
In other words, when modifying matrix coefficients, rows or columns the role of
the symbolic constraints and variables is interchanged.

\paragraph{Implicitly added variables and constraints}

You can refer to the implicitly added variables and constraints in the
procedures of the {\tt GMP::Coefficient}, {\tt GMP::Row} and {\tt GMP::Column}
name\-spaces through the {\tt .ExtendedVariable} and {\tt .ExtendedConstraint}
suffices described above. After solving the dual math program, AIMMS will
store the dual solution in the suffices {\tt .ExtendedVariable.ReducedCost} and
{\tt .ExtendedConstraint.ShadowPrice}, respectively.

\paragraph{Extended suffices for linearization}

By calling the procedures {\tt GMP::Linearization:Add} or {\tt
GMP::Linearization::Add\-Single}, AIMMS will add the linearization for a
single nonlinear constraint instance, or for all nonlinear constraints from a
set of nonlinear constraints to a given math program instance. When doing so,
AIMMS will add an element {\tt Linearization}$k$ (where $k$ is a counter) to
the set {\tt AllGMPExtensions}, and will create for each nonlinear constraint
{\tt c(i)}
\begin{itemize}
\item a constraint {\tt c.ExtendedConstraint('Linearization}$k${\tt ',i)}, and
\item a variable {\tt c.ExtendedVariable('Linearization}$k${\tt ',i)} if deviations from the constraint are
permitted (see also Section~\ref{sec:gmp.lin}).
\end{itemize}

\paragraph{Elimination constraints and variables}

By calling the procedure {\tt GMP::Instance::AddIntegerEliminationRows},
AIMMS will add one or more constraints and variables to a math program
instance, which will eliminate the current integer solution from the math
program instance. When called, AIMMS will add elements of the form
\begin{itemize}
\item {\tt Elimination}$k$,
\item {\tt EliminationLowerBound}$k$, and
\item {\tt EliminationUpperBound}$k$
\end{itemize}
to the set {\tt AllGMPExtensions}. In addition, AIMMS will add
\begin{itemize}
\item a constraint {\tt mp.ExtendedConstraint('Linearization}$k${\tt ')} to exclude current solution for all binary variables from the math program {\tt mp} at hand,
and
\item for every integer variable {\tt c(i)} with a level value between its bounds the variables and constraints
\begin{itemize}
\item {\tt c.ExtendedVariable('Elimination}$k${\tt ',i)},
\item {\tt c.ExtendedVariable('EliminationLowerBound}$k${\tt ',i)},
\item {\tt c.ExtendedVariable('EliminationUpperBound}$k${\tt ',i)},
\item {\tt c.ExtendedConstraint('Elimination}$k${\tt ',i)},
\item {\tt c.ExtendedConstraint('EliminationLowerBound}$k${\tt ',i)}, and
\item {\tt c.ExtendedConstraint('EliminationUpperBound}$k${\tt ',i)}.
\end{itemize}
\end{itemize}

%\subsection{Deprecated matrix manipulation procedures}\label{sec:matrix.procedures}
%\index{matrix manipulation}\index{mathematical program!matrix manipulation}
%
%\paragraph{Deprecated old-style procedures}
%
%AIMMS versions prior to version 3.5, also supported a collection of matrix
%manipulation procedures with more limited functionality. Although these
%procedures will remain to be supported for all AIMMS 3.x versions, they have
%become deprecated. The deprecated manipulation procedures and their {\gmp}
%counterparts in AIMMS 3.5 and higher are listed in
%Table~\ref{table:matrix.procedures}.
%\begin{aimmstable}
%\procindex{MatrixModifyCoefficient}
%\procindex{MatrixModifyQuadraticCoefficient}
%\procindex{MatrixModifyRightHandSide} \procindex{MatrixModifyLeftHandSide}
%\procindex{MatrixModifyRowType} \procindex{MatrixAddRow}
%\procindex{MatrixRegenerateRow} \procindex{MatrixDeactivateRow}
%\procindex{MatrixActivateRow} \procindex{MatrixModifyLowerBound}
%\procindex{MatrixModifyUpperBound} \procindex{MatrixModifyColumnType}
%\procindex{MatrixAddColumn} \procindex{MatrixFreezeColumn}
%\procindex{MatrixUnfreezeColumn} \procindex{MatrixModifyType}
%\procindex{MatrixModifyDirection} \procindex{MatrixGenerate}
%\procindex{MatrixSolve} \procindex{MatrixSaveState}
%\procindex{MatrixRestoreState} \AIMMSlink{matrixmodifycoefficient}
%\AIMMSlink{matrixmodifyquadraticcoefficient}
%\AIMMSlink{matrixmodifyrighthandside} \AIMMSlink{matrixmodifylefthandside}
%\AIMMSlink{matrixmodifyrowtype} \AIMMSlink{matrixaddrow}
%\AIMMSlink{matrixregeneraterow} \AIMMSlink{matrixdeactivaterow}
%\AIMMSlink{matrixactivaterow} \AIMMSlink{matrixmodifylowerbound}
%\AIMMSlink{matrixmodifyupperbound} \AIMMSlink{matrixmodifycolumntype}
%\AIMMSlink{matrixaddcolumn} \AIMMSlink{matrixfreezecolumn}
%\AIMMSlink{matrixunfreezecolumn} \AIMMSlink{matrixmodifytype}
%\AIMMSlink{matrixmodifydirection} \AIMMSlink{matrixgenerate}
%\AIMMSlink{matrixsolve} \AIMMSlink{matrixsavestate}
%\AIMMSlink{matrixrestorestate} \size{8}{9pt}\selectfont
%\begin{tabular}{|l|l|}
%\hline\hline
%{\bf Deprecated procedure} & {\bf {\GMP} counterpart}\\
%\hline
%{\tt MatrixModifyCoefficient} & {\tt GMP::Coefficient::Set}\\
%{\tt MatrixModifyQuadraticCoefficient} & {\tt GMP::Coefficient::SetQuadratic }\\
%\hline
%{\tt MatrixModifyRightHandSide} & {\tt GMP::Row::SetRightHandSide}\\
%{\tt MatrixModifyLeftHandSide} & {\tt GMP::Row::SetLeftHandSide}\\
%{\tt MatrixModifyRowType} & {\tt GMP::Row::SetType}\\
%{\tt MatrixAddRow} & {\tt GMP::Row::Add}\\
%{\tt MatrixRegenerateRow} & {\tt GMP::Row::Generate}\\
%{\tt MatrixDeactivateRow} & {\tt GMP::Row::Deactivate}\\
%{\tt MatrixActivateRow} & {\tt GMP::Row::Activate}\\
%\hline
%{\tt MatrixModifyLowerBound} & {\tt GMP::Column::SetLowerBound}\\
%{\tt MatrixModifyUpperBound} & {\tt GMP::Column::SetUpperBound}\\
%{\tt MatrixModifyColumnType} & {\tt GMP::Column::SetType}\\
%{\tt MatrixAddColumn} & {\tt GMP::Column::Add}\\
%{\tt MatrixFreezeColumn} & {\tt GMP::Column::Freeze}\\
%{\tt MatrixUnfreezeColumn} & {\tt GMP::Column::Unfreeze}\\
%\hline
%{\tt MatrixModifyType} & {\tt GMP::Instance::SetMathematicalProgrammingType}\\
%{\tt MatrixModifyDirection} & {\tt GMP::Instance::SetDirection}\\
%\hline
%{\tt MatrixGenerate} & {\tt GMP::Instance::Generate}\\
%{\tt MatrixSolve} & {\tt GMP::Instance::Solve}, {\tt GMP::SolverSession::Execute}\\
%\hline
%{\tt MatrixSaveState} & {\tt GMP::Instance::Copy}\\
%{\tt MatrixRestoreState} & {\tt GMP::Instance::Copy}\\
%\hline\hline
%\end{tabular}
%\caption{Deprecated matrix manipulation
%procedures}\label{table:matrix.procedures}
%\end{aimmstable}
%
%\paragraph{Matrix state\\ no longer supported}
%
%Through the old-style matrix manipulation procedures {\tt MatrixSaveState} and
%{\tt MatrixRestoreState} it was possible to save the state of the matrix at a
%certain point in time, and restore the matrix to this state at a later time to
%undo later matrix modifications. This functionality is no longer supported in
%the {\gmp} library. As an alterative, you can create a copy of the particular
%mathematical program instance for which you want to save the current state, and
%restore the saved state using this copy.

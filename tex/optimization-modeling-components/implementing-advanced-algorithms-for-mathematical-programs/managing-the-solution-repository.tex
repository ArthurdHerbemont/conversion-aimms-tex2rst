\section{Managing the solution repository}\label{sec:gmp.solution}
\index{mathematical program!solution repository} \index{solution repository}

\paragraph{The solution repository}

The {\gmp} library maintains a solution repository for every generated
mathematical program instance. You can use this repository, for instance, to
store
\begin{itemize}
\item a number of starting solutions for a NLP problem to be solved
successively,
\item a number of incumbent solutions as reported by a MIP solver, or
\item let a solver store multiple solutions.
\end{itemize}
If you are using solver sessions to initiate a solver, you must explicitly
transfer the initial, intermediate or final solutions between the model, the
solution repository and the solver session. As discussed in
Section~\ref{sec:gmp.instance}, the function {\tt GMP::Instance::Solve} performs these
necessary solution transfer steps for you, and uses the fixed solution number~1
for all of its communication.

Some solvers are capable of finding multiple solutions instead of at most one.
Examples of such solvers are {\sc Baron}, {\sc Cplex} and {\sc Gurobi}.
When such a solver finds multiple solutions, these solutions are stored in the solution repository from number~1 on upwards.
The control mechanism to let solvers find multiple solutions is solver specific:
\begin{itemize}
\item {\tt BARON 19}: For more information see the {\bf Help} file for option {\tt Number of best solutions} in option category
                {\tt Specific solvers} -- {\tt BARON 19} -- {\tt General}.
\item {\tt CPLEX 12.9}: For more information see the {\bf Help} file for option {\tt Do~populate} in option category
                {\tt Specific solvers} -- {\tt CPLEX 12.9} -- {\tt MIP solution pool}.
\item {\tt GUROBI 8.1}: For more information see the {\bf Help} file for option {\tt Pool search mode} in option category
                {\tt Specific solvers} -- {\tt GUROBI 8.1} -- {\tt Solution pool}.
\end{itemize}


\paragraph{Solution repository functions}

The procedures and functions of the {\tt GMP::Solution} namespace are listed in
Table~\ref{table:gmp.solution}. Through these functions you can
\begin{itemize}
\item
transfer a solution between the solution repository on the one side and the
symbolic model or the solver on the other side,
\item obtain and set solution properties of a solution in the repository, or
\item perform a feasibility check on a solution in the repository.
\end{itemize}
\begin{aimmstable}
\procindexns{GMP::Solution}{Copy} \procindexns{GMP::Solution}{Move}
\procindexns{GMP::Solution}{Delete} \procindexns{GMP::Solution}{DeleteAll}
\funcindexns{GMP::Solution}{GetSolutionsSet}
\procindexns{GMP::Solution}{SolutionCount}
\procindexns{GMP::Solution}{RetrieveFromModel}
\procindexns{GMP::Solution}{SendToModel}
\procindexns{GMP::Solution}{RetrieveFromSolverSession}
\procindexns{GMP::Solution}{SendToSolverSession}
\funcindexns{GMP::Solution}{GetObjective}
\procindexns{GMP::Solution}{SetObjective}
\funcindexns{GMP::Solution}{GetProgramStatus}
\funcindexns{GMP::Solution}{SetProgramStatus}
\funcindexns{GMP::Solution}{GetSolverStatus}
\funcindexns{GMP::Solution}{SetSolverStatus} \procindexns{GMP::Solution}{Check}
\procindexns{GMP::Solution}{SetIterationCount}
\procindexns{GMP::Solution}{IsInteger}
\procindexns{GMP::Solution}{IsDualDegenerated}
\procindexns{GMP::Solution}{IsPrimalDegenerated}
\procindexns{GMP::Solution}{Count}
\procindexns{GMP::Solution}{GetBestBound}
\procindexns{GMP::Solution}{GetIterationsUsed}
\procindexns{GMP::Solution}{GetMemoryUsed}
\procindexns{GMP::Solution}{GetTimeUsed}
\procindexns{GMP::Solution}{SendToModelSelection}
\procindexns{GMP::Solution}{SetSolverStatus}
\procindexns{GMP::Solution}{GetFirstOrderDerivative}
\funcindexns{GMP::Solution}{GetColumnValue}
\procindexns{GMP::Solution}{SetColumnValue}
\funcindexns{GMP::Solution}{GetRowValue}
\procindexns{GMP::Solution}{SetRowValue}
\procindexns{GMP::Solution}{ConstraintListing}
\AIMMSlink{GMP::Solution::Copy} \AIMMSlink{GMP::Solution::Move}
\AIMMSlink{GMP::Solution::Delete} \AIMMSlink{GMP::Solution::DeleteAll}
\AIMMSlink{GMP::Solution::GetSolutionsSet}
\AIMMSlink{GMP::Solution::SolutionCount}
\AIMMSlink{GMP::Solution::RetrieveFromModel}
\AIMMSlink{GMP::Solution::SendToModel}
\AIMMSlink{GMP::Solution::RetrieveFromSolverSession}
\AIMMSlink{GMP::Solution::SendToSolverSession}
\AIMMSlink{GMP::Solution::GetObjective}
\AIMMSlink{GMP::Solution::SetObjective}
\AIMMSlink{GMP::Solution::GetProgramStatus}
\AIMMSlink{GMP::Solution::SetProgramStatus}
\AIMMSlink{GMP::Solution::SetSolverStatus}
\AIMMSlink{GMP::Solution::GetSolverStatus}
\AIMMSlink{GMP::Solution::Check}
\AIMMSlink{GMP::Solution::SetIterationCount}
\AIMMSlink{GMP::Solution::IsInteger}
\AIMMSlink{GMP::Solution::IsDualDegenerated}
\AIMMSlink{GMP::Solution::IsPrimalDegenerated}
\AIMMSlink{GMP::Solution::Count}
\AIMMSlink{GMP::Solution::GetBestBound}
\AIMMSlink{GMP::Solution::GetIterationsUsed}
\AIMMSlink{GMP::Solution::GetMemoryUsed}
\AIMMSlink{GMP::Solution::GetTimeUsed}
\AIMMSlink{GMP::Solution::SendToModelSelection}
\AIMMSlink{GMP::Solution::GetFirstOrderDerivative}
\AIMMSlink{GMP::Solution::GetColumnValue}
\AIMMSlink{GMP::Solution::SetColumnValue}
\AIMMSlink{GMP::Solution::GetRowValue}
\AIMMSlink{GMP::Solution::SetRowValue}
\AIMMSlink{GMP::Solution::ConstraintListing}
\size{9}{11.5pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
{\tt Copy}({\em GMP}, {\em fromSol}, {\em toSol})\\
{\tt Move}({\em GMP}, {\em fromSol}, {\em toSol})\\
{\tt Delete}({\em GMP}, {\em solNo})\\
{\tt DeleteAll}({\em GMP})\\
\hline
{\tt GetSolutionsSet}({\em GMP})$\to${\tt Integers}\\
{\tt Count}({\em GMP})\\
\hline
{\tt RetrieveFromModel}({\em GMP}, {\em SolNr})\\
{\tt SendToModel}({\em GMP}, {\em SolNr})\\
{\tt SendToModelSelection}({\em GMP}, {\em SolNr}, {\em Identifiers}, {\em Suffices})\\
{\tt RetrieveFromSolverSession}({\em solverSession}, {\em SolNr})\\
{\tt SendToSolverSession}({\em solverSession}, {\em SolNr})\\
\hline
{\tt GetObjective}({\em GMP}, {\em SolNr})\\
{\tt GetBestBound}({\em GMP}, {\em SolNr})\\
{\tt GetProgramStatus}({\em GMP}, {\em SolNr})$\to${\tt AllSolutionStatus}\\
{\tt GetSolverStatus}({\em GMP}, {\em SolNr})$\to${\tt AllSolutionStatus}\\
{\tt GetIterationsUsed}({\em GMP}, {\em SolNr})\\
{\tt GetMemoryUsed}({\em GMP}, {\em SolNr})\\
{\tt GetTimeUsed}({\em GMP}, {\em SolNr})\\
\hline
{\tt SetObjective}({\em GMP}, {\em SolNr}, {\em value})\\
{\tt SetProgramStatus}({\em GMP}, {\em SolNr}, {\em PrStatus})\\
{\tt SetSolverStatus}({\em GMP}, {\em SolNr}, {\em PrStatus})\\
{\tt SetIterationCount}({\em GMP}, {\em SolNr}, {\em IterCnt})\\
\hline
{\tt GetColumnValue}({\em GMP}, {\em SolNr}, {\em column})\\
{\tt SetColumnValue}({\em GMP}, {\em SolNr}, {\em column}, {\em value})\\
{\tt GetRowValue}({\em GMP}, {\em SolNr}, {\em row})\\
{\tt SetRowValue}({\em GMP}, {\em SolNr}, {\em row}, {\em value})\\
\hline
{\tt Check}({\em GMP}, {\em SolNr}, {\em NumInf}, {\em SumInf}, {\em MaxInf}[, {\em skipObj}])\\
{\tt IsInteger}({\em GMP}, {\em SolNr})\\
\hline
{\tt IsPrimalDegenerated}({\em GMP}, {\em SolNr})\\
{\tt IsDualDegenerated}({\em GMP}, {\em SolNr})\\
\hline
{\tt GetFirstOrderDerivative}({\em GMP}, {\em SolNr}, {\em row}, {\em column})\\
\hline
{\tt ConstraintListing}({\em GMP}, {\em SolNr}, {\em name})\\
\hline\hline
\end{tabular}
\caption{Procedures and functions in {\tt GMP::Solution}
namespace}\label{table:gmp.solution}
\end{aimmstable}

\paragraph{Solution contents}

Each solution in the repository is represented by a solution vector containing
all relevant solution data, such as
\begin{itemize}
\item solution status,
\item level values,
\item basis information,
\item marginals, and
\item other relevant requested sensitivity information.
\end{itemize}

\paragraph{Solution numbering}

Each generated mathematical program instance has its own associated solution
repository. Each solution in the repository is represented by an integer
solution number. Through the function {\tt GMP::Solution::GetSolutionsSet} you
can retrieve a subset of the predefined set {\tt Integers} containing the
set of all solution numbers that are currently in use for the given
mathematical program instance.

\paragraph{Solution transfer to the model}

Through the functions
\begin{itemize}
\item {\tt GMP::Solution::RetrieveFromModel},
\item {\tt GMP::Solution::SendToModel}, and
\item {\tt GMP::Solution::SendToModelSelection}
\end{itemize}
you can (re-)initialize a solution with the values currently contained in the
symbolic model, and vice versa. The function {\tt SendToModelSelection} allows you to only initialize a part of the model identifiers and suffices with a solution of from the solution repository.

\paragraph{Solution transfer to a solver session}

Through the functions
\begin{itemize}
\item {\tt GMP::Solution::RetrieveFromSolverSession}, and
\item {\tt GMP::Solution::SendToSolverSession}
\end{itemize}
you can set a solution in the repository equal to a solution reported by a
given solver session, or initialize the (initial) solution of a solver session
with a solution stored in the repository. Notice that these functions do not
have a {\em GMP} argument. Because each solver session is uniquely associated
with a single mathematical program instance, AIMMS is able to determine the
correct solution repository.

\paragraph{Computing first order derivatives}

Using the function {\tt GMP::Solution::GetFirstOrderDerivative}, you can compute, for the given solution, first order derivative of a particular row in a mathematical program with respect to a given variable. You can use such a function, for instance, to implement a sequential linear programming approach for nonlinear programs, as outlined in Section~\ref{sec:matrix.examples.slp}.


\section{Multi-objective optimization}\label{sec:gmp.multiobjective}
\index{multi-objective optimization}
\index{optimization!multi-objective}

\paragraph{Multi-objective optimization}

Multi-objective optimization deals with mathematical optimization problems involving more than one objective function that have to be optimized simultaneously.
Optimal decisions need to take into account the presence of trade-offs between two or more conflicting objectives. For example, minimizing the travelling distance
while minimizing the travelling time (which might conflict if the shortest route is not the fastest).
AIMMS allows you to define multiple objectives for linear models only. Multi-objective optimization in AIMMS is currently only supported by {\sc Cplex} and {\sc Gurobi}.

\paragraph{Blended or lexicographic objective}

You can define a mixture of blended and le\-xi\-co\-graphic (or hierarchical) objectives.
A blended objective consists of the linear combination of several objectives with given weights.
A lexicographic objective assumes that the objectives can be ranked in order of importance. A solution is considered le\-xi\-co\-graphi\-cal\-ly better than another
solution if it is better in the first objective where they differ (following the order). For a minimization problem, an optimal solution is one that is
le\-xi\-co\-graphi\-cal\-ly minimal.

\paragraph{The procedure {\tt GMP::Column::Set\-As\-Multi\-Objec\-tive}}

Currently, the only way to specify a multi-objective optimization model is by using the {\gmp} library.
The procedure {\tt GMP::Column::Set\-As\-Multi\-Objec\-tive} can be used to mark a variable as an objective used for multi-objective optimization. Typically, the definition
of such a variable defines the objective but the variable can also be used in an equality constraint which then defines the objective.

\paragraph{Example}

Consider the following declarations
\begin{example}
Variable TotalDistance {
    Definition :  sum( (i,j), Distance(i,j) * X(i,j) );
}
Variable TotalTime {
    Definition :  sum( (i,j), TravelTime(i,j) * X(i,j) );
}
\end{example}
Here {\tt X(i,j)} is a (binary) variable indicating whether the road between $i$ and $j$ is used. The variables {\tt TotalDistance} and {\tt TotalTime} can be specified
as objectives in a multi-objective optimization model using:

\begin{example}
GMP::Column::SetAsMultiObjective( genMP, TotalDistance, 2, 1.0, 0, 0.1 );
GMP::Column::SetAsMultiObjective( genMP, TotalTime, 1, 1.0, 0, 0.0 );
\end{example}
In this example, AIMMS will only pass the coefficients of the variable {\tt X} as the multi-objective coefficients to the solver, so {\tt Distance(i,j)}
for the first objective and {\tt TravelTime(i,j)} for the second objective. (In other words, the multi-objective variables {\tt TotalDistance} and
{\tt TotalTime} will be substituted by their definitions.) After solving the model, the objectives can be deleted by calling the procedure
{\tt GMP::Instance::De\-le\-te\-Mul\-ti\-Objec\-ti\-ves}.

\paragraph{Priority and weight}

The priority of the objective can be specified using the third argument of the procedure {\tt GMP::Column::Set\-As\-Multi\-Objec\-tive}. Its fourth argument
defines the weight by which the objective coefficients are multiplied when forming a blended objective, i.e., if multiple objectives have the same priority.
The last two (optional) arguments specify the absolute and relative tolerance respectively, which define the amount by which a solution may deviate from the optimal
value for the objective.

\paragraph{Mathematical program objective}

In case of multi-objective optimization, the variable specified in the {\tt Objective} attribute of the mathematical program will be treated as a normal variable,
that is, it will not be used as one of the multi-objectives.


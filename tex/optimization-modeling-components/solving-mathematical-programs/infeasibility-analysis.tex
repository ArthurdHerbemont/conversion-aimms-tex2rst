\section{Infeasibility analysis}\label{sec:mp.infeas}

\paragraph{Infeasibility analysis}

One of the more daunting tasks in mathematical programming is to find the cause
of an infeasible mathematical program. Such infeasibilities may occur
\begin{itemize}
\item either when you are developing a new model due to modeling errors,
\item or in a complete (and well-tested), model-based, end-user application employed by your customers due to inconsistencies in the model data.
\end{itemize}

\paragraph{Infeasibilities due to modeling errors}

There are several types of modeling errors that you can make during the
development of a mathematical program that can lead to hard-to-explain
infeasibilities. The most common are:
\begin{itemize}
\item simple typing errors, leading, for instance, to a wrong variable being referenced in a constraint,
\item a logical flaw in the model formulation, i.e.\ the formulation of one or more constraints just makes no sense,
\item the domain restriction of a constraint is not restrictive enough, i.e.\ constraints are generated that should not be generated,
\item the domain restriction of a variable is wrong, leading to too many or too few terms being generated in constraints referring to such a variable, or
\item the restriction in iterative operators (such as {\tt SUM} or {\tt PROD}) in the definition of constraints or defined variables is wrong, leading to too many or too little terms being generated in that particular constraint.
\end{itemize}
In general, trying to find infeasibilities that occur during model development
may force you to generate a constraint listing of your mathematical program and
carefully examine the generated constraints in order to find the modeling
error.

\paragraph{Infeasibilities due to data inconsistencies}

Even when the formulation of a mathematical program is internally consistent,
and shipped as an end-user application to your customers, infeasibilities may
occur due to inconsistencies in the model data. The most common data errors
are:
\begin{itemize}
\item inconsistencies in the structural data defining the topology of a model, e.g.\ in a network model a demand node may have been added for which no incoming arcs have been specified, or
\item inconsistencies in the quantitive model data, e.g.\ to total demand exceeds the total supply.
\end{itemize}
While most data inconsistencies may be detected  by methodically checking the
consistency all input data prior to actually solving the mathematical program
(for example, by using {\tt Assertions}, see also
Section~\ref{sec:data.assert}), it is often hard to cover all possible data
inconsistencies.

\paragraph{Adding excess variables}

A commonly used approach to try and deal with infeasibilities, is to add
explicit excess variables to all or some constraints in a model, along with a
penalty term in the objective that will keep all excess variables equal to 0 if
the model is feasible. If this procedure is executed properly, the {\em
modified} mathematical program will always be feasible, while the {\em
original} mathematical program is feasible if and only if the excess variables
are all equal to 0. In the case of an infeasibility, an examination of the
excess variables may provide useful information about the cause of
infeasibility.

\paragraph{Laborious procedure}

While adding excess variables to your model may certainly help you to resolve
any infeasibilities, the process of manually adding these excess variables to a
mathematical program is laborious and error-prone:
\begin{itemize}
\item you have to add the declarations of the excess variables for all (or some) constraints in your model,
\item the selected constraints have to be modified to include these excess variables, and
\item the objective has to be modified to include the excess-related penalty terms.
\end{itemize}
In addition, adding excess variables may considerably increase the size of the
generated matrix, so you may want to write supporting code to exclude the
excess variables from your mathematical program unless you encounter an
infeasibility.

\subsection{Adding infeasibility analysis to your model}\label{sec:mp.infeas.specifying}

\paragraph{The {\tt ViolationPenalty} attribute}
\presetindex{AllVariablesConstraints}

To ease the manual process described above, AIMMS offers support to {\em
automatically} extend your mathematical program with excess variables during
the generation of the matrix for the solver. You enable this feature through
the {\tt ViolationPenalty} attribute of a {\tt MathematicalProgram}
declaration. The value of the {\tt ViolationPenalty} attribute must be either
a
\begin{itemize}
\item 1-dimensional parameter with index domain {\tt AllVariables\-Constraints}, or
\item 2-dimensional parameter defined over {\tt AllVariables\-Con\-straints} and {\tt All\-ViolationTypes}.
\end{itemize}
The predefined set {\tt AllVariablesConstraints} is a subset of the set {\tt
AllIdenti\-fiers} and contains the names of all the variables and constraints
in your model. Through one of these two types of parameters you can specify for
which variables and constraints in your mathematical program AIMMS must
generate excess variables, as well as the penalty coefficient of these excess
variables in the modified objective.

\paragraph{The set {\tt AllViolationTypes}}
\presetindex{AllViolationTypes}

The predefined set {\tt AllViolationTypes} is a fixed set containing the three
types of possible violations for which AIMMS can generate excess variables.
The elements in the set {\tt AllViolationTypes} are
\begin{itemize}
\item {\tt Lower}: generate excess variables for the violation of a lower bound,
\item {\tt Upper}: generate excess variables for the violation of an upper bound, and
\item {\tt Definition}: generate excess variables for the violation of the equality between a defined variable and its definition.
\end{itemize}

\paragraph{Interpretation of {\tt ViolationPenalty} attribute}

If a parameter you entered in the {\tt ViolationPenalty} attribute contains no
data, AIMMS will generate the mathematical program without any generated
excess variables. If you specify a 2-dimensional parameter which is not empty,
all values must be nonnegative or assume the special value {\tt ZERO} (see also
Section~\ref{sec:expr.num.arith-ext}), and AIMMS will interpret its contents
as follows.

\paragraph{Penalty for objective variable}

The modified objective will include the original objective, unless a value of
{\tt ZERO} has been assigned to {\tt Definition} violation type for the original
objective variable. AIMMS will treat any other penalty value than {\tt ZERO} assigned to the
objective variable as 1.0! Note that by including the original objective the 
penalized mathematical program may become unbounded.

\paragraph{Penalty for constraints}

AIMMS will add nonnegative excess variables for the violation of a (finite)
lower and/or upper bound of every constraint for which a penalty value other
than 0.0 has been specified for the {\tt Lower} and/or {\tt Upper} violation
type, respectively. If a bound is infinite, no corresponding excess variable
will be generated. A penalty term will be added to the modified objective
consisting of the product of the specified (nonnegative) penalty coefficient
times the excess variable associated with the constraint, unless a penalty of
{\tt ZERO} has been specified in which case the corresponding term will not be
added to the modified objective.

\paragraph{Penalty for variables}

AIMMS will add nonnegative excess variables for the violation of a (finite)
lower and/or upper bound of every variable for which a penalty value other than
0.0 has been specified for the {\tt Lower} and/or {\tt Upper} violation type,
respectively. If a bound is infinite, no corresponding excess variable will be
generated. A penalty term will be added to the modified objective consisting of
the product of the specified (nonnegative) penalty coefficient times the excess
variable associated with the variable, unless a penalty of {\tt ZERO} has been
specified in which case the corresponding term will not be added to the
modified objective. The effect of using {\tt Lower} and/or {\tt Upper}
violations is that the variable can assume values outside their bounds
throughout the mathematical program.

\paragraph{Penalty for variable definitions}

AIMMS will add nonnegative excess variables for the violation of the
equality between a defined variable and its definition for every defined
variable for which a penalty value other than 0.0 has been specified for the
{\tt Definition} violation type.  A penalty term will be added to the
modified objective consisting of the product of the specified (nonnegative)
penalty times the excess variable(s) associated with the constraint expressing
the equality, unless a penalty of {\tt ZERO} has been specified in which case
the corresponding term(s) will not be added to the modified objective.

\paragraph{Definition versus lower/upper violations}

You can both use the {\tt Lower} and/or {\tt Upper} violation types and {\tt
Definition} violation type to compensate for a violation between the value of
the defined variable and its definition. However, when you use the {\tt
Definition} violation type, the value of the variable will remain within its
specified bounds throughout the mathematical program. It is up to you to decide
which violation type suits your needs best for a particular defined variable.

\paragraph{Interpretation of 1-dimension- al parameter}

If you specify a 1-dimensional parameter for the {\tt ViolationPenalty}
attribute, AIMMS will interpret this parameter as if it were a 2-dimensional
parameter, with the same value for all three violation types {\tt Lower}, {\tt
Upper} and {\tt Definition}.

\subsection{Inspecting your model for infeasibilities}\label{sec:mp.infeas.inspect}
\suffindex{variable}{Violation} \suffindex{variable}{DefinitionViolation}
\suffindex{constraint}{Violation}

\paragraph{Finding violations}

After you have let AIMMS extend your model with excess variables to find an
infeasibility, you must inspect the variables and constraints in your model to
find the violations. AIMMS allows you to do this through the use of two
suffices, the {\tt .Violation} suffix and the {\tt .DefinitionViolation}
suffix.

\paragraph{The {\tt .Violation} suffix\dots}

The {\tt .Violation} suffix denotes the amount by which a variable or
constraint violates its lower or upper bound. If you have specified a nonzero
violation penalty for the {\tt Upper} violation type, the {\tt .Violation}
suffix can assume positive values, while it can assume negative values whenever
you have specified a nonzero violation penalty for the {\tt Lower} violation
type.

\paragraph{\dots\ for variables}

For variables the {\tt .Violation} suffix denotes the amount by which the
variable violates its
\begin{itemize}
\item upper bound (if the suffix assumes a positive value), or
\item lower bound (if the suffix assumes a negative value).
\end{itemize}

\paragraph{\dots\ for constraints}

For constraints the {\tt .Violation} suffix denotes the amount by which the
constraint violates its
\begin{itemize}
\item upper bound (if the suffix assumes a positive value),
\item lower bound (if the suffix assumes a negative value, for ranged constraints).
\end{itemize}
If the constraint is an equality constraint, the {\tt .Violation} suffix
denotes the (positive or negative) amount by which the left hand side differs
from the (constant) right hand side.

\paragraph{The {\tt .Definition- Violation} suffix}

With the {\tt .DefinitionViolation} suffix, you can locate violations in the
definitions of defined variables for which you have specified a positive
penalty for the {\tt Definition} violation type. The value of the suffix
denotes the (positive or negative) amount by which the defined variable differs
from its definition. Note that a defined variable may violate both its bounds
and its definition, depending on the type of allowed violations you have
specified.

\paragraph{Locating violations}

To locate violations in a model which was extended by AIMMS with excess
variables, you may use the {\tt Card} function to locate variables and
constraints with nonzero {\tt .Violations} suffices. The following example
shows how to proceed, where {\tt v} is assumed to be an index in {\tt
AllVariables}.
\begin{example}
    for ( v | Card(v, 'Violation'}) ) do

        ! Take any action that you want to perform on this violated variable

    endfor;
\end{example}

\subsection{Application to goal programming}\label{sec:mp.infeas.goal-programming}
\index{goal programming}\index{programming! goal}

\paragraph{Goal program- ming \dots}

In goal programming a distinction is made between {\em hard constraints} that 
cannot be violated and {\em soft constraints}, which represent goals or targets 
one would like to achieve. The objective function in goal programming is to 
minimize the weighted sum of deviations from the goals set by the soft 
constraints.

\paragraph{\dots interpreted as violations}

In AIMMS, goal programming can be easily implemented using the {\tt 
Violation\-Penalty} attribute of a mathematical program, without the need to modify 
the formulation of all soft constraints. For each soft constraint in your goal 
programming model, you can assign the appropriate weight to the {\tt 
Violation\-Penalty} attribute to penalize deviations from the set target for that 
constraint.

\paragraph{Inspecting deviations}

Through the {\tt .Violation} suffix of constraints and variables you can inspect 
the deviations from the goals of the soft constraints in your goal programming model.

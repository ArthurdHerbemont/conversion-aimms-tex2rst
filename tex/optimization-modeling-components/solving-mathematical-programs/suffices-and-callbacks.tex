\section{Suffices and callbacks}\label{sec:mp.suffix}

\paragraph{Suffices}

A mathematical program has a number of suffices which can be used for various
purposes. Typical examples are:
\begin{itemize}
\item To obtain information about the solution process.
      This information is filled in by the solver at the end of the solution process.
      These suffixes are presented in Table~\ref{table:mp.suffix-mp.Solver}.
\item To determine when and how to activate a callback procedure.  This information
      can be filled in between solution steps.  See also Chapter~\ref{chap:gmp} where an
      alternative method for callbacks is presented.
      These suffixes are presented in Table~\ref{table:mp.suffix-mp.User}.
\item To get statistics of the generated mathematical program.  These
      statistics are determined when the generated mathematical program is constructed.
      These suffixes are presented in Table~\ref{table:mp.suffix-mp.Aimms}.
\end{itemize}


\begin{aimmstable}
\suffindex{mathematical program}{Objective}
\suffindex{mathematical program}{Incumbent}
\suffindex{mathematical program}{BestBound}
\suffindex{mathematical program}{ProgramStatus}
\suffindex{mathematical program}{SolverStatus}
\suffindex{mathematical program}{Iterations}
\suffindex{mathematical program}{Nodes}
\suffindex{mathematical program}{GenTime}
\suffindex{mathematical program}{SolutionTime}
\suffindex{mathematical program}{NumberOfBranches}
\suffindex{mathematical program}{NumberOfFails}
\suffindex{mathematical program}{NumberOfInfeasibilities}
\suffindex{mathematical program}{SumOfInfeasibilities}
\AIMMSlink{objective}
\AIMMSlink{incumbent}
\AIMMSlink{bestbound}
\AIMMSlink{programstatus}
\AIMMSlink{solverstatus}
\AIMMSlink{iterations}
\AIMMSlink{nodes}
\AIMMSlink{gentime}
\AIMMSlink{solutiontime}
\AIMMSlink{NumberOfBranches}
\AIMMSlink{NumberOfFails}
\AIMMSlink{numberofinfeasibilities}
\AIMMSlink{sumofinfeasibilities}
\size{9}{11.5pt}\selectfont
\begin{tabular}{|l|l|}
\hline\hline
{\bf Suffix} & {\bf Meaning} \\
\hline
{\tt Objective}         & Current objective value \\
{\tt Incumbent}             & Current incumbent value  \\
{\tt BestBound}             & Best bound on objective value \\
{\tt ProgramStatus}         & Current program status  \\
{\tt SolverStatus}          & Current solver status    \\
{\tt Iterations}            & Current number of iterations      \\
{\tt Nodes}                & Current number of nodes   \\
                           & (\verb|mip|, \verb|miqp|, and  \verb|miqcp| only)  \\
{\tt GenTime}             & Current generation time in [second]  \\
{\tt SolutionTime}          & Current solution time in [second]  \\
{\tt NumberOfBranches}             & Number of nodes visited by a CP solver   \\
{\tt NumberOfFails}                & Number of leaf nodes without  \\
                                   & solution in a CP search tree   \\
{\tt NumberOfInfeasibilities}   & Final number of infeasibilities  \\
{\tt SumOfInfeasibilities }     & Final sum of the infeasibilities  \\
\hline\hline
\end{tabular}
\caption{Suffices of a mathematical program filled by the solver}\label{table:mp.suffix-mp.Solver}
\end{aimmstable}

\begin{aimmstable}
\suffindex{mathematical program}{CallbackProcedure}
\suffindex{mathematical program}{CallbackIterations}
\suffindex{mathematical program}{CallbackTime}
\suffindex{mathematical program}{CallbackStatusChange}
\suffindex{mathematical program}{CallbackIncumbent}
\suffindex{mathematical program}{CallbackAddCut}
\suffindex{mathematical program}{CallbackReturnStatus}
\suffindex{mathematical program}{CallbackAOA}
\AIMMSlink{callbackprocedure}
\AIMMSlink{callbackiterations}
\AIMMSlink{callbacktime}
\AIMMSlink{callbackstatuschange}
\AIMMSlink{callbackincumbent}
\AIMMSlink{callbackaddcut}
\AIMMSlink{callbackreturnstatus}
\AIMMSlink{callbackaoa}
\size{9}{11.5pt}\selectfont
\begin{tabular}{|l|l|}
\hline\hline
{\bf Suffix} & {\bf Meaning} \\
\hline
{\tt CallbackProcedure}     & Name of callback procedure  \\
{\tt CallbackIterations}    & Return to callback after this  \\
                            & number of iterations  \\
{\tt CallbackTime}          & Name of callback procedure to be  \\
                            & called after some elapsed time  \\
{\tt CallbackStatusChange}  & Name of callback procedure to be  \\
                            & called after a status change  \\
{\tt CallbackIncumbent}     & Name of callback procedure to be  \\
                            & called for every new incumbent  \\
{\tt CallbackAddCut}        & Name of callback procedure to be  \\
                            & called to add additional cuts  \\
                            & ({\sc Cplex} and {\sc Gurobi})  \\
{\tt CallbackReturnStatus}  & Return status of callback  \\
{\tt CallbackAOA}           & Name of AOA callback procedure  \\
\hline\hline
\end{tabular}
\caption{Suffices of a mathematical program stated by the user}\label{table:mp.suffix-mp.User}
\end{aimmstable}

\begin{aimmstable}
\suffindex{mathematical program}{SolverCalls}
\suffindex{mathematical program}{NumberOfConstraints}
\suffindex{mathematical program}{NumberOfVariables}
\suffindex{mathematical program}{NumberOfNonzeros}
\suffindex{mathematical program}{NumberOfIntegerVariables}
\suffindex{mathematical program}{NumberOfIndicatorConstraints}
\suffindex{mathematical program}{NumberOfSOS1Constraints}
\suffindex{mathematical program}{NumberOfSOS2Constraints}
\suffindex{mathematical program}{NumberOfNonlinearConstraints}
\suffindex{mathematical program}{NumberOfNonlinearVariables}
\suffindex{mathematical program}{NumberOfNonlinearNonzeros}
\AIMMSlink{solvercalls}
\AIMMSlink{numberofconstraints}
\AIMMSlink{numberofvariables}
\AIMMSlink{numberofnonzeros}
\AIMMSlink{NumberOfIntegerVariables}
\AIMMSlink{NumberOfIndicatorConstraints}
\AIMMSlink{NumberOfSOS1Constraints}
\AIMMSlink{NumberOfSOS2Constraints}
\AIMMSlink{NumberOfNonlinearConstraints}
\AIMMSlink{NumberOfNonlinearVariables}
\AIMMSlink{NumberOfNonlinearNonzeros}
\size{9}{11.5pt}\selectfont
\begin{tabular}{|l|l|}
\hline\hline
{\bf Suffix} & {\bf Meaning} \\
\hline
{\tt SolverCalls}                  & Total number of applied {\tt SOLVE}'s  \\
{\tt NumberOfConstraints}          & Number of individual constraints   \\
{\tt NumberOfVariables}            & Number of individual variables  \\
{\tt NumberOfNonzeros}             & Number of nonzeros  \\
{\tt NumberOfIntegerVariables}     & Number of individual integer variables  \\
{\tt NumberOfIndicatorConstraints} & Number of individual constraints   \\
                                   & with an activating condition   \\
{\tt NumberOfSOS1Constraints}      & Number of individual SOS1 constraints   \\
{\tt NumberOfSOS2Constraints}      & Number of individual SOS2 constraints   \\
{\tt NumberOfNonlinearConstraints} & Number of individual nonlinear  \\
                                   & constraints  \\
{\tt NumberOfNonlinearVariables}   & Number of individual nonlinear  \\
                                    & variables  \\
{\tt NumberOfNonlinearNonzeros}    & Number of nonlinear nonzeros  \\
\hline\hline
\end{tabular}
\caption{Suffices of a mathematical program statistics from AIMMS}\label{table:mp.suffix-mp.Aimms}
\end{aimmstable}




\paragraph{Solver callbacks}
\index{mathematical program!solver callback} \index{solver callback}
\index{callback}

After each iteration the external solver calls back to the AIMMS system to
offer AIMMS the opportunity to take control. AIMMS, in turn, allows you
to execute a procedure which is referred to as a {\em callback procedure}. Once
the callback procedure has finished, the control is returned to the external
solver to continue with the next iteration. By including a callback procedure
you can perform several tasks such as:
\begin{itemize}
\item inspect the current status of the solution process,
\item update one or more model parameters, which can be used, for instance,
to provide a graphical overview of the solution process,
\item retrieve (part of) the current solution, and
\item abort the solution process, and
\end{itemize}
You can nominate any procedure as a callback procedure by assigning its name to
the suffix {\tt CallbackProcedure} of the associated mathematical program as
in:
\begin{example}
     TransportModel.CallbackProcedure := 'MyCallbackProcedure' ;
\end{example}
Note that values assigned to the suffix {\tt CallbackProcedure} or any of the
other suffices holding the name of a callback procedure, must be elements of
the predefined set {\tt AllProcedures}. Therefore, if you assign a literal
procedure name to such a suffix, you should make sure to quote it, as
illustrated in the example above.

\paragraph{When activated}

Callback procedures under your control may cause a considerable computational
overhead, and should only be activated when necessary. To give you control of
the frequency of callbacks, AIMMS provide three separate suffices to trigger
a callback procedure. Specifically, a callback procedure can be called
\begin{itemize}
\item after a specified number of iterations,
\item after a specified number of seconds,
\item after a change of status of the solution process, or
\item at every new incumbent during the solution process
of a mixed integer program.
\end{itemize}

\paragraph{Activated after iterations}
\suffindex{mathematical program}{CallbackIterations}

With the suffix {\tt CallbackIterations} you can indicate after how many
iterations the callback procedure specified by the {\tt CallbackProcedure}
suffix must be called again. If you specify the number 0 (default), no such
callbacks will be made.

\paragraph{Activated after time}
\suffindex{mathematical program}{CallbackTime}

With the suffix {\tt CallbackTime} you specify the name of the callback
procedure to be called when a certain number of seconds has elapsed.
When not specified (the default), no such callbacks are made.

\paragraph[0.8]{Activated after status change}
\suffindex{mathematical program}{CallbackStatusChange}

With the suffix {\tt CallbackStatusChange} you specify the name of the callback
procedure to be performed when the status of the solution process changes. When
not specified (the default), no such callbacks are made.

\paragraph{Activated after new incumbent}
\suffindex{mathematical program}{CallbackIncumbent}

With the suffix {\tt CallbackIncumbent} you specify the name of the callback
procedure to be performed when the solver finds a new incumbent during
the solution process of a mixed integer program. When not specified (the
default), no such callbacks are made.

\paragraph{Watch objective values}
\suffindex{mathematical program}{Incumbent} \suffindex{mathematical
program}{Objective} \suffindex{mathematical program}{BestBound}

During a callback procedure you can access various objective values as they are
reported by the solver during a mixed integer program through several suffices
of the mathematical program at hand. The following suffices provide information
about the objective values:
\begin{itemize}
\item through the suffix {\tt Incumbent} you can obtain the objective
value of the best integer solution found so far,
\item through the suffix {\tt BestBound} you can obtain the best bound
on the objective value during the branch-and-bound process, and
\item through the suffix {\tt Objective} you can obtain the current
objective value reported by the solver at the precise time of the callback.
\end{itemize}
For mixed integer programs the suffix {\tt Objective} will be meaningless in
most cases during the solution process.

\paragraph{Watch intermediate solution values}

In a callback procedure you can access the current solution values of the
variables in the mathematical program, and assign these to other identifiers in
your model. One possible use of this feature is to store multiple feasible
integer solutions of a mixed integer linear program.

\paragraph{The procedure {\tt RetrieveCurrentVariableValues}}
\procindex{RetrieveCurrentVariableValues}
\AIMMSlink{retrievecurrentvariablevalues}

For some solvers there may be a considerable overhead involved to retrieve the
current variable values during the running solution process. Therefore,
AIMMS will only do so when you explicitly call the procedure
\begin{quote}
\em {\tt RetrieveCurrentVariableValues}({\em VariableSet})
\end{quote}
With the {\em VariableSet} argument you can specify the subset of the set {\tt
AllVari\-ables} consisting of all (symbolic) variables for which you want the
current values to be retrieved. When you call this procedure outside the
context of a solver callback procedure, AIMMS will produce a runtime error.

\paragraph{Adding additional cuts}
\suffindex{mathematical program}{CallbackAddCut} \procindex{GenerateCut}
\AIMMSlink{GenerateCut}

When you want to add additional cuts during the solution process of a mixed
integer program, you should install a callback procedure to generate these
constraints using the {\tt CallbackAddCut} suffix. This procedure is called at
every node that has an LP-optimal solution with an objective function value
below the current cutoff and is integer infeasible. The procedure allows you to
add individual constraints using the {\tt GenerateCut({\em row}, {\em local})}
function. The {\em row} argument should always be a scalar reference to an
existing constraint name in your model. The {\em local} argument should be a
scalar binary that indicates whether the cut is a local cut (value 1) or a
global one (value 0). The {\em local} argument is an optional argument, and has
a default of 1.

\paragraph{Example}

Consider a model with the following constraint.

\begin{example}
Constraint Triangle_Cut {
    IndexDomain  : (i1,i2,i3) | (i1 < i2) and (i2 < i3);
    Definition   : x(i1) + x(i2) + x(i3) - y(i1,i2) - y(i1,i3) - y(i2,i3) <= 1;
}
\end{example}

Then the following piece of code, when specified as the procedure body of the
{\tt CallbackAddCut} procedure, will only add those triangle cuts that are
violated.

\begin{example}
    RetrieveCurrentVariableValues(AllVariables);

    for ( (i1,i2,i3) | (i1 < i2) and (i2 < i3) ) do
        if ( x(i1) + x(i2) + x(i3) - y(i1,i2) - y(i1,i3) - y(i2,i3) > 1 + eps ) then
            GenerateCut( Triangle_Cut(i1,i2,i3), 1 );
        endif;
    endfor;
\end{example}

\paragraph{Aborting the solution process}
\suffindex{mathematical program}{CallbackReturnStatus}

When you want to abort the solution process, you can set the suffix {\tt
Callback\-ReturnStatus} to {\tt 'abort'} during the execution of your callback
procedure, as in:
\begin{example}
     TransportModel.CallbackReturnStatus := 'abort' ;
\end{example}
After aborting the process, AIMMS will retrieve the current solution and set
the final solver status to {\tt UserInterrupt}.

\paragraph{Example}

Consider a mathematical program {\tt TransportModel} which incorporates a
callback procedure. The following callback procedure will abort the solution
process if the total solution time exceeded 1800 seconds, and if the progress
is less than 1\% compared to the last nonzero objective function value.
\begin{example}
    if ( TransportModel.SolutionTime > 1800 [second] and PreviousObjective and
         (TransportModel.Objective - PreviousObjective) < 0.01*PreviousObjective )
    then
       TransportModel.CallbackReturnStatus := 'abort';
    else
       PreviousObjective := TransportModel.Objective;
    endif;
\end{example}

\paragraph{Solver and program status}
\suffindex{mathematical program}{ProgramStatus} \suffindex{mathematical
program}{SolverStatus} \presetindex{AllSolutionStates}
\AIMMSlink{allsolutionstates}

Both the {\tt ProgramStatus} and the {\tt SolverStatus} suffix take their value
in the predefined set {\tt AllSolutionStates} presented in
Table~\ref{table:mp.status}.

\begin{aimmstable}
\begin{tabular}{|l|l|}
\hline\hline
{\bf Program status} & {\bf Solver status}\\
\hline
 {\tt ProgramNotSolved      }         & {\tt SolverNotCalled     } \\
 {\tt Optimal               }         & {\tt NormalCompletion    } \\
 {\tt LocallyOptimal        }         & {\tt IterationInterrupt  } \\
 {\tt Unbounded             }         & {\tt ResourceInterrupt   } \\
 {\tt Infeasible            }         & {\tt TerminatedBySolver  } \\
 {\tt LocallyInfeasible     }         & {\tt EvaluationErrorLimit} \\
 {\tt IntermediateInfeasible}         & {\tt Unknown             } \\
 {\tt IntermediateNonOptimal}         & {\tt UserInterrupt       } \\
 {\tt IntegerSolution       }         & {\tt PreprocessorError   } \\
 {\tt IntermediateNonInteger}         & {\tt SetupFailure        } \\
 {\tt IntegerInfeasible     }         & {\tt SolverFailure       } \\
 {\tt InfeasibleOrUnbounded }         & {\tt InternalSolverError } \\
 {\tt UnknownError          }         & {\tt PostProcessorError  } \\
 {\tt NoSolution            }         & {\tt SystemFailure       } \\
\hline\hline
\end{tabular}
\caption{Mathematical program and solver status}\label{table:mp.status}
\end{aimmstable}


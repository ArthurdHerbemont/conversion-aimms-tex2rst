\section{\tttext{MathematicalProgram} declaration and attributes}\label{sec:mp.mp}
\label{ssec:decl:constr-mp:mp}

\paragraph{Attributes}
\declindex{MathematicalProgram} \AIMMSlink{mathematical_program}

The attributes of mathematical programs are listed in
Table~\ref{table:mp.attr-mp}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute}    & {\bf Value-type}               & {\bf See also} \\
                   &                                & {\bf page} \\
\hline
\verb|Objective|   & {\em variable-identifier}      & \\
\verb|Direction|   & {\tt minimize}, {\tt maximize} & \\
\verb|Variables|   & {\em variable-set}             & \\
\verb|Constraints| & {\em constraint-set}           & \\
\verb|Type|        & {\em model-type}               & \\
\verb|ViolationPenalty| & {\em reference}        & \pageref{sec:mp.infeas}\\
\verb|Text|        & {\em string}                   & \pageref{attr:prelim.text}\\
\verb|Comment|     & {\em comment string}           & \pageref{attr:prelim.comment}\\
{\tt Convention} & {\em convention} & \pageref{sec:units.convention} \\
\hline\hline
\end{tabular}
\caption{{\tt MathematicalProgram} attributes}\label{table:mp.attr-mp}
\end{aimmstable}

\paragraph{Example}

The following example illustrates a typical mathematical program.
\begin{example}
MathematicalProgram TransportModel {
    Objective   : TransportCost;
    Direction   : minimize;
    Constraints : AllConstraints;
    Variables   : AllVariables;
    Type        : lp;
}
\end{example}
It defines the linear program {\tt TransportModel}, which is built up from all
constraints and variables in the model text. The variable {\tt TransportCost}
serves as the objective function to be minimized.

\paragraph{The {\tt Objective} attribute}\herelabel{attr:mp.objective}
\declattrindex{mathematical program}{Objective}
\AIMMSlink{mathematical_program.objective}

With the {\tt Objective} attribute you can specify the objective of your
mathematical program. Its value must be a reference to a (defined) variable or
any other variable expression. When you want to use the objective value in the
end-user interface of your model, the {\tt Objective} attribute must be a
variable reference.

\paragraph{Omitting the objective}

If you do not specify an objective, your mathematical program will be solved to
find a feasible solution and it will then terminate.

\paragraph{The {\tt Direction} attribute}\herelabel{attr:mp.direction}
\declattrindex{mathematical program}{Direction}
\AIMMSlink{mathematical_program.direction}

In conjunction with an objective you must use the {\tt Direction} attribute to
indicate whether the solver should {\tt minimize} or {\tt maximize} the
objective. During a {\tt SOLVE} statement you can override this direction by
using a {\tt WHERE} clause for the {\tt direction} option.

\paragraph{The {\tt Variables} attribute}\herelabel{attr.mp.variables}
\declattrindex{mathematical program}{Variables} \presetindex{AllVariables}
\AIMMSlink{mathematical_program.variables} \AIMMSlink{allvariables}

With the {\tt Variables} attribute you can specify which set of variables are
to be included in your mathematical program. Its must be either the predefined
set {\tt AllVariables} or a subset thereof. The set {\tt AllVariables} is
predefined by AIMMS, and it contains the names of all the variables declared
in your model. Its contents cannot be changed. If you mathematical program
contains an objective, AIMMS will automatically add this to set of generated
variables during generation.

\paragraph{Variables as parameters}

If the {\tt Variables} attribute is assigned a subset of the set {\tt
AllVariables}, AIMMS will treat all the variables outside this set as if
they were parameters. That is, all occurrences of such variables will not
result in the generation of individual variables for the solver, but will be
accounted for in the right-hand side of the constraint according to their value
during generation.

\paragraph{Compare to {\tt NonvarStatus}}
\subtypindex{attribute}{NonvarStatus}{versus Variables attribute@{versus {\tt Variables} attribute}} 
\subtypindex{attribute}{Variables}{versus NonvarStatus attribute@{versus {\tt NonvarStatus} attribute}}

The {\tt Variables} attribute performs a similar function as the {\tt NonvarStatus} attribute or the {\tt .NonVar} suffix of a variable (see also
Section~\ref{sec:var.var}). The {\tt Variables} attribute in a mathematical
program allows you to quickly change the status of an entire class of
variables, while the {\tt NonvarStatus} (in a variable declaration)
gives much finer control at the individual level. As shown below, the latter is
very useful to perform model algebra.

\paragraph{The {\tt Constraints} attribute}\herelabel{attr:mp.constraints}
\declattrindex{mathematical program}{Constraints} \presetindex{AllConstraints}
\AIMMSlink{mathematical_program.constraints} \AIMMSlink{allconstraints}

With the {\tt Constraints} attribute you can specify which constraints are part
of your mathematical program. Its value must be either the predefined set {\tt
All\-Constraints} or a subset thereof. The set {\tt AllConstraints} contains
the names of all declared constraints plus the names of all variables which
have a definition attribute. Its contents is computed at compile time, and
cannot be changed.
\begin{itemize}
\item If you specify the set {\tt AllConstraints}, AIMMS will generate
individual constraints for all declared constraints and variables with a
definition.
\item If you specify a subset of the set {\tt AllConstraints},
AIMMS will only generate individual constraints for the declared constraints
and defined variables in that subset.
\end{itemize}
If you mathematical program has an objective which is a defined variable, its
definition is automatically added to the set of generated constraints during
generation.

\paragraph{Defined variables}

Variables with a nonempty definition attribute have a somewhat special status.
Namely, for every defined variable AIMMS will not only generate this
variable, but will also generate a constraint containing its definition.
Therefore, defined variables are contained in both the predefined sets {\tt
AllVariables} and {\tt AllConstraints}. You can add a defined variable to the
variable and constraint set of a mathematical program independently.
\begin{itemize}
\item If you omit a defined variable from the variable set
of a mathematical program, all occurrences of the variable will be fixed to its
current value and accounted for in the right-hand side of all constraints.
\item If you omit a defined variable from the constraint
set of a mathematical program, the defining constraint will not be generated.
\end{itemize}

\paragraph{Performing model algebra}
\index{mathematical program!model algebra} \index{model algebra}

By changing the contents of the identifier sets that you have entered at the
{\tt Variables} and {\tt Constraints} attributes of a mathematical program you
can perform a simple form of {\em model algebra}. That is, you can investigate
the effects of adding or removing constraints from within the graphical
interface. Furthermore, it allows you to reconfigure your model based on the
value of your model data.

\paragraph{Synchronizing variable and constraint sets}

When changing the contents of either the variable or the constraint set of a
mathematical program, you may find that the contents of the other set also
needs some adjustment. For instance, adding a variable to a mathematical
program makes no sense if there are no constraints that refer to it. AIMMS
offers two special set-valued functions to help you to accomplish this task.

\paragraph{The function {\tt Variable\-Constraints}}
\sfuncindex{VariableConstraints} \AIMMSlink{variableconstraints}

The function {\tt VariableConstraints} takes a subset of the predefined set
{\tt All\-Variables} as its argument, and returns a subset of the predefined
set {\tt AllCon\-straints}. The resulting constraint set contains all
constraints which use one or more of the variables in the argument set.

\paragraph{The function {\tt Constraint\-Variables}}
\sfuncindex{ConstraintVariables} \AIMMSlink{constraintvariables}

The function {\tt ConstraintVariables} performs the opposite task. It takes a
subset of the set {\tt AllConstraints} as its arguments, and returns a subset
of the set {\tt AllVariables}. The resulting variable set contains all
variables which are referred to in one or more constraints in the argument set.
Also included are all variables referred to in the definitions of other
variables inside the set.

\paragraph{Example}

Consider the use of the functions {\tt VariableConstraints} and {\tt
ConstraintVariables} in conjunction with the following declaration of a
mathematical program.
\begin{example}
MathematicalProgram PartialTransportModel {
    Objective   : TransportCost;
    Direction   : minimize;
    Constraints : PartialConstraintSet;
    Variables   : PartialVariableSet;
}
\end{example}
Assume that the set {\tt PartialVariableSet} contains a subset of the variables
declared in the model. Further assume that you would like to build up the
contents of the set {\tt PartialConstraintSet} together with the required
additions to {\tt PartialVariableSet} so that the contents of both sets are
maximal. This is referred to as their transitive closure. By successively
calling the functions {\tt Variable\-Constraints} and {\tt
Constraint\-Variables}, the following loop computes the transitive closure of
the variable and constraint sets.
\begin{example}
    repeat
       PreviousCardinality  := Card( PartialVariableSet );
       PartialConstraintSet := VariableConstraints( PartialVariableSet   );
       PartialVariableSet   := ConstraintVariables( PartialConstraintSet );

       break when Card( PartialVariableSet ) = PreviousCardinality;
    endrepeat ;
\end{example}
The {\tt break} occurs when the set {\tt PartialVariableSet} has not increased
in size.

\paragraph{The {\tt Type} attribute}\herelabel{attr:mp.type}
\declattrindex{mathematical program}{Type}
\AIMMSlink{mathematical_program.type}

With the {\tt Type} attribute of a mathematical program you can prescribe a
solution type. When the specified type is not compatible with the generated
mathematical program, AIMMS will return an error message. You can override
the type during a {\tt SOLVE} statement using a {\tt WHERE} clause for the {\tt
type} option. You can use this, for instance, to easily switch between the {\tt
mip} and {\tt rmip} types.

\paragraph{Available types}
\index{mathematical program!supported types}

A complete list of the mathematical program types available within AIMMS is
given in Table~\ref{table:mp.types}. Most are self-explanatory. When the type
{\tt rmip} is specified, all integer variables are treated as continuous within
their bounds. The {\tt rmip} type is the global version of the {\tt Relax}
attribute associated with individual variables (see also
Section~\ref{sec:var.var}). The types {\tt ls} and {\tt nls} can only be
selected in the absence of the {\tt Objective} attribute.

\begin{aimmstable}
\begin{tabular}{|l|l|}
\hline\hline
{\bf Type}  & {\bf Description}\\
\hline
\verb|lp|       & linear program\\
\verb|ls|      & linear system\\
\verb|qp|       & quadratic program\\
\verb|nlp|      & nonlinear program\\
\verb|nls|     & nonlinear system\\
\verb|mip|      & mixed integer program\\
\verb|rmip|     & relaxed mixed integer program\\
\verb|minlp|    & mixed integer nonlinear program\\
\verb|rminlp|    & relaxed mixed integer nonlinear program\\
\verb|qp|       & quadratic program\\
\verb|miqp|       & mixed integer quadratic program\\
\verb|qcp|       & quadratic constraint program\\
\verb|miqcp|       & mixed integer quadratic constraint program\\
\verb|network|  & pure network program\\
\verb|mcp|      & mixed complementarity program\\
\verb|mpcc|     & mathematical program with complementarity constraint\\
%\verb|ode| & ordinary differential equations\\
\hline\hline
\end{tabular}
\caption{Available model types with AIMMS}\label{table:mp.types}%
\end{aimmstable}

\paragraph{The {\tt Convention} attribute}
\declattrindex{mathematical program}{Convention}
\AIMMSlink{mathematical_program.convention}

You can use the {\tt Convention} attribute to specify the unit convention that
you want to be used for scaling the variables and constraints in your
mathematical program. For further details on this issue you are referred to
Section~\ref{sec:units.convention}.

\paragraph{The {\tt ViolationPenalty} attribute}
\declattrindex{mathematical program}{ViolationPenalty}
\AIMMSlink{mathematical_program.violation_penalty}

With the {\tt ViolationPenalty} attribute you can instruct AIMMS to
automatically add artificial terms to the constraints of your mathematical
program to help resolve and/or track infeasibilities in your mathematical
program. Infeasibility analysis and the use of the {\tt ViolationPenalty}
attribute is discussed in full detail in Section~\ref{sec:mp.infeas}.
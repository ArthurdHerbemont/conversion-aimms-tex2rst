\section{The \tttext{SOLVE} statement}\label{sec:mp.solve}

\paragraph{The {\tt SOLVE} statement}
\statindex{SOLVE} \AIMMSlink{solve}

With the {\tt SOLVE} statement you can instruct AIMMS to compute the
solution of a {\tt MathematicalProgram}, resulting in the following actions.
\begin{itemize}
\item AIMMS determines which solution
method(s) are appropriate, and checks whether the specified type is also
appropriate.
\item AIMMS then generates the Jacobian matrix (first derivatives of all the
constraints), the bounds on all variables and constraints, and an objective
where appropriate.
\item AIMMS communicates the problem to an underlying solver that is able to
perform the chosen solution method.
\item AIMMS finally reads the computed solution back from the solver.
\end{itemize}

\paragraph{Syntax}

In addition to initiating the solution process of a {\tt MathematicalProgram},
you can also use the {\tt SOLVE} statement to provide local overrides of
particular AIMMS settings that influence the way in which the solution
process takes place. The syntax of the {\tt SOLVE} statement follows.

\paragraph[0.8]{}
\begin{syntax}
\syntaxdiagram{solve-statement}{solve}
\end{syntax}

\paragraph{Replace and merge mode}
\subtypindex{statement}{SOLVE}{REPLACE mode@{{\tt REPLACE} mode}}
\subtypindex{statement}{SOLVE}{MERGE mode@{{\tt MERGE} mode}}
\subtypindex{mode}{MERGE}{in SOLVE statement@{in {\tt SOLVE} statement}}
\subtypindex{mode}{REPLACE}{in SOLVE statement@{in {\tt SOLVE} statement}}

You can instruct AIMMS to read back the solution in either {\em replace} or
{\em merge} mode. If you do not specify a mode, AIMMS assumes replace mode.
In replace mode AIMMS will, before reading back the solution of the
mathematical program, remove the values of the variables in the {\tt Variables}
set of the mathematical program for all index tuples except those that are
fixed
\begin{itemize}
\item because they are not within their current domain (i.e.\ inactive),
\item through the {\tt NonvarStatus} attribute or the {\tt .NonVar}
suffix of the variable,
\item because they are outside the planning interval of a {\tt
Horizon} (see Section~\ref{sec:time.horizon}), or
\item because their upper and lower bounds are equal.
\end{itemize}
In merge mode AIMMS will only replace the {\em individual variable values}
involved in the mathematical program. This mode is very useful, for instance,
when you are iteratively solving subproblems which correspond to slices of the
symbolic variables in your model.

\paragraph{Infeasible and unbounded problems}

Whenever the invoked solver finds that a mathematical program is infeasible or
unbounded, AIMMS will assign one of the special values {\tt na}, {\tt inf}
or {\tt -inf} to the objective variable. For you, this will serve as a reminder
of the fact that there is a problem even when you do not check the {\tt
ProgramStatus} and {\tt SolverStatus} suffices. For all other variables,
AIMMS will read back the last values computed by the solver just before
returning with infeasibility or unboundedness.

\paragraph{Temporary option settings}
\statindex{OPTION}

Sometimes you may need some temporary option settings during a single {\tt
SOLVE} statement.  Instead of having to change the relevant options using the
{\tt OPTION} statement and set them back afterwards, AIMMS also allows you
to specify values for options that are used only during the current {\tt SOLVE}
statement. The syntax is similar to that of the {\tt OPTION} statement.

\paragraph{Also for attributes}
\subtypindex{statement}{SOLVE}{WHERE clause@{{\tt WHERE} clause}} \index{WHERE
clause@{{\tt WHERE} clause}}

Apart from specifying temporary option settings you can also use the {\tt
WHERE} clause to override the {\tt type} and {\tt direction} attributes
specified in the declaration of the mathematical program, as well as the {\tt
solver} to use for the solution process.

\paragraph{Example}

The following {\tt SOLVE} statement selects {\tt 'cplex'} as its solver, sets
the model type to {\tt 'rmip'}, and sets the {\sc Cplex} option {\tt LpMethod}
to {\tt 'Barrier'}.
\begin{example}
    solve TransportModel in replace mode
        where solver    := 'cplex',
              type      := 'rmip',
              LpMethod  := 'Barrier' ;
\end{example}

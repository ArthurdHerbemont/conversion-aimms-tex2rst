\section{Constraint Programming essentials}
\label{sec:constraint.programming.essentials}

\paragraph{Variables}

In constraint programming, models are built using 
variables and constraints, and as such is similar to integer programming.
One fundamental difference is that, in integer programming,
the range of a variable is specified and maintained as an interval,
while in constraint programming, the variable range is
maintained explicitly as a set of elements. 
Note that in the constraint programming literature, the range 
of a variable is commonly referred to as its {\em domain}. 

\paragraph{Constraints}

As a consequence of this explicit range representation,
constraint programming can offer a wide variety of constraint types.
Most constraint programming solvers allow constraints to be defined by 
arbitrary expressions that combine algebraic or logical 
operators. Moreover, meta-constraints can be formulated by
interpreting the logical value of an expression as a boolean
value in a logical relation, or as a binary value in an algebraic
relation. For example, to express that every two distinct tasks $i$ and $j$,
from a set of tasks $T$ with respective starting times $s_i,s_j$ and durations
$d_i, d_j$, should not overlap in time, we can use logical
disjunctions:
\begin{equation} \label{eq:logical.disjunctions}
(s_i + d_i \leq s_j) \vee (s_j + d_j \leq s_i), \; \forall i,j \in T, i \neq j.
\end{equation}
As another example, we can set a restriction such that no more than
half the variables from $x_1, \dots, x_n$ are assigned 
to a specific value $v$ as $\sum_{i=1}^n (x_i = v) \leq 0.5n$.

\paragraph{Global constraints}
 
In addition, constraint programming offers special
symbolic constraints that are called {\em global constraints}.
These constraints can be defined over an arbitrary set of 
variables, and encapsulate a combinatorial structure
that is exploited during the solving process. 
The constraint $\texttt{cp::AllDifferent}(x_1  \dots, x_n)$ is an example of such a global constraint.
This global constraint requires the variables $x_1  \dots, x_n$ to take
distinct values.

\paragraph{Solving}

The solving process underpinning constraint programming combines 
systematic search with inference techniques. The systematic
search implicitly enumerates all possible variable-value 
combinations, thus defining a search tree in which the 
root represents the original problem to be solved. At each
node in the search tree, an inference is made by means
of {\em domain filtering} and {\em constraint propagation}. 
Each constraint in the model has an associated domain filtering 
algorithm that removes provably inconsistent values from the 
variable domains. Here, a domain value is inconsistent if it
does not belong to any solution of the constraint.
The updated domains are then communicated
to the other constraints, whose domain filtering algorithms
in turn become active; this is the constraint propagation
process. In practice, the most effective filtering algorithms
are those associated with global constraints. Therefore, most
practical applications that are to be solved with constraint programming
are formulated using global constraints.

\paragraph{Application domains}

Constraint programming can be particularly effective with
highly combinatorial problem domains, such as timetabling or
resource-constrained scheduling.
For such problems an integer programming model
may be non-intuitive to express. Moreover, the associated
continuous relaxation may be quite weak, which makes it much
harder to find a provably optimal solution.
For example, again consider 
two tasks $A$ and $B$ that must not overlap in time. In integer 
programming one can introduce two binary variables, $y_{AB}$ and 
$y_{BA}$, representing that task $A$ must be processed either before or
after, task $B$. The non-overlapping constraint can 
then be expressed as $y_{AB} + y_{BA} = 1$, for which a
continuous linear relaxation may assign a solution $y_{AB} = 
y_{BA} = 0.5$, which is not very informative. 
In contrast, the non-overlapping requirement 
can be handled very effectively using a specific `sequential
resource' scheduling constraint in constraint programming
that effectively groups together all pairwise logical disjunctions
in~(\ref{eq:logical.disjunctions}) above. Such a constraint is also referred to 
as a disjunctive or unary constraint in the constraint programming
literature.

\paragraph{Designing models}

The expressiveness of constraint programming offers
a powerful modeling environment, albeit one that comes with a caveat.
Namely, that different syntactically equivalent formulations may 
yield quite different solving times.
For example, an alternative to the 
constraint \texttt{cp::AllDifferent}$(x_1, x_2, \dots, x_n)$ is its decomposition
into pairwise not-equal constraints $x_i \neq x_j$ for $1
\leq i < j \leq n$. The domain filtering algorithm for 
\texttt{cp::AllDifferent} provably removes more inconsistent domain values
than the individual not-equal constraints, which results in 
much smaller search trees and faster solution times.
Therefore, when designing a constraint programming model, 
one should be aware of the effect that different formulations 
can have on the solving time. In most situations, it is advisable
to apply global constraints to exploit their algorithmic power.

\subsection{Variables in constraint programming}

\paragraph{Variables of a constraint program}

A constraint programming problem is made up of discrete variables and
constraints over these discrete variables. A discrete variable is a 
variable that takes on a discrete value. 
%This subsection is on the variables of a constraint programming problem. 
AIMMS supports two base types of discrete variables for constraint programming. 
The first type of variable is the integer variable; an ordinary variable 
with a range formulated such as {\tt\{a..b\}} where 
{\tt a} and {\tt b} are numbers or references to parameters 
(see Chapter~\ref{chap:var}). Such variables can also be used in {\tt MIP} problems.
The second type of variable is the element variable, which will 
be further detailed in this section. This type of variable can
only be used in a constraint programming problem. 
These two types of variable can be combined in a third type, 
called an integer element variable, which supports the operations 
that are defined for both the integer variable and the element variable.

\subsubsection{ElementVariable declaration and attributes}
\declindex{ElementVariable} \AIMMSlink{element_variable}
%\index{element variable}\index{variable!element}

An element variable is a variable that takes an element as its value.
It can have the attributes specified in 
Table~\ref{table:cp.attr-element-variable}.
The attributes {\tt IndexDomain}, {\tt Priority}, 
{\tt NonvarStatus}, {\tt Text}, {\tt Comment} are the same as those for the
variables introduced in Chapter~\ref{chap:var}.


\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute}      & {\bf Value-type}          & {\bf See also} \\
\hline 
\verb|IndexDomain|  & {\em index-domain}        & \pageref{attr:var.index-domain} \\
\verb|Range|         & {\em named set}           & \\
\verb|Default|       & {\em constant-expression} & \pageref{attr:par.default}, \pageref{attr:var.default} \\
\verb|Priority|      & {\em expression}          & \pageref{attr:var.priority} \\
\verb|NonvarStatus| & {\em expression}          & \pageref{attr:var.nonvar}  \\
\verb|Property|      & {\tt NoSave}              & \pageref{attr:set.property}  \\
\verb|Text|          & {\em string}              & \pageref{attr:prelim.text} \\
\verb|Comment|       & {\em comment string }     & \pageref{attr:prelim.comment} \\
\verb|Definition|    & {\em expression}          & \pageref{attr:var.definition} \\
\hline\hline
\end{tabular}
\caption{{\tt ElementVariable} attributes}\label{table:cp.attr-element-variable}
\end{aimmstable}

\paragraph{The {\tt Range} attribute}
\declattrindex{element variable}{Range} 
\AIMMSlink{element_variable.range}
The range of an element variable is a one-dimensional set,
similar to the range of an element parameter. This attribute must
be a set identifier; and this permits the compiler to verify the semantics
when element variables are used in expressions.
This attribute is mandatory. 

\paragraph{The {\tt Default} attribute}
\declattrindex{element variable}{Default} 
\AIMMSlink{element_variable.default}
The attribute {\tt default} of an element variable is a quoted element. 
This attribute is not mandatory.

\paragraph{The {\tt Definition} attribute}
\declattrindex{element variable}{Definition} 
\AIMMSlink{element_variable.definition}
The {\tt Definition} attribute of an element variable is similar to the 
definition attribute of a variable, see also page~\pageref{attr:var.definition}, except that its value is an element 
and the resulting element must lie inside the range of the 
element variable.
This attribute is not mandatory.

\paragraph{The {\tt Property} attribute}
\declattrindex{element variable}{Property} 
\AIMMSlink{element_variable.property}
\declpropindex{element variable}{NoSave}
\declpropindex{element variable}{EmptyElementAllowed}
\AIMMSlink{EmptyElementAllowed}
The following properties are available to element variables:
\begin{description}
\item[{\bf Nosave}] When set, this property indicates that the element variable is not to be saved in cases.
\item[{\bf EmptyElementAllowed}]` When set, this property indicates that in a feasible solution, the value of 
      this variable can, but need not, be the empty element {\tt ''}.
      When the range of the element variable is a subset of the set {\tt Integers}, this property is not available.
      In the following example, for the element variable {\tt eV}, not selecting an element from {\tt S}, is a valid choice,
      but this choice forces the integer variable {\tt X} to 0.
      \begin{example}
      ElementVariable eV {
          Range       :  S;
          Property    :  EmptyElementAllowed;
      }
      Constraint Force_X_to_zero_when_no_choice_for_eV {
         Definition   :  if eV = '' then X = 0 endif;
      }
      \end{example}
\end{description}
This attribute is not mandatory.

\paragraph{Element translation}
Constraint programming solvers only use integer variables, and AIMMS translates an element variable, 
say {\tt eV} with range the set {\tt S} containing $n$ elements
into a integer variable, say {\tt v}, with range $\{ 0 .. n-1\}$. By design, this translation leaves no room for the empty element {\tt ''}, and 
subsequently, in a feasible solution, the empty element is no part of it. 
In order to permit the explicit consideration of the empty element as part of a solution, the property {\tt EmptyElementAllowed} 
can be set for {\tt eV}.  In that case the range of {\tt v} is $\{ 0 .. n\}$ whereby the value 0 corresponds to the empty element.
 


\subsubsection{Selecting a variable type}

\paragraph{Choosing variable type}

When there are multiple types of objects, such as integer variables 
and element variables in AIMMS, the following two questions naturally 
arise:
\begin{enumerate}
\item How to choose between the various types?
\item Can these types be combined?
\end{enumerate}
The answers to these questions are as follows:
\begin{enumerate}
\item You may want to base the choice of types of 
      variables on the operations that can be  
      performed meaningfully on these types. Which operation is appropriate
      for which variable type is described below.
\item An identifier can have both the 'integer variable' and 'element variable' types
      and is then called an 'integer element variable'. This is created as an element 
      variable with a named subset of the predeclared
      set {\tt Integers} as its range.
\end{enumerate}

\paragraph{Operations on variables}
\herelabel{cp.variable.operation}
The operations on variables that are interesting in constraint programming are:
\begin{itemize}
\item {\bf Numeric} operations, such as multiplication, addition, 
      and taking the absolute value.  These operations are 
      applicable to integer variables and to integer element variables.
\item {\bf Index} operations; selecting an element of an indexed 
      parameter or variable. An element variable {\tt eV} can be 
      an argument of a parameter {\tt P} or a variable {\tt X} in
      expressions such as {\tt P(eV)} or {\tt X(eV)}.\index{element constraint}\index{constraint!element}
      These operations are applicable to all element variables.
      In the Constraint Programming literature, such operations 
      are often implemented using so-called element constraints.
\item {\bf Compare, subtract, min and max} operations. These operations
      are applicable to all discrete variables, including element variables.
      For element variables, AIMMS uses the ordering of sets, see Section~\ref{sec:set.decl}.
\end{itemize}
All of the above operations are available with integer element
variables. 

\paragraph{Contiguous range}
In order to limit an element variable to a contiguous subset of
its named range, element valued suffixes {\tt .lower} and 
{\tt .upper} can be used.  In the example below, the assignment
to {\tt eV.Lower} restricts the variable {\tt eV} to the contiguous set {\tt\{c..e\}}.

\begin{example}
Set someLetters {
    Definition   : data { a, b, c, d, e };
}
ElementVariable eV {
    Range        : someLetters;
}
Procedure Restrict_eV {
    Body         : eV.lower := 'c';
}
\end{example}

The specification of non-contiguous ranges, informally known as ranges with holes,
is detailed in the next subsection.

\subsection{Constraints in constraint programming}
\label{cp:ss:global}

\paragraph{Introduction}

The constraints in constraint programming allow a rich variety of restrictions 
to be placed on the variables in a constraint program, ranging from 
direct domain restrictions on the variables to global constraints that
come with powerful propagation algorithms.

\paragraph{Domain restrictions}\index{Domain restrictions}

A domain restriction restricts the domain of a single variable, or of 
multiple variables, and is specified using the {\tt IN} operator. For
example, we can restrict the domain of an element variable {\tt eV} as follows:
\begin{example}
Constraint DomRestr1 {
    Definition   :  eV in setA;
}
\end{example}
When we apply the {\tt IN} operator to multiple variables, we 
can define a constraint by explicitly listing all tuples 
that are allowed. For example:
\begin{example}
Constraint DomRestr2 {
    Definition   :  (eV1, eV2, eV3) in ThreeDimRelation;
    Comment      : "ThreeDimRelation contains all allowed tuples";
}
\end{example}
\begin{example}
Constraint DomRestr3 {
    Definition   :  not( (eV1, eV2, eV3) in ComplementRelation );
    Comment      : "ComplementRelation contains all forbidden tuples";
}
\end{example}
In constraint {\tt DomRestr2} above, the three element variables are restricted 
to elements from the set of allowed tuples defined by {\tt ThreeDimRelation}.
Alternatively, we can define such a restriction using the complement, i.e.,
a list of forbidden tuples, as with constraint {\tt DomRestr3}. In constraint
programming, these constraints are also known as {\em Table} constraints;\index{table constraint}\index{constraint!table}
the data for these constraints resemble tables in a relational database.
\index{table constraint}


\paragraph{Algebraic restrictions}
The following operations are permitted on discrete variables, resulting
in expressions that can be used in constraint programming constraints:

\begin{enumerate}
\item The binary {\tt min(a,b)}, {\tt max(a,b)} and the iterative {\tt min(i,x(i))}, {\tt max(i,x(i))} can both be used,
\item multiplication {\tt *}, addition {\tt +}, subtraction {\tt -}, absolute value {\tt abs} and square {\tt sqr},
\item integer division {\tt div(a,b)}, integer modulo {\tt mod(a,b)}, 
\item floating point division {\tt /}, and
\item indexing: an element variable is used as an argument of another parameter or variable, {\tt P(eV)}, {\tt V(eV)},
\end{enumerate}
Note that the operation must be meaningful for the variable type, see page~\pageref{cp.variable.operation}.

These expressions can be compared, using the operators {\tt <=}, {\tt <}, {\tt =},
{\tt <>}, {\tt >}, and {\tt >=} to create algebraic restrictions. Simple examples of 
algebraic constraints, taken from Einstein's Logic Puzzle, are presented below.

\begin{example}
Constraint Clue15 {
    Definition   :  abs( Smoke('Blends') - Drink('Water') ) = 1;
    Comment      :  "The man who smokes Blends has a neighbor who drinks water.";
}
Constraint TheQuestion {
    Definition   :  National(eV)=Pet('Fish');
    Comment      :  "Who owns the pet fish? Result stored in element variable eV";
}
\end{example}


\paragraph{Combining restrictions}\index{constraint!meta}\index{meta constraints}

The constraints above can be combined to create other constraints called meta-constraints. 
Meta-constraints can be formed by using the scalar operators
{\tt AND}\index{AND}, {\tt OR}\index{OR}, {\tt XOR}\index{XOR}, {\tt NOT}\index{NOT} and {\tt IF-THEN-ELSE-ENDIF}. For example:
\begin{example}
Constraint OneTaskComesBeforeTheOther {
    Definition   : {
        ( StartA + DurA <= StartB ) or
        ( StartB + DurB <= StartA )
    }
}
\end{example}
In addition, restrictions can be combined into meta-constraints\index{meta constraint}\index{constraint!meta} using the iterative
operators {\tt FORALL}\index{Forall} and {\tt EXISTS}\index{Exists}. Moreover, restrictions can be counted 
using the iterative operator {\tt SUM} and the result compared with another value.
Finally, meta-constraints are restrictions themselves, they can be combined into 
even more complex meta-constraints. 
The following example is a variable definition, in which the collection of constraints 
{\tt (Finish(i) > Deadline(i))} is used to form a meta-constraint.
\begin{example}
Variable TotalTardinessCost {
    Definition  :  Sum( i, TardinessCost(i) | ( Finish(i) > Deadline(i) ) );
}
\end{example}
In the following example, the binary variable {\tt y} gets the value 1 if each {\tt X(i)} is greater than {\tt P(i)}.
\begin{example}
Constraint Ydef {
    Definition   :  y = FORALL( i, X(i) > P(i) );
}
\end{example}
From the Steel Mill example, we can model that we do not want more than two colors for each slab by the following nested usage of meta-constraints: 
\begin{example}
Constraint EnhancedColorCst {
    IndexDomain  :  (sl);
    Definition   :  sum( c, EXISTS (o in ColorOrders(c), SlabOfOrder(o)=sl)) <= 2;
}
\end{example}

\paragraph{Global constraints}

AIMMS supports the global constraints presented in Table~\ref{table:constraint.programming.special.restrictions}.  These 
global constraints come with powerful filtering techniques that may significantly reduce the search tree and thus the 
time needed to solve a problem. 

\begin{aimmstable}
\def\FI{\funcindex}\def\AL{\AIMMSlink}
\begin{tabular}{|l|l|}
\hline\hline
{\bf Global constraint} & {\bf Meaning} \\
\hline                                                                                    
{\tt cp::AllDifferent} ($i$, $x_i$) & The $x_i$ must have distinct values. $\forall i,j| i \neq j: x_i \neq x_j$ \\
{\tt cp::Count} ($i$, $x_i$, $c$, $\otimes$, $y$) & The number of $x_i$ related to $c$ is $y$. $\sum_i (x_i=c) \otimes y$ where \hspace{1cm}$\otimes \in \{\leq, \geq, =, >, <, \neq\}$ \\
{\tt cp::Cardinality} ($i$, $x_i$, $j$, $c_j$, $y_j$) & The number of $x_i$ equal to $c_j$ is $y_j$. $\forall j: \sum_i (x_i=c_j) = y_j$ \\
{\tt cp::Sequence} ($i$, $x_i$, $S$, $q$, $l$, $u$) & The number of $x_i\in S$ for each subsequence of length $q$ is between $l$ and $u$. $\forall i=1..n-q+1:$ $l \leq \sum_{j=i}^{i+q-1} (x_j\in S) \leq u$ \\
{\tt cp::Channel} ($i$, $x_i$, $j$, $y_j$) & Channel variable $x_i\to J$ to $y_j\to I$ $\forall i,j: x_i=j \Leftrightarrow y_j=i$ \\
{\tt cp::Lexicographic} ($i$, $x_i$, $y_i$) & $x$ is lexicographically before $y$ $\exists i: \forall j<i: x_j=y_j \wedge x_i<y_i$ \\
{\tt cp::BinPacking} ($i$, $l_i$, $j$, $a_j$, $s_j$) & Assign object $j$ of known size $s_j$ to bin $a_j \to I$. Size of bin $i \in I$ is $l_i$. $\forall i: \sum_{j \mid a_j = i} s_j \leq l_i$ \\
\hline
\hline\hline
\end{tabular}
\caption{Global constraints}\label{table:constraint.programming.special.restrictions}
\end{aimmstable}

The example below illustrates the use of the global constraint {\tt cp::AllDifferent} 
as used in the Latin square completion problem.  A Latin square of order $n$ is an $n\times n$ matrix where the values are in the 
range $\{1..n\}$ and distinct over each row and column.

\begin{example}
Constraint RowsAllDifferent {
    IndexDomain  :  r;
    Definition   :  cp::AllDifferent( c, Entry(r, c) );
}
Constraint ColsAllDifferent {
    IndexDomain  :  c;
    Definition   :  cp::AllDifferent( r, Entry(r, c) );
}
\end{example}

Additional examples of global constraints are present in the AIMMS Function Reference. 
Unless stated otherwise in the function reference, global constraints can also be used outside of
constraints definitions, for example in assignments or parameter definitions.

\paragraph{Global constraint vector arguments}
These global constraints have vectors as arguments.  The size of a vector is defined by a preceding 
index binding argument. Further information on index binding can be found in the Chapter on Index Binding~\ref{chap:bind}.
Such a vector can be a vector of elements, for example the fourth argument of {\tt cp::Cardinality}.
In a vector of elements, the empty element {\tt ''} is not allowed; comparison of an element variable against the empty element is not supported.

\paragraph{Basic scheduling constraints}

AIMMS offers support for both basic scheduling and advanced scheduling. Advanced scheduling will be
detailed in the next section but, for basic scheduling, AIMMS offers the following two global constraints:

\begin{enumerate}
\item The global constraint {\tt cp::SequentialSchedule}($j$, $s_j$, $d_j$, $e_j$) ensures that two 
      distinct jobs do not overlap where job $j$ has start time $s_j$, duration $d_j$ 
      and end time $e_j$.  This constraint is equivalent to: 
      \begin{itemize}
      \item $\forall i,j,i \neq j:(s_i+d_i\leq s_j) \vee (s_j + d_j \leq s_i)$. 
      \item $\forall j: s_j + d_j = e_j$
      \end{itemize}
      This and similar constraints are also known 
      as {\tt unary}\index{unary} or {\tt disjunctive}\index{disjunctive} constraints within the Constraint Programming literature.
      \funcindex{cp::SequentialSchedule}\AIMMSlink{cp::SequentialSchedule}
\item The global constraint {\tt cp::ParallelSchedule}($l$, $u$, $j$, $s_j$, $d_j$, $e_j$, $h_j$) allows
      a single resource to handle multiple jobs, within limits $l$ and $u$, at the same time. 
      Here job $j$ has start time $s_j$, duration $d_j$, end time $e_j$ and resource consumption (height) $h_j$.
      This constraint is equivalent to:
      \begin{itemize}
      \item $\forall t: l\leq\sum_{j|s_j\leq{}t<e_j} h_j\leq u$. 
      \item $\forall j: s_j + d_j = e_j$
      \end{itemize}
      This and similar constraints are also known 
      as {\tt cumulative}\index{cumulative} constraints within the Constraint Programming literature.
      \funcindex{cp::ParallelSchedule}\AIMMSlink{cp::ParallelSchedule}
\end{enumerate}


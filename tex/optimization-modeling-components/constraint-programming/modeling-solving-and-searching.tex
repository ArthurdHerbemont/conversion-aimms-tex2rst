\section{Modeling, solving and searching}

\paragraph{Introduction}

In this section we explain how constraint programming
models are formulated and solved in AIMMS.  
We start by explaining how constraint programming formulations are fit 
into the paradigms of AIMMS like the free objective and units of measurement.
We then explain the different
mathematical programming types associated to constraint programming,
and finally we discuss how a user can modify the search 
procedure in AIMMS.

\paragraph{The free objective}

By design, the objective of any mathematical program in AIMMS is a {\em free} variable, even when 
it can be deduced that the objective function is always integer valued for a feasible solution.
However, for an infeasible solution, AIMMS will assign the special value {\tt NA} to the objective variable, 
in order to emphasize that a feasible solution is not available.
Having a single variable for the objective function is convenient when 
communicating its value in the progress window and other places of AIMMS.

\subsection{Constraint programming and units of measurement}

\paragraph{Integer coefficients only}

Constraint programming solvers require the coefficients used in constraints 
to be integer; fractional values or special values such as {\tt -inf}, {\tt zero}, {\tt na}, 
{\tt undf}, or {\tt inf} are not allowed. In applications where the choice of base units is free, 
fractional values are easily avoided by 
choosing the base unit of quantities, and the units of variables and 
constraints such that all amounts to be considered
are integer multiples of those base units, as detailed in Section~\ref{sec:units.scaling.mp}.  
As an example, consider a simple constraint stating that the integer variable {\tt y} is 0.9[m] 
away from the integer variable {\tt x}.  Both variables are multiples of the derived unit dm, i.e., 0.1[m].
The variables and constraints are declared as follows:
\begin{example}
Variable x {
    Range       :  integer;
    Unit        :  dm;
}
Variable y {
    Range       :  integer;
    Unit        :  dm;
}
Constraint y_away_from_x {
    Unit        :  dm;
    Definition  :  y = abs( x - 0.9[m] );
}
\end{example}

Using the unit {\tt m} as a base unit, this will lead to fractional values, as can be seen in
the constraint listing:

\begin{example}
y_away_from_x .. [ 1 | 1 | before ]

    y =
    abs((x-0.9))  ****

    name            lower           level           upper           scale
    x                   0               0 21474836470.000           0.100
    y                   0               0 21474836470.000           0.100
\end{example}

The coefficient for distance, 0.9[m], is in meters and the variables {\tt x} and {\tt y}
have the same units inside the row.  This row is scaled back to {\tt dm} afterwards. As a result of all this
scaling, the computations go from the domain of integer arithmetic into the domain of 
floating point arithmetic.  For constraint programming, we need to avoid the domain of floating point arithmetic.

\paragraph{Adapted base unit}

We will continue the above example, by adapting the base unit such that all amounts are integral multiples
of that base unit; we select {\tt dm} as a base unit.  This will lead to the following row communicated 
to the solver:

\begin{example}
y_away_from_x .. [ 1 | 1 | before ]

    y =
    abs((x-9))  ****

    name       lower      level      upper
    x              0          0 2147483647
    y              0          0 2147483647
\end{example}

Observe that scaling is not needed anymore.  
Using the scaling based on {\tt dm} instead of {\tt m} will keep all computations during
the solution process in the domain of integer arithmetic.


\paragraph{Quantity based scaling}

In the example of the previous paragraph, the base unit was adapted to the needs of 
constraint programming; namely to stay within the domain of integer arithmetic.
For multi-purpose applications, freedom of choosing the base units of quantities
according to the needs of a constraint programming problem is not always available.
In order to stay within the domain of integer arithmetic, we can associate a convention 
with the mathematical program and 
filling in the {\tt per quantity} attribute, see also Section~\ref{sec:units.convention}. 
By filling in the {\tt per quantity} attribute, AIMMS will generate the mathematical program
where all coefficients are scaled with respect to the units specified in the {\tt per quantity} attribute. 
Let us continue the example of the previous paragraph using {\tt m} as the base unit and adding
a convention to the mathematical program.

\begin{example}
Quantity SI_Length {
    BaseUnit     :  m;
    Conversions  : {
        dm -> m : # -> # / 10,
        cm -> m : # -> # / 100
    }
    Comment      :  "Expresses the value of a distance.";
}
Parameter LengthGranul {
    InitialData  :  10;
}
Convention solveConv {
    PerQuantity  :  SI_Length : LengthGranul * cm;
}
MathematicalProgram myCP {
    Direction    :  minimize;
    Constraints  :  AllConstraints;
    Variables    :  AllVariables;
    Type         :  Automatic;
    Convention   :  solveConv;
}
\end{example}

Again, AIMMS will generate the constraint such that only integer arithmetic is needed.  
The constraint listing of that constraint is similar to the constraint listing presented 
in the paragraph {\em adapted base unit} above and not repeated.
Note also that with conventions, we can now use parameters to further control the scaling; 
if we want to change the model such that we can use multiples of {\tt 20[cm]} instead of 
multiples of {\tt 10[cm]}, we only need to change the value of {\tt LengthGranul}.

\paragraph{Calendar used for timeline}
 
Scheduling applications in which the schedule domain is based on a
calendar, the length of a timeslot is equal to the unit of the calendar, 
see Section~\ref{sec:time.calendar}. The time quantity is overridden, as if an entry in the {\tt per quantity} 
attribute of the associated convention is given, selecting the calendar unit.  Even if no convention was
associated with the mathematical program. In short, for scheduling applications,
AIMMS will scale time based data according to the length of a timeslot.


\subsection{Solving a constraint program}

\paragraph{Mathematical Programming types}

AIMMS distinguishes two types of mathematical programs
that are associated with constraint programming models:
{\tt COP} for constraint optimization problems, and 
{\tt CSP} for constraint satisfaction problems.
Both {\tt COP} and {\tt CSP} are exact in that 
{\tt COP} provides a proven optimal solution
while {\tt CSP} provides a solution, or proves that none exist,
if time permits.

\paragraph{Limiting solution time}

Constraint programming problems are combinatorial problems and therefore may 
take a long time to solve, especially when trying to prove optimality. In order 
to avoid unexpectedly long solution times, 
you can limit the amount of time allocated to the solver for solving your 
problem as follows:

\begin{example}
    solve myCOP where time_limit := pMaxSolutionTime ; 
    ! pMaxSolutionTime is in seconds.
\end{example}

Alternatively, when you are satisfied with the current objective, 
as presented in the progress window, and not want to 
wait on further improvements, you can interrupt the solution process by using the 
key-stroke {\em ctrl-shift-s}.




\subsection{Search Heuristics}

\paragraph{Search heuristics}

During the solving process, constraint programming employs
search heuristics that define the shape of the search tree,
and the order in which the search tree nodes are visited.
The shape of the search tree is typically defined by the 
order of the variables to branch on, and the corresponding
value assignment. AIMMS\ allows the user to specify which variable
and value selection heuristics are to be used.
For example, to decide the next variable on which to branch, a commonly used 
search heuristic is to choose a non-fixed variable with the minimum 
domain size, and assign its minimum domain value.

\paragraph{Search phases}

The first method offered by AIMMS to influence 
the search process is through using the {\tt Priority}
attributes of the variables.  AIMMS will group together all variables that have
the same priority value, and each block of variables
will define a {\em search phase}. That is, the solver will 
first assign the variables in the block with the highest priority,
then choose the next block, and so on. 
As discussed in Section~\ref{sec:var.var.solver-attr}, the highest 
priority is the one with the highest positive value. 
Defining search phases can be
very useful. For example, when scheduling activities to various
alternative resources, it is natural to first assign an activity
to its resource before assigning its begin.


\paragraph{Variable and value selection}

The variable and value selection heuristics offered by AIMMS are
presented in Table~\ref{table:constraint.programming.search.heuristics}.
They can be accessed through the `solver options' configuration window.
As an example, we can define a `constructive' scheduling heuristic 
that builds up the schedule from the begin of the schedule domain
by using {\tt MinValue} as the variable selection, and {\tt Min}
as the value selection. Indeed, this heuristic will attempt to greedily
schedule the activities as early as possible.
Note that both these variable and value heuristics apply to the entire
search process. If no variable priorities are specified, the variable 
selection heuristic will consider all variables at a time. Otherwise,
the variable selection heuristic is applied to each block individually.


\begin{aimmstable}
\begin{tabular}{|l|l|}
\hline\hline
{\bf Heuristic}      & {\bf Interpretation}    \\
\hline
Variable selection:  &  choose the non-fixed variable with: \\
{\tt Automatic}      &  use the solver's default heuristic \\
{\tt MinSize}        &  the smallest domain size \\
{\tt MaxSize}        &  the largest domain size \\
{\tt MinValue}       &  the smallest domain value \\
{\tt MaxValue}       &  the largest domain value \\
\hline
Value selection:     &  assign:\\
{\tt Automatic}      &  use the solver's default heuristic \\
{\tt Min}            &  the smallest domain value \\
{\tt Max}            &  the largest domain value \\
{\tt Random}         &  a uniform-random domain value  \\
\hline\hline
\end{tabular}
\caption{Search heuristics}\label{table:constraint.programming.search.heuristics}
\end{aimmstable}


%\paragraph{Other search options}

%The solver IBM ILOG CP Optimizer 12.2 offers several additional search 
%options, including multi-point search and restart search (deafult).
%It may be good to offer these through AIMMS\ for completeness.



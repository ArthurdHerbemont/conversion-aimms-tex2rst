\section{Basic algorithm}

\paragraph{Algorithm in words}

The algorithm solves an alternating sequence of mixed-integer linear models and
nonlinear models.
\begin{enumerate}
\item First, the entire model is solved as a nonlinear program with all the integer variables relaxed as
continuous variables between their bounds.
\item Then a linearization is carried out around the optimal
solution, and the resulting constraints are added to the linear constraints
that are already present. This new linear model is referred to as the master
MIP model.
\item The master MIP problem is solved as an mixed-integer linear program.
\item The integer part of the resulting optimal solution is then temporarily fixed, and the original MINLP
model with fixed integer variables is solved as a nonlinear subproblem.
\item Again, a linearization around the
optimal solution is constructed and the new linear constraints are added to the
master MIP problem.  To prevent cycling, one or more constraints are added to cut
off the previously-found integer solution of the master problem.
\item Steps 3--5 are repeated until one of the termination criteria is satisfied.
\end{enumerate}
A more detailed description of the general Outer Approximation algorithm can be found in \cite{bib:DG86}.

\paragraph{Convexity and convergence}

As linearizations are added to the master MIP problem, the model becomes an
improved approximation of the original MINLP model. Using the usual convexity
assumption regarding the nonlinear subproblem, convergence to a global optimum
occurs when the objective function value of the master MIP problem is worse than
the value associated with the NLP subproblem.


\paragraph{Termination $\ldots$}

Several termination criteria are used in practice.  These criteria can be used
in isolation or in some logical combination.  Three of them are discussed in
the following paragraphs.

\paragraph{$\ldots$ iteration limit}

Perhaps the most frequently-used criterion is the iteration limit. One reason
is that a good solution is usually found during the first few iterations.
Another reason for using an iteration limit is that the size of the underlying
master MIP problem tends to grow significantly each time linearization
constraints are added, causing an increase in computation time.

\paragraph{$\ldots$ objective worsening}

A second criterion is the worsening of the objective function value of two
successive nonlinear subproblems.  This worsening occurs quite frequently, even
if the NLP subproblem is convex. The underlying reason is that the master MIP
problem will not always select binary solutions that lead to successively
improving NLPs.  This criterion seems appropriate when the worsening is
persistent over several iterations.


\paragraph{$\ldots$ crossover}

A third termination criterion is insufficient improvement in the objective
function value of the master MIP problem in relation to the objective function
value of the previously solved NLP subproblem.  The difference between these two
values represents the optimality gap, since the master MIP problem represents an
outer approximation (thus a relaxation) of the original MINLP model. When the
gap is closed at crossover, the optimal solution has been found provided the
NLP subproblem is convex.


\paragraph{Final solution}

Upon termination of the algorithm, the known best solution (also referred to as
the incumbent solution) is declared as the final solution.  In many practical
applications, this solution is not necessarily optimal due to termination based
on an iteration limit. In addition, it is often not possible to verify that the
NLP subproblem is convex.

\paragraph{Linearizations}

The term `outer approximation' refers to the linear approximation of the convex
nonlinear constraints at selected points along the boundary of the convex
solution region.  The accumulation of such inequality constraints forms an
outer approximation of the solution region, and this approximation can be used
in the optimization rather than the nonlinear constraints from which it was
derived.  The formula for the linearization of a scalar nonlinear inequality
$g(x,y) \leq 0$ around the point $(x,y) = (x^0,y^0)$ is as follows.

\begin{displaymath}
g(x^0,y^0) + \bigtriangledown g(x^0,y^0)^T
\begin{bmatrix}
x - x^0 \\
y - y^0
\end{bmatrix}
\leq 0
\end{displaymath}

\paragraph{The nonconvex case}

The linear approximation ceases to be an outer approximation if the solution
region is not convex.  In this situation there is the possibility that portions
of the solution region are cut off as illustrated in
Figure~\ref{fg:aoa:nonconvex}.



\begin{figure}
\begin{center}
\psset{xunit=0.5cm,yunit=0.5cm}
\begin{pspicture}(0,0)(11,9)
    \psaxes[labels=none,ticks=none](0,0)(11,0)(0,9)
    \pscustom[linestyle=none,fillstyle=solid,fillcolor=lightgray]{
        \pscurve{-}(0.4,5.15)(2,6.5)(4,7)(5,6)(5.5,4.5)(6,3.8)(7,4)(8.5,4.2)(9.4,2.9)
        \psline(9.4,2.9)(5.0,1.4)
        \psline(5.0,1.4)(0.5,5.15)
    }
    \pscurve{-}(0.3,5)(2,6.5)(4,7)(5,6)(5.5,4.5)(6,3.8)(7,4)(8.5,4.2)(9.5,2.5)
    \psline(0.1,5.5)(6,0.5)
    \psline(4,1)(11,3.5)
    \psline[linestyle=dashed](1,8)(10.5,3.2)
    \psline[linestyle=dashed](1.5,8.99)(11,4.19)
    \psline{->}(5.75,5.6)(6.25,6.59)
\end{pspicture}
\end{center}
\caption{Effect of loosening a linearization} \label{fg:aoa:nonconvex}
\end{figure}

\paragraph{Loosening inequalities}

In practical implementations of the outer approximation algorithm, the
linearizations are allowed to move away from the feasible region. Such
heuristic flexibility allows solutions to be found that would otherwise have
been cut off.  The implementation allows deviations through the use of
artificial nonnegative variables and then penalizing them while solving the
master problem.

\paragraph{Open solver approach}

The basic outer approximation algorithm that is part of the {\sc aoa} module
has been completely implemented using functionality provided by the {\gmp}
library.
\begin{itemize}
\item From the math program instance representing the original MINLP model, a new math program instance
representing the initial master MIP problem can be created using the function
{\tt GMP::Instance::CreateMasterMIP}.
\item The functions from the {\tt GMP::Linearization} namespace can be used to add linearizations of
the nonlinear constraints of the original MINLP model to the master MIP, in a
customizable manner.
\item Using the {\tt GMP::Instance::FixColumns} procedure, the integer columns of the nonlinear subproblem
can fixed to the current integer solution of the master MIP.
\item Using the {\tt GMP::Instance::AddIntegerEliminationRows} procedure, prior integer solutions of the master
MIP are excluded from subsequent solves.
\end{itemize}



\section{Control parameters that influence the AOA algorithm} \label{sec:aoa.control.par}

\paragraph{Control parameters}

The multistart module defines several parameters that influence the outer approximation algorithm.
These parameters have a similar functionality as options of a solver, e.g., {\sc Cplex}. The most
important parameters, with their default setting, are shown in Table~\ref{table:aoa.controlparam}.
\begin{aimmstable}
\begin{tabular}{|l|l|l|l|}
\hline\hline
{\bf Parameter} &  {\bf Default} &  {\bf Range} &  {\bf Subsection} \\
\hline
{\tt IterationMax}                    & 20    & \{0,maxint\} & \\
{\tt TimeLimit}                       & 0     & \{0,maxint\} & \ref{sec:aoa.time.limit} \\
{\tt CreateStatusFile}                & 0     & \{0,1\}      & \\
{\tt UsePresolver}                    & 1     & \{0,1\}      & \ref{sec:aoa.presolver} \\
{\tt UseMultistart}                   & 0     & \{0,1\}      & \ref{sec:aoa.multistart} \\
{\tt TerminateAfterFirstNLPIsInteger} & 1     & \{0,1\}      & \ref{sec:aoa.terminate.first} \\
{\tt IsConvex}                        & 0     & \{0,1\}      & \ref{sec:aoa.convex} \\
{\tt RelativeOptimalityTolerance}     & 1e-5  & \{0,1\}      & \ref{sec:aoa.convex} \\
{\tt NLPUseInitialValues}             & 1     & \{0,1\}      & \ref{sec:aoa.nlp.use.initial} \\
\hline\hline
\end{tabular}
\caption{Control parameters in the outer approximation module}\label{table:aoa.controlparam}
\end{aimmstable}
The parameters that are not self-explanatory are explained in this section;
the last column in the table refers to the subsection that discusses the corresponding parameter.

\subsection{Specifying a time limit} \label{sec:aoa.time.limit}

\paragraph{Parameter {\tt Time\-Limit}}

The parameter {\tt Time\-Limit} can be used to set a limit on the total elapsed time
(in seconds) used by the outer approximation algorithm. The default value of 0 has a special
meaning; in that case there is no time limit.

\subsection{Using the AIMMS Presolver} \label{sec:aoa.presolver}

\paragraph{Parameter {\tt Use\-Presolver}}

By default the outer approximation algorithm starts by applying the AIMMS Presolver
to the MINLP model. By preprocessing the MINLP model, the model might become smaller and
easier to solve. The parameter {\tt Use\-Presolver} can be used to switch off the preprocessing
step.

\subsection{Combining outer approximation with multistart} \label{sec:aoa.multistart}

\paragraph{Parameter {\tt Use\-Multi\-start}}

If the parameter {\tt Use\-Multi\-start} is switched on then the outer approximation
algorithm will use the multistart algorithm to solve the nonlinear subproblems. For
non-convex models this can have a positive effect on the quality of the solution that is
returned by the outer approximation algorithm. The multistart algorithm is described
in section \ref{sec:nlp.multistart}. The parameters {\tt Multistart\-Number\-Of\-Sample\-Points}
and {\tt Multistart\-Number\-Of\-Selected\-Sample\-Points} can be used to specify the
number of sample and selected sample points, respecively, as used by the multistart
algorithm.

\paragraph{Multistart module}

To use the multistart algorithm, the system module {\tt Multi Start} should be added to your
project. You can install this module using the {\bf Install System Module} command in the
AIMMS {\bf Settings} menu.

\subsection{Terminate if solution of relaxed model is integer} \label{sec:aoa.terminate.first}

\paragraph{Parameter {\tt Terminate\-After\-First\-NLP\-Is\-Integer}}

By default the outer approximation algorithm will terminate if it finds an integer solution for
the initial NLP problem, which is obtained from the MINLP model by relaxing the integer variables.
By switching off the parameter {\tt Termin\-ate\-After\-First\-NLP\-Is\-Integer} you can enforce
the algorithm to continue.

\subsection{Solving a convex model} \label{sec:aoa.convex}

\paragraph{Parameter {\tt Is\-Convex}}

The parameter {\tt Is\-Convex} can be used to indicate that the model is convex.
In that case the outer approximation algorithm will no longer stop after the 
iteration limit is hit, as specified by the parameter {\tt Iteration\-Max}.
Instead, the algorithm will stop if the gap between the objective values of
the master MIP problem and the nonlinear subproblem is sufficiently small, as
controlled by the parameter {\tt Rela\-tive\-Opti\-mality\-To\-le\-ran\-ce}.
Note that AIMMS cannot identify whether a model is convex or not.

\subsection{Starting point strategy for NLP subproblems} \label{sec:aoa.nlp.use.initial}

\paragraph{Parameter {\tt NLP\-Use\-Initial\-Values}}

The parameter {\tt NLP\-Use\-Initial\-Values} specifies the starting point strategy used
for solving the NLP subproblems. For nonconvex nonlinear problems the starting point
often has a big influence on the solution that the NLP solver will find. By default the
{\sc aoa} algorithm will use the initial values as provided by the user for all NLP
subproblems that are solved. By setting this parameter to 0, the algorithm will use
the solution of the previous master MIP problem as the starting point for the next NLP
subproblem (and for the initial NLP it will use the initial values provided by the user).
Note: if one of the parameters {\tt Use\-Multi\-start} or {\tt Is\-Convex} equals 1 then
{\tt NLP\-Use\-Initial\-Values} is automatically set to 0.



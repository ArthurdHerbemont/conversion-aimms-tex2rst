\section{Stochastic parameters and variables}\label{sec:stoch.stoch}

\paragraph{The set {\tt All- Stochastic- Scenarios}}
\presetindex{AllStochasticScenarios}

To allow the storage of scenario-dependent parameter and variable data for multiple scenarios in a stochastic model, all such scenarios should be added to the predefined set {\tt AllStochastic\-Scenarios}. If your application contains multiple stochastic models---each with different scenario sets---the set {\tt AllStoch\-asticScenarios} should be the union of all these scenario sets. For each stochastic model you can then define an associated subset of {\tt AllStochasticScenarios} to use with that particular stochastic model.

\paragraph{Stochastic parameters}
\declpropindex{parameter}{Stochastic}\suffindex{parameter}{Stochastic}

Stochastic events are modeled in AIMMS as numeric {\tt Parameters} for which the {\tt Stochastic} property has been set (see also Section~\ref{sec:par.decl}).  For stochastic parameters AIMMS provides an additional {\tt .Stochastic} suffix, which you can use to store scenario-dependent stochastic event outcomes. The data stored in the suffix is used by AIMMS when generating the stochastic model. The index domain of the {\tt .Stochastic} suffix is, therefore, the set {\tt AllStochasticScenarios} plus the original domain of the parameter.

\paragraph{Example}

Consider the following declarations
\begin{example}
Set MyScenarios {
    SubsetOf     : AllStochasticScenarios;
    Index        : sc;
}
Parameter Demand {
    IndexDomain  : (c,t);
    Property     : Stochastic;
}
\end{example}
These declarations will cause AIMMS to create a {\tt .Stochastic} suffix for the parameter {\tt Demand(c,t)}. To use, or assign values to, {\tt Demand.Stochastic}, you must use an additional index into (a subset of) {\tt AllStochasticScenarios}. The following statement provides an example of such a statement.
\begin{example}
    Demand.Stochastic(sc,c,t) := Uniform(10,20);
\end{example}
If a constraint contains a reference to the parameter {\tt Demand}, AIMMS will use the data in {\tt Demand.Stochastic} to generate the appropriate demand constraint for every scenario.

\paragraph{Stochastic variables}
\declpropindex{variable}{Stochastic}

By setting the {\tt Stochastic} property for a {\tt Variable} in your model, you indicate to AIMMS that this variable may have multiple, scenario-dependent, solutions when used in a stochastic model. Consequently, when generating a matrix for the stochastic model, a column will be generated conceptually for every single scenario.

\paragraph{The {\tt Stage} attribute}
\declattrindex{variable}{Stage}

For stochastic variables you must also specify the mandatory {\tt Stage} attribute. Through the {\tt Stage} attribute you specify the stage at the end of which the decision corresponding to the stochastic variable is to be taken. The value of the {\tt Stage} attribute must be an explicit positive integer value, or a parameter reference involving some or all of the indices on the index list of the declared variable.

\paragraph{Non-anti\-cipativity constraints\dots}
\index{non-anticipativity constraint}\index{constraint!non-anticipativity}

As discussed in the previous section, for every scenario $s_0$, a stochastic variable $x$ gets its value $x_{s_0}$ at the end of stage $n$ as specified in the {\tt Stage} attribute of the variable.  In addition, its value is based on the specific outcomes of the stochastic events for that scenario taking place during stages $1,\dots,n$, but only on the distribution of the stochastic event outcomes for any further stages. Therefore, the value $x_s$ must be equal to $x_{s_0}$ for every other scenario $s$ that passes through the same node in the scenario tree at the end of stage $n$ as $s_0$. The constraints enforcing this equality are called {\em non-anticipativity constraints}---they do not allow the solution to anticipate on stochastic outcomes that lie beyond the stage as specified by the {\tt Stage} suffix.

\paragraph{\dots enforced explicitly or implicitly}

When generating a stochastic model, AIMMS will automatically enforce the non-anticipativity constraints, either by explicitly adding them to the generated matrix, or implicitly by substituting a single representative $x_{s_0}$ for every other variable $x_s$. While enforcing non-anticipativity in an implicit manner will drastically reduce the matrix size, an explicit representation may be helpful for solvers able to decompose the generated matrix.

\paragraph{Non-stochastic variables}

If a variable in a stochastic model has not been declared stochastic, it is deterministic in the sense that it assumes the same value for every scenario, as is the case with first stage variables.

\paragraph{The {\tt .Stochastic} suffix for variables}
\suffindex{variable}{Stochastic}

Variables can also have a {\tt .Stochastic} suffix in AIMMS. It follows the same rules for its index domain as the {\tt .Stochastic} suffix of parameters. AIMMS uses the {\tt .Stochastic} suffix of variables to store the solution data of a stochastic model after solving it. However, contrary to stochastic parameters, AIMMS will not only create the {\tt .Stochastic} suffix for stochastic variables, but for {\em all} variables that are involved in a stochastic model.

\paragraph{Contents of {\tt .Stochastic} suffix}

The values stored in the {\tt .Stochastic} suffix after solving a stochastic model for each type of variable are as follows:
\begin{itemize}
\item for stochastic variables, the {\tt .Stochastic} suffix will contain the solution of the variable for each scenario,
\item for the objective variable, the {\tt .Stochastic} suffix will contain the contribution to the objective of each scenario, as well as the weighted objective value of the stochastic model itself,
\item for any other non-stochastic variable, the {\tt .Stochastic} suffix will contain the deterministic solution of that variable for the stochastic model.
\end{itemize}
As the solution of a stochastic model is entirely stored in the {\tt .Stochastic} suffix, the solution of the underlying deterministic model remains completely intact after solving the stochastic model. This makes it easy to visually, and/or programmatically, compare the solutions of the deterministic and stochastic model.

\paragraph{Non-stochastic solution data}

As the objective value and solution of the non-stochastic variables of the stochastic model cannot be coupled directly with one specific scenario in the scenario set, AIMMS creates an extra element in the set {\tt AllStochastic\-Scenarios} for this purpose. You must specify the name of this element when solving the stochastic model (see also Section~\ref{sec:stoch.solve}).


\section{Solving stochastic models}\label{sec:stoch.solve}

\paragraph{Solving stochastic models}

After generating stochastic event data and a scenario tree, you can generate and solve the stochastic model by using methods from the {\sc GMP} library discussed in Chapter~\ref{chap:gmp}. AIMMS supports two methods for solving a stochastic model:
\begin{itemize}
\item by solving its {\em deterministic equivalent}, or
\item for purely linear mathematical programs only, through the {\em stochastic Benders algorithm}, or
\item using the Benders decomposition algorithm in {\sc Cplex}~12.7 or higher.
\end{itemize}
Both Benders algorithms will decompose the stochastic model into multiple smaller models, and thus is better suited to solve stochastic models where the deterministic equivalent, either by the size of the deterministic model or because of a huge number of scenarios, becomes too big or time-consuming to solve at once.
The Benders decomposition algorithm in {\sc Cplex} can be used to solve stochastic models with integer variables, as long as all integer variables are assigned to the first stage. For more information see the {\sc Cplex} option {\tt Benders\_strategy}.

\subsection{Generating and solving the deterministic equivalent}
\index{stochastic programming!deterministic equivalent}

\paragraph{Generating a stochastic model}
\procindexns{GMP::Instance}{GenerateStochasticProgram}

The method for generating a stochastic model for a {\tt MathematicalProgram} {\em MP} is
\begin{itemize}
\item {\tt GMP::Instance::GenerateStochasticProgram}(
\\\hbox to 1cm{\hfill}{\em MP},
                {\em StochasticParameters},
                {\em StochasticVariables},
\\\hbox to 1cm{\hfill}{\em Scenarios},
                {\em ScenarioProbability},
                {\em ScenarioTreeMap},
\\\hbox to 1cm{\hfill}{\em DeterministicScenarioName}[,
                {\em GenerationMode}][,
                {\em Name}])
\end{itemize}
The function returns an element into the set {\tt AllGeneratedMathematicalPro\-grams}. This generated math program instance contains a memory-efficient representation of the technology matrix of the stochastic model and the stochastic event data, and can be used to create a deterministic equivalent of the stochastic model, as well as the submodels necessary for a stochastic Benders approach.

\paragraph{Specifying stochastic identifiers}

Through the arguments {\em StochasticParameters} and {\em StochasticVariables} you indicate to AIMMS which stochastic parameters and variables you want to take into consideration when generating this stochastic model. These arguments must be subsets of the predefined sets {\tt AllStochasticParameters} and {\tt AllStochastic\-Variables}, respectively.  You may want to use real subsets, for  instance, when your AIMMS project contains multiple stochastic models, each referring only to a subset of the stochastic parameters and variables.

\paragraph{Specifying scenarios}

Through the {\em Scenarios}, {\em ScenarioProbability} and {\em ScenarioTreeMap} arguments you specify the set of scenarios, their probabilities and the mapping defining the scenario tree for which you want to generate the stochastic model to AIMMS. Through the string argument {\em DeterministicScenarioName}, you supply the name of the artificial element that AIMMS will add to the predefined set {\tt AllStochas\-ticScenarios} (if not created already), and use to store the solution of non-stochastic variables in their respective {\tt .Stochastic} suffices as explained in Section~\ref{sec:stoch.stoch}.

\paragraph{Enforcing non-anticipativity constraints}

Using the {\em GenerationMode} argument you can specify whether you want AIMMS to explicitly add the non-anticipativity constraints to your stochastic model, or whether you want non-anticipativity to be enforced implicitly by substituting the representative scenario for every non-representative scenario at every stage. {\em GenerationMode} is an element parameter into the predefined set {\tt AllStochasticGenerationModes}, with possible values
\begin{itemize}
\item {\tt 'CreateNonAnticipativityConstraints'}, and
\item {\tt 'SubstituteStochasticVariables'} (the default value).
\end{itemize}

\paragraph{Name argument}

With the optional {\em Name} argument you can explicitly specify a name for the generated mathematical program. If you do not choose a name, AIMMS will use the name of the underlying {\tt MathematicalProgram} as the name of the generated mathematical program as well. Please note, that AIMMS will also use this name as the default name for solving the deterministic model. Therefore, if you do not want the generated mathematical program of the deterministic model to be deleted, then you have to choose a non-default name.

\paragraph{Solving the deterministic equivalent of a stochastic model}
\procindexns{GMP::Instance}{Solve}

You can solve a stochastic model by using the regular {\sc gmp} procedure
\begin{itemize}
\item {\tt GMP::Instance::Solve}({\em gmp})
\end{itemize}
By applying this function to a generated mathematical program associated with a stochastic model, AIMMS will create the deterministic equivalent and pass it to the appropriate LP/MIP solver. The {\tt GMP::Instance::Solve} me\-thod is discussed in full detail in Section~\ref{sec:gmp.instance}.

\paragraph{Changing the model input}

Note that, when you adjust the scenario tree map, the stochastic data, the scenario probabilities, or the value of the {\tt Stage} attribute of some variables after you generated the stochastic model, you should regenerate the stochastic model again to reflect these changes.

\paragraph{Example}

Consider the following call to {\tt GMP::Instance::GenerateStochasticProgram}
\begin{example}
    GMP::Instance::GenerateStochasticProgram(
        TransportModel, AllStochasticParameters, AllStochasticVariables,
        MyScenarios, MyScenarioProbability, MyScenarioTreeMap,
        "TransportModel", 'SubstituteStochasticVariables', "StochasticTransportModel");
\end{example}
After solving the generated stochastic model, its solution will be stored as follows, where {\tt sc} is an index into {\tt MyScenarios}
\begin{itemize}
\item the per-scenario solution of a stochastic variable {\tt Transport(i,j)} will be stored in {\tt Transport.Stochastic(sc,i,j)},
\item the deterministic solution of a non-stochastic variable {\tt InitialStock(i)} will be stored in {\tt InitialStock.Stochastic('TransportModel',i)},
\item the weighted objective value for the objective variable {\tt TotalCost} will be stored in  {\tt TotalObjective.Stochastic('TransportModel')}, while the contribution by every scenario is available through {\tt TotalCost.Stochastic(sc)}.
\end{itemize}

\subsection{Using the stochastic Benders algorithm}\label{sec:stoch.benders}
\index{stochastic programming!Benders algorithm}
\index{stochastic Benders algorithm}
\index{algorithm!stochastic Benders}

\paragraph{Using the stochastic Benders algorithm}

Instead of solving the deterministic equivalent of a stochastic model, AIMMS
also allows you to solve {\em linear} stochastic models using a stochastic
Benders algorithm. The stochastic Benders algorithm is based on a reformulation
of the original model as a sequence of models outlined below. The solution of
the original model can be achieved by solving the sequence of models iteratively
until a terminating condition is reached. A more detailed discussion of the stochastic Benders algorithm can be found in \cite{bib:DT98} or \cite{bib:Al03}.

\paragraph{Definitions}

All nodes in the scenario tree are numbered starting at 1 (the root node).
\begin{model}
\item[Indices:]\ \\
\declline{$i$}{index for the set of nodes $N$}\\
\declline{$t$}{index for the set of stages $T$}\\
\item[Parameters:]\ \\
\declline{$\mathit{q}_i$}{probability belonging to node $i$}\\
\declline{$\mathit{p}_i$}{parent of node $i$}\\
\item[Sets:]\ \\
\declline{$\mathit{I}_i$}{set with children of node $i$}\\
\declline{$\mathit{N}_t$}{set of nodes belonging to stage $t$}
\end{model}

\paragraph{Convention}

In the algorithmic outline below we identify the problem names with their
associated solutions. That is, if a problem is, for instance, identified as
$F_i(x_{p_i})$, we will also use this name to denote its solution in other sub-
problems.

\paragraph{The original model}

The nested Benders algorithm can be used for problems of the form

\begin{model}
\direction{Minimize:}
\begin{alignat*}{4}
   \modelline{}{\sum_{t\in T}\sum_{i\in N_t} q_i c_i^T x_i}{}{}
\end{alignat*}
\subjecttomath
\begin{alignat*}{4}
   \modelline{}{W_1 x_1}{= h_1}{}\\
   \modelline{}{A_i x_{p_i} + W_i x_i}{= h_i}{\forall i\in N_t, t\in T\backslash\{1\}}\\
   \modelline{}{x_i}{\geq 0}{\forall i\in N_t, t\in T}
\end{alignat*}
\end{model}

\paragraph{A reformulation as a sequence of models}

This problem corresponds to the following sequence of problems. For node $i = 1$, the problem $F_1$ is defined as
\begin{model}
\direction{Minimize:}
\begin{alignat*}{4}
\modelline{}{c_1^Tx_1 + \sum_{j\in I_1} q_i F_j(x_1)}{}{}
\end{alignat*}
\subjecttomath
\begin{alignat*}{4}
\modelline{}{W_1x_1}{= h_1}{}\\
\modelline{}{x_1}{\geq 0}{}
\end{alignat*}
\end{model}
For all other nodes $i\in N_t$ in stage $t\in T\backslash\{1\}$, the problem $F_i(x_{p_i})$ is defined as follows (note that $\sum_{j\in I_i} q_j = q_i$)
\begin{model}
\direction{Minimize:}
\begin{alignat*}{4}
\modelline{}{c_i^Tx_i + \sum_{j\in I_i} \frac{q_j}{q_i} F_j(x_i)}{}{}
\end{alignat*}
\subjecttomath
\begin{alignat*}{4}
\modelline{}{W_ix_i}{= h_i - A_ix_{p_i}}{}\\
\modelline{}{x_i}{\geq 0}{}
\end{alignat*}
\end{model}
For the leaf nodes in the scenario tree, the term $\sum_{j\in I_i}\frac{q_j}{q_i}F_j(x_i)$ is omitted.

\paragraph{Formulated differently}

If we now introduce an upper bound $\theta_i$ to replace the term $\sum_{j\in I_i}\frac{q_j}{q_i}F_j(x_i)$, we can rewrite the subproblem $F_i(x_{p_i})$ as
\begin{model}
\direction{Minimize:}
\begin{alignat*}{4}
\modelline{}{c_i^Tx_i + \theta_i}{}{}
\end{alignat*}
\subjecttomath
\begin{alignat*}{4}
\modelline{}{W_ix_i}{= h_i - A_ix_{p_i}}{}\\
\modelline{}{\theta_i}{\geq \sum_{j\in I_i}\frac{q_j}{q_i}F_j(x_i)}{}\\
\modelline{}{x_i}{\geq 0}{}
\end{alignat*}
\end{model}
Because of the linear nature of the original problem, the terms $\sum_{j\in I_i}\frac{q_j}{q_i}F_j(x_i)$ are piecewise linear and convex. Therefore there exists an (a priori unknown) set of equations
\[
    D^l_i x_i = d^l_i
\]
that describes such a term and for which
\[
    D^l_i x_i + \theta_i \geq d^l_i.
\]
Moreover, we are only interested in those $x_i$ such that $F_j(x_i)$ are feasible for all $j\in I_i$. This requirement can be enforced by an (a priori unknown) set of constraints
\[
    E^l_i x_i  \geq e^l_i.
\]
By substituting these constraints we obtain the following reformulation of problem $F_i(x_{p_i})$
\begin{model}
\direction{Minimize:}
\begin{alignat*}{4}
\modelline{}{c_i^Tx_i + \theta_i}{}{}
\end{alignat*}
\subjecttomath
\begin{alignat*}{4}
\modelline{}{W_ix_i}{= h_i - A_ix_{p_i}}{}\\
\modelline{}{D^l_i x_i + \theta_i}{\geq  d^l_i}{\forall l\in 1,\dots,R_i}\\
\modelline{}{E^l_i x_i}{\geq e^l_i}{\forall l\in 1,\dots,S_i}\\
\modelline{}{x_i}{\geq 0}{}
\end{alignat*}
\end{model}

\paragraph{The relaxed master problem}

The actual problem that is solved at node $i$ is the following relaxed master problem $\tilde{N}_i(x_{p_i})$ defined as follows:     	
\begin{model}
\direction{Minimize:}
\begin{alignat*}{4}
\modelline{}{c_i^Tx_i + \theta_i}{}{}
\end{alignat*}
\subjecttomath
\begin{alignat*}{4}
\modelline{}{W_ix_i}{= h_i - A_ix_{p_i}}{}\\
\modelline{}{D^l_i x_i + \theta_i}{\geq  d^l_i}{\forall l\in 1,\dots,r_i}\\
\modelline{}{E^l_i x_i}{\geq e^l_i}{\forall l\in 1,\dots,s_i}\\
\modelline{}{x_i}{\geq 0}{}
\end{alignat*}
\end{model}
At the start of the Benders algorithm $r_i$ and $s_i$ will be 0 for all $i\in N$. The constraints $D^l_i x_i + \theta_i\geq  d^l_i$ are optimality cuts obtained from the children. That is, if $\tilde{N}_j(x_i)$ is feasible for all $j\in I_i$ (but not optimal) then an optimality cut is added to $\tilde{N}_i(x_{p_i})$. The optimality cut is constructed by using a combination of the dual solutions of $\tilde{N}_j(x_i)$ for all $j\in I_i$. Adding an optimality cut does not make a feasible relaxed master problem infeasible.
The Benders algorithm fails if one of the subproblems is unbounded. This can be avoided by giving all variables, except the objective variable, finite bounds.

\paragraph{Adding feasibility cuts}

The constraints $E^l_i x_i\geq e^l_i$ are feasibility cuts obtained from a child. If some child problem $\tilde{N}_j(x_i)$ is not feasible then the following problem $\tilde{E}_j(x_i)$ is solved
\begin{model}
\direction{Minimize:}
\begin{alignat*}{4}
\modelline{}{w_j = e^T u^+_j + e^T u^-_j}{}{}
\end{alignat*}
\subjecttomath
\begin{alignat*}{4}
\modelline{}{W_jx_j + I u^+_j - I u^-_j }{= h_j - A_jx_i}{}\\
\modelline{}{E^l_j x_j}{\geq e^l_j}{\forall l\in 1,\dots,s_j}\\
\modelline{}{x_j}{\geq 0}{}\\
\modelline{}{u^+_j}{\geq 0}{}\\
\modelline{}{u^-_j}{\geq 0}{}\\
\end{alignat*}
\end{model}
This feasibility problem can only be formulated for linear problems, is always feasible, and bounded from below by 0. Its dual solution is used to construct a new feasibility constraint for $\tilde{N}_i(x_{p_i})$. Note that node $j$ in its turn obtains optimality and/or feasibility cuts from its children for $\tilde{N}_j(x_i)$ and $\tilde{E}_j(x_i)$, unless $j$ refers to a leaf node.

\paragraph{Terminating condition}

If $(x_i, \theta_i)$ is an optimal solution of $\tilde{N}_i(x_{p_i})$ and
\[
	\theta_i  \geq  \tilde{N}_i(x_{p_i})
\]
then $(x_i, \theta_i)$ is an optimal solution of $F_i(x_{p_i})$.
If this holds for all non-leaf nodes then we have found an optimal solution of our original problem. For the leaf nodes, $x_i$ only needs to be an optimal solution of $\tilde{N}_i(x_{p_i})$.

\paragraph{Implementation in AIMMS}

The stochastic Benders algorithm outlined above is implemented in AIMMS as a system module that you can include into your model, together with a number of supporting functions in the {\gmp} library to perform a number of algorithmic steps that cannot be performed in the AIMMS language itself, for instance, to actually generate the stochastic sub-problems, and to generate feasibility and optimality cuts.

\paragraph{Adding the module}

You can add the system module implementing the stochastic Benders algorithm to your model through the {\bf Settings-Install System Module\dots} menu. By selecting the {\bf Stochastic Decomposition Module} in the {\bf Install System Module} dialog box, AIMMS will add this system module to your model.

\paragraph{Using the stochastic Benders module}
\procindex{DoStochasticDecomposition}

The main procedure for using the stochastic Benders algorithm is {\tt DoStochas\-tic\-Decomposition}. Its inputs are:
\begin{itemize}
\item a stochastic {\gmp},
\item the set of stages to consider, and
\item the set of scenarios to consider.
\end{itemize}
The procedure implements the algorithm outlined above. The supporting {\gmp} functions for the stochastic Benders algorithm are described in Section~\ref{sec:gmp.stochastic}.

\paragraph{Modifying the algorithm}

Because the stochastic Benders algorithm is written in the AIMMS language, you have complete freedom to modify the algorithm in order to tune it for your stochastic programs. Also, the basic algorithm solves all sub-problems sequentially. If your AIMMS license permits parallel solver sessions, you can also speed up the algorithm by solving multiple sub-problems in parallel using the {\gmp} function {\tt GMP::SolverSession::AsynchronousExecute}.



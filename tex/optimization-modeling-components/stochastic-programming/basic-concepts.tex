\section{Basic concepts}\label{sec:stoch.concepts}
\index{stochastic programming!basic concepts}

\paragraph{Basic concepts}\label{sec:stochastic.basicconcepts}

In this section you will find a number of basic concepts that are commonly used
in stochastic programming. They will help you to unambiguously understand the
stochastic programming facilities in AIMMS.

\paragraph{Stages}\index{stage}\index{stochastic programming!stage}

In stochastic programming {\em stages} define a collection of consecutive periods of time. Stages are usually identified through positive integers $1,2,\dots$, and are characterized as follows:
\begin{itemize}
\item during each stage one or more stochastic (i.e.\ uncertain) events take place, and
\item at the end of a stage decisions are taken, taking into account the specific outcomes of the stochastic events of this and previous stages.
\end{itemize}
Stochastic events may be such quantities as the demand realized during a
period. They are usually represented as input data used in the deterministic
model. Any variables in the deterministic model that are modeled conceptually to take their value \emph{at the beginning} of a period (for instance, the stock at the beginning of a period), should be considered as decisions taken at the end of the previous period/stage within the stage concept.

\paragraph{Stages versus model periods}

If the deterministic model already contains a {\tt Horizon} (see Section~\ref{sec:time.horizon}) or any other set of time periods, the stages of
the stochastic model may naturally coincide with the time periods from the deterministic model, but this certainly needs not be the case. A single period model, possibly even without an explicit period set, but with variables representing decisions taken at the beginning \emph{and} at the end of the period, may still constitute a two-stage stochastic model. For a multi-period model, a single stage in the stochastic model may consist of multiple time periods from the deterministic model, and hence one can always construct a mapping from the deterministic period set to stages in the stochastic model.

\paragraph{Variables and stages}

Every individual stochastic variable in a stochastic model should be uniquely associated with a single stage. This stage represents the period at the end of which the variable conceptually takes it value
\begin{itemize}
\item taking into consideration the {\em specific} outcomes of
stochastic events taking place during the stage at hand and during prior stages,
\item but only taking into account the {\em distribution} of possible outcomes of the stochastic events taking place in any further stage.
\end{itemize}
Even if model periods of the deterministic model and the stages of the
stochastic model coincide, variables with an index into the period set do not
have to be associated with the stage corresponding to the value of that index.
As discussed above, variables that conceptually take their value at the beginning of a period, provide a first example of this behavior as they must be associated
with the stage corresponding to the previous period within the stochastic model.

\paragraph{Examples}

Other examples may arise, for instance, when the monthly productions of January, February and March should be decided upon prior to the beginning of January, regardless of the specific outcomes for the demands during these months. Conversely, if market research has delivered good estimates for the demand in January, February and March, the decisions for the production in these months should take into consideration the demand estimates of all three months. Hence the production variables for January, February and March should be part of the stage associated with March.

\paragraph{Scenarios}\index{scenario}\index{stochastic programming!scenario}\index{scenario!probability}

A {\em scenario} for a stochastic model is a collection of outcomes for all the stochastic events taking place in the model, along with the associated probability of the scenario to occur. For the event values associated with each scenario, one could solve a deterministic model, which would yield the optimal decisions for that particular scenario. For different scenarios, however, the decisions resulting from solving such deterministic models individually are, in general, completely unrelated, even if the event outcomes of the scenarios are exactly the same up to a certain stage. To address this problem, the scenarios of a stochastic model must be organized into a scenario tree.

\paragraph{Scenario trees}
\index{scenario!tree}\index{stochastic programming!scenario tree}

A single scenario can be graphically represented as a simple tree illustrated in Figure~\ref{fig:stoch.single-scen}.
\begin{aimmsfigure}
\psset{dimen=middle,xunit=0.65cm,yunit=0.65cm}
\begin{pspicture}(2,3.5)(15,6)
  \rput(1.5,4.5){\em stage}
  \cnode*(2.5,5){2pt}{node_1}\rput(3.5,4.5){$1$}
  \cnode*(4.5,5){2pt}{node_2}\rput(5.5,4.5){$2$}
  \cnode*(6.5,5){2pt}{node_3}
  \cnode*(11.5,5){2pt}{node_n2}\rput(12.5,4.5){$n-1$}
  \cnode*(13.5,5){2pt}{node_n1}\rput(14.5,4.5){$n$}
  \cnode*(15.5,5){2pt}{node_n}\rput(16.5,5){\em sc$_1$}
  \ncline{-}{node_1}{node_2}
  \ncline{-}{node_2}{node_3}
  \ncline[linestyle=dashed]{-}{node_3}{node_n2}
  \ncline{-}{node_n2}{node_n1}
  \ncline{-}{node_n1}{node_n}
  \end{pspicture}
\caption{A tree representing a single scenario {\em sc$_1$}}\label{fig:stoch.single-scen}
\end{aimmsfigure}

For multiple scenarios, the specific outcomes of the stochastic events up to a
certain stage usually coincide for a subset of the scenarios. This gives rise
to a scenario tree as illustrated in Figure~\ref{fig:stoch.tree}.
\begin{aimmsfigure}
\psset{dimen=middle,xunit=0.65cm,yunit=0.65cm}
\begin{pspicture}(-1,1)(10,9)
  \rput(-0.5,1){\em stage}
  \rput(1.5,1){$1$}
  \rput(3.5,1){$2$}
  \rput(5.5,1){$3$}
  \rput(7.5,1){$4$}
  \cnode*(0.5,5){2pt}{node_0}
  \cnode*(2.5,5){2pt}{node_1}  \ncline{-}{node_0}{node_1}
  \cnode*(4.5,3.29){2pt}{node_11}\ncline{-}{node_1}{node_11}
  \cnode*(4.5,6.71){2pt}{node_12}\ncline{-}{node_1}{node_12}
  \cnode*(6.5,2.43){2pt}{node_111}\ncline{-}{node_11}{node_111}
  \cnode*(6.5,4.14){2pt}{node_112}\ncline{-}{node_11}{node_112}
  \cnode*(6.5,5.85){2pt}{node_121}\ncline{-}{node_12}{node_121}
  \cnode*(6.5,7.57){2pt}{node_122}\ncline{-}{node_12}{node_122}
  \cnode*(8.5,2){2pt}{node_1111}\ncline{-}{node_111}{node_1111}\rput(9.5,2){\em sc$_8$}
  \cnode*(8.5,2.86){2pt}{node_1112}\ncline{-}{node_111}{node_1112}\rput(9.5,2.86){\em sc$_7$}
  \cnode*(8.5,3.71){2pt}{node_1121}\ncline{-}{node_112}{node_1121}\rput(9.5,3.71){\em sc$_6$}
  \cnode*(8.5,4.57){2pt}{node_1122}\ncline{-}{node_112}{node_1122}\rput(9.5,4.57){\em sc$_5$}
  \cnode*(8.5,5.43){2pt}{node_1211}\ncline{-}{node_121}{node_1211}\rput(9.5,5.43){\em sc$_4$}
  \cnode*(8.5,6.28){2pt}{node_1212}\ncline{-}{node_121}{node_1212}\rput(9.5,6.28){\em sc$_3$}
  \cnode*(8.5,7.14){2pt}{node_1221}\ncline{-}{node_122}{node_1221}\rput(9.5,7.14){\em sc$_2$}
  \cnode*(8.5,8){2pt}{node_1222}\ncline{-}{node_122}{node_1222}\rput(9.5,8){\em sc$_1$}
  \end{pspicture}
\caption{A scenario tree with 8 scenarios}\label{fig:stoch.tree}
\end{aimmsfigure}
In such a scenario tree, the path from the root node of the tree to each of its leaf nodes corresponds to a single scenario, and the event outcomes for scenarios that pass through the same intermediate node are identical for all stages up to that node. If a stage consists of multiple time periods in the deterministic model, this means that the stochastic events taking place during {\em all} periods associated with the stage should coincide. The solution process of a stochastic model will make sure that the decisions that are to be taken at the end of these stages are identical for all the scenarios passing through the node, as one would intuitively expect.

\paragraph{Scenario generation}
\index{scenario!generation}\index{stochastic programming!scenario generation}

The scenarios and the scenario tree used in a stochastic model are usually generated by using one of the following two techniques
\begin{itemize}
\item generate scenarios by incrementally creating a scenario tree according to a given distribution for each stochastic event, or
\item given an externally created set of scenarios, create a scenario tree by grouping identical or similar scenarios at every level of the tree.
\end{itemize}

\paragraph{Distribution-based scenario generation}

Given a leaf node in an intermediate scenario tree, for every stochastic event that occurs during the stage directly following that node, a fixed number of values is computed according to a given distribution (each with its own relative probability of taking place). For each of these values a new branch is added to the node. The process starts by adding branches to the root node of the tree and ends when a tree is generated for all stages. The total number of scenarios generated by the process is the final number of leaf nodes generated. The probability of a scenario is the multiplication of the relative probabilities associated with each branch along the path from  root  to  leaf node.  The scenario tree in Figure~\ref{fig:stoch.tree} could be generated in this way, for example, by choosing, at every intermediate node,  a {\em high} or a {\em low} level for the demand during the stage following that particular node.

\paragraph{Scenario-based tree generation}

Another approach is to start from a given collection of scenarios with probabilities adding up to 1. Such a collection of scenarios can either be randomly generated or be the result of some external process. As a tree, they can be represented as a trivial scenario tree, as illustrated in Figure~\ref{fig:stoch.scengen}.
\begin{aimmsfigure}
\psset{dimen=middle,xunit=0.65cm,yunit=0.65cm}
\begin{pspicture}(2,-4.5)(15,4.5)
  \rput(1.5,-4){\em stage}
  \rput(3.5,-4){$1$}
  \rput(5.5,-4){$2$}
  \rput(12.5,-4){$n-1$}
  \rput(14.5,-4){$n$}
  \cnode*(2.5,0){2pt}{node_1}
  \cnode*(4.5,3){2pt}{node_12}
  \cnode*(6.5,3){2pt}{node_13}
  \cnode*(11.5,3){2pt}{node_1n2}
  \cnode*(13.5,3){2pt}{node_1n1}
  \cnode*(15.5,3){2pt}{node_1n}\rput(16.5,3){\em sc$_1$}
  \ncline{-}{node_1}{node_12}
  \ncline{-}{node_12}{node_13}
  \ncline[linestyle=dashed]{-}{node_13}{node_1n2}
  \ncline{-}{node_1n2}{node_1n1}
  \ncline{-}{node_1n1}{node_1n}
  \cnode*(4.5,2){2pt}{node_22}
  \cnode*(6.5,2){2pt}{node_23}
  \cnode*(11.5,2){2pt}{node_2n2}
  \cnode*(13.5,2){2pt}{node_2n1}
  \cnode*(15.5,2){2pt}{node_2n}\rput(16.5,2){\em sc$_2$}
  \ncline{-}{node_1}{node_22}
  \ncline{-}{node_22}{node_23}
  \ncline[linestyle=dashed]{-}{node_23}{node_2n2}
  \ncline{-}{node_2n2}{node_2n1}
  \ncline{-}{node_2n1}{node_2n}
  \cnode*(4.5,-2){2pt}{node_32}
  \cnode*(6.5,-2){2pt}{node_33}
  \cnode*(11.5,-2){2pt}{node_3n2}
  \cnode*(13.5,-2){2pt}{node_3n1}
  \cnode*(15.5,-2){2pt}{node_3n}\rput(16.5,-2){\em sc$_{m-1}$}
  \ncline{-}{node_1}{node_32}
  \ncline{-}{node_32}{node_33}
  \ncline[linestyle=dashed]{-}{node_33}{node_3n2}
  \ncline{-}{node_3n2}{node_3n1}
  \ncline{-}{node_3n1}{node_3n}
  \cnode*(4.5,-3){2pt}{node_42}
  \cnode*(6.5,-3){2pt}{node_43}
  \cnode*(11.5,-3){2pt}{node_4n2}
  \cnode*(13.5,-3){2pt}{node_4n1}
  \cnode*(15.5,-3){2pt}{node_4n}\rput(16.5,-3){\em sc$_m$}
  \ncline{-}{node_1}{node_42}
  \ncline{-}{node_42}{node_43}
  \ncline[linestyle=dashed]{-}{node_43}{node_4n2}
  \ncline{-}{node_4n2}{node_4n1}
  \ncline{-}{node_4n1}{node_4n}
  \psline[linestyle=dotted](4.5,1.25)(4.5,-1.25)
  \psline[linestyle=dotted](6.5,1.25)(6.5,-1.25)
  \psline[linestyle=dotted](11.5,1.25)(11.5,-1.25)
  \psline[linestyle=dotted](13.5,1.25)(13.5,-1.25)
  \psline[linestyle=dotted](15.5,1.25)(15.5,-1.25)
\end{pspicture}
\caption{An initial disconnected scenario tree}\label{fig:stoch.scengen}
\end{aimmsfigure}
This tree can be transformed into a scenario tree by bundling together identical or similar scenarios into a fixed or dynamic number of branches. The group of scenarios passing through a particular intermediate node in the scenario tree is analyzed and grouped into subgroups of scenarios with similar or identical outcomes of the stochastic events during the stage following that node. For every subgroup, the existing branches are bundled into a single branch, and the stochastic event outcomes are made identical for all scenarios in the subgroup. The process starts by analyzing all scenarios at the root node of the tree, and ends when every scenario is associated with a single leaf node.

\paragraph{Basic procedure for solving stochastic models}

The implementation of stochastic programming in AIMMS closely follows the concepts described in this section. The basic procedure to create and solve a stochastic model in AIMMS is as follows:
\begin{itemize}
\item indicate in your model which parameters and variables are to become stochastic,
\item for every stochastic variable in your model specify during which stage of the stochastic model the decision is to be taken,
\item generate scenarios, their stochastic data, and a scenario tree, using one of the techniques described above, and
\item generate and solve the stochastic model using the special methods available for this purpose in AIMMS.
\end{itemize}
Each of these steps is explained in more detail in the sections to follow.
Note that changing parameters and variables in your model into stochastic parameters and variables, does in no way influence the possibility to solve the underlying deterministic model in its original form. Thus, the stochastic programming facilities in AIMMS always form a true extension of the functionality of the existing AIMMS application.

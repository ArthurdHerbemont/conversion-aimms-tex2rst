\section{Control parameters that influence the multistart algorithm} \label{sec:multistart.control.par}

\paragraph{Control parameters}

The multistart module defines several parameters that influence the multistart algorithm.
These parameters have a similar functionality as options of a solver, e.g., {\sc Cplex}. The most
important parameters, with their default setting, are shown in Table~\ref{table:multistart.controlparam}.
\begin{aimmstable}
\begin{tabular}{|l|l|l|l|}
\hline\hline
{\bf Parameter} &  {\bf Default} &  {\bf Range} &  {\bf Subsection} \\
\hline
{\tt IterationLimit}                 & 5     & \{1,maxint\} & \ref{sec:multistart.iteration.limit} \\
{\tt TimeLimit}                      & 0     & \{0,maxint\} & \ref{sec:multistart.time.limit} \\
{\tt TimeLimitSingleSolve}           & 0     & \{0,maxint\} & \ref{sec:multistart.time.limit} \\
{\tt ThreadLimit}                    & 0     & \{0,maxint\} & \ref{sec:multistart.thread.limit} \\
{\tt UseOpportunisticAlgorithm}      & 0     & \{0,1\}      & \ref{sec:multistart.oppor.mode} \\
{\tt NumberOfBestSolutions}          & 1     & \{1,1000\}   & \ref{sec:multistart.multiple.solutions} \\
{\tt ShrinkFactor}                   & 0.95  & [0,1]        & \ref{sec:multistart.shrink.factor} \\
{\tt UsePresolver}                   & 1     & \{0,1\}      & \ref{sec:multistart.aimms.presolver} \\
{\tt UseInitialPoint}                & 1     & \{0,1\}      & \ref{sec:multistart.starting.point} \\
{\tt UseConstraintConsensusMethod}   & 0     & \{-1,1\}     & \ref{sec:multistart.improve.sample} \\
{\tt MaximalVariableBound}           & 1000  & [0,inf)      & \ref{sec:multistart.unbounded.vars} \\
{\tt ShowSolverProgress}             & 0     & \{0,1\}      & \ref{sec:multistart.solver.progress} \\
\hline\hline
\end{tabular}
\caption{Control parameters in the multistart module}\label{table:multistart.controlparam}
\end{aimmstable}
The parameters that are not self-explanatory are explained in this section;
the last column in the table refers to the subsection that discusses the corresponding parameter.

\subsection{Specifying an iteration limit} \label{sec:multistart.iteration.limit}

\paragraph{Parameter {\tt Iteration\-Limit}}

The parameter {\tt Iteration\-Limit} can be used to set a limit on the number of iterations
used by the multistart algorithm. This limit is use in the basic algorithm and in the first
phase of the dynamic algorithm.

\subsection{Specifying a time limit} \label{sec:multistart.time.limit}

\paragraph{Parameter {\tt Time\-Limit}}

The parameter {\tt Time\-Limit} can be used to set a limit on the total elapsed time
(in seconds) used by the multistart algorithm. The default value of 0 has a special
meaning; in that case there is no time limit.

\paragraph{Parameter {\tt Time\-Limit\-Single\-Solve}}

It is also possible to set a time limit for every single solve started by the
multistart algorithm by using the parameter {\tt Time\-Limit\-Single\-Solve}.
Also the default value of 0 of this parameter has a special meaning; in that case
there is no time limit.

\subsection{Using multiple threads} \label{sec:multistart.thread.limit}

\paragraph{Parameter {\tt Thread\-Limit}}

The parameter {\tt Thread\-Limit} controls the number of threads that should
be used by the multistart algorithm. Each thread will be used 
to solve one NLP using an asynchronous solver session. At its default setting of 0,
the algorithm will automatically use the maximum number of threads, which is
limited by the number of cores on the machine and the amount of solver
sessions allowed by the AIMMS license.

\subsection{Deterministic versus opportunistic} \label{sec:multistart.oppor.mode}

\paragraph{Parameter {\tt Use\-Oppor\-tunis\-tic\-Algo\-rithm}}

By default the multistart algorithm runs in deterministic mode. Deterministic means
that multiple runs with the same model using the same parameter settings and the same
solver on the same computer will reproduce the same results. The number of NLP problems
solved by the multistart algorithm will then also be the same. In contrast, opportunistic
implies that the results, and the number of NLP problems solved, might
be different. Usually the opportunistic mode provides better performance. The
parameter {\tt Use\-Oppor\-tunis\-tic\-Algo\-rithm} can be used to switch to the
opportunistic mode. Note that if the multistart algorithm uses only one thread then
the algorithm will always be deterministic.

\subsection{Getting multiple solutions} \label{sec:multistart.multiple.solutions}

\paragraph{Parameter {\tt Number\-Of\-Best\-Solutions}}

By default the multistart algorithm will return one solution, namely the best
solution that the algorithm finds. By setting the parameter
{\tt Num\-ber\-Of\-Best\-So\-lu\-tions} to a value higher than 1, the multistart
algorithm will store the best $n$ solutions found in the solution repository (see
Section~\ref{sec:gmp.solution}). Here $n$ denotes the value of this parameter.

\subsection{Shrinking the clusters} \label{sec:multistart.shrink.factor}

\paragraph{Parameter {\tt Shrink\-Factor}}

The clusters created by the multistart algorithm would normally grow
as more and more points are assigned to the clusters. As a side effect,
a new sample point is then more likely to be directly assigned to a cluster, in which
case no NLP is solved for that sample point, thereby increasing the chance that it
ends up in the wrong cluster. To overcome this
problem, the multistart algorithm automatically shrinks all clusters after each
iteration by a constant factor which is specified by the parameter
{\tt Shrink\-Factor}.

\subsection{Combining multistart and presolver} \label{sec:multistart.aimms.presolver}

\paragraph{Parameter {\tt Use\-Presolver}}

By default the multistart algorithm starts by applying the AIMMS Presolver
to the NLP problem. By preprocessing the problem, the ranges of the variables might
become smaller which has a positive effect on the multistart algorithm as then
the randomly generated sample points are more likely to be good starting points.
The parameter {\tt Use\-Presolver} can be used to switch off the preprocessing
step.

\subsection{Using a starting point} \label{sec:multistart.starting.point}

\paragraph{Parameter {\tt Use\-Initial\-Point}}

Sometimes the level values, assigned to the variables before solving the NLP
problem, provide a good starting point. By default the multistart algorithm
will use this initial point as the first sample point but only in the first
iteration. This behavior is controlled by the parameter {\tt Use\-Initial\-Point}.

\subsection{Improving the sample points} \label{sec:multistart.improve.sample}

\paragraph{Parameter {\tt Use\-Constraint\-Consensus\-Method}}

The sample points are randomly generated by using the intervals defined by the lower
and upper bounds of the variables. Such a sample point is very likely to be infeasible
with respect to the constraints. The constraint consensus method, which is described in \cite{bib:Ch04},
tries to find an approximately feasible point for a sample point. Using this method might
slow down the multistart algorithm but the chance of generating (almost) feasible sample
points increases. The constraint consensus method can be activated by using the
parameter {\tt Use\-Con\-straint\-Con\-sen\-sus\-Method}. If this parameter is set to 1 then
the constraint consensus method will be used whenever possible, and if it is set to -1 then
it will never be used. At its default value of 0, the algorithm automatically decides when
to use the constraint consensus method.

\subsection{Unbounded variables} \label{sec:multistart.unbounded.vars}

\paragraph{Parameter {\tt Maximal\-Variable\-Bound}}

A multistart algorithm requires that all variable bounds are finite. Therefore
the multistart algorithm in AIMMS will use a fixed value for all infinite upper and
lower variable bounds. This fixed value is specified by the parameter
{\tt Maxi\-mal\-Varia\-ble\-Bound}. The value of this parameter might be updated automatically
in case the dynamic algorithm is used.

\subsection{Solver progress} \label{sec:multistart.solver.progress}

\paragraph{Parameter {\tt Show\-Solver\-Progress}}

By default the progress window will only show general progress information for
the multistart algorithm, including the objective value, the number of iterations,
the elapsed time, etc. By switching on the parameter {\tt Show\-Solver\-Pro\-gress}
also progress information by the NLP solver will be displayed. If multiple
solver sessions are (asynchronous) executing at the same time then only the progress
information of one of them will be shown.


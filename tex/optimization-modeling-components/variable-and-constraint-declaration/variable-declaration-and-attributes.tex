\section{\tttext{Variable} declaration and attributes}\label{sec:var.var}

\paragraph{Declaration and attributes}
\declindex{Variable} \AIMMSlink{variable}

Variables have some additional attributes above those of parameters. These
extra attributes are used to steer a solver, or to hold additional information
about solution values provided by the solver. The possible attributes of
variables are given in Table~\ref{table:var.attr-variable}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}               & {\bf See also} \\
                &                                    & {\bf page} \\
\hline \verb|IndexDomain|   & {\em index-domain}                  &
                \pageref{attr:par.index-domain}\\
\verb|Range|    & {\em range}                          & \pageref{attr:par.range}\\
\verb|Default|  &{\em  constant-expression}            & \pageref{attr:par.default}\\
\verb|Unit|     &{\em  unit-expression}         &
\pageref{attr:par.unit}, \pageref{chap:units}\\
\verb|Priority| & {\em expression}              & \\
\verb|NonvarStatus|   & {\em expression}              & \\
\verb|RelaxStatus|    & {\em expression}           & \\
\verb|Property|     &{\tt NoSave}, {\em numeric-storage-property}, {\tt Inline} &
        \pageref{attr:set.property},
        \pageref{attr:par.property} \\
        & {\tt SemiContinuous}, {\tt Basic},  {\tt Stochastic}, {\tt Adjustable}& \\
        & {\tt ReducedCost}, {\tt ValueRange}, {\tt CoefficientRange},  & \\
                & {\em constraint-related-sensitivity-property} & \\
\verb|Text|         & {\em string}                     & \pageref{attr:prelim.text}\\
\verb|Comment|      & {\em comment string }            & \pageref{attr:prelim.comment}\\
\verb|Definition|   & {\em expression}                 &
        \pageref{attr:set.definition}, \pageref{attr:par.definition}\\
\hline
\verb|Stage|    & {\em expression}           & \pageref{sec:var.uncertainty}, \pageref{sec:stoch.stoch}\\
\verb|Dependency| & {\em expression}					& \pageref{sec:var.uncertainty}, \pageref{attr:robust.dependency} \\
\hline\hline
\end{tabular}
\caption{{\tt Variable} attributes}\label{table:var.attr-variable}
\end{aimmstable}

\paragraph{Index domain for variables}\herelabel{attr:var.index-domain}
\declattrindex{variable}{IndexDomain} \AIMMSlink{variable.index_domain}

By specifying the {\tt IndexDomain} attribute you can restrict the domain of a
variable in the same way as that of a parameter. For variables, however, the
domain restriction has an additional effect. During the generation of
individual constraints AIMMS will reduce the size of the generated
mathematical program by including only those variables that satisfy all domain
restrictions.

\paragraph{The {\tt Range} attribute}\herelabel{attr:var.range}
\declattrindex{variable}{Range} \AIMMSlink{variable.range}
\typindex{range}{Real} \typindex{range}{Nonnegative}
\typindex{range}{Nonpositive} \typindex{range}{Integer}
\typindex{range}{Binary} \index{range!interval} \index{interval range}
\index{range!integer} \index{integer range}

The values of the {\tt Range} attribute of variables determine the bounds that
are passed on to the solver. In addition, during an assignment, the {\tt Range}
attribute restricts the range of allowed values that can be assigned to a
particular interval (as for parameters). The possible values for the {\tt
Range} attribute are:
\begin{itemize}
\item one of the predefined ranges {\tt Real}, {\tt Nonnegative}, {\tt
Nonpositive}, {\tt Integer} or {\tt Binary},
\item any one of the interval expressions {\tt [}$a,b${\tt ]}, {\tt
[}$a,b${\tt )}, {\tt (}$a,b${\tt ]} or {\tt (}$a,b${\tt )}, where $a$ and $b$
can be a constant number, {\tt inf}, {\tt -inf}, or a parameter reference
involving some or all of the indices on the index list of the declared
variable,
\item any enumerated integer set expression, e.g.\ \verb|{|$a$ {\tt ..}
$b$\verb|}| with $a$ and $b$ as above, or
\item an integer set identifier.
\end{itemize}
If you specify {\tt Real}, {\tt Nonnegative}, {\tt Nonpositive}, or an interval
expression, AIMMS will interpret the variable as a continuous variable. If
you specify {\tt Integer}, {\tt Binary} or an integer set expression, AIMMS
will interpret the variable as a binary or integer variable.

\paragraph{Example}
The following example illustrates a simple variable declaration.
\begin{example}
Variable Transport {
    IndexDomain  : (i,j) in Connections;
    Range        : [ MinTransport(i), Capacity(i,j) ];
}
\end{example}
The declaration of the variable {\tt Transport(i,j)} sets its lower bound equal
to {\tt MinTransport(i)} and its upper bound to {\tt Capacity(i,j)}. When
generating the mathematical program, the variable {\tt Transport} will only be
generated for those tuples ({\tt i},{\tt j}) that lie in the set {\tt
Connections}. Note that the specification of the lower bound only uses a
subdomain ({\tt i}) of the full index domain of the variable ({\tt i,j}).

\paragraph{The {\tt .Lower} and {\tt .Upper} suffices}
\herelabel{attr:var.lower-upper}
\suffindex{variable}{Lower}
\suffindex{variable}{Upper}
\AIMMSlink{lower}
\AIMMSlink{upper}

Besides using the {\tt Range} attribute to specify the lower and upper bounds,
you can also use the {\tt .Lower} and {\tt .Upper} suffices in assignment
statements to accomplish this task. The {\tt .Lower} and {\tt .Upper} suffices
are attached to the name of the variable, and, as a result, the corresponding
bounds are defined for the entire index domain. This may lead to increased
memory usage when variables share their bounds for slices of the domain. For
this reason, you are advised to use the {\tt Range} attribute as much as
possible when specifying the lower and upper bounds.

\paragraph{When allowed}

You can only make a bound assignment with either the {\tt .Lower} or {\tt
.Upper} suffix when you have not used a parameter reference (or a non-constant
expression) at the corresponding position in the {\tt Range} attribute. Bound
assignments via the {\tt .Lower} and {\tt .Upper} suffices must always lie
within the range specified in the {\tt Range} attribute.

\paragraph{Example}

Consider the variable {\tt Transport} declared in the previous example. The
following assignment to {\tt Transport.Lower(i,j)} is not allowed, because you
have already specified a parameter reference at the corresponding position in
the {\tt Range} attribute.
\begin{example}
    Transport.Lower(i,j) := MinTransport(i) ;
\end{example}
On the other hand, given the following declaration,
\begin{example}
Variable Shipment {
    IndexDomain  : (i,j) in Connections;
    Range        : Nonnegative;
}
\end{example}
the following assignment is allowed:
\begin{example}
    Shipment.Lower(i,j) := MinTransport(i);
\end{example}
AIMMS will produce a run-time error message if any value of {\tt
MinTransport(i)} is less than zero, because this violates the bound in the {\tt
Range} attribute of the variable {\tt Shipment}.

\paragraph{The {\tt Default} attribute}\herelabel{attr:var.default}
\declattrindex{variable}{Default} \AIMMSlink{variable.default}

Variables that have not been initialized, evaluate to a default value
automatically. These default values are also passed as initial values to the
solver. You can specify the default value using the {\tt Default} attribute.
The value of this attribute {\em must} be a constant expression. If you do not
provide a default value, AIMMS will use a default of 0.

\paragraph{The {\tt Unit} attribute}\herelabel{attr:var.unit}
\declattrindex{variable}{Unit} \AIMMSlink{variable.unit}

Providing a {\tt Unit} for every variable and constraint in your model will
help you in a number of ways.
\begin{itemize}
\item  AIMMS will help you to check the consistency of all your
constraints and assignments in your model, and
\item AIMMS will use the units  to scale the model that
is sent to the solver.
\end{itemize}
Proper scaling of a model will generally result in a more accurate and robust
solution process. You can find more information on the definition and use of
units to scale mathematical programs in Chapter~\ref{chap:units}.

\paragraph{The {\tt Definition} attribute}\herelabel{attr:var.definition}
\declattrindex{variable}{Definition} \AIMMSlink{variable.definition}

It is not unusual that symbolic constraints in a model are equalities defining
just one variable in terms of others. Under these conditions, it is preferable
to provide the definition of the variable through its {\tt Definition}
attribute. As a result, you no longer need to specify extra constraints for
just variable definitions. In the constraint listing, the constraints
associated with a defined variable will be listed with a generated name
consisting of the name of the variable with an additional suffix ``{\tt
\_definition}''.

\paragraph{Example}
The following example defines the total cost of transport, based on unit
transport cost and actual transport taking place.
\begin{example}
Variable TransportCost {
    Definition : sum( (i,j), UnitTransportCost(i,j)*Transport(i,j) );
}
\end{example}

\subsection{The \tttext{Priority}, \tttext{Nonvar} and \tttext{RelaxStatus} attributes}\label{sec:var.var.solver-attr}

\paragraph{The {\tt Priority} attribute}\herelabel{attr:var.priority}
\declattrindex{variable}{Priority} \AIMMSlink{variable.priority}

With the {\tt Priority} attribute you can assign priorities to integer
variables (or continuous variables when using the solver {\sc Baron}).
The value of this attribute must be an expression using some or all
of the indices in the index domain of the variable, and must be nonnegative and integer.
All variables with priority zero will be considered last by the
branch-and-bound process of the solver. For variables with a positive priority
value, those with the highest priority value will be considered first.

\paragraph{The {\tt .Priority} suffix}\herelabel{suffix:var.priority}
\suffindex{variable}{Priority}

Alternatively, you can specify priorities through assignments to the {\tt
.Priority} suffix. This is only allowed if you have not specified the {\tt
Priority} attribute. In both cases, you can use the {\tt .Priority} suffix to
refer to the priority of a variable in expressions.

\paragraph{Use of priorities}
\index{use of!priority} \index{variable!priority} \index{priority}

The solution algorithm (i.e.\ solver) for integer and mixed-integer programs
initially solves without the integer restriction, and then adds this
restriction one variable at a time according to their priority. By default, all
integer variables have equal priority. Some decisions, however, have a natural
order in time or space. For example, the decision to build a factory at some
site comes before the decision to purchase production capacity for that
factory. Obeying this order naturally limits the number of subsequent choices,
and could speed up the overall search by the solution algorithm.

\paragraph{The {\tt NonvarStatus} attribute}\herelabel{attr:var.nonvar}
\declattrindex{variable}{NonvarStatus} \AIMMSlink{variable.nonvar_status}

You can use the {\tt NonvarStatus} attribute to tell AIMMS which variables
should be considered as parameters during the execution of a {\tt SOLVE}
statement. The value of the {\tt NonvarStatus} attribute must be an expression
in some or all of the indices in the index list of the variable, allowing you
to change the nonvariable status of individual elements or groups of elements
at once.

\paragraph{Positive versus negative values}

The sign of the {\tt NonvarStatus} value determines whether and how the variable is
passed on to the solver. The following rules apply.
\begin{itemize}
\item If the value is 0 (the default value), the corresponding
individual variable is generated, along with its specified lower and upper
bounds.
\item If the value is negative, the corresponding individual variable
is still generated, but its lower and upper bounds are set equal to the current
value of the variable.
\item If the value is positive, the corresponding individual variable
is no longer generated but passed as a constant to the solver.
\end{itemize}
When you specify a negative value, you will still be able to inspect the
corresponding reduced cost values. In addition, you can modify the nonvariable
status to zero without causing AIMMS to regenerate the model. When you
specify a positive value, the size of the mathematical program is kept to a
minimum, but any subsequent changes to the nonvariable status will require
regeneration of the model constraints.

\paragraph{The {\tt .NonVar} suffix}
\suffindex{variable}{NonVar} \AIMMSlink{nonvar}

Alternatively, you can change the nonvariable status through assignments to the
{\tt .NonVar} suffix. This is only allowed if you have not specified the {\tt
Nonvar\-Status} attribute. In both cases, you can use the {\tt .NonVar} suffix to refer
to the variable status of a variable in expressions.

\paragraph{When to change the nonvariable status}
\index{use of!nonvar status} \index{variable!nonvar status} \index{nonvar
status}

By altering the nonvariable status of variables you are essentially
reconfiguring your mathematical program. You could, for instance, reverse the
role of an input parameter (declared as a variable with negative nonvariable
status) and an output variable in your model to observe what input level is
required to obtain a desired output level. Another example of temporary
reconfiguration is to solve a smaller version of a mathematical program by
first discarding selected variables, and then changing their status back to
solve the larger mathematical program using the previous solution as a starting
point.

\paragraph{The {\tt RelaxStatus} attribute}\herelabel{attr:var.relax}
\declattrindex{variable}{RelaxStatus} \AIMMSlink{variable.relax_status}

With the {\tt RelaxStatus} attribute you can tell AIMMS to relax the
integer restriction for those tuples in the domain of an integer variable for
which the value of the relax status is nonzero. AIMMS will generate
continuous variables for such tuples instead, i.e.\ variables which may assume
any real value between their bounds.

\paragraph{The {\tt .Relax} suffix}
\suffindex{variable}{Relax} \AIMMSlink{relax}

Alternatively, you can relax integer variables by making assignments to the
{\tt .Relax} suffix. This is only allowed if you have not specified the {\tt
RelaxStatus} attribute. In both cases, you can use the {\tt .Relax} suffix to refer
to the relax status of a variable in expressions.

\paragraph{When to relax variables}
\index{use of!relax status} \index{variable!relax status} \index{relax status}

When solving large mixed integer programs, the solution times may become
unacceptably high with an increase in the number of integer variables. You can
try to resolve this by relaxing the integer condition of some of the integer
variables. For instance, in a multi-period planning model, an accurate integer
solution for the first few periods and an approximating continuous solution for
the remaining periods may very well be acceptable, and at the same time reduce
solution times drastically.

\paragraph{Effect on mathematical program type}

As you will see in Chapter~\ref{chap:mp}, there are several types of
mathematical programs. By changing the nonvariable and/or relax status of
variables you may alter the type of your mathematical program. For instance, if
your constraints contains a nonlinear term {\tt x*y}, then changing the
nonvariable status of either {\tt x} or {\tt y} will change it into a linear
term. Eventually, this may result in a nonlinear mathematical program becoming
a linear one. Similarly, changing the nonvariable or relax status of integer
variables may at some point change a mixed integer program into a linear
program.

\subsection{Variable properties}\label{sec:var.properties}

\paragraph{Properties of variables}\herelabel{attr:var.property}

Variables can have one or more of the following properties: {\tt NoSave}, {\tt
Inline}, {\tt SemiContinuous}, {\tt ReducedCost}, {\tt CoefficientRange}, 
{\tt ValueRange}, {\tt Stochastic}, and {\tt Adj\-ustable}. 
They are described in the paragraphs below.

\paragraph{Use of {\tt PROPERTY} statement}
\declattrindex{variable}{Property}

You can also change the properties of a variable during the execution of your
model by calling the {\tt PROPERTY} statement. Identifier properties are
changed by adding the property name as a suffix to the identifier name in a
{\tt PROPERTY} statement. When the value is set to {\tt off}, the property no
longer holds.

\paragraph{The {\tt NoSave} property}
\declpropindex{variable}{NoSave}

With the property {\tt NoSave} you indicate that you do not want to store data
associated with this variable in a case. This property is especially suited for
those identifiers that are intermediate quantities in the model, and that are
not used anywhere in the graphical end-user interface.

\paragraph{Inline variables}
\declpropindex{variable}{Inline}

With the property {\tt Inline} you can indicate that AIMMS should substitute
all references to the variable at hand by its defining expression when
generating the constraints of a mathematical program. Setting this property
only makes sense for defined variables, and will result in a mathematical
program with less rows and columns but with a (possibly) larger number of
nonzeros. After the mathematical program has been solved, AIMMS will compute
the level values of all inline variables by evaluating their definition.
However, no sensitivity information will be available.

\paragraph{Semi-continuous variables}
\declpropindex{variable}{SemiContinuous}

To any continuous or integer variable you can assign the property {\tt
SemiContin\-uous}. This indicates to the solver that this variable is either
zero, or lies within its specified range. Not all solvers support
semi-continuous variables. In the latter case, AIMMS will automatically add
the necessary constraints to the model.

\subsection{Sensitivity related properties}\label{sec:var.sensitivity}

\paragraph{Basic, superbasic, and nonbasic variables}
\herelabel{prop:var.Basic}
\declpropindex{variable}{Basic} \suffindex{variable}{Basic}
\index{variable!basic} \index{variable!superbasic} \index{variable!nonbasic}
\index{basic variable} \index{superbasic variable} \index{nonbasic variable}

With the {\tt Basic} property you can instruct AIMMS to retrieve basic
information of a specific variable from the solver. If retrieved, basic
information can be accessed through the {\tt .Basic} suffix. The basic
information is presented as an element in the predefined AIMMS set {\tt
AllBasicValues} (i.e.\ {\em \{Basic, Nonbasic, Superbasic\}}). In linear
programming a variable will either be basic or nonbasic, while in nonlinear
programming the number of variables with zero reduced cost can be larger than
the number of constraints. The solution algorithm then divides these variables
into so-called {\em basics} and {\em superbasics}. The basic variables define a
square system of nonlinear equations which is solved for fixed values of the
remaining variables. The superbasics are assigned a fixed value between their
bounds, while the nonbasics take their value at a bound.

\paragraph{The {\tt ReducedCost}
property}\herelabel{attr:var.marginal} \index{sensitivity analysis}
\declpropindex{variable}{ReducedCost} \suffindex{variable}{ReducedCost}
\AIMMSlink{ReducedCost}

You can use the {\tt ReducedCost} property to specify whether you are
interested in the reduced cost values of the variable after each {\tt SOLVE}
step. Storing the reduced costs of all variables may be very memory consuming,
therefore, the default in AIMMS is not to store these values. If reduced
costs are requested, the stored values can be accessed through the suffices
{\tt .ReducedCost} or {\tt .m}.

\paragraph{Interpretation of reduced cost}\herelabel{attr:var.reducedcost}
\index{use of!reduced cost} \index{variable!reduced cost} \index{reduced cost}

The reduced cost indicates by how much the cost coefficient in the objective
function should be reduced before the variable becomes active (off its bound).
By definition, the reduced cost value of a variable between its bounds is zero.
The precise mathematical interpretation of reduced cost is discussed in most
text books on mathematical programming. Note: if a basic or superbasic variable
has a reduced cost of zero then it will be displayed as 0.0, but if a nonbasic
variable has a reduced cost of zero then it will be displayed as \verb|ZERO|.

\paragraph{Unit of reduced cost}
\index{variable!unit of reduced cost} \index{reduced cost!unit} \index{unit!of
reduced cost}

When the variables in your model have an associated unit (see
Chapter~\ref{chap:units}), special care is required in interpreting the values
returned through the {\tt .Reduced\-Cost} suffix. To obtain the reduced cost in
terms of the units specified in the model, the values of the {\tt .ReducedCost}
suffix must be scaled as explained in Section~\ref{sec:units.scaling.mp}.

\paragraph{The property {\tt Coefficient\-Range}}\herelabel{attr:var.coeff-ranges}
\index{sensitivity analysis} \declpropindex{variable}{CoefficientRange}
\suffindex{variable}{SmallestCoefficient}
\suffindex{variable}{NominalCoefficient}
\suffindex{variable}{LargestCoefficient} \AIMMSlink{smallestcoefficient}
\AIMMSlink{nominalcoefficient} \AIMMSlink{largestcoefficient}

With the property {\tt CoefficientRange} you request AIMMS to conduct a
first type of sensitivity analysis on this variable during a {\tt SOLVE}
statement of a linear program. The result of this sensitivity analysis are
three parameters, representing the smallest, nominal, and largest values for
the {\em objective coefficient} of the variable so that the optimal basis
remains constant. Their values are accessible through the suffices {\tt
.SmallestCoefficient}, {\tt .NominalCoefficient} and {\tt .LargestCoefficient}.

\paragraph{The property {\tt ValueRange}}\herelabel{attr:var.var-ranges}
\index{sensitivity analysis} \declpropindex{variable}{ValueRange}
\suffindex{variable}{SmallestValue} \suffindex{variable}{LargestValue}
\AIMMSlink{smallestvalue} \AIMMSlink{largestvalue}

With the property {\tt ValueRange} you request AIMMS to conduct a second
type of sensitivity analysis during a {\tt SOLVE} statement of a linear
program. The result of the sensitivity analysis are two parameters,
representing the smallest and largest values that the {\em variable} can take
while holding the objective value constant. Their values are accessible through
the {\tt .SmallestValue} and {\tt .LargestValue} suffices.

\paragraph{Linear programs only}

AIMMS only supports the sensitivity analysis conducted through the
properties {\tt CoefficientRange} and {\tt ValueRange} for linear mathematical
programs. If you want to apply these types of analysis to the final solution of
a mixed-integer program, you should fix all integer variables to their final
solution (using the {\tt .NonVar} suffix) and re-solve the resulting
mathematical program as a linear program (e.g.\ by adding the clause {\tt WHERE
type:='lp'} to the {\tt SOLVE} statement).

\paragraph{Storage and computational costs}
\index{sensitivity analysis!memory considerations}

Setting any of the properties {\tt ReducedCost}, {\tt CoefficientRange} or {\tt
ValueRange} may result in an increase of the memory usage. In addition,
the computations required to compute the {\tt ValueRange} may considerably
increase the total solution time of your mathematical program.

\paragraph{Constraint related properties}
\index{constraint!implied by variable definition} \index{variable!implied
constraint}

Whenever a defined variable (which is not declared {\tt Inline}) is part of a
mathematical program, AIMMS implicitly adds a constraint to the generated
model expressing this definition. In addition to the variable-related
sensitivity properties discussed in this section, you can specify the
constraint-related sensitivity properties {\tt ShadowPrice}, {\tt
RightHandSideRange} and {\tt ShadowPriceRange} (see also
Section~\ref{sec:var.constr}) for such variables to obtain the sensitivity
information that can be related to these constraint. You can access the
requested sensitivity information by appending the associated suffices to the
name of the defined variable.

\subsection{Uncertainty related properties and attributes}\label{sec:var.uncertainty}

\paragraph{Stochastic programming and robust optimization}

The AIMMS modeling language offers facilities for both stochastic programs and robust optimization models. 
For both types of models you can specify special {\tt Variable} properties and attributes to define uncertainty-related relationships. 

\paragraph{The {\tt Stochastic} property}

Through the {\tt Stochastic} property you can indicate that, within a
stochastic model, the variable can hold scenario-dependent solutions.
AIMMS will add a {\tt Stage} attribute for every variable for which the
{\tt Stochastic} property has been set.

\paragraph{The {\tt Stage} attribute}
\declpropindex{variable}{Stochastic}\declattrindex{variable}{Stage}
\AIMMSlink{variable.stage}

The value of the {\tt Stage} attribute must be a numerical expression evaluating 
to in integer number
indicating the stage at the end of which the variable takes its value
during the solution process of a stochastic model. Stochastic programming,
and the {\tt Stochastic} property and {\tt Stage} attribute are discussed
in full detail in Section~\ref{sec:stoch.stoch}.

\paragraph{The {\tt Adjustable} property}

By setting the {\tt Adjustable} property for a variable, you indicate that a variable
in a robust optimization model has a functional dependency on some or all of the uncertain parameters 
in the model. If you declare a variable to be adjustable, the {\tt Dependency} attribute also 
becomes available for that variable. 
\paragraph{The {\tt Dependency} attribute}
\declpropindex{variable}{Adjustable}\declattrindex{variable}{Dependency}
\AIMMSlink{variable.dependency}

Through the {\tt Dependency} attribute you specify the precise 
collection of uncertain parameters on which the variable at hand depends. At this moment, AIMMS only 
supports affine relations between uncertain parameters and adjustable variables. The precise semantics of the 
{\tt Dependency} attribute is discussed in Section~\ref{sec:robust.adjustable}.

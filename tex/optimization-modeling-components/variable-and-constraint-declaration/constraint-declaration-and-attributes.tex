\section{\tttext{Constraint} declaration and attributes}\label{sec:var.constr}

\paragraph{Definitions}
\index{use of!constraint} \index{constraint!use of}

Constraints form the major mechanism for specifying a mathematical program in
AIMMS. They are used to restrict the values of variables with interlocking
relationships. {\em Constraints} are numerical relations containing expressions
in terms of variables, parameters and constants.

\paragraph{Constraint attributes}
\declindex{Constraint} \AIMMSlink{constraint}

The possible attributes of constraints are given in
Table~\ref{table:var.attr-constraint}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}               & {\bf See also} \\
                &                                    & {\bf page} \\
\hline
\verb|IndexDomain|   & {\em index-domain}                  & \pageref{attr:par.index-domain}\\
\verb|Unit|     &{\em  unit-valued expression}          &
                \pageref{attr:par.unit}, \pageref{chap:units}\\
\verb|Text|         & {\em string     }                 & \pageref{attr:prelim.text}\\
\verb|Comment|      & {\em comment string }             & \pageref{attr:prelim.comment}\\
\verb|Definition|   & {\em expression}                  &
\pageref{attr:par.definition}, \pageref{attr:var.definition}\\
\verb|Property|  & {\tt NoSave}, {\tt Sos1}, {\tt Sos2}, {\tt IndicatorConstraint}      &
\pageref{attr:set.property}, \pageref{attr:par.property}\\
         & {\tt Level}, {\tt Bound}, {\tt Basic}, {\tt ShadowPrice}, &\pageref{attr:var.property}\\
         & {\tt RightHandSideRange}, {\tt ShadowPriceRange}, & \\
         & {\tt IsDiversificationFilter}, {\tt IsRangeFilter}, & \\
         & {\tt IncludeInLazyConstraintPool}, & \\
         & {\tt IncludeInCutPool}, {\tt Chance} & \\
\hline
% \verb|SOS PRIORITY| & {\em expression} & \\
\verb|SosWeight|  & {\em sos-weights} & \\
\verb|ActivatingCondition| & {\em expression} & \\
\hline
\verb|Probability| & {\em expression} & \pageref{sec:var.constr.chance}, \pageref{attr:robust.chance.probability} \\
\verb|Aproximation| & {\em element-expression} & \pageref{sec:var.constr.chance}, \pageref{attr:robust.chance.approximation} \\
\hline\hline
\end{tabular}
\caption{{\tt Constraint} attributes}\label{table:var.attr-constraint}
\end{aimmstable}

\paragraph{Domain restriction for constraints}\herelabel{attr:var.constr.index-domain}
\declattrindex{constraint}{IndexDomain} \AIMMSlink{constraint.index_domain}

Restricting the domain of constraints through the {\tt IndexDomain} attribute
influences the matrix generation process. Constraints are generated only for
those tuples in the index domain that satisfy the domain restriction.

\paragraph{The {\tt Definition} attribute}\herelabel{attr:var.constr.definition}
\declattrindex{constraint}{Definition} \AIMMSlink{constraint.definition}

With the {\tt Definition} attribute of a constraint you specify a numerical
relationship between variables in your model. Without a definition a constraint
is indeterminate. Constraint definitions consist of two or three expressions
separated by one of the relational operators ``{\tt =}'', ``{\tt >=}'' or
``{\tt <=}''.

\paragraph{Example}
The following constraints express the simultaneous requirements that the sum of
all transports from a city {\tt i} must not exceed {\tt Supply(i)}, and that
for each city {\tt j} the {\tt Demand(j)} must be met.
\begin{example}
Constraint SupplyConstraint {
    IndexDomain  : i;
    Unit         : kton;
    Definition   : sum( j, Transport(i,j) ) <= Supply(i);
}
Constraint DemandConstraint {
    IndexDomain  : j;
    Unit         : kton;
    Definition   : sum( i, Transport(i,j) ) >= Demand(j);
}
\end{example}

\paragraph{Allowed relationships}
\index{constraint!allowed relations}

If $a$ and $b$ are expressions consisting of only parameters and $f(x,\dots)$
and $g(x,\dots)$ are expressions containing parameters and variables, the
following two kinds of relationships are allowed.
\[
        a \leq f(x,\dots) \leq b \qquad \mbox{or} \qquad
        f(x,\dots) \gtrless g(x,\dots)
\]
where $\gtrless$ denotes any of the relational operators ``{\tt =}'', ``{\tt
>=}'' or ``{\tt <=}''.  Either $a$ or $b$ can be omitted if there is no lower
or upper bound on the expression $f(x,\dots)$, respectively.  When both $a$ and
$b$ are present, the constraint is referred to as a {\em ranged} constraint.
The expressions may have linear and nonlinear terms, and may utilize the full
range of intrinsic functions of AIMMS except for the random number
functions.

\paragraph{Conditional expressions in constraints}

You must take extreme care to ensure continuity when the constraints in your
model contain logical conditions that include references to variables. Such
constraints are viewed by AIMMS as nonlinear constraints, and thus can only
be passed to a solver that can handle nonlinearities. It is possible that the
outcome of a logical condition, and thus the form of the constraint, changes
each time the underlying solver asks AIMMS for function values and
gradients. For example, if {\tt x(i)} is a decision variable, and a constraint
contains the expression
\begin{example}
    sum[ i, if ( x(i) > 0 ) then  x(i)^2 endif ]
\end{example}
it may or may not contain the term {\tt x(i)}\verb|^2|, depending on the
current value of {\tt x(i)}. In this example, both the expression and its
gradient are continuous functions at {\tt x(i) = 0}.

\subsection{Constraint properties}\label{sec:var.constr.property}

\paragraph{The {\tt Property} attribute}\herelabel{attr:var.constr.property}
\declattrindex{constraint}{Property} \AIMMSlink{constraint.property}

With the {\tt Property} attribute you can specify further characteristics of
the constraint at hand. The possible properties of a constraint are {\tt
NoSave}, {\tt Sos1}, {\tt Sos2}, {\tt Level}, {\tt Bound}, {\tt Basic}, {\tt
ShadowPrice}, {\tt RightHandSideRange}, and {\tt ShadowPriceRange}.

\paragraph{The {\tt NoSave} property}
\declpropindex{constraint}{NoSave}

When you specify the {\tt NoSave} property you indicate that you do not want
AIMMS to store data associated with the constraint in a case, regardless of
the specified case identifier selection.

\subsection{SOS properties}\label{sec:var.constr.sos}

\paragraph{The SOS properties}

The constraint types {\tt Sos1} and {\tt Sos2} are used in mixed integer
programming, and mutually exclusive. In the context of mathematical programming
SOS is an acronym for Special Ordered Sets. A SOS set is associated with every
(individual) constraint of type {\tt Sos1} or {\tt Sos2}.

\paragraph{Additional SOS attribute}

When you specify that a constraint is of type {\tt Sos1} or {\tt Sos2},
an additional SOS-specific attributes becomes available, namely the {\tt SosWeight} 
attributes. With this attributes, you can
provide further information to the solver about the contents and ordering of
the SOS set to be associated with the constraint.

\paragraph{{\tt Sos1} constraints}
\declpropindex{constraint}{Sos1}

A type {\tt Sos1} constraint specifies to the solver that at most one of the
variables within the SOS set associated with the constraint is allowed to be
nonzero, while all other variables in the SOS set must be zero. Inside a {\tt
Sos1} constraint all variables in the SOS set must have a lower bound of zero
and an upper bound greater than zero.

\paragraph{{\tt Sos2} constraints}
\declpropindex{constraint}{Sos2}

A type {\tt Sos2} constraint specifies to the solver that at most two
consecutive variables within the SOS set associated with the constraint are
allowed to be nonzero, while all other variables within the SOS set must be
zero. All individual variables within the SOS set must have a lower bound of
zero and an upper bound greater than zero. The order of the individual
variables within the SOS set is determined by their weights (as specified in
the {\tt SosWeight} attribute), where the ordering is from low to high weight.


% \paragraph{The {\tt SOS PRIORITY} attribute}
% \declattrindex{constraint}{SOS PRIORITY} \AIMMSlink{constraint.sos_priority}
% 
% With the {\tt SOS PRIORITY} attribute you can assign priorities to SOS sets.
% The value of this attribute must be an expression using some or all of the
% indices in the index domain of the constraint, and must be nonnegative. All SOS
% sets with priority zero will be considered last by the branch-and-bound process
% of the solver. For SOS sets with a positive priority value, those with the
% smallest priority value will be considered first by the solver.

\paragraph{The {\tt SosWeight} attribute}
\declattrindex{constraint}{SosWeight} \AIMMSlink{constraint.sos_weight}

With the {\tt SosWeight} attribute you must specify the contents of the SOS
set to be associated with a {\tt Sos1} or {\tt Sos2} constraint, as well the
ordering of its elements. Section~\ref{OMsos_sets} of the AIMMS book on
Optimization Modeling describes how these weights are used during the
branch-and- bound process. The syntax of the {\tt SosWeight} attribute is as
follows.

\paragraph{Syntax}
\syntaxdiagram{sos-weights}{sos-weights}

\paragraph{}

\syntaxmark{variable-reference} Within the {\tt SosWeight} attribute you can
(but need not) specify a weight for every variable occurring in the constraint.
Each weight must be an expression using all the indices in the index domain of
the variable plus some or all of the indices in the index domain of the
constraint. All weights specified for a particular constraint must be unique,
i.e.\ you cannot specify the same weight for two (individual) variables. The
SOS set to be associated with the constraint will be constructed from all
variables---within the domain of both the constraint and variable---for which a
nonzero weight has been specified in the {\tt SosWeight} attribute, i.e.\ if the
value of the specified weight is {\tt 0.0} for a particular tuple, the corresponding
individual variable will not be included in the SOS set. The ordering of
variables within the SOS set is from low to high weight.

\paragraph{Consistency}

If you do not specify SOS weights, AIMMS will make sure that ordering of variables in each SOS set
is consistent over all SOS sets. If you specify SOS weights yourself, you have to make sure that the variable
orderings of all SOS sets of type {\tt Sos2} are consistent, or your model might become infeasible if
feasibility requires that two adjacent variables in one SOS set become nonzero, which are ordered
inconsistently in another SOS set. Therefore, AIMMS requires that you specify the {\tt SosWeight}
attributes for {\em all} SOS constraints in your model, whenever you specify it for {\em one} SOS constraint.

\paragraph{Example}

The following is specification of {\tt Sos2} constraint to determine the
variable {\tt y} piece-wise linearly from a variable {\tt x(i)}.
\begin{example}
Constraint DetermineY {
    Property     : Sos2;
    Definition   : y = sum[ i, x(i)*c(i) ];
    SosWeight    : x(i) : XWeight(i);
}
\end{example}

\subsection{Solution pool filtering}\label{sec:var.constr.solutionpool}

\paragraph{Solution pool}
\index{solution pool}

During the solution process of a MIP problem, the solvers {\sc Cplex} and {\sc Gurobi} are capable of storing multiple feasible integer solutions in a solution pool, for instance, to capture solutions with attractive properties that are hard to express in a linear fashion. 

\paragraph{Filtering}
\index{solution pool!filtering}\index{filtering!solution pool}

While populating the solution pool, {\sc Cplex} offers advanced filtering capabilities, allowing you to control which solutions end up in the solution pool.
{\sc Cplex} provides two predefined ways to filter solutions:
\begin{itemize}
\item
if you want to filter solutions based on their difference as compared to a reference solution, use a {\em diversity} filter, or
\item 
if you want to filter solutions based on their validity in an additional linear constraint, use a {\em range} filter.
\end{itemize}
To enable filters the {\sc Cplex} option \verb|Do_Populate| need to be on.

\paragraph{Diversity filters}
\index{filtering!diversity filter}

A diversity filter allows you to generate solutions that are similar to (or different from) a set of reference values that you specify for a set of binary variables. In particular, you can use a diversity filter to generate more solutions that are similar to an existing solution or to an existing partial solution. Several diversity filters can be used simultaneously, for example, to generate solutions that share the characteristics of several different solutions.

\paragraph{The {\tt IsDiversificationFilter} property}
\propindex{IsDiversificationFilter}

In AIMMS, a constraint is used as a diversity filter if the constraint property {\tt IsDiversificationFilter} has been set. In a diversification filter, the {\tt Abs} function is used to measure the distance from a given binary variable, and all variables should only occur as the argument of an {\tt Abs} function.

\paragraph{Example}
This following diversification filter forces the solutions to have a distance of at least 1 from variable {\tt x}.
\begin{example}
Constraint filter1 {
    Property     :  IsDiversificationFilter;
    Definition   :  Abs(x - 1) >= 1;
}
\end{example}

\paragraph{Range filters}
\index{filtering!range filter}

A  range filter allows you to generate solutions that obey a new constraint, specified as a linear expression within a range. Range filters can be used to express diversity constraints that are more complex than the standard form implemented by diversity filters. In particular, range filters also apply to general integer variables, semi-integer variables, continuous variables, and semi-continuous variables, not just to binary variables.

\paragraph{The {\tt IsRange- Filter} property}
\propindex{IsRangeFilter}

In AIMMS, a constraint is used as a range filter if the constraint property {\tt IsRangeFilter} has been set for the constraint.

\paragraph{Example}
The following range filter specifies that any solution to be added to the solution pool should satisfy the following constraint.
\begin{example}
Contraint filter2 {
   Property     :  IsRangeFilter;
   Definition   :  x + y + z >= 2;
}
\end{example}

\subsection{Indicator constraints, lazy constraints and cut pools}\label{sec:var.constr.indicator}

\paragraph{Indicator constraints}
\index{constraint!indicator}\index{indicator constraint}

An indicator constraint is a new way of controlling whether or not a constraint takes effect, based on the value of a binary variable.  Traditionally, such relationships are expressed by so-called big-$M$ formulations. Big-$M$ formulations, however, can introduce unwanted side-effects and numerical instabilities into a mathematical program. Using indicator constraints, such relationships between a constraint and a variable can be directly expressed in the constraint declaration. Indicator constraints are supported by the solvers {\sc Cplex}, {\sc Gurobi} and {\sc Odh-Cplex}.

\paragraph{The {\tt Indicator\-Constraint} property}
\propindex{IndicatorConstraint}

You can specify that a constraint is an indicator constraint by settings it {\tt Indi\-ca\-torConstraint} property. For indicator constraints, a new attribute called {\tt ActivatingCondition} will become available in the constraint declaration.

\paragraph{The {\tt ActivatingCondition} attribute}
\declattrindex{constraint}{ActivatingCondition} \AIMMSlink{constraint.activating_condition}

Through the {\tt ActivatingCondition} attribute you can specify under which condition the constraint definition should become active during the solution process. Its value should be an expression of the form
\begin{quote}
    binary-variable {\em\tt =} expression
\end{quote}
where the {\em expression} must take one of the values 0 or 1. Note: stochastic variables and parameters are not allowed inside an activation condition.

\paragraph{Example}

Consider the following big-$M$ constraint
\begin{example}
Constraint BigMConstraint {
    Definition : x1 + x2 <= M*y;
}
\end{example}
where {\tt y} is a binary variable. Using the {\tt IndicatorConstraint} property,
the constraint can be reformulated as an indicator constraint as follows
\begin{example}
Constraint NonBigMConstraint {
    Property             : IndicatorConstraint;
    ActivatingCondition  : y = 0;
    Definition           : x1 + x2 = 0;
}
\end{example}
The constraint only becomes effective, whenever the binary variable {\tt y} takes the value 0. To solve the model with the indicator constraint, you need the {\sc Cplex}, {\sc Gurobi} or {\sc Odh-Cplex} solver.

\paragraph{Lazy constraints}
\index{constraint!lazy}\index{lazy constraint}

Sometimes, for a MIP formulation, a user can already identify a group of constraints that are unlikely to be violated (lazy constraints). Simply including these constraints in the original formulation could make the LP subproblem of a MIP optimization very large or too expensive to solve. {\sc Cplex}, {\sc Gurobi} and {\sc Odh-Cplex} can handle problems with lazy constraints more efficiently, and therefore AIMMS allows you to identify lazy constraints in your model.

\paragraph{The {\tt IncludeIn- LazyConstraint- Pool} property}
\propindex{IncludeInLazyConstraintPool}

You can specify that a constraint should be added to the pool of lazy constraints considered by {\sc Cplex}, {\sc Gurobi} or {\sc Odh-Cplex} by setting the property {\tt Include\-In\-Lazy\-Constraint\-Pool}. You need the {\sc Cplex}, {\sc Gurobi} or {\sc Odh-Cplex} solver to use this constraint property. When solving your MIP model, {\sc Cplex}, {\sc Gurobi} and {\sc Odh-Cplex} will only consider these constraints when they are violated.

\paragraph{User cut pools}
\index{constraint!user cut}\index{user cut pool}

As discussed in Section~\ref{sec:mp.suffix}, AIMMS allows you to add cuts to your mathematical program on the fly during the solution process by using the {\tt Callback\-Add\-Cut} callback. However, when the set of cuts you want to generate is fixed and known upfront, using the {\tt Callback\-Add\-Cut} may add significant overhead to the solution process of your model while you don't need its flexibility. For those situations, {\sc Cplex} allows you to specify a fixed pool of user cuts during the generation of your mathematical program.

\paragraph{The {\tt Include- InCutPool} property}
\propindex{IncludeInCutPool}

By setting the constraint property {\tt IncludeInCutPool} you can indicate that this constraint should be included in the pool of user cuts associated with your mathematical program. You need the {\sc Cplex} solver to use this constraint property. When solving your MIP model, {\sc Cplex} will consider the user cuts added in this manner when appropriate.

\subsection{Constraint levels, bounds and marginals}\label{sec:constr.values}

\paragraph{Constraint levels and bounds}

A constraint in AIMMS can conceptually be divided such that one side
consists of all variable terms, whereas the other side consists of all
remaining constant terms. The {\em level} value of a constraint is the
accumulated value of the variable terms, while the constant terms make up the
{\em bound} of the constraint.

\paragraph{The {\tt Level}, {\tt Bound}, {\tt Basic} and {\tt
ShadowPrice} properties} \declpropindex{constraint}{Level}
\declpropindex{constraint}{Bound} \declpropindex{constraint}{Basic}
\declpropindex{constraint}{ShadowPrice} \suffindex{constraint}{Lower}
\suffindex{constraint}{Upper} \suffindex{constraint}{Basic}
\suffindex{constraint}{ShadowPrice} \AIMMSlink{shadowprice}

With the {\tt Level}, {\tt Bound}, {\tt Basic} and {\tt ShadowPrice} properties
you indicate whether you want to store (and have access to) particular
parametric data associated with the constraint.
\begin{itemize}
\item When you specify the {\tt Level} property AIMMS will retain
the level values of the constraint as provided by the solver. You can access
the level values of a constraint by using the constraint name as if it were a
parameter.
\item By specifying the {\tt Bound} property, AIMMS will store the
upper and lower bound of the constraint as employed by the solver. You get
access to the bounds by using the {\tt .Lower} and {\tt .Upper} suffices with
the constraint identifier.
\item If the {\tt Basic} property has been specified, AIMMS stores
basic information is available through the {\tt .Basic} suffix as an element in
of the predefined AIMMS set {\tt AllBasicValues}. A constraint is said to be
basic (nonbasic or superbasic) if its associated slack variable is basic
(nonbasic or superbasic).
\item With the {\tt ShadowPrice} property you indicate that you want to
store the shadow prices as computed by the solver. You can access these shadow
prices by means of the {\tt .ShadowPrice} attribute.
\end{itemize}

\paragraph{Interpretation of shadow prices}\herelabel{par:constr.interpr.shadowpr}
\index{sensitivity analysis} \index{use of!shadow price} \index{shadow price}
\index{constraint!shadow price}

The shadow price (or dual value) of a constraint is the marginal change in the
objective value with respect to a change in the right-hand side (i.e.\ the
constant part) of the constraint. This value is determined by the solver after
a {\tt SOLVE} statement has been executed. The precise mathematical
interpretation of the shadow price is discussed in detail in many text books on
mathematical programming. Note: if a basic or superbasic constraint
has a shadow price of zero then it will be displayed as 0.0, but if a nonbasic
constraint has a shadow price of zero then it will be displayed as \verb|ZERO|.

\paragraph{Unit of shadow price}
\index{variable!unit of shadow price} \index{shadow price!unit} \index{unit!of
shadow price}

When the variables and constraints in your model have an associated unit (see
Chapter~\ref{chap:units}), special care is required in interpreting the values
returned through the {\tt .ShadowPrice} suffix. To obtain the shadow price in
terms of the units specified in the model, the values of the {\tt .ShadowPrice}
suffix must be scaled as explained in Section~\ref{sec:units.scaling.mp}.

\paragraph{The property {\tt RightHandSideRange}}\herelabel{attr:var.constr.bound-ranges}
\declpropindex{constraint}{RightHandSideRange}
\suffindex{constraint}{SmallestRightHandSide}
\suffindex{constraint}{NominalRightHandSide}
\suffindex{constraint}{LargestRightHandSide}
\AIMMSlink{smallestrighthandside}
\AIMMSlink{nominalrighthandside}
\AIMMSlink{largestrighthandside}

By specifying the {\tt RightHandSideRange} property you request AIMMS to
conduct a first type of sensitivity analysis on this constraint during a {\tt
SOLVE} statement of a linear program. The result of this sensitivity analysis
are three parameters defined over the domain of the constraint. Two of these
parameters represent the smallest and largest values of an interval over which
an individual {\em right-hand side} (or left-hand side) value can be varied such
that the basis remains constant. Consequently, the shadow prices and the reduced
costs remain unchanged for variations of a single value within the interval.
The third parameter specifies the nominal value for the right-hand side
(or left-hand side) of the constraint.

\paragraph{Single sided or ranged constraint}

There are three cases we have to consider for the {\tt RightHandSideRange} property:
\begin{itemize}
\item if the constraint is single sided (i.e.\ $f(x) \leq a$) then the smallest, nominal, and largest value for the constraint side are reported (both when constraint is binding and not binding)
\item if the constraint is of range type (i.e.\ $a \leq f(x) \leq b$) and it is binding at one side, then the smallest, nominal, and largest value for the binding side of the constraint are reported
\item if the constraint is of range type (i.e.\ $a \leq f(x) \leq b$) and it is not binding at neither side, then the lowest upper bound and the highest lower bound are reported.
\end{itemize}
The values are accessible through the suffices {\tt .SmallestRightHandSide},
{\tt .Nomin\-al\-RightHandSide}, and {\tt .LargestRightHandSide}.

\paragraph{The property {\tt ShadowPriceRange}}
\herelabel{attr:var.constr.mar-ranges}
\declpropindex{constraint}{ShadowPriceRange}
\suffindex{constraint}{SmallestShadowPrice}
\suffindex{constraint}{LargestShadowPrice}
\AIMMSlink{smallestshadowprice}
\AIMMSlink{largestshadowprice}

With the {\tt ShadowPriceRange} property you request AIMMS to conduct a
second type of sensitivity analysis on this constraint during a {\tt SOLVE}
statement of a linear program. The result of the sensitivity analysis are two
parameters defined over the domain of the variable. The values assigned to the
parameters will be the smallest and largest values that the {\em shadow price}
of the constraint can take while holding the objective value constant. The
smallest and largest values of the constraint marginals are accessible through
the suffices {\tt .SmallestShadowPrice} and {\tt .LargestShadowPrice}.

\paragraph{Linear programs only}

As with the advanced sensitivity properties of variables (see
Section~\ref{sec:var.properties}), AIMMS also supports the advanced
sensitivity analysis conducted through the properties {\tt RightHandSideRange}
and {\tt ShadowPriceRange} for linear mathematical programs only. Again, if you
want to apply these types of analysis to the final solution of a mixed-integer
program, you should fix all integer variables to their final solution (using
the {\tt .NonVar} suffix) and re-solve the resulting mathematical program as a
linear program.

\paragraph{Storage and computational costs}
\index{sensitivity analysis!constraints memory considerations}

Setting any of the properties {\tt ShadowPrice}, {\tt ShadowPriceRange} or {\tt
Right\-Hand\-Side\-Range} may result in an increase of the memory usage. In addition,
the computations required to compute the {\tt ShadowPriceRange} may considerably
increase the total solution time of your mathematical program.

\subsection{Constraint suffices for global optimization}\label{sec:var.constr.glob-suff}

\paragraph{Suffices for global opti- mization}
\AIMMSlink{constraint.globopt}

AIMMS provides a number of constraint suffices especially for the global optimization solver {\sc Baron}. They are:
\begin{itemize}
\item the {\tt .Convex} suffix, and
\item the {\tt .RelaxationOnly} suffix.
\end{itemize}
By providing additional knowledge, that cannot be determined automatically by {\sc Baron} itself,
about the constraints in your model through these suffices, the {\sc Baron} solver may be able to
optimize your global optimization model in a more efficient manner. For more detailed information
about the specific capabilities of the {\sc Baron} solver, you are referred to the {\sc Baron} website
\url{http://www.theoptimizationfirm.com/}.

\paragraph{The {\tt .Convex} suffix}
\suffindex{constraint}{Convex}

The algorithm of the {\sc Baron} solver exploits convexity---either identified automatically by {\sc Baron} itself
or explicitly supplied in the model formulation---in order to generate polyhedral cutting planes and relaxations for
multivariate non-convex problems. Through the {\tt .Convex} suffix you can explicitly indicate that a particular
constraint is convex if {\sc Baron} is unable to determine its convexity automatically.

\paragraph{The {\tt .Relax- ationOnly} suffix}
\suffindex{constraint}{RelaxationOnly}

Using the {\tt .RelaxationOnly} suffix, you can considerably enhance the convexification capabilities
of {\sc Baron}. Some nonlinear problem reformulations can often tighten the relaxation process
of {\sc Baron}'s branch-and-bound algorithm while making local search considerably more difficult.
By assigning a nonzero value to the {\tt .RelaxationOnly} suffix, you indicate to {\sc Baron} that
the constraint at hand should only be included as a relaxation to the branch-and-bound algorithm, while
it should be excluded from the local search.

\subsection{Chance constraints }\label{sec:var.constr.chance}

\paragraph{Chance constraints}
\declpropindex{constraint}{Chance}

The AIMMS modeling language offers facilities for robust optimization models, including support for {\em chance constraints} 
(see also Section~\ref{sec:robust.chance}). By setting the {\tt Chance} property of a constraint, the constraint will become a 
chance constraint when solving a mathematical program using robust optimization, using the distributions specified for the 
random parameters contained in its definition. When setting the {\tt Chance} property, two new attributes will become available, 
the {\tt Probability} attribute and the {\tt Approximation} attribute.

\paragraph{Only for robust optimization}

Note that setting the {\tt Chance} property does not influence the availability and use of the 
constraint outside the context of robust optimization. In that case, AIMMS will just use the original, deterministic, 
constraint definition, completely disregarding the uncertainty of the parameters used in the constraint.

\paragraph{The {\tt Probability} attribute}
\declattrindex{constraint}{Probability}

Through the {\tt Probability} attribute, you can specify the probability with which you want the constraint to be satisfied for 
any feasible solution to the robust counterpart of a robust optimization model. Its value must be a numerical expression in the 
range $[0,1]$.

\paragraph{The {\tt Approximation} attribute}
\declattrindex{constraint}{Approximation}\presetindex{AllChanceApproximationTypes}

When constructing the robust counterpart, AIMMS can use several types of approximations to approximate the chance constraint 
at hand. You can use the {\tt Approximation} attribute to specify the type of approximation you want to be applied. The 
chosen type of approximation may lead to a robust counterpart which is easier or harder to solve (see also 
Section~\ref{sec:robust.chance}). The value of the attribute must be an element expression into the predefined set {\tt AllChance\-ApproximationTypes}.

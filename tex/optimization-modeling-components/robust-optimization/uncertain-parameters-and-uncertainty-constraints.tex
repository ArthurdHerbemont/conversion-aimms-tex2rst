\section{Uncertain parameters and uncertainty constraints}\label{sec:robust.uncertain}
\index{uncertain parameter}\index{parameter!uncertain}

\paragraph{Uncertain parameters}

Uncertain parameters are modeled in AIMMS as numeric {\tt Parameters} for which the {\tt Uncertain} property has
been set (see also Section~\ref{sec:par.decl}). When a parameter has been declared {\tt Uncertain} AIMMS will create two new 
attributes {\tt Region} and  {\tt Uncertainty}. 

\paragraph{The {\tt Region} attribute}\label{attr:robust.region}
\declattrindex{parameter}{Region}

The {\tt Region} attribute of an uncertain parameter offers an easy way to define the uncertainty set without
the need to introduce additional uncertain parameters. AIMMS supports a number of predefined regions which you can enter here:
\begin{itemize}
\item ${\tt Box}(l,u)$,
\item ${\tt Ellipsoid}(c,r)$, and
\item ${\tt ConvexHull}(s, v(s))$.
\end{itemize}

\paragraph{{\tt Box} example}

If we want to specify that parameter {\tt A} is uncertain
and constrained as follows:
\[
	l(i,j) \leq A(i,j) \leq u(i,j)
\]
then it suffices to specify the uncertainty set of {\tt A} using its {\tt Region} attribute as follows
\begin{example}
Parameter A {
    IndexDomain  : (i,j);
    Property     : Uncertain;
    Region       : Box( l(i,j), u(i,j) );
}
\end{example}
where {\tt l(i,j)} and {\tt u(i,j)} are ordinary parameters in your model.

\paragraph{{\tt Ellipsoid} example}

It is also possible to specify the region using an {\tt Ellipsoid} region
\begin{example}
Parameter A {
    IndexDomain  : (i,j);
    Property     : Uncertain;
    Region       : Ellipsoid( A.level(i,j), r(i,j) );
}
\end{example}
which leads to an uncertainty set for {\tt A} defined as an ellipsoid around the nominal value of {\tt A} as follows:
\[
	\sum_{i,j} \Bigg( \frac{A(i,j) - A.\mbox{\em level}(i,j)}{r(i,j)} \Bigg)^2 \leq 1.
\]

\paragraph{{\tt ConvexHull} example}

The region can also be defined as a {\tt ConvexHull} region
\begin{example}
Parameter A {
    IndexDomain  : (i,j);
    Property     : Uncertain;
    Region 	     : ConvexHull( s, A_s(s,i,j) );
}
\end{example}
which says that the uncertain parameter {\tt A} belongs to an uncertainty set that is described by the 
convex hull of the values of a collection of values \verb|A_s| for a given set of scenarios, i.e., 
\begin{align*}
	A(i,j) &= \sum_s \lambda_s A_s(s,i,j)\\
	1  	   &=\sum_s \lambda_s, \qquad   \lambda_s \geq 0.
\end{align*}

\paragraph{Dependencies}

If there are two parameters {\tt A} and {\tt B} that both depend on scenario-dependent data, then those scenarios can either be 
dependent or independent. To differentiate between these two possibilities, AIMMS uses the name of the binding index used in 
the {\tt ConvexHull} operator. If the names of the binding indices are identical, then AIMMS assumes that the scenarios 
are dependent. If the index names are different, {\em even if they refer to the same scenario set}, AIMMS assumes the 
scenarios to be independent.  

\paragraph{Dependent scenarios example}

Consider the following two declarations of uncertain parameters
\begin{example}
Parameter A {
    IndexDomain  :  (i,j);
    Property     :  Uncertain;
    Region       :  ConvexHull( s, A_s(s,i,j) );
}
Parameter B {
    IndexDomain  :  (i,j);
    Property     :  Uncertain;
    Region       :  ConvexHull( s, B_s(s,i,j) );
}
\end{example}
Based on these declarations AIMMS will generate a single convex hull as follows
\begin{align*}
	\begin{bmatrix}A(i,j)\\B(i,j)\end{bmatrix} = &\sum_s \lambda_s \begin{bmatrix}A_s(s,i,j)\\B_s(s,i,j)\end{bmatrix}\\
	\sum_s \lambda_s =& 1, \qquad \lambda_s \geq 0.
\end{align*}
If {\tt A} and {\tt B} consist of a single value each, and there are two scenarios for {\tt s}, then the combined convex hull for {\tt A} and {\tt B} is depicted in Figure~\ref{fig:robust.ch1}.
\begin{aimmsfigure}
\psset{unit=0.2mm}
\begin{pspicture}(130,-80)(445,-296)
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx](145,-80)(145,-275) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx](145,-275)(445,-275) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx](145,-270)(145,-275) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx,linestyle=dashed,dash=5pt 5pt](145,-115)(445,-115) % Plain Dashed Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx,linestyle=dashed,dash=5pt 5pt](145,-210)(445,-210) % Plain Dashed Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx,linestyle=dashed,dash=5pt 5pt](210,-275)(210,-80) % Plain Dashed Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx,linestyle=dashed,dash=5pt 5pt](370,-275)(370,-80) % Plain Dashed Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.762mm}\psline[linecolor=lpx](210,-210)(370,-115) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](210,-275)(370,-275) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](145,-115)(145,-210) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\rput[lc]{*0}(290,-296){\shortstack{A}} % Plain Text
        \newrgbcolor{lpx}{0 0 0}\rput[lc]{*0}(130,-166){\shortstack{B}} % Plain Text
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](135,-115)(155,-115) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](135,-210)(155,-210) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](210,-265)(210,-285) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](370,-265)(370,-285) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0} % Set color to black again (default font color)
\end{pspicture}
\caption{Combined convex hull for dependent scenarios}\label{fig:robust.ch1}
\end{aimmsfigure}

\paragraph{Independent scenarios example}

If, on the other hand, both declarations are given as
\begin{example}
Parameter A {
    IndexDomain  :  (i,j);
    Property     :  Uncertain;
    Region       :  ConvexHull( s, A_s(s,i,j) );
}
Parameter B {
    IndexDomain  :  (i,j);
    Property     :  Uncertain;
    Region       :  ConvexHull( t, B_t(t,i,j) );
}
\end{example}
then AIMMS will generate two separate convex hulls as follows
\begin{align*}
	\begin{bmatrix}A(i,j)\\B(i,j)\end{bmatrix} = & 
		  \begin{bmatrix}\sum_{s}\lambda_s A_s(s,i,j)\\\sum_{t}\mu_t B_t(t,i,j)\end{bmatrix}\\
		\sum_s \lambda_s=&\sum_t \mu_t = 1, \qquad \lambda_s \geq 0, \mu_t \geq 0.
\end{align*}
If {\tt A} and {\tt B} consist of a single value each, and there are two scenarios for {\tt s} and {\tt t} each, then the combined convex hull for {\tt A} and {\tt B} is depicted in Figure~\ref{fig:robust.ch2}.
\begin{aimmsfigure}
\psset{unit=0.2mm}
\begin{pspicture}(130,-80)(445,-296)
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx](145,-80)(145,-275) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx](145,-275)(445,-275) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx](145,-270)(145,-275) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx,linestyle=dashed,dash=5pt 5pt](145,-115)(445,-115) % Plain Dashed Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx,linestyle=dashed,dash=5pt 5pt](145,-210)(445,-210) % Plain Dashed Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx,linestyle=dashed,dash=5pt 5pt](210,-275)(210,-80) % Plain Dashed Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.254mm}\psline[linecolor=lpx,linestyle=dashed,dash=5pt 5pt](370,-275)(370,-80) % Plain Dashed Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](210,-275)(370,-275) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](145,-115)(145,-210) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\rput[lc]{*0}(290,-296){\shortstack{A}} % Plain Text
        \newrgbcolor{lpx}{0 0 0}\rput[lc]{*0}(130,-166){\shortstack{B}} % Plain Text
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](135,-115)(155,-115) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](135,-210)(155,-210) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](210,-265)(210,-285) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.508mm}\psline[linecolor=lpx](370,-265)(370,-285) % Plain Solid Line
        \newrgbcolor{lpx}{0 0 0}\psset{linewidth=0.762mm}\psframe[linecolor=lpx,fillstyle=vlines](210,-115)(370,-210) % Plain Solid Rectangle
        \newrgbcolor{lpx}{0 0 0} % Set color to black again (default font color)
\end{pspicture}
\caption{Combined convex hull for independent scenarios}\label{fig:robust.ch2}
\end{aimmsfigure}

\paragraph{{\tt ConvexHullEx}}

The {\tt ConvexHull} operator AIMMS can be used to express that an uncertain parameter is defined as the
convex combination of a certain parameter on some set of scenarios. The {\tt ConvexHullEx} operator is an extension
for which the user explicitly has to define the ``lambda'' parameter as an uncertain parameter. For example:
\begin{example}
Parameter A {
    IndexDomain  : (i,j);
    Property     : Uncertain;
    Region 	     : ConvexHullEx( s, A_s(s,i,j), L(s,i) );
}
\end{example}
which says that the uncertain parameter {\tt A} belongs to an uncertainty set that is described by the 
convex hull of the values of a collection of values \verb|A_s| for a given set of scenarios using the uncertain
parameter {\tt L}, i.e., 
\begin{align*}
	A(i,j) &= \sum_s L_s(i) A_s(s,i,j)\\
	1  	   &=\sum_s L_s(i), \qquad   L_s(i) \geq 0.
\end{align*}

\paragraph{More flexibility}

The {\tt ConvexHullEx} operator offers more flexibility as demonstrated by the above example in which the
{\em lambda} parameter {\tt L} depends on the indices {\tt s} and {\tt i} while the implicitly generated
{\em lambda} parameter in case of the {\tt ConvexHull} operator only depends on the index {\tt s}.
Moreover, the {\em lambda} parameter can be used in the {\tt Dependency} attribute of an adjustable variable
(see Section~\ref{sec:robust.adjustable}). The same {\em lambda} parameter can be used in {\tt ConvexHullEx}
in regions of different uncertain parameters to define a dependency between the uncertain parameters. As the
{\em lambda} parameter is not an ordinary uncertainty parameter, it cannot be used in uncertainty constraints.

\paragraph{The {\tt Uncertainty} attribute}\label{attr:robust.uncertainty}
\declattrindex{parameter}{Uncertainty}

Through the {\tt Uncertainty} attribute of an uncertain parameter you can define a relation in term of other ordinary and 
uncertain parameters in your model which must hold for the uncertain value of that parameter. 

\paragraph{Example}

Consider the following declaration
\begin{example}
Parameter Demand {
    IndexDomain  : (c,t);
    Property     : Uncertain;
    Uncertainty  : Demand.level(c,t) + Sum[k, D(c,t,k) * xi(k)];
}
\end{example}
where {\tt D(c,t,k)} is an ordinary parameter and {\tt xi} an uncertain parameter. The reference to {\tt Demand.level} in
the {\tt Uncertainty} attribute refers to the deterministic (or nominal) value of {\tt Demand}. The uncertain value of 
{\tt Demand} is defined as its nominal value plus a linear combination of some other uncertain parameter {\tt xi(k)}.

\paragraph{Non-exclusive attributes}

Note that the {\tt Region} and {\tt Uncertainty} attributes are non-exclusive, i.e., you can use them in conjuction to each other. 
In such a case, AIMMS will make sure that the solution is robust with respect to both relations.

\paragraph{Uncertainty constraints}
\index{uncertainty constraint}
\index{constraint!uncertainty}

The {\tt Region} and the {\tt Uncertainty} attribute of a uncertain parameter can be used to specify possible
realizations of the uncertain parameters. In some cases, however, more flexibility is needed in specifying special relations for  
one or more uncertain parameters. For this purpose AIMMS allows you to specify {\tt UncertaintyConstraints}. 
An {\tt UncertaintyConstraint} is a constraint that specifies the relation between uncertain parameters. It is similar to an 
ordinary constraint in which the uncertain parameters play the role for variables; the definition of an {\tt UncertaintyConstraint} may only refer to normal and uncertain parameters, and not to variables.

\paragraph{Example}

The following example specifies a condition on an uncertain parameter that cannot be expressed through its {\tt Region} or {
\tt Uncertainty} attributes.
\begin{example}
Parameter A {
    IndexDomain  : (i,j);
    Property     : Uncertain;
}
UncertaintyConstraint ConditionOnA {
    IndexDomain  : i;
    Definition   : Sum( j, A(i,j) ) <= 1;
}
\end{example}

\paragraph{The {\tt Constraints} attribute}\label{attr:robust.constraints}

Through the {\tt Constraint} attribute of an {\tt UncertaintyConstraint} you can specify to which
(normal) constraints the {\tt UncertaintyConstraint} should apply. In this way it is possible to use different
uncertainty sets for different constraints. If the {\tt Constraints} attribute is empty then the
{\tt UncertaintyConstraint} will be active for all constraints.

\paragraph{Example}

Consider the following declarations
\begin{example}
UncertaintyConstraint ConditionOnA {
    IndexDomain  : i;
    Constraints  : CapacityRestriction(j) : UncertaintyDependency(i,j);
    Definition   : Sum( j, A(i,j) ) <= 1;
}
Constraint CapacityRestriction {
    IndexDomain  : j;
    Definition   : Sum( i, A(i,j) * Transport(i,j) ) <= Capacity(j);
}
Parameter UncertaintyDependency {
    IndexDomain  : (i,j);
    Definition   : 1 $ (i = j);
}
\end{example}
These declarations yield that the uncertainty constraint {\tt ConditionOnA(i)} is only active for
constraint {\tt CapacityRestriction(j)} for all elements {\tt j} equal to {\tt i}.

\paragraph{Generalized ellipsoid}

Besides linear uncertainty constraints, AIMMS also allows you to formulate the following uncertainty set for a uncertain parameter $\xi$, that generalizes the ellipsoidal uncertainty sets that can be defined by using the {\tt Ellipsoid} region:
\[
	\xi^T Q_0 \xi + \sum_{m=1}^{M} \sqrt{\xi^T Q_m \xi} \le b,
\]
where $Q_0$ and $Q_m$ should be positive semidefinite matrices. If your model contains an ellipsoidal
uncertainty constraint then the robust counterpart will become a second-order cone program,
except if the ellipsoidal uncertainty constraints are of the form
\[
	\sum_i \sqrt{\xi_i^2} \le b,
\]
in which case the robust counterpart will be a linear program.


\section{Chance constraints}\label{sec:robust.chance}
\index{chance constraint}\index{constraint!chance}

\paragraph{Chance constraints}

In the previous sections we assumed that each constraint with uncertain data was satisfied with probability 1. In many situations, 
however, such a requirement may lead to solutions that over-emphasize the worst-case. In such cases, it is more natural to 
require that a candidate solution has to satisfy a constraint with uncertain data for ``nearly all'' realizations of the 
uncertain data. More specifically, in this approach one requires that the robust solution has to satisfy the constraint with 
probability  at least $1 - \epsilon$, where $\epsilon \in [0,1]$ is a pre-specified small tolerance. 
Instead of the deterministic constraint 
\[
		a^T x \le b
\]
we now require that the {\em chance constraint}
\[
	\text{Prob} \left(x : a(\xi)^T x \le b \right) \geq 1 - \epsilon
\]
be satisfied, where the probability is associated with the specific distribution of the uncertain parameter(s) $\xi$.

\paragraph{Approximation}

In general, (linear) problems with chance constraints are very hard to solve even if the probability distribution of the 
uncertain data is completely known. It is, however, possible to construct safe tractable approximations of chance constraints
using robust optimization (see, for instance, Chapter 2 of \cite{bib:BGN09}).  The way a chance constraint is approximated
depends merely on the general characteristics of the data distribution, rather than on precise specification of 
the distribution. If more information is available about the distribution, this will generally result in a tighter approximation. 
A tighter approximation, however, could result in a more difficult solution process (for instance, requiring second-order cone  
programming instead of just linear programming).

\paragraph{Chance constraints in AIMMS}

The procedure to introduce chance constraints into your robust optimization model is as follows:
\begin{itemize}
\item indicate which parameters in your model should become random, and specify the properties of their distributions, and
\item specify which constraints should be considered chance constraints, and specify their probability and method of approximation. 
\end{itemize} 

\paragraph{Random parameters}\label{attr:robust.distribution}
\index{random parameter}\index{parameter!random}

A probability distribution is modeled in AIMMS as a numeric {\tt Parameter} for which the {\tt Random}
property has been set (see also Section~\ref{sec:par.decl}). If the property {\tt Random} is set,
AIMMS will create the mandatory {\tt Distribution} attribute for this parameter which must be used to specify
the characteristics of the distribution to be used for that parameter. All random parameters for which a distribution has been 
specified are considered to be independent.

\paragraph{Example}

Consider the following declaration
\begin{example}
Parameter Demand {
    IndexDomain  : i;
    Property     : Random;
    Distribution : Bounded(Demand(i).level,0.1);
}
\end{example}
This declaration states that parameter {\tt Demand} corresponds to a bounded probability distribution
with a mean equal to the nominal value of {\tt Demand} and a support of 0.1.

\paragraph{Supported distributions}
\index{random parameter!supported distributions}
\index{distribution!Bounded@{\tt Bounded}}
\index{Bounded distribution@{{\tt Bounded} distribution}}
\index{distribution!Unimodal@{\tt Unimodal}}
\index{Unimodal distribution@{{\tt Unimodal} distribution}}
\index{distribution!Symmetric@{\tt Symmetric}}
\index{Symmetric distribution@{{\tt Symmetric} distribution}}
\index{distribution!Support@{\tt Support}}
\index{Support distribution@{{\tt Support} distribution}}
\index{distribution!Gaussian@{\tt Gaussian}}
\index{Gaussian distribution@{{\tt Gaussian} distribution}}

AIMMS supports the distribution types listed in Table~\ref{fig:robust.distributions}
\begin{aimmstable}
\begin{tabular}{|l|p{7cm}|}
\hline\hline
{\bf Distribution} & {\bf Meaning}\\
\hline
${\tt Bounded}(m,s)$ & \pbsr mean $m$ with range $ [m-s,m+s]$\\
${\tt Bounded}(m,l,u)$ & \pbsr range $[l,u]$ and mean $m$ not in the center of the range\\
${\tt Bounded}(m_l,m_u,l,u)$ & \pbsr range $[l,u]$ and mean in interval $[m_l,m_u]$\\
${\tt Bounded}(m_l,m_u,l,u,v)$ & \pbsr range $[l,u]$ and mean in interval  $[m_l,m_u]$, and variance bounded by $v$\\
${\tt Unimodal}(c,s)$ & \pbsr unimodal around $c$ with range $[c-s,c+s]$\\
${\tt Symmetric}(c,s)$ & \pbsr symmetric around $c$ with range $[c-s,c+s]$\\
${\tt Symmetric}(c,s,v)$ & \pbsr symmetric around $c$ with range $[c-s,c+s]$, and variance bounded by $v$\\
${\tt Support}(l,u)$ & \pbsr range $[l,u]$ (and no information about the mean)\\
${\tt Gaussian}(m_l,m_u,v)$ & \pbsr Gaussian with mean in interval $[m_l,m_u]$ and variance bounded by $v$\\
\hline\hline
\end{tabular}
\caption{Supported distribution types for robust optimization}
\label{fig:robust.distributions}
\end{aimmstable}
All distributions in this table are bounded except the Gaussian distribution. The distributions ${\tt Bounded}(m_l,m_u,l,u)$,
${\tt Bounded}(m_l,m_u,l,u,v)$ and ${\tt Symmetric}(c,s,v)$  are currently not implemented.

\paragraph{Symmetric unimodal distribution}

A distribution is called unimodal if its density function is monotonically increasing up to a certain point $c$ and monotonically 
decreasing afterwards. For symmetric distribution AIMMS offers the possibility to mark it as unimodal by using the {\tt 
unimodal} keyword:
\begin{example}
Parameter Demand {
    IndexDomain  : i;
    Property     : Random;
    Distribution : Symmetric(Demand(i).level,0.1), unimodal;
}
\end{example}
The {\tt unimodal} keyword can only be used in combination with a symmetric distribution.

\paragraph{Linear relation}

In addition to specifying a random parameter using an independent distribution, AIMMS also allows you to define a random 
parameter as a linear combination of other random parameters (but not as combination of uncertain parameters). For example,
\begin{example}
Parameter Demand {
    Property     : Random;
    Distribution : Sum( i, xi(i) );
}
\end{example}
where {\tt xi} is an random parameter. To avoid cyclic definitions, AIMMS requires that the distributions of random 
parameters cannot be specified as an expression of other random parameters which are themselves defined as an expression of 
random parameters. 

\paragraph{Chance constraints}

A constraint in your mathematical program becomes a chance constraint in the context of robust optimization by setting its {\tt 
Chance} property. The definition of a chance constraint may only contain random parameters, normal parameters and variables. 
Uncertain parameters are not allowed inside a chance constraint. When setting the {\tt Chance} property for a constraint, you must specify two new attributes for the constraint, the {\tt Probability} attribute and the {\tt Approximation} attribute.  
It is allowed to use chance constraints in a mixed-integer program.

\paragraph{The {\tt Probability} attribute}
\declattrindex{constraint}{Probability}
\label{attr:robust.chance.probability}

The {\tt Probability} attribute specifies the probability with which the chance constraint should be satisfied when solving a robust optimization model. The value of the {\tt Probability} attribute should be a numerical expression in the range $[0,1]$. If the probability is 0, then AIMMS will not generate the chance constraint. If the probability is 1, then AIMMS will generate an uncertainty constraint. 

\paragraph{The {\tt Approximation} attribute}
\declattrindex{constraint}{Approximation}
\label{attr:robust.chance.approximation}
\presetindex{AllChanceApproximationTypes}

The {\tt Approximation} attribute is used to define the approximation that should be used to approximate the chance constraint. 
Its value should be an element expression into the predefined set {\tt AllChanceApproximationTypes}. 

\paragraph{Supported approximation types}

The approximations 
supported by AIMMS are:
\begin{itemize}
\item {\em Ball},
\item {\em Box},
\item {\em Ball-box},
\item {\em Budgeted}, and
\item {\em Automatic}.
\end{itemize}
A detailed mathematical definition of these approximation types can be found in Chapter~2 of \cite{bib:BGN09}.
Whether or not a particular approximation type is possible, depends on the characteristics of the distributions used in the chance 
constraint, as explained below. By specifying approximation type {\em Automatic} the most accurate approximation possible will be 
used. In some cases it might be beneficial to use a less tight approximation because it leads to a robust counterpart that is 
easier to solve.

\paragraph{Example}

Consider the declaration
\begin{example}   
Constraint ChanceConstraint {
    IndexDomain   : i;
    Property      : Chance;
    Definition    : Demand(i) * X(i) <= 10;
    Probability   : prob(i);
    Approximation : 'Ball';
}
\end{example}
This declaration states that {\tt ChanceConstraint} is a chance constraint with probability {\tt prob(i)}, and that approximation type {\em Ball} is used to approximate the chance constraint. 

\paragraph{Possible approximations per distribution}

Table~\ref{table:robust.chance-approximations} shows for each (supported) distribution which approximation types are possible. It also shows whether the approximation will result in a linear or a second-order cone robust counterpart.
\begin{aimmstable}
\tabcolsep0.5\tabcolsep
\begin{tabular}{|l|l|l|l|l|l|}
\hline\hline
{\bf Distribution} & {\bf Automatic} & {\bf Ball} & {\bf Box} & {\bf Ball-box} & {\bf Budgeted}\\
\hline
${\tt Bounded}(m,s)$              & linear & conic & linear       & conic & linear \\
${\tt Bounded}(m,l,u)$           & conic  &       & linear       &       &             \\
${\tt Unimodal}(c,s)$             & conic  &       & linear       &       &             \\
${\tt Symmetric}(c,s)$ (unimodal) & conic & conic & linear       & conic & linear      \\
${\tt Support}(l,u)$              & linear &       & linear       &       &             \\
${\tt Gaussian}(m_l,m_u,v)$       & conic  &       &              &       &             \\
\hline\hline
\end{tabular}
\caption{Allowed approximations and their resulting problem type}\label{table:robust.chance-approximations}
\end{aimmstable}
For the ${\tt Bounded}(m,s)$ distribution the automatic approximation equals the {\em Budgeted} approximation,
and the automatic approximation of the ${\tt Support}(l,u)$ distribution equals the {\em Box} approximation.
The non-unimodal ${\tt Symmetric}(c,s)$ distribution is treated as a ${\tt Bounded}(m,s)$ distribution.

\paragraph{Combining distributions}

A chance constraint cannot contain both bounded random parameters and Gaussian random parameters.
Different types of bounded random parameters can be combined, in which case only a part of the available
information will be used. The possible combinations of bounded random parameters are given in Table~\ref{table:robust.distr-mix}.
\begin{aimmstable}
\begin{tabular}{|c|l|c|c|c|c|c|}
\hline\hline
& {\bf Distribution} &  1 &  2 & 3 & 4 & 5\\
\hline
1 &  ${\tt Bounded}(m,s)$               &  1 &  2 & -- &  1 &  5\\
2 &  ${\tt Bounded}(m,l,u)$            &  2 &  2 & -- &  2 &  5\\
3 &  ${\tt Unimodal}(c,s)$              & -- & -- &  3 &  3 &  5\\
4 &  ${\tt Symmetric}(c,s)$ (unimodal) &  1 &  2 &  3 &  4 &  5\\
5 &  ${\tt Support}(l,u)$               &  5 &  5 &  5 &  5 &  5\\
\hline\hline
\end{tabular}
\caption{Resulting distribution type when combining distributions}\label{table:robust.distr-mix}
\end{aimmstable}

\paragraph{Explanation}

If a random parameter with a ${\tt Bounded}(m,l,u)$ distribution and a
random parameter with a ${\tt Support}(l,u)$ distribution are used in a single chance constraint, 
then Table~\ref{table:robust.distr-mix} states that the ${\tt Bounded}(m,l,u))$ distribution of the first random parameter 
will be treated as a 
${\tt Support}(l,u)$ distribution. Unimodal distributions can only be mixed with unimodal ${\tt Symmetric}(c,s)$ and 
${\tt Support}(l,u)$ distributions.


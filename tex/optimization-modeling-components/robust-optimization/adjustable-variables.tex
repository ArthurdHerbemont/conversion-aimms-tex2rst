\section{Adjustable variables}\label{sec:robust.adjustable}
\index{adjustable variable}\index{variable!adjustable}

\paragraph{Adjustable variables}

An {\em adjustable variable} reflects a decision made after uncertain data has been revealed. In robust optimization this is 
interpreted as the adjustable variable taking some (explicit or implicit) functional form in terms of the uncertain data on 
which it depends. In AIMMS, you indicate that a {\tt Variable} should be treated as adjustable by setting its {\tt 
Adjustable} property.

\paragraph{The {\tt Dependency} attribute}\label{attr:robust.dependency}
\declattrindex{variable}{Dependency}

For any adjustable variable, AIMMS will create a {\tt Dependency} attribute which you can use to specify on which uncertain 
parameters the variable depends. The attribute value must be a comma-separated list of mappings from an uncertain parameter to a binary 
parameter, indicating for which combination of indices a dependency exists on that uncertain parameter.

\paragraph{Linear decision rule only}

AIMMS currently only supports the {\em linear decision rule}, which means any adjustable variable will be expressed
as an affine relation in terms of the uncertain parameters which it depends on. More explicitly, if an adjustable 
variable $x(t)$ depends on uncertain parameters $d_r$, then, under the linear decision rule, AIMMS assumes that $x(t)$ takes 
the form 
\[
	x(t) = X_0(t) + \sum_r X_r(t) d_r
\]
where $X_0(t)$ and $X_r(t)$ are newly introduced intermediate variables, the value of which is determined by solving the robust 
counterpart.  As such, the value of an adjustable variable is not fully determined by the solver. It can be computed 
afterwards for a given realization of the uncertain parameters. AIMMS will automatically generate the affine relation based 
on the dependencies you indicated in the {\tt 
Dependency} attribute, without the need for you to introduce the appropriate intermediate variables. 

\paragraph{Requirements for adjustable variables}
\index{fixed recourse}\index{robust optimization!fixed recourse}

In order for AIMMS to be able to generate the robust counterpart of a robust optimization model, the model must satisfy the 
{\em fixed recourse} condition, i.e., the coefficients of any adjustable variables in your model must not depend on uncertain 
parameters. In addition, for AIMMS to be able to generate the robust counterpart, adjustable variables may {\em not} occur in 
chance constraints. Also, adjustable variables cannot be integer.

\paragraph{The {\tt .Adjustable} suffix for variables}
\suffindex{variable}{Adjustable}

The collection of intermediate variables introduced during this process, automatically becomes available through the {\tt 
.Adjustable} attribute of the adjustable variable at hand, followed by the name of the uncertain parameter involved. That is, if 
an adjustable variable {\tt x(i)} depends on an uncertain parameter {\tt a(j)}, then the corresponding intermediate variable is 
available as the expression {\tt x.Adjustable.a(i,j)}. In addition, a variable {\tt x.Adjustable.Constant(i)} will be created to 
account for the 
constant part of the affine relation. If necessary, you can bound these variables through the~{\tt .Lower} and 
{\tt .Upper} suffices, or you can formulate additional constraints on these variables.  

\paragraph{Example}

Consider the following declarations
\begin{example}
Variable Stock {
    IndexDomain  :  t;
    Property     :  Adjustable;
    Dependency   :  Demand(t2) : StockDemandDependency(t,t2);
}
Parameter Demand {
    IndexDomain  :  t;
    Property     :  Uncertain;
}
Parameter StockDemandDependency {
    IndexDomain  :  (t,t2);
    Definition   :  1 $ (t2 < t);
}
\end{example}
These declarations yield that the adjustable variable {\tt Stock(t)} depends on the uncertain parameter {\tt Demand(t2)}
for all elements {\tt t2} smaller than {\tt t}. Given these declarations, AIMMS will generate the following definition
for {\tt Stock(t)}
\begin{example}
    Stock(t) = Stock.Adjustable.Constant(t) + 
	           sum(t2 | StockDemandDependency(t,t2), Stock.Adjustable.Demand(t,t2)*Demand(t2))
\end{example}
If the data for {\tt Demand(t)} becomes available, you can use the computed values of {\tt Stock.Adjustable.Demand(t,t2)} 
and {\tt Stock.Adjustable.Constant} to compute the value of {\tt Stock(t)}.

\paragraph{Warning: using same indices}

You should be aware that using the same indices in the {\tt Dependency} attribute and the index domain of the adjustable
variable will restrict the dependencies that are generated. For example, assume we have the following declarations
\begin{example}
Variable Stock {
    IndexDomain  :  t;
    Property     :  Adjustable;
    Dependency   :  Demand(t);
}
Parameter Demand {
    IndexDomain  :  t;
    Property     :  Uncertain;
}
\end{example}
Given these declarations, AIMMS will generate the following definition
for {\tt Stock(t)}
\begin{example}
    Stock(t) = Stock.Adjustable.Constant(t) + Stock.Adjustable.Demand(t)*Demand(t)
\end{example}
If you want {\tt Stock(t)} to depend on all possible {\tt Demand} then you should use a different index in the {\tt Dependency}
attribute, e.g.,
\begin{example}
Variable Stock {
    IndexDomain  :  t;
    Property     :  Adjustable;
    Dependency   :  Demand(t2);
}
\end{example}

\paragraph{Evaluating adjustable variables}

To compute the values of an adjustable variable for a given realization of the uncertain parameters 
of the robust optimization model, you do not have to explicitly add the appropriate definitions to your model. 
AIMMS offers the function {\tt GMP::Robust::Evaluate\-Adjustable\-Variables}, discussed in Section~\ref{sec:gmp.robust}, 
to automatically compute these values for you. 


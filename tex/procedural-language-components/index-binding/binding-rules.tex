\section{Binding rules}\label{sec:bind.rules}

\paragraph{Repetitive operations}

During execution, indices are used to traverse a set to repeatedly apply a
specific operation on all elements of a set. These operations concern
\begin{itemize}
\item indexed assignment statements,\index{assignment!index binding}
\item {\tt FOR} statements,\subtypindex{statement}{FOR}{index binding}
\item iterative operations like summation over a
domain,\index{iterative operator!index binding}
\item constraint generation,\index{constraint!index binding}
\item arc generation, and\index{arc!index binding}
\item constructed set expression.\index{constructed set!index binding}
\end{itemize}

\paragraph{Index binding}

{\em Index binding} is the process by which AIMMS repeatedly couples the
value of an index to elements of a specific set to execute repetitive
operations.

\paragraph{Different types of binding}

There are three ways in which index binding takes place:
\begin{itemize}
\item {\em local} binding,
\item {\em default} binding, and
\item {\em context} binding.
\end{itemize}

\paragraph{Local binding}\index{index binding!local}

{\em Local binding} takes place through the use of an {\tt IN} modifier at the
index binding position as illustrated in the following example.
\begin{example}
    NettoTransport(i in SupplyCities, j in DestinationCitiesFromSupply(i)) :=
        Transport(i,j) - Transport(j,i);
\end{example}
Instead of executing the assignment for all cities {\tt i} and {\tt j}, it is
only executed for those combinations for which city {\tt i} is in {\tt
SupplyCities} and city {\tt j} is in {\tt DestinationCitiesFromSupply(i)}.

\paragraph{Default binding}\index{index binding!default}

Indices can have a {\em default binding}. This is the binding specified in a
declaration. You can specify a default binding either via the {\tt Index}
attribute of a set, or via the {\tt Range} attribute of an {\tt Index}
declaration. Whenever you use an index with a default binding and do not
specify a local binding, AIMMS will couple this index to its default set
automatically. The following example illustrates default binding.
\begin{example}
    IntermediateTransportCitiesInBetween(i,j) :=
           DestinationCitiesFromSupply(i) * SupplyCitiesToDestination(j);
\end{example}
Assuming that {\tt i} and {\tt j} have a default binding to the set {\tt
Cities}, the assignment takes place for all tuples of cities ({\tt i},{\tt j}).

\paragraph{Context binding}\index{index binding!context}

Whenever you use an index that has no default binding and for which you do not
provide a local binding, AIMMS will try to determine a {\em context binding}
from the context. Assume that {\tt k} is an index without a default binding.
Further assume that {\tt LargestTransport} is an element parameter into {\tt
Cities} and indexed over {\tt Cities}. Then the following example is an
illustration of context binding.
\begin{example}
    LargestTransport(k) := ArgMax( j, Transport(k,j) );
\end{example}
In this assignment AIMMS will automatically bind the index {\tt k} to {\tt
Cities}, because the identifier {\tt LargestTransport} has been declared with
the index domain {\tt Cities}. Note that context binding will only work in
indexed assignments.

\paragraph{Nested index binding}\index{index binding!nested}

Index binding can be nested through the use of indexed element-valued
parameters on the left-hand side of an assignment. The binding takes place in
the way that you would expect, applying the same rules as for non-nested index
binding. For example, given the declarations
\begin{example}
ElementParameter NextCity {
   IndexDomain  : i;
   Range        : Cities;
}
ElementParameter PreviousCity {
   IndexDomain  : i;
   Range        : Cities;
}
\end{example}
the following assignment, which computes the value of {\tt PreviousCity} given
the contents of {\tt NextCity}, will bind the nested reference to the index
{\tt i}.
\begin{example}
   PreviousCity( NextCity(i) ) := i;
\end{example}
This binding is sparse, in the sense that the statement is only executed for
those {\tt i} for which {\tt NextCity(i)} assumes a nonempty value.

\paragraph{Compatible index binding only}

In general, AIMMS will never accept the use of an index in references to
indexed identifiers when the binding set does not have the same root set as the
index domain of the identifier. This is even the case when the elements,
referenced in the particular statement, have identical names in both the
binding set and the index domain. Internally, AIMMS stores a set elements as
a unique (integer) number with respect to its root set, and uses this number
for storing data for that element in indexed identifiers. Thus, when the root
sets of the binding set and the index domain are not identical, the set element
numbers will be incompatible, preventing AIMMS from referencing the correct
data.

\paragraph{Use indirect referencing}
\efuncindex{ElementCast}

When you want to use a binding set which is incompatible with the index domain
of identifier on the left-hand side of an assignment, you should manually
create an element parameter which maps elements in one root to the
corresponding elements the other root set. Such a mapping can be easily created
using the function {\tt ElementCast} (discussed in
Section~\ref{sec:set-expr.elem.functions}), as exemplified below.
\begin{example}
    ElementMap(i) := ElementCast( IncompatibleRootSet, i );
\end{example}
Subsequently, you can use a nested binding through the element parameter {\tt
ElementMap} to reference elements in the index domain of the identifier on the
left-hand side of an assignment, while still using the index {\tt i} as a
binding index, as illustrated in the following statement.
\begin{example}
    IncompatibleParameter( ElementMap(i) ) := CompatibleParameter(i);
\end{example}

\paragraph{Use the {\tt ElementCast} function}

Conversely, when you want to use an incompatible set element in a parameter
reference on the right-hand side of an assignment, there is no direct need to
create a mapping parameter. In an expression on the right of an assignment, you
can use the function {\tt ElementCast} directly at any index position, as
illustrated below.
\begin{example}
    CompatibleParameter(i) := IncompatibleParameter( ElementCast(IncompatibleRootSet, i) );
\end{example}

\paragraph{Universal set}\index{set!universal}

Note that you could have accomplished the same effect by creating a universal
set of which all other sets are subsets. As a result, all set elements are
represented as unique integer numbers with respect to the same root set,
allowing the index domains of all identifiers to be referenced in a compatible
manner. However, often it is not very natural to do so, and the usage of a
universal set is likely to slow down the performance of AIMMS.

\paragraph{Index binding rules}\index{index binding!rules}

For most situations the result of index binding is self-evident and the
behavior of the system is as you would expect. Following are the precise rules
for index binding.
\begin{description}
\item[Dominance rule] Whenever
index binding takes place, local binding precedes default binding, which in
turn precedes context binding. If no method is applicable, a compile time error
will result. \index{index binding!dominance rule} \index{dominance rule}
\item[Intersection rule] In indexed assignments the binding set(s)
should be compatible with the index domain. The assignment will be performed
for all tuples on the left-hand side that lie in the intersection of the
binding set(s) and the index domain of the corresponding identifier.
\index{index binding!intersection rule} \index{intersection rule}
\item[Ordering rule] Lag and lead operators, as well as the {\tt Ord}
and {\tt Element} functions operate according to the order of elements in the
corresponding binding set. \index{index binding!ordering rule} \index{ordering
rule}
\end{description}

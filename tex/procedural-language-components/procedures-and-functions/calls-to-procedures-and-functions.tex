\section{Calls to procedures and functions}\label{sec:intern.ref}

\paragraph{Consistency with prototype}
\index{call to procedure or function} \index{procedure!call}
\index{function!call}

Functions and procedures must be called from within AIMMS in accordance with
the prototype as specified in their declaration. For every call to a function
or procedure, AIMMS will verify not only the number of arguments, but also
whether the arguments and result are consistent with the specified domains and
ranges.

\paragraph{Example procedure call}

Consider the procedure {\tt ComputeShortestDistance} defined in
Section~\ref{sec:intern.proc}. Further assume that {\tt DistanceMatrix} and
{\tt ShortestDistanceMatrix} are two-dimen\-sional identifiers defined over
{\tt Cities}${}\times{}${\tt Cities}. Then the following assignment illustrates
a valid procedure call.
\begin{example}
    for ( i ) do
        ComputeShortestDistance(i, DistanceMatrix, ShortestDistanceMatrix(i,.)) ;
    endfor;
\end{example}
As you will see later on, the ``{\tt .}'' notation used in the third argument
is a shorthand for the corresponding domain set. In this instance, the
corresponding domain set of {\tt ShortestDistanceMatrix(i,.)} is the set {\tt
Cities}.

\paragraph{Domain checking of arguments}
\index{domain checking} \index{argument!domain checking}

In analyzing the resulting domains of the arguments, AIMMS takes into
account the following considerations.
\begin{itemize}
\item Due to the surrounding {\tt FOR} statement the index {\tt i} is
bound, so that the first argument is indeed an element in the set {\tt Cities}.
\item The second argument {\tt DistanceMatrix} is provided without
an explicit domain. AIMMS will interpret this as offering the complete
two-dimensional identifier {\tt DistanceMatrix}. As expected, the argument is
defined over {\tt Cities}${}\times{}${\tt Cities}.
\item Because of the binding of index {\tt i}, the third argument {\tt
ShortestDistance\-Matrix(i,.)} results into the (expected) one-dimensional
slice over the set {\tt Cities} in which the result of the computation will be
stored.
\end{itemize}
Thus, the domains of the  actual arguments coincide with the domains of the
formal arguments, and AIMMS can correctly compute the result.

\paragraph{Example function call}

Now consider the function {\tt ShortestDistance} defined in
Section~\ref{sec:intern.proc}. The following statement is equivalent to the
{\tt FOR} statement of the previous example.
\begin{example}
    ShortestDistanceMatrix(i,j) := ShortestDistance(i, DistanceMatrix)(j) ;
\end{example}
In this example index binding takes place through the indexed assignment. Per
city {\tt i} AIMMS will call the function {\tt ShortestDistance} once, and
assign the one-dimensional result (indexed by {\tt j}) to the one-dimensional
slice {\tt Shortest\-DistanceMatrix(i,j)}.

\paragraph{Call syntax}

The general forms of procedure and function calls are identical, except that a
function reference can have additional indexing.

\paragraph{}
\begin{syntax}
\syntaxdiagram{procedure-call}{subr-ref}
\syntaxdiagram{function-call}{func-ref}
\end{syntax}

\paragraph{Actual arguments}
\index{actual argument} \index{argument!actual}

Each actual argument can be
\begin{itemize}
\item any type of scalar expression for {\em scalar} arguments, and
\item a reference to an identifier slice of the proper dimensions for
{\em non-scalar} arguments.
\end{itemize}
Actual arguments can be tagged with their formal argument name used inside the
declaration of the function or procedure. The syntax follows.

\paragraph{}
\begin{syntax}
\hbox{\hbox to .6\textwidth{\vtop{%
\syntaxdiagram{tagged-argument}{tagged-arg}}\hss}%
\hbox to .4\textwidth{\vtop{%
\syntaxdiagram{actual-argument}{actual-arg}}\hss}}
\syntaxdiagram{identifier-slice}{ref-arg}
\end{syntax}

\paragraph{Scalar and set arguments}

For scalar and set arguments that are of type {\tt Input} you can enter any
scalar or set expression, respectively. Scalar and set arguments that are of
type {\tt InOut} or {\tt Output} must contain a reference to a scalar parameter
or set, or to a scalar slice of an indexed parameter or set. The latter is
necessary so that AIMMS knows where to store the output value.

\paragraph{No slices of indexed sets}

Note that AIMMS does not allow you to pass slices of an indexed set as a set
arguments to functions and procedures. If you want to pass the contents of a
slice of an indexed set as an argument to a procedure or function, you should
assign the contents to a simple (sub)set instead, and pass that set as an
argument.

\paragraph{Multi- dimensional arguments}
\index{argument!sliced} \index{sliced argument}

For multidimensional actual arguments AIMMS only allows references to
identifiers or slices thereof. Such arguments can be indicated in two manners.
\begin{itemize}
\item If you just enter the name of a multidimensional identifier,
AIMMS assumes that you want to pass the fully dimensioned data block
associated with the identifier.
\item If you enter an identifier name plus
\begin{itemize}
\item a ``{\tt .}'',
\item a set element, or
\item a set expression
\end{itemize}
at each position in the index domain of the identifier, AIMMS will pass the
corresponding identifier {\em slice} or {\em subdomain}.
\end{itemize}

\paragraph{The ``{\tt .}'' notation}
\operindex{.}

When passing slices or subdomains of a multidimensional identifier argument,
you can use the ``{\tt .}'' shorthand notation at a particular position in the
index domain. With it you indicate that AIMMS should use the corresponding
domain set of the identifier at hand at that index position. Recall the
argument {\tt ShortestDistanceMatrix(i,.)} in the call to the procedure {\tt
Compute\-Shortest\-Distance} discussed at the beginning of this section. As the
index domain of {\tt ShortestDistanceMatrix} is the set {\tt Cities} $\times$
{\tt Cities}, the ``{\tt .}'' reference stands for a reference to the set {\tt
Cities}.

\paragraph{Slicing}

By specifying an explicit set element or an element expression at a certain
index position of an actual argument, you will decrease the dimension of the
resulting slice by one. The call to the procedure {\tt ComputeShortestDistance}
discussed earlier in this section illustrates an example of an actual argument
containing a one-dimensional slice of a two-dimensional parameter.

\paragraph{Dimensions must match}

Note that AIMMS requires that the dimensions of the formal and actual
arguments match exactly. 

\paragraph{Subdomains}
\index{argument!over subdomain}

By specifying a subset expression at a particular index position of an indexed
argument, you indicate to AIMMS that the procedure or function should only
consider the argument as defined over this subdomain.

\paragraph{Example}

Consider the Cobb-Douglas function discussed in the previous section, and
assume the existence of a parameter {\tt a(f)} and a parameter {\tt c(f)}, both
defined over a set {\tt Factors}. Then the statement
\begin{example}
     Result := CobbDouglas(a,c) ;
\end{example}
will compute the result by taking the product of exponents over all factors
{\tt f}. If {\tt SubFactors} is a subset of {\tt Factors}, satisfying the
condition on the share parameter {\tt a(f)}, then the following call will
compute the result by only taking the product over factors {\tt f} in the
subset {\tt SubFactors}.
\begin{example}
    Result := CobbDouglas( a(SubFactors), c(SubFactors) );
\end{example}

\paragraph{Global subdomains}

Whenever a formal argument refers to an indexed identifier defined over global
sets, it could be that an actual argument in a function or procedure call
refers to an identifier defined over a superset of one or more of these global
sets. In this case, AIMMS will automatically restrict the domain of the
actual argument to the domain of the formal argument. Likewise, if an index set
of an actual argument is a real subset of the corresponding global index set of
a formal argument, the values of the formal argument, when referred to from
within the body of the procedure, will assume the default value of the formal
argument in the complement of the index (sub)set of actual argument.

\paragraph{Local subdomains}

Whenever a formal argument refers to an indexed identifier defined over local
sets, the domain of the actual argument can be further restricted to a
subdomain as in the example above. In any case, the (sub)domain of the actual
argument determines the contents of the local set(s) used in the formal
arguments. Note that consistency in the specified domains of the actual
arguments is required when a local set is used in the index domain of several
formal arguments.

\paragraph{Tagging arguments}
\index{argument!tag} \index{tag!argument} \index{procedure!tagged argument}
\index{function!tagged argument}

\syntaxmark{arg-tag} In order to improve the understandability of calls to
procedures and functions the actual arguments in a reference may be tagged with
the formal argument names used in the declaration. In a procedure reference, it
is mandatory to tag all {\em optional} arguments which do not occur in their
natural order.

\paragraph{Permuting tagged arguments}

Tagged arguments may be inserted at any position in the argument list, because
AIMMS can determine their actual position based on the tag. The non-tagged
arguments must keep their relative position, and will be intertwined with the
(permuted) tagged arguments to form the complete argument list.

\paragraph{Example}

The following permuted call to the procedure {\tt ComputeShortestDistance}
illustrates the use of tags.
\begin{example}
    for ( i ) do
        ComputeShortestDistance( Distance       : ShortestDistanceMatrix(i,.),
                                 DistanceMatrix : DistanceMatrix,
                                 City           : i );
    endfor;
\end{example}

\paragraph{Using the return value}\herelabel{proc.ret-val.use}
\index{procedure!return value} \index{return value}

As indicated in Section~\ref{sec:intern.proc} procedures in AIMMS can return
with an integer return value. Its use is limited to two situations.
\begin{itemize}
\item You can assign the return value of a procedure to a scalar
parameter in the calling procedure. However, a procedure call can never be part
of a numerical expression.
\item You can use the return value in a logical condition in, for
instance, an {\tt IF} statement to terminate the execution when a procedure
returns with an error condition.
\end{itemize}
You can use a procedure just as a single statement and ignore the return value,
or use the return value as described above. In the latter case, AIMMS will
first execute the procedure, and subsequently use the return value as
indicated.

\paragraph{Example}

Assume the existence of a procedure {\tt AskForUserInputs(Inputs,Outputs)}
which presents a dialog box to the user, passes the results to the {\tt
Outputs} argument, and returns with a nonzero value when the user has pressed
the OK button in the dialog box. Then the following {\tt IF} statement
illustrates a valid use of the return value.
\begin{example}
    if ( AskForUserInputs( Inputs, Outputs ) )
    then
       ... /* Take appropriate action to process user inputs */
    else
       ... /* Take actions to process invalid user input */
    endif ;
\end{example}

\subsection{The \tttext{APPLY} operator}\label{sec:intern.ref.apply}
\operindex{APPLY} \index{procedure!as data} \index{function!as data}
\AIMMSlink{apply}

\tipparagraph{Data-driven procedures}{apply}

In many real-life applications the exact nature of a specific type of
computation may heavily depend on particular characteristics of its input data.
To accommodate such data-driven computations, AIMMS offers the {\tt APPLY}
operator which can be used to dynamically select a procedure or function of a
given prototype to perform a particular computation. The following two examples
give you some feeling of the possible uses.

\paragraph{Example: processing events}

In event-based applications many different types of events may exist, each of
which may require an event-type specific sequence of actions to process it. For
instance, a ship arrival event should be treated differently from an event
representing a pipeline batch, or an event representing a batch feeding a crude
distiller unit. Ideally, such event-specific actions should be modeled as a
separate procedure for each event type.

\paragraph{Example: product blending}

A common action in the oil-processing industry is the blending of crudes and
intermediate products. During this process certain material properties are
monitored, and their computation for a blend require a property-specific {\em
blending rule}. For instance, the sulphur content of a mixture may blend
linearly in weight, while for density the reciprocal density values blend
linear in weight. Ideally, each blending rule should be implemented as a
separate procedure or function.

\paragraph{The {\tt APPLY} operator}

With the {\tt APPLY} operator you can dynamically select a procedure or
function to be called. The first argument of the {\tt APPLY} operator must be
the name of the procedure or function that you want to call. If the called
procedure or function has arguments itself, these must be added as the second
and further arguments to the {\tt APPLY} operator. In case of an indexed-valued
function, you can add indexing to the {\tt APPLY} operator as if it were a
function call.

\paragraph{Requirements}
\presetindex{AllIdentifiers}

In order to allow AIMMS to perform the necessary dynamic type checking for
the {\tt APPLY} operator, certain requirements must be met:
\begin{itemize}
\item the first argument of the {\tt APPLY} operator must be a
reference to a string parameter or to an element parameter into the set {\tt
AllIdentifiers},
\item this element parameter must have a {\tt Default}
value, which is the name of an existing procedure or function in your model,
and
\item all other values that this string or element parameter assumes must be
existing procedures or functions with the same prototype as its {\tt Default}
value.
\end{itemize}

\paragraph{Example: processing events elaborated}

Consider a set of {\tt Events} with an index {\tt e} and an element parameter
named {\tt CurrentEvent}. Assume that each event {\tt e} has been assigned an
event type from a set {\tt EventTypes}, and that an event handler is defined
for each event type. It is further assumed that the event handler of a
particular event type takes the appropriate actions for that type. The
following declarations illustrates this set up.
\begin{example}
ElementParameter EventType {
    IndexDomain    : e;
    Range          : EventTypes;
}
ElementParameter EventHandler {
    IndexDomain    : et in EventTypes;
    Range          : AllIdentifiers;
    Default        : NoEventHandlerSelected;
    InitialData    : {
          DATA { ShipArrivalEvent    : DischargeShip,
                 PipelineEvent       : PumpoverPipelineBatch,
                 CrudeDistillerEvent : CrudeDistillerBatch }
    }
}
\end{example}
The {\tt Default} value of the parameter {\tt EventHandler(et)}, as well as all
of the values assigned in the {\tt InitialData} attribute, must be valid
procedure names in the model, each having the same prototype. In this example,
it is assumed that the procedures {\tt NoEventHandlerSelected}, {\tt
DischargeShip}, {\tt PumpoverPipelineBatch}, and {\tt CrudeDistillerBatch} all
have two arguments, the first being an element of a set {\tt Events}, and the
second being the time at which the event has to commence. Then the following
call to the {\tt APPLY} statement implements the call to an event type specific
event handler for a particular event {\tt CurrentEvent} at time {\tt
NewEventTime}.
\begin{example}
    Apply( EventHandler(EventType(CurrentEvent)), CurrentEvent, NewEventTime );
\end{example}
When no event handler for a particular event type has been provided, the
default procedure {\tt NoEventHandlerSelected} is run which can abort with an
appropriate error message.

\paragraph{Use in constraints}
\subtypindex{operator}{APPLY}{use in constraint}

When applied to functions, you can also use the {\tt APPLY} operator inside
constraints. This allows you, for instance, to provide a generic constraint
where the individual terms depend on the value of set elements in the domain of
the constraint. Note, that such use of the {\tt APPLY} operator will only work
in conjunction with external functions, which allow the use of variable
arguments (see Section~\ref{sec:extern.constraints}).

\paragraph{Example: product blending}

Consider a set of {\tt Products} with index {\tt p}, and a set of monitored
{\tt Properties} with index {\tt q}. With each property {\tt q} a blend rule
function can be associated such that the resulting values blend linear in
weight. These property-dependent functions can be expressed by the element
parameter {\tt BlendRule(q)} given by
\begin{example}
ElementParameter BlendRule {
    IndexDomain    : q;
    Range          : AllIdentifiers;
    Default        : BlendLinear;
    InitialData    : {
          DATA { Sulphur    : BlendLinear,
                 Density    : BlendReciprocal,
                 Viscosity  : BlendViscosity   }
    }
}
\end{example}
Thus, the computation of the property values of a product blend can be
expressed by the following single constraint, which takes into account the
differing blend rules for all properties.
\begin{example}
Constraint ComputeBlendProperty {
    IndexDomain    : q;
    Definition     : {
        Sum[p, ProductAmount(p)  * Apply(BlendRule(q), ProductProperty(p,q))] =
        Sum[p, ProductAmount(p)] * Apply(BlendRule(q), BlendProperty(q))
    }
}
\end{example}
Depending on the precise computation in the blend rules functions for every
property {\tt q}, the {\tt APPLY} operator may result in linear or nonlinear
terms being added to the constraint.

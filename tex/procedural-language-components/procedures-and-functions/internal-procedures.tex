\section{Internal procedures}\label{sec:intern.proc}
\index{procedure!internal} \index{internal procedure}

\paragraph{AIMMS and internal procedures}

Internal procedures are pieces of execution code to perform a dedicated task.
For most tasks, and particularly large ones, it is strongly recommended that
you use procedures to break your task into smaller, purpose-specific tasks.
This provides code structure which is easier to maintain and run. Often it is
appropriate to write procedures to obtain input data from users, databases and
files, to execute data consistency checks, to perform side computations, to
solve a mathematical program, and to create selected reports. Procedures can be
called both inside the model text and inside the graphical user interface.

\paragraph{Declaration and attributes}
\declindex{Procedure} \AIMMSlink{procedure}

\syntaxmark{procedure} Procedures are added by inserting a special type of node
in the model tree. The attributes of a {\tt Procedure} specify its arguments
and execution code. All possible attributes of a {\tt Procedure} node are given
in Table~\ref{table:intern.attr-proc}.
\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type} & {\bf See also} \\
                &                  & {\bf page} \\
\hline
{\tt Arguments} & {\em argument-list} & \\
{\tt Property}  & {\tt UndoSafe} & \\
{\tt Body}      & {\em statements} & \pageref{chap:exec} \\
{\tt Comment}   & {\em comment string} & \\
\hline\hline
\end{tabular}
\caption{{\tt Procedure} attributes}\label{table:intern.attr-proc}
\end{aimmstable}

\paragraph{Formal arguments}
\declattrindex{procedure}{Arguments} \index{formal argument}
\index{argument!formal} \AIMMSlink{procedure.arguments}

The arguments of a procedure are given as a parenthesized, comma-separated list
of formal argument names. These argument names are only the formal identifier
names without reference to their index domains. AIMMS allows formal
arguments of the following types:
\begin{itemize}
\item simple sets and relations, and
\item scalar and indexed parameters (either element-valued,
string-valued or numerical).
\end{itemize}
The type and dimension of every formal argument is not part of the argument
list, and must be specified as part of the argument's (mandatory) local
declaration in a declaration subnode of the procedure.

\paragraph{Interactive support}

When you add new formal arguments to a procedure in the AIMMS Model
Explorer, AIMMS provides support to automatically add these arguments as
local identifiers to the procedure. For all formal arguments which have not yet
been declared as local identifiers, AIMMS will pop up a dialog box to let
you choose from all supported identifier types. After finishing the dialog box,
all new arguments will be added as (scalar) local identifiers of the indicated
type. When an argument is indexed, you still need to add the proper {\tt IndexDomain} 
manually in the attribute form of the argument declaration.

\paragraph{Range checking}
\index{argument!range checking}

If the declaration of a formal argument of a procedure contains a numerical
range, AIMMS will automatically perform a range check on the actual
arguments based on the specified range of the formal argument.

\paragraph{Input or output}
\propindex{Input} \propindex{Output} \propindex{InOut} \propindex{Optional}
\AIMMSlink{input} \AIMMSlink{output} \AIMMSlink{inout} \AIMMSlink{optional}

In the declaration of each argument you can specify its type by setting one of
the properties
\begin{itemize}
\item {\tt Input},
\item {\tt Output},
\item {\tt InOut} (default), or
\item {\tt Optional}.
\end{itemize}
AIMMS passes the values of any {\tt Input} and {\tt InOut} arguments when
entering the procedure, and passes back the values of {\tt Output} and {\tt
InOut} arguments. For this reason an actual {\tt Input} argument can be any
expression, but actual {\tt Output} and {\tt InOut} arguments must be parameter
references or set references.

\paragraph{Optional arguments}

An argument can be made optional by setting the property {\tt Optional} in its
declaration. Optional arguments are always input, and must be scalar. When an
optional argument is not provided in a procedure call, AIMMS will pass its
default value as specified in its declaration.

\paragraph{The {\tt Body} attribute}
\declattrindex{procedure}{Body} \AIMMSlink{procedure.body}

In the {\tt Body} attribute you can specify the sequence of AIMMS execution
statements that you want to be executed when the procedure is run. All
statements in the body of a procedure are executed in their order of
appearance.

\paragraph{Example}

The following example illustrates the declaration of a simple procedure in
AIMMS. The body of the procedure has only been outlined.
\begin{example}
Procedure ComputeShortestDistance {
    Arguments  : (City, DistanceMatrix, Distance);
    Comment    : {
        "This procedure computes the distance along the shortest path
        from City to any other city j, given DistanceMatrix."
    }
    Body: {
        Distance(j) := DistanceMatrix(City,j);

        for ( j | not Distance(j) ) do
            /*
             *  Compute the shortest path and the corresponding distance
             *  for cities j without a direct connection to City.
             */
        endfor
    }
}
\end{example}
The procedure {\tt ComputeShortestDistance} has three formal arguments, which
must be declared in a declaration subnode of the procedure. Their declarations
within this subnode could be as follows.
\begin{example}
ElementParameter City {
    Range        : Cities;
    Property     : Input;
}
Parameter DistanceMatrix {
    IndexDomain  : (i,j);
    Property     : Input;
}
Parameter Distance {
    IndexDomain  : j;
    Property     : Output;
}
\end{example}
From these declarations (and not from the argument list itself) AIMMS can
deduce that
\begin{itemize}
\item the first actual (input) argument in a call to {\tt
ComputeShortestDistance} must be an element of the (global) set {\tt Cities},
\item the second (input) argument must be a two-dimensional parameter
over {\tt Cities}${}\times{}${\tt Cities}, and
\item the third (output) arguments must be a one-dimensional parameter
over {\tt Cities}.
\end{itemize}

\paragraph{Arguments declared over global sets}

As in the example above, arguments of procedures can be indexed identifiers
declared over global sets. An advantage is that no local sets need to be
defined. A disadvantage is that the corresponding procedure is not generic.
Procedures with arguments declared over global sets are preferred when the
procedure is uniquely designed for the application at hand, and direct
references to global sets add to the overall understandability and
maintainability.

\paragraph{Arguments declared over local sets}
\index{procedure!arguments over local sets}
\index{local set}
\index{set!local}

The index domain or range of a procedure argument need not always be defined in
terms of global sets. Also sets that are declared locally within the procedure
can be used as index domain or range of that procedure. When a procedure with
such arguments is called, AIMMS will examine the actual arguments, and pass
the global domain set to the local set identifier {\em by reference}. This
allows you to implement procedures performing generic functionality for which a
priori knowledge of the index domain or range of the arguments is not relevant.

\paragraph{Local sets are read-only}

When you pass arguments defined over local sets, AIMMS does not allow you to
modify the contents of these local sets during the execution of the procedure.
Because such local sets are passed by reference, this will prevent you from
inadvertently modifying the contents of the global domain sets. When you do
want to modify the contents of the global domain sets, you should pass these
sets as explicit arguments as well.

\paragraph{Unit analysis of arguments}
\index{procedure!argument!unit of measurement} \index{function!argument!unit of
measurement} \index{argument!unit of measurement} \index{unit!procedure
argument}

Whenever your model contains one or more {\tt Quantity} declarations (see
Section~\ref{sec:units.quantity}), AIMMS allows you to associate units of
measurements with every argument. Similarly as the index domains of
multidimensional arguments can be expressed either in terms of global sets, or
in terms of local sets that are determined at runtime, the units of
measurements of function and procedure arguments can also be expressed either
in terms of globally defined units, or in terms of local unit parameters that
are determined runtime by AIMMS. The unit analysis of procedure arguments is
discussed in full detail in Section~\ref{sec:units.analysis.arg}.

\paragraph{Local identifiers}
\index{procedure!local identifier}

Besides the arguments, you can also declare other local scalar or indexed
identifiers in a declaration subnode of a procedure or function in AIMMS.
Local identifiers cannot have a definition, and their scope is limited to the
procedure or function itself.

\paragraph{The property {\tt RetainsValue}}
\propindex{RetainsValue} \AIMMSlink{retainsvalue}

For each local identifier of a procedure or function that is not a formal
argument, you can specify the option {\tt RetainsValue}. With it you can
indicate that such a local identifier must retain its last assigned value
between successive calls to that procedure or function. You can use this
feature, for instance, to retain local data that must be initialized once and
can be used during every subsequent call to the procedure, or to keep track of
the number of calls to a procedure.

\paragraph{Execution subnodes}
\index{procedure!subnodes} \index{subnodes}

In addition to AIMMS execution statements, you can include references to
(named) execution subnodes to the body of a procedure. AIMMS supports
several types of execution subnodes. They can either contain just execution
statements or provide a graphical input form for complicated statements like
the {\tt READ}, {\tt WRITE} and {\tt SOLVE} statement. The contents of the
execution subnodes will be expanded by AIMMS into the body of the procedure
at the position of their references.

\paragraph{Top-down implementation}

By partitioning the body of a long procedure into several execution subnodes,
you can effectively implement the procedure in a self-documenting top-down
approach. While the body can just contain the outermost structure of the
procedure's execution, the implementation details can be hidden behind subnode
references with meaningful names.

\paragraph{The {\tt RETURN} statement}
\statindex{RETURN} \typindex{clause}{WHEN} \index{RETURN statement@{\tt RETURN}
statement!{\tt WHEN} clause} \AIMMSlink{return}

In some situations, you may want to return from a procedure or function before
the end of its execution has been reached. You use the {\tt RETURN} statement
for this purpose. It can be subject to a conditional {\tt WHEN} clause similar
to the {\tt SKIP} and {\tt BREAK} statements in loops. The syntax follows.

\paragraph{Syntax}
\begin{syntax}
\syntaxdiagram{return-statement}{return}
\end{syntax}

\paragraph{Return value}\herelabel{proc.ret-val.decl}
\index{procedure!return value} \index{return value}

\syntaxmark{return-value} Procedures in AIMMS can have an (integer) return
value, which you can pass by means of the {\tt RETURN} statement. You can use
the return value only in a limited sense: you can assign it to a scalar
parameter, or use it in a logical condition in, for instance, an {\tt IF}
statement. You cannot use the return value in a compound numerical expression.
For more details, refer to Section~\ref{sec:intern.ref}.

\paragraph{The {\tt Property} attribute}
\declattrindex{procedure}{Property} \declpropindex{procedure}{UndoSafe}
\AIMMSlink{procedure.property} \herelabel{attr:undosafe}

In the {\tt Property} attribute of internal procedures you can specify a single
property, {\tt UndoSafe}. With the {\tt UndoSafe} property you can indicate
that the procedure, when called from a page within the graphical end-user
interface of a model, should leave the stack of end-user undo actions intact.
Normally, procedure calls made from within the end-user interface will clear
the undo stack, because such calls usually make additional modifications to
(global) data based on end-user edits.

\paragraph{Procedures summarized}

The following list summarizes the main characteristics of AIMMS procedures.
\begin{itemize}
\item  The arguments of a procedure can be sets, set elements and
parameters.
\item  The arguments, together with their attributes, must be declared
in a local declaration subnode.
\item  The domain and range of indexed arguments can be in terms of
either global or local sets.
\item  Each argument is of type {\tt Input}, {\tt Output}, {\tt Optional}
or {\tt InOut} (default).
\item  Optional arguments must be scalar, and you must specify a default
value. Optional arguments are always of type {\tt Input}.
\item  AIMMS performs range checking on the actual arguments at runtime,
based on the specified range of the formal arguments.
\end{itemize}


\section{Internal functions}\label{sec:intern.func}
\index{function!internal} \index{internal function}

\paragraph{Similar to procedures}

\syntaxmark{function} The specification of a function is very similar to that
of a procedure. The following items provide a summary of their similarities.
\begin{itemize}
\item  Arguments, together with their attributes, must be declared
in a local declaration subnode.
\item  The domain and range of indexed arguments can be in terms of
either global or local sets.
\item  The units of arguments can be expressed in terms of globally defined units of measurement, or in locally defined unit parameters.
\item  Optional arguments must be scalar, and you must specify a default
value.
\item  AIMMS performs range checking on the actual arguments at runtime.
\item  Both functions and procedures can have a {\tt RETURN}
statement.
\end{itemize}

\paragraph{There are differences}

There are also differences between a function and a procedure, as summarized
below:
\begin{itemize}
\item Functions return a result that can be used in numerical
expressions. The result can be either scalar-valued or indexed, and can have an
associated unit of measurement.
\item Functions cannot have side effects either on global identifiers
or on their arguments, i.e.\ every function argument is of type {\tt Input} by
definition.
\end{itemize}

\paragraph{Not allowed in constraints}

AIMMS only allows the (possibly multi-dimensional) result of a function to
be used in constraints if none of the function arguments are variables.
Allowing function arguments to be variables, would require AIMMS to compute
the Jacobian of the function with respect to its variable arguments, which is
not a straightforward task. External functions in AIMMS do support variables
as arguments (see also Section~\ref{sec:extern.constraints}).

\paragraph{Example: the Cobb-Douglas function }\herelabel{examp:Cobb-Douglas}
\index{Cobb-Douglas function} \index{function!Cobb-Douglas}

The Cobb-Douglas (CD) function is a scalar-valued function that is often used
in economical models. It has the following form:
\[
        q = CD_{(a_1,\ldots,a_k)}(c_1, \ldots, c_k) = \prod_f c_f^{a_f},
\]
where \vskip2\jot \qquad\begin{tabular}{ll}
$q$ & is the quantity produced,\\
$c_f$ & is the factor input $f$,\\
$a_f$ & is the share parameter satisfying $a_f\geq 0$ and $\sum_f a_f = 1$.
\end{tabular}
\vskip2\jot In its simplest form, the declaration of the Cobb-Douglas function
could look as follows.
\begin{example}
Function CobbDouglas {
    Arguments  : (a,c);
    Range      : nonnegative;
    Body       : {
        CobbDouglas := prod[f, c(f)^a(f)]
    }
}
\end{example}
The arguments of the {\tt CobbDouglas} function must be declared in a local
declaration subnode. The following declarations describe the arguments.
\begin{example}
Set InputFactors {
    Index        : f;
}
Parameter a {
    IndexDomain  : f;
}
Parameter c {
    IndexDomain : f;
}
\end{example}

\paragraph{Function attributes}
\declindex{Function} \AIMMSlink{function}

The attributes of functions are listed in Table~\ref{table:intern.attr-func}.
Most of them are the same as those of procedures.
\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type} & {\bf See also} \\
\hline
{\tt Arguments} & {\em argument-list}  & \\
{\tt IndexDomain} & {\em index-domain} & \pageref{attr:par.index-domain} \\
{\tt Range} & {\em range} & \pageref{attr:par.range} \\
{\tt Unit}  & {\em unit-expression}  & \pageref{attr:par.unit} \\
{\tt Property} & {\tt RetainsValue} & \\
{\tt Body}      & {\em statements}     & \pageref{chap:exec} \\
%{\tt DERIVATIVE} & {\em statements}    & \\
{\tt Comment}   & {\em comment string} & \\
\hline\hline
\end{tabular}
\caption{{\tt Function} attributes}\label{table:intern.attr-func}
\end{aimmstable}

\paragraph{Returning the result}
\declattrindex{function}{IndexDomain} \index{function!result}
\AIMMSlink{function.index_domain}

By providing an index domain to the function, you indicate that the result of
the function is multidimensional. Inside the function you can use the function
name with its indices as if it were a locally defined parameter. The result of
the function must be assigned to this `parameter'. As a consequence, the body
of any function should contain at least one assignment to itself to be useful.
Note that the {\tt RETURN} statement cannot have a return value in the context
of a function body.

\paragraph{The {\tt Range} attribute}\herelabel{attr:function.range}
\declattrindex{function}{Range} \AIMMSlink{function.range}

Through the {\tt Range} attribute you can specify in which numerical, set,
element or string range the function should assume its result. If the result of
the function is numeric and multidimensional, you can specify a range using
multidimensional parameters which depend on all or only a subset of the indices
specified in the {\tt IndexDomain} of the function. This is similar as for
parameters (see also page~\pageref{attr:par.range}). Upon return from the
function, AIMMS will verify that the function result lies within the
specified range.

\paragraph{The {\tt Unit} attribute}\herelabel{attr:function.unit}
\declattrindex{function}{Unit} \AIMMSlink{function.unit}

Through the {\tt Unit} attribute of a function you can associate a unit with
the function result.  AIMMS will use the unit specified here during the unit
consistency check of each assignment to the result parameter within the
function body, based on the units of the global identifiers and function
arguments that are referenced in the assigned expression. In addition, AIMMS
will use the value of  the {\tt Unit} attribute during unit consistency checks
of all expressions that contain calls to the function at hand. You can find
general information on the use of units in Chapter~\ref{chap:units}.
Section~\ref{sec:units.analysis.arg} focusses on unit consistency checking for
functions and procedures.

\paragraph[0.2]{Example: computing the shortest distance}

The procedure {\tt ComputeShortestDistance} discussed in the previous section
can also be implemented as a function {\tt ShortestDistance}, returning an
indexed result. In this case, the declaration looks as follows.
\begin{example}
Function ShortestDistance {
    Arguments    : (City, DistanceMatrix);
    IndexDomain  : j;
    Range        : nonnegative;
    Comment      : {
        "This procedure computes the distance along the shortest path
         from City to any other city j, given DistanceMatrix."
    }
    Body         : {
        ShortestDistance(j) := DistanceMatrix(City,j);

        for ( j | not ShortestDistance(j) ) do
            /*
             *  Compute the shortest path and the corresponding distance
             *  for cities j without a direct connection to City.
             */
        endfor
    }
\end{example}

%%\paragraph{Variable arguments}
%%\index{function!use in constraint}
%%\index{constraint!use of functions}
%%\index{variable!as function argument}
%%
%%AIMMS allows you to use functions in the constraints of a mathematical
%%program. To accommodate this, AIMMS makes a distinction between
%%function arguments of type {\tt PARAMETER} and arguments of type {\tt
%%VARIABLE}. When a function is executed as part of an expression in an
%%ordinary assignment, AIMMS makes no distinction between both types of
%%arguments. In the context of a mathematical program, however, AIMMS
%%will provide the solver with the derivative information for all variable
%%arguments of the function, while it will not do so for parameter
%%arguments. The actual computation of the derivatives is explained in the
%%next section.
%%
%%\subsection{Derivative computation}\label{sec:intern.func.derivative}
%%
%%\paragraph{Functions in constraints}
%%
%%Whenever you use functions with variable arguments in constraints of a
%%mathematical program, the following rules apply.
%%\begin{itemize}
%%\item AIMMS requires that the mathematical program dependent on these
%%constraints be declared as nonlinear.
%%\item All the actual variable arguments must correspond to formal
%%arguments which have been locally declared as {\tt VARIABLES}.
%%\end{itemize}
%%If you fail to comply with these rules, a compiler error will result.
%%
%%\paragraph{Providing derivatives}
%%\index{function!derivative computation}
%%\index{derivative!of a function}
%%
%%During the solution process of a mathematical program containing such
%%functions, partial derivative information of the function with respect
%%to all the variable arguments must be passed to the solver. AIMMS
%%supports three methods to compute the derivatives of a function:
%%\begin{itemize}
%%\item you provide the actual statements for computing the derivatives
%%as a part of the function declaration,
%%\item AIMMS computes the derivatives based on the contents of the
%%body, or
%%\item AIMMS estimates the derivatives using a simple
%%differencing scheme.
%%\end{itemize}
%%
%%\paragraph{Automatic derivative computation}
%%
%%When the body of a function consists of a {\em single} AIMMS
%%assignment to compute the function value, AIMMS is able to generate
%%the instructions for the computation of all partial derivatives
%%automatically from this assignment as well. Thus, for such simple
%%functions there is no need for you to provide any further information.
%%
%%\paragraph{Example: the Cobb-Douglas function}
%%
%%Consider the Cobb-Douglas function discussed in the previous section.
%%Because its body consists of a single assignment, AIMMS is able to
%%compute its partial derivatives for the Cobb-Douglas function
%%automatically. This will result in the following automatic
%%computations:
%%\[
%%   \frac{\partial q}{\partial c_i} = a_i c_i^{a_i-1} \prod_{f\neq i} c_f^{a_f}
%%\]
%%
%%\paragraph{The {\tt DERIVATIVE} attribute}
%%\declattrindex{function}{DERIVATIVE}
%%\AIMMSlink{function.derivative}
%%
%%When AIMMS is not able to perform the single-assignment automatic
%%derivative computation, and you decide to provide the partial
%%derivatives yourself, their computation must be specified in the
%%special {\tt DERIVATIVE} attribute that is unique to functions. The
%%following rules apply.
%%\begin{itemize}
%%\item If the solver only needs the function value itself, AIMMS
%%will only execute the statements in the {\tt BODY} attribute.
%%\item If the solver needs the function value as well as its partial
%%derivatives, AIMMS will only execute the statements in the {\tt
%%DERIVATIVE} attribute.
%%\end{itemize}
%%As a consequence, the {\tt DERIVATIVE} attribute should compute the
%%function value as well. The reason for this combined computation is
%%that the function value and derivative can often be computed quicker
%%in parallel than by calculating the derivative separate from
%%calculating the function value. If there is no penalty using serial
%%computation, then the function value computation can be shared by the
%%{\tt BODY} and {\tt DERIVATIVE} attributes by creating an execution
%%subnode containing the computation, and referencing this node in both
%%attributes.
%%
%%\paragraph{The {\tt .IsVariable} suffix}
%%\suffindex{function}{IsVariable}
%%\AIMMSlink{isvariable}
%%
%%When a function with formal variable arguments is called inside a
%%constraint, the actual arguments need not necessarily be variables at
%%all. Consequently, there is no need to compute the partial derivatives
%%of these arguments in such a case. During the evaluation of the {\tt
%%DERIVATIVE} attribute of a function you can use the {\tt .IsVariable}
%%suffix of a formal variable argument to verify whether an actual
%%argument in this call to the function is really a variable.
%%
%%\paragraph{The {\tt .Derivative} suffix}
%%\suffindex{function}{Derivative}
%%\AIMMSlink{derivative}
%%
%%For every function argument which is a variable and for which the {\tt
%%.IsVariable} suffix is set, you must assign the partial derivative
%%value(s) to the {\tt .Derivative} suffix of that variable. Note that
%%this will have an impact on the number of indices. If the result of a
%%block-valued function is $m$-dimensional, the derivative information
%%with respect to an $n$-dimensional variable argument will result in an
%%$(m+n)$-dimensional identifier holding the derivative.
%%
%%\paragraph{Abstract example}
%%
%%Consider a function {\tt f} with an index domain $(i_1,\dots,i_m)$ and
%%a variable argument {\tt x} with index domain $(j_1,\dots,j_n)$. Then
%%the matrix with partial derivatives of {\tt f} with respect to the
%%argument {\tt x} must be provided as assignments to the suffix ${\tt
%%x.Derivative}(i_1,\dots,i_m,j_1,\dots,j_n)$. Each element of this
%%identifier represents the partial derivative
%%\[
%%        \frac{\partial {\tt f}(i_1,\dots,i_m)}
%%             {\partial {\tt x}(j_1,\dots,j_n)}
%%\]
%%
%%
%%\paragraph{Cobb-Douglas function revisited}
%%\index{Cobb-Douglas function}
%%\index{function!Cobb-Douglas}
%%
%%Consider the Cobb-Douglas function discussed above. Although AIMMS
%%is capable of computing its partial derivatives automatically, you may
%%verify that the derivative with respect to argument $c_i$ can also be
%%written more compactly as follows:
%%\[
%%   \frac{\partial q}{\partial c_i} = \frac{a_i}{c_i}CD(c_1, \ldots, c_k)
%%\]
%%Thus, you could consider providing the computation of the partial
%%derivatives for the Cobb-Douglas function (although in this particular
%%example this will probably not lead to increased efficiency). This can
%%be implemented in AIMMS as follows.
%%\begin{example}
%%FUNCTION
%%    identifier : CobbDouglas
%%    arguments  : (a,c)
%%    body       : CobbDouglas := prod[f, c(f)^a(f)];
%%    derivative : CobbDouglas := prod[f, c(f)^a(f)];
%%                 if ( c.IsVariable ) then
%%                    c.Derivative(f) := (a(f)/c(f)) * CobbDouglas ;
%%                 endif;
%%\end{example}
%%Because the function value is scalar, the derivative suffix for the
%%variable argument {\tt c} has the same dimension as {\tt c} itself.
%%
%%\paragraph{Numerical differencing}
%%\index{derivative!numerical differencing}
%%\index{function!derivative by differencing}
%%\index{numerical differencing}
%%
%%When AIMMS cannot compute the derivative values automatically, and
%%you have not specified the {\tt DERIVATIVE} attribute to compute the
%%derivatives yourself, AIMMS employs a simple differencing scheme to
%%estimate the derivatives. For example, assume that AIMMS requires
%%the derivative of a function $f(x_1, x_2,\ldots, x_k)$ at the point
%%$(\bar x_1, \bar x_2, \ldots, \bar x_k)$. Then AIMMS will
%%approximate each partial derivative as follows:
%%\[
%%  \frac{\partial}{\partial x_i}f(\bar x_1, \bar x_2, \ldots, \bar x_k)
%%  \approx \frac{{f(\bar x_1,\ldots, \bar x_i + \varepsilon, \ldots,
%%  \bar x_k ) - f(\bar x_1, \ldots, \bar x_k)}}{\varepsilon}
%%\]
%%where $\varepsilon$ is a small number.
%%
%%\paragraph{The {\tt DIFFERENCING} attribute}
%%\declattrindex{variable}{DIFFERENCING}
%%\declattrindex{function}{DIFFERENCING}
%%\AIMMSlink{variable.differencing}
%%\AIMMSlink{function.differencing}
%%\AIMMSlink{differencingeps}
%%
%%In the declaration of the formal arguments of a function, you can
%%specify the value for $\varepsilon$ that AIMMS will use for each
%%individual variable argument. For this purpose all {\tt VARIABLE}
%%declarations inside a function have an additional {\tt DIFFERENCING}
%%attribute that is not present for variable declarations outside the
%%context of a function. If you do not specify a value for the {\tt
%%DIFFERENCING} attribute of a particular variable, AIMMS will use
%%the current value of the global option {\tt Differencing\_Delta} instead.
%%
%%\paragraph{Disadvantages of numerical differencing}
%%
%%While the numerical differencing scheme does not require any action
%%from the user, there are two distinct disadvantages.
%%\begin{itemize}
%%\item First of all, numerical differencing is not always a stable
%%process, and the results may not be accurate enough. As a result, a
%%nonlinear solver may have trouble converging to a solution.
%%\item Secondly, the process can be computationally very expensive.
%%\end{itemize}
%%In general, it is recommended that you do not rely on numerical
%%differencing. This is especially the case when the function body is
%%quite extensive, or when the function, at the individual level, has a
%%lot of variable arguments or contains conditional loops.


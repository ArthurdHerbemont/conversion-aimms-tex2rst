\section{\win32~calling conventions}\label{sec:extern.win32}

\paragraph{\win32~calling conventions}
\index{external procedure or function!calling convention} \index{calling
convention!Win32@{\win32}}

The 32-bit Windows environment (\win32) supports several calling conventions
that influence the precise manner in which arguments are passed to a function,
and how the return value must be retrieved. When calling an external function
or procedure in this environment, AIMMS will {\em always} assume the {\tt
WINAPI} calling convention. The following macro in \CLANGUAGE/ makes sure that
the {\tt WINAPI} calling convention is used. That same macro also makes sure
that the function or procedure is automatically exported from the DLL.
\begin{example}
    #include <windows.h>
    #define DLL_EXPORT(type) __declspec(dllexport) type WINAPI
\end{example}
You can add this macro to the implementation of any function that you want to
call from within AIMMS, as illustrated below.
\begin{example}
    DLL_EXPORT(double) Cobb_Douglas( int n, double *a, double *c )
    {
        /* Implementation of Cobb_Douglas goes here */
    }
\end{example}

\paragraph{Prevent {\C++} name mangling}
\index{name mangling} \index{external procedure or function!name mangling}

By default, {\C++} compilers will perform a process referred to as {\em name
mangling}, modifying each function name in your source code according to its
prototype. By doing this, {\C++} is able to deal with the same function name
defined for different argument types. If you want to export a DLL function to
AIMMS, however, you must prevent name mangling to take place, ensuring that
AIMMS can find the exported function name within the DLL. You can do this by
declaring the prototype of the function using the following macro, which
accounts for both {\C} and {\C++}.
\begin{example}
    #ifdef __cplusplus
    #define DLL_EXPORT_PROTO(type) extern "C" __declspec(dllexport) type WINAPI
    #else
    #define DLL_EXPORT_PROTO(type) extern __declspec(dllexport) type WINAPI
    #endif
\end{example}
Thus, to make sure that a {\C++} implementation of {\tt Cobb\_Douglas} is
exported without name mangling, declare its prototype as follows before
providing the function implementation.
\begin{example}
    DLL_EXPORT_PROTO(double) Cobb_Douglas( int n, double *a, double *c );
\end{example}
Function declarations like this are usually stored in a separate header file.
Note that along with this prototype declaration, you must still use the {\tt
DLL\_EXPORT} macro in the implementation of {\tt Cobb\_Douglas}.

\paragraph{DLL initialization}

When your external DLL requires initialization statements to be executed when
the DLL is loaded, or requires the execution of some cleanup statements when
the DLL is closed, you can accomplish this by adding a function {\tt DllMain}
to your DLL. When the linker finds a function named {\tt DllMain} in your DLL,
it will execute this function when opening and closing the DLL. The following
example provides a skeleton {\tt DllMain} implementation which you can directly
copy into your DLL source code.
\pagebreak
\begin{example}
    #include <windows.h>

    BOOL WINAPI DllMain(HINSTANCE hdll, DWORD reason, LPVOID reserved)
    {
        switch( reason ) {
           case DLL_THREAD_ATTACH:
               break;
           case DLL_PROCESS_ATTACH:
               /* Your DLL initialization code goes here */
               break;
           case DLL_THREAD_DETACH:
               break;
           case DLL_PROCESS_DETACH:
               /* Your DLL exit code goes here */
               break;
        }
        return 1; /* Return 0 in case of an error */
    }
\end{example}
To prevent name mangling to take place, you can best declare the function {\tt
DllMain} as follows.
\begin{example}
    #ifdef __cplusplus
    extern "C" BOOL WINAPI DllMain(HINSTANCE hdll, DWORD reason, LPVOID reserved);
    #else
    BOOL WINAPI DllMain(HINSTANCE hdll, DWORD reason, LPVOID reserved);
    #endif
\end{example}

\section{\C~versus \fortran~conventions}\label{sec:extern.language}

\paragraph{Language conventions}
\index{calling convention!C versus FORTRAN@{\C} versus \fortran}
\index{external procedure or function!C versus FORTRAN@{\C} versus \fortran}

For any external procedure or function you can specify whether the DLL
procedure or function to which the execution is relayed, is written in
{\C}-like languages (such as {\C} and {\C}++) or {\fortran} (see also
Section~\ref{sec:extern.declaration}). For {\fortran} code AIMMS will make
sure that
\begin{itemize}
\item scalar values are always passed by reference (i.e.\ as a
pointer), and
\item multidimensional arrays are ordered in a {\fortran}-compatible manner.
\end{itemize}
By default, AIMMS will use \CLANGUAGE/ conventions when passing arguments to
the DLL procedure or function.

\paragraph{Strings excluded}

AIMMS will not directly translate strings into {\fortran} format, because
most {\fortran} compilers use their own particular string representation. Thus,
if you want to pass strings to a {\tt fortran} subroutine, you should write
your own \CLANGUAGE/ interface which converts \CLANGUAGE/ strings into the
format appropriate for your {\fortran} compiler.

\paragraph{Array dimensions and ordering}

When a multidimensional parameter (or parameter slice) is specified as a {\tt
array} argument to an external procedure, AIMMS passes an array of the
specified type which is constructed as follows. If the actual argument has $n$
remaining (i.e.\ non-sliced) dimensions of cardinality $N_1,\dots,N_n$,
respectively, then the associated values are passed as a (one-dimensional)
array of length $N_1\cdots N_n$. The value associated with the tuple
$(i_1,\dots,i_n)$ is mapped onto the element
\[
         i_n + N_n\bigl( i_{n-1} + N_{n-1}\bigl( \cdots \bigl( i_2 +
         N_2 i_1\bigr) \cdots \bigr)\bigr)
\]
for running indices $i_j=0,\dots,N_j-1$ (\CLANGUAGE/-style programming). For
\PASCAL/-like languages (with indices running from $1,\dots,N$) all running
indices in this formula must be decreased by 1, and the final result increased
by 1. This ordering is compatible with the \CLANGUAGE/ declaration of e.g.\ the
multidimensional array
\begin{quote}\examplefont\em
     double arr[$N_1$][$N_2$]\dots[$N_n$];
\end{quote}

\paragraph{Multidimen\-sional example in C}

The C function {\tt ComputeAverage} defined below computes the average of a
2-dimensional parameter {\tt a(i,j)} passed as an argument in AIMMS.
\begin{example}
    DLL_EXPORT(void) ComputeAverage( double *a, int card_i, int card_j, double *average )
    { int i, j;
      double sum_a = 0.0;

    #define __A(i,j)    a[j + i*card_j]

      for ( i = 0; i < card_i; i++ )
        for ( j = 0; j < card_j; j++ )
          sum_a += __A(i,j);

      *average = sum_a / (card_i*card_j);
    }
\end{example}
Within your AIMMS model, you can call this procedure via an external
procedure declaration {\tt ExternalAverage} defined as follows.
\begin{example}
ExternalProcedure ExternalAverage {
    Arguments     : (x,res);
    DLLName       : "Userfunc.dll";
    BodyCall      : ComputeAverage(double array: x, card: i, card: j, double scalar: res);
}
\end{example}
where the argument {\tt x} and {\tt res} are declared as
\begin{example}
Parameter x {
    IndexDomain   : (i,j);
    Property      : Input;
}
Parameter res {
    Property      : Output;
}
\end{example}

\paragraph{{\tt Fortran} array ordering}

When you specify the {\fortran} language convention for an external procedure,
AIMMS will order the array passed to the external procedure such that the
tuple $(i_1,\dots,i_n)$ is mapped onto the element
\[
         i_1 + N_1\bigl( i_{2} - 1 + N_{2}\bigl( \cdots \bigl( i_{n-1} -1 +
         N_{n-1} \bigl(i_n-1\bigr)\bigr) \cdots \bigr)\bigr)
\]
for running indices $i_j=1,\dots,N_j$. This is compatible with the default
storage of multidimensional arrays in {\fortran}, and allows you to access such
array arguments using the ordinary multidimensional notation.

\paragraph{Example}

Consider a parameter {\tt a(i,j)}, where the index {\tt i} is associated with
the set {\{{\tt 1}, {\tt 2}\}} and {\tt j} with the set {\{{\tt 1}, {\tt 2},
{\tt 3}\}}. When this parameter is passed as a {\tt array} argument to an
external procedure, the resulting array (as a one-dimensional array with 6
elements) is ordered as follows in the {\C} convention (default).
\[
\begin{array}{|l|c|c|c|c|c|c|}
\hline
\mbox{\bf Element \#} & 0 & 1 & 2 & 3 & 4 & 5 \\
\hline \mbox{\bf Value} & \mbox{\tt a(1,1)} & \mbox{\tt a(1,2)} & \mbox{\tt
a(1,3)} &
\mbox{\tt a(2,1)} & \mbox{\tt a(2,2)} & \mbox{\tt a(2,3)} \\
\hline
\end{array}
\]
With the {\fortran} language convention, the ordering is changed as follows.
\[
\begin{array}{|l|c|c|c|c|c|c|}
\hline
\mbox{\bf Element \#} & 1 & 2 & 3 & 4 & 5 & 6 \\
\hline \mbox{\bf Value} & \mbox{\tt a(1,1)} & \mbox{\tt a(2,1)} & \mbox{\tt
a(1,2)} &
\mbox{\tt a(2,2)} & \mbox{\tt a(1,3)} & \mbox{\tt a(2,3)} \\
\hline
\end{array}
\]

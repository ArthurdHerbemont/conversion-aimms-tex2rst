\section{Introduction}\label{sec:extern.intro}

\paragraph{Getting started}
\index{procedure!external} \index{function!external} \index{external procedure
or function}

The aim of this section is to give you a quick feel for the effort required to
make a link to an external function or procedure through a short illustrative
example linking a {\C} implementation of the Cobb-Douglas function (discussed
in Section~\ref{examp:Cobb-Douglas}) into an AIMMS application.
Section~\ref{sec:api.intro} contains a more elaborate example of an external
procedure which uses AIMMS API functions to obtain additional information
about the passed arguments.

\paragraph{External procedures and functions}

The interface to external procedures and functions is arranged through special
{\tt ExternalProcedure} and {\tt ExternalFunction} declarations which behave
just like internal procedures and functions. Instead of specifying a body to
initiate internal AIMMS computations, the execution of external procedures
and functions is relayed to the indicated procedures and functions inside one
or more DLL's.

\paragraph{The Cobb-Douglas function}
\index{Cobb-Douglas function} \index{function!Cobb-Douglas}

Consider the {\tt Cobb-Douglas} function discussed in
Section~\ref{examp:Cobb-Douglas}. Given the cardinality {\tt n} of the set {\tt
InputFactors} and two arrays {\tt a} and {\tt c} of doubles representing the
one-dimensional input arguments of the Cobb-Douglas function (both defined over
{\tt InputFactors}), the following simple {\tt C} function computes its value.
\begin{example}
double Cobb_Douglas( int n, double *a, double *c ) {
  int i;
  double CD = 1.0 ;

  for ( i = 0; i < n; i++ )
    CD = CD * pow(c[i],a[i]) ;

  return CD;
}
\end{example}
In the sequel it is assumed that this function is contained in a DLL named {\tt
"Userfunc.dll"}.

\paragraph{Linking to AIMMS}

In order to make the function available in AIMMS you have to declare an {\tt
ExternalFunction} {\tt CobbDouglasExternal}, which just relays its execution
to the {\C} implementation of the Cobb-Douglas function discussed above. The
declaration of {\tt CobbDouglasExternal} looks as follows.
\begin{example}
ExternalFunction CobbDouglasExternal {
    Arguments     : (a,c);
    Range         : nonnegative;
    DLLName       : "Userfunc.dll";
    ReturnType    : double;
    BodyCall      : Cobb_Douglas( card : InputFactors, array: a, array: c );
}
\end{example}
The arguments {\tt a} and {\tt c} must be declared in the same way as for the
internal {\tt CobbDouglas} function discussed on
page~\pageref{examp:Cobb-Douglas}, with the exception that for the external
implementation we will also compute the Jacobian with respect to the argument
{\tt c(f)}. For this reason, the argument {\tt c(f)} is declared as a {\tt
Variable}.
\begin{example}
Set InputFactors {
    Index        : f;
}
Parameter a {
    IndexDomain  : f;
}
Variable c {
    IndexDomain  : f;
}
\end{example}

\paragraph{Explanation}

The translation type ``{\tt card}'' of the set argument {\tt InputFactors}
causes AIMMS to pass the cardinality of the set as an integer value to the
external function \verb|Cobb_Douglas|. The translation type ``{\tt array}'' of
the arguments {\tt a} and {\tt c} are instructions to AIMMS to pass these
arguments as full arrays of double precision values. As function arguments are
always of type {\tt Input}, AIMMS will disregard any changes made to the
arguments by the external function. The {\tt double} return value of the {\C}
function \verb|Cobb_Douglas| will become the result of the function {\tt
CobbDouglasExternal}.

\paragraph{Calling external functions}

After the declaration of an external function or procedure you can use it as if
it were an internal function or procedure. Thus, to call the external function
{\tt CobbDouglasExternal} in the body of a procedure the following statement
suffices.
\begin{example}
     CobbDouglasValue := CobbDouglasExternal(a,c) ;
\end{example}
Of course, any two (possibly sliced) identifiers with single common index
domain could have been used as arguments. AIMMS will determine this common
index domain, and pass its cardinality to the external function.

\paragraph{Use in constraints}

Unlike internal functions, external functions can be called inside constraints.
To accomplish this, the declaration has to be extended with a {\tt DerivativeCall} attribute. 
For this attribute you specify the external call that has to
be made when AIMMS also needs the partial derivatives of all variable
arguments inside constraints of mathematical programs. In the absence of a {\tt
DerivativeCall} attribute, AIMMS will use a differencing scheme to estimate
these derivatives. The details of using external functions in constraints, as
well as the obvious extension to compute the derivative of the Cobb-Douglas
function directly, are given in Section~\ref{sec:extern.constraints}.

\paragraph{Setting up external libraries}

Once you have developed a collection of external functions and procedures, it
may be a good idea to make this available in the form of a library for use in
AIMMS applications. In this way, the users of your library do not have to
spend any time translating their AIMMS arguments into external arguments of
the appropriate type in the external procedure and function declarations.

\paragraph{Save library as\\ include file}

To provide a library as an entity on its own, you can store all the external
procedures and functions in a separate model section, and save this section as
a source file. The functions and procedures in the library can then be made
available by simply including this source file into a model.

\paragraph{Hiding the interface}

When you want to protect the interface to your external library, you can
accomplish this by encrypting the include file containing the function library
(see also the AIMMS User's Guide). Thus, the interface to the external
library becomes invisible, effectively preventing misuse of the library outside
AIMMS.

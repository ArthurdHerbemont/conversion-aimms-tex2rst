\section{Declaration of external procedures and functions}\label{sec:extern.declaration}

\paragraph{External procedures and functions}
\declindex{ExternalProcedure} \declindex{ExternalFunction}
\AIMMSlink{external_procedure} \AIMMSlink{external_function}

External procedures and functions are special types of nodes in the model tree.
They have the same attributes as internal procedures and functions with the
exception of the {\tt Body} and {\tt Derivative} attributes, which are replaced
by the attributes in Table~\ref{table:extern.attr}.
\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type} & {\bf See also} \\
                &                  & {\bf page} \\
\hline
{\tt DllName}        & {\em string}, {\em file-identifier}  & \\
{\tt ReturnType}     & {\tt integer}, {\tt double}  & \\
{\tt Property}        & {\tt FortranConventions}, {\tt UndoSafe} &
\pageref{attr:undosafe} \\
{\tt BodyCall} & {\em external-call} & \\ {\tt DerivativeCall} &
{\em external-call} & \\
\hline\hline
\end{tabular}
\caption{Additional attributes of external procedures and
functions}\label{table:extern.attr}
\end{aimmstable}

\paragraph{The {\tt DllName} attribute}
\declattrindex{external procedure or function}{DllName} \index{PATH
environment variable@{\tt PATH} environment variable} \index{AIMMSUSERDLL
environment variable@{\tt AIMMSUSERDLL} environment variable}
\index{environment variable!PATH@{\tt PATH}} \index{environment
variable!AIMMSUSERDLL@{\tt AIMMSUSERDLL}}
\AIMMSlink{external_procedure.dll_name} \AIMMSlink{external_function.dll_name}

With the mandatory {\tt DllName} attribute you can specify the name of the DLL
which contains the external procedure or function to which you want to make a
link in your AIMMS application. The value of the attribute must be a string,
a string parameter, or a {\tt File} identifier, representing the path to the
external DLL. 

\paragraph{Search path}

If you only specify a DLL name, AIMMS will search for the DLL in all directories 
in the {\tt AIMMSUSERDLL} environment variable, and the {\tt PATH} environment variable
on Windows, or the \verb|LD_LIBRARY_PATH| environment variables on Linux, respectively. 
In addition, on Windows, AIMMS will also search for the DLL in the project folder. 
If you specify a relative path including a folder (possibly ./), AIMMS will take this path
relative to the project folder. If you specify an absolute path, AIMMS will try to open the DLL at the 
specified location.

\paragraph{{\tt File} identifier and unit conventions}
\declindex{File} \declattrindex{file}{Convention}

When you use a {\tt File} identifier to specify an external DLL name, AIMMS
will use the {\tt Convention} attribute of that {\tt File} identifier (if
specified) to pass numeric values to any procedure or function in that DLL
according to the specified unit convention (see also
Section~\ref{sec:units.convention}). When the DLL name has not been specified
through a {\tt File} identifier, or when its {\tt Convention} attribute is left
empty, AIMMS will use the unit convention specified for the main model.

\paragraph{Default argument scaling}

Without any such convention, AIMMS will use the default convention, i.e.\
arguments will be scaled according to the unit specified for each argument, and
AIMMS will assume that the result of an external function is scaled
according to the unit specified in its {\tt Unit} attribute. Unit analysis for
functions and procedures is discussed in full detail in
Section~\ref{sec:units.analysis.arg}.

\paragraph{The {\tt ReturnType} attribute}
\declattrindex{external procedure or function}{ReturnType}
\AIMMSlink{external_procedure.return_type}
\AIMMSlink{external_function.return_type}

The {\tt ReturnType} indicates the type of any {\em scalar} numerical value
returned by the DLL function. The possible values are {\tt integer} and {\tt
double}. AIMMS will use the value returned by the DLL function either as the
return value of the {\tt ExternalProcedure}, or as the (numerical) function
value of the {\tt ExternalFunction}, whichever is applicable. If you do not
specify the {\tt ReturnType} attribute, AIMMS will discard any value
returned by the function.

\paragraph{Restricted use}

You cannot directly use the returned value of a DLL function as the function
value of an {\tt ExternalFunction} when its return value is either an indexed
parameter, a set, a set element or a string. In such cases you must pass the
function name as an additional external argument to the DLL function, and
specify how the function value must be dealt with.

\paragraph{Example}

Consider a {\C} function \verb|Cobb_Douglas_Arg| with prototype
\begin{example}
    void Cobb_Douglas_Arg( int n, double *a, double *c, double *CDValue );
\end{example}
which passes the Cobb-Douglas function value through the argument {\tt CDValue}
instead of as the return value. In this example {\tt CDValue} is a scalar,
which could have been passed as the result of the DLL function as well. The
following {\tt ExternalFunction} declaration provides a link with
\verb|Cobb_Douglas_Arg| and obtains its function value via the argument list.
\begin{example}
ExternalFunction CobbDouglasArgument {
    Arguments     : (a,c);
    Range         : nonnegative;
    DllName       : "Userfunc.dll";
    BodyCall      : {
        Cobb_Douglas_Arg( card : InputFactors, array: a, array: c,
            scalar: CobbDouglasArgument );
    }
}
\end{example}

\paragraph{The {\tt Property} attribute}
\declattrindex{external procedure or function}{Property}
\declpropindex{external procedure or function}{FortranConventions}
\declpropindex{external procedure or function}{UndoSafe}
\AIMMSlink{external_procedure.property} \AIMMSlink{external_function.property}

With the {\tt Property} attribute you can specify through the {\tt
FortranConventions} property whether the external function is based on
{\fortran} calling conventions. By default, AIMMS will assume that the DLL
function is written in a {\C}-like languages such as {\C}, {\C++} or {\pascal}.
The precise differences between both calling conventions are explained in full
detail in Section~\ref{sec:extern.language}. In addition, for external
procedures, you can specify the {\tt UndoSafe} property. The semantics of the
{\tt UndoSafe} property is discussed in Section~\ref{sec:intern.proc}.

\paragraph{Formal argument types}

As with internal procedures and functions, all formal arguments of an external
procedure or function must be declared as local identifiers. AIMMS supports
the following identifier types for formal arguments of external procedures and
functions:
\begin{itemize}
\item simple sets and relations,
\item scalar and indexed {\tt Parameters},
\item scalar and indexed {\tt Variables} (external functions only), and
\item {\tt Handles} (external procedures only).
\end{itemize}

\paragraph{Argument handling}

Many details regarding the handling of arguments of internal procedures and
functions also apply to external procedures and functions. Thus, arguments of
external procedures and functions can be defined over global and local sets,
and their associated units of measurement can be specified in terms of either
global units or locally defined unit parameters, completely similar to internal
procedures and functions (see Section~\ref{sec:intern.proc}).

\paragraph{{\tt Handle} arguments}
\declindex{Handle} \AIMMSlink{handle}

The {\tt Handle} identifier type is only supported for formal arguments of
external procedures, i.e.\ it is not possible to declare global identifiers of
type {\tt Handle}. The following rules apply:
\begin{itemize}
\item {\tt Handle} arguments are always declared as scalar local identifiers,
\item {\tt Handle} arguments can only be passed to the DLL function
as an integer {\tt Handle} (see below), and
\item the actual argument in a call to the external procedure
corresponding to a formal {\tt Handle} argument can be a (sliced) reference to
an identifier in your model {\em of any type and of any dimension}.
\end{itemize}
{\tt Handle} arguments allow you to completely circumvent any type checking on
actual arguments with respect to the dimension and the respective index domains
of the corresponding formal arguments in the call to an external procedure. As
a result of this, however, the actual data transfer of {\tt Handle} arguments
to the DLL function must completely take place via the AIMMS API (see also
Chapter~\ref{chap:api}).

\paragraph{The {\tt BodyCall} attribute}
\declattrindex{external procedure or function}{BodyCall}
\AIMMSlink{external_procedure.body_call}
\AIMMSlink{external_function.body_call}

\syntaxmark{DLL-function} In the mandatory {\tt BodyCall} attribute you must
specify the call to the DLL procedure or function, to which the execution of
the {\tt ExternalProcedure} or {\tt Function} must be relayed. Such an
external call specifies:
\begin{itemize}
\item the name of the DLL function or procedure that must be called, and
\item how the actual AIMMS arguments must be translated into arguments
suitable for the DLL function or procedure.
\end{itemize}
Any external call must be specified according to the syntax below. In the Model
Explorer, you can specify all components of the {\tt BodyCall} attribute using
a wizard which will guide you through most of the necessary detail.

\paragraph{Syntax}
\syntaxdiagram{external-call}{extern-call}
\syntaxdiagram{external-argument}{extern-arg}

\paragraph{Mandatory translation type}
\index{argument!external} \index{external argument} \index{external
argument!translation type} \index{translation type} \typindex{translation
type}{scalar} \typindex{translation type}{literal} \typindex{translation
type}{array} \typindex{translation type}{card} \typindex{translation
type}{handle} \typindex{translation type}{work}

\syntaxmark{translation-type} The mandatory translation type indicates the type
of the external argument into which the actual argument must be translated
before being passed to the external procedure. The following translation types
are supported.
\begin{itemize}
\item {\bf\tt scalar}: the actual scalar AIMMS argument is passed on as a
scalar of the indicated external data type.
\item {\bf\tt literal}: the literal specified in the external call is passed on as a
scalar of the indicated external data type, i.e.\ a {\tt literal} argument does
{\em never} correspond to an actual AIMMS argument, but is specified
directly in the {\tt BodyCall} attribute.
\item {\bf\tt array}: the AIMMS argument is passed on as an array of values
according to the indicated translation type and external data type. The precise
manner in which the translation takes place is discussed below.
\item {\bf\tt card}: the cardinality of a set argument is passed on as
an integer value. The set argument can be either a set passed as an actual
AIMMS argument or the domain set of a multi-dimensional parameter passed as
an actual argument.
\item {\bf\tt handle}: an integer handle to a (sliced) set or parameter
argument is passed on. Within the external procedure you must use functions
from the AIMMS API (see also Chapter~\ref{chap:api}) to obtain the
dimension, domain and range associated with the handle, or to retrieve or
change its data values.
\item {\bf\tt work}: an array of the indicated type is passed as a temporary
workspace to the external procedure. The actual argument must be an integer
expression and is interpreted as the size of the array to be passed on. This
translation type is useful for programmers of languages such as standard {\sc
F}77 {\fortran} which lack facilities for dynamic memory allocation.
\end{itemize}

\paragraph{Actual external argument}
\index{external argument! actual}

\syntaxmark{actual-external-argument} The actual external argument specified in
an external argument of the {\tt BodyCall} attribute can be
\begin{itemize}
\item a reference to a formal argument of the {\tt ExternalProcedure} at
hand (for the {\tt scalar}, {\tt array}, {\tt card}, {\tt handle} and {\tt
work} translation types),
\item a reference to a domain set of a formal
multi-dimensional argument of the {\tt ExternalProcedure} at hand (for the
{\tt card} translation type), or
\item an integer, double or string literal (such as {\tt 12345}, {\tt
123.45} or {\tt "This is a string"}) directly specified within the {\tt BodyCall} attribute (for the {\tt literal} translation type).
\end{itemize}

\paragraph{Input-output type}
\index{external argument!input-output type}

For every formal argument of an {\tt ExternalProcedure}, you can specify its
associated {\em input-output} type through the {\tt Input}, {\tt InOut}
(default) or {\tt Output} properties in the {\tt Propert} attribute of the
local argument declaration. With it, you indicate whether or not AIMMS
should consider any changes made to the argument by the DLL function. For each
input-output type, AIMMS performs the following actions:
\begin{itemize}
\item {\bf\tt Input}: AIMMS initializes the external argument, but
discards all changes made to it by the DLL function,
\item {\bf\tt InOut}: AIMMS initializes the external
argument, and passes back to the model the values returned by the DLL function,
or
\item {\bf\tt Output}: AIMMS allocates memory for the external
argument, but does not initialize it; the values returned by the DLL function
are passed back to the model.
\end{itemize}
As with internal functions, all {\tt ExternalFunction} arguments are {\tt
Input} by definition. The return value of an {\tt ExternalProcedure} and the
function value of an {\tt ExternalFunction} are considered as an (implicit)
{\tt Output} argument when passed to the {\tt DLL} function as an external
argument.

\paragraph{External data type}
\index{external argument!external data type} \index{external data type}
\typindex{external data type}{integer} \typindex{external data type}{double}
\typindex{external data type}{string} \typindex{external data type}{integer8}
\typindex{external data type}{integer16} \typindex{external data
type}{integer32}

\syntaxmark{external-data-type} In translating AIMMS arguments into values
(or arrays of values) suitable as arguments for an external procedure or
function, AIMMS supports the external data types listed in
Table~\ref{table:extern.external-types}.
\begin{aimmstable}
\begin{tabular}{|l|l|}
\hline\hline
{\bf External data type} & {\bf Passed as} \\
\hline
{\tt integer}   & 4-byte (signed) integer \\
{\tt double}    & 8-byte double precision floating number \\
{\tt string}    & \CLANGUAGE/-style string \\
\hline
{\tt integer8}  & 1-byte (signed) integer \\
{\tt integer16} & 2-byte (signed) integer \\
{\tt integer32} & 4-byte (signed) integer \\
%% \hline
%% {\tt bit}       &  bit array \\
\hline\hline
\end{tabular}
\caption{External data types}\label{table:extern.external-types}
\end{aimmstable}

\paragraph{Allowed combinations}

Not all combinations of input-output types, translation types and external data
types are supported (or even useful).
Table~\ref{table:extern.translation-types} describes all allowed combinations,
as well as the resulting argument type that is passed on to the external
procedure. The external data types printed in bold are the default, and can be
omitted if appropriate. Throughout the table, the data type {\tt integer} can
be replaced by any of the other integer types {\tt integer8}, {\tt integer16}
or {\tt integer32}.

\begin{aimmstable}
\tabcolsep0.5\tabcolsep
\def\carg#1{\multicolumn{1}{|c|}{{\bf #1}}}
\def\ccarg#1{\multicolumn{1}{c|}{{\bf #1}}}
\def\ctarg#1{\multicolumn{3}{|c|}{{\bf #1}}}
\def\lcarg#1{\multicolumn{1}{c|}{{\bf #1}}}
\def\bftt{\bf\tt}
\begin{tabular}{|l|l|l|l|l|}
\hline \hline
\ctarg{Allowed types}                                        &                     &                      \\
\cline{1-3}
\carg{translation}    & \ccarg{input-}& \ccarg{data}         & \lcarg{AIMMS argument} & \lcarg{Passed as}  \\
                      & \ccarg{output}&                      &                     &                      \\
\hline
{\tt scalar}          & {\tt input}   & {\tt integer}        & scalar expression   & integer              \\
                      &               & {\bftt double}       &                     & double               \\
                      &               & {\tt string}         &                     & string               \\
\cline{2-5}
                      & {\bftt inout} & {\tt integer}        & scalar reference    & integer pointer      \\
                      & {\tt output}  & {\bftt double}       &                     & double pointer       \\
                      &               & {\tt string}         &                     & string               \\
\hline
{\tt literal}         & ---           & {\tt integer}        & ---            & integer              \\
                      &               & {\bftt double}       &                     & double               \\
                      &               & {\tt string}         &                     & string               \\
\hline
{\tt card}            & ---           & ---                  & set, parameter      & integer              \\
\hline
{\tt array}           & {\tt input}   & {\tt integer}        & parameter           & integer array        \\
                      & {\bftt inout} & {\bftt double}       &                     & double  array        \\
                      & {\tt output}  &                      &                     &                    \\
\cline{3-5}
                      &               & {\bftt integer}      & element parameter   & integer array        \\
                      &               & {\tt string}         & set                 & string  array        \\
\cline{3-5}
                      &               & {\bftt string}       & string/unit parameter & string  array        \\
\hline
{\tt handle}          &{\tt input}    & ---                  & set, parameter, handle      & integer              \\
                      &{\bftt inout}  &                      &                     &                      \\
                      &{\tt output}   &                      &                     &                      \\
\hline
{\tt work}            & ---           & {\tt integer}        & integer expression  & integer array        \\
                      &               & {\bftt double}       &                     & double  array        \\
\hline\hline
\end{tabular}
\caption{Allowed combinations of translation, input-output and data
types}\label{table:extern.translation-types}
\end{aimmstable}

\paragraph{Passing array arguments}

When you are passing a multidimensional AIMMS identifier to an external
procedure or function as a {\tt array} argument, AIMMS passes a
one-dimensional buffer in which all values are stored in a manner that is
compatible with the storage of multidimensional arrays in the language which
you have specified through the {\tt Property} attribute. The precise array
numbering conventions for both {\C}-like and {\fortran} arrays are explained in
Section~\ref{sec:extern.language}.

\paragraph{Encoding of string arguments}
\AIMMSlink{string_parameter.encoding}
\declattrindex{string parameter}{Encoding}

The strings communicated with your DLL have an encoding. 
This encoding is set by the option {\tt external\_string\_character\_encoding}, which has a default of {\tt UTF8}. 
This option can be overridden by using the {\tt Encoding} attribute of string parameters, 
similar to the {\tt Encoding} attribute of a {\tt File}, see Page~\pageref{attr.file.encoding}.
On Windows, using the encoding {\tt UTF-16LE} and on Linux, using the encoding {\tt UTF-32LE}, 
the strings are passed as a {\tt wchar\_t*} array, otherwise the strings are passed as a {\tt char *} array.

\paragraph{Output string arguments}
\AIMMSlink{String.Passed.BufferSize}

When you pass a scalar or multidimensional output string argument, AIMMS
will pass a single {\tt char} buffer of fixed length, or an array of such buffers. 
The length is determined by the option {\tt external} {\tt function} {\tt string} {\tt buf} {\tt size}. 
The default of this option is 2048.
You must use the {\C} function {\tt strcpy} or a similar function to
copy the string data in your DLL to the appropriate {\tt char} buffer
associated with the output string argument.

\paragraph{Full versus sparse data transfer}
\index{external argument!full versus sparse}

When considering your options on how to pass a high-dimensional parameter to an
external procedure, you will find that passing it as an array is often not the
best solution. Not only will the memory requirements grow rapidly for
increasing dimension, but also running over all elements in the array inside
your DLL function may turn out to be a very time-consuming process. In such a
case, it is much better practice to pass the argument as an integer {\tt
handle}, and use the AIMMS API functions discussed in
Section~\ref{sec:api.value} to retrieve only the nondefault values associated
with the handle. You can then set up your own sparse data structures to deal
with high-dimensional parameters efficiently.

\paragraph{Translation modifiers \ldots}
\index{translation modifier} \index{external argument!translation modifier}

\syntaxmark{translation-modifier} In addition to the translation types,
input-output types and external data types you can specify one or more
translation modifiers for each external argument. Translation modifiers allow
you to slightly modify the manner in which AIMMS will pass the arguments to
the DLL function. AIMMS supports translation modifiers for specifying the
precise manner in which
\begin{itemize}
\item special values,
\item the data associated with handles, and
\item set elements,
\end{itemize}
are passed.

\paragraph{\ldots\ for special values}
\typindex{translation modifier}{retainspecials}

When a parameter or variable that you want to pass to an external DLL contains
special values like {\tt ZERO} or {\tt INF}, AIMMS will, by default, pass
{\tt ZERO} as 0.0, {\tt INF} and {\tt -INF} as $\pm$1.0e150, and will not pass
any of the values {\tt NA} and {\tt UNDF}. When you specify the translation
modifier {\tt retainspecials}, AIMMS will pass all special numbers by their
internal representation as a double precision floating point number. You can
use the AIMMS API functions discussed in Section~\ref{sec:api.value} to
obtain the {\tt MapVal} value (see also Table~\ref{table:expr.arith-ext})
associated with each number. The translation modifier {\tt retainspecials} can
be specified for numeric arguments that are passed either as a full array or as
an integer handle.

\paragraph{\ldots\ for handles}
\typindex{translation modifier}{raw} \typindex{translation modifier}{ordered}

When passing a multidimensional identifier handle to an external DLL, AIMMS
can provide several methods of access to the data associated with the handle by
specifying one of the following translation modifiers:
\begin{itemize}
\item {\tt ordered}: the data retrieval functions will pass the data values
according to the particular ordering imposed any of the domain sets of the
identifier associated with the handle. By default, AIMMS will use the
natural ordering determined by the data entry order of all domain sets.
\item {\tt raw}: the data retrieval functions will also pass inactive
data (see also Section~\ref{sec:data.control}). By default, AIMMS will not
pass inactive data.
\end{itemize}
The details of ordered versus unordered and raw data transfer are discussed in
full detail in Section~\ref{sec:api.value}.

\paragraph{\ldots\ for set elements}
\typindex{translation modifier}{string} \typindex{translation
modifier}{integer} \typindex{translation modifier}{ordinalnumber}
\typindex{translation modifier}{indicator} \typindex{translation
modifier}{elementnumber}

AIMMS can pass set elements (in the context of element parameters and sets)
to external procedures in various manners. More specifically, set elements can
be translated into:
\begin{itemize}
\item an {\tt integer} external data type, or
\item a {\tt string} external data type.
\end{itemize}
When the external data type is {\tt string}, AIMMS will pass the element
name for each set element. Transfer of element names is always input only. In
general, when the external data type is {\tt integer}, AIMMS can pass either
\begin{itemize}
\item the ordinal number with respect to its associated subset domain
({\tt ordi\-nal\-number} modifier), or
\item the element number with respect to its associated root set ({\tt
elementnumber} modifier).
\end{itemize}
Alternatively, when set elements are passed in the context of a set you can
specify the {\tt indicator} modifier in combination with the {\tt integer}
external data type. This will result in the transfer of a multidimensional
binary parameter which indicates whether a particular tuple is or is not
contained in the set.

\paragraph{Passing element parameters}

When you pass an element parameter as an integer {\tt scalar} or {\tt array}
argument, AIMMS will assume the {\tt ordinalnumber} modifier by default.
When passed as integer, element parameters can be input, output or inout
arguments. When element parameters are passed as string arguments, they can be
input only.

\paragraph{When to use}

Element numbers and ordinal numbers each can have their use within an DLL
function. Element numbers remain identical throughout a modeling session using
a single data set, regardless of addition and deletion of set elements, or any
change in set ordering. For this reason, it is best to use element numbers when
the set elements need to be used in multiple calls of the DLL function. Ordinal
numbers, on the other hand, are the most convenient means for passing
permutations that are used within the current external call only. With it, you
can directly access a permuted reference in other array arguments.

\paragraph{Passing set arguments}
\index{external argument!set}

Sets can be passed as {\tt array} arguments to an external DLL function. When
passing set arguments, you have to make a distinction between one-dimensional
root sets, one-dimensional subsets (both either simple or relation), and
multidimensional subsets and indexed sets. The following rules apply.

\paragraph{Pass as one- dimensional array}

One-dimensional root sets and subsets can be passed as a one-dimensional array
of length equal to the cardinality of the set. To accomplish this, you can must
pass such a set as
\begin{itemize}
\item an array of {\tt integer} numbers, representing either the
ordinal or element numbers of each element in the set (using the {\tt
ordinalnumber} or {\tt element\-number} modifier), or
\item a {\tt string} array, representing the names of all elements in
the set.
\end{itemize}
One-dimensional set arguments passed in this manner can only be input
arguments. As a specific consequence, you cannot modify the contents of root
sets passed as array arguments.

\paragraph{Pass as indicator parameter}

You can pass any subset (whether it is simple, relation or indexed) as a
multidimensional {\tt integer indicator} array defined over its respective
domain sets, indicating whether a particular tuple of domain set elements is
contained in the subset (value equals 1) or not (value equals 0). The dimension
of such indicator parameters is given by the following set of rules:
\begin{itemize}
\item the dimension for a {\em simple subset} is 1,
\item the dimension for a multidimensional relation is the dimension of the Cartesian product of which
the set is a subset,
\item the dimension of an {\em indexed set} is the dimension of
the index domain of the set plus 1.
\end{itemize}
Set arguments passed as an {\tt indicator} argument can be of input, output, or
inout type. In the latter two cases modifications to the 0-1 values of the
indicator parameter are translated back into the corresponding element
memberships of the subset.

\paragraph{Set argument defaults}

When you pass set arguments to an external DLL, AIMMS will assume no default
translation methods when the set is passed as an {\tt integer} array, as each
type of set does not allow every translation method. For integer set arguments
you should therefore always specify one of the translation modifiers {\tt
ordinalnumber}, {\tt elementnumber} or {\tt indicator}.

\paragraph{Passing set handles}

Sets can also be passed by an integer handle. AIMMS offers various API
functions (see also Section~\ref{sec:api.attribute}) to obtain information
about the domain of the set, its cardinality and elements, and to add or remove
elements to the set.


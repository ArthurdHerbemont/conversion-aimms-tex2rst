\section{Flow control statements}\label{sec:exec.flow}

\paragraph{Six forms of flow control}
\index{flow control statement} \index{statement!flow control}

Execution statements such as assignment statements, \verb|SOLVE| statements or
data management statements are normally executed in their order of appearance
in the body of a procedure. However, the presence of control flow statements
can redirect the flow of execution as the need arises. AIMMS provides six
forms of flow control:
\begin{itemize}
\item the \verb|IF-THEN-ELSE| statement for conditional execution,
\item the \verb|WHILE| statement for repetitive conditional execution,
\item the \verb|REPEAT| statement for repetitive unconditional execution,
\item the \verb|FOR| statement for repetitive domain-driven execution,
\item the \verb|SWITCH| statement for branching on set and integer values,
\item the \verb|HALT| and {\tt RETURN} statement for terminating the
current execution,
\item the {\tt SKIP} and {\tt BREAK} statements for terminating the
current repetitive execution, and
\item the {\tt BLOCK} statement for visually grouping together multiple statements.
\end{itemize}

\paragraph[.7]{Syntax}
\begin{syntax}
\syntaxdiagram{flow-control-statement}{flow-control-stmt}
\end{syntax}

\paragraph{Flow control statements and special numbers}

In the condition of flow control
statements such as {\tt IF-THEN-ELSE}, {\tt WHILE} and {\tt REPEAT} it is needed to know
whether the result is equal to {\tt 0.0} or not in order to
take the appropriate branch of execution. The special number {\tt NA} has the interpretation
``not yet available'' thus it is also not yet known whether it is equal to 0.0 or not.
The special number {\tt UNDF} is the result
of an illegal operation, so its value cannot be known. Therefor, AIMMS will issue an error message if
the result of a condition in these statements evaluates to {\tt NA} or {\tt UNDF}.
Special numbers and their interpretation
as logical values are discussed in full detail in Sections~\ref{sec:expr.num.arith-ext}
and~\ref{sec:expr.logic}.


\subsection{The \tttext{IF-THEN-ELSE} statement}\label{sec:exec.flow.if}\fcstatindex{IF-THEN-ELSE}
\AIMMSlink{if} \AIMMSlink{then} \AIMMSlink{else} \AIMMSlink{endif}

The conditional \verb|IF-THEN-ELSE| statement is used to choose between the
execution of several groups of statements depending on the outcome of one or
more logical conditions. The syntax of the \verb|IF-THEN-ELSE| statement is
given in the following diagram.

\paragraph{Syntax}

\begin{syntax}
\syntaxdiagram{if-then-else-statement}{if-then-else-stmt}
\end{syntax}
AIMMS will evaluate all logical conditions in succession and stops at the
first condition that is satisfied. The statements associated with that
particular branch are executed. If none of the conditions is satisfied, the
statements of the {\tt ELSE} branch, if present, will be executed.

\paragraph{Example}
The following code illustrates the use of the \verb|IF-THEN-ELSE| statement.
\begin{example}
    if ( not SupplyDepot ) then
        DialogMessage( "Select a supply depot before solving the model" );
    elseif ( Exists[ p, Supply(SupplyDepot,p) < Sum( i, Demand(i,p) ) ] ) then
        DialogMessage( "The selected supply depot has insufficient capacity" );
    else
        solve TransportModel ;
    endif ;
\end{example}
Note that in this particular example the evaluation of the {\tt ELSEIF}
condition only makes sense when a {\tt SupplyDepot} exists. This is
automatically enforced because the {\tt IF} condition is not satisfied.
Similarly, successful execution of the {\tt ELSE} branch apparently depends on
the failure of both the {\tt IF} and {\tt ELSEIF} conditions.

\subsection{The \tttext{WHILE} and \tttext{REPEAT}
statements}\label{sec:exec.flow.while-repeat}\fcstatindex{WHILE}\fcstatindex{REPEAT}
\AIMMSlink{while} \AIMMSlink{endwhile} \AIMMSlink{repeat} \AIMMSlink{endrepeat}

The {\tt WHILE} and {\tt REPEAT} statements group a series of execution
statements and execute them repeatedly. The execution of the repetitive loop
can be terminated by a logical condition that is part of the {\tt WHILE}
statement, or by means of a {\tt BREAK} statement from within both the {\tt
WHILE} and {\tt REPEAT} statements.

\paragraph{Syntax}
\begin{syntax}
\syntaxdiagram{while-statement}{while} \syntaxdiagram{repeat-statement}{repeat}
\end{syntax}
\vskip\baselineskip\noindent Loop strings are discussed in
Section~\ref{sec:exec.flow.while-repeat-adv}.

\paragraph{Termination by {\tt WHILE} condition}

The execution of a {\tt WHILE} statement is subject to a logical condition that
is verified each time the statements in the loop are executed. If the condition
is false initially, the statements in the loop will never be executed. In case
the {\tt WHILE} loop does not contain a {\tt BREAK}, {\tt HALT} or {\tt RETURN}
statement, the statements inside the loop must in some way influence the
outcome of the logical condition for the loop to terminate.

\paragraph{Termination by a {\tt BREAK} statement}\fcstatindex{BREAK}
\AIMMSlink{break}

An alternative way to terminate a {\tt WHILE} or {\tt REPEAT} statement is the
use of a {\tt BREAK} statement inside the loop. {\tt BREAK} statements make it
possible to abort the execution at any position inside the loop. This freedom
allows you to formulate more natural termination conditions than would
otherwise be possible with just the logical condition in the {\tt WHILE}
statement.  After aborting the loop, AIMMS will continue with the first
statement following it.

\paragraph{Skipping the remainder of a loop}\fcstatindex{SKIP}
\AIMMSlink{skip}

In addition to the {\tt BREAK} statement, AIMMS also offers a {\tt SKIP}
statement. With it you instruct AIMMS to skip the remaining statements
inside the current iteration of the loop, and immediately return to the top of
the {\tt WHILE} or {\tt REPEAT} statement to execute the next iteration. The
{\tt SKIP} statement is an elegant alternative to placing the statements inside
the loop following the {\tt SKIP} statement in a conditional {\tt IF}
statement.

\paragraph{Syntax}
\begin{syntax}
\syntaxdiagram{skip-break-statement}{skip}
\end{syntax}

\paragraph{The {\tt WHEN} clause}
\subtypindex{statement}{BREAK}{WHEN clause@{{\tt WHEN} clause}}
\subtypindex{statement}{SKIP}{WHEN clause@{{\tt WHEN} clause}}
\typindex{clause}{WHEN} \AIMMSlink{when}

By adding a {\tt WHEN} clause to either a {\tt BREAK} or {\tt SKIP} statement,
you make its execution conditional to a logical expression. In practice, the
execution of a {\tt BREAK} or {\tt SKIP} statement is almost always subject to
some condition.

\paragraph{Example {\tt WHILE} statement}

This example computes the {\em machine epsilon}, which is the smallest number
that, when added to 1.0, gives a value different from 1.0. It is a measure of
the accuracy of the floating point arithmetic, and it is machine dependent. We
assume that {\tt meps} is a scalar parameter, and that the numeric comparison
tolerances are set to zero (see also Section~\ref{sec:expr.logic.num-rel}).
\begin{example}
    meps := 1.0;

    while (1.0 + meps/2 > 1.0) do
        meps /= 2;
    endwhile;
\end{example}
Since the parameter {\tt meps} is determined iteratively, and the loop
condition will eventually be satisfied, this example illustrates an appropriate
use of the \verb|WHILE| loop.

\paragraph{Example {\tt REPEAT} statement}

By applying a {\tt BREAK} statement, the machine epsilon can be computed
equivalently using the following {\tt REPEAT} statement.
\begin{example}
    meps := 1.0;

    repeat
        break when (1.0 + meps/2 = 1.0) ;
        meps /= 2;
    endrepeat;
\end{example}
The {\tt BREAK} statement could also have been formulated in an equivalent but
less elegant manner without a {\tt WHEN} clause:
\begin{example}
    if (1.0 + meps/2 = 1.0) then
       break;
    endif;
\end{example}

\subsection{Advanced use of \tttext{WHILE} and \tttext{REPEAT}}%
\label{sec:exec.flow.while-repeat-adv}

\paragraph{Advanced uses}

Next to the common use of the {\tt WHILE} and {\tt REPEAT} statements described
in the previous section, AIMMS offers some special constructs that help you
\begin{itemize}
\item keep track of the number executed iterations automatically, and
\item control nested arrangements of {\tt WHILE} and {\tt REPEAT} statements.
\end{itemize}

\paragraph{Nonconvergent loops}

There are practical examples in which the terminating condition of a repetitive
statement may not be met at all or at least not within a reasonable amount of
work or time.  A good example of this behavior are solution algorithms for
which convergence is likely but not guaranteed. In these cases, it is common
practice to terminate the execution of the loop when the total number of
iterations exceeds a certain limit.

\tipparagraph{The {\tt LoopCount} operator}{loopcount} \operindex{LoopCount}
\AIMMSlink{loopcount}

In AIMMS, such conditions can be formulated easily without the need to
\begin{itemize}
\item introduce an additional parameter,
\item add a statement to initialize it, and
\item increase the parameter every iteration of the loop.
\end{itemize}
Each repetitive statement keeps track of its iteration count automatically and
makes the number of times the loop is entered available by means of the
predefined operator {\tt LoopCount}. Upon entering a repetitive statement
AIMMS will set its value to 1, and will increase it by 1 at the end of every
iteration.

\paragraph{Example}

Whether the following sequence will converge depends on the initial value
of~{\tt x}. In the case where there is no convergence or if convergence is too
slow, the loop in the following example will terminate after 100 iterations.
\begin{example}
    while ( Abs(x-OldValue) >= Tolerance and LoopCount <= 100 ) do
        OldValue := x ;
        x        := x^2 - x ;
    endwhile ;
\end{example}

\paragraph{Naming nested loops}
\subtypindex{statement}{FOR}{loop string} \subtypindex{statement}{WHILE}{loop
string} \subtypindex{statement}{REPEAT}{loop string} \index{loop string}

So far, we have considered single loops. However, in practice it is quite
common that repetitive statements appear in nested arrangements. To provide
finer control over the flow of execution in such situations, AIMMS allows
you to label a particular repetitive statement with a {\em loop string}.

\paragraph{Use of loop strings}

\syntaxmark{loop-string} Using a loop string in conjunction with the {\tt
BREAK} and {\tt SKIP} statements, it is possible to break out from several
nested repetitive statements with a single {\tt BREAK} statement. The loop
string argument can also be supplied to the {\tt LoopCount} operator so the
break can be conditional on the number of iterations of any loop. Without
specifying a loop string, {\tt BREAK}, {\tt SKIP} and {\tt LoopCount} refer to
the current loop by default.

\paragraph{Example}
The following example illustrates the use of loop strings and the {\tt
LoopCount} operator in nested repetitive statements. It outlines an algorithm
in which the domain of definition of a particular problem is extended in every
loop based on the current solution, after which the new problem is solved by
means of a sequential solution process.
\begin{example}
    repeat "OuterLoop"
         ... ! Determine initial settings for sequential solution process

        while( Abs( Solution - OldSolution ) <= Tolerance ) do
            OldSolution := Solution ;

            ... ! Set up and solve next sequential step ...

            ! ... but terminate algorithm when convergence is too slow
            break "OuterLoop" when LoopCount >= LoopCount("OuterLoop")^2 ;
        endwhile;

        ... ! Extend the domain of definition based on current solution,
            ! or break from the loop when no extension is possible anymore.
    endrepeat;
\end{example}

\subsection{The \tttext{FOR} statement}\label{sec:exec.flow.for}\fcstatindex{FOR}
\AIMMSlink{for} \AIMMSlink{endfor}

The \verb|FOR| statement is related to the use of iterative operators in
expressions. An iterative operator such as {\tt SUM} or {\tt MIN} applies a
particular operation to all expressions defined over a particular domain.
Similarly, the {\tt FOR} statement executes a group of execution statements for
all elements in its domain. The syntax of the {\tt FOR} statement is given in
the following diagram.

\paragraph{Syntax}

\begin{syntax}
\syntaxdiagram{for-statement}{for}
\end{syntax}

\paragraph{Execution is sequential}

The binding domain of a {\tt FOR} statement can only contain free indices,
which are then bound by the statement. All statements inside a {\tt FOR}
statement are executed in sequence for the specific elements in the binding
domain. Unless specified otherwise, the ordering of elements in the binding
domain, and hence the execution order of the {\tt FOR} statement, is the same
as the order of the corresponding binding set(s).

\paragraph{Integer domains}
\subtypindex{statement}{FOR}{integer domain}


{\tt FOR} statements with an integer domain in the form of an enumerated set
behave in a similar manner as the {\tt FOR} statement in programming languages
like C or Pascal. Like the example below, {\tt FOR} statements of this type are
mostly of an algorithmic nature, and the indices bound by the {\tt FOR}
statement typically serve as an iteration count.

\paragraph{Example}
\begin{example}
    for ( n in { 1 .. MaxPriority } ) do

        x.NonVar( i | x.Priority(i) < n ) := 1;
        x.Relax ( i | x.Priority(i) = n ) := 0;
        x.Relax ( i | x.Priority(i) > n ) := 1;

        Solve IntegerModel;
    endfor;
\end{example}
This example tries to solve a mixed-integer mathematical program heuristically
in stages. The algorithm first only solves for those integer variables that
have a particular integer priority, and then changes them to non-variables
before going on to the next priority. The suffices used in this example are
discussed in Section~\ref{sec:var.var}.

\paragraph{Non-integer domains}
\subtypindex{statement}{FOR}{non-integer domain}

{\tt FOR} statements with non-integer binding domains are typically used to
process the data of a model for all elements in a data-related domain. The use
of a {\tt FOR} statement in such a situation is only necessary if the
statements inside it form a unit, for which sequential execution for each
element in the domain of the entire group of statements is essential. An
example follows.

\paragraph{Example}

\begin{example}
    for ( i in Cities ) do
        SmallestTransportCity := ArgMin( j, Transport(i,j) ) ;
        DiscardedTransports   += Transport( i, SmallestTransportCity ) ;
        Transport( i, SmallestTransportCity ) := 0 ;
    endfor;
\end{example}
In this example the three assignments form an inseparable unit. For each
particular value of {\tt i}, the second and third assignment depend on the
correct value of {\tt Smallest\-Transport} in the first assignment.

\paragraph{Use {\tt FOR} only when needed}
\index{assignment!versus FOR statement@versus {\tt FOR} statement}
\subtypindex{statement}{FOR}{versus assignment}

If you are familiar with programming language like {\sc Pascal} and {\sc C},
then the use of \verb|FOR| statements will seem quite natural. In AIMMS,
however, \verb|FOR| statements are often not needed, especially in the context
of indexed assignments. Indexed assignments bind the free indices in their
domain implicitly, resulting in sequential execution of that particular
assignment for all elements in its domain. In general, such an index binding
assignment is executed much more efficiently than the same assignment placed
inside an equivalent {\tt FOR} statement. In general, you should use \verb|FOR|
statements only when really necessary.

\paragraph{AIMMS issues a warning}

AIMMS will provide a warning when it detects unnecessary {\tt FOR}
statements in your model. Typically {\tt FOR} statement are not required when
the loop only contains assignments that do not refer to scalar identifiers
(either numeric or element-valued) to which assignments have been made inside
the loop as well. For instance, in the last example the {\tt FOR} statement is
essential, because the assignment and use of the element parameter {\tt
LargestTransportCity} is inside the loop.

\paragraph{Example}
The following example shows an unnecessary use of the {\tt FOR} statement.
\begin{example}
    solve OptimizationModel;

    ! Mark variables with large marginal values
    for (i) do
        if ( Abs[x.Marginal(i)] > HighPrice ) then
            Mark(i) := x.Marginal(i);
        else
            Mark(i) := 0.0;
        endif;
    endfor;
\end{example}
While this statement may seem very natural to C or Pascal programmers, in a
sparse execution language like AIMMS it should preferably be written by the
following simpler, and faster, assignment statement.
\begin{example}
    Mark(i) := x.Marginal(i) OnlyIf ( Abs[x.Marginal(i)] > HighPrice );
\end{example}

\paragraph{The {\tt SPARSE}, {\tt ORDERED} and {\tt UNORDERED}
keywords} \keyindex{SPARSE} \keyindex{ORDERED} \keyindex{UNORDERED}
\AIMMSlink{sparse} \AIMMSlink{ordered} \AIMMSlink{unordered}

With the optional keywords {\tt SPARSE}, {\tt ORDERED} and {\tt UNORDERED} you
can indicate that AIMMS should follow one of three possible strategies to
execute the {\tt FOR} statement. If you do not explicitly specify a strategy,
AIMMS will follow the {\tt SPARSE} strategy by default, and issue a warning
when this strategy leads to severe inefficiencies. You can find an explanation
of each of the strategies, as well as a description of the cases in which you
may want to choose a specific strategy in Section~\ref{sec:eff.set.for}.

\paragraph{{\tt FOR} as a repetitive statement}

Like the {\tt WHILE} and the {\tt REPEAT} statements, {\tt FOR} is a repetitive
statement. Thus, you can use the {\tt SKIP} and {\tt BREAK} statements and the
{\tt LoopCount} operator. In addition, you can identify a {\tt FOR} statement
with a loop string thereby controlling execution in nested arrangements as
discussed in the previous section.

\paragraph{Use of {\tt SKIP}\\ and {\tt BREAK}}\fcstatindex{BREAK}\fcstatindex{SKIP}

The {\tt SKIP} statement skips the remaining statements in the {\tt FOR} loop
and continues to execute the loop for the next element in the binding domain.
The {\tt BREAK} statement will abort the execution of the {\tt FOR} statement
all together.

\subsection{The \tttext{SWITCH} statement}\label{sec:exec.flow.switch}\fcstatindex{SWITCH}
\AIMMSlink{switch} \AIMMSlink{endswitch}

\paragraph{The {\tt SWITCH} statement}

The {\tt SWITCH} statement is used to choose between the execution of different
groups of statements depending on the value of a scalar parameter reference.
The syntax of the {\tt SWITCH} statement is given in the following two
diagrams.

\paragraph[.8]{Syntax}
\begin{syntax}
\syntaxdiagram{switch-statement}{switch} \syntaxdiagram{selector}{selector}
\end{syntax}

\paragraph{Integers and set element}

The {\tt SWITCH} statement can switch on two types of scalar parameter
references: set element-valued or integer-valued. When you try to switch on
references to string-valued or non-integer numerical parameters, AIMMS will
issue a compile time error

\paragraph{Switch selectors}

Each selector in a {\tt SWITCH} statement must be a comma-separated list of
values or value ranges, matching the type of the selecting scalar parameter.
Expressions and ranges used in a {\tt SWITCH} statement must only contain
constant integers and set elements. Set elements used in a switch selector must
be known at compile time, i.e.\ the data initialization of the corresponding
set must be a part of the model description.

\paragraph{The {\tt DEFAULT} selector last}
\subtypindex{statement}{SWITCH}{DEFAULT selector@{{\tt DEFAULT} selector}}
\index{DEFAULT selector@{\tt DEFAULT} selector}

The optional {\tt DEFAULT} selector matches every reference. Since AIMMS
executes only those statements associated with the {\em first} selector
matching the value of the scalar reference, it is clear that the {\tt DEFAULT}
selector should be placed last.

\paragraph{Example}
The following {\tt SWITCH} statement takes different actions based on the model
status returned by a {\tt SOLVE} statement.
\begin{example}
    solve OptimizationModel;

    switch OptimizationModel.ProgramStatus do
        'Optimal', 'LocallyOptimal' :
                ObservedModelStatus := 'Solved' ;

        'Unbounded', 'Infeasible', 'IntegerInfeasible', 'LocallyInfeasible' :
                ObservedModelStatus := 'Infeasible' ;

        'IntermediateInfeasible', 'IntermediateNonInteger', 'IntermediateNonOptimal' :
                ObservedModelStatus := 'Interrupted' ;

        default :
                ObservedModelStatus := 'Not solved' ;
    endswitch ;
\end{example}

\subsection{The \tttext{HALT} statement}\label{sec:exec.flow.halt}
\fcstatindex{HALT} \AIMMSlink{halt}

\paragraph{Terminating execution}

With a {\tt HALT} statement you can stop the current execution. You can use it,
for example, if your model has run into an unrecoverable error condition during
its execution, or if you simply want to skip the remaining statements because
they are no longer relevant in a particular situation.

\paragraph{Compare to {\tt RETURN}}
\subtypindex{statement}{HALT}{versus RETURN statement@{versus {\tt RETURN}
statement}} \fcstatindex{RETURN}

Instead of the {\tt HALT} statement you can also use the {\tt RETURN} statement
(see also Section~\ref{sec:intern.proc}) to terminate the current execution.
The {\tt HALT} statement directly jumps back to the user interface, but a {\tt
RETURN} statement in a procedure only passes back control to the calling
procedure and continues execution from there.

\paragraph{Syntax}
The syntax of the {\tt HALT} statement follows. \vskip\medskipamount
\begin{syntax}
\syntaxdiagram{halt-statement}{exit}
\end{syntax}

\paragraph{Printing a message}

You can optionally specify a string in the {\tt HALT} statement that will be
printed in a message dialog box when execution has stopped. This is useful, for
instance, to pass on an appropriate message to the user when a particular error
condition has occurred.

\paragraph{The {\tt WHEN} clause}
\typindex{clause}{WHEN} \subtypindex{statement}{HALT}{WHEN clause@{{\tt WHEN}
clause}}

You can make the execution of the {\tt HALT} statement conditional on a {\tt
WHEN} clause. If present, the current run will only be aborted if the condition
after the {\tt WHEN} clause is satisfied.

\paragraph{Example}
The following example terminates the current run if the {\tt SOLVE} statement
does not solve to optimality. When aborting, the user will be notified with an
explanatory message.
\begin{example}
     solve LinearOptimizationModel;

     halt with "Execution aborted: model not solved to optimality"
          when OptimizationModel.ProgramStatus <> 'Optimal' ;
\end{example}

\paragraph{Alternative}
Note that the type of model termination initiated by calling the {\tt HALT} statement cannot be guarded against using
AIMMS' error handling facilities (see Section~\ref{sec:exec.error}).  An alternative to the {\tt HALT} statement,
which enables error handling, is the {\tt RAISE} statement discussed in Section~\ref{sec:exec.error.raising}.  When 
you want to let the {\tt HALT} act as a {\tt RAISE} statement, you can switch the option 
{\tt halt\_acts\_as\_raise\_error} on.


\subsection{The \tttext{BLOCK} statement}
\label{sec:exec.flow.block}\fcstatindex{BLOCK} \AIMMSlink{block}\AIMMSlink{endblock}

\paragraph{The {\tt BLOCK} statement}

A sequence of statements can be grouped together into a single statement
using the {\tt BLOCK} statement, possibly serving one or more of the
following purposes:
\begin{itemize}
\item to emphasize the logical structure of the model,
\item to execute a group of statements with different option settings, or
\item to permit error handling on a group of statements (see Section~\ref{sec:exec.error}).
\end{itemize}
The syntax of the {\tt BLOCK} statement is as follows.

\paragraph{Syntax}
\begin{syntax}
\syntaxdiagram{block-statement}{block}
\end{syntax}

\paragraph{Emphasizing logical structure in the model}

Consider the following {\tt BLOCK} statement containing a group of statements.
\begin{example}
    block ! Initialize measured compositions as observable.
        CompositionObservable(nmf,c in MeasuredComponents(nmf)) := 1;
        CompositionObservable(mf,mc) := 0;

        if ( not CheckComputableFlows ) then
            UnobservableComposition(nmf,c) := 1$(not CompositionObservable(nmf,c));
            return 0;
        endif;

        CompositionCount(pu,c) :=
            Count((f,g) | Admissable(pu,c,f,g) and CompositionObservable(g,c));
        NewCount := Card ( CompositionObservable );
    endblock ;
\end{example}
In the AIMMS syntax editor, a block can be displayed in either a
collapsed or an expanded state. When collapsed, the block will be displayed
as follows, using a single line comment following the {\tt BLOCK}
keyword as its description.
\begin{figure}[H]
\hskip.75cm\vbox{\hbox{\guidefig{collapsed-block}}}
\end{figure}
When in a collapsed state, AIMMS will show the contents of the
block in a tooltip if the mouse pointer is placed over the collapsed
block, as illustrated in the figure below.
\begin{figure}[H]
\hskip.75cm\vbox{\hbox{\guidefig{collapsed-block-tooltip}}}
\end{figure}

\paragraph{Executing with different option settings}
During the execution of a block statement, the options in the
{\tt WHERE} clause will have the specified values set
at the beginning of the block statement, and the
old values restored at the end of the block statement.
More on the format of option names and value settings can be
found in Section~\ref{sec:exec.option}.
The example below prints various parameters using various settings
of the option {\tt Listing\_number\_precision}.
\begin{example}
    ! The default value of the option Listing_number_precision is 3.
    block ! Start printing numbers using 6 decimals.
        where Listing_number_precision := 6 ;

        display A, B ;

        block ! Start printing numbers without decimals.
            where Listing_number_precision := 0 ;
            display C, D ; ! The output looks as if C and D are integers.
        endblock ;

        display E, F ; ! Back to printing numbers using 6 decimals.

    endblock ;

    display G, H ; ! Back to printing numbers using 3 decimals.
\end{example}
In the above example, a nested block statement is used to set the scope of option
settings; the inner block statement temporarily overrides
the option setting of the outer block statement, which overrides
the global option settings.

\paragraph{The {\tt OnError} clause}

The {\tt OnError} clause is one of the means of handling runtime errors in AIMMS. It
is discussed in detail in Section~\ref{sec:exec.error.handling}.


\section{The \tttext{OPTION} and \tttext{PROPERTY} statements}
\label{sec:exec.option}\label{sec:exec.property}\statindex{OPTION}\statindex{PROPERTY}
\AIMMSlink{option} \AIMMSlink{property}

\paragraph{Options}

\syntaxmark{option} Options are directives to AIMMS or to the solvers to
execute a task in a particular manner. Options have a name and can assume a
value that is either numeric or string-valued. You can modify the value of an
option from within the graphical interface. The assigned value is stored along
with the project. All global options are set to their stored values at the
beginning of each session. During execution you can change option settings
using the {\tt OPTION} statement.

\paragraph{Syntax}
\begin{syntax}
\syntaxdiagram{option-statement}{option-stmt}
\end{syntax}
\noindent You can find a complete list of global options for AIMMS and its
solvers in the help system.

\paragraph{Option values}

The right-hand side of an {\tt OPTION} statement must be a scalar expression of
the proper type. If the option expects a string value, AIMMS will accept
both string- or element-valued expressions. An example follows.
\begin{example}
    option  Bound_Tolerance  := 1.0e-6,
            Iteration_Limit  := UserSettings('IterationLimit');
\end{example}

\paragraph{Solver options}

Some solver options are available for more than one solver. If you modify such
a solver option per se, AIMMS will modify the option for all solver that
support it. If you want to restrict the change to only a single solver, you can
prefix the option name by the name of the solver followed by a dot ``{\tt .}'',
as illustrated in the example below.
\begin{example}
    option  'Cplex 12.9'.lp_method := 'dual simplex';
\end{example}
This statement will set the option {\tt lp\_method} of the solver that is known
to the system as {\tt 'Cplex 12.9'} equal to {\tt 'dual simplex'}. The solver name can be either a
quoted solver name, or an element parameter into the predefined set {\tt
AllSolvers}.

\paragraph{Identifier properties}

Identifier properties can be turned {\tt on} or {\tt off}. All properties
default to {\tt off}, unless they are turned on---either in the declaration of
the identifier or in a {\tt PROPERTY} statement. During the execution of your
model you can dynamically change the default values of properties through the
{\tt PROPERTY} execution statements.

\paragraph{Syntax}
\begin{syntax}
\syntaxdiagram{property-statement}{prop-stmt}
\end{syntax}

\paragraph{Resetting properties}

The properties of all identifier types can be found in the identifier
declaration sections. Not all property settings can be changed, e.g.\ you
cannot dynamically change the {\tt Input} or {\tt Output} property of arguments
of functions and procedures. In such cases, AIMMS will produce a runtime
error. An example of the {\tt PROPERTY} statement follows.
\begin{example}
    if ( Card(Cities) > 100 ) then
       property IntermediateTransport.NoSave := on;
    endif;
\end{example}
Once the set of {\tt Cities} contains more than 100 elements, the identifier
{\tt Interme\-diateTransport} is no longer saved as part of a case file.

\paragraph{Multiple identifiers}

When the {\tt PROPERTY} statement is applied to an index into a subset of the
predefined set {\tt AllIdentifiers}, AIMMS will change the corresponding
property for all identifiers in that subset.

\paragraph{Example}

The following example illustrates how the {\tt PROPERTY} statement can be used
to obtain additional sensitivity data for a set {\tt SensitivityVariables} of
(symbolic) variables that has been previously determined.
\begin{example}
    for ( var in SensitivityVariables ) do
        property var.CoefficientRanges := on;
    endfor;
\end{example}
Here, you request AIMMS to determine the smallest and largest values for the
objective coefficient of each variable in {\tt SensitivityVariables} during the
execution of a {\tt SOLVE} statement such that the optimal basis remains
constant (see also Section~\ref{sec:var.properties}).

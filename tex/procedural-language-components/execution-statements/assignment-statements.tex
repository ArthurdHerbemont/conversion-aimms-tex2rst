\section{Assignment statements}\label{sec:exec.assign}
\index{assignment} \index{statement!assignment}

\paragraph{Assignment}

Assignment statements are used to set or change the values of sets, parameters
and variables during the execution of a procedure or a function. The syntax of
an assignment statement is straightforward.

\paragraph{Syntax}
\begin{syntax}
\syntaxdiagram{assignment-statement}{assignment}
\syntaxdiagram{data-selection}{data-selection}
\end{syntax}

\paragraph{Assignment operators}
\index{assignment!operator} \operindex{:=} \operindex{+=} \operindex{-=}
\operindex{*=} \operindex{/=} \operindex{\char`\^=}

AIMMS offers several \syntaxmark{assignment-operator} assignment operators.
The standard {\em replacement} assignment operator {\tt :=} replaces the value
of all elements specified on the left hand side with the value of the
expression on the right hand side. The {\em arithmetic} assignment operators
{\tt +=}, {\tt -=}, {\tt *=}, {\tt /=} and \verb|^=| combine an assignment with
an arithmetic operation. Thus, the assignments
\begin{quote}
\em {\tt a += b}, \qquad {\tt a -= b}, \qquad {\tt a *= b}, \qquad {\tt a /=
b}, \qquad {\tt a \verb|^|= b}
\end{quote}
form a shorthand notation for the assignments
\begin{quote}
\em {\tt a := a + b}, \quad {\tt a := a - b}, \quad {\tt a := a * b}, \quad
{\tt a := a / b}, \quad \verb|a := a ^ b|.
\end{quote}


\paragraph{Index binding}
\index{index binding!assignment} \index{assignment!index binding}

Assignment is an {\em index binding} statement. AIMMS also binds unbound
indices in (nested) references to element-valued parameters that are used for
indexing the left-hand side. AIMMS will execute the assignment repeatedly
for {\em all} elements in the binding domain, and in the order as specified by
the declaration(s) of the binding set(s). The precise rules for index binding
are explained in Section~\ref{sec:bind.rules}.

\paragraph{Allowed binding domains}
\index{domain!binding} \index{binding domain!assignment}
\index{assignment!binding domain}

In contrast to the binding domain of iterative operators and the {\tt FOR}
statements, the binding domain of an indexed assignment can contain the full
range of element expressions:
\begin{itemize}
\item references to unbound indices, which will be bound by the assignment,
\item references to scalar element parameters and bound indices,
\item references to indexed element parameters, for which any nested
unbound index will be bound as well,
\item calls to element-valued functions, and
\item element-valued iterative operators.
\end{itemize}
If the element expression inside the binding domain of an indexed assignment is
too lengthy, it may be better to use an intermediate element parameter to
improve readability.

\paragraph{Conditional assignments}
\index{assignment!conditional}

Like any binding domain, the binding domain of an indexed assignment can be
subject to a logical condition. Such an assignment is referred to as a {\em
conditional assignment}, and is only executed for those elements in the binding
domain that satisfy the logical condition.

\paragraph{Domain checking}
\index{domain!condition}

In addition, if the identifier on the left-hand side of the assignment has its
own domain restriction, then the assignment is limited to those elements of the
binding domain that satisfy this restriction. Assignments to elements outside
the restricted domain are not considered.

\paragraph{Example}

The following five examples illustrate some simple assignment statements. In
all examples we assume that {\tt i} and {\tt j} are unbound indices into a set
{\tt Cities}, and that {\tt LargestCity} is an element parameter into {\tt
Cities}.
\begin{enumerate}
\item
The first example illustrates a simple {\em scalar assignment}.
\begin{example}
    TotalTransportCost := sum[(i,j), UnitTransportCost(i,j)*Transport(i,j)];
\end{example}
The value of the scalar identifier on the left-hand side is replaced with the
value of the expression on the right-hand side.
\item
The second example illustrates an {\em index binding assignment}.
\begin{example}
    UnitTransportCost(i,j) *= CostWeightFactor(i,j) ;
\end{example}
For {\em all} cities {\tt i} and {\tt j} in the index domain of {\tt
UnitTransportCost} , the old values of the identifier {\tt
UnitTransportCost(i,j)} are multiplied with the values of the identifier {\tt
CostWeightFactor(i,j)} and then used to replace the old values.
\item The third example illustrates a {\em conditional assignment}.
\begin{example}
    Transport((i,j) | UnitTransportCost(i,j) > 100) := 0;
\end{example}
The zero assignment to {\tt Transport} is made to only those cities {\tt i} and
{\tt j} for which the {\tt UnitTransportCost} is too high.
\item The fourth example illustrates a {\em sliced assignment}, i.e.\
an assignment that only changes the values of a lower-dimensional subspace of
the index domain of the left-hand side identifier.
\begin{example}
    Transport(LargestCity,j) := 0;
\end{example}
The sliced assignment in this example binds only the index {\tt j}. The values
of the parameter {\tt Transport} are set to zero from the city {\tt
LargestCity} to {\em every} city {\tt j}, but the values from every other city
{\em i} to all cities {\tt j} remain unchanged.
\item The fifth example illustrates a {\em nested index binding
statement}.
\begin{example}
   PreviousCity( NextCity(i) ) := i;
\end{example}
The index {\tt i} is bound, because it is used in the nested reference of the
element parameter {\tt NextCity(i)}, which in turn is used for indexing the
identifier {\tt PreviousCity}. Note that, in a tour, city {\tt i} by definition
is the previous city of the specific (next) city it is linked with.
\end{enumerate}

\paragraph{Sequential execution}
\index{execution!assignment} \index{assignment!sequential execution}

Indexed assignments are executed in a sequential manner, i.e. as if it was
replaced by a sequence of individual assignments to every element in the
binding domain. Thus, if {\tt Periods} is the integer set \verb|{0 .. 3}| with
index {\tt t}, then the indexed assignment
\begin{example}
    Stock( t | t > 0 ) := Stock(t-1) + Supply(t) - Demand(t);
\end{example}
is executed (conceptually) as the sequence of individual statements
\begin{example}
    Stock(1) := Stock(0) + Supply(1) - Demand(1);
    Stock(2) := Stock(1) + Supply(2) - Demand(2);
    Stock(3) := Stock(2) + Supply(3) - Demand(3);
\end{example}
Therefore, in the right hand side expression it is possible to refer to
elements of the identifier on the left which have received their value prior to
the execution of the current individual assignment. This type of behavior is
typically observed and wanted in stock balance type applications which use lag
references as shown above. The same argument also applies to assignments that
use element parameters for indexing on either the left- or right-hand side of
the assignment.

\paragraph{Indexed assignment versus {\tt FOR}}
\index{assignment!versus FOR statement@versus {\tt FOR} statement}
\subtypindex{statement}{FOR}{versus assignment}

In addition to the indexed assignment, AIMMS also possesses a more general
{\tt FOR} statement which repeatedly executes a group of statements for all
elements in its binding domain (see also Section~\ref{sec:exec.flow.for}). If
you are familiar with programming languages like {\sc C} or {\sc Pascal} you
might be tempted to embed every indexed assignment into one or more {\tt FOR}
statements with the proper domain. Although this will conceptually produce the
same results, we strongly recommend against it for two reasons.
\begin{itemize}
\item By omitting the {\tt FOR} statements you improve to the
readability and maintainability of your model code.
\item By including the {\tt FOR} statement unnecessarily you are
effectively degrading the performance of your model, because AIMMS can
execute an indexed assignment much more efficiently than the equivalent {\tt
FOR} statement.
\end{itemize}
Whenever you use a {\tt FOR} statement unnecessarily, AIMMS will produce a
compile time warning to tell you that the code would be more efficient by
removing the {\tt FOR} statement.

\paragraph{Example}

Consider the indexed assignment
\begin{example}
    Transport((i,j) | UnitTransportCost(i,j) > 100) := 0;
\end{example}
and the equivalent {\tt FOR} statement
\begin{example}
    for ((i,j) | UnitTransportCost(i,j) > 100) do
        Transport(i,j) := 0;
    endfor;
\end{example}
Notice that the indexed assignment is more compact than the {\tt FOR} statement
and is easier to read. In this example AIMMS will warn against this use of
the {\tt FOR} statement, because it can be removed without any change in
semantics, and will lead to more efficient execution.

\paragraph{Undefined left-hand references}
\index{assignment!with lag and lead} \index{lag/lead operator!in assignment}
\index{assignment!with element parameter} \index{element parameter!in
assignment}

When there are undefined references with lag and lead operators on the
left-hand side of an assignment (i.e.\ references that evaluate to the empty
element), the corresponding assignments will be skipped. The same is true if
the identifier on the left contains undefined references to element parameters.
Notice that this behavior is different from the behavior of a reference
containing undefined lag and lead expressions on the right-hand side of an
assignment. These evaluate to zero.

\paragraph{Example}
Consider the assignment to the parameter {\tt Stock} above. It could also have
been written as
\begin{example}
    Stock(t+1) := Stock(t) + Supply(t+1) - Demand(t+1);
\end{example}
In this case, there is no need to add a condition to the assignment for {\tt
t}${}=3$. The reference to {\tt t+1} is undefined, and hence the assignment
will be skipped. Similarly, the assignment
\begin{example}
    PreviousCity( NextCity(i) ) := i;
\end{example}
will only be executed for those cities {\tt i} for which {\tt NextCity(i)} is
defined.


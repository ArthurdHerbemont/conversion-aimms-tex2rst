\section{Raising and handling warnings and errors}
\label{sec:exec.error}\fcstatindex{OnError}\AIMMSlink{onerror}

\paragraph{Errors and warnings}
During the development and deployment of an AIMMS application, unexpected, possibly harmful, situations can arise.
These situations are divided into errors and warnings. An error is a situation
that cannot be handled by the procedure encountering it. A warning is a situation
that can be handled by the procedure encountering it, but might warrant further
inspection by the model developer or by the model user. Note that, even when
a procedure cannot handle an error itself, it should be able to recover from that error.
In this section, you will find AIMMS facilities to
\begin{itemize}
\item handle errors; to handle an error, AIMMS will give you access to the information therein.
      A handler is a piece of AIMMS code that handles selected errors and warnings.
      Errors and warnings can be communicated to handlers higher in the execution stack.
\item raise an error; not only AIMMS may detect situations warranting an error or warning message, but also the application itself.
      For such situations AIMMS provides a facility to raise custom errors from within your model.
\item handle a legacy situation; external and intrinsic AIMMS procedures may return a status code indicating success or failure.
      Whenever a failure status of an external and intrinsic procedure remains unnoticed, AIMMS can automatically raise an error in such situations.
\item extensively check the code; AIMMS can check your application for many different kinds of situations that occassionally warrant a warning.
      It is usually worthwhile to apply all these checks to your application.
\end{itemize}


\subsection{Handling errors}\label{sec:exec.error.handling}

\paragraph{Subsection overview}
In this subsection you will find an introduction to both the global and local error handling mechanisms available in AIMMS.
Global error handling, by means of specifying a single handler procedure, is used to treat runtime errors
occuring anywhere inside the entire model that are not handled elsewhere.
Local error handling, by means of the {\tt OnError} clause in a {\tt BLOCK} statement, allows error handling
of runtime errors occuring in a specific block of code.
Global and local error handling are the
blocks on which the error handling framework in AIMMS is built.
At the end of this subsection, you will find a description of all the intrinsic functions available for accessing and
manipulating information regarding errors.

\paragraph{Global error handling}\AIMMSlink{GlobalErrorHandling}

To activate global error handling, the name of a handling procedure in your model must be assigned to the option {\tt Global\_error\_handler}.
Such a procedure must have a single element parameter argument {\tt err} in the predeclared set {\tt errh::PendingErrors}.
The global error handling procedure will be executed for each pending error whenever an execution run has been terminated because of errors
that have not been handled elsewhere in the model. The global error handler will also be called at the end of a finished execution
run if there are unhandled warnings. In this context, an execution run is any call to an AIMMS procedure initiated either through the
AIMMS GUI or through the AIMMS API.

\paragraph{Example}

Below a global error handling procedure {\tt MyErrorHandler} is
illustrated. The lines in the body of the procedure are numbered to
facilitate the explanation of the example.
\begin{example}
Procedure MyErrorHandler {
    Arguments  :  err;
    ElementParameter  err {
        Range      :  errh::PendingErrors;
        Property   :  Input;
    }
    Body: {
        1  if errh::Node(err) = 'DefP' then
        2      DialogMessage(errh::Message(err) + "; resetting P to its default.");
        3      Empty P ;
        4      errh::MarkAsHandled(err);
        5  elseif errh::InsideCategory(err,'IO') then
        6      errh::Adapt(err,message:"IO error: please consult ...; "
        7         + errh::Message(err)  ); ! Pass adapted message on to next handler.
        8  else
        9      ! Errors not handled will be passed on to the error/warning window.
        10  endif
    }
}
\end{example}

\paragraph{Example explanation}

The procedure starts with declaring the argument {\tt err} as an element parameter with
the predeclared set {\tt errh::PendingErrors}, with a subset of the predeclared set {\tt Integers} as its range.
During an execution run, this set is filled with the numbers of the errors and warnings raised.  Each number
refers to an error or warning with various pieces of information therein, such as
its error description, the node in which the error or warning occurred and its severity. In addition, each
error belongs to a category. All this information can be accessed using intrinsic functions.
The body of the procedure is now explained line by line:
\begin{description}
\item[line 1] The intrinsic function {\tt errh::Node} is used to determine whether or not the error occurred inside the procedure {\tt DefP}.
              This intrinsic function returns the identifier or node in which the error occured as an element of the predeclared set {\tt AllSymbols}.
\item[lines 2, 3] If the error did happen inside the procedure {\tt DefP}, the application user is notified and
              {\tt P} is reset to its default.
              The notification uses the original error description obtained using
              the intrinsic function {\tt errh::Message(err)}.
\item[line 4] Each handled error will be marked as such.
              When an error handler finishes, it will delete the errors
              that have been marked as handled from the predeclared set
              {\tt errh::PendingErrors}.
\item[line 5] To discern the type of an error, errors are divided into categories.
              For each error, the category to which it belongs can be obtained using
              the function {\tt errh::Category(err)}. The error categories form a nested structure.
              For instance, both {\tt IO} and {\tt Generation} errors are {\tt Execution} errors.
              The intrinsic function {\tt errh::InsideCategory(err)} can be used to determine
              whether or not an error is within a particular category.
\item[lines 6, 7] Translate the error by adapting information.
              In this example, only the message is actually adapted, but
              most parts of an error can be adapted. Note that in this {\tt else} branch, the function {\tt errh::MarkAsHandled}
              is not called, the result being that the adapted error message will appear in the messages/errors window.
\item[line 8] In this branch, the error is not handled. An error that has not been handled when the error handler finishes will not be deleted.
              Instead, it is being displayed in the messages/errors window.
\end{description}


\paragraph{Local error handling by means of the {\tt OnError} clause}

The following template of a {\tt BLOCK} statement illustrates local error handling by means of the {\tt OnError} clause.

\begin{example}
1    BLOCK
2       statement_1 ;
3       ...
4       statement_n ;
5    ONERROR err DO
6           ...
7           ...
8    ENDBLOCK ;
\end{example}

All errors occuring inside {\tt statement\_1} ... {\tt statement\_n} on lines 2 ... 4 are handled by the
error handler on lines 6 and 7, where {\tt err} is an element parameter of the set {\tt errh::PendingErrors}.
Block statements can be nested, either directly in a single body, or in other procedures called from within block statements.
This gives rise to a stack of error handlers as illustrated below.
A detailed example of a local error handler is given in Section~\ref{sec:module.runtime}.

\paragraph{Error flow architecture}

The global error handlers and the {\tt OnError} error handlers are essential building blocks
of the error handling framework of AIMMS. This error handling framework is illustrated
in Figure~\ref{fig:errFlow.arch}.
\begin{aimmsfigure}
\figscale[0.08]{stack-of-exception-handlers}
\caption{Error flow through handlers}\label{fig:errFlow.arch}
\end{aimmsfigure}


\paragraph{Construction of the error handler stack}
At the start of each execution run, a new stack of error handlers
is created. At the bottom of this stack is the standard handler
{\tt To Global Collector}. When the option
{\tt Global\_error\_handler} is set, the specified
procedure is placed on top of this new stack.
Additional handlers are placed on the
stack by each {\tt OnError} clause in a nested {\tt BLOCK} statement.

\paragraph{Errors flowing through a handler stack}

When raised, each error is set aside for handling by the topmost error handler. When the number of
errors set aside reaches the limit specified by the option {\tt Errors\_until\_execution\_interrupt},
the execution is interrupted and resumes by executing the code in the topmost error handler.
When the execution is not interrupted, but there are pending errors or warnings,
the error handling code is executed after the completion of the last statement
prior to the {\tt BLOCK} statement.

\paragraph{Multiple errors may require handling}
\index{OnError clause@{\tt OnError} clause!and RETURN, HALT, SKIP, BREAK@and {\tt RETURN}, {\tt HALT}, {\tt SKIP}, {\tt BREAK}}

A single statement may result in multiple error messages, for instance a solve statement or a data assignment statement with several duplicate entries.
Thus, even if the option {\tt Errors\_until\_execution\_interrupt} is 1 (its default), multiple errors may need to be handled.
If multiple errors caused by a single statement are handled inside the {\tt OnError} 
clause of a {\tt BLOCK} statement, the code within the {\tt OnError} clause will 
be executed unconditionally {\em for every single error}, unless you explicitly break away from the{\tt OnError} clause.

\paragraph{Break away from handling}
If you use a {\tt RETURN}, {\tt HALT}, {\tt BREAK} or {\tt RAISE ERROR} statement
inside the {\tt OnError} clause, the handling of any subsequent errors or warnings will be stopped.
You are actually indicating that these further errors and warnings are no longer of interest and thus
they will be automatically set as handled. A plain {\tt BREAK} statement just breaks the error 
handling loop. If the {\tt Block} statement is inside an outer loop statement like {\tt FOR} or {\tt WHILE} and 
you want to break from that loop, you need to use a {\em loop string} (see Section~\ref{sec:exec.flow.while-repeat-adv}). 

\paragraph{SKIP in OnError}
A plain {\tt Skip} statement in the {\tt OnError} clause simply skips the remaining statements and continues
with the next error that needs to be handled. You can use a {\tt SKIP} with a {\em loop string} to skip the statements
of an outer loop statement. This will break away from the {\tt OnError} clause as described above.

\paragraph{What to do with an error}
For each error, the error handling code will decide whether to handle that
error itself, let another handler handle the error, or ignore the error (as
was already illustrated in the example above).

\paragraph{Handling an error inside a handler}
Errors may also occur during the execution of the {\tt OnError} clause or of a {\tt BLOCK} statement or the global error handling procedure.
These errors are handled by the next error handler in the stack of error handlers.

\paragraph{Error collector}

When an error reaches the handler {\tt To Global Collector}, it is
sent to the \textbf{Error and Warning Collector} object which collects all
errors that have fallen through the various handlers (if any). Errors in the \textbf{Error and Warning Collector} can
be queried from within the AIMMS API or viewed from within the messages/errors window of the AIMMS GUI.


\paragraph{The predeclared module {\tt ErrorHandling}}
Errors to be handled can be queried using the following predeclared identifiers and intrinsic functions from the module {\tt ErrorHandling} with
prefix {\tt errh}:
\begin{description}
\item[{\tt errh::PendingErrors}] A predeclared set filled with the numbers of the errors that can be handled at this point.
\item[{\tt errh::IndexPendingErrors}] An index of the above predeclared set.
    \item[error parts] An error is made up of several parts; each of which can be obtained separately using
                       the intrinsic functions below. Each of the functions below will raise an error of their own
                       if {\tt err} is not a valid error that can be handled at that point.
    \begin{description}
    \item[{\tt errh::Severity(err)}]     An element in {\tt errh::AllErrorSeverities} is returned indicating the severity of the error.
    \item[{\tt errh::Message(err)}]      A string containing the error description is returned. This string is not empty.
    \item[{\tt errh::Category(err)}]     An element in {\tt errh::AllErrorCategories} is returned indicating the category of the error.
    \item[{\tt errh::Code(err)}]         The element in {\tt errh::ErrorCodes} that is return\-ed by this function identifies the message code of the error.
                                         This element name may be cryptic; as it is primarily used for identification of the error within the AIMMS system.
    \item[{\tt errh::NumberOfLocations(err)}] The number of locations relevant to this error.
                                              For compilation errors, there is typically only one relevant location.
                                              For an AIMMS initialization error there are no relevant locations.
                                              For an execution error the positions in all the active procedures are recorded.
                                              For an error during file read, at least the positions in the data file and the read statement
                                              are recorded. Similarly, for an error during the generation of a constraint, at least
                                              the constraint and the {\tt SOLVE} statement are recorded as relevant positions.
    \item[{\tt errh::Node(err,loc)}]         An element in {\tt AllSymbols} is returned for an error location inside the model.
                                             The optional argument {\tt loc} defaults to 1 and should be in the range
                                             {\tt \{ 1 .. NumberOfLocations \} }.
                                             The element returned by this function is non-empty except for the first location when reading data from a file.
    \item[{\tt errh::Attribute(err,loc)}]    An element in {\tt AllAttributeNames}.
    \item[{\tt errh::Line(err,loc)}]         An integer indicating the line number of the error in the attribute or file, or 0 if not known.
    \item[{\tt errh::Column(err)}]       An integer indicating the column position in an erroneous line being read from a data file.
                                         All errors when reading a data file are reported separately, such that the {\tt loc} argument is not applicable.
    \item[{\tt errh::Filename(err)}]     A non-empty string is returned when reading from a data file.
                                         All errors when reading a data file are reported separately, and so the {\tt loc} argument is not applicable.
    \item[{\tt errh::Multiplicity(err)}] An integer indicating the number of occurrences of this error. Two errors are considered equal if
                                         they are equal in all of the following parts: {\tt Severity}, {\tt Message}, {\tt Category}, {\tt Code}
                                         and the first location (if available). The first location is the location in the file being read when the error 
                                         occurs during a read statement, otherwise it is the statement being executed.
    \item[{\tt errh::CreationTime(err,fmt)}] A string representing the creation time of the first occurrence of the error, 
                                         formatted according to time format {\tt fmt}.
    \end{description}
\item[{\tt errh::InsideCategory(err,cat)}] Returns 1 if the error code of {\tt err} falls inside the category {\tt cat}.
\item[{\tt errh::IsMarkedAsHandled(err)}] Returns 1 if the error is marked as handled.
\item[{\tt errh::Adapt(err, severity, message, category, code)} ] The error {\tt err} is adap\-ted with the components specified.
                                         Besides the mandatory argument {\tt err}, there should be at least one other argument.
\item[{\tt errh::MarkAsHandled(err,actually)}] The error {\tt err} is marked as handled if the argument {\tt actually} is non-zero.
                                           Marked errors will not be passed to the next error handler.
                                           The default of the optional argument {\tt actually} is 1. Using 0 will remove the mark from the error.
\end{description}



\paragraph{The log file {\tt aimms.err}}

AIMMS logs all errors and warnings to the file {\tt aimms.err} as they are raised.
The folder in which this file resides is controlled by the option {\tt Listing\_and\_tem\-porary\_files}.
The number of backups retained of this file is controlled by the option {\tt Number\_of\_log\_file\_backups}.

\subsection{Raising errors and warnings}\label{sec:exec.error.raising}\AIMMSlink{raise}

\paragraph{Raising errors}
The {\tt RAISE} statement is used to
\begin{itemize}
\item raise an error regarding a situation that cannot be handled, or to
\item raise a warning regarding a situation that can be handled but might warrant further investigation.
\end{itemize}
The syntax of the {\tt RAISE} statement is straightforward.

\paragraph{Syntax}
\begin{syntax}
\syntaxdiagram{raise-statement}{raise}
\end{syntax}

\paragraph{Example}
In the following example an error is raised when the inflow of a node exceeds its capacity.
\begin{example}
if inflow > stockCap then
    RAISE ERROR "Inflow exceeds stock capacity" CODE 'TooMuchInflow' ;
endif ;
\end{example}

\paragraph{Error code and category}
In order to enable an error handler to recognize the type of error being raised by a {\tt RAISE} statement,
that statement allows an optional error code to be specified.
This is an element in the set {\tt errh::ErrorCodes}. If the specified element does not yet exist,
it is created and added to that set. The category of an error raised by the {\tt RAISE} statement is fixed to {\tt 'User'}.

\paragraph{Position information}
AIMMS uses the line/procedure in which the {\tt RAISE} statement is specified as the position information associated with the error.
This permits the messages/errors window to open the attribute window of the procedure and place the cursor on
the statement where the problematic situation is detected.

\paragraph{Raising warnings}
Not only AIMMS itself but also procedures written in AIMMS may recognize situations
that can be handled but might warrant closer inspection by the application user.
For this purpose, the {\tt RAISE} statement can raise a warning, for example:
\begin{example}
if card( RawMaterialTraders ) = 0 then
   RAISE WARNING "There are no raw material traders, this may lead to " +
                 "infeasibilities in the case of too many accepted deliveries." ;
endif ;
\end{example}
The handling of warnings generated by a {\tt RAISE} statement is controlled
by the option {\tt Warning\_user}, with default {\tt common\_warning\_default}. The control of warning handling
is further explained in Subsection~\ref{sec:exec.error.warning}.

\subsection{Legacy: intrinsics with a return status}
\label{sec:exec.legacy.intrinsic.procedure}
\AIMMSlink{LegacyIntrinsicsWithReturnStatus}

\paragraph{Legacy situation}\AIMMSlink{LegacyIntrinsicProcedure}
% Return values < 0 for error can be observed in:
% - CaseGetDatasetReference
% - CaseLoadCurrent
AIMMS external procedures and intrinsic procedures can both return a status code
indicating whether or not they were successful. A return value $\leq 0.0$ is
interpreted as not successful, wheareas a return value $> 0.0$ is successful. In addition, when they
are not successful, the error message is often left in {\tt CurrentErrorMessage},
although this is only a guideline. The return value of a call to an intrinsic procedure is either
\begin{description}
\item[checked] As illustrated in the example:
    \begin{example}
       retval := PageOpen(...) ;
       if retval <= 0 then
          ... use CurrentErrorMessage ...
       endif ;
    \end{example}
\item[not checked] As illustrated in the example:
    \begin{example}
       PageOpen(...) ;
    \end{example}
\end{description}

\paragraph{Available error handling methods}
In the context of the error handling facility available in AIMMS, how should one handle 
the ``checked'' and ``not checked'' procedure calls when the return value is 0 and these
procedures have not raised an error themselves?
There are five error handling methods available to choose from:
\begin{description}
\item[{\tt ignore}]                     An error is never raised for an error
                                        occurring inside such a procedure, whether
                                        or not the return status is checked.
\item[{\tt raise\_warning\_when\_not\_checked}]  A warning is only raised if the return
                                        status of an intrinsic procedure is not checked.
\item[{\tt raise\_when\_not\_checked}]  An error is only raised if the return
                                        status of an intrinsic procedure is not checked.
\item[{\tt raise\_always\_warning}]     A warning is raised whether or not the return
                                        status is checked.
\item[{\tt raise\_always}]              An error is raised whether or not the return
                                        status is checked.
\end{description}
Which choice of error handling method is best depends on the application and can be controlled using the options:
\begin{description}
\item[{\tt Intrinsic\_procedure\_error\_handling}] for procedures with a return status supplied by AIMMS and
\item[{\tt External\_procedure\_error\_handling}] for externally supplied procedures.
\end{description}
The values of these options are the names of the error handling methods described above.
The default of both these options is {\tt raise\_when\_not\_checked}.
For projects created prior to the introduction of the error handling facilities in AIMMS (i.e. created in AIMMS 3.9 or lower),
these options generate the non-default value {\tt raise\_warning\_when\_not\_checked} in order to notify the model developer but do not change
the existing behavior of such projects significantly.

\subsection{Warnings}\label{sec:exec.error.warning}
\AIMMSlink{GroupingWarningsOnSeriousness}

\paragraph{Warnings}
AIMMS recognizes and warns about several types of possibly problematic situations.
These situations might warrant further investigation.  As with most other languages, AIMMS warns against the
use of identifiers before initializing them.  But unlike other languages, AIMMS also warns against
the inconsistent use of units of measurement (such as a comparison of a volume against a weight), or of model formulations
for which AIMMS can detect either compiletime or runtime issues that lead to sub-optimal performance or ambiguous results.
A selection of performance-related warnings is discussed in Section~\ref{subsection:eff.tuning-stmts.diagnostics}.

\paragraph{Complete flexibility}

The desired handling of each of these situations depends on the developer and the application;
varying from treating it as an error to fully ignoring it.  To permit complete
flexibility, there is separate option to control the reporting of each type of problematic
situation recognized.

\paragraph{Grouping\\Warning\\options}

Although all warnings can be controlled individually, this is not the most convenient way to employ the diagnostics provided by
these warnings. When entertaining a new idea (quick prototyping), most modelers understandably do not want to be
bothered by various warnings and want to be able to turn them all off. To facilitate this, all the warnings have been grouped into either common or strict warnings, and
the associated options assume default value for common and  strict warnings. Thus, all diagnostic warnings can be switched off by just changing the
options that control these defaults. For normal development work it is advisable to at least turn the common warnings on. In addition, we would encourage to turn
on the strict warnings during application tests.

\paragraph{Choosing the option setting}
In order to implement the above scheme and still permit full flexibility, each option controlling
the detection of a type of problematic situation can take on one of the following values:
\begin{description}
\item[{\tt error}] The situation is marked as an error and treated as an error.
\item[{\tt warning\_handle}]  The warning is raised in the current error handler, but does not count
                              toward the interruption of normal execution.
\item[{\tt common\_warning\_default}] The value of the option {\tt Common\_warning\_default} is used.
\item[{\tt warning\_collect}] The warning is raised in the {\tt Global\_error\_collector}, bypassing
                              the stack of error handlers.
\item[{\tt strict\_warning\_default}] The value of the option {\tt Strict\_warning\_default} is used.
\item[{\tt off}]              The warning is ignored.
\end{description}
The default of these options is either {\tt
common\_warning\_default} or {\tt strict\_warning\-\_default},
thereby effectively dividing these options into common and
strict groups. The range of options for {\tt common\_warning\_default}
and {\tt strict\_warning\_\-default} is {\tt \{off, warning\_collect,
warning\_handle, error\}}.  The default of the option {\tt
common\_warning\_default} is {\tt warning\_handle} and the default
of the option {\tt strict\_\-warning\_default} is {\tt off}.


\section{Expressions and statements}\label{sec:prelim.expr}

\paragraph{Model execution}
\index{model!execution} \index{execution!procedural}
\index{execution!nonprocedural} \attrindex{Definition} \attrindex{Body}

The creation of an AIMMS model is implemented using two separate but
interacting mechanisms. They are:
\begin{itemize}
\item automatic updating of the functional relationships specified
through expressions in the {\tt Definition} attributes of sets and parameters
in your model, and
\item manual execution of the statements that constitute the {\tt
Body} attribute of the procedures and functions defined in your application.
\end{itemize}
The precise manner in which these components are executed, and the way they
interact, is discussed in detail in Chapters~\ref{chap:nonproc}
and~\ref{chap:exec}. This section discusses the general structure of an
AIMMS model as well as the requirements for the {\tt Definition} and {\tt
Body} attributes.

\paragraph{Line length and empty lines}

The length of any particular line in the {\tt Definition} attribute of an
identifier or the {\tt Body} attribute of a procedure or function is limited to
255 characters. Although this full line length may be convenient for data
instantiation in the form of large tables, it is recommended that you do not
exceed a line length of 80 characters in these attributes in order to preserve
maximum readability. Empty lines can be inserted anywhere for easier reading.

\paragraph{Commenting}
\index{comment} \index{/* ... */ (comment)@{\tt /*}\dots{\tt */} (comment)}
\index{"! (comment)@{\tt "!} (comment)}

Expressions and statements in the {\tt Body} attribute of a procedure or
function can be interspersed with comments that are ignored during compilation.
AIMMS supports two kinds of comments:
\begin{itemize}
\item the tokens ``\verb|/*|'' and ``\verb|*/|'' for a block
comment, and
\item the exclamation mark ``\verb|!|'' for a one line comment.
\end{itemize}
Each block comment starts with a ``\verb|/*|'' token, and runs up to the
matching ``\verb|*/|'' token, and cannot be nested. It is a useful method for
entering pieces of explanatory text, as well as for temporarily commenting out
one or more execution statements. A one-line comment starts anywhere on a line
with an exclamation mark ``\verb|!|'', and runs up to the end of that line.

\paragraph{Expressions}
\index{expression}

The value of a {\tt Definition} attribute must be a valid expression of the
appropriate type. An expression in AIMMS can result in either
\begin{itemize}
\item a set,
\item a set element,
\item a string,
\item a numerical value,
\item a logical value, or
\item a unit expression.
\end{itemize}
Set, element and string expressions are discussed in full detail in
Chapter~\ref{chap:set-expr}, numerical and logical expressions in
Chapter~\ref{chap:expr}, while unit expressions are discussed in
Chapter~\ref{chap:units}.

\paragraph{Statements}

AIMMS statements in the body of procedures and functions constitute the
algorithmic part of a modeling application. All statements are terminated by a
semicolon. You may enter multiple statements on a single line, or a single
statement over several lines.

\paragraph{Execution statements}
\index{statement}

To specify the algorithmic part of your modeling application, the following
statements can be used:
\begin{itemize}\sloppy
\item assignments---to compute a new value for a data item,
\item the \verb|SOLVE| statement---to solve a mathematical program for
the values of its variables,
\item flow control statements like \verb|IF-THEN-ELSE|, \verb|FOR|,
\verb|WHILE|, {\tt REPEAT}, \verb|SWITCH|, and \verb|HALT|---to manage the flow
of execution,
\item the \verb|OPTION| and {\tt Property} statements---to set
identifier properties and options dealing with execution, output, progress, and
solvers,
\item the data control statements \verb|EMPTY|, \verb|CLEANUP|,
\verb|READ|, \verb|WRITE|, \verb|DISPLAY|, and \verb|PUT|---to manage the
contents of internal and external data.
\item procedure calls---to execute the statements contained in a procedure.
\end{itemize}
The precise syntax of these execution statements is discussed in
Chapters~\ref{chap:exec} and further.


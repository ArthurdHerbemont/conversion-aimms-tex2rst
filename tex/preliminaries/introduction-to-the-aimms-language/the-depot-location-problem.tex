\section{The depot location problem}\label{sec:intro.decl}\index{depot
location problem}\index{problem!depot location}

\paragraph{The modeling process}

In translating any real-life problem into a valid AIMMS optimization model
(referred to as a mathematical program) several conceptual steps are required.
They are:
\begin{itemize}
\item describe the input and output data using sets and indexed identifiers,
\item specify the mathematical program,
\item specify procedures for data pre- and post-processing,
\item initialize the input data from files and databases,
\item solve the mathematical program, and
\item display the results (or write them back to a database).
\end{itemize}

\paragraph{Problem description}

The example in this chapter is based on a simple depot location problem which
can be summarized as follows.
\begin{quote}
Consider the distribution of a single product from one or more depots to
multiple customers. The objective is to select depots from a predefined set of
possible depots (each with a given capacity) such that
\begin{itemize}
\item the demand of each customer is met,
\item the capacity of each selected depot is not exceeded, and
\item the total cost for both depot rental and transport to the
customers is minimized.
\end{itemize}
\end{quote}

\paragraph{Use of sets}
\index{set!indexing}\index{index set!use of}\index{use of!index set}

In the above problem you can see that there are two entities that determine the
size of the problem: depots and customers. With these entities a number of
instances are associated, e.g.\ a particular instance of a depot could be {\tt
'Amsterdam'}. The precise collection of instances, however, may differ from run
to run. Therefore, when translating the problem into a symbolic model it is
customary and desirable not to make any explicit reference to individual
instances. Such high-level model specification can be accomplished through the
use of {\em sets}, each with an associated {\em index} for referencing
arbitrary elements in that set.

\paragraph{Initial set declarations}

The following set declarations in AIMMS introduce the two sets {\tt Depots}
and {\tt Customers} with indices {\tt d} and {\tt c}, respectively. AIMMS
has a convenient graphical model editor to create your model. It allows you to
enter all model input using graphical forms. However, in the interest of
compactness we will use a textual representation for declarations that closely
resembles the contents of a graphical form throughout this manual.
\begin{example}
Set Depots {
    Index :  d;
}
Set Customers{
    Index :  c;
}
\end{example}

\paragraph{Parameters for input data}
\index{parameter!use of}\index{use of!parameter}

In most models there is input data that can be naturally associated with a
particular element or tuple of elements in a set. In AIMMS, such data is
stored in Parameters. A good example in the depot location problem is the
quantity {\tt Distance}, which can be defined as the distance between depot
{\tt d} and customer {\tt c}. To define {\tt Distance} a index tuple ({\tt
d},{\tt c}) is required and it is referred to as the associated {\tt IndexDomain} of this quantity.

\paragraph{Example}

In AIMMS, the identifier {\tt Distance} is viewed as a {\tt Parameter} (a
known quantity), and can be declared as follows.
\begin{example}
Parameter Distance {
    Index :  (d,c);
}
\end{example}
In this example the identifier {\tt Distance} is referred to as an ndexed
identifier, because it has a nonempty index domain.

\paragraph{Scalar data}

Not all identifiers in a model need to be indexed. The following declarations
illustrate two scalar parameters which are used later.
\begin{example}
Parameter MaxDeliveryDistance;
Parameter UnitTransportRate;
\end{example}

\paragraph{Restricting permitted routes}
\index{domain condition}

For real-life applications the collection of all possible routes {\tt (d,c)}
may be huge. In practice, routes {\tt (d,c)} for which the distance {\tt
Distance(d,c)} is big, will never become a part of the solution. It, therefore,
makes sense to exclude such routes {\tt (d,c)} from the entire solution process
altogether. We can do this by computing a set of {\tt PermittedRoutes} which we
will use throughout the sequel of the example.

\paragraph{Example}\herelabel{examp:tutor.routes}

In AIMMS, the relation {\tt PermittedRoutes} can be declared as follows.
\begin{example}
Set PermittedRoutes {
    SubsetOf     :  (Depots, Customers);
    Definition   :  {
        { (d,c) | Distance(d,c) <= MaxDeliveryDistance }
    }
}
\end{example}

\paragraph{Explanation}

In the {\tt SubsetOf} attribute of the above declaration it is indicated that
the set {\tt Permitted\-Routes} is a subset of the Cartesian product of the
simple sets {\tt Depots} and {\tt Customers}. The {\tt Definition} attribute
globally defines the set {\tt Permitted\-Routes} as the set of those tuples
({\tt d}, {\tt c}) for which the associated {\tt Distance(d,c)} does not exceed
the value of the scalar parameter {\tt MaxDeliveryDistance}. AIMMS will
assure that such a global relationship is valid at any time during the
execution of the model. Note that the set notation in the {\tt Definition}
attribute resembles the standard set notation found in mathematical literature.

\paragraph{Applying domain restrictions}

Now that we have restricted the collection of permitted routes, we can use the
relation {\tt PermittedRoutes} throughout the model to restrict the domain of
identifiers declared over {\tt (d,c)} to only hold data for permitted routes
{\tt (d,c)}.

\paragraph{Example}

In AIMMS, the parameter {\tt UnitTransportCost} can be declared as follows.
\begin{example}
Parameter UnitTransportCost {
    IndexDomain  :  (d,c) in PermittedRoutes;
    Definition   :  UnitTransportRate * Distance(d,c);
}
\end{example}
This parameter is defined through a simple formula. Once an identifier has its
own definition, AIMMS will not allow you to make an assignment to this
identifier anywhere else in your model text.

\paragraph{Effects of domain restriction}

As an effect of applying a domain restriction to the parameter {\tt
UnitTransport\-Cost}, any reference to {\tt UnitTransportCost(d,c)} for tuples
({\tt d},{\tt c}) outside the set {\tt Permit\-tedRoutes} is not defined, and
AIMMS will evaluate this quantity to 0. In addition, AIMMS will use the
domain restriction in its GUI, and will not allow you to enter numerical values
of {\tt UnitTransportCost(d,c)} outside of its domain.

\paragraph{Additional parameter declarations}

To further define the depot location problem the following parameters are
required:
\begin{itemize}
\item the fixed rental charge for every depot {\tt d},
\item the available capacity of every depot {\tt d}, and
\item the product demand of every customer {\tt c}.
\end{itemize}
The AIMMS declarations are as follows.
\begin{example}
Parameter DepotRentalCost {
    IndexDomain  :  d;
}
Parameter DepotCapacity {
    IndexDomain  :  d;
}
Parameter CustomerDemand {
    IndexDomain	 :  c;
}
\end{example}


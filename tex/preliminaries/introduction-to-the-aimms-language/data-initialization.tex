\section{Data initialization}\label{sec:intro.expressions}
\index{initialization!data} \index{model data} \index{data initialization}

\paragraph{Separation of model and data}

In the previous section the entire depot location model was specified without
any reference
\begin{itemize}
\item to specific elements in the sets {\tt Depots} and {\tt Customers}, or
\item to specific values of parameters defined over such elements.
\end{itemize}
As a result of this clear separation of model and data values, the model can
easily be run for different data sets.

\paragraph{Data sources}

A data set can come from various sources. In AIMMS there are six sources you
might consider for your application. They are:
\begin{itemize}
\item commercial databases,
\item text data files,
\item AIMMS case files,
\item internal procedures,
\item external procedures, or
\item the AIMMS graphical user interface (GUI).
\end{itemize}
These data sources are self-explanatory with perhaps the AIMMS case files as
an exception. AIMMS case files are obtained by using the case management
facilities of AIMMS to store data values from previous runs of your model.

\paragraph{A simple data set in text format}
\keyindex{DATA} \keyindex{DATA TABLE} \keyindex{COMPOSITE TABLE}

The following fictitious data set is provided in the form of an text data
file. It illustrates the basic constructs available for providing data in text
format. In this file, assignments are made using the '{\tt :=}' operator and
the keywords of {\tt DATA TABLE} and {\tt COMPOSITE TABLE} announce the table
format. The exclamation mark denotes a comment line.
\begin{example}
    Depots    := DATA { Amsterdam, Rotterdam };
    Customers := DATA { Shell, Philips, Heineken, Unilever };

    COMPOSITE TABLE
        d        DepotRentalCost    DepotCapacity
  ! ---------    ---------------    -------------
    Amsterdam        25550              12500
    Rotterdam        31200              14000
  ;

    COMPOSITE TABLE
        c        CustomerDemand
  ! ---------    --------------
    Shell            10000
    Philips           5000
    Heineken          3000
    Unilever          5000
  ;

    Distance(d,c) := DATA TABLE
                  Shell   Philips   Heineken   Unilever
    !             -----   -------   --------   --------
    Amsterdam      100      200        50         150
    Rotterdam       75      100        50          75
    ;

    UnitTransportRate   := 1.25 ;
    MaxDeliveryDistance := 125  ;
\end{example}

\paragraph{Reading in the data}
\statindex{READ}

Assuming that the text data file specified above was named {\tt
"initial.dat"}, then its data can easily be read using the following {\tt READ}
statement.
\begin{example}
    read from file "initial.dat" ;
\end{example}
Such {\tt READ} statements are typically placed in the predefined procedure
{\tt Main\-Initialization}. This procedure is automatically executed at the
beginning of every session immediately following the compilation of your model
source.

\paragraph{Automatic initialization}

When AIMMS encounters any reference to a set or parameter with its own
definition inside a procedure, AIMMS will automatically compute its value on
the basis of its definition. When used inside the procedure {\tt
MainInitialization}, this form of data initialization can be viewed as yet
another data source in addition to the six data sources mentioned at the
beginning of this section.

\section{Nonprocedural execution}\label{sec:nonproc.exec}
\index{execution!nonprocedural} \index{nonprocedural execution}

\paragraph{Execution based on definitions}

Execution based on definitions is typically not controlled by the user. It
takes place automatically, but only when up-to-date values of defined sets or
parameters are needed. Basically, execution can be triggered automatically from
within:
\begin{itemize}
\item the body of a function or procedure, or
\item an object in the graphical user interface.
\end{itemize}

\paragraph{Relating definitions and procedures}

Consider a set or a parameter with a definition which is referenced in an
execution statement inside a function or a procedure. Whenever the value of
such a set or parameter is not up-to-date due to previous data changes,
AIMMS will compute its current value just prior to executing the
corresponding statement. This mechanism ensures that, during execution of
functions or procedures, the functional relationships expressed in the
definitions are always valid.

\paragraph{Lazy evaluation}
\index{definition!lazy evaluation} \index{lazy evaluation} \index{nonprocedural
execution!lazy evaluation}

During execution AIMMS minimizes its efforts and updates only those values
of defined identifiers that are needed at the current point of execution. Such
{\em lazy evaluation} can avoid unnecessary computations and reduces
computational time significantly when the number of dependencies is large, and
when relatively few dependencies need to be resolved at any particular point in
time.

\paragraph{GUI requests}

For the graphical objects in an end-user interface you may specify whether the
data in that object must be up-to-date at all times, or just when the page
containing the object is opened. AIMMS will react accordingly, and
automatically update all corresponding identifiers as specified.

\paragraph{The set {\tt Current\-AutoUpdated\-Definitions}}
\presetindex{CurrentAutoUpdatedDefinitions} \presetindex{AllDefinedSets}
\presetindex{AllDefinedParameters} \AIMMSlink{currentautoupdateddefinitions}
\AIMMSlink{alldefinedsets} \AIMMSlink{alldefinedparameters}

Which definitions are automatically updated in the graphical user interface
whenever they are out-of-date, is determined by the contents of the predefined
set {\tt CurrentAutoUpdatedDefinitions}. This set is a subset of the predefined
set {\tt AllIdentifiers}, and is initialized by AIMMS to the union of the
sets {\tt AllDefinedSets} and {\tt AllDefinedParameters} by default.

\paragraph{Exclude from auto-updating}

To prevent auto-updating of particular identifiers in your model, you should
remove such identifiers from the set {\tt CurrentAutoUpdatedDefinitions}. You
can change its contents either from within the language or from within the
graphical user interface. Typically, you should exclude those identifiers from
auto-updating whose computation takes a long time to finish. Instead of waiting
for their computation on every input change, it makes much more sense to
collect all input changes for such identifiers and request their re-computation
on demand.

\paragraph{Requesting updates}

All identifiers that are not contained in {\tt CurrentAutoUpdatedDefinitions}
must be updated manually under your control. AIMMS provides several
mechanisms:
\begin{itemize}
\item you can call the {\tt UPDATE} statement from within the
language, or
\item you can attach update requests of particular identifiers as
actions to buttons and pages in the end-user interface.
\end{itemize}

\paragraph{The {\tt UPDATE} statement}
\statindex{UPDATE} \AIMMSlink{update}
\herelabel{stmt:update}

The {\tt UPDATE} statement  can be used to update the contents of one or more
identifiers during the execution of a procedure that is called by the user. In
this way, selected identifiers which are shown in the graphical user interface
and not kept up-to-date automatically, can be made up-to-date once the
procedure is activated by the user.

\paragraph{Syntax}
\syntaxdiagram{update-statement}{update}

\paragraph{Allowed identifiers}

The following selections of identifiers are allowed in the {\tt UPDATE}
statement:
\begin{itemize}
\item identifiers with a definition,
\item identifiers associated with a structural section in the
model-tree, and
\item identifiers in a subset of the predefined set {\tt AllIdentifiers}.
\end{itemize}

\paragraph{Example}

The following execution statement inside a procedure will trigger AIMMS to
update the values of the identifiers {\tt FixedCost}, {\tt VariableCost} and
{\tt TotalCost} upon execution.
\begin{example}
    Update FixedCost, VariableCost, TotalCost;
\end{example}

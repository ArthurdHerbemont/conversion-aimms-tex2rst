\section{Expressions and statements allowed in definitions}\label{sec:nonproc.allowed}

\paragraph{Complicated definitions}

In most applications, the functional relationship between input and output
identifiers in the definition of a set or a parameter can be expressed as an
ordinary set-valued, set element-valued or numerical expression. In rare
occasions where a functional relationship cannot be written as a single
symbolic statement, a function or procedure can be used instead.

\paragraph{Allowed definitions}
\index{definition!allowed expressions}

In summary, you may use one of the following items in set and parameter
definitions:
\begin{itemize}
\item a set-valued expression,
\item an element-valued expression,
\item a numerical expression,
\item a call to a function, or
\item a call to a procedure.
\end{itemize}

\paragraph{Limited self- referencing allowed}
\index{definition!self-reference}

Under some conditions, expressions used in the definition of a particular
parameter can contain references to the parameter itself. Such self-referencing
is allowed if the {\em serial} computation of the definition over all elements
in the index domain of the parameter does not result in a cyclic reference to
the parameter at the individual level. This is useful, for instance, when
expressing stock balances in a functional manner with the use of lag operators.

\paragraph{Example}
The following definition illustrates a valid example of a self-reference.
\begin{example}
Parameter Stock {
    IndexDomain  : t;
    Definition   : {
        if ( t = FirstPeriod ) then BeginStock
            else Stock(t-1) + Supply(t) - Demand(t) endif
    }
}
\end{example}
If {\tt t} is an index into a set {\tt Periods = }\verb|{0..3}|, and {\tt
FirstPeriod} equals~{\tt 0}, then at the individual level the assignments with
self-references are:
\begin{example}
    Stock(0) := BeginStock ;
    Stock(1) := Stock(0) + Supply(1) - Demand(1) ;
    Stock(2) := Stock(1) + Supply(2) - Demand(2) ;
    Stock(3) := Stock(2) + Supply(3) - Demand(3) ;
\end{example}
Since there is no cyclic reference, the above definition is allowed.

\paragraph{Functions and procedures}
\index{definition!use of functions and procedures}

You can use a call to either a function or a procedure to compute those
definitions that cannot be expressed as a single statement. If you use a
procedure, then only a single output argument is allowed. In addition, the
procedure cannot have any side-effects on other global sets or parameters. This
means that no direct assignments to other global sets or parameters are
allowed.

\paragraph{Arguments\\ and global references}

The identifiers referenced in the actual arguments of a procedure call, as well
as the global identifiers that are referenced in the body of the procedure,
will be considered as input parameters for the computation of the current
definition. That is, data changes to any of these input identifiers will
trigger the re-execution of the procedure to make the definition up-to-date.
The same applies to functions used inside definitions.

\paragraph{Examples}
The following two examples illustrate the use of functions and procedures in
definitions.
\begin{itemize}
\item
Consider a function {\tt TotalCostFunction} which has a single argument for
individual cost coefficients. Then the following declaration illustrates a
definition with a function reference.
\begin{example}
Parameter TotalCost {
    Definition : TotalCostFunction( CostCoefficient );
}
\end{example}
AIMMS will consider the actual argument {\tt CostCoefficient}, as well any
other global identifier referenced in the body of {\tt TotalCostFunction} as
input parameters of the definition of {\tt TotalCost}.
\item
Similarly, consider a procedure {\tt TotalCostProcedure} which performs the
same computation as the function above, but returns the result via a (single)
output argument. Then the following declaration illustrates an equivalent
definition with a procedure reference.
\begin{example}
Parameter TotalCost {
    Definition : TotalCostProcedure( CostCoefficient, TotalCost );
}
\end{example}
\end{itemize}

\paragraph{One procedure for several definitions}

Whenever the values of a number of identifiers are computed simultaneously
inside a single procedure without arguments, then this procedure must be
referenced inside the definition of each and all of the corresponding
identifiers. If you do not reference the procedure for all corresponding
identifiers, a compile-time error will result. All other global identifiers
used inside the body of the procedure count as input identifiers.

\paragraph{Example}

Consider a procedure {\tt ComputeCosts} which computes the value of the global
parameters {\tt FixedCost(m,p)} and {\tt VariableCost(m,p)} simultaneously.
Then the following example illustrates a valid use of {\tt ComputeCosts} inside
a definition.
\begin{example}
Parameter FixedCost {
    IndexDomain  : (m,p);
    Definition   : ComputeCosts;
}
Parameter VariableCost {
    IndexDomain  : (m,p);
    Definition   : ComputeCosts;
}
\end{example}
Omitting {\tt ComputeCosts} in either definition will result in a compile-time
error.


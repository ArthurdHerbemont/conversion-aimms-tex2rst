\section{Sets and indices}\label{sec:set.intro}

\paragraph{General}

Sets and indices give your AIMMS model dimension and depth by providing a
mechanism for grouping parameters, variables, and constraints. Sets and indices
are also used as driving mechanism in arithmetic operations such as summation.
The use of sets for indexing expressions helps to describe large models in a
concise and understandable way.

\paragraph{Example}
\index{set!indexing} \index{use of!index set}

Consider a set of {\em Cities} and an identifier called {\em Transport} defined
between several pairs of cities $(i,j)$, representing the amount of product
transported from supply city $i$ to destination city $j$. Suppose that you are
interested in the quantities arriving in each city. Rather than adding many
individual terms, the following mathematical notation, using sets and indices,
concisely describes the desired computation of these quantities.
\[
        (\forall j \in Cities) \qquad Arrival_j = \sum_{i \in Cities} Transport_{ij}.
\]
This multidimensional index notation forms the foundation of the AIMMS
modeling language, and can be used in all expressions. In this example, {\em i}
and {\em j} are indices that refer to individual {\em Cities}.

\paragraph{Several types of sets}
\index{set}

A set in AIMMS
\begin{itemize}
\item has either {\em strings} or {\em integers} as elements,
\item is either a {\em simple} set, or a {\em relation}, and
\item is either {\em indexed} or {\em not indexed}.
\end{itemize}

\paragraph{String versus integer}

Sets can either have strings as elements (such as the set {\em Cities}
discussed above), or have integers as elements. An example of an integer set
could be a set of {\em Trials} represented by the numbers $1,\dots,n$. The
resulting integer set can then be used to refer to the results of each single
experiment.

\paragraph{Simple versus relation}
\index{set!relation} \index{relation}

A {\em simple} set is a one-dimensional set, such as the set {\em Cities}
mentioned above, while a {\em relation} or multidimensional set is the
Cartesian product of a number of simple sets or a subset thereof. An example of
a relation is the set of possible {\em Routes} between supply and destination
cities, which can be represented as a subset of the Cartesian product {\em
Cities} $\times$ {\em Cities}.


\paragraph{Indexing as basic mechanism}
\index{set!indexing}

Sets in AIMMS are the basis for creating multidimensional identifiers in
your model. Through indices into sets you have access to individual values of
these identifiers for each tuple of elements. In addition, the indexing
notation in AIMMS is your basic mechanism for expressing iterative
operations such as repeated addition, repeated multiplication, sequential
search for a maximum or minimum, etc.

\paragraph{Indexed sets}

Simple sets may be indexed. An indexed set is a family of
sets defined for every element in the index domain of the indexed set. An
example of an indexed set is the set of transport destination cities defined
for each supply city. On the other hand, the set {\em Cities} discussed above
is not an indexed set.

\paragraph{Sorting of sets}
\index{set!sort} \index{sorting sets}

The contents of any simple can be sorted in AIMMS. Sorting
can take place either automatically or manually.  Automatic sorting is based on
the value of some expression defined for all elements of the set. By using an
index into a sorted subset, you can access any subselection of data in the
specified order.  Such a subselection may be of interest in your end-user
interface or at a certain stage in your model.


\section{\tttext{Set} declaration and attributes}\label{sec:set.decl}

\paragraph{Set attributes}
\declindex{Set} \AIMMSlink{set}

Each set has an optional list of attributes which further specify its intended
behavior in the model.  The attributes of sets are given in
Table~\ref{table:set.attr-set}. The attributes {\tt IndexDomain} is only relevant to indexed sets.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}  & {\bf See also}\\
\hline
\verb|IndexDomain| & {\em index-domain}  & \pageref{attr:set.index-domain} \\
\verb|SubsetOf|    & {\em subset-domain}    & \\
\verb|Index|        & {\em identifier-list}  & \\
\verb|Parameter|    & {\em identifier-list}  & \\
\verb|Text|         & {\em string}           & \pageref{attr:prelim.text}\\
\verb|Comment|      & {\em comment string}   & \pageref{attr:prelim.comment}\\
\verb|Property|     & {\tt NoSave}, {\tt ElementsAreNumerical}, {\tt ElementsAreLabels}
& \\
\verb|Definition|   & {\em set-expression}   & \\
\verb|OrderBy|     & {\em expression-list}  & \\
%% \verb|COMPATIBLE WITH|  & {\em identifier-list} &  \\
\hline\hline
\end{tabular}
\caption{{\tt Set} attributes}\label{table:set.attr-set}
\end{aimmstable}

\breaksubsection[0.85]{Simple sets}\label{sec:set.simple} \index{set!simple}
\index{simple set}

\paragraph{Definition}

A {\em simple}\index{set!simple} set in AIMMS is a finite collection of
elements. These elements are either strings or integers. Strings are typically
used to identify real-world objects such as products, locations, persons, etc.
Integers are typically used for algorithmic purposes. With every simple set you
can associate indices through which you can refer (in succession) to all
individual elements of that set in indexed statements and expressions.

\paragraph{Most basic example}
An example of the most basic declaration for the set {\em Cities} from the
previous example follows.
\begin{example}
Set Cities {
    Index      : i,j;
}
\end{example}
This declares the identifier \verb|Cities| as a simple set, and binds the
identifiers \verb|i| and \verb|j| as indices to \verb|Cities| throughout your
model text.

\paragraph{More detailed example}

Consider a set {\em SupplyCities} which is declared as follows:
\begin{example}
Set SupplyCities {
    SubsetOf   : Cities;
    Parameter  : LargestSupplyCity;
    Text       : The subset of cities that act as supply city;
    Definition : {
        { i | Exists( j | Transport(i,j) ) }
    }
    OrderBy    : i;
}
\end{example}
The ``{\tt |}'' operator used in the definition is to be read as ``such that''
(it is explained in Chapter~\ref{chap:set-expr}). Thus, {\tt SupplyCities} is
defined as the set of all cities from which there is transport to at least one
other city. All elements in the set are ordered lexicographically. The set has
no index of its own, but does have an element parameter {\tt LargestSupplyCity}
that can hold any particular element with a specific property. For instance,
the following assignment forms one way to specify the value of this element
parameter:
\begin{example}
   LargestSupplyCity := ArgMax( i in SupplyCities, sum( j, Transport(i,j) ) );
\end{example}
Note that this assignment selects that particular element from the subset of
{\tt SupplyCities} for which the total amount of {\tt Transport} leaving that
element is the largest.

\paragraph{The {\tt SubsetOf} attribute}\herelabel{attr:set.subset-of}
\declattrindex{set}{SubsetOf} \AIMMSlink{set.subset_of}

With the {\tt SubsetOf} attribute you can tell AIMMS that the set at hand
is a subset of another set, called the {\em subset domain}. For simple sets,
such a subset domain is denoted by a single set identifier.  During the
execution of the model AIMMS will assert that this subset relationship is
satisfied at all times.

\paragraph{Root sets}
\index{root set} \index{set!root}

Each simple set that is not a subset of another set is called a {\em root
set}.\index{set!root} As will be explained later on, root sets have a special
role in AIMMS with respect to data storage and ordering.
%% In addition, a root set can be made compatible with another root set.

\paragraph{The {\tt Index} attribute}\herelabel{attr:set.index}
\declattrindex{set}{Index} \AIMMSlink{set.index}

An index takes the value of {\em all} elements of a set successively and in the
order specified by its declaration. It is used in operations like summation and
indexed assignment over the elements of a set. With the {\tt Index} attribute
you can associate identifiers as indices into the set at hand. The index
attributes of all sets must be unique identifiers, i.e.\ every index can be
declared only once.

\paragraph{The {\tt Parameter} attribute}\herelabel{attr:set.parameter}
\declattrindex{set}{Parameter} \AIMMSlink{set.parameter}

A parameter declared in the {\tt Parameter} attribute of a set takes the value
of a {\em specific} element of that set. Throughout the sequel we will refer to
such a parameter as an {\em element parameter}. It is a very useful device for
referring to set elements that have a special meaning in your model (as
illustrated in the previous example). In a later chapter you will see that an
element parameter can also be defined separately as a parameter which has a set
as its range.

\paragraph{The {\tt Text} and {\tt Comment} attributes}\herelabel{attr:set.text}\herelabel{attr:set.comment}
\declattrindex{set}{Text} \AIMMSlink{set.text} \declattrindex{set}{Comment}
\AIMMSlink{set.comment}

With the {\tt Text} attribute you can specify one line of descriptive text for
the end-user. This description can be made visible in the graphical user
interface when the data of an identifier is displayed in a page object. You can
use the {\tt Comment} attribute to provide a longer description of the
identifier at hand. This description is intended for the modeler and cannot be
made visible to an end-user. The {\tt Comment} attribute is a multi-line string
attribute.

\paragraph{Quoting identifier names in {\tt Comment}}

You can make AIMMS aware that specific words in your comment text are
intended as identifier names by putting them in single quotes. This has the
advantage that AIMMS will update your comment when you change the name of
that identifier in the model editor, or, that AIMMS will warn you when a
quoted name does not refer to an existing identifier.

\paragraph{The {\tt OrderBy} attribute}\herelabel{attr:set.order-by}
\declattrindex{set}{OrderBy} \AIMMSlink{set.order_by}

With the {\tt OrderBy} attribute you can indicate that you want the elements
of a certain set to be ordered according to a single or multiple ordering
criteria. Only simple sets can be ordered.

\paragraph{Ordering root sets}

A special word of caution is in place with respect to specifying an ordering
principle for root sets. Root sets play a special role within AIMMS because
all data defined over a root set or any of its subsets is stored in the
original {\em data entry} order in which elements have been added to that root
set. Thus, the data entry order defines the natural order of execution over a
particular domain, and specifying the {\tt OrderBy} attribute of a root set
may influence overall execution times of your model in a negative manner.
Section~\ref{sec:eff.set.ordering} discusses these efficiency aspects in more
detail, and provides alternative solutions.

\paragraph{Ordering criteria}
\setiterindex{Sort} \typindex{keyword}{User} \index{OrderBy attribute@{\tt
OrderBy} attribute!{\tt User}}

The value of the {\tt OrderBy} attribute can be a comma-separated list of one
or more ordering criteria. The following ordering criteria (numeric, string or
user-defined) can be specified.
\begin{itemize}
\item If the value of the {\tt OrderBy} attribute is an indexed numerical
expression defined over the elements of the set, AIMMS will order its
elements in increasing order according to the numerical values of the
expression.
\item If the value of the {\tt OrderBy} attribute is either an index into
the set, a set element-valued expression, or a string expression over the set,
then its elements will be ordered lexicographically with respect to the strings
associated with the expression. By preceding the expression with a minus sign,
the elements will be ordered reverse lexicographically.
\item If the value of the {\tt OrderBy} attribute is the keyword {\tt
User}, the elements will be ordered according to the order in which they have
been added to the subset, either by the user, the model, or by means of the
{\tt Sort} operator.
\end{itemize}

\paragraph{Specifying multiple criteria}

When applying a single ordering criterion, the resulting ordering may not be
unique. For instance, when you order according to the size of transport taking
place from a city, there may be multiple cities with equal transport. You may
want these cities to be ordered too.  In this case, you can enforce a more
refined ordering principle by specifying multiple criteria. AIMMS applies
all criteria in succession, and will order only those elements that could not
be uniquely distinguished by previous criteria.

\paragraph{Example}
The following set declarations give examples of various types of automatic
ordering. In the last declaration, the cities with equal transport are placed
in a lexicographical order.
\begin{example}
Set LexicographicSupplyCities {
    SubsetOf  : SupplyCities;
    OrderBy   : i;
}
Set ReverseLexicographicSupplyCities {
    SubsetOf  : SupplyCities;
    OrderBy   : - i;
}
Set SupplyCitiesByIncreasingTransport {
    SubsetOf  : SupplyCities;
    OrderBy   : sum( j, Transport(i,j) );
}
Set SupplyCitiesByDecreasingTransportThenLexicographic {
    SubsetOf  : SupplyCities;
    OrderBy   : - sum( j, Transport(i,j) ), i;
}
\end{example}

\paragraph{The {\tt Property} attribute}\herelabel{attr:set.property}
\declattrindex{set}{Property} \AIMMSlink{set.property}
\declpropindex{set}{NoSave}
\declpropindex{set}{ElementsAreNumerical}
\declpropindex{set}{ElementsAreLabels}

\syntaxmark{property} In general, you can use the {\tt Property} attribute to
assign additional properties to an identifier in your model. The applicable
properties depend on the identifier type. Sets, at the moment, only support a
single property.
\begin{itemize}
\item The property {\tt NoSave} specifies that the contents of the set
at hand will never be stored in a case file. This can be useful, for instance,
for intermediate sets that are necessary during the model's computation, but
are never important to an end-user.
\item The properites {\tt ElementsAreNumerical} and {\tt ElementsAreLabels} are 
only relevant for integer sets (see also Section~\ref{sec:set.integer}). They will
ignored for non-integer sets.
\end{itemize}

\paragraph{Dynamic property selection}
\statindex{PROPERTY}

The properties selected in the {\tt Property} attribute of an identifier are
{\tt on} by default, while the nonselected properties are {\tt off} by default.
During execution of your model you can also dynamically change a property
setting through the {\tt Property} statement. The {\tt PROPERTY} statement is
discussed in Section~\ref{sec:exec.property}.

\paragraph{The {\tt Definition} attribute}\herelabel{attr:set.definition}
\declattrindex{set}{Definition} \AIMMSlink{set.definition}

If an identifier can be uniquely defined throughout your model by a single
expression, you can (and should) use the {\tt Definition} attribute to specify
this global relationship. AIMMS stores the result of a {\tt Definition} and
recomputes it only when necessary. For sets where a global {\tt Definition} is
not possible, you can make assignments in procedures and functions. The value
of the {\tt Definition} attribute must be a valid expression of the appropriate
type, as exemplified in the declaration
\begin{example}
Set SupplyCities {
    SubsetOf   : Cities;
    Definition : {
        { i | Exists( j | Transport(i,j) ) }
    }
}
\end{example}

%% \paragraph{The {\tt COMPATIBLE WITH} attribute}
%% \herelabel{attr:set.compatible-with}
%% \declattrindex{set}{COMPATIBLE WITH}
%%
%% The {\tt COMPATIBLE WITH} attribute is an advanced subject. It is only
%% relevant in very specific situations where you need to index outside
%% the boundaries of the existing root set hierarchy. For a further
%% discussion of set compatibility and its use, please refer to
%% Section~\ref{sec:bind.rules} on index binding.

\subsection{Integer sets}\label{sec:set.integer}
\index{integer set} \index{set!integer} \presetindex{Integers}

\paragraph{Integer sets}

A special type of simple set is an integer set. Such a set is characterized by
the fact that the value of the {\tt SubsetOf} attribute must be equal to the
predefined set {\tt Integers} or a subset thereof. Integer sets are most often
used for algorithmic purposes.

\paragraph{Usage in expressions}

Elements of integer sets can also be used as integer values in numerical
expressions. In addition, the result of an integer-valued expression can be
added as an element to an integer set. Elements of non-integer sets that
represent numerical values cannot be used directly in numerical expressions. To
obtain the numerical value of such non-integer elements, you can use the {\tt
Val} function (see Section~\ref{sec:set-expr.elem.functions}).

\paragraph{Interpret values as integer or label?}
\declpropindex{set}{ElementsAreNumerical}
\declpropindex{set}{ElementsAreLabels}
\index{lag/lead operator!with integer sets}

The interpretation of integer set elements will as integer values
in numerical expressions, raises an ambiguity for certain types of expressions. 
If {\tt anInteger} is an element parameter into an integer set {\tt anIntegerSet},
\begin{itemize}
\item how should AIMMS handle the expression
\begin{example}
    if (anInteger) then
        ...
    endif;
\end{example}
where {\tt anInteger} holds the value {\tt '0'}. On the one hand, it is not the empty element, 
so if AIMMS would interpret this as a logical expression with a non-empty element parameter, 
the {\tt if} statement would evaluate to true. If AIMMS would interpret this as a numerical expression, the element
parameter would evaluate to the numerical value 0, and the {\tt if} statement would evaluate to false.
\item how should AIMMS handle the assignment
\begin{example}
    anInteger := anInteger + 3;
\end{example}
if the values in {\tt anIntegerSet} are non-contiguous? If AIMMS would interpret {\tt anInteger} as an ordinary 
element parameter, the {\tt +} operator would refer to a lead operator 
(see also Section~\ref{sec:set-expr.elem.lag-lead}), and the assignment would assign
the third next element of {\tt anInteger} in the set {\tt anIntegerSet}. If AIMMS would interpret 
{\tt anInteger} as an numerical value, the assignment would assign the numerical value of {\tt anInteger} plus 3,
assuming that this is an element of {\tt anIntegerSet}.
\end{itemize}
You can resolve this ambiguity assigning one of the properties {\tt ElementsAreLabels} and {\tt ElementsAreNumerical} to {\tt anIntegerSet}.
If you don't assign either property, and you use one of these expressions in your model, AIMMS will issue a warning about the ambiguity, and the 
end result might be unpredictable.

\paragraph{Construction}
\index{set!enumerated} \index{enumerated set}

In order to fill an integer set AIMMS provides the special operator ``{\tt
..}'' to specify an entire range of integer elements. This powerful feature is
discussed in more detail in Section~\ref{sec:set-expr.set.enum}.

\paragraph{Example}

The following somewhat abstract example demonstrates some of the features of
integer sets. Consider the following declarations.
\begin{example}
Parameter LowInt {
    Range      : Integer;
}
Parameter HighInt {
    Range      : Integer;
}
Set EvenNumbers {
    SubsetOf    : Integers;
    Index       : i;
    Parameter   : LargestPolynomialValue;
    OrderBy     : - i;
}
\end{example}
The following statements illustrate some of the possibilities to compute
integer sets on the basis of integer expressions, or to use the elements of an
integer set in expressions.
\begin{example}
    ! Fill the integer set with the even numbers between
    ! LowInt and HighInt. The first term in the expression
    ! ensures that the first integer is even.

    EvenNumbers := { (LowInt + mod(LowInt,2)) .. HighInt by 2 };

    ! Next the square of each element i of EvenNumbers is added
    ! to the set, if not already part of it (i.e. the union results)

    for ( i | i <= HighInt ) do
        EvenNumbers += i^2;
    endfor;

    ! Finally, compute that element of the set EvenNumbers, for
    ! which the polynomial expression assumes the maximum value.

    LargestPolynomialValue := ArgMax( i, i^4 - 10*i^3 + 10*i^2 - 100*i );
\end{example}

\paragraph{Ordering integer sets}
\index{sorting sets!integer} \declattrindex{set}{OrderBy}

By default, integer sets are ordered according to the numeric value of their
elements. Like with ordinary simple sets, you can override this default
ordering by using the {\tt OrderBy} attribute. When you use an index in
specifying the order of an integer set, AIMMS will interpret it as a numeric
expression.

\breaksubsection[0.8]{Relations}\label{sec:set.relation}

\paragraph{Relation}
\index{set!relation} \index{relation}

A {\em relation} or multidimensional set is the Cartesian product of a number
of simple sets or a subset thereof. Relations are typically used as the domain
space for multidimensional identifiers. Unlike simple sets, the elements of a relation 
cannot be referenced using a single index.

\paragraph{Tuples and index components}
\index{tuple}\index{index tuple} \index{index component}

An element of a relation is called a {\em tuple} and is denoted by the usual
mathematical notation, i.e.\ as a parenthesized list of comma-separated
elements. Throughout, the word {\em index component} will be used to denote the
index of a particular position inside a tuple.

\paragraph{Index tuple}

To reference an element in a relation, you can use an {\em index tuple}, in
which each tuple component contains an index corresponding to a simple set.

\paragraph{The {\tt SubsetOf} attribute}

The {\tt SubsetOf} attribute is mandatory for relations, and must contain the {\em subset domain} of the set. This subset domain
is denoted either as a parenthesized comma-separated list of simple set
identifiers, or, if it is a subset of another relation, just
the name of that set.

\paragraph{Example}

The following example demonstrates some elementary declarations of a relation, 
given the two-dimensional parameters {\tt Distance(i,j)} and
{\tt Transport\-Cost(i,j)}. The following set declaration defines a relation.
\begin{example}
Set HighCostConnections {
    SubsetOf   : (Cities, Cities);
    Definition : {
        { (i,j) | Distance(i,j) > 0 and TransportCost(i,j) > 100 }
    }
}
\end{example}

\subsection{Indexed sets}\label{sec:set.indexed}
\index{indexed set} \index{set!indexed}

\paragraph{Definition}

An {\em indexed set} represents a family of sets defined for all elements in
another set, called the {\em index domain}. The elements of all members of the
family must be from a single (sub)set. Although membership tables allow you to
reach the same effect, indexed sets often make it possible to express certain
operations very concisely and intuitively.

\paragraph{The {\tt IndexDomain} attribute}\herelabel{attr:set.index-domain}
\declattrindex{indexed set}{IndexDomain} \AIMMSlink{set.index_domain}
\index{index domain}

A set becomes an indexed set by specifying a value for the {\tt IndexDomain}
attribute. The value of this attribute must be a single index or a tuple of
indices, optionally followed by a logical condition. The precise syntax of the
{\tt IndexDomain} attribute is discussed on
page~\pageref{attr:par.index-domain}.

\paragraph{Example}

The following declarations illustrate some indexed sets with a content that
varies for all elements in their respective index domains.
\begin{example}
Set SupplyCitiesToDestination {
    IndexDomain  : j;
    SubsetOf     : Cities;
    Definition   : {
        { i | Transport(i,j) }
    }
}
Set DestinationCitiesFromSupply {
    IndexDomain  : i;
    SubsetOf     : Cities;
    Definition   : {
        { j | Transport(i,j) }
    }
}
Set IntermediateTransportCities {
    IndexDomain  : (i,j);
    SubsetOf     : Cities;
    Definition   : DestinationCitiesFromSupply(i) * SupplyCitiesToDestination(j);
    Comment      : {
        All intermediate cities via which an indirect transport
        from city i to city j with one intermediate city takes place
    }
}
\end{example}
The first two declarations both define a one-dimensional family of subsets of
{\tt Cities}, while the third declaration defines a two-dimensional family of
subsets of {\tt Cities}. Note that the {\tt
*} operator is applied to sets, and therefore denotes intersection.

\paragraph{Subset domains}

The subset domain of an indexed set family can be either a simple set
identifier, or another family of indexed simple sets of the same or lower dimension. 
The subset domain of an indexed set {\em cannot} be a relation.

\paragraph{No default indices}
\index{indexed set!indexing}

Declarations of indexed sets do not allow you to specify either the {\tt Index}
or {\tt Parameter} attribute. Consequently, if you want to use an indexed set
for indexing, you must locally bind an index to it. For more details on the use
of indices and index binding refer to Sections~\ref{sec:set.index}
and~\ref{sec:bind.rules}.

\section{String expressions}\label{sec:set-expr.string}
\index{expression!string} \index{string expression}

\paragraph{String expressions}

String expressions are useful for
\begin{itemize}
\item  creating descriptive texts associated with particular set elements and identifiers, or
\item forming customized messages for display in the graphical user
interface or in output reports.
\end{itemize}
This section discusses all available string expressions in AIMMS.

\paragraph[.7]{Syntax}
\syntaxdiagram{string-expression}{string-expr} The format of list expressions
are the same for string-valued and numerical expressions. They are discussed in
Section~\ref{sec:expr.num.list}.

\subsection{String operators}\label{sec:set-expr.string.operators}

\paragraph{String operators}

There are three binary string operators in AIMMS, string concatenation ({\tt
+} operator), string subtraction ({\tt -} operator), and string repetition
({\tt *} operator). There are no unary string operators.

\paragraph{String concatenation}\operindex{+}
\index{string!concatenation}

The simplest form of composing strings in AIMMS is by the concatenation of
two existing strings. String concatenation is represented as a simple addition
of strings by means of the {\tt +} operator.

\paragraph{String subtraction}\operindex{-}
\index{string!subtraction}

In addition to string concatenation, AIMMS also supports subtraction of two
strings by means of the {\tt -} operator. The result of the operation $s_1 -
s_2$ where $s_1$ and $s_2$ are string expressions will be the substring of
$s_1$ obtained by
\begin{itemize}
\item omitting $s_2$ on the right of $s_1$ when $s_1$ ends in the
string $s_2$, or
\item just $s_1$ otherwise.
\end{itemize}

\paragraph{String repetition}\operindex{*}
\index{string!repetition}

You can use the multiplication operator {\tt *} to obtain the string that is
the result of a given number of repetitions of a string. The left-hand operand
of the repetition operator {\tt *} must be a string expression, while the
right-hand operand must be an integer numerical expression.

\paragraph{Examples}
The following examples illustrate some basic string manipulations in AIMMS.
\begin{example}
    "This is "     + "a string"            ! "This is a string"
    "Filename.txt" - ".txt"                ! "Filename"
    "Filename"     - ".txt"                ! "Filename"
    "--"           * 5                     ! "----------"
\end{example}

\subsection{Formatting strings}\label{sec:set-expr.string.format}

\paragraph{The function {\tt FormatString}}
\strfuncindex{FormatString} \AIMMSlink{formatstring} \index{formatting strings}
\index{string!formatting} \index{function!string} \index{string function}

With the {\tt FormatString} function you can compose a string that is built up
from combinations of numbers, strings and set elements. Its arguments are:
\begin{itemize}
\item a {\em format string}, which specifies how the string is composed, and
\item one or more {\em arguments} (number, string or element) which are used to
form the string as specified.
\end{itemize}

\paragraph{The format string}
\index{formatting strings!conversion specifier} \index{conversion specifier}
\index{format specification!in FormatString function@{in {\tt FormatString}
function}}

The first argument of the function {\tt FormatString} is a mixture of ordinary
text plus {\em conversion specifiers} for each of the subsequent arguments. A
conversion specifier is a code to indicate that data of a specified type is to
be inserted as text. Each conversion specifier starts with the {\tt \%}
character followed by a letter indicating its type. The conversion specifier
for every argument type are given in
Table~\ref{table:set-expr.string.conv-codes}.
\begin{aimmstable}
\begin{tabular}{|c|l|}
\hline\hline
{\bf Conversion}  & \multicolumn{1}{c|}{{\bf Argument type}} \\
{\bf specifiers}  & \\
\hline
{\tt \%s}        & String expression \\
{\tt \%e}        & Element expression \\
{\tt \%f}        & Floating point number\\
{\tt \%g}        & Exponential format number\\
{\tt \%i}        & Integer expression  \\
{\tt \%n}        & Numerical expression \\
{\tt \%u}        & Unit expression \\
{\tt \%\%}       & \% sign \\
\hline\hline
\end{tabular}
\caption{Conversion codes for the {\tt FormatString} function}%
\label{table:set-expr.string.conv-codes}
\end{aimmstable}

\paragraph{Floating point vs. exponential format}

When using the {\tt \%f} or {\tt \%g} conversion specifier you explicitly choose
a floating point or exponential format, respectively.
The {\tt \%n} conversion specifier makes this choice for you.
If the absolute value of the corresponding argument 
is greater or equal to 1, {\tt \%n} assures that you get the shortest representation of
{\tt \%f} or {\tt \%g} (or even {\tt \%i} if the argument value is integral). However when 
a non zero width is specified, AIMMS assumes that the alignment of the decimal point is
important and thus {\tt \%n} will stick to the use of the floating point format as long as that fits
within the given width. 
If the absolute value of the corresponding argument is less than 1, {\tt \%n} uses
the floating point format as long as the result shows at least 1 significant digit.


\paragraph{Example}

In the example below, the current value of the parameter {\tt SmallVal} and
{\tt LargeVal} are 10 and 20, the current value of {\tt CapitalCity} is the
element {\tt 'Amsterdam'}, and {\tt UnitPar} is a unit-valued parameter with
value {\tt kton/hr}. The following calls to {\tt FormatString} illustrate its
use.
\begin{example}
   FormatString("The numbers %i and %i", 10, 20)                ! "The numbers 10 and 20"
   FormatString("The numbers %i and %i", SmallVal, LargeVal)    ! "The numbers 10 and 20"
   FormatString("The string %s", "is printed")                  ! "The string is printed"
   FormatString("The element %e", CapitalCity)                  ! "The element Amsterdam"
   FormatString("The unit is %u", UnitPar)                      ! "The unit is kton/hr"
   FormatString("The number %n", 4*ArcTan(1))                   ! "The number 3.141"
   FormatString("The large number %n", 1e+6)                    ! "The large number 1.000e+06"
   FormatString("The integer %n", 10)                           ! "The integer 10"
   FormatString("The fraction %n", 0.01)                        ! "The fraction 0.010"
   FormatString("The fraction %n", 0.0001)                      ! "The fraction 1.000e-04"
\end{example}

\paragraph{Modification flags}
\index{formatting strings!alignment} \index{formatting strings!field width}
\index{formatting strings!precision} \index{modification flag}

By default, AIMMS will use a default representation for arguments of each
type. By modifying the conversion specifier, you further dictate the manner in
which a particular argument of the {\tt FormatString} function is printed. {\em
This is done by inserting modification flags in between the \%-sign and the
conversion character}. The following modification directives can be added:
\begin{itemize}
\item{\em flags}:
  \begin{description}
  \item[{\tt <}]  for left alignment
  \item[{\tt <>}] for centered alignment
  \item[{\tt >}]  for right alignment
  \item[{\tt +}]  add a plus sign (nonnegative numbers)
  \item[{\tt \textvisiblespace}]  add a space (instead of the above
  {\tt +} sign)
  \item[{\tt 0}]  fill with zeroes (right-aligned numbers only)
  \item[{\tt t}]  print number using thousand separators, 
                  using local convention for both the thousand separator
				  and decimal separator.  Controlling these separators
				  is via the options {\tt Number 1000 separator} and
				  {\tt Number decimal separator}.
  \end{description}
\item {\em field width}: the converted argument will be printed in a
field of at least this width, or wider if necessary
\item {\em dot}: separating the field width from the precision
\item {\em precision}: the number of decimals for numbers, or the
maximal number of characters for strings or set elements.
\end{itemize}

\paragraph{Note the order}

It is important to note that the modification flags must be inserted in the
order as described above.

\paragraph{Field width and precision}

Both the field width and precision of a conversion specifier can be either an
integer constant, or a wildcard, {\tt *}. In the latter case the {\tt
FormatString} expects one additional integer argument for each wildcard just
before the argument of the associated conversion specifier. This allows you to
compute and specify either the field width or precision in a dynamic manner.
If you do not specify a precision as modification directive, the default precision is taken from 
the option {\tt Listing\_number\_precision}. Similarly, the default width is taken from
the option {\tt Listing\_number\_width}.

\paragraph{Example}

The following calls to {\tt FormatString} illustrate the use of modification
flags.
\begin{example}
   FormatString("The number %>+08i", 10)               ! "The number +0000010"
   FormatString("The number %>t8i", 100000)            ! "The number  100,000"
   FormatString("The number %> 8.2n", 4*ArcTan(1))     ! "The number     3.14"
   FormatString("The number %> *.*n", 8,2,4*ArcTan(1)) ! "The number     3.14"
   FormatString("The element %<5e", CapitalCity)       ! "The element Amsterdam"
   FormatString("The element %<>5.3e", CapitalCity)    ! "The element  Ams "
   FormatString("The large number %10.1n", 1e+6)       ! "The large number  1000000.0"
\end{example}

\tipparagraph{Special characters}{special-char} \index{formatting
strings!special characters}

AIMMS offers a number of special characters to allow you to use the full
range of characters in composing strings. These special characters are
contained in Table~\ref{table:set-expr.string.spec-kars}.
\begin{aimmstable}
\begin{tabular}{|l|l|l|}
\hline\hline
{\bf Special character} & {\bf text code} & {\bf Meaning} \\
\hline
\verb+\f+         & FF         & Form feed  \\
\verb+\t+         & HT         & Horizontal tab  \\
\verb+\n+         & LF         & Newline character  \\
\verb+\"+         & \verb+"+   & Double quote    \\
\verb+\\+         & \verb+\+   & Backslash       \\
\verb+\+$n$       & $n$        & character $n$ ($001\leq n\leq 65535$)\\
\hline\hline
\end{tabular}
\caption{Special characters} \label{table:set-expr.string.spec-kars}
\end{aimmstable}

\paragraph{Example}

Examples of the use of special characters within {\tt FormatString} follow.
\begin{example}
    FormatString("%i \037 \t %i %%", 10, 11)     ! "10 %       11 %"
    FormatString("This is a \"%s\" ", "string")  ! "This is a "string" "
\end{example}

\paragraph{Case conversion functions}%
\strfuncindex{StringToUpper}%
\strfuncindex{StringToLower}%
\strfuncindex{StringCapitalize} \AIMMSlink{stringtoupper}
\AIMMSlink{stringtolower} \AIMMSlink{stringcapitalize} \index{function!string}
\index{string function}

With the functions {\tt StringToUpper}, {\tt StringToLower} and {\tt
StringCapitalize} you can convert the case of a string to upper case, to lower
case, or capitalize it, as illustrated in the following example.
\begin{example}
    StringToUpper("Convert to upper case")    ! "CONVERT TO UPPER CASE"
    StringToLower("CONVERT to lower case")    ! "convert to lower case"
    StringCapitalize("capitaLIZED senTENCE")  ! "Capitalized sentence"
\end{example}

\subsection{String manipulation}\label{sec:set-expr.string.functions}
\index{string!manipulation} \index{function!string} \index{string function}

\paragraph{Other string related functions}

In addition to the {\tt FormatString} function, AIMMS offers a number of
other functions for string manipulation. They are:
\begin{itemize}
\item {\tt Substring} to obtain a substring of a particular string,
\item {\tt StringLength} to determine the length of a particular string,
\item {\tt FindString} to obtain the position of the first occurrence of a
particular substring,
\item {\tt FindNthString} to obtain the position of the $n$-th
occurrence of a particular substring, and
\item {\tt StringOccurrences} to obtain the number of occurrences of a
particular substring.
\end{itemize}

\paragraph{The function {\tt SubString}}\strfuncindex{Substring}
\AIMMSlink{substring}

With the {\tt SubString} function you can obtain a substring from a particular
begin position $m$ to an end position $n$ (or to the end of the string if the
requested end position exceeds the total string length). The positions $m$ and
$n$ can both be negative (but with $m \leq n$), in which case AIMMS will
start counting backwards from the end of the string. Examples are:
\begin{example}
    SubString("Take a substring of me", 8, 16)    ! returns "substring"
    SubString("Take a substring of me", 18, 100)  ! returns "of me"
    SubString("Take a substring of me", -5, -1)   ! returns "of me"
\end{example}

\paragraph{The function {\tt StringLength}}\strfuncindex{StringLength}

The function {\tt StringLength} can be used to determine the length of a string
in AIMMS. The function will return 0 for an empty string, and the total
number of characters for a nonempty string. An example follows.
\begin{example}
    StringLength("Guess my length")               ! returns 15
\end{example}

\paragraph{The functions {\tt FindString} and {\tt FindNthString}}%
\strfuncindex{FindString}\strfuncindex{FindNthString}
\AIMMSlink{findstring}\AIMMSlink{findnthstring}

With the functions {\tt FindString} and {\tt FindNthString} you can determine
the position of the second argument, the {\em key}, within the first argument,
the {\em search} string. The functions return zero if the key is not contained
in the search string. The function {\tt FindString} returns the position of the
first occurrence of the key in the search string starting from the left, while
the function {\tt FindNth\-String} will return the position of the $n$-th
appearance of the key. If $n$ is negative, the function {\tt FindNthString}
will search backwards starting from the right. Examples are:
\begin{example}
    FindString      ("Find a string in a string", "string"     )  ! returns 8
    FindNthString   ("Find a string in a string", "string",  2 )  ! returns 20
    FindNthString   ("Find a string in a string", "string", -1 )  ! returns 20

    FindString      ("Find a string in a string", "this string")  ! returns 0
    FindNthString   ("Find a string in a string", "string",  3 )  ! returns 0
\end{example}

\paragraph{Case sensitivity}

By default, the functions {\tt FindString} and {\tt FindNthString} will use a
case sensitive string comparison when searching for the key. You can modify
this behavior through the option {\tt Case\_Sensitive\_String\_Comparison}.

\paragraph{The function {\tt StringOccurrences}}
\strfuncindex{StringOccurrences}\AIMMSlink{stringoccurences}

The function {\tt StringOccurrences} allows you to determine the number of
occurrences of the second argument, the {\tt key}, within the first argument,
the {\em search} string. You can use this function, for instance, to delimit
the number of calls to the function {\tt FindNthString} a priori. An example
follows.
\begin{example}
    StringOccurrences("Find a string in a string", "string"     )  ! returns 2
\end{example}

\subsection{Converting strings to set
elements}\label{sec:set-expr.string.convert} \index{string!convert to element}
\index{conversion!string to element}

\paragraph{Converting strings to set elements}

Converting strings to new elements to or renaming existing elements in a set is
not an uncommon action when end-users of your application are entering new
element interactively or when you are obtaining strings (to be used as set
elements) from other applications through external procedures. AIMMS offers
the following support for dealing with such situations:
\begin{itemize}
\item the procedure {\tt SetElementAdd} to add a new element to a set,
\item the procedure {\tt SetElementRename} to rename an existing
element in a set, and
\item the function {\tt StringToElement} to convert strings to set elements.
\end{itemize}

\paragraph{Adding new set elements}\procindex{SetElementAdd}
\AIMMSlink{setelementadd}

The procedure {\tt SetElementAdd} lets you add new elements to a set. Its
arguments are:
\begin{itemize}
\item the {\em set} to which you want to add the new element,
\item an {\em element parameter} into {\em set} which holds the new
element after addition, and
\item the {\em stringname} of the new element to be added.
\end{itemize}
When you apply {\tt SetElementAdd} to a root set, the element will be added to
that root set. When you apply it to a subset, the element will be added to the
subset as well as to all its supersets, up to and including its associated root
set.

\paragraph{Renaming set elements}\procindex{SetElementRename}
\AIMMSlink{setelementrename}

Through the procedure {\tt SetElementRename} you can provide a new name for an
existing element in a particular set whenever this is necessary in your
application. Its arguments are:
\begin{itemize}
\item the {\em set} which contains the element to be renamed,
\item the {\em element} to be renamed, and
\item the {\em stringname} to which the element should be renamed.
\end{itemize}
After renaming the element, all data defined over the old element name will be
available under the new element name.

\paragraph{The function {\tt StringToElement}}\efuncindex{StringToElement}
\AIMMSlink{stringtoelement}

With the function {\tt StringToElement} you can convert string arguments into
(existing) elements of a set. If there is no such element, the function
evaluates to the empty element. Its arguments are:
\begin{itemize}
\item the {\em set} from which the element corresponding to {\em
stringname} must be returned,
\item the {\em stringname} for which you want to retrieve the corresponding
element, and
\item the optional {\em create} argument (values 0 or 1, with a
default of 0) indicating whether nonexisting elements must be added to the set.
\end{itemize}
With the {\em create} argument set to 1, a call to {\tt StringToElement} will
always return an element in {\em set}. Alternatively to setting the {\em
create} argument to 1, you can call the procedure {\tt SetElementAdd} to add
the element to the set.

\paragraph{Example}

The following example illustrates the combined use of {\tt StringToElement} and
{\tt SetElementAdd}. It checks for the existence of the string parameter {\tt
CityString} in the set {\tt Cities}, and adds it if necessary.
\begin{example}
    ThisCity := StringToElement( Cities, CityString );
    if ( not ThisCity ) then
       SetElementAdd( Cities, ThisCity, CityString );
    endif;
\end{example}
Alternatively, you can combine both statements by setting the optional {\em
create} argument of the function {\tt StringToElement} to 1.
\begin{example}
    ThisCity := StringToElement( Cities, CityString, create: 1 );
\end{example}

\paragraph{Converting element to string}
\index{element!convert to string} \index{conversion!element to string}

Reversely, you can use the {\tt \%e} specifier in the {\tt FormatString}
function to get a pure textual representation of a set element, as illustrated
in the following assignment.
\begin{example}
    CityString := FormatString("%e", ThisCity );
\end{example}

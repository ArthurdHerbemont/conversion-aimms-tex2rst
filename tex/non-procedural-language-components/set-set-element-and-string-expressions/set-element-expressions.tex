\section{Set element expressions}\label{sec:set-expr.elem}
\index{element expression} \index{expression!element}

\paragraph{Use of set element expressions}

Set element expressions reference a particular element or element tuple model
from a set or a tuple domain. Set element expressions allow for {\em sliced
assign\-ment}---executing an assignment only for a lesser-dimensional subdomain
by fixing certain dimensions to a specific set element. Potentially, this may
lead to a vast reduction in execution times for time-consuming calculations.

\paragraph{Passing elements from the GUI}

The most elementary form of a set element expression is an element parameter,
which turns out to be a useful device for communicating set element information
with the graphical interface. You can instruct AIMMS to locate the position
in a table or other object where an end-user made changes to a numerical value,
and have AIMMS pass the corresponding set element(s) to an element
parameter. As a result, you can execute data input checks defined over these
element parameters, thereby limiting the amount of computation. This issue is
discussed in more detail in the help regarding the Identifier Selection dialog.

\paragraph{Element expressions}

AIMMS supports several types of set element expressions, including {\sf\it
references} to parameters and (bound) indices, {\sf\it lag-lead-expressions},
element-valued {\sf\it functions}, and {\sf\it iterative-expressions}. The last
category turns out to be a useful device for computing the proper value of
element parameters in your model.

\paragraph{Syntax}
\syntaxdiagram{element-expression}{elem-expr} \syntaxcomment The format of list
expressions are the same for element and numerical expressions. They are
discussed in Section~\ref{sec:expr.num.list}.

\paragraph{Element references}
\index{element!reference} \index{reference!element}

An element reference is any reference to either an element parameter or a
(bound) index. 

\subsection{Intrinsic functions for sets and set elements}\label{sec:set-expr.elem.functions}

\paragraph{The element-related
functions\dots}%
\index{element-valued function} \index{function!element-valued}
\funcindex{Ord}\funcindex{Card}\efuncindex{Element}\efuncindex{ElementCast}\efuncindex{Val}
\AIMMSlink{ord}\AIMMSlink{card}\AIMMSlink{element}\AIMMSlink{elementcast}\AIMMSlink{val}

AIMMS supports functions to obtain the position of an element within a set,
the cardinality (i.e.\ number of elements) of a set, the $n$-th element in a
set, the element in a non-compatible set with the identical string
representation, and the numerical value represented by a set element. If
\verb|S| is a set identifier, \verb|i| an index bound to \verb|S|, $l$ an
element, and $n$ a positive integer, then possible calls to the \verb|Ord|,
{\tt Card}, {\tt Element}, {\tt ElementCast} and {\tt Val} functions are given
in Table~\ref{table:set-expr.set-func}.
\begin{aimmstable}
\begin{tabular}{|l|c|p{6.6cm}|}
\hline\hline
{\bf Function}     &   {\bf Value}& {\bf Meaning}\\
\hline \verb|Ord(i)| & {\sf\it integer} & Ordinal, returns the relative
position of the index
                        \verb|i| in the set \verb|S|. Does {\em not}
                        bind \verb|i|.\\
\verb|Ord(|$l$\verb|,S)|& {\sf\it integer} &   Returns the relative position of
the element $l$ in set \verb|S|. Returns zero if $l$
is not an element of \verb|S|.\\
{\tt Card(S)} & {\sf\it integer} & Cardinality of set {\tt S}. \\
{\tt Element(S,}$n${\tt )} & {\sf\it element} & Returns the element in set {\tt
S} at relative position $n$. Returns the empty element tuple if {\tt S}
contains less
then $n$ elements.\\
{\tt ElementCast(S},$l${\tt )} & {\sf\it element} & Returns the element in set
{\tt S}, which corresponds to the textual
representation of an element $l$ in any other index set.\\
{\tt Val(}$l${\tt )} & {\sf\it numerical} & Returns the numerical value
represented by $l$, or a runtime error if $l$ cannot be interpreted as
a number\\
$\mathtt{Max}(e_1,\dots,e_n)$ & {\sf\it Max}                   & Returns the set element with the highest ordinal\\
$\mathtt{Min}(e_1,\dots,e_n)$ & {\sf\it Min}                   & Returns the set element with the lowest ordinal\\
\hline\hline
\end{tabular}
\caption{Intrinsic functions operating on sets and set elements}\label{table:set-expr.set-func}
\end{aimmstable}

\paragraph{\dots for simple sets}

The {\tt Ord}, {\tt Card} and {\tt Element} functions can be applied to 
simple sets. In fact you can even apply \verb|Card| to parameters
and variables---it simply returns the number of nondefault elements associated
with a certain data structure.

\paragraph{Crossing root set boundaries}

By default, AIMMS does not allow you to use indices associated with one root
set hierarchy in your model, in references to index domains associated with
another root set hierarchy of your model. The function {\tt ElementCast} allows
you to cross root set boundaries, by returning the set element in the root set
associated with the first (set) argument that has the identical name as the
element (in another root set) passed as the second argument. The function {\tt
ElementCast} has an optional third argument {\em create} (values 0 or 1, with a
default of 0), through which you can indicate whether you want elements which
cannot be cast to the indicated set must be created within that set. In this
case, a call to {\tt ElementCast} will never fail. You can find more
information about root sets, as well as an illustrative example of the use of
{\tt ElementCast}, in Section~\ref{sec:bind.rules}.

\paragraph{Example}
In this example, we again use the set {\tt Cities} initialized through the
statement
\begin{example}
    Cities := DATA { Amsterdam, Rotterdam, 'The Hague', London, Paris, Berlin, Madrid } ;
\end{example}
The following table illustrates the intrinsic element-valued functions.
\begin{aimmstable}
\begin{tabular}{|l|c|}
\hline\hline
{\bf Expression} & {\bf Result} \\
\hline
   \verb|Ord('Amsterdam', Cities)|  &   1 \\
    \verb|Ord('New York', Cities)|  &   0 (i.e.\ not in the set)\\
    \hline
    \verb|Card(Cities)|        & 7 \\
    \hline
    \verb|Element(Cities, 1)|  & \verb|'Amsterdam'| \\
    \verb|Element(Cities, 8)|  & \verb|''| (i.e.\ no 8-th element)\\
\hline\hline
\end{tabular}
\end{aimmstable}

\paragraph{The {\tt Val} function}
\efuncindex{Val}

If your model contains a set with elements that represent numerical values, you
cannot directly use such elements as a numerical value in numerical
expressions, unless the set is an integer set (see
Section~\ref{sec:set.integer}). To obtain the numerical value of such set
elements, you can use the {\tt Val} function. You can also apply the {\tt Val}
function to strings that represent a numerical value. In both cases, a runtime
error will occur if the element or string argument of the {\tt Val} function
cannot be interpreted as a numerical value.

\paragraph{The {\tt Min} and {\tt Max} functions}
\efuncindex{Min}\efuncindex{Min}

The element-valued {\tt Min} and {\tt Max} functions operate on two or more element-valued expressions {\em in the same (sub-)set hierarchy}. 
If the arguments are references to element parameters (or bound indices), then the {\tt Range} attributes of these element parameters or indices must be sets in a single set hierarchy.
Through these functions you can obtain the elements with the lowest and highest ordinal relative to the set equal to highest ranking range set in the subset hierarchy of all its arguments. 
If one or more of the arguments are explicit labels, then AIMMS will verify that these labels are contained in that set, or will return an error otherwise. A compiler error will result, if no such set can be determined (i.e., when the function call refers to explicit labels only).

\subsection{Element-valued iterative expressions}\label{sec:set-expr.elem.iter}

\paragraph{Selecting elements}%
\index{element-valued iterative operator} \index{iterative
operator!element-valued}
\eliterindex{First}\eliterindex{Last}\eliterindex{Nth}\eliterindex{ArgMin}\eliterindex{ArgMax}\eliterindex{Min}\eliterindex{Max}
\AIMMSlink{first}\AIMMSlink{last}\AIMMSlink{nth}\AIMMSlink{argmin}\AIMMSlink{argmax}

AIMMS offers special iterative operators that let you select a specific
element from a domain. Table~\ref{table:set-expr.elem-iter}
shows all such operators that result in a set element value. The syntax of
iterative operators is explained in Section~\ref{sec:set-expr.set.sort}. The
second column in this table refers to the required number of expression
arguments following the binding domain argument.

\begin{aimmstable}
\begin{tabular}{|l|c|p{7cm}|}
\hline\hline
{\bf Name} & {\bf \# Expr.}   & {\bf Computes for all elements in the domain} \\
\hline
{\tt First}& 0 & the first element (tuple)  \\
{\tt Last} & 0 & the last element (tuple)\\
{\tt Nth}  & 1 & the $n$-th element (tuple)\\
{\tt Min}& 1& \PBS\raggedright the value of the element expression for which
the expression reaches its minimum ordinal value\\
{\tt Max}& 1& \PBS\raggedright the value of the element expression for which
the expression reaches its maximum ordinal value\\
{\tt ArgMin}& 1& \PBS\raggedright the first element (tuple) for which
the expression reaches its minimum value\\
{\tt ArgMax}& 1& \PBS\raggedright the first element (tuple)
for which the expression reaches its maximum value\\
\hline\hline
\end{tabular}
\caption{Element-valued iterative operators}\label{table:set-expr.elem-iter}
\end{aimmstable}

\paragraph{Single index}

The binding domain of the {\tt First}, {\tt Last}, {\tt Nth}, {\tt Min}, {\tt Max}, {\tt ArgMin}, and
{\tt ArgMax} operator can only consist of a single index in either a simple set, and the result is a single element in that domain. You can use
this result directly for indexing or referencing an indexed parameter or
variable. Alternatively, you can assign it to an element parameter in the
appropriate domain.

\paragraph{Compared expressions}

The {\tt ArgMin} and {\tt ArgMax} operators return the element for which an
expression reaches its minimum or maximum value. The allowed expressions are:
\begin{itemize}
\item numerical expressions, in which case AIMMS performs a numerical
comparison,
\item string expressions, in which case AIMMS uses the normal
alphabetic ordering, and
\item element expressions, in which case AIMMS compares the ordinal
numbers of the resulting elements. 
\end{itemize}
For element expressions, the iterative {\tt Min} and {\tt Max} operators return 
expression {\em values} with the minimum and maximum ordinal value.

\paragraph[0.9]{Example}
The following assignments illustrate the use of some of the domain related
iterative operators. The identifiers on the left are all element parameters.
\begin{example}
    FirstNonSupplyCity    := First ( i | not Exists(j | Transport(i,j)) ) ;
    SecondSupplyCity      := Nth   ( i | Exists(j | Transport(i,j)), 2  ) ;
    SmallestSupplyCity    := ArgMin( i, Sum(j, Transport(i,j))          ) ;
    LargestTransportRoute := ArgMax( r, Transport(r)                    ) ;
\end{example}
Note that the iterative operators {\tt Exists} and {\tt Sum} are used here for
illustrative purposes, and are not set- or element-related. They are treated in
Sections~\ref{sec:expr.logic.iter} and~\ref{sec:expr.num.iter}, respectively.

\subsection{Lag and lead element operators}\label{sec:set-expr.elem.lag-lead}
\index{lag/lead operator} \index{operator!lag/lead}

\paragraph{Lag and lead operators\dots}

There are four binary element operators, namely the lag and lead operators
\verb|+|, \verb|++|, \verb|-| and \verb|--|. The first operand of each of these
operators must be an element reference (such as an index or element parameter),
while the second operand must be an integer numerical expression. There are no
unary element operators.

\paragraph{\dots explained}

Lag and lead operators are used to relate an index or element parameter to
preceding and subsequent elements in a set. Such correspondence is
well-defined, except when a request extends beyond the bounds of the set.

\paragraph{Noncircular versus circular}
\operindex{+}\operindex{-}\operindex{++}\operindex{--}

There are two kinds of lag and lead operators, namely {\em noncircular} and
{\em circular} operators which behave differently when pushed beyond the
beginning and the end of a set.
\begin{itemize}
\item
The noncircular operators (\verb|+| and \verb|-|) consider the ordered set
elements as a {\em sequence} with no elements before the first element or after
the last element.
\item
The circular operators (\verb|++| and \verb|--|) consider ordered set elements
as a {\em circular chain}, in which the first and last elements are linked.
\end{itemize}

% \paragraph{Syntax}
% \syntaxdiagram{lag-lead-expression}{lag-lead-expr}

\paragraph{Definition}

Let \verb|S| be a set, \verb|i| a set element expression, and $k$ an
integer-valued expression. The lag and lead operators \verb|+|,
\verb|++|, \verb|-|, \verb|--| return the element of {\tt S} as
defined in Table~\ref{table:set-expr.lag-lead}. Please note that
these operators are also available in the form of \verb|+=|,
\verb|-=|, \verb|++=| and \verb|--=|. The operators in this form can
be used in statements like:
\begin{example}
    CurrentCity := 'Amsterdam';
    CurrentCity --= 1; ! Equal to CurrentCity := CurrentCity -- 1;
\end{example}

\begin{aimmstable}
\begin{tabular}{|c|p{8cm}|}
\hline\hline
{\bf Lag/lead expr.}  & {\bf Meaning} \\
\hline $\mathtt{i}\mathbin{\mathrm{+}}k$  & The element of \verb|S| positioned
$k$
                                elements after \verb|i|; the empty element
                                if there is no such element.\\
$\mathtt{i}\mathbin{\mathrm{++}}k$  & The circular version of
                                $\mathtt{i}\mathbin{\mathtt{+}}k$.\\
$\mathtt{i}\mathbin{\mathrm{-}}k$  & The member of \verb|S| positioned $k$
                               elements before \verb|i|; the empty element
                                if there is no such element.\\
$\mathtt{i}\mathbin{\mathrm{--}}k$  & The circular version of
                                   $\mathtt{i}\mathbin{\mathtt{-}}k$.\\
\hline\hline
\end{tabular}
\caption{Lag and lead operators}\label{table:set-expr.lag-lead}
\end{aimmstable}

\paragraph{Lag and lead operators for integer sets}
\index{lag/lead operator!with integer sets}

For elements in integer sets, AIMMS may interpret the {\tt +} and {\tt -} operators either
as lag/lead operators or as numerical operators. Section~\ref{sec:set.integer} discusses the 
way in which you can steer which interpretation AIMMS will employ.

\paragraph{Not for literal elements}

You cannot always use lag and lead operators in combination with literal set
elements. The reason for this is clear: a literal element can be an element of
more than one set, and in general, unless the context in which the lag or lead
operator is used dictates a particular (domain) set, it is impossible for
AIMMS to determine which set to work with.

\paragraph{Verify the effect of lags and leads}

Lag and lead operators are frequently used in indexed parameters and variables,
and may appear on the left- and right-hand side of assignments. You should be
careful to check the correct use of the lag and lead operators to avoid making
conceptual errors. For more specific information on the lag and lead operators
refer to Section~\ref{sec:exec.assign}, which treats assignments to parameters
and variables.

\paragraph{Example}
Consider the set \verb|Cities| initialized through the assignment
\begin{example}
    Cities := DATA { Amsterdam, Rotterdam, 'The Hague', London, Paris, Berlin, Madrid } ;
\end{example}
Assuming that the index {\tt i} and the element parameter {\tt CurrentCity}
both currently refer to {\tt 'Rotterdam'},
Table~\ref{table:set-expr.lag-lead-examp} illustrates the results of various
lag/lead expressions.

\begin{aimmstable}
\begin{tabular}{|l|c|}
\hline\hline
{\bf Lag/lead expression}     &    {\bf Result}\\
\hline
%\qquad\verb|i|           &    \verb|'Rotterdam'|\\
\qquad\verb|i+1|        &    \verb|'The Hague'|\\
\qquad\verb|i+6|         &    \verb|''|\\
\qquad\verb|i++6|        &    \verb|'Amsterdam'|\\
\qquad\verb|i++7|        &    \verb|'Rotterdam'|\\
\qquad\verb|i-2|         &    \verb|''|\\
\qquad\verb|i--2|        &    \verb|'Madrid'|\\
\qquad\verb|CurrentCity+2| &  \verb|'London'|\\
\qquad\verb|'Rotterdam' + 1|    &    ERROR \\
\hline\hline
\end{tabular}
\caption{Example of lag and lead
operators}\label{table:set-expr.lag-lead-examp}
\end{aimmstable}


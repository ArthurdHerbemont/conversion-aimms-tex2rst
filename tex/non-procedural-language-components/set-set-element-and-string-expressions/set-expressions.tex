\section{Set expressions}\label{sec:set-expr.set}

\paragraph{Set expressions}
\index{set expression} \index{expression!set}

Set expressions play an important role in the construction of index domains of
indexed identifiers, as well as in constructing the domain of execution of
particular indexed statements. The AIMMS language offers a powerful set of
set expressions, allowing you to express complex set constructs in a clear and
concise manner.

\paragraph{Constant set expressions}
\index{expression!constant}

A set expression is evaluated to yield the value of a set. As with all
expressions in AIMMS, set expressions come in two forms, {\em constant} and
{\em symbolic}. Constant set expressions refer to explicit set elements
directly, and are mainly intended for set initialization. The tabular format of
set initialization is treated in Section~\ref{sec:text.table}.

\paragraph{Symbolic set expressions}
\index{expression!symbolic}

Symbolic set expressions are formulas that can be executed to result in a set.
The contents of this set can vary throughout the execution of your model
depending on the values of the other model identifiers referenced inside the
symbolic formulas. Symbolic set expressions are typically used for specifying
index domains. In this section various forms of set expressions will be
treated.

\paragraph[0.7]{Syntax}

\syntaxdiagram{set-primary}{set-primary}
\syntaxdiagram{set-expression}{set-expr}

\paragraph{Set references}
\index{set!reference} \index{reference!set}

The simplest form of set expression is the reference to a set. The reference
can be scalar or indexed, and evaluates to the current contents of that set.

\subsection{Enumerated sets}\label{sec:set-expr.set.enum}

\paragraph{Enumerated sets}
\index{set!enumerated} \index{enumerated set}

An {\em enumerated} set is a set defined by an explicit enumeration of its
elements. Such an enumeration includes literal elements, set element
expressions, and (constant or symbolic) element ranges. An enumerated set can
be either a simple or a relation. If you use an {\em integer element
range}, an integer set will result.

\paragraph{Constant enumerated sets}
\keyindex{DATA}

Enumerated sets come in two flavors: {\em constant} and {\em symbolic}.
Constant enumerated sets are preceded by the keyword {\tt DATA}, and must only
contain literal set elements. These set elements do not have to be contained in
single quotes unless they contain characters other than the alpha-numeric
characters, the underscore, the plus or the minus sign.

\paragraph{Example}\herelabel{examp:set-expr.cities}

The following simple set and relation assignments illustrate constant
enumerated set expressions.
\begin{example}
    Cities := DATA { Amsterdam, Rotterdam, 'The Hague', London, Paris, Berlin, Madrid } ;

    DutchRoutes := DATA { (Amsterdam, Rotterdam  ), (Amsterdam, 'The Hague'),
                          (Rotterdam, Amsterdam  ), (Rotterdam, 'The Hague') } ;
\end{example}

\paragraph{Symbolic enumerated sets}

Any enumerated set not preceded by the keyword {\tt DATA} is considered
symbolic. Symbolic enumerated sets can also contain element parameters. In
order to distinguish between literal set elements and element parameters, all
literal elements inside symbolic enumerated sets must be quoted.

\paragraph{Examples}

The following two set assignments illustrate the use of enumerated sets that
depend on the value of the element parameters {\tt SmallestCity}, {\tt
LargestCity} and {\tt AirportCity}.
\begin{example}
    ExtremeCities := { SmallestCity, LargestCity } ;

    Routes        := { (LargestCity, SmallestCity), (AirportCity, LargestCity )  } ;
\end{example}
The following two set assignments contrast the semantics between constant and
symbolic enumerated sets.
\begin{example}
     SillyExtremes := DATA { SmallestCity, LargestCity } ;
     ! contents equals { 'SmallestCity', 'LargestCity' }

     ExtremeCities := { SmallestCity, LargestCity, 'Amsterdam' };
     ! contents equals e.g. { 'The Hague', 'London', 'Amsterdam' }
\end{example}
The syntax of enumerated set expressions is as follows.

\paragraph{Syntax}
\syntaxdiagram{enumerated-set}{enumerated-set}
\hbox{\hbox to .5\textwidth{\vtop{%
\syntaxdiagram{element-tuple}{elem-tuple}}\hss}%
\hbox to .5\textwidth{\vtop{%
\syntaxdiagram{tuple-component}{tuple-comp}}\hss}}

All elements in an enumerated set must have the same dimension.

\paragraph{Element range}
\index{element range} \index{range!element} \index{range!..@{\tt ..}}
\operindex{..}

By using the {\tt ..} operator, you can specify an {\em element range}. An
element range is a sequence of consecutively numbered elements. The following
set assignments illustrate both constant and symbolic element ranges. Their
difference is explained below.
\begin{example}
    NodeSet       := DATA { node1 .. node100 } ;

    FirstNode     := 1;
    LastNode      := 100;

    IntegerNodes  := { FirstNode .. LastNode } ;
\end{example}
The syntax of element ranges is as follows.

\paragraph{Syntax}
\syntaxdiagram{element-range}{elem-range}
\syntaxdiagram{range-bound}{range-bound}

\paragraph{Prefix and postfix strings}

\syntaxmark{postfix-string} A \syntaxmark{prefix-string} range bound must
consists of an integer number, and can be preceded or followed by a common
prefix or postfix string, respectively. The prefix and postfix strings used in
the lower and upper range bounds must coincide.

\paragraph{Constant range}
\keyindex{DATA}

If you use an element range in a static enumerated set expression (i.e.\
preceded by the keyword {\tt DATA}), the range can only refer to explicitly
numbered elements, which need not be quoted.  By padding the numbered elements
with zeroes, you indicate that AIMMS should create all elements with the
same element length.

\paragraph{Constant range versus {\tt ElementRange}}
\sfuncindex{ElementRange}

As the begin and end elements of a constant element range are literal elements,
you cannot use a constant element range to create sets with dynamically
changing border elements. If you want to accomplish this, you should use the
{\tt ElementRange} function, which is explained in detail in
Section~\ref{sec:set-expr.set.functions}. Its use in the following example is
self-explanatory. The following set assignments illustrate a constant element
range and its equivalent formulation using the {\tt ElementRange} function.
\begin{example}
    NodeSet     := DATA { node1   .. node100 } ;
    PaddedNodes := DATA { node001 .. node100 } ;

    NodeSet     := ElementRange( 1, 100, prefix: "node", fill: 0 );
    PaddedNodes := ElementRange( 1, 100, prefix: "node", fill: 1 );
\end{example}

\paragraph{Symbolic integer range}
\index{element range!integer} \index{integer!element range}

Element ranges in a symbolic enumerated set can be used to create integer
ranges. Now, both bounds can be numerical expressions. Such a construct will
result in the {\em dynamic} creation of a number of {\em integer} elements
based on the value of the numerical expressions at the range bounds. Such integer element ranges can only be assigned to {\em integer} sets (see Section~\ref{sec:set.integer}). An example of a dynamic integer range follows.
\begin{example}
    IntegerNodes := { FirstNode .. LastNode } ;
\end{example}
In this example {\tt IntegerNodes} must be an integer set.

\paragraph{Nonconsecutive range}
\index{element range!nonconsecutive} \index{element range!BY modifier@{\tt BY}
modifier} \typindex{modifier}{BY} \AIMMSlink{by}

If the elements in the range are not consecutive but lie at regular intervals
from one another, you can indicate this by adding a {\tt BY} modifier with the
proper interval length. For static enumerated sets the interval length must be
a constant, for dynamic enumerated sets it can be any numerical expression. The
following set assignments illustrate a constant and symbolic element range with
nonconsecutive elements.
\begin{example}
    EvenNodes         := DATA { node2 .. node100 by 2 } ;

    StepSize          := 2;
    EvenIntegerNodes  := { FirstNode .. LastNode by StepSize } ;
\end{example}

\paragraph{Element tuples}
\index{enumerated set!relation}

When specifying element tuples in an enumerated set expression, it is possible
to create multiple tuples in a concise manner using cross products. You can
specify multiple elements for a particular tuple component in the cross product
either by grouping single elements using the {\tt [} and {\tt ]} operators or
by using an element range, as shown below.
\begin{example}
    DutchRoutes := DATA { ( Amsterdam, [Rotterdam, 'The Hague'] ),
                          ( Rotterdam, [Amsterdam, 'The Hague'] )  } ;
    ! creates  { ( 'Amsterdam', 'Rotterdam' ), ( 'Amsterdam', 'The Hague' ),
    !            ( 'Rotterdam', 'Amsterdam' ), ( 'Rotterdam', 'The Hague' ) }

    Network     := DATA { ( node1 .. node100, node1 .. node100 ) } ;
\end{example}
The assignment to the set {\tt Network} will create a set with 10,000 elements.

\subsection{Constructed sets}\label{sec:set-expr.set.constructed}

\paragraph{Constructed sets}
\index{constructed set} \index{set!constructed}

A {\em constructed set} expression is one in which the selection of elements is
constructed through filtering on the basis of a particular condition. When a
constructed set expression contains an index, AIMMS will consider the
resulting tuples for every element in the binding set.

\paragraph{Example}
The following set assignments illustrate some constructed set expressions,
assuming that {\tt i} and {\tt j} are indices into the set {\tt Cities}.
\begin{example}
    LargeCities := { i | Population(i) > 500000 } ;

    Routes := { (i,j) | Distance(i,j) } ;

    RoutesFromLargestCity := { (LargestCity, j) in Routes } ;
\end{example}
In the latter assignment route tuples are constructed from {\tt LargestCity}
(an element-valued parameter) to every city {\tt j}, where additionally each
created tuple is required to lie in the set {\tt Routes}.

\paragraph[0.9]{Syntax}
\syntaxdiagram{constructed-set}{construct-set}
\syntaxdiagram{binding-domain}{binding-dom}
\hbox{\hbox to .5\textwidth{\vtop{%
\syntaxdiagram{binding-tuple}{binding-tuple}}\hss}%
\hbox to .5\textwidth{\vtop{%
\syntaxdiagram{binding-element}{binding-el}}\hss}}

\paragraph{Binding domain}
\index{binding domain} \index{domain!binding} \index{index binding!binding
domain}

The tuple selection in a constructed set expression behaves exactly the same as
the tuple selection on the left-hand side of an assignment to an indexed
parameter. This means that all tuple components can be either an explicit
quoted set element, a general set element expression, or a binding index. The
tuple can be subject to a logical condition, further restricting the number of
elements constructed.

\subsection{Set operators}\label{sec:set-expr.set.operators}%
\index{operator!set} \operindex{*}\operindex{+}\operindex{-}\operindex{CROSS}
\index{Cartesian product}\index{product!Cartesian}
\index{intersection}\index{union}\index{difference!set}
\index{set!intersection}\index{set!union}\index{set!difference}
\AIMMSlink{cross}

\paragraph{Four set operators}

\syntaxmark{set-operator} There are four binary set operators in AIMMS:
Cartesian product, intersection, union, and difference. Their notation and
precedence are given in Table~\ref{table:set-expr.set-op}. Expressions
containing these set operators are read from left to right and the operands can
be any set expression. There are no unary set operators.

\begin{aimmstable}
\begin{tabular}{|l|c|l|}
\hline\hline
{\bf Operator}    &         {\bf Notation}         &      {\bf Precedence} \\
\hline
intersection &              \verb|*|         &    \ \ \   3 (high)\\
difference   &              \verb|-|          &   \ \ \   2 \\
union        &              \verb|+|          &   \ \ \   2 \\
Cartesian product &         \verb|CROSS|     &    \ \ \   1 (low)\\
\hline\hline
\end{tabular}
\caption{Set operators}\label{table:set-expr.set-op}
\end{aimmstable}

\paragraph{Example}
The following set assignments to integer sets and Cartesian products of integer
sets illustrate the use of all available set operators.
\begin{example}
    S := {1,2,3,4} * {3,4,5,6} ;     ! Intersection of integer sets: {3,4}.

    S := {1,2} + {3,4} ;             ! Union of simple sets:
    S := {1,3,4} + {2} + {1,2} ;     ! {1,2,3,4}

    S := {1,2,3,4} - {2,4,5,7} ;     ! Difference of integer sets: {1,3}.

    T := {1,2} cross {1,2} ;         ! The cross of two integer sets:
                                     ! {(1,1),(1,2),(2,1),(2,2)}.
\end{example}
The precedence and associativity of the operators is demonstrated by the
assignments
\begin{example}
    T := A cross B - C ;      ! Same as A cross (B - C).
    T := A - B * C + D ;      ! Same as (A - (B * C)) + D.
    T := A - B * C + D * E ;  ! Same as (A - (B * C)) + (D * E).
\end{example}
The operands of union, difference, and intersection must have the same
dimensions.
\begin{example}
    T := {(1,2),(1,3)} * {(1,3)} ;              ! Same as {(1,3)}.

    T := {(1,2),(1,3)} + {(i,j) | a(i,j) > 1} ; ! Union of enumerated
                                                ! and constructed set of
                                                ! the same dimension.

    T := {(1,2),(1,3)} + {(1,2,3)} ;            ! ERROR: dimensions differ.
\end{example}

\subsection{Set functions}\label{sec:set-expr.set.functions}

\paragraph{Set functions}
\sfuncindex{ConstraintVariables} \sfuncindex{VariableConstraints}
\index{set-valued function} \index{function!set-valued}

A special type of set expression is a call to one of the following set-valued
functions
\begin{itemize}
\item {\tt ElementRange},
\item {\tt SubRange},
\item {\tt ConstraintVariables},
\item {\tt VariableConstraints}, or
\item A user-defined function.
\end{itemize}
The {\tt ElementRange} and {\tt SubRange} functions are discussed in this
section, while the functions {\tt ConstraintVariables} and {\tt
VariableConstraints} are discussed in Section~\ref{sec:mp.mp}. The syntax of
and use of tags in function calls is discussed in
Section~\ref{sec:intern.func}.

\paragraph{The function {\tt ElementRange}}\sfuncindex{ElementRange}
\AIMMSlink{elementrange}

The {\tt ElementRange} function allows you to {\em dynamically} create or
change the contents of a set of non-integer elements based on the value of
integer-valued scalars expressions.

\paragraph{Arguments}

The {\tt ElementRange} function has two mandatory integer arguments.
\begin{itemize}
\item {\em first}, the integer value for which the first element
must be created, and
\item {\em last}, the integer value for which the last element
must be created.
\end{itemize}
In addition, it allows the following four optional arguments.
\begin{itemize}
\item {\em incr}, the integer-valued interval length between two
consecutive elements (default value 1),
\item {\em prefix}, the prefix string for every element (by default,
the empty string),
\item {\em postfix}, the postfix string (by default,
the empty string), and
\item {\em fill}, a logical indicator (0 or 1) whether the numbers must be
padded with zeroes (default value 1).
\end{itemize}
If you use any of the optional arguments you must use their formal argument
names as tags.

\paragraph{Example}
Consider the sets {\tt S} and {\tt T} initialized by the constant set
expressions
\begin{example}
    NodeSet     := DATA { node1   .. node100 } ;
    PaddedNodes := DATA { node001 .. node100 } ;
    EvenNodes   := DATA { node2   .. node100 by 2 } ;
\end{example}
These sets can also be created in a dynamic manner by the following
applications of the {\tt ElementRange} function.
\begin{example}
    NodeSet     := ElementRange( 1, 100, prefix: "node", fill: 0 );
    PaddedNodes := ElementRange( 1, 100, prefix: "node", fill: 1 );
    EvenNodes   := ElementRange( 2, 100, prefix: "node", fill: 0, incr: 2 );
\end{example}

\paragraph{The {\tt SubRange} function}\sfuncindex{SubRange}
\AIMMSlink{subrange}

The {\tt SubRange} function has three arguments:
\begin{itemize}
\item a simple {\em set},
\item the {\em first} element, and
\item the {\em last} element.
\end{itemize}
The result of the function is the subset ranging from the {\em first} to the
{\em last} element. If the first element is positioned after the last element,
the empty set will result.

\paragraph{Example}
Assume that the set {\tt Cities} is organized such that all foreign cities are
consecutive, and that {\tt FirstForeignCity} and {\tt LastForeignCity} are
element-valued parameters into the set {\tt Cities}. Then the following
assignment will create the subset {\tt ForeignCities} of {\tt Cities}
\begin{example}
    ForeignCities := SubRange( Cities, FirstForeignCity, LastForeignCity ) ;
\end{example}

\subsection{Iterative set operators}\label{sec:set-expr.set.iter}\label{sec:set-expr.set.sort}

\paragraph{Iterative operators}
\index{iterative operator}

Iterative operators form an important class of operators that are especially
designed for indexed expressions in AIMMS. There are set, element-valued,
arithmetic, statistical, and logical iterative operators. The syntax is always
similar.

\paragraph[0.95]{Syntax}
\syntaxdiagram{iterative-expression}{iter-expr}

\paragraph{Explanation}

\syntaxmark{iterative-operator} The first argument of all iterative operators
is a {\em binding domain}. It consists of a single index or tuple of indices,
optionally qualified by a logical condition. The second argument and further
arguments must be expressions. These expressions are evaluated for every index
or tuple in the binding domain, and the result is input for the particular
iterative operator at hand. Indices in the expressions that are not part of the
binding domain of the iterative operators are referred to as {\em outer
indices}, and must be bound elsewhere.

\paragraph{Set-related iterative operators}%
\index{set-valued iterative operator} \index{iterative operator!set-valued}
\index{sort!iterative} \index{intersection!iterative} \index{union!iterative}
\setiterindex{Sort} \setiterindex{Intersection} \setiterindex{Union}
\setiterindex{NBest} \AIMMSlink{Sort}

AIMMS possesses the following set-related iterative operators:
\begin{itemize}
\item the {\tt Sort} operator for sorting the elements in a domain,
\item the {\tt NBest} operator for obtaining the $n$ best elements in
a domain according to a certain criterion, and
\item the {\tt Intersection} and {\tt Union} operators for repeated
intersection or union of indexed sets.
\end{itemize}

\paragraph{Reordering your data}
\attrindex{OrderBy}

Sorting the elements of a set is a useful tool for controlling the flow of
execution and for presenting reordered data in the graphical user interface.
There are two mechanism available to you for sorting set elements
\begin{itemize}
\item the {\tt OrderBy} attribute of a set, and
\item the {\tt Sort} operator.
\end{itemize}

\paragraph{Sorting semantics}

The second and further operands of the {\tt Sort} operator must be numerical,
element-valued or string expressions. The result of the {\tt Sort} operator
will consist of precisely those elements that satisfy the domain condition,
sorted according to the single or multiple ordering criteria specified by the
second and further operands. Section~\ref{attr:set.order-by} discusses the
expressions that can be used for specifying an ordering principle.

\paragraph{Receiving set}

Note that the set to which the result of the {\tt Sort} operator is assigned
must have the {\tt OrderBy} attribute set to {\tt User} (see also
Section~\ref{sec:set.simple}) for the operation to be useful. Without this
setting AIMMS will store the elements of the result set of the {\tt Sort}
operator, but will discard the underlying ordering.

\paragraph{Example}

The following assignments will result in the same set orderings as in the
example of the {\tt OrderBy} attribute in Section~\ref{attr:set.order-by}.
\begin{example}
   LexicographicSupplyCities := Sort( i in SupplyCities, i ) ;

   ReverseLexicographicSupplyCities := Sort( i in SupplyCities, -i );

   SupplyCitiesByIncreasingTransport :=
       Sort( i in SupplyCities, Sum( j, Transport(i,j) );

   SupplyCitiesByDecreasingTransportThenLexicographic :=
       Sort( i in SupplyCities, - Sum( j, Transport(i,j) ), i );
\end{example}

\paragraph{Sorting root sets}
\index{sort!root set}

AIMMS will even allow you to sort the elements of a root set. Because the
entire execution system of AIMMS is built around a fixed ordering of the
root sets, sorting root sets may influence the overall execution in a negative
manner. Section~\ref{sec:eff.set.ordering} explains the efficiency
considerations regarding root set ordering in more detail.

\paragraph{Obtaining the $n$ best elements}
\setiterindex{NBest} \AIMMSlink{nbest}

You can use the {\tt NBest} operator, when you need the $n$ best elements in a
set according to a single ordering criterion. The syntax of the {\tt NBest} is
similar to that of the {\tt Sort} operator. The first expression after the
binding domain is the criterion with respect to which you want elements in the
binding domain to be ordered. The second expression refers to the number of
elements $n$ in which you are interested.

\paragraph{Example}

The following assignment will, for every city {\tt i}, select the three cities
to which the largest transports emanating from {\tt i} take place. The result
is stored in the indexed set {\tt LargestTransportCities(i)}.
\begin{example}
    LargestTransportCities(i) := NBest( j, Transport(i,j), 3 );
\end{example}

\paragraph{Repeated intersection\\ and union}
\setiterindex{Intersection}\setiterindex{Union}
\AIMMSlink{intersection}\AIMMSlink{union}

With the {\tt Intersection} and {\tt Union} operators you can perform repeated
set intersection or union respectively. A typical application is to take the repeated intersection or union of all instances of an indexed set. However, any set valued expression can be used on the second argument.

\paragraph{Example}

Consider the following indexed set declarations.
\begin{example}
Set IndSet1 {
    IndexDomain : s1;
    SubsetOf    : S;
}
Set IndSet2 {
    IndexDomain : s1;
    SubsetOf    : S;
}
\end{example}
With these declarations, the following assignments illustrate valid uses of the {\tt Union} and {\tt Intersection} operators.
\begin{example}
      SubS := Union( s1, IndSet1(s1) );
      SubS := Intersection( s1, IndSet1(s1) + IndSet2(s1) );
\end{example}

\subsection{Set element expressions as singleton sets}\label{sec:set-expr.set.elem}

\paragraph{Element expressions \dots}
\index{element!as singleton set}

Element expressions can be used in a set expression as well. In the context of
a set expression, AIMMS will interpret an element expression as the
singleton set containing only the element represented by the element
expression. Set element expressions are discussed in full detail in
Section~\ref{sec:set-expr.elem}.

\paragraph{\dots versus enumerated sets}

Using an element expression as a set expression can equivalently be expressed
as a symbolic enumerated set containing the element expression as its sole
element. Whenever there is no need to group multiple elements, AIMMS allows
you to omit the surrounding braces.

\paragraph{Example}

The following set assignment illustrate some simple set element expressions
used as a singleton set expression.
\begin{example}
    ! Remove LargestCity from the set of Cities
    Cities -= LargestCity ;

    ! Remove first element from the set of Cities
    Cities -= Element(Cities,1) ;

    ! Remove LargestCity and SmallestCity from Cities
    Cities -= LargestCity + SmallestCity ;

    ! The set of Cities minus the CapitalCity
    NonCapitalCities := Cities - CapitalCity ;
\end{example}


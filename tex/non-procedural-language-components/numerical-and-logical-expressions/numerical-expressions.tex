\section{Numerical expressions}\label{sec:expr.num}
\index{expression!numerical}

\paragraph{Constant numerical expressions}
\index{expression!constant}

Like any expression in AIMMS, a numerical expression can either be a {\em
constant} or a {\em symbolic} expression. Constant expressions are those that
contain references to explicit set elements and values, but do not contain
references to other identifiers. Constant expressions are mostly intended for
the initialization of sets, parameters and variables. Such an initialization
must conform to one of the following formats:
\begin{itemize}
\item a {\em scalar} value,
\item a {\em list} expression,
\item a {\em table} expression, or
\item a {\em composite table}.
\end{itemize}
Table expressions and composite tables are mostly used for data initialization
from {\em external} files. They are discussed in Chapter~\ref{chap:text.data.file}.

\paragraph{Symbolic numerical expressions}
\index{expression!symbolic}

Symbolic expressions are those expressions that contain references to other
AIMMS identifiers. They can be used in the {\tt Definition} attributes of
sets, parameters and variables, or as the right-hand side of assignment
statements. AIMMS provides a powerful notation for expressions, and
complicated numerical manipulations can be expressed in a clear and concise
manner.

\paragraph[0.7]{Syntax}
\syntaxdiagram{numerical-expression}{num-expr}
%%\vskip\medskipamount\noindent Notice that only a scalar value and
%%a list expression can act as a constant expression.

\subsection{Real values and arithmetic extensions}\label{sec:expr.num.arith-ext}

\paragraph{Extension of the real line}
\index{extended arithmetic} \index{arithmetic extensions} \typindex{special
number}{NA} \typindex{special number}{UNDF} \typindex{special number}{INF}
\typindex{special number}{-INF} \typindex{special number}{ZERO} \AIMMSlink{na}
\AIMMSlink{undf} \AIMMSlink{inf} \AIMMSlink{zero}

Traditional arithmetic is defined on the real line, $\R=(-\infty,\infty)$,
which does not contain either $+\infty$ or $-\infty$. AIMMS' arithmetic is
defined on the set $\R\cup\{{}$\verb|-INF|, \verb|INF|, \verb|NA|, \verb|UNDF|,
\verb|ZERO|${}\}$ and summarized in Table~\ref{table:expr.arith-ext}. The
symbols \verb|INF| and \verb|-INF| are mostly used to model unbounded
variables. The symbols \verb|NA| and \verb|UNDF| stand for {\em not available}
and {\em undefined} data values respectively. The symbol \verb|ZERO| denotes
the numerical value zero, but has the logical value true (not zero).
\begin{aimmstable}
\begin{tabular}{|c|p{6.2cm}|c|c|}
\hline\hline
{\bf Symbol}  & {\bf Description} & {\bf Logical} & {\bf {\tt MapVal}} \\
        &             & {\bf value} & {\bf value} \\
\hline
{\em number}  & any valid real number & & 0 \\
\verb|UNDF|   &     undefined (result of an arithmetic error) & 1 & 4\\
\verb|NA|     &     not available & 1 & 5\\
\verb|INF|    &     $+\infty$ & 1 & 6\\
\verb|-INF|   &     $-\infty$ & 1 & 7 \\
\verb|ZERO|   &    \pbsr numerically indistinguishable from zero,
                        but has the logical value of one. & 1 & 8\\
\hline\hline
\end{tabular}
\caption{Extended values of the AIMMS language}\label{table:expr.arith-ext}
\end{aimmstable}

\paragraph{Numerical behavior}

AIMMS treats these special symbols as ordinary real numbers, and the results
of the available arithmetic operations and functions on these symbols are
defined. The values \verb|INF|, \verb|-INF| and \verb|ZERO| are accessible by
the user and are dealt with as expected: $1+{\tt INF}$ evaluates to \verb|INF|,
$1/{\tt INF}$ to 0, $1+{\tt ZERO}$ to 1, etc. However, the values of \verb|INF|
and \verb|-INF| are undetermined and therefore, it makes no sense to consider
${\tt INF}/{\tt INF}$, ${\tt -INF}+{\tt INF}$, etc. These expressions are
therefore evaluated to \verb|UNDF|. A runtime error will occur if the value
{\tt UNDF} is assigned to an identifier.

\paragraph{The symbol {\tt ZERO}}
\typindex{special number}{ZERO}

The symbol \verb|ZERO| behaves like zero numerically, but its logical value is
one. Using this symbol, you can make a distinction between the default value of
0 and an assigned \verb|ZERO|. As an illustration, consider a distance matrix
with distances between selected factory-depot combinations. A missing distance
value evaluates to 0, and could mean that the particular factory-depot
combination should not be considered. A \verb|ZERO| value in that case could be
used to indicate that the combination should be considered even though the
corresponding distance is zero because the depot and factory happen to be one
facility.

\paragraph{Expressions with 0 and {\tt ZERO}}

Whenever the values 0 and {\tt ZERO} appear in the same expression with equal
priority, the value of {\tt ZERO} prevails. For example, the expressions
$0+{\tt ZERO}$ or {\tt max}(0,{\tt ZERO}) will both result in a numerical value
of {\tt ZERO}. In this way, the logically distinctive effect of {\tt ZERO} is
retained as long as possible. You should note, however, that AIMMS will
evaluate the multiplication of 0 with {\em any} special number to 0.

\paragraph{The symbol {\tt NA}}
\typindex{special number}{NA}

The symbol {\tt NA} can be used for missing data. The interpretation is ``this number is not yet known''. 
Any operation that uses {\tt NA} and does not use the symbol {\tt UNDF} will also produce the result {\tt NA}. 
AIMMS can reason with this value as it propagates the value {\tt NA} through its computations and assignments. 
The only exception is the condition in control flow statements where it must be known whether 
the result of that condition is equal to {\tt 0.0} or not, see also Section~\ref{sec:exec.flow}.

\paragraph{The symbol {\tt UNDF}}
\typindex{special number}{UNDF}

The symbol {\tt UNDF} cannot be input directly by a user, but is, besides an error message, 
the result of an undefined or illegal arithmetic operation. 
For example, \verb|1/ZERO|, \verb|0/0|, \verb|(-2)^0.1| all result in {\tt UNDF}. 
Any operation containing the {\tt UNDF} symbol evaluates to {\tt UNDF}. 

\subsection{List expressions}\label{sec:expr.num.list}
\index{expression!list} \index{list expression} \index{list!enumerated}
\index{enumerated list}

\paragraph{Element-value pairs}

A {\em list} is a collection of {\em element-value pairs}. In a list a single
element or range of elements is combined with a numerical, element-, or
string-valued expression, separated by a {\em colon}. List expressions are the
numerical extension of enumerated set expressions. The elements to which a
value is assigned inside a list, are specified in exactly the same manner as in
an enumerated set expression as explained in
Section~\ref{sec:set-expr.set.enum}.

\paragraph{Syntax}
\syntaxdiagram{enumerated-list}{list}

\paragraph{Constant versus symbolic}
\keyindex{DATA} \AIMMSlink{data}

By preceding the list expression with the keyword {\tt DATA}, it becomes a {\em
constant} list expression, in a similar fashion as with constant set
expressions (see Section~\ref{sec:set-expr.set.enum}). In a constant list
expression, set elements need not be quoted and the assigned values must be
constants. All other list expressions are symbolic, in which both the elements
and the assigned values are the result of expression evaluation.

\paragraph{Example}
The following assignments illustrate the use of list expressions.
\begin{itemize}
\item The following constant list expression assigns distances to
tuples of cities.
\begin{example}
    Distance(i,j) := DATA {
        (Amsterdam, Rotterdam  ) : 85 [km] ,
        (Amsterdam, 'The Hague') : 65 [km] ,
        (Rotterdam, 'The Hague') : 25 [km]
    } ;
\end{example}
\item
The following symbolic list expression assigns a certain status to every node
in a number of dynamically computed ranges.
\begin{example}
    NodeUsage(i) := {
        FirstNode            .. FirstNode + Batch - 1   : 'InUse'   ,
        FirstNode + Batch    .. FirstNode + 2*Batch - 1 : 'StandBy' ,
        FirstNode + 2*Batch  .. LastNode                : 'Reserve'
    } ;
\end{example}
\end{itemize}

\subsection{References}\label{sec:expr.num.ref}

\paragraph{References}
\index{reference} \index{expression!reference}

Sets, parameters and variables can be referred to by name resulting in a set-,
set element-, string-valued, or numerical quantity. A reference can be scalar
or multidimensional, and index positions may contain either indices or element
expressions. By specifying a case reference in front, a reference can refer to
data from cases that are not in memory.

\paragraph[0.9]{Syntax}
\syntaxdiagram{reference}{reference}
\syntaxdiagram{identifier-part}{ident-part}

\paragraph{Scalar versus indexed}

A {\em scalar} set, parameter or variable has no indexing (dimension) and is
referenced simply by using its identifier. Indexed sets, parameters and
variables have dimensions equal to the number of indices.

\paragraph{Example}
The right-hand sides of the following assignments are examples of references to
scalar and indexed identifiers.
\begin{example}
    MainCity                := 'Amsterdam' ;

    DistanceFromMainCity(i) := Distance( MainCity, i );

    SecondNextCity(i)       := NextCity( NextCity(i) );

    NextPeriodStock(t)      := Stock( t + 1 );
\end{example}

\paragraph{Undefined references}
\index{reference!undefined}

The last two references, which make use of lag and lead operators and element
parameters, may sometimes be undefined. When used in an expression such
undefined references evaluate to the empty set, zero, the empty element, or the
empty string, depending on the value type of the identifier. When an undefined
lag or lead operator or element parameter occurs on the left-hand side of an
assignment, the assignment is skipped. For more details, refer to
Section~\ref{sec:exec.assign}.

\paragraph{Referring to module identifiers}
\index{reference!to module identifier} \index{module!reference}
\index{operator!namespace resolution} \index{namespace resolution operator}

\syntaxmark{module-prefix} When your model contains one or more {\tt Modules},
your model will be supplied multiple additional namespaces besides the global
namespace, one for each module. Identifiers declared within a module are, by
default, not contained in the global namespace. To refer to such identifiers
outside the module, you have to prefix the identifier name with a
module-specific prefix and the {\tt ::} namespace resolution operator. {\tt
Modules} and the namespace resolution operator are discussed in full detail in
Section~\ref{sec:module.module}.

\tipparagraph{Referring to other cases}{case.reference} \index{reference!case}
\index{case reference}

\syntaxmark{case-reference} When a reference is preceded by a {\em case
reference}, AIMMS will not retrieve the requested identifier data from the
case in memory, but from the case file associated with the case reference. Case
references are elements of the (predefined) set {\tt AllCases}, which contains
all the cases available in the data manager of AIMMS. The AIMMS User's
Guide describes all the mechanisms that are available and functions that you
can use to let an end-user of your application select one or more cases from
the set of all available cases. Case referencing is useful when you want to
perform advanced case comparison over multiple cases.

\paragraph{Example}

The following computes the differences of the values of the variable {\tt
Transport} in the current case compared to its values in all cases in the set
{\tt CurrentCase\-Selection}.
\begin{example}
    for ( c in CurrentCaseSelection ) do
        Difference(c,i,j) := c.Transport(i,j) - Transport(i,j) ;
    endfor;
\end{example}
During execution, AIMMS will (temporarily) retrieve the values of {\tt
Transport} from all requested cases to compute the difference with the data of
the current case.

\subsection{Arithmetic functions}\label{sec:expr.num.functions}

\paragraph{Standard functions}
\index{arithmetic function} \index{function!arithmetic}

AIMMS provides the commonly used standard arithmetic functions such as the
trigonometric functions, logarithms, and exponentiations.
Table~\ref{table:expr.num-func} lists the available arithmetic functions with
their arguments and result, where $x$ is an extended range arithmetic
expressions, $m$, $n$ are integer expressions, $i$ is an index, $l$ is a set
element, $I$ is a set identifier, and $e$ is a scalar reference.

\newcommand{\bdiv}{\operatorname{div}}

\begin{aimmstable}
\def\FI{\funcindex}\def\AL{\AIMMSlink}
\begin{tabular}{|l|p{8.2cm}|}
\hline\hline
{\bf Function} & {\bf Meaning}\\
\hline
$\mathtt{Abs}(x)$\FI{Abs}\AL{abs}                       & absolute value $|x|$\\
$\mathtt{Exp}(x)$\FI{Exp}\AL{exp}                       & $e^x$\\
$\mathtt{Log}(x)$\FI{Log}\AL{log}                       & $\log_e(x)$ for $x>0$,\verb|UNDF| otherwise\\
$\mathtt{Log10}(x)$\FI{Log10}\AL{log10}                 & $\log_{10}(x)$ for $x>0$, \verb|UNDF| otherwise\\
$\mathtt{Max}(x_1,\dots,x_n)$\FI{Max}                   & $\max(x_1,\dots,x_n)\quad (n>1)$\\
$\mathtt{Min}(x_1,\dots,x_n)$\FI{Min}                   & $\min(x_1,\dots,x_n)\quad (n>1)$\\
$\mathtt{Mod}(x_1,x_2)$\FI{Mod}\AL{mod}                 & $x_1 \bmod {x_2} \in [0,x_2)$ for $x_2 > 0$ or $\in(x_2,0]$ for $x_2<0$ \\
$\mathtt{Div}(x_1,x_2)$\FI{Div}\AL{Div}                 & $x_1 \bdiv {x_2}$ \\
$\mathtt{Sign}(x)$\FI{Sign}\AL{sign}                    & $\sign(x)=+1$ if $x>0$, $-1$ if $x<0$ and $0$ if $x=0$\\
$\mathtt{Sqr}(x)$\FI{Sqr}\AL{sqr}                       & $x^2$ \\
$\mathtt{Sqrt}(x)$\FI{Sqrt}\AL{sqrt}                    & $\sqrt x$ for $x\geq0$, \verb|UNDF| otherwise\\
$\mathtt{Power}(x_1,x_2)$\FI{Power}\AL{power}           & $x_1^{x_2}$, alternative for \verb|x^y| (see Section~\ref{sec:expr.operators})\\
$\mathtt{ErrorF}(x)$\FI{ErrorF}\AL{errorf}              & ${\frac{1}{\sqrt{2\pi}}}\int_{-\infty}^x e^{-{\frac{t^2}{2}}}\, dt$\\
\hline
$\mathtt{Cos}(x)$\FI{Cos}\AL{cos}                       & $\cos(x)$; $x$ in radians\\
$\mathtt{Sin}(x)$\FI{Sin}\AL{sin}                       & $\sin(x)$; $x$ in radians\\
$\mathtt{Tan}(x)$\FI{Tan}\AL{tan}                       & $\tan(x)$; $x$ in radians\\
$\mathtt{ArcCos}(x)$\FI{ArcCos}\AL{arccos}              & $\hbox{\rm arccos}(x)$; result in radians\\
$\mathtt{ArcSin}(x)$\FI{ArcSin}\AL{arcsin}              & $\hbox{\rm arcsin}(x)$; result in radians\\
$\mathtt{ArcTan}(x)$\FI{ArcTan}\AL{arctan}              & $\hbox{\rm arctan}(x)$; result in radians\\
$\mathtt{Degrees}(x)$\FI{Degrees}\AL{degrees}           & converts $x$ from radians to degrees\\
$\mathtt{Radians}(x)$\FI{Radians}\AL{radians}           & converts $x$ from degrees to radians\\
\hline
$\mathtt{Cosh}(x)$\FI{Cosh}\AL{cosh}                    & $\cosh(x)$\\
$\mathtt{Sinh}(x)$\FI{Sinh}\AL{sinh}                    & $\sinh(x)$\\
$\mathtt{Tanh}(x)$\FI{Tanh}\AL{tanh}                    & $\tanh(x)$\\
$\mathtt{ArcCosh}(x)$\FI{ArcCosh}\AL{arccosh}           & $\hbox{\rm arccosh}(x)$\\
$\mathtt{ArcSinh}(x)$\FI{ArcSinh}\AL{arcsinh}           & $\hbox{\rm arcsinh}(x)$\\
$\mathtt{ArcTanh}(x)$\FI{ArcTanh}\AL{arctanh}           & $\hbox{\rm arctanh}(x)$\\
\hline
$\mathtt{Card}(I[,\mbox{\em suffix}])$\FI{Card}         & cardinality of (suffix of) set, parameter or variable $I$\\
$\mathtt{Ord}(i)$\FI{Ord}                               & ordinal number of index $i$ in set $I$ (see also Table~\ref{table:set-expr.set-func})\\
$\mathtt{Ord}(l[,I])$                                   & ordinal number of element $l$ in set $I$\\
\hline
$\mathtt{Ceil}(x)$\FI{Ceil}\AL{ceil}                    & $\lceil x \rceil = \text{smallest integer} \geq x$\\
$\mathtt{Floor}(x)$\FI{Floor}\AL{floor}                 & $\lfloor x \rfloor = \text{largest integer} \leq x$\\
$\mathtt{Precision}(x,n)$\FI{Precision}\AL{precision}   & $x$ rounded to $n$ significant digits\\
$\mathtt{Round}(x)$\FI{Round}\AL{round}                 & $x$ rounded to nearest integer\\
$\mathtt{Round}(x,n)$                                   & $x$ rounded to $n$ decimal places left ($n<0$) or right ($n>0$) of the decimal point\\
$\mathtt{Trunc}(x)$\FI{Trunc}\AL{trunc}                 & truncated value of $x$: $\mathtt{Sign}(x)*\mathtt{Floor}(\mathtt{Abs}(x))$\\
\hline
$\mathtt{NonDefault}(e)$\FI{NonDefault}\AL{nondefault}  & $1$ if $e$ is not at its default value, $0$ otherwise\\
$\mathtt{MapVal}(x)$\FI{MapVal}\AL{mapval}              & {\tt MapVal} value of $x$ according to Table~\ref{table:expr.arith-ext}\\
\hline\hline
\end{tabular}
\caption{Intrinsic numerical functions of AIMMS} \label{table:expr.num-func}
\end{aimmstable}

\paragraph{Functions and extended arithmetic}
\index{extended arithmetic!in functions} \index{function!extended arithmetic}

Special caution is required when one or more of the arguments in the functions
are special symbols of AIMMS' extended range arithmetic. If the value of any
of the arguments is \verb|UNDF| or \verb|NA|, then the result will also be
\verb|UNDF| or \verb|NA|.  If the value of any of the arguments is \verb|ZERO|
and the numerical value of the result is zero, the function will return
\verb|ZERO|.

\subsection{Numerical operators}\label{sec:expr.operators}
\index{operator!numerical}

Using unary or binary numerical operators you can construct numerical
expressions that consist of multiple terms and/or factors. The syntax follows.

\paragraph{Syntax}
\syntaxdiagram{operator-expression}{num-op-expr}

\paragraph{Standard numerical operators}%
\operindex{+}%
\operindex{-}%
\operindex{*}%
\operindex{/}%
\operindex{/\$}%
\operindex{\char`\^} \index{addition} \index{subtraction}
\index{multiplication} \index{division}

\syntaxmark{binary-operator} The \syntaxmark{unary-operator} order of
precedence of the standard numerical operators in AIMMS is given in
Table~\ref{table:expr.num-oper}. Parentheses may be used to override the
precedence order. Expression evaluation is from left to right.
\begin{aimmstable}
\begin{tabular}{|c|l|c|}
\hline\hline
{\bf Operator} & {\bf Meaning} & {\bf Precedence} \\ % & {\bf Efficiency}\\
\hline
  \it Unary\hfill & ... & ... \\% &\\
        \verb|+|&       positive          &      n/a \\ % & sparse \\
        \verb|-|&       negative          &      n/a \\ % & sparse \\
  \it Binary\hfill & ... & ... \\% &\\
        \verb|^| &  exponentiation    &      3 (high) \\ % & dense \\
        \verb|*| &       multiplication   &       2   \\ % & intersection \\
        \verb|/| &       division          &      2   \\ % & dense \\
%        \verb|/$| &       division          &     2   \\ % & intersection \\
        \verb|+| &       addition          &      1   \\ % & union \\
        \verb|-| &       subtraction       &      1 (low) \\ %& union \\
\hline\hline
\end{tabular}
\caption{Numerical operators}\label{table:expr.num-oper}
\end{aimmstable}

\paragraph{Example}
The expression
\begin{example}
    p1 + p2 * p3 / p4^p5
\end{example}
is parsed by AIMMS as if it had been written
\begin{example}
    p1 + [(p2 * p3) / (p4^p5)]
\end{example}
In general, it is better to use parentheses than to rely on the precedence and
associativity of the operators. Not only because it prevents you from making
unwanted mistakes, but also because it makes your intentions clearer.

\paragraph{Exponential operator}

Special restrictions apply to the exponential operator ``\verb|^|''. AIMMS
accepts the following combinations of left-hand side operand (called the {\em
base}), and right-hand side operand (called the {\em exponent}):
\begin{itemize}
\item a positive base with a real exponent,
\item a negative base with an integer exponent, 
\item a zero base with a positive exponent, and
\item a zero base with a zero exponent results in one (as controlled by the option {\tt power\_0\_0}).
\end{itemize}


\subsection{Numerical iterative operators}\label{sec:expr.num.iter}
\index{iterative operator!numerical} \index{numerical iterative operator}

\paragraph{Arithmetic iterative operators}%
\iterindex{Sum}%
\iterindex{Prod}%
\iterindex{Count}%
\iterindex{Min}%
\iterindex{Max}%
\AIMMSlink{Sum}%
\AIMMSlink{Prod}%
\AIMMSlink{Count}%
\index{addition!iterative} \index{multiplication!iterative}

Iterative operators are used to express repeated arithmetic operations, such as
summation, in a concise manner. The arithmetic iterative operators supported by
AIMMS are listed in Table~\ref{table:expr.num-iter}. The second column in
this table refers to the required number of expression arguments following the
binding domain argument, while the last column refers to the result of the
operator in case of an empty domain.

\begin{aimmstable}
\begin{tabular}{|l|c|p{6.75cm}|c|}
\hline\hline
{\bf Name} & {\bf \# Expr.}   & {\bf Computes over all elements in the domain} & {\bf Default} \\
\hline
{\tt Sum}      & 1 & \pbsr the sum of the expression & 0\\
{\tt Prod}     & 1 & \pbsr the product of the expression & 1\\
{\tt Count}    & 0 & \pbsr the total number of elements in
the domain & 0 \\
{\tt Min}     & 1 & \pbsr the minimum value of the expression & {\tt INF}\\
{\tt Max}     & 1 & \pbsr the maximum value of the expression & {\tt -INF}\\
\hline\hline
\end{tabular}
\caption{Arithmetic iterative operators}\label{table:expr.num-iter}
\end{aimmstable}

\paragraph{Compared expressions}

The {\tt Min} and {\tt Max} operators return the minimum or maximum value of an
expression. The allowed expressions are:
\begin{itemize}
\item numerical expressions, in which case AIMMS returns the lowest
or highest numerical values,
\item string expressions, in which case AIMMS returns the strings
which are first or last with respect to the normal alphabetic ordering, and
\item element expressions, in which case AIMMS returns the elements
with the lowest or highest ordinal numbers (see also Section~\ref{sec:set-expr.elem.functions}).
\end{itemize}

\paragraph{Example}
The following assignments are valid examples of the use of the arithmetic
iterative operators.
\begin{example}
    NumberOfRoutes      := Count( (i,j) | Distance(i,j) ) ;
    NettoTransport(i)   := Sum( j, Transport(i,j) - Transport(j,i) ) ;
    MaximumTransport(i) := Max( j, Transport(i,j) ) ;
\end{example}


\subsection{Statistical functions and operators}\label{sec:expr.stat}

\paragraph{Distributions}
\index{distribution function} \index{function!distribution}

AIMMS provides the most commonly used distributions. They are listed in
Table~\ref{table:expr.distrib}, together with the required type of arguments
and a description of the result. You can find a more detailed description of
these distributions in Appendices~\ref{app:distribution.discrete}
and~\ref{app:distribution.cont}. When called as functions inside your model,
they behave as random number generators.

\begin{aimmstable}
\def\DFI{\dfuncindex}
\begin{tabular}{|l|p{7cm}|}
\hline\hline
{\bf Distribution} &{\bf              Meaning}\\
\hline $\mathtt{Binomial}(p,n)$\DFI{Binomial}  & \pbsr Binomial distribution
with probability $p$ and number of trials $n$
\\
$\mathtt{NegativeBinomial}(p,r)$\DFI{NegativeBinomial} & \pbsr Negative
Binomial distribution with probability $p$ and number of successes $r$
\\
$\mathtt{Poisson}(\lambda)$\DFI{Poisson} & \pbsr Poisson distribution with rate
$\lambda$
\\
$\mathtt{Geometric}(p)$\DFI{Geometric} & \pbsr Geometric distribution with
probability $p$
\\
$\mathtt{HyperGeometric}(p,n,N)$\DFI{HyperGeometric} & \pbsr Hypergeometric
distribution with initial probability of success $p$, number of trials $n$ and
population size $N$
\\
\hline $\mathtt{Uniform}(\Varr{min,max})$\DFI{Uniform} & \pbsr Uniform
distribution with lower bound \Varr{min} and upper bound \Varr{max}
\\
$\mathtt{Triangular}(\beta,\Varr{min},\Varr{max})$\DFI{Triangular} & \pbsr
Triangular distribution with shape $\beta$, lower bound \Varr{min}, and upper
bound \Varr{max}, where
$\beta=(x_{\text{peak}}-\Varr{min})/(\Varr{max}-\Varr{min})$
\\
$\mathtt{Beta}(\alpha,\beta,\Varr{min},\Varr{max})$\DFI{Beta} & \pbsr Beta
distribution with shapes $\alpha$, $\beta$, lower bound \Varr{min}, and upper
bound \Varr{max}
\\
\hline $\mathtt{LogNormal}(\beta,\Varr{min},s)$\DFI{LogNormal} & \pbsr
Lognormal distribution with shape $\beta$, lower bound \Varr{min}, and scale
$s$
\\
$\mathtt{Exponential}(\Varr{min},s)$\DFI{Exponential} & \pbsr Exponential
distribution with lower bound \Varr{min} and scale $s$
\\
$\mathtt{Gamma}(\beta,\Varr{min},s)$\DFI{Gamma} & \pbsr Gamma distribution with
shape~$\beta$, lower bound \Varr{min}, and scale $s$
\\
$\mathtt{Weibull}(\beta,\Varr{min},s)$\DFI{Weibull} & \pbsr Weibull
distribution with shape $\beta$, lower bound \Varr{min}, and scale $s$
\\
$\mathtt{Pareto}(\beta,l,s)$\DFI{Pareto} & \pbsr Pareto distribution  with
shape~$\beta$, location $l$, and scale $s$ ($\text{lower bound} = l + s$)
\\
\hline $\mathtt{Normal}(\mu,\sigma)$\DFI{Normal} & \pbsr Normal distribution
with mean $\mu$ and standard deviation $\sigma$
\\
$\mathtt{Logistic}(\mu,s)$\DFI{Logistic} & \pbsr Logistic distribution with
mean $\mu$ and scale $s$
\\
$\mathtt{ExtremeValue}(l,s)$\DFI{ExtremeValue} & \pbsr Extreme Value
distribution with location $l$ and scale $s$
\\
\hline\hline
\end{tabular}
\caption{Distributions available in AIMMS} \label{table:expr.distrib}
\end{aimmstable}

\paragraph{Setting the seed}
\index{distribution!set seed}

You can set the seed of the random number generators for all distributions
using the execution option {\tt seed}. By setting the seed explicitly you can
guarantee that your model results are reproducible.

\paragraph{Cumulative distributions and their derivatives}%
\dfuncindex{CumulativeDistribution}%
\dfuncindex{InverseCumulativeDistribution}%
\AIMMSlink{cumulativedistribution} \AIMMSlink{inversecumulativedistribution}

Each distribution in Table~\ref{table:expr.distrib} can be used as an argument
for four operators: {\tt DistributionCumulative} and {\tt
DistributionInverseCumulative}, and their derivatives {\tt DistributionDensity}
and {\tt DistributionInverseDensity}. In the explanation below it is assumed
that $\alpha \in [0,1]$, $x \in (-\infty,\infty)$, and $X$ a random variable
distributed according to the given distribution {\sf\em distr}.

\begin{itemize}

\item {\tt DistributionCumulative}({\em distr},$x$) computes
the probability $P(X\leq x)$.

\item {\tt DistributionInverseCumulative}({\em
distr},$\alpha$) computes the smallest $x$ such that the probability $P(X\leq x) \geq \alpha$, except for $\alpha = 0$ which
returns the lowest possible value for $X$.

\item {\tt DistributionDensity}({\em distr},$x$) computes for
continuous distributions the probability density $\lim_{\alpha \downarrow 0}P(x
\leq X\leq x+\alpha)/\alpha$. For discrete distributions, the operator is only
defined for integer values of $x$ and returns $P(X = x)$.

\item {\tt DistributionInverseDensity}({\em distr},$\alpha$) is the
derivative of {\tt Distribution\-In\-verse\-Cumulative}. For more details you
are referred to Appendix~\ref{app:distribution.stat}.

\end{itemize}

\paragraph{Use in constraints}
\index{distribution function!use in constraint} \index{constraint!use of
distributions}

For the continuous distributions in Table~\ref{table:expr.distrib}, AIMMS can
compute the derivatives of the cumulative and inverse cumulative distribution
functions. As a consequence, you may use these functions in the constraints of
a nonlinear model when the second argument is a variable.

\paragraph{Example}
The following statements demonstrate how the distributions can be used to
perform statistical tasks.

%%\newpage

\begin{enumerate}
\item Draw a random number from a distribution.
\begin{example}
    Draw := Normal(0,1);
    Draw := Uniform(LowestValue, HighestValue);
\end{example}
\item Compute the probability of at most 10 successes out of 50
trials, with a 0.25 probability of success.
\begin{example}
    Probability := DistributionCumulative( Binomial(0.25,50), 10 );
\end{example}
\item Compute a two-sided 90\% confidence interval of a Normal(0,1)
distribution.
\begin{example}
    LeftBound  := DistributionInverseCumulative( Normal(0,1), 0.05);
    RightBound := DistributionInverseCumulative( Normal(0,1), 0.95);
\end{example}
\end{enumerate}

\paragraph{Statistical operators}
\index{statistical iterative operator} \index{iterative operator!statistical}

The distributions, listed in Table~\ref{table:expr.distrib}, make it possible
for you to execute a stochastic experiment based on your model representation.
In order to analyze the subsequent results, AIMMS provides a number of
statistical iterative operators which are listed in
Table~\ref{table:expr.stat-iter}. The second column in this table refers to the
required number of expression arguments following the binding domain argument.
For the most common sample operators, AIMMS provides distribution operators
to calculate the corresponding expected values, assuming the sample is drawn
from a given distribution. These distribution operators are listed in
Table~\ref{table:expr.distrib-oper}. A more detailed description of these
operators is provided in Appendix~\ref{app:distribution}.

\begin{aimmstable}
\tabcolsep0.5\tabcolsep
\begin{tabular}{|l|c|p{7cm}|}
\hline\hline {\bf Name} & {\bf \# Expr.}   & {\bf Computes over all elements in
the
domain} \\
\hline
{\tt Mean} \statiterindex{Mean} & 1 & \pbsr the (arithmetic) mean\\
{\tt GeometricMean}\statiterindex{GeometricMean} & 1 & \pbsr the geometric mean\\
{\tt HarmonicMean}\statiterindex{HarmonicMean} & 1 & \pbsr the harmonic mean\\
{\tt RootMeanSquare}\statiterindex{RootMeanSquare} & 1 & \pbsr the root mean square\\
{\tt Median}\statiterindex{Median} & 1 & \pbsr the median\\
{\tt SampleDeviation}\statiterindex{SampleDeviation} & 1 & \pbsr the standard deviation of a sample\\
{\tt PopulationDeviation}\statiterindex{PopulationDeviation} & 1 & \pbsr the standard deviation of a population\\
{\tt Skewness}\statiterindex{Skewness} & 1 & \pbsr the coefficient of skewness\\
{\tt Kurtosis}\statiterindex{Kurtosis} & 1 & \pbsr the coefficient of kurtosis\\
{\tt Correlation}\statiterindex{Correlation} & 2 & \pbsr the correlation coefficient\\
{\tt RankCorrelation}\statiterindex{RankCorrelation} & 2 & \pbsr the rank correlation coefficient\\
\hline\hline
\end{tabular}
\caption{Statistical sample operators}\label{table:expr.stat-iter}
\end{aimmstable}

%\begin{table}[H]
%\begin{center}
\begin{aimmstable}
\tabcolsep0.5\tabcolsep
\begin{tabular}{|l|l|p{7cm}|}
\hline\hline
{\bf Name} & {\bf Computes for a given distribution} \\
\hline
{\tt DistributionMean}\statiterindex{DistributionMean} & \pbsr the (arithmetic) mean\\
{\tt DistributionDeviation}\statiterindex{DistributionDeviation} & \pbsr the (standard) deviation\\
{\tt DistributionVariance}\statiterindex{DistributionVariance} & \pbsr the variance (the square of the deviation)\\
{\tt DistributionSkewness}\statiterindex{DistributionSkewness} & \pbsr the coefficient of skewness \\
{\tt DistributionKurtosis}\statiterindex{DistributionKurtosis} & \pbsr the coefficient of kurtosis \\
\hline\hline
\end{tabular}
\caption{Statistical distribution operators}\label{table:expr.distrib-oper}
\end{aimmstable}
%\end{center}
%\end{table}

\paragraph{Example}
Assume that {\tt p} is an index into a set that has been used to index a number
of experiments resulting in observables {\tt x(p)} and {\tt y(p)}. Then the
following assignments demonstrate the use of the statistical operators in
AIMMS.
\begin{example}
    MeanX         := Mean(p, x(p));
    MeanX         := Mean(p | x(p), x(p));
    DeviationX    := SampleDeviation(p, x(p));
    CorrelationXY := Correlation(p, x(p), y(p));
\end{example}

In case the $x$ values are drawn from a {\tt Binomial(0.6,8)} distribution the
expected value of {\tt MeanX} is given by

\begin{example}
    ExpectedMeanX := DistributionMean(Binomial(0.6,8));
\end{example}

\paragraph{Units of measurement}
\index{distribution!unit of measurement} \index{unit!of distribution}

For all distributions, the units of measurement (see also
Chapter~\ref{chap:units}) of parameters and result should be consistent. The
unit relationships for each distribution are described in
Appendix~\ref{app:distribution} in full detail. In the presence of units of
measurement within your model, AIMMS will perform a unit consistency check.

\paragraph{Histogram support}
\index{histogram}

For easy visualization of statistical data,
AIMMS offers support for creating histograms based on a large collection of
observed values. Through a number of predefined procedures and functions,
AIMMS allows you to flexibly create interval-based histogram data, which can
easily be displayed, for instance, using the standard (graphical) AIMMS bar
chart object. For further information about creating and displaying histograms,
as well as an illustrative example, you are referred to section~\ref{sec:gui.histogram} in the Appendix.

\paragraph{Combinatoric functions}
\index{combinatoric function} \index{function!combinatoric}

In addition to the distribution and statistical operators listed above,
AIMMS also offers support for the most common combinatoric calculations.
Table~\ref{table:expr.combinatoric} contains the list of combinatoric functions
that are available in AIMMS.

\begin{aimmstable}
\begin{tabular}{|l|c|}
\hline\hline
{\bf Function} & {\bf Meaning}\\
\hline
$\mbox{\tt Factorial}(n)$\funcindex{Factorial}\AIMMSlink{factorial}           & $n!$\\
$\mbox{\tt Combination}(n,m)$\funcindex{Combination}\AIMMSlink{combination}       & $\binom{n}{m}$\\
$\mbox{\tt Permutation}(n,m)$\funcindex{Permutation}\AIMMSlink{permutation}       & $m!\cdot{\binom{n}{m}}$\\
\hline\hline
\end{tabular}
\caption{Combinatoric functions}\label{table:expr.combinatoric}
\end{aimmstable}

\subsection{Financial functions}\label{sec:expr.financial}
\index{financial function} \index{function!financial}

\tipparagraph{Financial functions}{finance.func}

AIMMS provides an extensive library of financial functions for a variety of
financial applications. The available functions can be classified as follows.
\begin{itemize}
\item Functions for the computation of the depreciation of assets using
various methods such as fixed-declining balance method, double-declining
balance method, etc.
\item Functions for computing various quantities regarding investments
that consist of a series of constant or variable periodic cash flows. The
computed quantities include present value, net present value, future value,
internal rate of return, interest and principal payments, etc.
\item Functions for computing various security-related quantities of,
for instance, discounted securities, securities that pay periodic interest and
securities that pay interest at maturity. The computed quantities include
yield, interest rate, redemption, price, accrued interest, etc.
\end{itemize}

\paragraph{Consult the online function reference}

The precise description of all financial functions available in AIMMS is not
included in this Language Reference. You can find a complete list of the
available financial functions on pages~\pageref{FRchap:finance-intro} and
further of the AIMMS Function Reference. The Function Reference provides a
description as well as the prototype of every financial function present in
AIMMS.

\subsection{Conditional expressions}\label{sec:expr.cond}
\index{conditional expression} \index{expression!conditional}

\paragraph{Two conditional expressions}
There are two ways to specify expressions that adopt different values depending
on one or more logical conditions. The \verb|ONLYIF| operator is the simpler
and operates as it sounds.  The \verb|IF-THEN-ELSE| expression is more powerful
in its ability to distinguish several cases.

\paragraph{Syntax}
\syntaxdiagram{conditional-expression}{cond-expr}

\paragraph{The {\tt ONLYIF} operator}
\index{expression!ONLYIF@{\tt ONLYIF}} \operindex{ONLYIF} \operindex{\$}

The simplest way of specifying a conditional expression is to use the
\verb|ONLYIF| operator. Its syntax is given by \vskip\baselineskip
\syntaxdiagram{onlyif-expression}{onlyif-expr} \syntaxcomment The {\tt ONLYIF}
expression evaluates to the arithmetic expression in the first argument if the
logical condition of the second argument is true. Otherwise, it is zero. The
``\verb=$='' symbol can be used as a synonym for the \verb|ONLYIF| operator.

\paragraph{Example}
A simple example of the use of the \verb|ONLYIF| operator is given by the
assignment
\begin{example}
    AverageVelocity := (Distance / TravelTime) ONLYIF TravelTime ;
\end{example}
or equivalently, using the {\tt \$} operator,
\begin{example}
    AverageVelocity := (Distance / TravelTime) $ TravelTime ;
\end{example}
Both expressions evaluate to {\tt Distance / TravelTime} if {\tt TravelTime}
assumes a nonzero value, or to zero otherwise. In
Section~\ref{sec:sparse.modifier.binary} you will see that this particular
expression can be written even more concisely using the sparsity
modifier~``{\tt \$}''.

\paragraph{{\tt IF-THEN-ELSE} expressions}
\index{IF-THEN-ELSE expression@{\tt IF-THEN-ELSE} expression}
\index{expression!IF-THEN-ELSE@{\tt IF-THEN-ELSE}}

A much more flexible way for specifying conditional expressions is given by the
\verb|IF-THEN-ELSE| operator. The syntax of the \verb|IF-THEN-ELSE| expression
is given below.

\paragraph{Syntax}
\syntaxdiagram{if-then-else-expression}{if-then-else-expr}

\paragraph{Explanation}

The \verb|IF-THEN-ELSE| expression works like a {\em switch statement}---a
series of \verb|ELSEIF|s can be used to denote numerous special cases. The
value of the \verb|IF-THEN-ELSE| expression is the first numerical expression
for which the corresponding logical condition is true. If none of the
conditions are true, then the value will be the numerical expression after the
\verb|ELSE| keyword if present or zero otherwise.

\paragraph{Example}
A simple illustration of the use of the \verb|IF-THEN-ELSE| construction is
given by the assignments
\begin{example}
    AverageVelocity := IF TravelTime THEN Distance / TravelTime ENDIF ;
\end{example}
which is equivalent to the {\tt ONLYIF} expression above. A more elaborate
example is given by the assignment
\begin{example}
    WeightedDistance(i) :=
        IF     Distance(i) <= 100 THEN Distance(i)
        ELSEIF Distance(i) <= 200 THEN (100 + Distance(i)) / 2
        ELSEIF Distance(i) <= 300 THEN (250 + Distance(i)) / 3
        ELSE   550 / 3
        ENDIF ;
\end{example}
The expression takes the value associated with the first logical expression
that is true.


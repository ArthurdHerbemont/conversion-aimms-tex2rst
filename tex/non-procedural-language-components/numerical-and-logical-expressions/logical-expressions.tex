\section{Logical expressions}\label{sec:expr.logic}
\index{expression!logical} \index{logical expression}

\paragraph{Logical expressions}

Logical expressions are expressions that evaluate to a logical value---0.0 for
false and 1.0 for true.  AIMMS supports several types of logical
expressions.

\paragraph{}
\syntaxdiagram{logical-expression}{logic-expr}

\paragraph{Numerical expressions\\ as logical}
\index{logical expression!numerical values as} \index{logical
expression!extended arithmetic} \index{special number!logical value}
\index{extended arithmetic!logical value}

As AIMMS permits numerical expressions as logical expressions it is
important to discuss how numerical expressions are interpreted logically, and
how logical expressions are interpreted numerically. Numerical expressions that
evaluate to zero (0.0) are false, while all others (including \verb|ZERO|,
\verb|NA| and \verb|UNDF|) are true. A false logical expression evaluates to
zero (0.0), while a true logical expression evaluates to one (1.0). If one or
more of the operands of a logical operator is \verb|UNDF| or \verb|NA|, the
numerical value is also \verb|UNDF| or \verb|NA|.
Note that AIMMS will not accept expressions that evaluate to \verb|UNDF| or \verb|NA| in 
the condition in control flow statements, where it must be known whether 
the result of that condition is equal to {\tt 0.0} or not (see also Section~\ref{sec:exec.flow}).

\paragraph{Example}

Table \ref{table:expr.logic-values} illustrates the different interpretation of
a number of numerical and logical expressions as either a numerical or a
logical expression. See also Table~\ref{table:expr.logic-oper} for the results
associated with the {\tt AND} operator.
\begin{aimmstable}
\begin{tabular}{|l|c|c|}
\hline\hline
{\bf Expression}          & {\bf Numerical value} & {\bf Logical value} \\
\hline
\verb|3*(2 > 1)|          & 3.0           & true\\
\verb|3*(1 > 2)|          & 0.0           & false\\
\verb|(1 < 2) + (2 < 3)|  & 2.0           & true\\
\verb|max((1 < 2),(2 < 3))|& 1.0          & true\\
\verb|2 AND 0.0|          & 0.0           & false\\
\verb|2 AND ZERO|         & 1.0           & true\\
\verb|2 AND NA|           & \verb|NA|     & true\\
\verb|UNDF < 0|           & \verb|UNDF|   & true\\
\hline\hline
\end{tabular}
\caption{Numerical and logical values}\label{table:expr.logic-values}
\end{aimmstable}

\subsection{Logical operator expressions}\label{expr:logic.oper}
\index{operator!logical}

\paragraph{Unary and binary logical operators}%
\operindex{AND}%
\operindex{OR}%
\operindex{XOR}%
\operindex{NOT}

AIMMS supports the \syntaxmark{unary-logical-operator} unary logical
operator \verb|NOT| and the \syntaxmark{binary-logical-operator} binary logical
operators \verb|AND|, \verb|OR|, and \verb|XOR|.
Table~\ref{table:expr.logic-oper} gives the logical results of these operators
for zero and nonzero operands.
\begin{aimmstable}
\begin{tabular}{|c|c||c|c|c|c|}
\hline\hline
\multicolumn{2}{|c||}{{\bf Operands}}&\multicolumn{4}{c|}{{\bf Result}}\\
\hline
\verb|a|&\verb|b|&\verb|a AND b|&\verb|a OR b|&\verb|a XOR b|&\verb|NOT a|\\
\hline
0        &     0      &    0    &       0     &      0       &    1\\
0        &  nonzero  &    0    &       1     &      1       &    1\\
nonzero &     0      &    0    &       1     &      1       &    0\\
nonzero &  nonzero  &    1    &       1     &      0       &    0\\
\hline\hline
\end{tabular}
\caption{Logical operators}\label{table:expr.logic-oper}
\end{aimmstable}

\paragraph{Precedence order}

The precedence order of these operators from highest to lowest is given by
\verb|NOT|, \verb|AND|, \verb|OR|, and \verb|XOR| respectively. Whenever the
precedence order is not immediately clear, it is advisable to use parentheses.
Besides preventing unwanted mistakes, it also make your model easier to
understand and maintain.

\paragraph{Example}
The expression
\begin{example}
    NOT a AND b XOR c OR d
\end{example}
is parsed by AIMMS as if it were written
\begin{example}
    ((NOT a) AND b) XOR (c OR d).
\end{example}

\paragraph{Execution order}

Due to the sparse execution system underlying AIMMS it is not guaranteed
that logical expressions containing binary logical operators are executed in a
strict left-to-right order. If you are a C/C++ programmer (where logical
conditions are executed in a strict left-to-right order), you should take extra
care to ensure that your logical conditions do not depend on this assumption.

\subsection{Numerical comparison}\label{sec:expr.logic.num-rel}
\index{logical expression!numerical comparison} \index{numerical comparison}
\index{comparison!numerical} \AIMMSlink{comparing_numerical_values}

\paragraph{Numerical comparison}%
\operindex{=}%
\operindex{<>}%
\operindex{<=}%
\operindex{<}%
\operindex{>}%
\operindex{>=}

\syntaxmark{relational-operator} Numerical relationships compare two numerical
expressions, using one of the relational operators \verb|=|, \verb|<>|,
\verb|>|, \verb|>=|, \verb|<|, or \verb|<=|. Numerical inclusions are
equivalent to two numerical relationships, and indicate whether a given
expression lies within two bounds.

\paragraph{Syntax}
\syntaxdiagram{expression-relationship}{num-rel}
\syntaxdiagram{expression-inclusion}{num-inc}

\paragraph{Numerical tolerances}
\index{numerical comparison!tolerances}

For two real numbers $x$ and $y$ the result of the comparison $x \gtrless y$,
where $\gtrless$ denotes any relational operator, depends on two tolerances
\begin{itemize}
\item {\tt Equality\_Absolute\_Tolerance}\quad (denoted as
$\varepsilon_a$), and
\item {\tt Equality\_Relative\_Tolerance}\quad (denoted as
$\varepsilon_r$).
\end{itemize}
You can set these tolerances through the options dialog box. Their default
values are $0$ and $10^{-13}$, respectively. If the number $\varepsilon_{x,y}$
is given by the formula
\[
    \varepsilon_{x,y} = \max(\varepsilon_a,\varepsilon_r\cdot
    x,\varepsilon_r \cdot y),
\]
then the relational operators evaluate as shown in the
Table~\ref{table:expr.equal.tol}.
\begin{aimmstable}
\begin{tabular}{|c|c|}
\hline\hline
{\bf AIMMS expression} & {\bf Evaluates as} \\
\hline
$ x \mathtt{=} y$ & $|x-y| \leq \varepsilon_{x,y}$\\
$ x \mathtt{<>} y$ & $|x-y| > \varepsilon_{x,y}$\\
$ x \mathtt{<=} y$ & $x-y \leq \varepsilon_{x,y}$\\
$ x \mathtt{<} y$ & $x-y < -\varepsilon_{x,y}$\\
\hline\hline
\end{tabular}
\caption{Interpretation of numerical tolerances}\label{table:expr.equal.tol}
\end{aimmstable}

\paragraph{Comparison for extended arithmetic}
\index{numerical comparison!extended arithmetic} \index{extended
arithmetic!numerical comparison} \index{special number!numerical comparison}

For any combination of an ordinary real number with one of the special symbols
\verb|ZERO|, \verb|INF|, and \verb|-INF|, the relational operators behave as
expected. If any of the operands is either \verb|NA| or \verb|UNDF|,
relationships other than \verb|=| and \verb|<>| also evaluate to \verb|NA| or
\verb|UNDF| and hence, as a logical expression, to true. In addition, the
logical expressions \verb|INF = INF| and \verb|-INF = -INF| evaluate to true.

\paragraph{Testing for zero value}

One can formulate numerous logical expressions to test for a zero value, and
one should be clear on the desired result. The following example makes the
point.
\begin{example}
    p_inv(i)             := 1 / p(i);
    p_inv(i | p(i))      := 1 / p(i);
    p_inv(i | p(i) <> 0) := 1 / p(i);
\end{example}
The first assignment will produce a runtime error when {\tt p(i)} assumes a
value of 0 or {\tt ZERO}. The second assignment will filter out the 0's, but
not the {\tt ZERO} values because {\tt ZERO} evaluates to the logical value
``true''. The last assignment will never produce runtime errors, because of the
{\em numerical} comparison to 0.

\subsection{Set and element comparison}\label{sec:expr.logic.set-rel}

\paragraph{Set relationships}

AIMMS features very powerful logical set comparison operators. Not only can
sets and their elements be compared using relational operators, but you can
also check for set membership with the \verb|IN| operator.

\paragraph{Syntax}
\syntaxdiagram{set-relationship}{set-rel}

\paragraph{Element relationship and inclusion}
\index{comparison!element} \index{element comparison} \index{logical
expression!element comparison}

Set elements that lie in the same set can be compared according to their
relative position inside that set. You can also compare the positions of
arbitrary set element expressions, as long as AIMMS is able to determine a
unique domain set in which the comparison has to take place. The allowed
relational operators are {\tt =}, {\tt <>}, {\tt <}, {\tt <=}, {\tt >}, and
{\tt >=}. As with numerical expression, AIMMS also allows you to specify an
inclusion relationship as a form of repeated comparison to verify whether an
element lies within two boundary elements.

\paragraph{Element comparison}%
\operindex{=}%
\operindex{<>}%
\operindex{<=}%
\operindex{<}%
\operindex{>}%
\operindex{>=}

The relational operators for element relationships are conveniently defined in
terms of the \verb|Ord| function. Let \verb|S| be a simple set, \verb|i| and
\verb|j| indices or element parameters in \verb|S|, $\pm$ any of the lag or
lead operators {\tt +}, {\tt ++}, {\tt -} or {\tt --}, $m$ and $n$ integer
expressions, and $\gtrless$ one of the operators {\tt =}, {\tt <>}, \verb|<|,
\verb|<=|, \verb|>|, or \verb|>=|. The relational operators $\gtrless$ have the
following definition for set elements, provided that the set elements on both
sides of the relational operator exist.
\[
        \mathtt{i}\pm m\gtrless \mathtt{j}\pm n\quad\Leftrightarrow \quad
                \begin{cases}
                    \mbox{$\mathtt{i}\pm m$ and $\mathtt{j}\pm n$ are both defined, and} \\
                    \mbox{$\mathtt{Ord(i\pm m,S)}\gtrless \mathtt{Ord(j\pm n,S)}$}
                \end{cases}
\]
Note that this type of relational expression evaluates to ``false'' if one or
both of the operands do not refer to existing set elements.

\paragraph{Compare within the same set}

Only elements that lie in the same set are comparable using the {\tt <}, {\tt
<=}, {\tt >}, and {\tt >=} operators. The {\tt =} and {\tt <>} operators can
also be used when the operands merely share the same root set.

\paragraph{Example}

The following set assignments demonstrate the correct use of element
comparisons.
\begin{example}
    FuturePeriods := { t in Periods | CurrentPeriod <= t <= PlanningHorizon } ;

    BandMatrix := { (i,j) | i - BandWidth <= j <= i + BandWidth } ;
\end{example}

\paragraph{Set membership}
\operindex{IN} \index{comparison!set} \index{set comparison} \index{logical
expression!set comparison}

Set membership can be tested using the \verb|IN| operator. This operator checks
whether a set element or an element tuple on the left-hand side is a member of
the set expression on the right-hand side. Both operands must have the same
root set.

\paragraph{Example}

Assume that all one-dimensional sets in the following two assignments share the
same root set {\tt Cities}. Then these statements illustrate the correct use of
the logical {\tt IN} operator.
\begin{example}
     NeighborhoodRoutes := { (i,j) in Routes | j in NeighborhoodCities(i) } ;
     ExcludedCities     := { i in ( SmallCities + ForeignCities ) } ;
\end{example}

\paragraph{Set comparisons}%
\operindex{=}%
\operindex{<>}%
\operindex{<=}%
\operindex{<}%
\operindex{>}%
\operindex{>=}

Sets can be logically compared using any of the relational operators {\tt =},
{\tt <>}, {\tt <}, {\tt <=}, {\tt >} and {\tt >=}. The inequality operators
denote the usual subset relationships. They replace the standard "contained in"
operators $\subsetneq$, $\subseteq$, $\supsetneq$ and $\supseteq$ which are not 
available on many keyboards.

\paragraph{Example}
The following statement illustrates a logical set comparison operator.
\begin{example}
    IF ( RoutesWithTransport <= NeighborhoodRoutes ) THEN
        DialogMessage( "Solution only contains neighborhood transports" );
    ENDIF;
\end{example}

%\paragraph{Set comparisons}
%
%Sets can be logically compared using any of the functions
%\begin{itemize}
%\item {\tt SetEqual},
%\item {\tt SetNotEqual},
%\item {\tt Subset}, and
%\item {\tt SubsetNotEqual}.
%\end{itemize}
%All functions have two set arguments, and return whether the first set
%relates to the second set in the specified manner.
%
%\paragraph{Example}
%The following statement illustrates the use of a logical set
%comparison function.
%\begin{example}
%    IF ( Subset(RoutesWithTransport, NeighborhoodRoutes) ) THEN
%        DialogMessage( "Solution only contains neighborhood transports" );
%    ENDIF;
%\end{example}

\subsection{String comparison}
\index{string comparison} \index{comparison!string} \index{logical
expression!string comparison}

\paragraph{String comparison}%
\operindex{=}%
\operindex{<>}%
\operindex{<=}%
\operindex{<}%
\operindex{>}%
\operindex{>=}

\syntaxmark{string-relationship} Besides their use for comparison of numerical,
element- and set-valued expressions, the relational operators {\tt =}, {\tt
<>}, {\tt <}, {\tt <=}, {\tt >}, and {\tt >=} can also be used for string
comparison. When used for string comparison, AIMMS employs the usual
lexicographical ordering. String comparison in AIMMS is case sensitive by
default, i.e.\ strings that only differ in case are considered to be unequal.
You can modify this behavior through the option {\tt
Case\_Sensitive\_String\_Comparison}.

\paragraph{Examples}

All the following string comparisons evaluate to true.
\begin{example}
    "The city of Amsterdam" <> "the city of amsterdam"     ! Note case
    "The city of Amsterdam" <> "The city of Amsterdam "    ! Note last space
    "The city of Amsterdam" <  "The city of Rotterdam"
\end{example}

\subsection{Logical iterative expressions}\label{sec:expr.logic.iter}
\index{logical iterative operator} \index{iterative operator!logical}

\paragraph{Logical iterative operators}%
\logiterindex{Exists}%
\logiterindex{Atleast}%
\logiterindex{Atmost}%
\logiterindex{Exactly}%
\logiterindex{ForAll}%
\AIMMSlink{Exists}%
\AIMMSlink{Atleast}%
\AIMMSlink{Atmost}%
\AIMMSlink{Exactly}%
\AIMMSlink{ForAll}

Logical iterative operators verify whether some or all elements in a domain
satisfy a certain logical condition. Table~\ref{table:expr.logic-iter} lists
all logical iterative operators supported by AIMMS. The second column in
this table refers to the required number of expression arguments following the
binding domain argument.
\begin{aimmstable}
\begin{tabular}{|l|c|p{8cm}|}
\hline\hline
{\bf Name} & {\bf \# Expr.}   & {\bf Meaning} \\
\hline
{\tt Exists}      & 0 & \pbsr true if the domain is not empty\\
{\tt Atleast}     & 1 & \pbsr true if the domain contains
at least $n$ elements\\
{\tt Atmost}     & 1 & \pbsr true if the domain contains
at most $n$ elements\\
{\tt Exactly}     & 1 & \pbsr true if the domain contains
at exactly $n$ elements\\
{\tt ForAll}     & 1 & \pbsr true if the expression is true
for all elements in the domain\\
\hline\hline
\end{tabular}
\caption{Logical iterative operators}\label{table:expr.logic-iter}
\end{aimmstable}

\paragraph{Example}
The following statements illustrate the use of some of the logical iterative
operators listed in Table~\ref{table:expr.logic-iter}.
\begin{example}
    MultipleSupplyCities := { i | Atleast( j | Transport(i,j), 2 ) } ;

    IF ( ForAll( i, Exists( j | Transport(i,j) ) ) ) THEN
        DialogMessage( "There are no cities without a transport" );
    ENDIF ;
\end{example}


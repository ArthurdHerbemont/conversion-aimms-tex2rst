\section{\tttext{MACRO} declaration and attributes}\label{sec:expr.macro}

\paragraph{{\tt Macro} facility}

The \verb|MACRO| facility offers a mechanism for parameterizing expressions.
Macros are useful for enhancing the readability of models, and avoiding
inconsistencies in frequently used expressions.

\paragraph{Declaration and attributes}
\declindex{MACRO} \AIMMSlink{macro}

Macros are declared as ordinary identifiers in your model. They can have
arguments.  The attributes of a {\tt Macro} declaration are listed in
Table~\ref{table:expr.attr-macro}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}               & {\bf See also page} \\
\hline \verb|Text|         & {\em string}                     &
\pageref{attr:prelim.text}\\
\verb|Arguments|    & {\em argument-list}              & \\
\verb|Comment|      & {\em comment string}             & \pageref{attr:prelim.comment}\\
\verb|Definition|   & {\em expression}                 & \pageref{attr:set.definition}\\
\hline\hline
\end{tabular}
\caption{{\tt Macro} attributes}\label{table:expr.attr-macro}
\end{aimmstable}

\paragraph{The {\tt Definition} attribute}
\declattrindex{macro}{Definition} \declattrindex{macro}{Arguments}
\AIMMSlink{macro.definition} \AIMMSlink{macro.arguments}

The {\tt Definiton} attribute of a macro declaration is the replacement text
that is substituted when a macro is used in the model text. The (optional) {\tt
Arguments} of a macro must be scalar entities. Unlike function arguments,
however, you do not have to declare {\tt Macro} arguments as local identifiers.
The {\tt Definition} of a macro must be a valid expression in its arguments.

\paragraph{Example}

When you define a macro with arguments, the actual replacement text depends on
the arguments that are supplied to it, as illustrated in the following example.
Using the macro declaration
\begin{example}
Macro MyAverage {
    Arguments  : (dom, expr);
    Definition : Sum(dom, expr) / Count(dom);
}
\end{example}
the assignments
\begin{example}
   AverageTransport   := MyAverage( (i,j), Transport(i,j) );
   AverageNZTransport := MyAverage( (i,j) | Transport(i,j),  Transport(i,j) );
\end{example}
are compiled as if they read:
\begin{example}
   AverageTransport   := Sum( (i,j), Transport(i,j) ) / Count( (i,j) );
   AverageNZTransport :=
          Sum  ( (i,j) | Transport(i,j),  Transport(i,j) ) /
          Count( (i,j) | Transport(i,j) );
\end{example}

\paragraph{Expression substitution}

When you use a macro with arguments, the actual arguments {\em must} be valid
expressions. As a result, there is no need to add additional braces to the
replacement text of the macro, like, for instance, in the C programming
language. The following example illustrates this point.
\begin{example}
Macro MyMult {
    Arguments  : (x,y);
    Definition : x*y;
}
\end{example}
Using this macro, the expression
\begin{example}
    a + MyMult(b+c,d+e) + f
\end{example}
will evaluate to
\begin{example}
    a + ((b+c)*(d+ e)) + f
\end{example}
instead of
\begin{example}
    a + b + c*d + e + f
\end{example}

\paragraph{Macro versus defined parameters}
\index{use of!macro} \index{macro!versus defined parameter} \index{defined
parameter!versus macro} \index{parameter!versus macro}

In many execution statements you have a choice to use either macros or defined
parameters as a mechanism to replace complicated expressions by descriptive
names. While a macro is purely substituted by its replacement text, the current
value of a defined parameter is stored and looked up when needed. When deciding
whether to use a macro or a defined parameter, you should consider both storage
and computational consequences. Macros are recomputed every time they are
referenced, and therefore there may be an unnecessary time penalty if the macro
is called with identical arguments in more than one place within your model.
When storage considerations are important, a macro may be attractive since it
does not introduce additional parameters.

\paragraph{Macro versus defined variables}
\index{macro!versus defined variable} \index{defined variable!versus macro}
\index{variable!versus macro}

You should also consider your choices when you use a macro with variables as
arguments in a constraint. In this case, you also have the option to use a
defined variable, or a defined {\tt Inline} variable (see also
Section~\ref{sec:var.var}). The following considerations are of interest.
\begin{itemize}
\item A macro can produce different expressions of the same structure
for different identifier arguments, but does not allow you to specify a domain
restriction that will reduce the number of generated columns in the matrix.
\item Defined and {\tt Inline} variables support an index domain
to restrict the number of generated columns, but only allow an expression in
terms of fixed identifiers. Compared to a macro or an {\tt Inline} variable,
the number of rows and columns increases for a defined variable, but if the
variable is referenced more than once in the other constraints, it will result
in a smaller number of nonzeros.
\item An advantage of variables (both defined and {\tt Inline}) over
macros is that their final values are stored by AIMMS, and can be retrieved
in other execution statements or in the graphical user interface, whereas a
macro has to be recomputed all the time.
\end{itemize}
\section{\tttext{Parameter} declaration and attributes}\label{sec:par.decl}

\paragraph{Declaration and attributes}
\index{parameter!value type} \index{parameter!value type!number}
\index{parameter!value type!string} \index{parameter!value type!element}
\index{parameter!value type!unit} \index{value type} 
\declindex{Parameter}
\declindex{ElementParameter}
\declindex{StringParameter}
\declindex{UnitParameter}
\AIMMSlink{parameter} \AIMMSlink{string_parameter}
\AIMMSlink{element_parameter}

There are four parameter types in AIMMS that can hold data of the following four data types:
\begin{itemize}
\item {\bf\tt Parameter} for numeric values,
\item {\bf\tt StringParameter} for strings,
\item {\bf\tt ElementParameter} for set elements, and
\item {\bf\tt UnitParameter} for unit expressions.
\end{itemize}
Prior to declaring a parameter in the model editor you need to decide on its
data type. In the model tree parameters of each type have their own icon. The
attributes of parameters are given in Table~\ref{table:par.attr-param}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute}             & {\bf Value-type}                 & {\bf See also} \\
                            &                                  & {\bf page} \\
\hline
\verb|IndexDomain|         & {\em index-domain}               & \\
\verb|Range|                & {\em range}                      & \\
\verb|Default|              & {\em  constant-expression}       & \\
\verb|Unit|                 & {\em unit-expression}            & \\
\verb|Property|             & \verb|NoSave|, {\tt Stochastic}, & \\
                            & {\tt Uncertain}, {\tt Random},   & \\ 
                            & {\em numeric-storage-property}   & \pageref{sec:par.uncertainty} \\
\verb|Text|                 & {\em string}                     & \pageref{attr:prelim.text}\\
\verb|Comment|              & {\em comment string}             & \pageref{attr:prelim.comment}, \pageref{attr:set.comment}\\
\verb|Definiton|           & {\em expression}                 & \pageref{attr:set.definition}\\
\verb|InitialData|         & {\em data enumeration}           & \pageref{attr:par.initialdata}\\
\hline
\verb|Uncertainty|          & {\em expression}				   & \pageref{attr:par.uncertainty}, \pageref{attr:robust.uncertainty}\\
\verb|Region| 		        & {\em expression}				   & \pageref{attr:par.region}, \pageref{attr:robust.region}\\
\verb|Distribution| 		& {\em expression}				   & \pageref{attr:par.distribution}, \pageref{attr:robust.distribution}\\
\hline\hline
\end{tabular}
\caption{{\tt Parameter} attributes}\label{table:par.attr-param}
\end{aimmstable}

\paragraph{Basic examples}
The following declarations demonstrate some basic parameter
declarations
\begin{example}
Parameter Population {
     IndexDomain  :  i;
     Range        :  [0,inf);
     Unit         :  [ 1000 ];
     Text         :  Population of city i in thousands;
}
Parameter Distance {
     IndexDomain  :  (i,j);
     Range        :  [0,inf);
     Unit         :  [ km ];
     Text         :  Distance from city i to city j in km;
}
ElementParameter cityWithLargestPopulation {
     Range        :  cities;
     Definition   :  argMax( i, Population( i ) );
}
StringParameter emergencyMessage {
     InitialData :  "Warning";
}
Quantity Currencies {
     BaseUnit     :  dollar;
     Conversions  :  euro -> dollar : # -> # * 1.3;
}
UnitParameter selectedCurrency {
     InitialData :  [euro];
}
\end{example}

\paragraph{The {\tt IndexDomain} attribute}\herelabel{attr:par.index-domain}
\declattrindex{parameter}{IndexDomain} \index{index domain}
\AIMMSlink{parameter.index_domain}

For each multidimensional identifier you need to specify its dimensions by
providing a list of index bindings at the {\tt IndexDomain} attribute.
Identifiers without an {\tt IndexDomain} are said to be {\em scalar}. In the
index domain you can specify default or local bindings to simple sets.
The totality of dimensions of all bindings determine the total
dimension of the identifier. Any references outside the index domain, either
through execution statements or from within the graphical user interface are
skipped.

\paragraph{Domain condition}
\index{domain!index|see{index domain}} \index{index domain!condition}
\index{parameter!domain condition}

You can also use the {\tt IndexDomain} attribute to specify a logical
expression which further restricts the valid tuples in the domain. During
execution, assignments to tuples that do not satisfy the domain condition are
ignored. Also, evaluation of references to such tuples in expressions will
result in the value zero. Note that, if the domain condition contains
references to other data in your model, the set of valid tuples in the domain
may change during a single interactive session.

\paragraph{Example}

Consider the sets {\tt ConnectedCities} with default index {\tt cc} and {\tt
DestinationCities\-FromSupply(i)} from the previous chapter. The following
statements illustrate a number of possible declarations of the two-dimensional
identifier {\tt Unit\-Trans\-portCost} with varying index domains.
\begin{example}
Parameter UnitTransportCost {
    IndexDomain : (i,j);
}
Parameter UnitTransportCostWithCondition {
    IndexDomain : (i,j) in ConnectedCities;
}
Parameter UnitTransportCostWithIndexedDomain {
    IndexDomain : (i, j in DestinationCitiesFromSupply(i)); 
}
\end{example}

\paragraph{Explanation}
The identifiers defined in the previous example will behave as follows.
\begin{itemize}
\item
The identifier {\tt UnitTransportCost} is defined over the full Cartesian
product {\tt Cities} $\times$ {\tt Cities} by means of the default bindings of
the indices {\tt i} and~{\tt j}. You will be able to assign values to every
pair of cities ({\tt i},{\tt j}), even though there is no connection between
them.
\item
The identifier {\tt UnitTransportCostWithCondition} is defined over the same
Cartesian product of sets. Its domain, however, is restricted by an additional
condition {\tt (i,j) in ConnectedCities} which will exclude assignments to
tuples that do not satisfy this condition, or evaluate to zero when referenced.
\item
Finally, the identifier {\tt UnitTransportCostWithIndexedDomain} is defined
over a subset of the Cartesian product {\tt Cities} $\times$ {\tt Cities}. The
second element {\tt j} must lie in the subset {\tt DestinationCities(i)}
associated with {\tt i}. AIMMS will produce a domain error if this condition
is not satisfied.
\end{itemize}

\paragraph{The {\tt Range} attribute}\herelabel{attr:par.range}
\declattrindex{parameter}{Range} \typindex{range}{Real}
\typindex{range}{Nonnegative} \typindex{range}{Nonpositive}
\typindex{range}{Integer} \typindex{range}{Binary} \index{range!interval}
\index{interval range} \index{range!integer} \index{integer range}
\index{range!set} \AIMMSlink{parameter.range}

With the {\tt Range} attribute you can restrict the values to certain intervals or sets. 
The {\tt Range} attribute is not applicable to a {\tt StringParameter} nor to a {\tt Unit\-Parameter}. 
The possible values for the {\tt Range} attribute are:
\begin{itemize}
\item one of the predefined ranges {\tt Real}, {\tt Nonnegative}, {\tt
      Nonpositive}, {\tt Integer}, or {\tt Binary},
      \item any one of the interval expressions {\tt [}$a,b${\tt ]}, {\tt
      [}$a,b${\tt )}, {\tt (}$a,b${\tt ]}, or {\tt (}$a,b${\tt )}, where a square
      bracket implies inclusion into the interval and a round bracket implies
      exclusion,
\item any enumerated integer set expression, e.g.\ \verb|{|$a$ {\tt
      ..} $b$\verb|}| covering all integers from $a$ until and including $b$,
\item a set reference, if you want the values to be elements of
      that set. For set element-valued parameters this entry is mandatory.
\end{itemize}
The values for $a$ and $b$ can be a constant number, {\tt inf}, {\tt -inf}, or
a parameter reference involving some or all of the indices on the index domain
of the declared identifier.

\paragraph{Example}
Consider the following declarations.
\begin{example}
Parameter UnitTransportCost {
    IndexDomain  :  (i,j);
    Range        :  [ UnitLoadingCost(i), 100 ];
}
Parameter  DefaultUnitsShipped {
    IndexDomain  :  (i,j);
    Range        :  {
        { MinShipment(i) .. MaxShipment(j) }
    }
}
Set States {
    Index        :  s;
}
Set adjacentStates {
    SubsetOf     :  States;
    IndexDomain  :  s;
}
ElementParameter nextState {
    IndexDomain  :  s;
    Range        :  adjacentStates(s);
}
\end{example}
It limits the values of the identifier {\tt UnitTransportCost(i,j)} to an
interval from {\tt UnitLoadingCost(i)} to 100. Note that the lower bound of the
interval has a smaller dimension than the identifier itself. The integer
identifier {\tt Default\-UnitsShipped(i,j)} is limited to an integer range
through an enumerated integer range inside the set brackets.

\paragraph{The {\tt Default} attribute}\herelabel{attr:par.default}
\declattrindex{parameter}{Default} \AIMMSlink{parameter.default}

In AIMMS, parameters that have not been assigned an explicit value are given
a default value automatically. You can specify the default value with the {\tt
Default} attribute. The value of this attribute {\em must} be a constant
expression. If you do not provide a default value for the parameter, AIMMS
will assume the following defaults:
\begin{itemize}
\item $0$ for numbers,
\item $1$ for unit-valued parameters,
\item the empty string {\tt ""} for strings, and
\item the empty element {\tt ''} for set elements.
\end{itemize}

\paragraph{The {\tt Definition} attribute}\herelabel{attr:par.definition}
\declattrindex{parameter}{Definition} \AIMMSlink{parameter.definition}

The {\tt Definition} attribute of a parameter can contain a valid (indexed) numerical
expression. Whenever a defined parameter is referenced inside your model,
AIMMS will, by default, recompute the associated data if (data) changes to
any of the identifiers referenced in its definition make its current data
out-of-date. In the definition expression you can refer to any of the indices
in the index domain as if the definition was the right-hand side of an
assignment statement to the parameter at hand (see also
Section~\ref{sec:exec.assign}).

\paragraph{Example}

The following declaration illustrates an indexed {\tt Definition} attribute.
\begin{example}
Parameter MaxTransportFrom {
    IndexDomain  : i;
    Definition   : Max(j, Transport(i,j));
}
\end{example}

\paragraph{Care when used in loops}

Whenever you provide a definition for an {\em indexed} parameter, you should
carefully verify whether and how that parameter is used in the context of one
of AIMMS' loop statements (see also Section~\ref{sec:exec.flow}). When, due
to changes in only a slice of the dependent data of a definition during a
previous iteration, AIMMS (in fact) only needs to evaluate a single slice of
a defined parameter during the actual iteration, you should probably not be
using a defined parameter. AIMMS' automatic evaluation scheme for defined
identifiers will always recompute the data for such identifiers {\em for the
whole domain of definition}, which can lead to severe inefficiencies for
high-dimensional defined parameters. You can find a more detailed discussion on
this issue in Section~\ref{sec:eff.definition}.

\paragraph{The {\tt Unit} attribute}\herelabel{attr:par.unit}
\declattrindex{parameter}{Unit} \AIMMSlink{parameter.unit}

By associating a {\tt Unit} to every numerical identifier in your model, you
can let AIMMS help you check your model's consistency. AIMMS also uses
the {\tt Unit} attribute when presenting data and results in both the output
files of a model and the graphical user interface. You can find more
information on the use of units in Chapter~\ref{chap:units}.

\paragraph{The {\tt Property} attribute}\herelabel{attr:par.property}
\declattrindex{parameter}{Property} \AIMMSlink{parameter.property}
\declpropindex{parameter}{Integer} \declpropindex{parameter}{Integer8}
\declpropindex{parameter}{Integer16} \declpropindex{parameter}{Integer32}
\declpropindex{parameter}{Double} \declpropindex{parameter}{NoSave}
\declpropindex{parameter}{Stochastic}\declpropindex{parameter}{Uncertain}
\declpropindex{parameter}{Random}

The {\tt Property} attribute can hold various properties of the identifier at
hand. The allowed properties for a parameter are {\tt NoSave} or one of the numerical storage properties {\tt Integer}, {\tt Integer32}, {\tt Integer16}, {\tt Integer8} or {\tt Double}, in addition to the properties {\tt Stochastic}, {\tt Uncertain}, {\tt Ran\-dom} which are discussed in Section~\ref{sec:par.uncertainty}.
\begin{itemize}
\item The property {\tt NoSave} indicates whether the identifier
values are stored in cases. It is discussed in detail in
Section~\ref{attr:set.property}.
\item By default, the values of numeric parameters are stored as
double precision floating point numbers. By specifying one of the storage
properties {\tt Integer}, {\tt Integer32}, {\tt Integer16}, {\tt Integer8}, or
{\tt Double} AIMMS will store the values of the identifier as (signed)
integers of default machine length, 4 bytes, 2 bytes or 1 byte, or as a double
precision floating point number respectively. These properties are only
applicable to parameters with an integer range.
\end{itemize}

\paragraph{The {\tt Property} statement}
\statindex{PROPERTY}

During execution you can change the properties of a parameter through the {\tt
Property} statement. The syntax of the {\tt Property} statement and examples of
its use can be found in Section~\ref{sec:exec.property}.

\paragraph{The {\tt Text} attribute}\herelabel{attr:par.text}
\declattrindex{parameter}{Text} \AIMMSlink{parameter.text}

With the {\tt Text} attribute you can provide one line of descriptive text for
the end-user. If the {\tt Text} string of an indexed parameter or variable
contains a reference to one or more indices in the index domain, then the
corresponding elements are substituted for these indices in any display of the
identifier text.

\subsection{Properties and attributes for uncertain data}\label{sec:par.uncertainty}

\paragraph{Stochastic programming and robust optimization}

The AIMMS modeling language allows you to specify both stochastic programs and robust optimization models. 
Both methodologies are designed to deal with models involving data uncertainty. 
In stochastic programming the uncertainty is expressed by 
specifying multiple scenarios, each of which can define scenario-specific values for certain parameters in your model. 
Stochastic programming is discussed in full detail in Chapter~\ref{chap:stoch}.
For robust optimization, parameters can be declared to not have a single fixed value, but to take their values from an 
user-defined uncertainty set. Robust optimization is discussed in Chapter~\ref{chap:robust}.

\paragraph{Properties}
The following {\tt Parameter} properties are available in support of stochastic programming and robust optimization models.
\begin{itemize}
\item The property {\tt Stochastic} indicates that the identifier
can hold stochastic event data for a stochastic model. It is discussed in detail in
Section~\ref{sec:stoch.stoch}.
\item The property {\tt Uncertain} indicates that the identifier
can hold uncertain values from an uncertainty set specified through the 
{\tt Uncertainty} and/or {\tt Region} attributes. Uncertain parameters are used in 
AIMMS' robust optimization facilities, and are discussed in detail in
Section~\ref{sec:robust.uncertain}.
\item The property {\tt Random} indicates that the identifier
can hold random values with respect to a distribution with characteristics specified through the 
{\tt Distribution} attribute. Random parameters are used in 
AIMMS' robust optimization facilities, and are discussed in detail in
Section~\ref{sec:robust.chance}.
\end{itemize}

\paragraph{The {\tt Uncertainty} and {\tt Region}  attributes}\label{attr:par.uncertainty}\label{attr:par.region}
\declattrindex{parameter}{Uncertainty} \AIMMSlink{parameter.uncertainty}
\declattrindex{parameter}{Region} \AIMMSlink{parameter.region}

The {\tt Uncertainty} and {\tt Region} attributes are available if the parameter at hand has been declared
uncertain using the {\tt Uncertain} property. Uncertain parameters are used by AIMMS' robust optimization framework, and are 
discussed in full detail in Section~\ref{sec:robust.uncertain}. With the {\tt Region} attribute you can specify an uncertainty set  
using one of the predefined uncertainty sets {\tt Box}, {\tt ConvexHull} or {\tt Ellipsoid}. The {\tt Uncertainty} attribute specifies 
a relationship between the uncertain parameter at hand, and one or more other (uncertain) parameters in your model. The 
{\tt Uncertainty} and {\tt Region} attributes are not exclusive, i.e., you are allowed to specify both, in which case AIMMS' 
generation process of the robust counterpart will make sure that both conditions are satisfied by the final solution. 

\paragraph{The {\tt Distribution} attribute}\label{attr:par.distribution}
\declattrindex{parameter}{Distribution} \AIMMSlink{parameter.distribution}

The {\tt Distribution} attribute is available if the parameter at hand has been declared
random using the {\tt Random} property. Random parameters are used by AIMMS' robust optimization framework, and are discussed in full detail in Section~\ref{sec:robust.chance}. With the {\tt Distribution} attribute you can declare that the values for the random parameter at hand adhere to one of the predefined distributions discussed in Section~\ref{sec:robust.chance}. 

\chapter{Parameter Declaration}\label{chap:par}

\paragraph{Terminology}

The word parameter does not have a uniform meaning in the scientific community.
When you are a statistician, you are likely to view a parameter as an unknown
quantity to be estimated from observed data. {\em In AIMMS the word
parameter denotes a known quantity that holds either numeric or string-valued
data.} In programming languages the term variable is used for this purpose.
However, this is not the convention adopted in AIMMS, where, in the context
of a mathematical program, the word variable is reserved for an unknown
quantity. Outside this context, a variable behaves as if it were a parameter.
The terminology in AIMMS is consistent with the standard operations research
terminology that distinguishes between parameters and variables.

\paragraph{Why use parameters}
\index{parameter!use of} \index{use of!parameter}

Rather than putting the explicit data values directly into your expressions, it
is a much better practice to group these values together in parameters and to
write all your expressions using these symbolic parameters. Maintaining a model
that contains explicit data is a painstaking task and error prone, because the
meaning of each separate number is not clear. Maintaining a model in symbolic
form, however, is much easier and frequently boils down to simply adjusting the
data of a few clearly named parameters at a single point.

\paragraph{Example}

Consider the set {\tt Cities} introduced in the previous chapter and a
parameter {\tt FixedTransport(i,j)}. Suppose that the cost of each unit of
transport between cities {\tt i} and {\tt j} is stored in the parameter {\tt
UnitTransportCost(i,j)}. Then the definition of {\tt TotalTransportCost} can be
expressed as
\begin{example}
    TotalTransportCost := sum[(i,j), UnitTransportCost(i,j)*FixedTransport(i,j)];
\end{example}
Not only is this expression easy to understand, it also makes your model
extendible. For instance, an extra city can be added to your model by simply
adding an extra element to the set {\tt Cities} as well as updating the tables
containing the data for the parameters {\tt UnitTransportCost} and {\tt
FixedTransport}. After these changes the above statement will automatically
compute {\tt Total\-TransportCost} based on the new settings without any
explicit change to the symbolic model formulation.


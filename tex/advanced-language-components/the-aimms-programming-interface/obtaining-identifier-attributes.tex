\section{Obtaining identifier attributes}\label{sec:api.attribute}

\paragraph{Obtaining attributes}
\index{application programming interface!handle attributes}
\index{handle!attributes}

For every identifier handle passed to (or created from within) an external
function, AIMMS can provide additional attributes that are either related to
the declaration of the identifier associated with the handle, or to the
particular identifier slice that was passed as an argument in the external
function call. Table~\ref{table:api.info} lists all AIMMS API functions
which can be used to obtain these additional attributes.
\begin{aimmstable}
\typindex{API function}{AimmsAttributeName} \typindex{API
function}{AimmsAttributeType} \typindex{API function}{AimmsAttributeStorage}
\typindex{API function}{AimmsAttributeDefault} \typindex{API
function}{AimmsAttributeDimension} \typindex{API
function}{AimmsAttributeRootDomain} \typindex{API
function}{AimmsAttributeCallDomain} \typindex{API
function}{AimmsAttributeRestriction} \typindex{API
function}{AimmsAttributeSlicing} \typindex{API
function}{AimmsAttributePermutation} \typindex{API
function}{AimmsAttributeFlagsSet} \typindex{API
function}{AimmsAttributeFlagsGet} \typindex{API function}{AimmsAttributeFlags}
\typindex{API function}{AimmsAttributeElementRange} \typindex{API
function}{AimmsAttributeSetUnit}\typindex{API
function}{AimmsAttributeGetUnit}\size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsAttributeName(int handle, AimmsString *name)|\\
\verb|int AimmsAttributeType(int handle, int *type)|\\
\verb|int AimmsAttributeStorage(int handle, int *storage)|\\
\verb|int AimmsAttributeDefault(int handle, AimmsValue *value)|\\
\hline
\verb|int AimmsAttributeSetUnit(int handle, char *unit, char *convention)|\\
\verb|int AimmsAttributeGetUnit(int handle, AimmsString *unitName)|\\
\hline
\verb|int AimmsAttributeDimension(int handle, int *full, int *slice)|\\
\verb|int AimmsAttributeRootDomain(int handle, int *domain)|\\
\verb|int AimmsAttributeDeclarationDomain(int handle, int *domain)|\\
\verb|int AimmsAttributeCallDomain(int handle, int *domain)|\\
\verb|int AimmsAttributeRestriction(int handle, int *domainhandle)|\\
\verb|int AimmsAttributeSlicing(int handle, int *slicing)|\\
\verb|int AimmsAttributePermutation(int handle, int *permutation)|\\
\hline
\verb|int AimmsAttributeFlagsSet(int handle, int flags)|\\
\verb|int AimmsAttributeFlagsGet(int handle, int *flags)|\\
\verb|int AimmsAttributeFlags(int handle, int *flags)|\\
\hline
\verb|int AimmsAttributeElementRange(int handle, int *sethandle)|\\
\hline\hline
\end{tabular}
\caption{AIMMS API functions for obtaining handle
attributes}\label{table:api.info}
\end{aimmstable}

\paragraph{Identifier name and type}
\typindex{API function}{AimmsAttributeName}

With the functions {\tt AimmsAttributeName} and {\tt AimmsAttributeType} you
can request the name and identifier type of the identifier associated with a
handle. AIMMS passes the name of an identifier through an {\tt AimmsString}
structure (explained below). AIMMS only allows handles for identifier types
with which data can be associated. More specifically, AIMMS distinguishes
the following identifier types:
\begin{itemize}
\item simple root set,
\item simple subset,
\item relation,
\item indexed set,
\item numeric parameter,
\item element parameter,
\item string parameter,
\item unit parameter,
\item variable,
\item element variable.
\end{itemize}
When the handle refers to a suffix of an identifier, the suffix type is
appended to the identifier name separated by a dot.

\paragraph{Storage type}
\typindex{API function}{AimmsAttributeStorage}

In addition to the identifier type, AIMMS also associates a storage type
with each handle. It is the data type in which AIMMS expects the data values
associated with the handle to be communicated. The function {\tt
AimmsAttributeStorage} returns the storage type. The possible storage types
are:
\begin{itemize}
\item double ({\tt double}),
\item integer ({\tt int}),
\item binary ({\tt int}, but only assuming 0-1 values),
\item string ({\tt char *}).
\end{itemize}
The complete list of identifier and storage type values returned by these
functions can be found in the header file {\tt aimmsapi.h}.

\paragraph{Default value}
\typindex{API function}{AimmsAttributeDefault}

With the function {\tt AimmsAttributeDefault} you can obtain the default value
of the identifier associated with a handle. The default value can either be a
double, integer or string value, depending on the storage type associated with
the handle. Below you will find the convention used by AIMMS to pass such
storage type dependent values back and forth.

\paragraph{Setting and getting units}

Through the functions {\tt AimmsAttributeGetUnit} and {\tt AimmsAttributeSetUnit} you can
get and set the units of measurement (see also Chapter~\ref{chap:units}) that will be used
when passing data from and to AIMMS. When setting the unit to be used, you can specify
both a unit and a convention. AIMMS will parse the given unit expression and use the specified
convention to compute the appriopriate multiplication factors between the internal and external
representation of the data of the identifier at hand.

\paragraph{Passing integer, double or string values}\herelabel{expl:extern.passing-values}
\typindex{API type}{AimmsString} \typindex{API type}{AimmsValue}

All transfer of integer, double or string values takes place through the record
structures {\tt AimmsString} and {\tt AimmsValue} defined as follows.
\begin{example}
    typedef struct AimmsStringRecord {
      int   Length;
      char *String;
    } AimmsString;
\end{example}
\clearpage
\begin{example}
    typedef union AimmsValueRecord {
      double      Double;
      int         Int;
      struct {
        int       Length;
        char     *String;
      }
    } AimmsValue;
\end{example}
When {\tt value} is such a structure, you can obtain an integer, double or
string value through {\tt value.Int}, {\tt value.Double} or {\tt value.String},
respectively.

\paragraph{Passing string lengths}

For strings, you must set {\tt value.Length} to the length of the string buffer
passed through {\tt value.String} before calling the API function. When
AIMMS fills the {\tt value.String} buffer, the actual length of the string
passed back is assigned to {\tt value.Length}. When the actual string length
exceeds the buffer size, AIMMS truncates the string passed back through {\tt
value} to the indicated buffer size, and assigns the length of the actual
string to {\tt value.Length}.

\paragraph{Identifier dimensions}
\typindex{API function}{AimmsAttributeDimension}

For each handle you can obtain the dimension of the associated identifier by
calling the function {\tt AimmsAttributeDimension}. The function returns:
\begin{itemize}
\item the {\em full} dimension of the identifier as given in its
declaration, and
\item the {\em slice} dimension, i.e.\ the resulting dimension of the
actual identifier slice associated with the handle.
\end{itemize}
AIMMS uses tuples of length equal to the full dimension whenever information
is communicated regarding the index domain of a handle or its slicing. When
explicit data values associated with a handle are passed using the AIMMS API
functions discussed in Section~\ref{sec:api.value}, AIMMS communicates such
values using tuples of length equal to the slice dimension.

\paragraph{Set dimensions}

For all data communication with external DLLs AIMMS considers sets to be
represented by binary indicator parameters indexed over their respective
root sets. For all elements in these root sets, such an
indicator parameter assumes the value 1 if a root set element (or tuple of root
set elements) is contained in the set at hand, or 0 otherwise. Since the
default of these indicator parameters is 0, AIMMS only needs to communicate
the nonzero values, i.e.\ exactly the tuples that are actually contained in the
set. In connection with this representation, AIMMS returns the following
(full or slice) dimensions for sets:
\begin{itemize}
\item the dimension of a {\em simple set} is 1,
\item the dimension of a {\em relation} is the dimension of the Cartesian
product of which the relation is a subset,
\item the dimension of an {\em indexed set} is the dimension of
the index domain of the set plus 1.
\end{itemize}

\paragraph{Identifier domains}
\typindex{API function}{AimmsAttributeRootDomain} \typindex{API
function}{AimmsAttributeCallDomain} \typindex{API
function}{AimmsAttributeDeclarationDomain}

The functions {\tt AimmsAttributeRootDomain}, {\tt
AimmsAttributeDeclarationDomain} and {\tt AimmsAttributeCallDomain} can be used
to obtain an integer array containing handles to domain sets for every
dimension of the identifier at hand. These domains play a different role in the
sparse data communication, as explained below.

\paragraph{Root domain handles}
\typindex{API function}{AimmsAttributeRootDomain}

The function {\tt AimmsAttributeRootDomain} returns an array of handles to the
respective root sets associated with the index domain specified in the
identifier's declaration. You need these handles, for instance, to obtain a
string representation of the element numbers returned by the data communication
AIMMS API functions discussed in Section~\ref{sec:api.value}.

\paragraph{Declaration domain handles}
\typindex{API function}{AimmsAttributeDeclarationDomain}

The function {\tt AimmsAttributeDeclarationDomain} returns an array of handles
to the respective domain sets specified in the identifier's declaration. These
domain sets can be equal to their corresponding root sets, or to subsets
thereof. AIMMS will only pass data values for element tuples in the
declaration domain, unless you have specified the {\tt raw} translation
modifier (see also Section~\ref{sec:extern.declaration}) for a handle argument,
or have created the handle yourself with the raw flag set (see also
Section~\ref{sec:api.identifier}).

\paragraph{Call domain handles}
\typindex{API function}{AimmsAttributeCallDomain}

The function {\tt AimmsAttributeCallDomain} returns an array of handles to the
particular subsets of the root sets (as returned in the root domain of the
handle) to which data communication is restricted for this handle. The call
domain can be different from the global domain if an actual external argument
has been restricted to a subdomain of the root set in an external call (see
also Section~\ref{sec:intern.ref}), or if you have created the handle with an
explicit call domain yourself (see also Section~\ref{sec:api.identifier}).
AIMMS will only pass data values associated with element tuples in just the
call domain (raw flag set), or in the intersection of the call and declaration
domain (raw flag not set).

\paragraph{Domain restriction}
\typindex{API function}{AimmsAttributeRestriction}

With the function {\tt AimmsAttributeRestriction} you can obtain a handle to
the global domain restriction of an indexed identifier as specified in its
declaration and (dynamically) maintained by AIMMS as necessary. You may want
to use this handle in conjunction with raw handles (explained in
Section~\ref{sec:api.value}) to verify whether a particular element satisfies
its domain restriction.

\paragraph{Example}

Consider the following set and parameter declarations.
\begin{example}
Set S_0 {
    Index        : i_0;
}
Set S_1 {
    SubsetOf     : S_0;
    Index        : i_1, j_1;
}
\end{example}
\clearpage
\begin{example}
Set S_2 {
    SubsetOf     : S_1;
    Index        : i_2;
}
Parameter p {
    IndexDomain  : i_0;
}
Parameter q {
    IndexDomain  : (i_1, j_1) | p(i_1);
}
\end{example}
A handle to (in AIMMS notation) \verb|q(i_1, i_2)| will return handles to
\begin{itemize}
\item \verb|S_0| and \verb|S_0| for the respective root domains,
\item \verb|S_1| and \verb|S_1| for the respective declaration
domains,
\item \verb|S_1| and \verb|S_2| for the respective call domains, and
\item \verb|p(i_1)| for the domain restriction.
\end{itemize}

\paragraph{Slicing}
\typindex{API function}{AimmsAttributeSlicing}

As discussed in Section~\ref{sec:intern.ref}, the actual arguments in a
procedure or function call can be slices of higher-dimensional identifiers
within your model. When the slice dimension of a handle in an external call is
less then its full dimension, you can use use the function {\tt
AimmsAttributeSlicing} to find out which dimensions of the associated AIMMS
identifier have been sliced, and to which elements. The function returns an
integer array containing, for every dimension, the element number (within the
associated root set) to which the corresponding domain has been sliced, or the
number {\tt AIMMSAPI\_NO\_ELEMENT} if no slicing took place.

\paragraph{Domain permutations}
\typindex{API function}{AimmsAttributePermutation}

Through the function {\tt AimmsAttributePermutation} you can obtain the
permutation of a permuted handle created with the function {\tt
AimmsAttributeHandle\-CreatePermuted}. The output {\tt permutation} argument
must be an integer array of length equal to the full dimension of the
identifier. AIMMS returns the following values:
\begin{itemize}
\item if a dimension of the handle is sliced, the corresponding position in the
{\tt permutation} array will be 0,
\item if a dimension is not sliced, the corresponding position in the {\tt
permutation} array will contain the sliced position (starting at 1, and
numbered from 1 to the handle's slice dimension)
\begin{itemize}
\item in which AIMMS will store elements of the corresponding dimension in a
{\tt tuple} returned by the functions {\tt Aimms\-ValueNext} and {\tt Aimms\-
ValueNextMulti}, or
\item in which AIMMS expects such elements in calls to the functions {\tt
AimmsValueSearch} and {\tt AimmsValueRetrieve}.
\end{itemize}
\end{itemize}

\paragraph{Getting ordered, special, raw and read-only flags}
\typindex{API function}{AimmsAttributeFlagsSet} \typindex{API
function}{AimmsAttributeFlagsGet} \typindex{API function}{AimmsAttributeFlags}

By specifying the input-output type and the {\tt ordered}, {\tt
retainspecials}, {\tt elements\-asordinals} or {\tt raw} translation modifiers
for arguments in an external call (see also
Section~\ref{sec:extern.declaration}), you can influence the manner in which
data is passed to an external function. With the AIMMS API function {\tt
AimmsAttributeFlagsGet} you obtain the active set of {\tt flags} indicating
whether
\begin{itemize}
\item the data associated with a handle is passed ordered ({\tt ordered} flag),
\item special values are passed unchanged or are translated ({\tt retainspecials} flag),
\item element tuples are passed by their element numbers ({\tt elementsasordinals} flag),
\item inactive data is passed ({\tt raw} flag), and
\item you can make assignments to the handle (input-output type).
\end{itemize}
The result is the {\em bitwise or} function of the individual flag values as
defined in the {\tt aimmsapi.h} header file.

\paragraph{Setting flags}

Through the function {\tt AimmsAttributeFlagSet} you can modify the flag
settings for an existing handle. Note that the result of calls to {\tt
AimmsValueNext} may become unpredictable after modifying the {\tt ordered}
flag. In such a case, you are advised to reset the handle through the function
{\tt AimmsHandleReset}.

\paragraph{Element range}
\typindex{API function}{AimmsAttributeElementRange}

When a handle is associated with an element parameter within your application,
you can use the function {\tt AimmsAttributeElementRange} to obtain a handle to
the set constituting the element range of the element parameter. You need this
handle, for instance, when you want to obtain a string representation of the
element numbers within the element range communicated by AIMMS in the
AIMMS API functions discussed Section~\ref{sec:api.value}.


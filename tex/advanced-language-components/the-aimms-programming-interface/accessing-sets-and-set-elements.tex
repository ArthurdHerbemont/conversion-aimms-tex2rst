\section{Accessing sets and set elements}\label{sec:api.set}

\paragraph{Accessing sets}
\index{application programming interface!accessing sets}

The AIMMS API functions discussed in the previous section allow you to
retrieve and assign individual values of (slices of) indexed identifiers
associated with tuples of set element numbers used by AIMMS internally. The
AIMMS API functions discussed in this section allow you to add elements to
simple sets, and let you convert element numbers into ordinal
numbers and element names, or vice versa. Table~\ref{table:api.set} presents
all set related API functions.

\begin{aimmstable}
\typindex{API function}{AimmsSetAddElement} \typindex{API function}{AimmsSetAddElementMulti} \typindex{API
function}{AimmsSetAddElementRecursive} \typindex{API
function}{AimmsSetAddElementRecursiveMulti} \typindex{API
function}{AimmsSetRenameElement} \typindex{API function}{AimmsSetDeleteElement}
\typindex{API function}{AimmsSetElementToOrdinal} \typindex{API
function}{AimmsSetElementToName} \typindex{API
function}{AimmsSetOrdinalToElement} \typindex{API
function}{AimmsSetOrdinalToName} \typindex{API function}{AimmsSetNameToElement}
\typindex{API function}{AimmsSetNameToOrdinal} \size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsSetAddElement(int handle, char *name, int *element)|\\
\verb|int AimmsSetAddElementMulti(int handle, int n, int *elementNumbers)|\\
\verb|int AimmsSetAddElementRecursive(int handle, char *name, int *element)|\\
\verb|int AimmsSetRenameElement(int handle, int element, char *name)|\\
\verb|int AimmsSetDeleteElement(int handle, int element)|\\
\hline
\verb|int AimmsSetElementNumber(int handle, char *name, int allowCreate,|\\
\verb|                          int *elementNumber, int *isCreated)|\\
\verb|int AimmsSetAddElementMulti(int handle, int n, int *elementNumbers)|\\
\verb|int AimmsSetAddElementRecursiveMulti(int handle, int n, int *elementNumbers)|\\
\hline
\verb|int AimmsSetElementToOrdinal(int handle, int element, int *ordinal)|\\
\verb|int AimmsSetElementToName(int handle, int element, AimmsString *name)|\\
\verb|int AimmsSetOrdinalToElement(int handle, int ordinal, int *element)|\\
\verb|int AimmsSetOrdinalToName(int handle, int ordinal, AimmsString *name)|\\
\verb|int AimmsSetNameToElement(int handle, char *name, int *element)|\\
\verb|int AimmsSetNameToOrdinal(int handle, char *name, int *ordinal)|\\
\hline\hline
\end{tabular}
\caption{AIMMS API functions for passing set data}\label{table:api.set}
\end{aimmstable}

\paragraph{Adding elements to simple sets}
\typindex{API function}{AimmsSetAddElement} 

The function {\tt AimmsSetAddElement} allows you to add new element names to a
simple set. AIMMS will return with the internal element number assigned to
the element, which you can use for further references to the element. The
function fails if an element with the specified name already exists, but still
sets {\tt element} to the corresponding element number. 

\paragraph{Adding element to subsets}
\typindex{API function}{AimmsSetAddElementRecursive}

If the set is a subset, AIMMS will add the element to that subset only.
Thus, the function will fail and return no element number if the corresponding
element does not already exist in the associated root set. If the element is
present in the root set, but not in the domain of the subset, the functions
will fail but still return the element number corresponding to the presented
string. With the function {\tt AimmsSetAddElementRecursive} you can add an
element to a subset itself as well as to all its supersets, up to the
associated root set.

\paragraph{Renaming set elements}
\typindex{API function}{AimmsSetRenameElement}

Through the function {\tt AimmsSetRenameElement} you can provide a new name for
an element number associated with an existing element in a set. The change in
name does not imply any change in the data previously defined over the element.
However, the element will be displayed according to its new name in the
graphical user interface, or in data exchange with external data sources.

\paragraph{Deleting set elements}
\typindex{API function}{AimmsSetDeleteElement}

With the function {\tt AimmsSetDeleteElement} you can delete the element with
the given element number from a simple set. If the set is a 
{\em root} set, any remaining data defined over the element
in subsets parameters and variables will become inactive. To remove such
inactive references to the deleted element, you can use the API function {\tt
Aimms\-IdentifierCleanup} (see also Section~\ref{sec:api.identifier}).

\paragraph{Modifying subset contents}
\typindex{API function}{AimmsValueAssign}

Alternatively to applying the functions {\tt AimmsSetAddElement} and {\tt
AimmsSetDe\-leteElement} to subsets, you can also use the function {\tt
AimmsValueAssign} to modify the contents of a subset. In that case, you should
assign the value 1 to the tuple that should be added to the subset, or 0 to a
tuple that should be removed (as discussed in the previous section). The
function {\tt AimmsValueAssign} will also work on indexed sets and relations.

\paragraph{Adding multiple set elements to a simple set\dots}\typindex{API function}{AimmsSetAddElementMulti}
\typindex{API function}{AimmsSetAddElementRecursiveMulti}\typindex{API function}{AimmsSetElementNumber}

When, as part of a large data transfer from an external data source to AIMMS, you have to
add a large amount of (non-existing) set elements to a simple AIMMS set, the use of the functions
{\tt AimmsSetElement} and {\tt AimmsSetElement\-Recur\-sive} may become a performance bottleneck compared
to any bulk data transfer of multidimensional data defined over these set elements. The reason for this is that
the function {\tt AimmsSetElementAdd} adds elements one at a time, and may need to extend the internal data
structures used to store set data many times, which is a relatively expensive action.

\paragraph{\dots\ in an efficient manner}

As an alternative, AIMMS offers a different set of functions that combined allow you to add
multiple set elements much more efficiently, at the expense of a slightly more complex sequence of
actions. The functions are:
\begin{itemize}
\item the function {\tt AimmsSetElementNumber}, which retrieves an existing, or creates a new, set element number
for a given element name, {\em but does not add it yet to any set}, and
\item the functions {\tt AimmsSetAddElementMulti} and {\tt AimmsSetAddElementRecur\-sive\-Multi}, which
add multiple elements  to a set or a hierarchy of sets simultaneously by passing an array
of set element numbers created through the function {\tt AimmsSetElementNumber}.
\end{itemize}

\paragraph{Set element representations}
\typindex{API function}{AimmsSetElementToOrdinal} \typindex{API
function}{AimmsSetElementToName} \typindex{API
function}{AimmsSetOrdinalToElement} \typindex{API
function}{AimmsSetOrdinalToName} \typindex{API function}{AimmsSetNameToElement}
\typindex{API function}{AimmsSetNameToOrdinal}

The functions
\begin{itemize}
\item {\tt AimmsSetElementToOrdinal},
\item {\tt AimmsSetElementToName},
\item {\tt AimmsSetOrdinalToElement},
\item {\tt AimmsSetOrdinalToName},
\item {\tt AimmsSetNameToElement}, and
\item {\tt AimmsSetNameToOrdinal}
\end{itemize}
allow you to convert AIMMS' element numbers into ordinal numbers within a
particular subset, and element names and vice versa. The functions will fail
when the input representation does not correspond to an existing element.

\paragraph{Ordinal numbers may change}

In working with ordinal numbers, you should be aware that ordinal numbers are
not invariant under changes to a set. When an element is added to or deleted
from a set, or when the ordering of the set has changed, the ordinal numbers of
some or all of its elements may have changed. In contrast, the element numbers
and names of elements remain constant as long as the case used by the AIMMS
model has not changed, or when the {\tt CLEANDEPENDENTS} operator has not been
applied to one or more root sets. You can verify the latter condition with a
call to the function {\tt AimmsIdentifierDataVersion} (see also
Section~\ref{sec:api.identifier}).


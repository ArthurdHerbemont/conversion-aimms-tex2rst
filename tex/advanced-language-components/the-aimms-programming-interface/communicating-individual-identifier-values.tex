\section{Communicating individual identifier values}\label{sec:api.value}

\paragraph{Communicating identifier values}
\index{application programming interface!communicating values}

With every identifier handle AIMMS lets you retrieve all associated
nondefault data values on an element-by-element basis. In addition, AIMMS
lets you search whether a nondefault value exists for a particular element
tuple, and make assignments to individual element tuples.
Table~\ref{table:api.value} lists all the available AIMMS API functions for
this purpose.

\begin{aimmstable}
\typindex{API function}{AimmsValueCard} \typindex{API
function}{AimmsValueResetHandle} \typindex{API function}{AimmsValueSearch}
\typindex{API function}{AimmsValueNext} \typindex{API
function}{AimmsValueNextMulti} \typindex{API function}{AimmsValueRetrieve}
\typindex{API function}{AimmsValueAssign} \typindex{API
function}{AimmsValueAssignMulti} \typindex{API
function}{AimmsValueDoubleToMapval} \typindex{API
function}{AimmsValueMapvalToDouble} \size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsValueCard(int handle, int *card)|\\
\hline
\verb|int AimmsValueResetHandle(int handle)|\\
\verb|int AimmsValueSearch(int handle, int *tuple, AimmsValue *value)|\\
\verb|int AimmsValueNext(int handle, int *tuple, AimmsValue *value)|\\
\verb|int AimmsValueNextMulti(int handle, int *n, int *tuples, AimmsValue *values)|\\
\hline
\verb|int AimmsValueRetrieve(int handle, int *tuple, AimmsValue *value)|\\
\verb|int AimmsValueAssign(int handle, int *tuple, AimmsValue *value)|\\
\verb|int AimmsValueAssignMulti(int handle, int n, int *tuples, AimmsValue *values)|\\
\hline
\verb|int AimmsValueDoubleToMapval(double value, int *mapval)|\\
\verb|int AimmsValueMapvalToDouble(int mapval, double *value)|\\
\hline\hline
\end{tabular}
\caption{AIMMS API functions for sparse data
communication}\label{table:api.value}
\end{aimmstable}

\paragraph{Cardinality}
\typindex{API function}{AimmsValueCard}

The function {\tt AimmsValueCard} returns the cardinality of a handle, i.e.\
the number of nondefault elements of the associated identifier slice. You can
call this function, for instance, when you need to allocate memory for the data
structures in your own code before actually retrieving the data.

\paragraph{Retrieving nondefault values}
\typindex{API function}{AimmsValueResetHandle} \typindex{API
function}{AimmsValueSearch} \typindex{API function}{AimmsValueNext}

The functions {\tt AimmsValueResetHandle}, {\tt AimmsValueSearch} and {\tt
AimmsValueNext}  retrieve nondefault values associated with a handle on an
element-by-element basis.
\begin{itemize}
\item The function {\tt AimmsValueResetHandle} resets the handle to
the position just before the first nondefault element.
\item The function {\tt AimmsValueSearch} expects an input tuple of
element numbers (in the slice domain), and returns the first tuple for which a
nondefault value exists on or following the input tuple.
\item The function {\tt AimmsValueNext} returns the first
nondefault element directly following the element returned by the last call to
{\tt AimmsValueNext} or {\tt Aimms\-ValueSearch}, or the first element if the
function {\tt AimmsValueResetHandle} was called last. The function fails when
there is no such element.
\end{itemize}
By calling {\tt AimmsValueResetHandle} and subsequently {\tt AimmsValueNext} it
is possible to retrieve {\em all} nondefault values. By calling the function
{\tt AimmsValueSearch} you can directly skip to a particular element tuple if
you have found that the intermediate tuples are not interesting anymore, and
continue from there.

\paragraph{No scalar handles}

The functions {\tt AimmsValueResetHandle}, {\tt AimmsValueNext} and {\tt
AimmsValueSearch} do not accept handles to scalar (i.e.\ 0-dimensional)
identifier slices. To retrieve and assign scalar values you should use the
functions {\tt AimmsValueRetrieve} and {\tt AimmsValueAssign} explained below.

\paragraph{Unordered versus ordered retrieval}

The particular element returned by the functions {\tt AimmsValueSearch} and
{\tt Aimms\-ValueNext} may differ depending on the setting of the {\tt ordered}
flag for the handle. If the handle has been created unordered (default), the
values returned successively are ordered by increasing element number in a
right-to- left tuple order. If the handle has been created ordered, AIMMS
will return values in accordance with the ordering principles imposed on all
{\em local} tuple domains.

\paragraph{Raw data retrieval}

By default, AIMMS will only pass values for element tuples that lie within
the {\em current} contents of the intersection of the call domain and
declaration domain of an identifier. Thus, the values that get passed may
depend on a dynamically changing domain restriction that is part of the index
domain in the declaration of an identifier. When the {\tt raw} modification
flag is set for a handle, AIMMS will pass {\em all} available data values in
the call domain, regardless of the domain restrictions.

\paragraph{Return tuple and value}

All data retrieval functions return a {\tt tuple} and the associated nondefault
{\tt value}. The interpretation of the {\tt value} argument for all possible
storage types was discussed on page~\pageref{expl:extern.passing-values}. The
{\tt tuple} argument must be an integer array of length equal to the {\em
slice} dimension of the handle. Upon success, the tuple contains the element
numbers in the {\em global} domain sets for every non-sliced dimension.

\paragraph{Element or ordinal numbers}

By setting the flag {\tt elementsasordinals} during the creation of a handle,
you can modify the default tuple representation. If this flag is set, the
tuples returned by AIMMS will contain ordinal numbers corresponding to the
respective call domains associated with the handle. Similarly, AIMMS expects
tuples that are passed to it, to contain ordinal numbers as well, when this
flag is set.

\paragraph{Rationale}

While at first sight the choice for representing tuples by their element
numbers in the global domain of a handle may seem less convenient than ordinal
numbers in its call domain, you must be aware that the latter representation is
not invariant under changes in the contents of the call domain. Alternatively
to setting the flag {\tt elementsasordinals}, you can also convert the returned
element numbers into these formats using the AIMMS API functions discussed
in Section~\ref{sec:api.set}.

\paragraph{Value types}

The expected storage type of the data values returned by the data retrieve
functions can be obtained using the function {\tt AimmsAttributeStorage}. The
possible storage types for the various identifier types are listed below:
\begin{itemize}
\item numeric parameters and variables return double or integer values,
\item all set types return binary values,
\item element parameters return integer element numbers, and
\item string and unit parameters return string values.
\end{itemize}

\paragraph{Element parameter values}
\typindex{API function}{AimmsAttributeElementRange}

The element numbers returned for element parameters are relative to the set
handle returned by the function {\tt AimmsAttributeElementRange}. You can use
the AIMMS API functions of Section~\ref{sec:api.set} to obtain the
associated ordinal numbers or string representations.

\paragraph{Set values}

For sets (either simple, relation or indexed), the data retrieval
functions return the binary value 1 for just those elements (or element tuples)
that are contained in the set. For indexed
sets, AIMMS returns tuples for which the last component is the element number 
of an element contained in the set slice associated
with all but the last tuple components.

\paragraph{Converting special numbers}
\typindex{API function}{AimmsValueDoubleToMapval} \typindex{API
function}{AimmsValueMapvalToDouble}

When a handle to a numeric parameter or variable has been created with the {\tt
special} flag set, the data retrieval functions will pass any special number
value associated with the handle as is (see also
Sections~\ref{sec:extern.declaration} and~\ref{sec:api.attribute}). AIMMS
represents special numbers as double precision floating point numbers outside
AIMMS' ordinary range of computation. The function {\tt
AimmsValueDoubleToMapval} returns the {\tt MapVal} value associated with any
double value (see also Table~\ref{table:expr.arith-ext}), while the function
{\tt AimmsValueMapvalToDouble} returns the double representation associated
with any type of special number.

\paragraph{Retrieving specific values}
\typindex{API function}{AimmsValueRetrieve}

The function {\tt AimmsValueRetrieve} returns the value for a specific element
tuple in the slice domain. This value can be either the default value or a
nondefault value. The tuple must consist of element numbers in the
corresponding domain sets. When the {\tt raw} flag is not set, the function
fails (but still returns the default value of the associated identifier) for
any tuple outside of the index domain of the handle. When the {\tt raw} flag is
set, the function fails only when there is no data for the tuple.

\paragraph{Assigning values}
\typindex{API function}{AimmsValueAssign}

The function {\tt AimmsValueAssign} lets you assign a new value to a particular
element tuple in the slice domain. If you want to assign the default value you
can either pass a null pointer for {\tt value}, or a pointer to the appropriate
default value. The function fails if you try to assign a value to an element
tuple outside the contents of the call domain of the handle. When the {\tt raw}
flag is not set, the function will also fail if the assigned tuple lies outside
of the current (active) contents of the declaration domain.

\paragraph{Exchanging multiple values}
\typindex{API function}{AimmsValueNextMulti} \typindex{API
function}{AimmsValueAssignMulti}

When a particular identifier handle requires the exchange of a large amount of
values, you are strongly encouraged to use the functions {\tt AimmsValueNextMulti} and
{\tt AimmsValueAssignMulti} instead of the functions {\tt AimmsValueNext} and
{\tt Aimms\-Value\-Assign}. In general, AIMMS can perform the simultaneous
exchange of multiple values much more efficient than the equivalent sequence of
single exchanges. For both functions, the {\tt tuples} array must be an integer
array of length {\tt n} times the {\em slice} dimension of the handle, while
the {\tt values} array must be the corresponding {\tt AimmsValue} array of
length {\tt n}.
\begin{itemize}
\item In the function {\tt AimmsValueNextMulti}, AIMMS will fill the {\tt
tuples} array with the respective tuples for which nondefault values are
returned in the {\tt values} array. Upon return, the {\tt n} argument will
contain the actual number of values passed.
\item In the function {\tt AimmsValueAssignMulti}, the {\tt tuples} array must
be filled sequentially with the respective tuples to which the assignments take
place via the {\tt values} array.
\end{itemize}
When your data transfer involves the addition of a large amount of set elements to an
AIMMS set as well, you may also want to consider using the function
{\tt AimmsSetAddElementMulti} (see Section~\ref{sec:api.set}).

\paragraph{Communicating scalar values}

When a handle corresponds to a 0-dimensional (i.e.\ scalar) identifier slice,
you can still use the {\tt AimmsValueRetrieve} and {\tt AimmsValueAssign} to
retrieve its value or assign a value to it. In this case, the {\tt tuple}
argument is ignored.

\paragraph{Assigning set values}
\typindex{API function}{AimmsSetAddElement} 

When you want to delete or add an existing element or element tuple to a set,
you must assign the value 0 or 1 to the associated tuple respectively. If you
want to add a tuple of nonexisting simple elements, you must first add these
elements to the corresponding global simple domain sets using the function {\tt
AimmsSetAddElement} discussed below. 


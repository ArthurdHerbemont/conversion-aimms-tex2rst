\section{Raising and handling errors}\label{sec:api.error.handling}
\index{application programming interface!Raising and handling errors}

\paragraph{Raising and handling errors}

The error passing described in the previous section is retained in AIMMS 3 in order not to break existing applications.
The use of the error handling described in this section, however, is encouraged 
as it is more in line with the error handling framework described in Section~\ref{sec:exec.error}.
In addition, all errors, including all their parts, can be retrieved.
The AIMMS API functions in Table~\ref{table:api.handle.error} enable the raising 
and handling of errors and retrieving the current AIMMS status. 

\begin{aimmstable}
\typindex{API function}{AimmsErrorCount}
\size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsErrorStatus(void)|\\
\verb|int AimmsErrorCount(void)|\\
\hline
\verb|char *AimmsErrorMessage(int errNo)|\\
\verb|int AimmsErrorSeverity(int errNo)|\\
\verb|char *AimmsErrorCode(int errNo)|\\
\verb|char *AimmsErrorCategory(int errNo)|\\
\verb|int AimmsErrorNumberOfLocations(int errNo)|\\
\verb|char *AimmsErrorFilename(int errNo)|\\
\verb|char *AimmsErrorNode(int errNo, int pos)|\\
\verb|char *AimmsErrorAttributeName(int errNo, int pos)|\\
\verb|int AimmsErrorLine(int errNo, int pos)|\\
\verb|int AimmsErrorColumn(int errNo)|\\
\verb|time_t AimmsErrorCreationTime(int errNo)|\\
\hline
\verb|int AimmsErrorDelete(int errNo)|\\
\verb|int AimmsErrorClear(void)|\\
\hline
\verb|int AimmsErrorRaise(int severity, char *message, char *code)|\\
\hline\hline
\end{tabular}
\caption{AIMMS Raising and handling errors in the AIMMS API}\label{table:api.handle.error}
\end{aimmstable}

\paragraph{Global error collector manipulation}
The functions {\tt AimmsErrorStatus}, {\tt AimmsErrorCount}, {\tt AimmsErrorGet}, {\tt AimmsErrorDe\-lete} and {\tt AimmsErrorClear} 
all manipulate the global error collector.  The global error collector is described in Section~\ref{sec:exec.error}. 
The function {\tt AimmsErrorStatus} scans the contents of the global error collector and returns 
\begin{description}
\item[{\tt AIMMSAPI\_SEVERITY\_CODE\_NEVER}] if the global error collector is empty,
\item[{\tt AIMMSAPI\_SEVERITY\_CODE\_WARNING}] if it contains only warnings, or
\item[{\tt AIMMSAPI\_SEVERITY\_CODE\_ERROR}] if it contains at least one error.
\end{description}
The function {\tt AimmsErrorCount} does not 
return a status code, instead it directly returns the number of errors and warnings in the global error collector. 
With the functions {\tt AimmsErrorMessage},  {\tt AimmsErrorSeverity}, {\tt AimmsErrorCategory}, {\tt Aimms\-ErrorCode}, 
{\tt AimmsErrorNumberOfLocations}, {\tt AimmsErrorLine}, {\tt AimmsErrorNode}, {\tt Aimms\-ErrorAttributeName}, 
{\tt AimmsErrorFilename}, {\tt AimmsErrorColumn}, and {\tt AimmsErr\-or\-CreationTime} actual error information is obtained.
In these functions the {\tt errNo} argument should be in the range \{1..{\tt AimmsErrorCount()}\} and 
the {\tt pos} argument should be in the range \{1..{\tt AimmsErrorNumberOfLocations(errNo)}\}.

\paragraph{Example for API calls}
None of the AIMMS API functions throws an exception, nor do any of them let an exception pass through.
The example below serves as a simple template to call {\tt AimmsProcedureRun} and handle all errors occurring during that 
execution run.
\begin{example}
int ErrCount, errNo, apr_stat ;

apr_stat = AimmsProcedureRun( procHandle, ... );
ErrCount = AimmsErrorCount();
if ( ErrCount ) {
    for ( errNo = 1 ; errNo <= ErrCount ; errNo ++ ) {
    
        // Handle the error; replace the next line as 
        // appropriate for the application at hand.
        printf( "Error %d: %s\n", errNo, AimmsErrorMessage(errNo) );
 
    }
    AimmsErrorClear();
} else if ( apr_stat == AIMMSAPI_FAILURE ) {
    printf("Aimms failed for an unknown reason.\n");
}
\end{example}

\paragraph{Raising an error} 

The function {\tt AimmsErrorRaise(severity, message, code)} can raise errors and warnings. These errors will be 
handled by the currently active error handler as described in Section~\ref{sec:exec.error}.
If there is no currently active error handler, the error is directly placed in the global error collector.
The call to this function is similar to the {\tt RAISE} statement, see Section~\ref{sec:exec.error.raising}.
The {\tt code} argument is optional. The {\tt severity} argument should be either 
\begin{description}
\item[{\tt AIMMSAPI\_SEVERITY\_CODE\_WARNING}] indicating a warning or 
\item[{\tt AIMMSAPI\_SEVERITY\_CODE\_ERROR}] indicating an error.
\end{description}
The category of an error raised by {\tt AimmsErrorRaise} is fixed to {\tt 'User'}.


\section{Executing AIMMS procedures}\label{sec:api.procedure}

\paragraph{Running AIMMS procedures}
\index{application programming interface!running procedures}

The AIMMS API allows you to execute procedures contained in the AIMMS
model from within an external application. Both procedures with and without
arguments can be executed, and scalar output results can be directly passed
back to the external application. Table~\ref{table:api.execution} lists the
AIMMS API functions offered to obtain procedure handles, to execute AIMMS
procedures or to schedule AIMMS procedures for later execution.

\begin{aimmstable}
\typindex{API function}{AimmsProcedureHandleCreate} 
\typindex{API function}{AimmsProcedureHandleDelete} 
\typindex{API function}{AimmsProcedureRun} 
\typindex{API function}{AimmsProcedureArgumentHandleCreate} 
\typindex{API function}{AimmsProcedureAsyncRunCreate} 
\typindex{API function}{AimmsProcedureAsyncRunDelete} 
\typindex{API function}{AimmsProcedureAsyncRunStatus} 
\typindex{API function}{AimmsExecutionInterrupt} 
\size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsProcedureHandleCreate(char *procedure, int *handle, int *nargs, int *argtype)|\\
\verb|int AimmsProcedureHandleDelete(int handle)|\\
\verb|int AimmsProcedureRun(int handle, int *argtype, AimmsValue *arglist, int *result)|\\
\hline
\verb|int AimmsProcedureArgumentHandleCreate(int prochandle, int argnumber, int *arghandle)|\\
\hline
\verb|int AimmsProcedureAsyncRunCreate(int handle, int *argtype, AimmsValue *arglist, int *request)|\\
\verb|int AimmsProcedureAsyncRunDelete(int request)|\\
\verb|int AimmsProcedureAsyncRunStatus(int request, int *status, int *result)|\\
\hline
\verb|int AimmsExecutionInterrupt(void)|\\
\hline\hline
\end{tabular}
\caption{AIMMS API functions for execution
requests}\label{table:api.execution}
\end{aimmstable}

\paragraph{Obtaining procedure handles}
\typindex{API function}{AimmsProcedureHandleCreate} \typindex{API
function}{AimmsProcedureHandleDelete}

With the function {\tt AimmsProcedureHandleCreate} you can obtain a handle to a
procedure with the given name within the model. In addition, AIMMS will
return the number of arguments of the procedure, as well as the type of each
argument. The possible argument types are:
\begin{itemize}
\item one of the storage types {\em double}, {\em integer}, {\em
binary} or {\em string} (discussed in Section~\ref{sec:api.attribute}) for
scalar formal arguments, or
\item a {\em handle} for non-scalar formal arguments.
\end{itemize}
In addition to indicating the storage type of each argument, the {\tt argtype}
argument will also indicate whether an argument is input, output, or input-
output. Through the function {\tt AimmsProcedureHandleDelete} you can delete
procedure handles created with {\tt AimmsProcedureHandleCreate}.

\paragraph{Calling procedures}
\typindex{API function}{AimmsProcedureRun}

You can use the function {\tt AimmsProcedureRun} to run the AIMMS procedure
associated with a given handle. If the AIMMS procedure has arguments, then
you have to provide these, together with their types, through the {\tt arglist}
and {\tt argtype} arguments. The (integer) return value of the procedure (see
also pages~\pageref{proc.ret-val.decl} and~\pageref{proc.ret-val.use}) is
returned through the {\tt result} argument. If AIMMS is already executing
another procedure (started by another thread), the call to {\tt
AimmsProcedureRun} blocks until the other execution request has finished.
Section~\ref{sec:api.control} explains how to prevent this blocking behavior by
obtaining exclusive control over AIMMS.

\paragraph{Passing arguments}

For each argument of the AIMMS procedure you have to provide both the type
and value through the {\tt argtype} and {\tt arglist} arguments in the call to
{\tt AimmsProce\-dureRun}. You have the following possibilities.
\begin{itemize}
\item If the argument is scalar, the argument type can be
\begin{itemize}
\item the storage type returned by the function {\tt
AimmsProcedureHandle\-Create}, in which case the argument value must be a
pointer to a buffer of the indicated type containing the argument, or
\item a handle, in which case the argument value must be a handle
associated with a scalar AIMMS identifier (slice) that you want to pass.
\end{itemize}
\item If the argument is non-scalar, the argument type can only be a
handle, and the argument value must be a handle corresponding to the identifier
(slice) that you want to pass.
\end{itemize}
If you pass an argument as an identifier handle, this can either be a handle to
a global identifier defined within the model, or a local argument handle
obtained through a call to the function {\tt
AimmsProcedureArgumentHandleCreate} (see below).

\paragraph{Output values}

When the input-output type of one or more of the arguments is {\tt inout} or
{\tt output}, AIMMS will update the values associated with any handle
argument, or, if a buffer containing a scalar value was passed, fill the buffer
with the new value of the argument.

\paragraph{Obtaining argument handles}
\typindex{API function}{AimmsProcedureArgumentHandleCreate}

Through the function {\tt AimmsProcedureArgumentHandleCreate} you can obtain a
handle to the local arguments of procedures within your model. After creating
these handles you can pass them as arguments to the function {\tt
AimmsProcedure\-Run}. The following rules apply.
\begin{itemize}
\item After creation, handles created by {\tt AimmsProcedureArgumentHandleCreate} have no associated data.
\item If the handle corresponds to an {\tt Input} argument of the procedure, you can supply data prior to calling the procedure, and AIMMS will empty the handle after the execution of the procedure has completed.
\item If the handle corresponds to an {\tt InOut} or {\tt Output} argument of the procedure, AIMMS will not empty the handle after completion of the procedure. If you want to supply data to a handle corresponding to an {\tt InOut} argument in subsequent calls, you have to make sure to empty the handle (through the function {\tt AimmsIdentifierEmpty}) prior to supplying the input data.
\end{itemize}


\paragraph{Requesting asynchronous execution}
\typindex{API function}{AimmsProcedureAsyncRunCreate}

With the function {\tt AimmsProcedureAsyncRunCreate} you can request
asynchronous execution of a particular AIMMS procedure. The function returns
an integer request handle for further reference. AIMMS will execute a
requested procedure as soon as there are no other execution requests currently
being executed or waiting to be executed. {\em Note that you should make sure
that the {\tt AimmsValue} array passed to AIMMS stays alive during the
asynchronous execution of the procedure.} Failure to do so, may result in
illegal memory references during the actual execution of the AIMMS
procedure. This is especially true when the array contains references to scalar
integer, double or string {\tt InOut} or {\tt Output} buffers within your
application to be filled by the AIMMS procedure.

\paragraph{Obtaining the status}
\typindex{API function}{AimmsProcedureAsyncRunStatus}

Through the function {\tt AimmsProcedureAsyncRunStatus} you can obtain the
status of an outstanding asynchronous execution request. The status of such a
request can be
\begin{itemize}
\item pending,
\item running,
\item finished,
\item deleted, or
\item unknown (for an invalid request handle).
\end{itemize}
When the request is in the finished state, the return value of the AIMMS
procedure will be returned via the {\tt result} argument.

\paragraph{Deleting a request}
\typindex{API function}{AimmsProcedureAsyncRunDelete}

You should make sure to delete all asynchronous execution handles requested
during a session using the function {\tt AimmsProcedureAsyncRunDelete}. {\em
Failure to delete all finished requests may result in a serious memory leak if
your external DLL generates many small asynchronous execution requests.} If you
delete a pending request, AIMMS will remove the request from the current
execution queue. The function will fail if you try to delete a request that is
currently being executed.

\paragraph{Interrupting an existing run}

When an AIMMS procedure has been started by a separate thread in your program
you can interrupt it using the function {\tt AimmsExecutionInterrupt}. This function
returns {\tt AIMMSAPI\_SUCCESS} when AIMMS was idle and {\tt AIMMSAPI\_FAILURE} was
executing a procedure.


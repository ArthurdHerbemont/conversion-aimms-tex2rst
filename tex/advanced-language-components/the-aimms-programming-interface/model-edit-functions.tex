\section{Model Edit Functions}\label{sec:api.model.editing}

\paragraph{AIMMS Model Edit Functions}
The AIMMS API supports Model Edit Functions allowing external
applications to inspect, modify, or even construct AIMMS models.
In this section, the model edit functions are introduced using a small example. 
Subsequently, after briefly describing the relation to runtime libraries plus the conventions used, 
several tables containing model edit functions, are presented and described.
Finally, the limitations of using model edit functions through the AIMMS API are described briefly.

\paragraph{Small example}
In the following example, an element parameter {\tt nextCity} is created with a simple definition.
Model editing is done using \textbf{model editor} handles. These handles provide access
to the identifiers in the model, and should not be confused with the data handles and procedure handles
described elsewhere in this chapter.

The model editor handle {\tt int dsMEH} refers to a declaration section, whereas
the model editor handle {\tt int ncMEH} refers to the parameter {\tt nextCity}.
\begin{example}
1  AimmsMeCreateNode("nextCity", AIMMSAPI_ME_IDTYPE_ELEMENT_PARAMETER, dsMEH, 0, &ncMEH);
2  AimmsMeSetAttribute(ncMEH, AIMMSAPI_ME_ATTR_INDEX_DOMAIN, "i");
3  AimmsMeSetAttribute(ncMEH, AIMMSAPI_ME_ATTR_RANGE, "Cities");
4  AimmsMeSetAttribute(ncMEH, AIMMSAPI_ME_ATTR_DEFINITION,
                "if i == last(Cities) then first(Cities) "
                "else Element(Cities,ord(i)+1) endif");
5  AimmsMeCompile(ncMEH);
6  AimmsMeCloseNode(ncMEH);
\end{example}
A line by line explanation of this example follows below. 
For the sake of brevity, error handling, such as suggested 
in Section~\ref{sec:api.error.handling}, is omitted here.
\begin{description}
\item[Line 1] Creates an element parameter named {\tt nextCity}. 
              The fourth argument of {\tt AimmsMeCreateNode} is the position within the section. 
              A 0 indicates that it should be placed at the end of the section.
\item[Lines 2-4] Set the attributes {\tt IndexDomain}, {\tt Range}, and {\tt Definition} of this parameter.
              Note that only the text is passed, these calls do not use the AIMMS compiler to compile them.
\item[Line 5] Compiles the element parameter {\tt nextCity}. Only now is the text of the attributes actually checked and compiled.
\item[Line 6] The function {\tt AimmsMeCloseHandle} de-allocates the handle {\tt ncMEH} but the created identifier {\tt nextCity}
              remains in the model.
\end{description}

\paragraph{Relation to runtime libraries}
Section~\ref{sec:module.runtime} describes the model editing facility available in 
the AIMMS language using runtime libraries.  The advantage of using the AIMMS API, 
instead of runtime libraries, for model editing is that the entire model can be edited, 
including the main model, provided that there is no AIMMS procedure active 
while executing a model edit function from within the AIMMS API. The cost is that multiple 
languages have to be used.

\paragraph{Conventions for model edit functions}

The model edit functions follow the following conventions:
\begin{itemize}
\item Each function starts with {\tt AimmsMe}.
\item These functions return either {\tt AIMMSAPI\_SUCCESS} or {\tt AIMMSAPI\_FAILURE}.
\item A model editor handle {\tt MEH} has to be closed by either 
      {\tt AimmsMeCloseNode} or {\tt AimmsMeDestroyNode}. 
\item No distinction is made between identifiers and nodes in the model editor tree, they are both called "nodes".     
\item String output arguments use the type {\tt AimmsString} as is explained in Section~\ref{expl:extern.passing-values}. 
\end{itemize}

\paragraph{Model roots}
Table~\ref{table:api.model.edit.roots} lists the functions available for manipulating model editor roots.
The number of roots available for model editing is stored by the function {\tt AimmsMeRootCount(count)} in 
its output argument {\tt count}. Note that {\tt count} is always at least 1, since there is always the main model.
The root {\tt Predeclared identifiers} is not included in this count.
As the root {\tt Predeclared identifiers} and its sub-nodes cannot be changed, it is not included in this count.
To obtain a handle for an existing root, the function {\tt int AimmsMeOpenRoot(pos, MEH)} can be used. 
A model editor handle is then created and stored in {\tt MEH}.
If {\tt pos} is {\tt 0}, the main model root is opened.
If {\tt pos} is in the range {\tt \{ 1 .. count-1 \}} then a library is opened.
If {\tt pos} equals {\tt count} then the predeclared root {\tt Predeclared identifiers} is opened. 
The root {\tt Predeclared identifiers} and its sub-nodes are read only. 
In order to create a new runtime library the function {\tt AimmsMeCreateRuntimeLibrary(name, prefix, MEH)} can 
be used. The position of the new library is at the end of the existing libraries.  

\begin{aimmstable}
\typindex{API function}{AimmsMeRootCount}
\typindex{API function}{AimmsMeOpenRoot}
\typindex{API function}{AimmsMeCreateRuntimeLibrary}
\typindex{API function}{AimmsMeNodeExists}
\typindex{API function}{AimmsMeOpenNode}
\typindex{API function}{AimmsMeCreateNode}
\typindex{API function}{AimmsMeCloseNode}
\typindex{API function}{AimmsMeDestroyNode}
\size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsMeRootCount(int *count)|\\
\verb|int AimmsMeOpenRoot(int pos, int *MEH)|\\
\verb|int AimmsMeCreateRuntimeLibrary(char *name, char *prefix, int *MEH)|\\
\hline
\verb|int AimmsMeNodeExists(char*name, int nMEH, int *exists)|\\
\verb|int AimmsMeOpenNode(char*name, int nMEH, int *MEH)|\\
\verb|int AimmsMeCreateNode(char *name, int idtype, int pMEH, int pos, int *MEH)|\\
\hline
\verb|int AimmsMeCloseNode(int MEH)|\\
\verb|int AimmsMeDestroyNode(int MEH)|\\
\hline\hline
\end{tabular}
\caption{Model edit functions for roots and nodes}\label{table:api.model.edit.roots}
\end{aimmstable}

\paragraph{Opening or creating a node}
The function {\tt AimmsMeNodeExists(name, nMEH, exists)} can be used to test if an identifier exists. This function
returns {\tt AIMMSAPI\_FAILURE} when nMEH does not indicate a valid namespace, or when name is not a valid identifier
name.  If the name is a declared identifier in namespace {\tt nMEH}, then {\tt exists} is set to 1, and if not to 0.
The function {\tt AimmsMeOpenNode(name, nMEH, MEH)} creates a handle 
to the node with name {\tt name} in the namespace determined by the model editor handle {\tt nMEH}.
If successful, a model editor handle is created and stored in the output 
argument {\tt MEH}. If {\tt nMEH} equals {\tt AIMMSAPI\_NULL\_HANDLE\_NUMBER}, then the namespace of the main model is used.
A new node with name {\tt name} and type {\tt idtype} can be created using the function 
{\tt AimmsMeCreateNode(name, idtype, pMEH, pos, MEH)}. 
The value of {\tt idtype} must be one of the constants defined in {\tt aimmsapi.h} starting with {\tt AIMMSAPI\_ME\_IDTYPE\_}. 
The parent node of the new node is determined by the model editor handle {\tt pMEH}.
The value {\tt pos} determines the new position of the node within the parent node. If {\tt pos}
is outside the range of existing children \{1..n\}, the new identifier is placed at the end, otherwise the existing
children at positions {\tt pos} .. {\tt n} are shifted to positions {\tt pos+1} .. {\tt n+1} where {\tt n}
was the old number of children of {\tt pMEH}.  

\paragraph{Closing or destroying a node}
Table~\ref{table:api.model.edit.roots} not only lists the functions to open or create nodes, 
but also shows the complementary functions to close or destroy nodes.
The function {\tt AimmsMeCloseNode(MEH)} de-allocates the handle {\tt MEH} but leaves the corresponding node
in the model intact. The function {\tt AimmsMeDestroyNode(MEH)} destroys the node corresponding to {\tt MEH} 
and all nodes below that node in the model, and subsequently deallocates the handle {\tt MEH}.

\paragraph{The name of a node}
Table~\ref{table:api.model.edit.name} lists the functions that return the name of a node.
The function {\tt AimmsMeName(MEH, name)} stores the name of the node 
to which {\tt MEH} refers without any prefixes in the output 
argument {\tt name}. The function {\tt AimmsMe\-RelativeName(MEH, rMEH, rName)} 
stores the name of {\tt MEH} such as it should be used from 
within the node {\tt rMEH} in the output argument {\tt rName}.  
A fully qualified name is stored 
in {\tt rName} when {\tt MEH} is the {\tt AIMMSAPI\_ME\_NULL\_HANDLE\_NUMBER} handle.  

\begin{aimmstable}
\typindex{API function}{AimmsMeName}
\typindex{API function}{AimmsMeRelativeName}
\typindex{API function}{AimmsMeType}
\typindex{API function}{AimmsMeTypeName}
\typindex{API function}{AimmsMeAllowedChildTypes}
\size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsMeName(int MEH, AimmsString *name)|\\
\verb|int AimmsMeRelativeName(int MEH, int rMEH, AimmsString *rName)|\\
\hline
\verb|int AimmsMeType(int MEH, int *meType)|\\
\verb|int AimmsMeTypeName(int typeNo, AimmsString *tName)|\\
\verb|int AimmsMeAllowedChildTypes(int MEH, int *typeBuf, int typeBufsize, int *maxTypes)|\\
\hline\hline
\end{tabular}
\caption{Model edit functions for name and type}\label{table:api.model.edit.name}
\end{aimmstable}

\paragraph{The type of a node}
In addition, Table~\ref{table:api.model.edit.name} lists the functions for the type of a node. 
The function {\tt AimmsMeType(MEH, meType)} stores the type of the node {\tt MEH} in 
the output argument {\tt meType}. The value of {\tt meType} refers to one
of the constants in {\tt aimmsapi.h} starting with {\tt AIMMSAPI\_ME\_IDTYPE\_}.
The function {\tt AimmsMeAllowed\-ChildTypes(MEH, typeBuf, typeBufsize, maxTypes)} stores 
the types of children allowed below the node {\tt MEH} in the buffer {\tt typeBuf} while respecting its 
size {\tt type\-Bufsize}. The maximum number of child types below {\tt MEH} is stored in the 
output argument {\tt maxTypes}.
The utility function {\tt AimmsMeTypeName(typeNo, tName)} stores
the name of the type {\tt typeNo} in the output argument {\tt tName}. 

\begin{aimmstable}
\typindex{API function}{AimmsMeGetAttribute}
\typindex{API function}{AimmsMeSetAttribute}
\typindex{API function}{AimmsMeAttributes}
\typindex{API function}{AimmsMeAttributeName}
\size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsMeGetAttribute(int MEH, int attr, AimmsString *text)|\\
\verb|int AimmsMeSetAttribute(int MEH, int attr, const char *txt)|\\
\verb|int AimmsMeAttributes(int MEH, int attrsBuf[], int attrBufSize, int *maxNoAttrs)|\\
\verb|int AimmsMeAttributeName(int attr, AimmsString *name)|\\
\hline\hline
\end{tabular}
\caption{Model edit functions for attributes}\label{table:api.model.edit.attributes}
\end{aimmstable}

\paragraph{The attributes of a node}
Table~\ref{table:api.model.edit.attributes} lists the functions available for handling the attributes of a node.
All attributes correspond to constants in the {\tt aimmsapi.h} file.  These constants start with {\tt AIMMSAPI\_ME\_ATTR\_}.
The function {\tt AimmsMeGetAttribute(MEH,attr,text)} stores the contents of attribute {\tt attr} of node {\tt MEH}
in the output argument {\tt text}.  The function {\tt AimmsMeSetAttribute(MEH,attr,txt)} sets the contents of attribute
{\tt attr} of node {\tt MEH} to {\tt txt}.  
This function will fail if attribute {\tt attr} is not applicable to identifier {\tt MEH}, but the text itself is not checked for errors.  
The function {\tt AimmsMeAttributes(MEH, attrsBuf, attrBufSize, maxNoAttrs)} provides the applicable attributes for these two functions.
It will store
 the constants corresponding to the attributes available to node {\tt MEH} in {\tt attrBuf} while respecting the size of that buffer {\tt attrBufSize}.
The maximum number of attributes available to node {\tt MEH} is stored in {\tt maxNoAttrs}. The function 
{\tt AimmsMeAttributeName(attr, name)} stores the name of {\tt attr} in {\tt name}.

\begin{aimmstable}
\typindex{API function}{AimmsMeNodeRename}
\typindex{API function}{AimmsMeNodeMove}
\typindex{API function}{AimmsMeNodeChangeType}
\typindex{API function}{AimmsMeNodeAllowedTypes}
\size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsMeNodeRename(int MEH, char *newName)|\\
\verb|int AimmsMeNodeMove(int MEH, int pMEH, int pos)|\\
\verb|int AimmsMeNodeChangeType(int MEH, int newType)|\\
\verb|int AimmsMeNodeAllowedTypes(int MEH, int* typeBuf, int typeBufsize, int *maxNoTypes)|\\
\hline\hline
\end{tabular}
\caption{Model edit functions for node manipulations}\label{table:api.model.node.manipulations}
\end{aimmstable}

\paragraph{Basic node manipulations}
The functions that support changing the aspects of a node such as name, location, 
and type of a node are also shown in Table~\ref{table:api.model.node.manipulations}.
The function {\tt AimmsMeNode\-Rename(MEH, newName)} changes the name of a node, and the 
name\-change is applied to the attribute texts that reference this node.
An entry is appended to the name change file if the node is not a runtime node.
The function {\tt AimmsMeNodeMove(MEH, pMEH, pos)} moves the node {\tt MEH} to child position {\tt pos} of node {\tt pMEH}. 
If this results in a change of namespace, the corresponding namechange is applied to the attributes that reference this node.
In addition, an entry is appended to the corresponding name change file if this node is not a runtime node. 
Moves from one library to another are not supported, nor is a move in or out of the main model.
The function {\tt AimmsMeNodeChangeType(MEH, newType)} changes the type of a node. It will retain
available attributes whenever possible. 
The function {\tt AimmsMeNode\-AllowedTypes} can be used to query which types, if any, a particular node can be changed to.
The function {\tt AimmsMeNode\-AllowedTypes(MEH, typeBuf, typeBufsize, maxNoTypes)} will store all the types into which node {\tt MEH} can
be changed in a buffer {\tt typeBuf} that respects the size {\tt typeBufsize}.  The maximum number of types into which {\tt MEH} can be
changed is stored in {\tt maxNoTypes}.


\paragraph{Tree walk of the model}
Table~\ref{table:api.model.edit.tree.walk} lists the functions that permit walking all nodes in the model editor tree.
The function {\tt AimmsMeParent(MEH, pMEH)} creates a model editor handle to the parent of {\tt MEH}, and stores this handle in the output argument {\tt pMEH}.
The function {\tt AimmsMeFirst(MEH, fMEH)} creates a model editor handle to the first child of {\tt MEH}, and stores this handle in the output argument {\tt fMEH}.
The function {\tt AimmsMeNext( MEH, nMEH)} creates a model editor handle to the node next to {\tt MEH}, and stores this handle in the output argument {\tt nMEH}.
If such a parent, first child, or next node does not exist the {\tt AIMMSAPI\_ME\_NULL\_HANDLE\_NUMBER} handle is stored in the output argument although 
the corresponding function does not fail.

\begin{aimmstable}
\typindex{API function}{AimmsMeParent}
\typindex{API function}{AimmsMeFirst}
\typindex{API function}{AimmsMeNext}
\typindex{API function}{AimmsMeImportNode}
\typindex{API function}{AimmsMeExportNode}
\size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsMeParent(int MEH, int *pMEH)|\\
\verb|int AimmsMeFirst(int MEH, int *fMEH)|\\
\verb|int AimmsMeNext(int MEH, int *nMEH)|\\
\hline
\verb|int AimmsMeImportNode(int MEH, char *fn, const char *pwd)|\\
\verb|int AimmsMeExportNode(int MEH, char *fn, const char *pwd)|\\
\hline\hline
\end{tabular}
\caption{Reading, writing and tree walking a model editor tree}\label{table:api.model.edit.tree.walk}
\end{aimmstable}

\paragraph{Reading and writing (portions of) a model}
The functions that allow the reading of an AIMMS section from a file, or writing a section to a file are also listed in Table~\ref{table:api.model.edit.tree.walk}.
They use the {\tt Text .ams} file format. 
The function {\tt AimmsMeImportNode(MEH, fn, pwd)} reads a file {\tt fn} and stores the 
resulting model structure at node {\tt MEH}. 
The function {\tt AimmsMeExportNode(MEH, fn, pwd)} writes the model structure at node {\tt MEH} to file {\tt fn}. 
If {\tt MEH} does not refer to an AIMMS section, module, library, or model, the functions {\tt AimmsMeImportNode} and {\tt AimmsMeExportNode} will fail.

\paragraph{Compilation}
The model edit functions available for compilation and model status queries are listed in Table~\ref{table:api.model.edit.compilation}.
The central function {\tt AimmsMeCompile (MEH)} compiles the node {\tt MEH} and all its sub-nodes. 
The entire application (main model and libraries) is compiled if the argument {\tt MEH} equals {\tt AIMMS\-API\_ME\_NULL\_HANDLE\_NUMBER}.
If this compilation step is successful then the procedures are runnable.
%If the argument {\tt MEH} is {\tt AIMMS\-API\_ME\_NULL\_HANDLE\_NUMBER}, the 
%entire application (main model and libraries) are compiled and if successful the procedures are runnable.
The function {\tt AimmsMe\-IsRunnable(MEH, r)} stores 1 in the output argument {\tt r} if the procedure referenced by {\tt MEH} is runnable.
The function {\tt AimmsMeIsReadOnly(MEH, r)} stores 1 in the output argument {\tt r} if the node resides in a read-only library, such as the {\tt predeclared identifiers},
or a library that was read from a read only file.

\begin{aimmstable}
\typindex{API function}{AimmsMeCompile}
\typindex{API function}{AimmsIsRunnable}
\typindex{API function}{AimmsIsReadOnly}
\size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsMeCompile(int MEH)|\\
\verb|int AimmsMeIsRunnable(int MEH, int *r)|\\
\verb|int AimmsMeIsReadOnly(int MEH, int *r)|\\
\hline\hline
\end{tabular}
\caption{Model edit functions for compilation and status queries}\label{table:api.model.edit.compilation}
\end{aimmstable}

\paragraph{Limitations}
The following limitations apply to model edit functions from within the AIMMS API:
\begin{enumerate}
\item The {\tt SourceFile} attribute is not supported.
\item The current maximum number of identifiers is thirty thousand.      
\end{enumerate}
Further, when an AIMMS procedure is running, the identifiers 
in the main application can not be modified as explained in Section~\ref{sec:module.runtime}.


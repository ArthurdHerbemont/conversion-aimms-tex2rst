\section{Managing identifier handles}\label{sec:api.identifier}

\paragraph{Creation and data control}
\index{application programming interface!handle management}
\index{handle!management}

AIMMS offers the capability to dynamically create and delete handles to any
desired identifier slice over any desired local subdomain from within a DLL. In
addition, a subset of the AIMMS data control operators (as discussed in
Section~\ref{sec:data.control}) can be called from within external DLLs.
Table~\ref{table:api.identifier} lists all available AIMMS API functions for
creating handles and performing data control operations.
\begin{aimmstable}
\typindex{API function}{AimmsIdentifierCreate} \typindex{API
function}{AimmsIdentifierCreatePermuted} \typindex{API
function}{AimmsIdentifierDelete} \typindex{API function}{AimmsIdentifierEmpty}
\typindex{API function}{AimmsIdentifierCleanup} \typindex{API
function}{AimmsIdentifierUpdate} \typindex{API
function}{AimmsIdentifierDataVersion} \size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsIdentifierHandleCreate(char *name, int *domain, int *slicing, int flags, int *handle)|\\
\verb|int AimmsIdentifierHandleCreatePermuted(char *name, int *domain, int *slicing, int *permutation, int flags, int *handle)|\\
\verb|int AimmsIdentifierHandleDelete(int handle)|\\
\verb|int AimmsIdentifierEmpty(int handle)|\\
\verb|int AimmsIdentifierCleanup(int handle)|\\
\verb|int AimmsIdentifierUpdate(int handle)|\\
\verb|int AimmsIdentifierDataVersion(int handle, int *version)|\\
\hline\hline
\end{tabular}
\caption{AIMMS API functions for handle
management}\label{table:api.identifier}
\end{aimmstable}

\paragraph{Creating a handle}
\typindex{API function}{AimmsIdentifierCreate}

You can use the function {\tt AimmsIdentifierHandleCreate} to dynamically
create a handle to (a slice of) an AIMMS identifier or a suffix thereof
within an external function or procedure. You can restrict the scope of a
handle by
\begin{itemize}
\item specifying a {\em call} domain to which you want to restrict the
handle, or
\item by {\em slicing} one or more dimensions of the identifier.
\end{itemize}

\paragraph{Obtaining a suffix}

If you want a handle to an identifier itself, the name passed to {\tt
AimmsIdentifier\-HandleCreate} should just be the identifier name. If you want
a handle to a suffix of an identifier, you should pass the name of the
identifier followed by a dot and the suffix name. Thus, for instance, you
should pass the name {\tt "Transport.ReducedCost"} if you want a handle to the
reduced costs of the variable {\tt Transport}.

\paragraph{Specifying a call domain}

When you want to create a handle over the full root domain, you can simply pass
a null pointer for the {\tt domain} argument. If you want to specify an
additional call domain, you must pass an integer array of length equal to the
identifier's full dimension, each element containing a handle to the set to
which you want to restrict the domain. If the raw flag is not set, passing a
null pointer for the domain handle will effectively restrict the declaration
domain of the identifier at hand, because of the semantics of the raw flag (see
also Sections~\ref{sec:api.attribute} and~\ref{sec:api.value}).

\paragraph{Specifying a slice}

When you want to create a handle over the full dimension of an identifier, you
can simply pass a null pointer for the {\tt slicing} argument. If you want to
create a handle to a slice, you must pass an integer array of length equal to
the identifier's full dimension, each element containing either a null element
for all the domains that you do not want to slice, or the element number of the
element to which you want to slice.

\paragraph{Modification flags}

With the {\tt flags} argument in a call to {\tt AimmsIdentifierHandleCreate}
you can specify which modification flags should be set for the handle to be
created. The format of the {\tt flags} argument is the same as in the function
{\tt AimmsAttributeFlags} discussed in the previous section.

\paragraph{Creating a permuted handle}
\typindex{API function}{AimmsIdentifierHandleCreatePermuted}

With the function {\tt AimmsIdentifierHandleCreatePermuted} you can obtain a
handle to a multidimensional identifier, for which the order in which element
tuples are returned is permuted. Handles created by {\tt
AimmsIdentifierHandle\- CreatePermuted} are always read-only, i.e.\ cannot be
used in the functions {\tt Aimms\-ValueAssign} and {\tt AimmsValueAssignMulti}.
The {\tt permutation} argument must be specified according to the rules
explained for the function {\tt AimmsAttributePer\-mutation}.

\paragraph{Example}

Consider an identifier {\tt p(i,j,k,l)} for which you want to retrieve the
values as if the identifier were defined as {\tt p(k,i,l,j)}. To retrieve all
values of {\tt p} in this order, the {\tt permutation} array must be specified
as {\tt [2,4,1,3]}.

\paragraph{Deleting handles}
\typindex{API function}{AimmsIdentifierDelete}

With the function {\tt AimmsIdentifierHandleDelete} you can delete a
dynamically created handle that is no longer needed. The function fails when
you try to delete a handle that was passed as an argument to the DLL. After
deletion the handle can no longer be used in conjunction with any AIMMS API
function.

\paragraph{Empty, cleanup and update handles}
\typindex{API function}{AimmsIdentifierEmpty} \typindex{API
function}{AimmsIdentifierCleanup} \typindex{API
function}{AimmsIdentifierUpdate}

The AIMMS API functions
\begin{itemize}
\item {\tt AimmsIdentifierEmpty},
\item {\tt AimmsIdentifierCleanup}, and
\item {\tt AimmsIdentifierUpdate}
\end{itemize}
can be called to perform the identical actions on a set or identifier (slice)
from within an external DLL as can be accomplished by the data control
operators {\tt EMPTY}, {\tt CLEANUP} and {\tt UPDATE} from within AIMMS,
respectively. The function {\tt AimmsIdentifierEmpty} will empty the particular
slice and subdomain of the identifier associated with the handle. The other two
functions will cleanup or update the {\em entire} data set of the identifier
associated with the handle, regardless of the specified slicing and local
domain.

\paragraph{Data version}
\typindex{API function}{AimmsIdentifierDataVersion}

For every identifier within your model AIMMS maintains a version number of
the data associated with the identifier. This number is incremented each time a
data value of the identifier has been changed. You can use the function {\tt
AimmsIdentifierDataVersion} to retrieve this version number, for instance, to
verify whether the data has changed relative to the last time you retrieved it.

\paragraph{Checking for global data changes}

When you apply the function {\tt AimmsIdentifierDataVersion} to the predefined
handle value {\tt AIMMSAPI\_MODEL\_HANDLE}, AIMMS will return a data version
number based on the cases and datasets currently active within the model.
AIMMS will update this number as soon as the combined configuration of the
active case and/or datasets within the model has changed, as well as after a
call to the {\tt CLEANDEPENDENTS} operator. A change in this global data
version number is a good indication that the contents of all or a number of
domain sets may have changed, and must be retrieved again.


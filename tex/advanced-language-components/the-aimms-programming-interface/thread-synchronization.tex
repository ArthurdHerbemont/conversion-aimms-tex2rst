\section{Thread synchronization}\label{sec:api.control}

\paragraph{Multiple threads}
\index{application programming interface!thread synchronization}

The AIMMS API allows multiple DLLs to be active within the context of a
single project. While some of these DLLs may only be useful when called from
within your AIMMS project itself, you may want other DLLs to run
independently in a separate thread of execution. Such behavior may be
necessary, for instance, when
\begin{itemize}
\item you want to link AIMMS to an online data source, where an
independent DLL collects the online data and passes it on to AIMMS whenever
appropriate, or
\item  you want to call AIMMS as an independent optimization
engine from within your own program and need to pass data to AIMMS whenever
necessary.
\end{itemize}

\paragraph{AIMMS thread}

When you open an AIMMS project by calling the function {\tt
AimmsProjectOpen} from within your own application, AIMMS will create a new
thread. This AIMMS thread will deal with
\begin{itemize}
\item all end-user interaction initiated from within the AIMMS
end-user interface (which is created as part of opening the project), and
\item all asynchronous execution requests that are initiated
either from within your application, another external DLL linked to your
AIMMS project, or from within the model itself.
\end{itemize}

\paragraph{Thread initialization}

Whenever you want to call AIMMS API functions from within a thread started
by yourself, you must make sure that the thread is well-equipped to do so by
calling the AIMMS thread (un)initialization functions listed in
Table~\ref{table:api.thread-initialization}.

\begin{aimmstable}
\typindex{API function}{AimmsThreadAttach} \typindex{API
function}{AimmsThreadDetach} \size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsThreadAttach(void)|\\
\verb|int AimmsThreadDetach(void)|\\
\hline\hline
\end{tabular}
\caption{AIMMS API functions for thread
initialization}\label{table:api.thread-initialization}
\end{aimmstable}

\paragraph{Initializing threads\dots}
\typindex{API function}{AimmsThreadAttach} \typindex{API
function}{AimmsThreadDetach}

Prior to calling any other AIMMS API function from within a newly created
thread, you should call the function {\tt AimmsThreadAttach}. It will make sure
that any thread-specific initialization required for calling the AIMMS
execution engine is performed properly. Similarly, you should call the function
{\tt Aimms\-Thread\-Detach} just prior to exiting the thread.

\paragraph{\dots\ includes COM initialization}

Among others, a call to {\tt AimmsThreadAttach} will initialize the Microsoft
COM library in a manner compatible with the COM apartment model employed by
AIMMS. Therefore, if you are using COM interfaces within your thread, you
should not call the COM SDK functions {\tt CoInitialize} (or {\tt
CoInitializeEx}) and {\tt CoUninitialize} directly to initialize the COM
library, but rather call {\tt Aimms\-Thread\-Attach} and {\tt
AimmsThreadDetach}.

\paragraph{Thread synchronization}

Whenever an AIMMS project runs in a multi-threaded environment,
synchronization of the execution and data retrieval requests becomes of the
utmost importance. By default, AIMMS will make sure that no two execution or
data retrieval requests initiated from different threads are dealt with
simultaneously. However, this default synchronization scheme does not preclude
that the execution of two subsequent requests from one thread is interrupted by
a request from another thread.

\paragraph{Obtaining exclusive control}

When the proper functioning of your application requires that your execution
and data retrieval requests to AIMMS are not interrupted by requests from
competing threads, you can use the functions listed in
Table~\ref{table:api.control} to obtain exclusive control over the AIMMS
execution engine.

\begin{aimmstable}
\typindex{API function}{AimmsControlGet} \typindex{API
function}{AimmsControlRelease} \size{8}{9pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
\verb|int AimmsControlGet(int timeout)|\\
\verb|int AimmsControlRelease(void)|\\
\hline\hline
\end{tabular}
\caption{AIMMS API functions for obtaining exclusive
control}\label{table:api.control}
\end{aimmstable}

\paragraph{Obtaining and releasing control}
\typindex{API function}{AimmsControlGet} \typindex{API
function}{AimmsControlRelease}

With the function {\tt AimmsControlGet} you can restrict control over the
current AIMMS session exclusively to the thread calling {\tt
AimmsControlGet}. Execution and data retrieval requests from any thread other
than this controlling thread (including the AIMMS thread itself) will block
until the controlling thread has released the control. The function {\tt
AimmsControlRelease} releases the exclusive control over the AIMMS session.
{\em Note that every successful call to {\tt AimmsControlGet} must be followed
by a corresponding call to {\tt AimmsControlRelease}}, or AIMMS will be
inaccessible to all other threads for the remainder of the session. {\tt
AimmsControlRelease} fails when the calling thread does not have exclusive
control.

\paragraph{Waiting for the control}

When another thread has exclusive control over AIMMS, either obtained
explicitly through a call to {\tt AimmsControlGet} or implicitly through an
execution or data retrieval request, the function {\tt AimmsControlGet} will
block {\em timeout} milliseconds before returning with a failure. By choosing a
{\em timeout} of {\tt WAIT\_INFINITE}, the function {\tt AimmsControlGet} will
block until it gets exclusive control.

\paragraph{Nonblocking execution}

If you want to make sure that a subsequent execution request will never block,
you can
\begin{itemize}
\item call {\tt AimmsControlGet} with a timeout of 0 milliseconds,
\item perform the execution request when successful, and
\item subsequently release the control.
\end{itemize}
The call to {\tt AimmsControlGet} has the effect of verifying that no other
thread is using AIMMS at the moment. If you cannot get exclusive control,
you must store the request for later execution.


\section{Creating timetables}\label{sec:time.timetable}

\paragraph{Timetables}
\index{timetable}
\index{indexed set!timetable}

\syntaxmark{timetable}
A {\em timetable} in AIMMS is an indexed set, which, for every
period in a {\tt Horizon}, lists the corresponding time slots in the
associated {\tt Calendar}. Timetables play a central role during the
conversion from calendar data to horizon data and vice versa.

\paragraph{The procedure {\tt CreateTimeTable}}
\procindex{CreateTimeTable}
\AIMMSlink{createtimetable}

Through the predefined procedure {\tt CreateTimeTable}, you can
request AIMMS to flexibly construct a {\em timetable} on the basis
of
\begin{itemize}
\item a {\em time slot} in the  calendar and a {\em period} in the horizon
that should be aligned at the beginning of the planning interval,
\item the desired {\em length} of each period in the horizon expressed
as a number of time slots in the calendar,
\item an indication, for every period in the horizon, whether the
{\em length dominates} over any specified delimiter slots,
\item a set of {\em inactive time slots}, which should be excluded
from the timetable and, consequently, from the period length
computation, and
\item a set of {\em delimiter time slots}, at which new horizon periods should
begin.
\end{itemize}

\paragraph{Syntax}

The syntax of the procedure {\tt CreateTimeTable} is as follows:
\begin{itemize}
\item {\tt CreateTimeTable}({\em timetable}, {\em current-timeslot}, {\em current-period}, {\em period-length}, {\em length-dominates}, {\em inactive-slots}, {\em delimiter-slots})
\end{itemize}
The (output) {\em timetable} argument of the procedure {\tt
CreateTimeTable} must, in general, be an indexed set in a calendar and
defined over the horizon to be linked to the calendar. Its contents is
completely determined by AIMMS on the basis of the other arguments.
The {\em current-timeslot} and {\em current-period} arguments must be
elements of the appropriate calendar and horizon, respectively.

\paragraph{Element parameter as timetable}

In the special case that you know a priori that each period in the
timetable is associated with exactly one time slot in the calendar,
AIMMS also allows the {\em timetable} argument of the {\tt
CreateTimeTable} procedure to be an element parameter (instead of an
indexed set). When you specify an element parameter, however, a
runtime error will result if the input arguments of the call to {\tt
CreateTimeTable} give rise to periods consisting of multiple time
slots.

\paragraph{Several possibilities}

You have several possibilities of specifying your input data which
influence the way in which the timetable is created. You can:
\begin{itemize}
\item only specify the length of each period to be created,
\item only specify delimiter slots at which a new period must
begin, or
\item flexibly combine both of the above two methods.
\end{itemize}

\paragraph{Period length}

The {\em period-length} argument must be a positive integer-valued
one-dimensional parameter defined over the horizon. It specifies the
desired length of each period in the horizon in terms of the number of
time slots to be contained in it. If you do not provide delimiter
slots (explained below), AIMMS will create a timetable solely on
the basis of the indicated period lengths.

\paragraph{Inactive slots}
\index{inactive time slot}
\index{time slot!inactive}

The {\em inactive-slots} argument must be a subset of the calendar
that is specified as the range of the {\em timetable} argument.
Through this argument you can specify a set of time slots that are
always to be excluded from the timetable. You can use this argument,
for instance, to indicate that weekend days or holidays are not to be
part of a planning period. Inactive time slots are excluded from the
timetable, and are not accounted for in the computation of the desired
period length.

\paragraph{Delimiter slots}
\index{delimiter time slot}
\index{time slot!delimiter}

The {\em delimiter-slots} argument must be a subset of the calendar
that is specified as the range of the {\em timetable} argument.
AIMMS will begin a new period in the horizon whenever it encounters
a delimiter slot in the calendar provided no (offending) period length
has been specified for the period that is terminated at the delimiter
slot.

\paragraph{Combining period length and delimiters}

In addition to using either of the above methods to create a
timetable, you can also combine them to create timetables in an even
more flexible manner by specifying the {\em length-dominates}
argument, which must be a one-dimensional parameter defined over the
horizon. The following rules apply.
\begin{itemize}
\hfuzz 2pt
\item If the {\em length-dominates} argument is non\-zero for a
particular period, meeting the specified period length prevails over
any delimiter slots that are possibly contained in that period.
\item If the {\em length-dominates} argument is zero for a particular
period and the specified period length is 0, AIMMS will not
restrict that period on the basis of length, but only on the basis of
delimiter slots.
\item If the {\em length-dominates} argument is zero for a particular
period and the specified period length is positive, AIMMS will try
to construct a period of the indicated length, but will terminate the
period earlier if it encounters a delimiter slot first.
\end{itemize}

\paragraph{Timetable creation}

In creating a timetable, AIMMS will always start by aligning the
{\em current-timeslot} argument with the beginning of the {\em
current-period}. Periods beyond {\em current-period} are determined
sequentially by moving forward time slot by time slot, until a new
period must be started due to hitting the period length criterion of
the current period (taking into account the inactive slots), or by
hitting a delimiter slot. Periods prior to {\em current-period} are
determined sequentially by moving backwards in time starting at {\em
current-timeslot}.

\paragraph{Adapting timetables}

As a timetable is nothing more than an indexed set, you still have the
opportunity to make manual changes to a timetable after its contents
have been computed by the AIMMS procedure {\tt CreateTimeTable}.
This allows you to make any change to the timetable that you cannot,
or do not want to, implement directly using the procedure {\tt
CreateTimeTable}.

\paragraph{Example}
\subtypindex{procedure}{CreateTimeTable}{example of use}

Consider a timetable which links the daily calendar declared in
Section~\ref{sec:time.calendar} and the horizon of
Section~\ref{sec:time.horizon}, which consists of 10 periods named
{\tt p-01} \dots{}{\tt p-10}. The following conditions should be
met:
\begin{itemize}
\item the planning interval starts at period {\tt p-02}, i.e.\ period
{\tt p-01} is in the past,
\item periods {\tt p-01}\dots{\tt p-05} have a fixed length of 1 day,
\item periods {\tt p-06}\dots{\tt p-10} should have a length of at
most a week, with new periods starting on every Monday.
\end{itemize}
To create the corresponding timetable using the procedure {\tt
CreateTimeTable}, the following additional identifiers need to be
added to the model:
\begin{itemize}
\item an indexed subset {\tt TimeTable(h)} of {\tt DailyCalendar},
\item a subset {\tt DelimiterDays} of {\tt DailyCalendar} containing
all Mondays in the calendar (i.e.\ {\tt '01-01-96'}, {\tt '08-01-96'},
etc.),
\item a subset {\tt InactiveDays} of {\tt DailyCalendar} containing
all days that you want to exclude from the timetable (e.g.\ all
weekend days),
\item a parameter {\tt PeriodLength(h)} assuming the value 1 for
the periods {\tt p-01} \dots{} {\tt p-05}, and zero otherwise,
\item a parameter {\tt LengthDominates(h)} assuming the value 1 for
the periods {\tt p-01} \dots{} {\tt p-05}, and zero otherwise.
\end{itemize}
To compute the contents of the timetable, aligning the time slot
pointed at by {\tt CurrentDay} and period {\tt IntervalStart}, one
should call
\begin{example}
    CreateTimeTable( TimeTable, CurrentDay, IntervalStart,
                     PeriodLength, LengthDominates,
                     InactiveDays, DelimiterDays );
\end{example}
If all weekend days are inactive, and {\tt CurrentDay} equals {\tt
'24/01/96'} (a Wednesday), then {\tt TimeTable} describes the
following mapping.
\begin{aimmstable}
\begin{tabular}{|c|l||c|l|}
\hline\hline
{\bf Period} & {\bf Calendar slots} & {\bf Period} & {\bf Calendar
slots}\\
\hline
{\tt p-01} & {\tt 23/01/96} (Tue) & {\tt p-06} & {\tt 30/01/96 - 02/02/96} (Tue-Fri)\\
{\tt p-02} & {\tt 24/01/96} (Wed) & {\tt p-07} & {\tt 05/01/96 - 09/02/96} (Mon-Fri)\\
{\tt p-03} & {\tt 25/01/96} (Thu) & {\tt p-08} & {\tt 12/01/96 - 16/02/96} (Mon-Fri)\\
{\tt p-04} & {\tt 26/01/96} (Fri) & {\tt p-09} & {\tt 19/01/96 - 23/02/96} (Mon-Fri)\\
{\tt p-05} & {\tt 29/01/96} (Mon) & {\tt p-10} & {\tt 26/01/96 - 01/03/96} (Mon-Fri)\\
\hline\hline
\end{tabular}
\end{aimmstable}

\paragraph{The function {\tt Timeslot- Characteristic}}
\funcindex{TimeslotCharacteristic}
\AIMMSlink{timeslotcharacteristic}

The process of initializing the sets used in the {\em
delimiter-slots} and {\em inactive-slots} arguments can be quite
cumbersome when your model covers a large time span. For that
reason AIMMS offers the convenient function {\tt
TimeslotChar\-acter\-istic}. With it, you can obtain a numeric
value which characterizes the time slot, in terms of its day of
the week, its day in the year, etc. The syntax of the function is
straightforward:
\begin{itemize}
\item {\tt TimeslotCharacteristic}({\em timeslot}, {\em characteristic}[, {\em timezone}[, ignoredst]])
\end{itemize}
The {\em characteristic} argument must be an element of the predefined
set {\tt Time\-slotCharacteristics}. The elements of this set, as well
as the associated function values are listed in
Table~\ref{table:time.time-char}.
\begin{aimmstable}
\begin{tabular}{|l|c|l|}
\hline\hline
{\bf Characteristic} & {\bf Function value} & {\bf First} \\
& {\bf range} & \\
\hline
{\tt century}  & 0, \dots, 99 & \\
{\tt year}  & 0, \dots, 99 & \\
{\tt quarter} & 1, \dots, 4 & \\
{\tt month} & 1, \dots, 12 & January \\
\hline
{\tt weekday} &  1, \dots, 7 & Monday \\
{\tt yearday} & 1, \dots, 366 & \\
{\tt monthday} & 1, \dots, 31 & \\
\hline
{\tt week} & 1, \dots, 53 & \\
{\tt weekyear} &  0, \dots, 99 &  \\
{\tt weekcentury} &  0, \dots, 99 &  \\
\hline
{\tt hour}    & 0, \dots, 23 & \\
{\tt minute}  & 0, \dots, 59 & \\
{\tt second}  & 0, \dots, 59 & \\
{\tt tick}    & 0, \dots, 99 & \\
\hline
{\tt dst}     & 0, 1 & \\
\hline\hline
\end{tabular}
\caption{Elements of the set {\tt TimeslotCharacteristics}}\label{table:time.time-char}
\end{aimmstable}

\paragraph{Day and week numbering}\herelabel{time:weeknumbering}
\index{week numbering}
\subtypindex{function}{TimeslotCharacteristics}{week numbering}

Internally, AIMMS takes Monday as the first day in a week, and
considers week 1 as the first week that contains at least four days of
the new year. This is equivalent to stating that week 1 contains the
first Thursday of the new year. Through the {\tt 'week'}, {\tt 'weekyear'}
and {\tt 'weekcentury'} characteristics you obtain the week number
corresponding to a particular date and its corresponding year and
century. For instance, Friday January 1, 1999 is day 5 of week 53 of
year 1998.

\paragraph{Example}

Consider a daily calendar {\tt DailyCalendar} with index {\tt d}. The
following assignment to a subset {\tt WorkingDays} of a {\tt
DailyCalendar} will select all non-weekend days in the calendar.
\begin{example}
    WorkingDays := { d | TimeslotCharacteristic(d,'weekday') <= 5 } ;
\end{example}

\paragraph{Calendar-calendar linkage}

You can also use the function {\tt TimeslotCharacteristic} to create a
timetable linking two calendars (e.g.\ to create monthly overviews of
daily data). As an example, consider the calendars {\tt DailyCalendar}
and {\tt MonthlyCalendar} declared in Section~\ref{sec:time.calendar},
as well as an indexed set {\tt MonthDays(m)} of {\tt DailyCalendar},
which can serve as a timetable. {\tt MonthDays} can be computed as
follows.
\begin{example}
    MonthDays(m) := { d | TimeslotCharacteristic(d,'year') =
                                TimeslotCharacteristic(m,'year') and
                          TimeslotCharacteristic(d,'month') =
                                TimeslotCharacteristic(m,'month')    };
\end{example}
A check on the {\tt 'year'} characteristic is not necessary if both
calendars are contained within a single calendar year.

\paragraph{Time zone support}
\index{time zone! TimeslotCharacteristic@{\tt TimeSlotCharacteristic}}
\index{daylight saving time! TimeslotCharacteristic@{\tt TimeSlotCharacteristic}}
\presetindex{AllTimeZones}

Through the optional {\em timezone} argument of the function {\tt TimeslotCharacteris\-tic}, you can specify with respect to which time zone you want to obtain the specified characteristic. The {\em timezone} argument must be an element of the predefined set {\tt AllTimeZones} (see also Section~\ref{sec:time.format.dst}). By default, AIMMS assumes the local time zone without daylight saving time.

\paragraph{Daylight saving time}

When you specify a time zone with daylight saving time, you can retrieve whether daylight saving time is active through the {\tt 'dst'} characteristic.
With the optional argument {\em ignoredst} (default 0) of the function {\tt TimeSlotCharacter\-istic}, you can specify whether you want daylight saving time to be ignored. With {\em ignoredst} set to 1, or in a time zone without daylight saving time, the outcome for the {\tt 'dst'} characteristic will always be 0.


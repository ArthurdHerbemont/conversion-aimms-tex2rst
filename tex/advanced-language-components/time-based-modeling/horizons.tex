\section{Horizons}\label{sec:time.horizon}

\paragraph{Horizons}
\index{use of!horizon}
\index{horizon!use of}

A horizon in AIMMS is basically a set of planning periods. The
elements in a horizon are divided into three groups, also referred to
as time blocks. The main group of elements comprise the {\em planning
interval}. Periods prior to the planning interval form the {\em past},
while periods following the planning interval form the {\em beyond}.
When variables and constraints are indexed over a horizon, AIMMS
automatically restricts the generation of these constraints and
variables to periods within the planning interval.

\paragraph{Effect on constraints and assignments}
\index{constraint!use of horizon}
\index{variable!use of horizon}

Whenever you use a horizon to construct a time-dependent model,
AIMMS has the following features:
\begin{itemize}
\item constraints are excluded from the past and beyond periods,
\item variables are assumed to be fixed for these periods, and
\item assignments and definitions to variables and parameters are, by
default, only executed for the periods in the planning interval.
\end{itemize}

\paragraph{Horizon attributes}
\declindex{Horizon}
\AIMMSlink{horizon}

Horizons, like calendars, have a number of associated attributes, some
of which are inherited from sets. They are summarized in
Table~\ref{table:time.attr-horizon}.

\begin{aimmstable}
\AIMMSlink{horizon.subset_of}
\AIMMSlink{horizon.index}
\AIMMSlink{horizon.parameter}
\begin{tabular}{|l|l|c|c|}
\hline
\hline
{\bf Attribute} & {\bf Value-type} & {\bf See also} & {\bf Mandatory} \\
& & {\bf page} & \\
\hline
{\tt SubsetOf}   & {\em subset-domain} &\pageref{attr:set.subset-of}& \\
\verb|Index|     & {\em identifier-list} &\pageref{attr:set.index}  & yes \\
\verb|Parameter| & {\em identifier-list} &\pageref{attr:set.parameter}  & \\
\verb|Text|       & {\em string}           & \pageref{attr:prelim.text}&\\
\verb|Comment|    & {\em comment string}   & \pageref{attr:prelim.comment}&\\
{\tt Definition}  & {\em set-expression}&\pageref{attr:set.definition}& yes\\
\hline
\verb|CurrentPeriod|       & {\em element}    & & yes \\
\verb|IntervalLength|      & {\em integer-reference} & &  \\
\hline\hline
\end{tabular}
\caption{Horizon attributes}\label{table:time.attr-horizon}
\end{aimmstable}

\paragraph{Horizon attributes explained}
\declattrindex{horizon}{CurrentPeriod}
\declattrindex{horizon}{IntervalLength}
\AIMMSlink{horizon.current_period}
\AIMMSlink{horizon.interval_length}

The {\tt CurrentPeriod} attribute denotes the first period of the
planning interval. The periods prior to the current period belong to
the past. The integer value associated with the attribute {\tt
IntervalLength} determines the number of periods in the planning
interval (including the current period). Without an input value, all
periods from the current period onwards are part of the planning
interval.

\paragraph{Definition is mandatory}
\declattrindex{horizon}{Definition}
\AIMMSlink{horizon.definition}

AIMMS requires you to specify the contents of a {\tt Horizon}
uniquely through its {\tt Definition} attribute. The ordering of the
periods in the horizon is fully determined by the set expression in
its definition. You still have the freedom, however, to specify the
{\tt Horizon} as a subset of another set.

\paragraph{Example}

Given a scalar parameter {\tt MaxPeriods}, the following example
illustrates the declaration of a horizon.
\begin{example}
Horizon ModelPeriods {
    Index          : h;
    Parameter      : IntervalStart;
    CurrentPeriod  : IntervalStart;
    Definition     : ElementRange( 1, MaxPeriods, prefix: "p-" );
}
\end{example}
If, for instance, the scalar {\tt MaxPeriods} equals 10, this will
result in the creation of a period set containing the elements {\tt
'p-01'},\dots,{\tt 'p-10'}. The start of the planning interval is
fully determined by the value of the element parameter {\tt
IntervalStart}, which for instance could be equal to {\tt 'p-02'}.
This will result in a planning interval consisting of the periods {\tt
'p-02'},\dots,{\tt 'p-10'}.

\paragraph{Example of use}
\index{horizon!use in constraint}
\index{horizon!use in variable}

Consider the parameter {\tt Demand(h)} together with the variables
{\tt Production(h)} and {\tt Stock(h)}. Then the definition of the
variable {\tt Stock} can be declared as follows.
\begin{example}
Variable Stock {
    IndexDomain    : h;
    Range          : NonNegative;
    Definition     : Stock(h-1) + Production(h) - Demand(h);
}
\end{example}
When the variable {\tt Stock} is included in a mathematical program,
AIMMS will only generate individual variables with their associated
definition for those values of {\tt h} that correspond to the current
period and onwards. The reference {\tt Stock(h-1)} refers to a fixed
value of {\tt Stock} from the past whenever the index {\tt h} points
to the current period. The values associated with periods from the
past (and from the beyond if they were there) are assumed to be fixed.

\paragraph{Accessing past and beyond}
\suffindex{horizon}{Past}
\suffindex{horizon}{Planning}
\suffindex{horizon}{Beyond}

To provide easy access to periods in the past and the beyond, AIMMS
offers three horizon-specific suffices. They are:
\begin{itemize}
\item the {\tt Past} suffix,
\item the {\tt Planning} suffix, and
\item the {\tt Beyond} suffix.
\end{itemize}
These suffices provide access to the subsets of the horizon
representing the past, the planning interval and the beyond.

\paragraph{Horizon binding rules}
\index{index binding!horizon}

When you use a horizon index in an index binding operation (see
Chapter~\ref{chap:bind}), AIMMS will, by default, perform that
operation only for the periods in the planning interval. You can
override this default behavior by a local binding using the suffices
discussed above.

\paragraph{Example}
\index{horizon!example of use}

Consider the horizon {\tt ModelPeriods} of the previous example. The
following assignments illustrate the binding behavior of horizons.
\begin{example}
    Demand(h)                          := 10; ! only periods in planning interval (default)
    Demand(h in ModelPeriods.Planning) := 10; ! only periods in planning interval

    Demand(h in ModelPeriods.Past)     := 10; ! only periods in the past
    Demand(h in ModelPeriods.Beyond)   := 10; ! only periods in the beyond

    Demand(h in ModelPeriods)          := 10; ! all periods in the horizon
\end{example}

\paragraph{Use of lag and lead operators}
\index{lag/lead operator!horizon}
\index{horizon!lag/lead operator}

When you use one of the lag and lead operators \verb|+|, \verb|++|,
\verb|-| or \verb|--| (see also
Section~\ref{sec:set-expr.elem.lag-lead}) in conjunction with a
horizon index, AIMMS will interpret such references with respect to
the {\em entire} horizon, and not just with respect to the planning
period. If the horizon index is locally re-bound to one of the subsets
of periods in the {\tt Past} or {\tt Beyond}, as illustrated above,
the lag or lead operation will be interpreted with respect to the
specified subset.

\paragraph{Example}

Consider the horizon {\tt ModelPeriods} of the previous example. The
following assignments illustrate the use of lag and lead operators in
conjuction with horizons.
\begin{example}
    Stock(h)                              := Stock(h-1) + Supply(h) - Demand(h);
    Stock(h | h in ModelPeriods.Planning) := Stock(h-1) + Supply(h) - Demand(h);

    Stock(h in ModelPeriods.Planning)     := Stock(h-1) + Supply(h) - Demand(h);
    Stock(h in ModelPeriods.Planning)     := Stock(h--1) + Supply(h) - Demand(h);
\end{example}
The first two assignments are completely equivalent (in fact, the
second assignment is precisely the way in which AIMMS interprets
the default binding behavior of a horizon index). For the first
element in the planning interval, the reference {\tt h-1} refers to
the last element of the past interval. In the third assignment, {\tt
h-1} refers to a non-existing element for the first element in the
planning interval, completely in accordance with the default semantics
of lag and lead operators. In the fourth assignment, {\tt h--1} refers
to the last element of the planning interval.

\paragraph{Data transfer on entire domain}
\subtypindex{statement}{READ}{horizon-based data}
\subtypindex{statement}{WRITE}{horizon-based data}
\subtypindex{statement}{DISPLAY}{horizon-based data}

Operations which can be applied to identifiers without references to
their indices (such as the {\tt READ}, {\tt WRITE} or {\tt DISPLAY}
statements), operate on the entire horizon domain. Thus, for example,
during data transfer with a database, AIMMS will retrieve or store
the data for {\em all} periods in the horizon, and not just for the
periods in the planning interval.


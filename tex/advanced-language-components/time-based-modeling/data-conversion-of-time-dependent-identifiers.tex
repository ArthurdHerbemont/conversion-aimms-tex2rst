\section{Data conversion of time-dependent identifiers}\label{sec:time.conversion}

\paragraph{Time-dependent data}

When you are working with time-dependent data, it is usually not
sufficient to provide and work with a single fixed-time scale.
The following examples serve as an illustration.
\begin{itemize}
\item Demand data is available in a database on a day-by-day basis,
but is needed in a mathematical program for each horizon period.
\item Production quantities are computed per horizon period, but are
needed on a day-by-day basis.
\item For all of the above data weekly or monthly overviews are
also required.
\end{itemize}

\paragraph{The procedures {\tt Aggregate} and {\tt Disaggregate}}
\procindex{Aggregate}
\procindex{Disaggregate}
\AIMMSlink{aggregate}
\AIMMSlink{disaggregate}

With the procedures {\tt Aggregate} and {\tt Disaggregate} you can
instruct AIMMS to perform an aggregation or disaggregation step
from one time scale to another. Both procedures perform the
aggregation or disaggregation of a single identifier in one time scale
to another identifier in a second time scale, given a timetable
linking both time scales and a predefined aggregation type. The syntax
is as follows.
\begin{itemize}
\item {\tt Aggregate}({\em timeslot-data}, {\em period-data}, {\em
timetable}, {\em type}[, {\em locus}])
\item {\tt Disaggregate}({\em period-data}, {\em timeslot-data}, {\em
timetable}, {\em type}[, {\em locus}])
\end{itemize}

\paragraph{Time slot and period data}

The identifiers (or identifier slices) passed to the {\tt Aggregate}
and {\tt Disaggregate} procedures holding the time-dependent data must
be of equal dimension. All domain sets in the index domains must
coincide, except for the time domains. These must be consistent with
the domain and range of the specified timetable.

\paragraph{Different conversions}
\index{aggregation!during conversion}
\index{aggregation!at conversion}

As was mentioned in Section~\ref{sec:time.intro}, time-dependent data
can be interpreted as taking place {\em during} a period or {\em at a
given moment} in the period. Calendar data, which takes place {\em
during} a period, needs to be converted into a period-based
representation by allocating the data values in proportion to the
overlap between time slots and horizon periods. On the other hand,
calendar data which takes place {\em at a given moment}, needs to be
converted to a period-based representation by linearly interpolating
the original data values.

\paragraph{Aggregation types}

\syntaxmark{conversion} The possible values for the {\em type}
argument of the {\tt Aggregate} and {\tt Disaggregate} procedures are
the elements of the predefined set {\tt AggregationTypes} given by:
\begin{itemize}
\item {\tt summation},
\item {\tt average},
\item {\tt maximum},
\item {\tt minimum}, and
\item {\tt interpolation}.
\end{itemize}

\paragraph{Reverse conversion}

All of the above predefined conversion rules are characterized by the
following property.
\begin{quote}
The disaggregation of period data into time slot data, followed by
immediate aggregation, will reproduce identical values of the period
data.
\end{quote}
Aggregation followed by disaggregation does not have this property.
Fortunately, as the horizon rolls along, disaggregation followed by
aggregation is the essential conversion.

\paragraph{The {\tt summation} rule}
\index{aggregation!during conversion!summation}

The conversion rule {\tt summation} is the most commonly used
aggregation/dis\-aggregation rule for quantities that take place {\em
during} a period. It is appropriate for such typical quantities as
production and arrivals. Data values from a number of consecutive time
slots in the calendar are summed together to form a single value for a
multi-unit period in the horizon. The reverse conversion takes place
by dividing the single value equally between the consecutive time
slots.

\paragraph{The {\tt average}, {\tt maximum}, and {\tt minimum} rules}
\index{aggregation!during conversion!average}
\index{aggregation!during conversion!maximum}
\index{aggregation!during conversion!minimum}

The conversion rules {\tt average}, {\tt maximum}, and {\tt minimum}
are less frequently used aggregation/disaggregation rules for
quantities that take place {\em during} a period. These rules are
appropriate for such typical quantities as temperature or capacity.
Aggregation of data from a number of consecutive time slots to a
single period in the horizon takes place by considering the average or
the maximum or minimum value over all time slots contained in the
period. The reverse conversion consists of assigning the single value
to each time slot contained in the period.

\paragraph{Illustration of aggregation}
\index{aggregation!during conversion!example of use}

Table~\ref{table:time.conv-during} demonstrates the aggregation and
disaggregation taking place for each conversion rule. The conversion
operates on a single period consisting of 3 time slots in the
calendar.
\begin{aimmstable}
\begin{tabular}{|c|p{.85cm}|p{.85cm}|p{.85cm}||p{.85cm}|p{.85cm}|p{.85cm}|}
\hline\hline
{\bf Conversion rule} & \multicolumn{3}{|c||}{{\bf Calendar to horizon}} &
\multicolumn{3}{|c|}{{\bf Horizon to calendar}} \\
\cline{2-7}
 & \hskip.3cm 3 & \hskip.3cm 1 & \hskip.3cm 2 &  \multicolumn{3}{|c|}{3} \\
\hline
{\tt summation}  & \multicolumn{3}{|c||}{6} & \hskip.3cm 1 & \hskip.3cm 1 & \hskip.3cm 1 \\
\hline
{\tt average}    & \multicolumn{3}{|c||}{2} & \hskip.3cm 3 & \hskip.3cm 3 & \hskip.3cm 3 \\
\hline
{\tt maximum}    & \multicolumn{3}{|c||}{3} & \hskip.3cm 3 & \hskip.3cm 3 & \hskip.3cm 3 \\
\hline
{\tt minimum}    & \multicolumn{3}{|c||}{1} & \hskip.3cm 3 & \hskip.3cm 3 & \hskip.3cm 3 \\
\hline\hline
\end{tabular}
\caption{Conversion rules for ``during'' quantities}\label{table:time.conv-during}
\end{aimmstable}

\paragraph{Interpolation}
\index{aggregation!at conversion!interpolation}
\index{interpolation}
\index{locus}

The {\tt interpolation} rule should be used for all quantities that
take place {\em at a given moment} in a period. For the {\tt
interpolation} rule you have to specify one additional argument in the
{\tt Aggregate} and {\tt Disaggregate} procedures, the {\em locus}.
The {\em locus} of the {\tt interpolation} defines at which moment in
a period---as a value between 0 and 1---the quantity at hand is to be
measured. Thus, a {\em locus} of 0 means that the quantity is measured
at the beginning of every period, a {\em locus} of 1 means that the
quantity is measured at the end of every period, while a {\em locus}
of 0.5 means that the quantity is measured midway through the period.

\paragraph{Interpolation for disaggregation}

When disaggregating data from periods to time slots, AIMMS
interpolates linearly between the respective loci of two subsequent
periods. For the outermost periods, AIMMS assigns the last
available interpolated value.

\paragraph{Interpolation for aggregation}

AIMMS applies a simple rule for the seemingly awkward interpolation
of data from unit-length time slots to variable-length horizon
periods.  It will simply take the value associated with the time slot
in which the {\tt locus} is contained, and assign it to the period.
This simple rule works well for loci of 0 and 1, which are the
most common values.

\paragraph{Illustration of interpolation}
\index{aggregation!at conversion!example of use}

Table~\ref{table:time.conv-interpol} demonstrates aggregation and
disaggregation of a horizon of 3 periods, each consisting of 3 time
slots, for loci of 0, 1, and 0.5. The underlined values are the values
determined by the reverse conversion.

\begin{aimmstable}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
\hline\hline
& \multicolumn{9}{|c|}{{\bf Horizon data}} \\
\cline{2-10}
{\bf Locus} & \multicolumn{3}{|c|}{0} &
\multicolumn{3}{|c|}{3} & \multicolumn{3}{|c|}{9} \\
\hline
 0 & $\underline{0}$ & 1 & 2 & $\underline{3}$ & 5 & 7 & $\underline{9}$ & 9 & 9 \\
\hline
 1 & 0 & 0 & $\underline{0}$ & 1 & 2 & $\underline{3}$ & 5 & 7 & $\underline{9}$ \\
\hline
 0.5 & 0 & $\underline{0}$ & 1 & 2 & $\underline{3}$ & 5 & 7 & $\underline{9}$ & 9 \\
\hline\hline
\end{tabular}
\caption{Conversion rules for interpolated data}\label{table:time.conv-interpol}
\end{aimmstable}

\paragraph{Example}
\subtypindex{procedure}{Aggregate}{example of use}
\subtypindex{procedure}{Disaggregate}{example of use}

Consider the calendar {\tt DailyCalendar}, the horizon {\tt
ModelPeriods} and the time\-table {\tt TimeTable} declared in
Sections~\ref{sec:time.calendar}, \ref{sec:time.horizon}
and~\ref{sec:time.timetable}, along with the identifiers
\begin{itemize}
\item {\tt DailyDemand(d)},
\item {\tt Demand(h)},
\item {\tt DailyStock(d)}, and
\item {\tt Stock(h)}.
\end{itemize}
The aggregation of {\tt DailyDemand} to {\tt Demand} can then be
accomplished by the statement
\begin{example}
    Aggregate( DailyDemand, Demand, TimeTable, 'summation' );
\end{example}
Assuming that the {\tt Stock} is computed at the end of each period,
the disaggregation (by interpolation) to daily values is accomplished
by the statement
\begin{example}
    Disaggregate( Stock, DailyStock, TimeTable, 'interpolation', locus: 1 );
\end{example}

\paragraph{User-defined conversions}
\index{aggregation!user defined}

If your particular aggregation/disaggregation scheme is not covered by
the predefined aggregation types available in AIMMS, it is usually
not too difficult to implement a custom aggregation scheme yourself in
AIMMS. For instance, the aggregation by summation from {\tt
DailyDemand} to {\tt Demand} can be implemented as
\begin{example}
    Demand(h) := sum( d in TimeTable(h), DailyDemand(d) );
\end{example}
while the associated disaggregation rule becomes the statement
\begin{example}
    DailyDemand(d) := sum( h | d in TimeTable(h), Demand(h)/Card(TimeTable(per)) );
\end{example}


\section{Working in multiple time zones}\label{sec:time.tz}

\paragraph{Multiple time zones}

If your application uses external time-dependent data supplied by users worldwide, you are faced with the problem of converting all dates and times to the calendar in which your application is set up to run. The facilities described in this section help you with that task.

\paragraph{Obtaining time zone info}
\funcindex{TimeZoneOffSet}
\funcindex{DaylightSavingStartDate}
\funcindex{DaylightSavingEndDate}

With the functions {\tt TimeZoneOffset}, {\tt DaylightSavingStartDate} and {\tt DayLightSa\-vingEndDate} you can obtain various time zone specific characteristics.
\begin{itemize}
\item \verb|TimeZoneOffset|({\em FromTimeZone}, {\em ToTimeZone})
\item \verb|DaylightSavingStartDate|({\em year}, {\em TimeZone})
\item \verb|DaylightSavingEndDate|({\em year}, {\em TimeZone})
\end{itemize}
The function {\tt TimeZoneOffset} returns the offset in minutes between the time zones {\em FromTimeZone} and {\em ToTimeZone}. You can use the function {\tt Daylight\-Sa\-ving\-StartDate} and {\tt DaylightSavingEndDate} to retrieve the reference dates, within a particular time zone, corresponding to the begin and end date of daylight saving time.

\paragraph{Example}
\begin{example}
    offset    := TimeZoneOffset('UTC', 'Eastern Standard Time');
                 ! result: -300 [min]
    startdate := DaylightSavingStartDate( '2001', 'Eastern Standard Time');
                 ! result: "2001-04-01 02"
\end{example}

\paragraph{Converting reference dates}
\funcindex{ConvertReferenceDate}

With the function {\tt ConvertReferenceDate} you can convert reference dates to different time zones. Its syntax is:
\begin{itemize}
\item \verb|ConvertReferenceDate|({\em ReferenceDate}, {\em FromTimeZone}, {\em ToTimeZone}[, {\em IgnoreDST}])
\end{itemize}
With the function you can convert a reference date {\em ReferenceDate}, within the time zone {\em FromTimeZone}, to a reference date within the time zone {\em ToTimeZone}. If the optional argument {\em IgnoreDST} is set to 1, AIMMS will ignore daylight saving time for both input and output reference dates (see also Section~\ref{sec:time.format.dst}). By default, AIMMS will not ignore daylight saving time.

\paragraph{Example}
\begin{example}
    UTCdate := ConvertReferenceDate("2001-05-01 12", 'Local', 'UTC');
               ! result: "2001-05-01 11"
\end{example}

\paragraph{{\tt Conventions} and time zones}

When your application needs to read data from or write data to a database or file with dates not specified in local time, some kind of conversion of all dates appearing in the data source is required during the data transfer. To support you with such date conversions, AIMMS allows you to override the default timeslot format of one or more calendars in your model through a unit {\tt Convention} (see also Section~\ref{sec:units.convention}).

\paragraph{The {\tt TimeslotFormat} attribute}

In the {\tt TimeslotFormat} attribute of a {\tt Convention} you can specify the time slot format (see also Section~\ref{sec:time.format}) to be used for each {\tt Calendar} while communicating with an external source. The syntax of the {\tt TimeslotFormat} attribute is:

\paragraph{Syntax}
\begin{syntax}
\syntaxdiagram{timeslot-format-list}{ts-format-list}
\end{syntax}

\paragraph{Use in graphical user interface}

When an active {\tt Convention} overrides the time slot format for a particular calendar, all calendar elements will be displayed according to the time slot format specified in the {\tt Convention}. The time slot format specified in the calendar itself is then ignored.

\paragraph{Reading and writing to file}

If you use a {\tt Convention} to override the time slot format while writing data to a data file, report file or listing file, AIMMS will convert all calendar elements to the format specified in the {\tt TimeslotFormat} for that calendar in the {\tt Convention} prior to writing the data. Similarly, when reading data from a data file, AIMMS will interpret the calendar slots present in the file according to the specified time slot format, and convert them to the corresponding calendar elements in the model.

\paragraph{Database communication\dots}

When communicating calendar data to a database, there is one additional issue that you have to take into account, namely the data type of the column in which the calendar data is stored in the database table. If calendar data is stored in a string column, AIMMS will transfer data according to the exact date-time format specified in the {\tt TimeslotFormat} attribute of the {\tt Convention}, including any indicators for specifying the time zone and/or daylight saving time.

\paragraph{\dots\ for date- time columns}

However, if the data type of the column is a special date-time data type, AIMMS will always communicate calendar slots as reference dates, which is  the standard date-time string representation used by ODBC. In translating calendar slots to reference dates, AIMMS will adhere to the format specified in the {\tt TimeslotFormat} attribute of either the {\tt Calendar} itself, or as overridden in the currently active {\tt Convention}.

\paragraph{Generated reference dates}

The reference dates are generated according to the time zone specified in the \verb|%TZ| specifier of the currently active {\tt TIMESLOT SPECIFIER}.
These dates always ignore daylight saving time (i.e.\ shift back by one hour if necessary), as daylight saving time cannot be represented in the fixed reference date format. Specifiers in the {\tt TIMESLOT FORMAT} other than the \verb|%TZ| specifier are not used when mapping to date-time values in a database.
If you do not specify a \verb|%TZ| specifier in the {\tt TIMESLOT FORMAT},
AIMMS will assume that all date-time columns in a database are represented in the {\tt 'Local'} time zone (the default).

\paragraph{Example}

Consider the calendar {\tt HourCalendarLocalDST} defined below.
\begin{example}
Calendar HourCalendarLocalDST {
    Index           :  hd;
    Unit            :  hour;
    BeginDate       :  "2001-03-25 00";
    EndDate         :  "2001-03-25 06";
    TimeslotFormat  :  "%c%y-%m-%d %H:00%TZ('LocalDST')|\"\"|\" DST\"|";
}
\end{example}
If you want to transfer data defined over this calendar with a database table in which all dates are represented in UTC time, you should define a convention defined as follows.
\pagebreak
\begin{example}
Convention UTCConvention {
    TimeslotFormat  : {
        HourCalendarLocalDST : "%c%y-%m-%d %H:00%TZ('UTC')"
    }
}
\end{example}
When this convention is active, AIMMS will represent all calendar slots of the calendar {\tt HourCalendar\-LocalDST} as reference dates according to the UTC time zone, by shifting as many hours as dictated by the local time zone and/or daylight saving time if applicable.
Hence, when you use the convention {\tt UTCConven\-tion} during data transfer with the database, all calendar data slots will be in the expected format.

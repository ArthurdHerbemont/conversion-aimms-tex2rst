\section{Implementing a model with a rolling horizon}\label{sec:time.rolling}

\paragraph{Rolling horizons}
\index{rolling horizon}
\index{horizon!rolling}

The term {\em rolling horizon} is used to indicate that a
time-dependent model is solved repeatedly, and in which the planning
interval is moved forward in time during each solution step. With the
facilities introduced in the previous sections setting up such a model
is relatively easy. This section outlines the steps that are required
to implement a model with a rolling horizon, without going into detail
regarding the contents of the underlying model.

\paragraph{Two strategies}

In this section you will find two strategies for implementing a
rolling horizon. One is a simple strategy that will only work with
certain restrictions. It requires just a single aggregation step and a
single disaggregation step. The other is a generic strategy that will
work in all cases. This strategy, however, requires that aggregation
and disaggregation steps be performed between every two subsequent
{\tt SOLVE} statements.

\paragraph{Simple strategy}
\index{rolling horizon!simple strategy}

The simple strategy will work provided that
\begin{itemize}
\item all periods in the horizon are of equal length, and
\item the horizon rolls from period boundary to period boundary.
\end{itemize}
It is then sufficient to make the horizon sufficiently large so as to
cover the whole time range of interest.

\paragraph{Algorithm outline}

The algorithm to implement the rolling horizon can be outlined as
follows.
\begin{enumerate}
\item Select the current time slot and period, and create the
global timetable.
\item Aggregate all calendar-based data into horizon-based data.
\item Solve the optimization model for a planning interval that is
 a subset of the complete horizon.
\item Move the current period to the next period boundary of interest,
and repeat from steps until the time range of interest has passed.
\item Disaggregate the horizon-based solution into a calendar-based
solution.
\end{enumerate}

\paragraph{Assumptions}

The examples below that illustrate both the simple and generic
strategy make the following assumptions.
\begin{itemize}
\item The model contains the daily  calendar {\tt DailyCalendar},
the horizon {\tt Model\-Periods} and the timetable {\tt TimeTable}
declared in Sections~\ref{sec:time.calendar},
\ref{sec:time.horizon} and \ref{sec:time.timetable}, respectively.
\item The model contains a time-dependent mathematical program {\tt
TimeDe\-pendentModel}, which produces a plan over the planning
interval associated with {\tt ModelPeriods}.
\item The planning horizon, for which the model is to be solved, rolls along
from {\tt FirstWeekBegin} to {\tt LastWeekBegin} in steps of one week.
Both identifiers are element parameters in {\tt DailyCalendar}.
\end{itemize}

\paragraph{Code outline}

The outline of the simple strategy can be implemented as follows.
\begin{example}
    CurrentDay := FirstWeekBegin;
    CreateTimeTable( TimeTable   , CurrentDay     , IntervalStart,
                     PeriodLength, LengthDominates,
                     InactiveDays, DelimiterDays  );

    Aggregate( DailyDemand, Demand, TimeTable, 'summation' );
    ! ... along with any other aggregation required

    repeat
        solve TimeDependentModel;

        CurrentDay    += 7;
        IntervalStart += 1;

        break when (not CurrentDay) or (CurrentDay > LastWeekBegin);
    endrepeat;

    Disaggregate( Stock     , DailyStock     , TimeTable, 'interpolation', locus: 1 );
    Disaggregate( Production, DailyProduction, TimeTable, 'summation' );
    ! ... along with any other disaggregation required
\end{example}

\paragraph{Generic strategy}
\index{rolling horizon!generic strategy}

The simple strategy will not work
\begin{itemize}
\item whenever the lengths of periods in the horizon (expressed in
time slots of the calendar) vary, or
\item when the start of a new planning interval does not align
with a future model period.
\end{itemize}
In both cases, the horizon-based solution obtained from a previous
solve will not be accurate when you move the planning interval. Thus,
you should follow a generic strategy which adds an additional
disaggregation and aggregation step to every iteration.

\paragraph{Algorithm outline}

The generic strategy for implementing a rolling horizon is outlined as
follows.
\begin{enumerate}
\item Select the initial current time slot and period, and create the
initial time\-table.
\item Aggregate all calendar-based data into horizon-based data.
\item Solve the mathematical program.
\item Disaggregate all horizon-based variables to calendar-based
identifiers.
\item Move the current time slot forward in time, and recreate
the timetable.
\item Aggregate all identifiers disaggregated in step 4 back to the
horizon using the updated timetable.
\item Repeat from step 2 until the time range of interest has passed.
\end{enumerate}

\paragraph{Code outline}

The outline of the generic strategy can be implemented as follows.
\begin{example}
    CurrentDay := FirstWeekBegin;
    CreateTimeTable( TimeTable   , CurrentDay     , IntervalStart,
                     PeriodLength, LengthDominates,
                     InactiveDays, DelimiterDays  );

    repeat
        Aggregate( DailyDemand, Demand, TimeTable, 'summation' );
        ! ... along with any other aggregation required

        solve TimeDependentModel;

        Disaggregate( Stock     , DailyStock     , TimeTable, 'interpolation', locus: 1 );
        Disaggregate( Production, DailyProduction, TimeTable, 'summation' );
        ! ... along with any other disaggregation required

        CurrentDay += 7;
        break when (not CurrentDay) or (CurrentDay > LastWeekBegin);
        CreateTimeTable( TimeTable   , CurrentDay     , IntervalStart,
                         PeriodLength, LengthDominates,
                         InactiveDays, DelimiterDays  );

        Aggregate( DailyStock     , Stock     , TimeTable, 'interpolation', locus: 1 );
        Aggregate( DailyProduction, Production, TimeTable, 'summation' );
        ! ... along with any other aggregation required
    endrepeat;
\end{example}


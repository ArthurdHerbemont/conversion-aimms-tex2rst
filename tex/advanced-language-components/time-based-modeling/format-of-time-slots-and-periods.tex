\section{Format of time slots and periods}\label{sec:time.format}

\paragraph{Flexible time slot and period formats}
\index{format!period}
\index{period format}
\index{time slot format}

\syntaxmark{timeslot-format}
While the {\tt BeginDate} and {\tt EndDate} attributes have to be
specified using the fixed {\em reference date} format (see
Section~\ref{sec:time.calendar}), AIMMS provides much more flexible
formatting capabilities to describe
\begin{itemize}
\item time slots in a {\tt Calendar} consisting of a single basic time unit
(e.g.\ 1-day time slots),
\item time slots in a {\tt Calendar} consisting of multiple basic time units
(e.g.\ 3-day time slots), and
\item periods in a timetable consisting of multiple time slots.
\end{itemize}
The formatting capabilities described in this section are quite
extensive, and allow for maximum flexibility.

\paragraph{Wizard support}
\index{period format!wizard}

In the Model Explorer, AIMMS provides a wizard to support you in
constructing the appropriate formats. Through this wizard, you can not
only select from a number of predefined formats (including some that
use the regional settings of your computer), you also have the
possibility of constructing a custom format, observing the result as
you proceed.

\paragraph{Basic and extended format}

AIMMS offers both a {\em basic} and an {\em extended format} for
the description of time slots and periods. The basic format only
refers to the beginning of a time slot or period. The extended format
allows you to refer to both the first and last basic time unit
contained in a time slot or period. Both the basic and extended
formats are constructed according to the same rules.

\paragraph{Care is needed}
\declattrindex{calendar}{TimeslotFormat}

The {\tt TimeslotFormat} used in a {\tt Calendar} must contain a
reference to either its beginning, its end, or both. As the specified
format is used to identify calendar elements when reading data from
external data sources such as files and databases, you have to ensure
that the specified format contains sufficient date and time references
to uniquely identify each time slot in a calendar.

\paragraph{Example}

For instance, the description ``{\tt January 1}'' is sufficient to
uniquely identify a time slot in a calendar with a range of one year.
However, in a two-year calendar, corresponding days in the first and
second year are identified using exactly the same element description.
In such a case, you must make sure that the specified format contains
a reference to a year.

\paragraph{Building blocks}

A format description is a sequence of four types of components.
These are
\begin{itemize}
\item predefined date components,
\item predefined time components,
\item predefined period references (extended format), and
\item ordinary characters.
\end{itemize}

\paragraph{Ordinary characters}

Predefined components begin with the \verb|%| sign. Components that
begin otherwise are interpreted as ordinary characters. To use a
percent sign as an ordinary character, escape it with another percent
sign, as in \verb|%%|.

\subsection{Date-specific components}\label{sec:time.format.date}

\paragraph{Date-specific components}
\index{period format!date-specific component}
\index{conversion specifier!date-specific}

The date-specific components act as conversion specifiers to denote
portions of a date description. They may seem rather cryptic at first,
but you will find them useful and constructive when creating
customized references to time. They are summarized in
Table~\ref{table:time.cal-date}.
\begin{aimmstable}
\begin{tabular}{|l|l|l|}
\hline
\hline
{\bf Conversion}& {\bf Meaning} & {\bf Possible}\\ {\bf specifier} & &
{\bf entries}\\
\hline
\verb|%d|       & day          &$01,\dots,31$\\
\verb|%m|       & month        &$01,\dots,12$\\
\verb/%Am|/{\em set-identifier}\verb/|/ & month & {\em element}\\
\verb|%y|       & year         &$00,\dots,99$\\
\verb|%q|       & quarter      &$01,\dots,04$\\
\verb|%Y|       & weekyear     &$00,\dots,99$\\
\verb|%c|       & century      &$00,\dots,99$\\
\verb|%C|       & weekcentury  &$00,\dots,99$\\
\hline
\verb/%w/       & day of week       &$1,\dots,7$\\
\verb/%Aw|/{\em set-identifier}\verb/|/ & day of week & {\em element}\\
\verb/%W/       & week of year & $01,\dots, 53$\\
\verb|%j|       & day of year  &$001,\dots,366$\\
\hline\hline
\end{tabular}
\caption{Conversion specifiers for date components}\label{table:time.cal-date}
\end{aimmstable}

\paragraph{Custom date-specific references}

All date conversion specifiers allow only predefined numerical values,
except for the specifiers \verb|%Am| and \verb|%Aw|. These allow you
to specify references to sets. You can use \verb|%Am| and
\verb|%Aw| to denote months and days by the elements in a specified set.
These are typically the names of the months or days in your native
language. AIMMS will interpret the elements by their ordinal
number. The predefined identifiers {\tt AllMonths}, {\tt
AllAbbrMonths}, {\tt AllWeekdays} and {\tt AllAbbrWeekdays} hold the
full and abbreviated English names of both months and days.

\paragraph{Week year and century}

The \verb|%Y| and \verb|%C| specifiers refer to the weekyear and
weekcentury values of a specific date, as explained on page
\pageref{time:weeknumbering}. You can use these if you want to
refer to weekly calendar periods by their week number and year.

\paragraph{Omitting leading zeros}

AIMMS can interpret numerical date-specific references with or
without leading zeros when reading your input data. When writing data,
AIMMS will insert all leading zeros to ensure a uniform length for
date elements. If you do not want leading zeros for a specific
component, you can insert the '\verb|s|' modifier directly after the
\verb|%| sign. For instance, the string ``\verb|%sd|'' will direct
AIMMS to produce single-digit numbers for the first nine days.

\paragraph{Omitting trailing blanks}

When using the \verb|%Am| and \verb|%Aw| specifiers, AIMMS will
generate uniform length elements by adding sufficient trailing blanks
to the shorter elements. As with leading zeros, you can use the {\tt
s} modifier to override the generation of these trailing blanks.

\paragraph{Example}

The format ``\verb!%Am|AllMonths| %sd, %c%y!'' will result in the
generation of time slots such as {\tt 'January 1, 1996'}. The date
portion of the fixed reference date format used to specify the {\tt
Begin} and {\tt EndDate} attributes of a calendar can be reproduced
using the format ``\verb|%c%y-%m-%d|''.

\subsection{Time-specific components}\label{sec:time.format.time}

\paragraph{Time-specific components}
\index{period format!time-specific component}
\index{conversion specifier!time-specific}

The conversion specifiers for time components are listed in
Table~\ref{table:time.cal-time}. There are no custom time-specific
references in this table, because the predefined numerical values are
standard throughout the world.

\begin{aimmstable}
\begin{tabular}{|l|l|l|}
\hline
\hline
{\bf Conversion}& {\bf Meaning} & {\bf Possible}\\
{\bf specifier} &               & {\bf entries}\\
\hline
\verb|%h|       & hour          & $01,\dots,12$\\
\verb|%H|       & hour          & $00,\dots,23$\\
\verb|%M|       & minute        & $00,\dots,59$\\
\verb|%S|       & second        & $00,\dots,59$\\
\verb|%t|       & tick          & $00,\dots,99$\\
\verb|%p|       & before or after noon& AM, PM \\
\hline
\hline
\end{tabular}
\caption{Conversion specifiers for time components}\label{table:time.cal-time}
\end{aimmstable}

\paragraph{Omitting leading zeros}

AIMMS can interpret numerical time-specific references with or
without leading zeros when reading your input data. When writing data,
AIMMS will insert leading zeros to ensure a uniform length for time
elements. If you do not want leading zeros for a specific component,
you can insert the '\verb|s|' modifier directly after the
\verb|%| sign. For instance, the string ``\verb|%sh|'' will direct
AIMMS to produce single-digit numbers for the first nine hours.

\paragraph{Example}

The time slot format ``\verb!%sAw|WeekDays| %sh:%M %p!'' will result
in the generation of time slots such as {\tt 'Friday 11:00 PM'}, {\tt
'Friday 12:00 PM'} and {\tt 'Saturday 1:00 AM'}. The full reference
date format is given by ``\verb|%c%y-%m-%d %H:%M:%S|''.

\subsection{Period-specific components}\label{sec:time.format.period}

\paragraph{Use of period references}
\index{period format!period-specific component}
\index{conversion specifier!period-specific}
\funcindex{PeriodToString}

With period-specific conversion specifiers in either a time slot
format or a period format you can indicate that you want AIMMS to
display both the begin and end date/time of a time slot or period. You
only need to use period-specific references in the following cases.
\begin{itemize}
\item The {\tt Unit} attribute of your calendar consists of a multiple
of one of the basic time units known to AIMMS (e.g.\ each time slot
in your calendar consists of 3 days), and you want to refer to the
begin and end day of every time slot.
\item You want to provide a description for a period in a timetable
consisting of multiple time slots in the associated calendar using the
function {\tt PeriodToString} (see also Section~\ref{sec:time.convert}),
referring to both the first and last time slot in the period.
\end{itemize}

\paragraph{Period-specific components}

By including a period-specific component in a time slot or period
format, you indicate to AIMMS that any date, or time, specific
component following it refers to either the beginning or the end of a
time slot or period. The list of available period-specific conversion
specifiers is given in Table~\ref{table:time.cal-per-ref}.
\begin{aimmstable}
\begin{tabular}{|l|p{6cm}|}
\hline
\hline
{\bf Conversion specifier}& {\bf Meaning}\\
\hline
\verb/%B/ & begin of unit period \\
\verb/%b/ & begin of time slot \\
\verb/%I/ & end of period (inclusive)\\
\verb/%i/ & end of period (inclusive), but
omitted when equal to begin of period\\
\verb/%E/ & end of period (exclusive)\\
\verb/%e/ & end of time slot \\
%\verb/%AP/{\em set-identifier} & period given by AIMMS element\\
\hline
\hline
\end{tabular}
\caption{Period-specific conversion specifiers.}\label{table:time.cal-per-ref}
\end{aimmstable}

\paragraph{Inclusive or exclusive}
\index{period format!inclusive versus exclusive}

Through the ``\verb|%I|'' and ``\verb|%E|'' specifiers you can
indicate whether you want any date/ time components used in the
description of the end of a period (or time slot) to be included in
that period or excluded from it. Inclusive behavior is common for date
references, e.g.\ the description ``Monday -- Wednesday'' usually
means the period consisting of Monday, Tuesday {\em and} Wednesday.
For time references exclusive behavior is used most commonly, i.e.\
``1:00 -- 3:00 PM'' usually means the period from 1:00 PM {\em until}
3:00 PM.

\paragraph{Limited resolution}

After a conversion specifier that refers to the end of a period or
time slot (i.e. ``\verb|%E|'', ``\verb|%I|'' or ``\verb|%i|'') you
should take care when using other date, or time, specific specifiers.
AIMMS will only be able to discern time units that are larger than
the basic time unit specified in the {\tt Unit} attribute of the
calendar at hand (or, when you use the function {\tt PeriodToString},
of the calendar associated with the timetable at hand). For instance,
when the time slots of a calendar consists of periods of 2 months,
AIMMS will be able to distinguish the specific months at the
beginning and end of each time slot, but will not know the specific
week number, week day or month day at the end of each time slot. Thus,
in this case you should avoid the use of the ``\verb|%W|'', the
``\verb|%w|'' and the ``\verb|%d|'' specifiers after a ``\verb|%E|'',
``\verb|%I|'' or ``\verb|%i|'' specifier.

\paragraph{The {\tt \%i} specifier}

With the ``\verb|%i|'' specifier you indicate inclusive behavior, and
additionally you indicate that AIMMS must omit the remaining text
when the basic time units (w.r.t.\ the underlying calendar) of begin
and end slot of the period to which the specifier is applied,
coincide. In practice, the ``\verb|%i|'' specifier only makes sense
when used in the function {\tt PeriodToString} (see also
Section~\ref{sec:time.convert}), as time slots in a calendar always
have a fixed length.

\paragraph{First example}

The period description ``\verb|Monday 12:00-15:00|'' contains three
logical references, namely to a day, to the begin time in hours, and
to the end time in hours. The day reference is intended to be shared
by the begin and end times.
\begin{itemize}
\item The day reference is based on the elements of the (predefined)
set {\tt All\-Weekdays}. The corresponding conversion specifier is
``\verb/%Aw|AllWeekDays|/''
\item
The descriptions of the begin and end times both use the conversion
specifier ``\verb|%H:%M|''. To denote the begin time of the period you
must use the ``\verb|%B|'' period reference. For the end time of the
period, which is not included in the period, you must
use~``\verb|%E|''.
\end{itemize}
By combining these building blocks with a few ordinary characters you
get the complete format string
``\verb/%Aw|AllWeekDays| %B%H:%M-%E%H:%M/''.
With this string AIMMS can correctly interpret
the element ``\verb|Monday 12:00-15:00|'' within a calendar covering
no more than one week.

\paragraph{Second example}

Consider the format ``\verb/%B%Aw|AllWeekDays|%I/ \verb|-|
\verb/%Aw|AllWeekDays|/'' within a calendar with \verb|day| as its basic
time unit, and covering at most a week. Using this format string
AIMMS will interpret the element ``\verb|Monday - Wednesday|'' as
the three-day period consisting of Monday, Tuesday, and Wednesday.

\subsection{Support for time zones and daylight saving time}\label{sec:time.format.dst}

\paragraph{Support for day- light~saving~time}

When your time zone has daylight saving time, and you are working with time slots or periods on an hourly basis,
you may want to include an indicator into the time slot or period format to indicate whether daylight saving time is active during a particular time slot or period. Such an indicator enables you, for instance, to distinguish between the duplicate hour when the clock is set back at the end of daylight saving time.

\paragraph{Support for time zones}

In addition, when your application has users located in different time zones, you may wish to present each user with calendar elements corresponding to their particular time zone. Or, when time-dependent data is stored in a database using UTC time (Universal Time Coordinate, or Greenwich Mean Time), a translation may be required to your own local time representation.

\paragraph{AIMMS support}

To support you in scenarios as described above, AIMMS provides
\begin{itemize}
\item a special time zone conversion specifier, which can modify the representation of calendar elements based on specified time zone and daylight saving time, and
\item a {\tt TimeslotFormat} attribute in unit {\tt Conventions} (see also Section~\ref{sec:units.convention}), which you can use to override the time slot format of every calendar when the {\tt Convention} is active.
\end{itemize}

\paragraph{Time zone specifier}

With the conversion specifier \verb/%TZ/, described in Table~\ref{table:time.tz},
you can accomplish the following {\tt Calendar}-related tasks:
\begin{itemize}
\item create the {\tt Calendar} elements between the given {\tt BeginDate} and {\tt EndDate} relative to a specified time zone, and
\item specify the indicators that must be added to the {\tt Calendar} elements when standard or daylight saving time is active.
\end{itemize}
\begin{aimmstable}
\begin{tabular}{|l|p{6cm}|}
\hline
\hline
{\bf Conversion specifier}& {\bf Meaning}\\
\hline
\verb/%TZ(/{\em TimeZone}\verb/)/ &
translation of calendar element to spe\-cified {\em TimeZone}, ignoring daylight saving time\\
\verb/%TZ(/{\em TimeZone}\verb/)|/{\em Std}\verb/|/{\em Dst}\verb/|/ &
translation of calendar element to spe\-cified {\em TimeZone}, plus string indicator for standard time ({\em Std}) and daylight saving time ({\em Dst})\\
\hline
\hline
\end{tabular}
\caption{Time zone conversion specifier}\label{table:time.tz}
\end{aimmstable}

\paragraph{Specifying the time zone}

The {\em TimeZone} component of the \verb|%TZ| conversion specifier that you must specify, is a time zone corresponding to the elements in your {\tt Calendar}.
You must specify the time zone as an explicit and quoted element of the predefined set {\tt AllTimeZones} (explained below), or through a reference to an element parameter into that set. If you do not specify the {\em Std} and {\em Dst} indicators, AIMMS will ignore daylight saving time when generating the time slots, regardless whether daylight saving time is defined for that time zone.
If you do not use the \verb|%TZ| specifier to specify a time zone,
AIMMS assumes that you intend to use the local time zone without daylight saving time.

\paragraph{The set {\tt AllTimeZones}}
\presetindex{AllTimeZones}

AIMMS provides you access to all time zones defined by your operating system through the predefined set {\tt AllTimeZones}.
The set {\tt AllTimeZones} contains
\begin{itemize}
\item the fixed element {\tt 'Local'}, representing the local time zone without daylight saving time,
\item the fixed element {\tt 'LocalDST'}, representing the local time zone with daylight saving time (if applicable),
\item the fixed element {\tt 'UTC'}, representing the Universal Time Coordinate (or Greenwich Mean Time) time zone, and
\item all time zones defined by your operating system.
\end{itemize}

\paragraph{Daylight saving time indicators}

The remaining components of the \verb|%TZ|
specifier are two string indicators {\em Std} and {\em Dst}, which are displayed in all generated {\tt Calendar} slots or period strings when standard time (i.e.\ no daylight saving time) or daylight saving time is active, respectively. Both indicators must be either quoted strings, or references to scalar string parameters. In addition, a run time error will occur when both indicators evaluate to the same string.

\paragraph{Effect on time slots}

When you use the \verb|%TZ| specifier,
the date and time components of the generated time slots of a calendar may differ when you specify different time zones, but do not modify the reference dates specified in the {\tt BeginDate} and {\tt EndDate} attributes of the calendar. AIMMS always assumes that reference dates are specified in local time without daylight saving time (i.e.\ in the {\tt 'Local'} time zone). Hence, all time slots will be shifted by the time differences between the specified time zone and the {\tt 'Local'} time zone, plus any additional difference caused by daylight saving time.

\paragraph{Examples}

Consider the following four {\tt Calendar} declarations.
\begin{example}
Calendar HourCalendarLocal {
    Index           :  hl;
    Unit            :  hour;
    BeginDate       :  "2001-03-25 00";
    EndDate         :  "2001-03-25 06";
    TimeslotFormat  :  "%c%y-%m-%d %H:00";
}
Calendar HourCalendarLocalIgnore {
    Index           :  hi;
    Unit            :  hour;
    BeginDate       :  "2001-03-25 00";
    EndDate         :  "2001-03-25 06";
    TimeslotFormat  :  "%c%y-%m-%d %H:00%TZ('LocalDST')";
}
Calendar HourCalendarLocalDST {
    Index           :  hd;
    Unit            :  hour;
    BeginDate       :  "2001-03-25 00";
    EndDate         :  "2001-03-25 06";
    TimeslotFormat  :  "%c%y-%m-%d %H:00%TZ('LocalDST')|\"\"|\" DST\"|";
}
Calendar HourCalendarUTC {
    Index           :  hc;
    Unit            :  hour;
    BeginDate       :  "2001-03-25 00";
    EndDate         :  "2001-03-25 06";
    TimeslotFormat  :  "%c%y-%m-%d %H:00%TZ('UTC')|\"\"|\" DST\"|";
}
\end{example}
Assuming that the {\tt 'Local'} time zone has an offset of $+1$ hours compared to the {\tt 'UTC'} time zone, this will result in the generation of the following time slots for each of the calendars
\begin{example}
!   HourCalendarLocal    HourCalendarIgnore   HourCalendarLocalDST     HourCalendarUTC
!   ------------------   ------------------   ----------------------   ------------------
    '2001-03-25 00:00'   '2001-03-25 00:00'   '2001-03-25 00:00'       '2001-03-24 23:00'
    '2001-03-25 01:00'   '2001-03-25 01:00'   '2001-03-25 01:00'       '2001-03-25 00:00'
    '2001-03-25 02:00'   '2001-03-25 02:00'   '2001-03-25 03:00 DST'   '2001-03-25 01:00'
    '2001-03-25 03:00'   '2001-03-25 03:00'   '2001-03-25 04:00 DST'   '2001-03-25 02:00'
    '2001-03-25 04:00'   '2001-03-25 04:00'   '2001-03-25 05:00 DST'   '2001-03-25 03:00'
    '2001-03-25 05:00'   '2001-03-25 05:00'   '2001-03-25 06:00 DST'   '2001-03-25 04:00'
    '2001-03-25 06:00'   '2001-03-25 06:00'   '2001-03-25 07:00 DST'   '2001-03-25 05:00'
\end{example}
Note that the time slots generated for {\tt HourCalendarLocal} and {\tt HourCalendar\-Ignore} are identical (although {\tt 'LocalDST'} supports daylight saving time). This is because daylight saving time is ignored when the \verb|%TZ| specifier has no {\em Std} and {\em Dst} indicators. The time slots generated for {\tt HourCalendarUTC} do not contain the specified daylight saving time indicator, because the {\tt 'UTC'} time zone has no daylight saving time.


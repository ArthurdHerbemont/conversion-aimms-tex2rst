\section{Working with elapsed time}\label{sec:time.continuous}

\paragraph{Use of elapsed time}
\index{elapsed time}

Sometimes you may find it easier to formulate your model in terms of
(continuous) elapsed time with respect to some reference date rather
than in terms of discrete time periods. For example, for a task in a
schedule it is often more natural to store just the start and end time
rather than to specify all of the time slots in a calendar during
which the task will be executed. In addition, working with elapsed
time allows you to store time references to any desired accuracy.

\paragraph{Input-output conversion}

For data entry or for the generation of reports, however, elapsed time
may not be your preferred format. In this event AIMMS offers a
number of functions for the conversion of elapsed time to calendar
strings (or set elements) and vice versa, using the conversion
specifiers described in section~\ref{sec:time.format}.

\paragraph{Conversion to calendar elements}
\index{elapsed time!convert to time slot}
\index{time slot!convert to elapsed time}
\funcindex{MomentToTimeSlot}
\funcindex{TimeSlotToMoment}

The following functions allow conversion between elapsed time and time
slots in an existing calendar. Their syntax is
\begin{itemize}
\item \verb|MomentToTimeSlot|({\em calendar, reference-date, elapsed-time\/})
\item \verb|TimeSlotToMoment|({\em calendar, reference-date, time-slot\/}).
\end{itemize}
The {\em reference-date} argument must be a time slot in the specified
calendar. The {\em elapsed-time} argument is the elapsed time from the
{\em reference-date} measured in terms of the calendar's unit. The
result of the function \verb|MomentToTimeSlot| is the time slot
containing the moment represented by the reference date plus the
elapsed time. The result of the function \verb|TimeSlotToMoment| is
the elapsed time from the reference date to the value of the {\em
time-slot} argument (measured in the calendar's unit).

\paragraph{Conversion to calendar strings}
\index{elapsed time!convert to string}
\index{string!convert to elapsed time}
\funcindex{MomentToString}
\funcindex{StringToMoment}

The following functions enable conversion between elapsed time and
free format strings. Their syntax is
\begin{itemize}
\item \verb|MomentToString|({\em format-string, unit, reference-date, elapsed-time\/})
\item \verb|StringToMoment|({\em format-string, unit, reference-date, moment-string\/}).
\end{itemize}
The {\em reference-date} argument must be provided in the fixed format
for reference dates, as described in Section~\ref{sec:time.calendar}.
The {\em moment-string} argument must be a period in the format given
by {\em format-string}. The {\em elapsed-time} argument is the elapsed
time in {\em unit} with respect to the {\em reference-date} argument.
The result of the function \verb|MomentToString| is a description of
the corresponding moment according to {\em format-string}. Strictly spoken, the {\em unit} argument in {\tt MomentToString} is only required when the option {\tt elapsed\_time\_is\_unitless} (see below) is set to {\tt on}, and, consequently, {\em elapsed-time} is unitless. In the case that {\tt elapsed\_time\_is\_unitless} is set to {\tt off} (the default), you are advised to set the {\em unit} argument equal to the associated unit of the {\em elapsed-time} argument.
The result of the function \verb|StringToMoment| is the elapsed time in {\em unit} between
{\em reference-date} and {\em moment-string}.

\paragraph{Example}
\begin{example}
    moment := MomentToString("%c%y-%Am|AllAbbrMonths|-%d (%sAw|AllWeekdays|) %H:%M",
                             [hour], "1996-01-01 14:00", 2.2 [hour] );
              ! result : "1996-Jan-01 (Monday) 16:12"

    elapsed := StringToMoment("%c%y-%Am|AllAbbrMonths|-%d (%sAw|AllWeekdays|) %H:%M",
                              [hour], "1996-01-01 14:00", "1996-Jan-01 (Monday) 16:12" );
              ! result : 2.2 [hour]
\end{example}

\paragraph{Obtaining the current time}
\index{current time!convert to elapsed time}
\funcindex{CurrentToMoment}

The function {\tt CurrentToMoment} can be used to obtain the elapsed
time since {\em reference-date} in the specified {\em unit} of the
current time. Its syntax is
\begin{itemize}
\item \verb|CurrentToMoment|({\em unit}, {\em reference-date}).
\end{itemize}

\paragraph{Unitless result}

By default, the result of the functions {\tt TimeSlotToMoment}, {\tt StringToMoment} and {\tt CurrentToMoment} will have an associated unit, namely the unit specified in the {\em unit} argument. In addition, AIMMS expects the {\em elapsed-time} argument in the function {\tt MomentToTimeSlot} and {\tt MomentToString} to be of the same unit as its associated {\em unit} argument. If you want the result or arguments of these functions to be unitless, you can accomplish this by setting the compile time option {\tt elapsed\_time\_is\_unitless} to {\tt on}. Note, however, that any change to this option affects all calls to these function throughout your model.


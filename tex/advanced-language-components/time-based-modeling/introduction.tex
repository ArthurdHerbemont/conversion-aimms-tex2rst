\section{Introduction}\label{sec:time.intro}

\paragraph{Time and models}

Time plays an important role in various real-life modeling
applications. Typical examples are found in the areas of planning,
scheduling, and control. The time scale in control models is typically
seconds and minutes. Scheduling models typically refer to hours and
days, while the associated time unit in planning models is usually
expressed in terms of weeks, months, or even years. To facilitate
time-based modeling, AIMMS provides a number of tools to relate model
time and calendar time.

\paragraph{Use of time periods}

Time-dependent data in a model is usually associated with time
periods. Some data items associated with a period index can be
interpreted as taking place {\em during} the period, while others take
place {\em at a particular moment}. For instance, the stock in a tank
is usually measured at, and associated with, a specific moment in a
period, while the flow of material into the tank is usually associated
with the entire period.

\paragraph{Use of time as a continuous quantity}

Time-dependent data in a model can also represent continuous time
values. For instance, consider a parameter containing the starting
times of a number of processes. Even though this representation is not
ideal for constructing most time-based optimization models, it allows
time to be expressed to any desired accuracy.

\paragraph{Calendar periods versus model periods}

A large portion of the data in time-dependent models originates from
the real world where quantities are specified relative to some
calendar. Optimization models usually refer to abstract model periods
such as $p_1$, $p_2$, $p_3$, etc., allowing the optimization model to
be formulated independent of real time. This common distinction makes
it essential that quantities associated with real calendar time can be
converted to quantities associated with model periods and vice versa.

\paragraph{Rolling horizon}
\index{rolling horizon}

In many planning and scheduling applications, time-dependent models
are solved repeatedly as time passes. Future data becomes present data
and eventually becomes past data. Such a moving time span is usually
referred to as a ``{\em rolling horizon}''. By using the various
features discussed in this chapter, it is fairly straightforward to
implement models with a rolling horizon.

\paragraph{{\tt Calendars} and {\tt Horizons}}
\typindex{identifier}{Calendar}
\typindex{identifier}{Horizon}
\index{time-based modeling!HORIZON@{\tt Horizon}}
\index{time-based modeling!CALENDAR@{\tt Calendar}}

AIMMS offers two special data types for time-based modeling
applications, namely {\tt Calendar} and {\tt Horizon}. Both are index
sets with special features for dealing with time. {\tt Calendars}
allow you to create a set of time slots of fixed length in real time,
while {\tt Horizons} enable you to distinguish past, planning and
beyond periods in your model.

\paragraph{Timetables}
\index{timetable}
\index{time-based modeling!timetable}

In addition, AIMMS offers support for automatically creating {\em
timetables} (represented through indexed sets) which link model
periods in a {\tt Horizon} to time slots in a {\tt Calendar} in a
flexible manner. Based on a timetable, AIMMS provides functions to
let you {\em aggregate} data defined over a {\tt Calendar} to data
defined over the corresponding {\tt Horizon} and vice versa.
Figure~\ref{fig:time.timetable} illustrates an example of a timetable
relating a horizon and a calendar.

\begin{aimmsfigure}
\psset{xunit=0.5cm,yunit=0.6cm,dimen=middle,linewidth=0.6pt}
\begin{center}
\begin{pspicture}(20,-1.75)(33,8.0)
   \psframe[fillstyle=solid,fillcolor=gray](23,0)(24,1)
   \multips(18,0)(1,0){14}{\pscustom{\psline(0,-0)(0,1)(1,1)(1,0)(0,0)}}
   \psline[linestyle=dashed](16,0)(18,0)
   \psline[linestyle=dashed](16,1)(18,1)
   \psline[linestyle=dashed](32,0)(34,0)
   \psline[linestyle=dashed](32,1)(34,1)
%   \psframe[linewidth=1.2pt](19,-0.25)(31,1.25)
   \rput(25.0,-1.75){calendar (divided into time slots)}
   \psframe(19,4)(21,5)
%   \psline[linestyle=dashed,dash=3pt 4pt](20,4)(20,5)
   \psframe(21,4)(23,5)
%   \psline[linestyle=dashed,dash=3pt 4pt](22,4)(22,5)
   \psframe[fillstyle=solid,fillcolor=gray](23,4)(25,5)
%   \psframe(24,4)(25,5)
   \psframe(25,4)(28,5)
%   \psline[linestyle=dashed,dash=3pt 4pt](26,4)(26,5)
%   \psline[linestyle=dashed,dash=3pt 4pt](27,4)(27,5)
   \psframe(28,4)(31,5)
%   \psline[linestyle=dashed,dash=3pt 4pt](29,4)(29,5)
%   \psline[linestyle=dashed,dash=3pt 4pt](30,4)(30,5)
   \rput(25.0,7.5){horizon (divided into periods)}
   \psline[linestyle=dashed,dash=3pt 2pt](19,1)(19,4)
   \psline[linestyle=dashed,dash=3pt 2pt](21,1)(21,4)
   \psline(23,1)(23,4)
%   \psline(24,1)(24,4)
   \psline[linestyle=dashed,dash=3pt 2pt](25,1)(25,4)
   \psline[linestyle=dashed,dash=3pt 2pt](28,1)(28,4)
   \psline[linestyle=dashed,dash=3pt 2pt](31,1)(31,4)
   \psline{->}(23.5,-1.25)(23.5,-0.35)
   \rput[l](24.0,-0.75){\footnotesize current date}
   \psline{<-}(23.5,6.0)(23.5,7.0)
   \rput[l](24.0,6.5){\footnotesize current period}
   \psline[linewidth=1.0pt]{<->}(31.5,1.5)(31.5,3.5)
   \rput[l](32.0,2.5){\footnotesize conversion rules}
   \rput[t](20,5.25){\rotateleft{\scaleboxto(0.15,1.0){\Bigg\}}}}
   \rput[t](22,5.25){\rotateleft{\scaleboxto(0.15,1.0){\Bigg\}}}}
   \rput[t](24,5.25){\rotateleft{\scaleboxto(0.15,1.0){\Bigg\}}}}
   \rput[t](26.5,5.25){\rotateleft{\scaleboxto(0.15,1.5){\Bigg\}}}}
   \rput[t](29.5,5.25){\rotateleft{\scaleboxto(0.15,1.5){\Bigg\}}}}
   \rput[b](20,5.5){\footnotesize $p_1$}
   \rput[b](22,5.5){\footnotesize $p_2$}
   \rput[b](24,5.5){\footnotesize $p_3$}
%   \rput[b](24.5,5.5){\footnotesize $p_4$}
   \rput[b](26.5,5.5){\footnotesize $p_4$}
   \rput[b](29.5,5.5){\footnotesize $p_5$}
   \rput(21,4.5){\psframebox*[framesep=0.5pt]{\footnotesize past}}
   \psline[linewidth=2.0pt]{c-c}(23,3.5)(23,5.5)
   \rput(25.5,4.5){\psframebox*[framesep=0.5pt]{\footnotesize planning
   interval}}
   \psline[linewidth=2.0pt]{c-c}(28,3.5)(28,5.5)
   \rput(29.5,4.5){\psframebox*[framesep=0.5pt]{\footnotesize beyond}}
   \rput[l](32,4.5){\psframebox*[framesep=0.5pt]{\footnotesize time blocks}}
\end{pspicture}
\end{center}
\caption{Timetable relating calendar and horizon}\label{fig:time.timetable}
\end{aimmsfigure}

\paragraph{Explanation}

The horizon consists of periods divided into three time blocks, namely
a past, the planning interval, and beyond. There is a current period
in the horizon which can be linked to a current date in the calendar.
The calendar consists of time slots and its range is defined by a
begin date and an end date. When you construct your mathematical
program, it will typically be in terms of periods in the planning
interval of the horizon. However, the input data of the model will
typically be in terms of calendar periods. The conversion of calendar
data into horizon data and vice versa is done on request by AIMMS
in accordance with pre-specified conversion rules.
\section{Calendars}\label{sec:time.calendar}

\paragraph{Calendars}
\index{use of!calendar}
\index{calendar!use of}
\index{time slot}

\syntaxmark{calendar}
A {\em calendar} is defined as a set of consecutive {\em time slots}
of unit length covering the complete time frame from the calendar's
begin date to its end date. You can use a calendar to index data
defined in terms of calendar time.

\paragraph{Calendar attributes}
\declindex{Calendar}
\AIMMSlink{calendar}

Calendars have several associated attributes, which are listed in
Table~\ref{table:time.attr-calendar}. Some of these attributes are
inherited from sets, while others are new and specific to calendars.
The new ones are discussed in this section.

\begin{aimmstable}
\begin{tabular}{|l|l|c|c|}
\hline
\hline
{\bf Attribute}     & {\bf Value-type}     & {\bf See also}
& {\bf Mandatory}  \\
&& {\bf page} & \\
\hline
\verb|BeginDate|   & {\em string}    &&yes \\
\verb|EndDate|     & {\em string}    &&yes \\
\verb|Unit|         & {\em unit}      &&yes \\
\verb|TimeslotFormat| & {\em string} & &yes \\
\hline
\verb|Index|        & {\em identifier-list}  &\pageref{attr:set.index}  &yes \\
\verb|Parameter|    & {\em identifier-list} &\pageref{attr:set.parameter}& \\
\hline
\verb|Text|         & {\em string}   & \pageref{attr:prelim.text}   & \\
\verb|Comment|      & {\em comment string}   & \pageref{attr:prelim.comment}&\\
\hline\hline
\end{tabular}
\caption{Calendar attributes}\label{table:time.attr-calendar}
\end{aimmstable}

\paragraph{Unit}
\declattrindex{calendar}{Unit}
\AIMMSlink{calendar.unit}

The {\tt Unit} attribute defines the length of a single time slot in
the calendar. It must be specified as one of the following
time units or an integer multiple thereof:
\begin{itemize}
\item \verb|century|,
\item \verb|year|,
\item \verb|month|,
\item \verb|day|,
\item \verb|hour|,
\item \verb|minute|,
\item \verb|second|, and
\item \verb|tick| (i.e.\ \verb|sec|/100).\index{tick@{\tt tick}}
\end{itemize}
Thus, \verb|15*min| and \verb|3*month| are valid time units, but the
equivalent \verb|0.25*hour| and \verb|0.25*year| are not. Besides a
constant integer number it is also allowed to use an AIMMS
parameter to specify the length of the time slots in the calendar (e.g.\
\verb|NumberOfMinutesPerTimeslot*min|).

\paragraph{Not predefined}
\typindex{identifier}{Quantity}

Although you can only use the fixed unit names listed above to specify
the {\tt Unit} attribute of a calendar, AIMMS does not have a
predefined {\tt Quantity} for time (see also
Chapter~\ref{chap:units}). This means that the units of time you want
to use in your model, do not have to coincide with the time units
required in the calendar declaration. Therefore, prior to specifying
the {\tt Unit} attribute of a calendar, you must first specify a
quantity defining both your own time units and the conversion factors
to the time units required by AIMMS. In the {\bf Model Explorer},
AIMMS will automatically offer to add the relevant time {\tt
Quantity} to your model when the calendar unit does not yet exist in
the model tree.

\paragraph{BeginDate and EndDate}
\declattrindex{calendar}{BeginDate}
\declattrindex{calendar}{EndDate}
\AIMMSlink{calendar.begin_date}
\AIMMSlink{calendar.end_date}

The mandatory {\tt BeginDate} and {\tt EndDate} attributes of a
calendar specify its range. AIMMS will generate all time slots of
the specified length, whose {\em begin time} lies between the
specified {\tt BeginDate} and {\tt EndDate}. As a consequence, the {\em
end time} of the last time slot may be after the specified {\tt EndDate}. An example of this behavior occurs, for instance, when the
requested length of all time slots is 3 days and the {\tt EndDate}
does not lie on a 3-day boundary from the {\tt BeginDate}. Any period
references that start outside this range will be ignored by the
system. This makes it easy to select all relevant time-dependent data
from a database.

\paragraph{Reference date format}
\index{reference date format}
\index{format!reference date}
\index{time zone! reference date}
\index{daylight saving time! reference date}
\herelabel{par:ref.date.format}

Any set element describing either the {\tt BeginDate} or the {\tt EndDate} must be given in the following fixed {\em reference date} format
which contains the specific year, month, etc. up to and including the
appropriate reference to the time unit associated with the calendar.
\begin{quote}
    \verb|YYYY-MM-DD hh:mm:ss|
\end{quote}
All entries must be numbers {\em with leading zeros present}. The
hours are expressed using the 24-hour clock. You do not need to
specify all entries. Only those fields that refer to time units that
are longer or equal to the predefined AIMMS time unit in your
calendar are required. All time/date fields beyond the requested 
granularity are ignored. For instance, a calendar expressed in hours may
have a {\tt BeginDate} such as
\begin{itemize}
\item ``{\tt 1996-01-20 09:00:00}'', or
\item ``{\tt 1996-01-20 09:00}'', or
\item ``{\tt 1996-01-20 09}'',
\end{itemize}
which all refer to exactly the same time, 9:00 AM on January 20{\em
th}, 1996. 

\paragraph{Time zone and DST offsets}

AIMMS always assumes that reference dates are specified 
according to the local time zone without daylight saving time. 
However, for calendars with granularity {\tt day} AIMMS will ignore 
any timezone and daylight saving time offsets, and just take the day as specified.
In the example above, a daily calendar with the above {\tt BeginDate} will always start with period 
``{\tt 1996-01-20}'', while an hourly calendar may start with a period ``{\tt 1996-01-19 23:00}'' if the difference between
the local time zone, and the time zone specification in the timeslot format is 10 hours.

\paragraph{Format of time-related attributes}
\attrindex{TimeslotFormat}
\AIMMSlink{calendar.timeslot_format}

Set elements and string-valued parameters capturing time-related
information must deal with a variety of formatting possibilities in
order to meet end-user requirements around the globe (there are no
true international standards for formatting time slots and time
periods). The flexible construction of dates and date formats using
the {\tt TimeslotFormat} is presented in
Section~\ref{sec:time.format}.

\paragraph{Example}
\index{calendar!example of use}

The following example is a declaration of a daily calendar and a
monthly calendar
\begin{example}
Calendar DailyCalendar {
    Index            : d;
    Parameter        : CurrentDay;
    Text             : A work-week calendar for production planning;
    BeginDate        : "1996-01-01";
    EndDate          : "1997-06-30";
    Unit             : day;
    TimeslotFormat   : {
        "%d/%m/%y"      ! format explained later
    }
}
Calendar MonthlyCalendar {
    Index            : m;
    BeginDate        : CalendarBeginMonth;
    EndDate          : CalendarEndMonth;
    Unit             : month;
    TimeslotDormat   : {
        "%m/%y"         ! format explained later
    }
}
\end{example}

\paragraph{Varying number of time slots}

The calendar {\tt DailyCalendar} thus declared will be a set
containing the elements {\tt '01/01/96'},\dots,{\tt '06/30/97'} for
every day in the period from January 1, 1996 through June 30, 1997.
When the {\tt BeginDate} and {\tt EndDate} attributes are specified as
string parameters containing the respective begin and end dates (as in
{\tt MonthlyCalendar}), the number of generated time slots can be
changed dynamically.  In order to generate zero time slots, leave 
one of these string parameters empty.

\paragraph{Time zones and daylight saving time}
\index{time zone}
\index{daylight saving time}
\index{calendar!daylight saving time}

By default, AIMMS assumes that a calendar uses the local time zone without daylight saving time, in accordance with the specification of the {\tt BeginDate} and {\tt EndDate} attributes. However, if this is not the case, you can modify the {\tt TimeslotFormat} attribute in such a manner, that AIMMS
\begin{itemize}
\item will take daylight saving time into account during the construction of the calendar slots, or,
\item will generate the calendar slots according to a specified time zone.
\end{itemize}
In both cases, AIMMS still requires that the {\tt BeginDate} and {\tt EndDate} attributes be specified as reference dates in the local time zone without daylight saving time, as already indicated. Support for time zones and daylight saving time is explained in full detail in Section~\ref{sec:time.format.dst}.


\section{Unit expressions}\label{sec:units.expr}
\index{expression!unit}
\index{unit expression}

\paragraph{Unit expressions}
\index{value type!unit}

Unit expressions can be used at various places in an AIMMS model, such as:
\begin{itemize}\hfuzz 0.5pt
\item the {\tt BaseUnit} attribute of a {\tt Quantity} declaration (defined in Section~\ref{sec:units.quantity}),
\item in a local unit override of a numerical (sub-)expression (discussed in Section~\ref{sec:units.local-override})
\item in a convention list of the {\tt PerUnit}, {\tt PerQuantity} or {\tt PerIdentifier} attributes of a {\tt Convention} (see also Section~\ref{sec:units.convention}), or
\item on the right hand side of an assignment to a unit parameter (see Section~\ref{sec:units.unit-par}).
\end{itemize}
The syntax of a unit expression is straightforward, and given below.

\paragraph[.8]{Syntax}
\syntaxdiagram{unit-expression}{unit-expr}

\paragraph{Unit symbols and references}
\index{unit!symbol}

\syntaxmark{unit-reference}
The simplest form of unit expression is just a unit symbol, as defined in either the {\tt BaseUnit} or the {\tt Conversion} attribute of a {\tt Quantity} declaration. A reference to either a (scalar or indexed) unit parameter (see Section~\ref{sec:units.unit-par}) or to the {\tt .Unit} suffix of any identifier with an associated unit (see Section~\ref{sec:units.ident}), is a second form of unit expression.

\paragraph{Unit operators and functions}
\operindex{*}
\operindex{/}
\operindex{\char`\^}

More complex unit expressions can be obtained by applying the binary unit operators {\tt *}, {\tt /} and \verb|^|, with the usual left-to-right evaluation order. The following rules apply:
\begin{itemize}
\item the operand on the right of the {\tt *} operator must be a unit expression, while the operand on the left can either be a unit expression or a numerical expression (expressing a numeric scale factor),
\item both operands of the {\tt /} operator must be unit expressions, and
\item the operand on the left of the \verb|^| operator must be a unit expression, while the exponent operand must be an integer numerical expression.
\end{itemize}
In addition, AIMMS supports a number of unit functions, which can create new unit values or construct associated unit values from a given unit expression (see Section~\ref{sec:units.expr.func}).

\paragraph{Three types of unit expressions}

However, AIMMS requires that any unit expressions uniquely falls into one of the three categories
\begin{itemize}
\item unit constant,
\item simple unit expression, or
\item computed unit expression.
\end{itemize}

\paragraph{Unit constants}
\index{unit expression!constant}
\index{unit constant}

{\em Unit constants} are unit expressions which consist solely of unit symbols, scalar constants and the three unit operators {\tt *}, {\tt /} and \verb|^|. Unit constants can be used in
\begin{itemize}
\item the {\tt BaseUnit} attribute of a {\tt Quantity},
\item the lists associated with a {\tt Convention}, and
\item the unit-valued function {\tt Unit}.
\end{itemize}
In addition, unit constants can be
\begin{itemize}
\item displayed and entered via the AIMMS graphical user interface,
\item assigned to unit parameters through data statements (see Chapter~\ref{chap:text.data.file}), and
\item exchanged with external data sources via the {\tt READ} and {\tt WRITE} statements (see Chapter~\ref{chap:rw}).
\end{itemize}

\paragraph{Simple unit expressions}
\index{unit expression!simple}
\index{simple unit expression}

{\em Simple unit expressions} are an extension of unit constants. They are unit expressions which consist solely of unit symbols, unit references without indexing, scalar constants and the three unit operators {\tt *}, {\tt /} and \verb|^|. Simple unit expressions can be used in
\begin{itemize}
\item local unit overrides, and
\item assignments to unit parameters.
\end{itemize}

\paragraph{Computed unit expressions}
\index{unit expression!computed}
\index{computed unit expression}

Computed unit expression can use the full range of unit expressions, with the exception of unit constants. If you want to refer to unit constants within the context of a computed unit expression, you must embed it within a call to the function {\tt Unit}, discussed in the next section. Computed unit expressions can be used
\begin{itemize}
\item in assignments to unit parameters, and
\item as an argument of the functions {\tt ConvertUnit}, {\tt AtomicUnit} and {\tt EvaluateUnit} (see Sections~\ref{sec:units.expr.func} and~\ref{sec:units.expr.eval}).
\end{itemize}

\subsection{Unit-valued functions}\label{sec:units.expr.func}

\paragraph{Unit-valued functions}
\funcindex{Unit}
\funcindex{StringToUnit}
\funcindex{ConvertUnit}

AIMMS supports the following unit-valued functions:
\begin{itemize}
\item {\tt Unit}({\em unit-constant})
\item {\tt StringToUnit}({\em unit-string})
\item {\tt AtomicUnit}({\em unit-expr})
\item {\tt ConvertUnit}({\em unit-expr}, {\em convention})
\end{itemize}

\paragraph{The function {\tt Unit}}

The function {\tt Unit} simply returns its argument, which must be a unit constant. The function {\tt Unit} is available to allow the usage of unit constants within computed unit expressions (as discussed in the previous section).

\paragraph{The function {\tt StringToUnit}}

The function {\tt StringToUnit} converts a string, which represents a unit expression, to the corresponding unit value. You can use this function, for instance, after reading external string data that needs to be converted to real unit values for further use in your model.

\paragraph{The function {\tt AtomicUnit}}

With the function {\tt AtomicUnit} you can retrieve the atomic unit expression corresponding to the unit expression passed as the argument to the function. Thus, the unit expression
\begin{example}
    AnIdentifier.Unit / AtomicUnit(AnIdentifier.Unit)
\end{example}
will result in a (unitless) unit value that exactly represents the scale factor between the unit of an identifier and its associated atomic unit expression. You can obtain the corresponding numerical value, to be used in numerical expressions, by applying the function {\tt EvaluateUnit} discussed in the next section.

\paragraph{The function {\tt ConvertUnit}}

The function {\tt ConvertUnit} returns the unit value corresponding to the unit expression of the first argument, but taking into consideration the convention specified in the second argument. If the first argument contains a reference to a {\tt .Unit} suffix, AIMMS will apply the full range of conversions
including those specified in the {\tt PerIdentifier} attribute of the convention.

\paragraph{Examples}

The expression
\begin{example}
    ConvertUnit(AnIdentifier.Unit, ConventionUsed)
\end{example}
returns the associated unit of the identifier {\tt AnIdentifier} as if the convention {\tt ConventionUsed} were active. A further example of the use of the function {\tt ConvertUnit} is given in Section~\ref{sec:units.scaling.mp}.

\subsection{Converting unit expressions to numerical expressions}\label{sec:units.expr.eval}

\paragraph{Numeric value of a unit expression}

Although numerical values and unit values are two very distinct data types in AIMMS, the distinction between the two in real life applications is not always as strict. For instance, in the previous section the computation of the ratio between a unit and its associated atomic unit expression returned a unit value, which represents nothing more than a (unitless) scale factor. In practice, however, it is the numeric scale factor value that is of interest, and can be used in numerical computations.

\paragraph{The function {\tt EvaluateUnit}}
\funcindex{EvaluateUnit}

Using the function {\tt EvaluateUnit} you can compute the numerical value associated with a computed unit expression. Its syntax is:
\begin{itemize}
\item {\tt EvaluateUnit}({\em computed-unit-expression})
\end{itemize}
The numeric function value precisely corresponds to one unit of the specified computed unit expression, measured in the evaluated unit of its argument.

\paragraph{Example}

The following assignment  to the scalar parameter {\tt ScaleFactor} computes the (unitless) scale factor between the unit of an identifier and its associated atom\-ic unit expression.
\begin{example}
    ScaleFactor := EvaluateUnit( AnIdentifier.Unit / AtomicUnit(AnIdentifier.Unit) );
\end{example}

\paragraph{Extension of local overrides}

As you will see in the next section, the function {\tt EvaluateUnit} offers extension the local unit override capability. The argument of {\tt EvaluateUnit} can be a computed unit expression (see Section~\ref{sec:units.expr}), whereas local unit overrides can only accept simple unit expressions.


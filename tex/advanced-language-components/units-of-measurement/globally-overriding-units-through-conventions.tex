\section{Globally overriding units through \tttext{Conventions}}\label{sec:units.convention}

\paragraph{Unit conventions}
\index{unit!convention}
\index{use of!unit convention}

In addition to locally overriding the unit definition of an identifier
in a particular statement, you can also {\em globally} override the
default format for data exchange using {\tt READ} and {\tt WRITE},
{\tt DISPLAY} and {\tt SOLVE} statements by selecting an appropriate
{\em unit convention}. A convention offers a global medium to
specify alternative (scaled) units for multiple quantities, units, and
identifiers. In addition, one can specify alternative representations for a calendar in a convention.

\paragraph{Effect of conventions}

Once you have selected a convention, AIMMS will interpret all
data transfer with an external component according to the units that
are specified in the convention. When no convention has been selected
for a particular external component, AIMMS will use the default
convention, i.e.\ apply the unit as specified in the declaration of an
identifier. For a compound quantity not present in a convention,
AIMMS will apply the convention to all composing atomic units used in the compound quantity.

\paragraph{Convention attributes}
\declindex{Convention}
\AIMMSlink{convention}

Conventions must be declared before their use. The list of
attributes of a {\tt Convention} declaration are described in
Table~\ref{table:units.attr-convention}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type} & {\bf See also} \\
 & & {\bf page} \\
\hline
\verb|Text|         & {\em string }  & \\
\verb|Comment|      & {\em comment string } &  \\
\verb|PerIdentifier| & {\em convention-list}/{\em reference} & \\
\verb|PerQuantity|   & {\em convention-list } &  \\
\verb|PerUnit|     & {\em convention-list} & \\
\hline
\verb|TimeslotFormat| & {\em timeslot-format-list} & \pageref{sec:time.tz} \\
\hline\hline
\end{tabular}
\caption{Simple {\tt Convention} attributes}\label{table:units.attr-convention}
\end{aimmstable}

\paragraph{Convention list}
\declattrindex{convention}{PerIdentifier}
\declattrindex{convention}{PerQuantity}
\declattrindex{convention}{PerUnit}
\AIMMSlink{convention.per_identifier}
\AIMMSlink{convention.per_quantity}
\AIMMSlink{convention.per_unit}
\AIMMSlink{convention.timeslot_format}

A convention list is a simple list associating single quantities,
units and identifiers with a particular (scaled) unit expression. The
specified unit expressions must be consistent with the base unit of the
quantity, the specified unit, or the identifier unit, respectively.

\paragraph[0.8]{Syntax}
\begin{syntax}
\syntaxdiagram{convention-list}{unit-list}
\end{syntax}

\paragraph{Customizable conventions}

In addition to a fixed convention list, the {\tt PerIdentifier} attribute also accepts a reference to a unit-valued parameter defined over the set {\tt AllIdentifiers} or a subset thereof. In that case, the convention will dynamically construct a convention list based on the contents of the unit-valued parameter.

\paragraph{Example}

The following declaration illustrates the use of a {\tt Convention} to
define the more common units in the Anglo-American unit system at the
quantity level, the unit level and the identifier level.
\begin{example}
Convention AngloAmericanUnits {
    PerIdentifier  : {
        GasolinePurchase : gallon,
        PersonalHeight   : feet
    }
    PerQuantity    : {
        Velocity         : mph,
        Temperature      : degF,
        Length           : mile
    }
    PerUnit        : {
        cm               : inch,
        m                : yard,
        km               : mile
    }
}
\end{example}

\paragraph{Customizable example}

Assuming that {\tt IdentifierUnits} is a unit-valued parameter defined over {\tt All\-Identifiers}, the following {\tt Convention} declaration illustrates a convention that can be custom\-ized at runtime by modifying the contents of the unit parameter {\tt Identifier\-Units}.
\begin{example}
Convention CustomizableConvention {
    PerIdentifier  : IdentifierUnits;
}
\end{example}


\paragraph{Application order}
\index{convention!application order}

For a particular identifier, AIMMS will select a unit from a
convention in the following order.
\begin{itemize}
\item If a unit has been specified for the identifier, AIMMS will
use it.
\item If the identifier can be associated with a specific quantity in
the convention, AIMMS will use the unit specified for that
quantity.
\item In all other cases AIMMS will apply the convention to an atomic unit directly, or to all composing atomic units used in a compound unit.
\end{itemize}

\paragraph{Timeslot format list}

In addition to globally overriding units, {\tt Conventions} can also be used, through the {\tt TimeslotFormat} attribute, to override the time slot format of calendars. You may need to specify alternative time slot formats, for instance, when you are reading data from an external database or file, in which all dates are not specified in the same time zone as the one your model assumes. The {\tt TimeslotFormat} attribute of a {\tt Convention} is discussed in full detail in Section~\ref{sec:time.tz}.

\paragraph{The {\tt Convention} attribute}
\declattrindex{model}{Convention}
\declattrindex{mathematical program}{Convention}
\declattrindex{file}{Convention}
\declattrindex{database table}{Convention}
\declattrindex{database procedure}{Convention}
% \AIMMSlink{model.convention}
% \AIMMSlink{mathematical_program.convention}
\AIMMSlink{database_table.convention}
\AIMMSlink{database_procedure.convention}

You can declare more than one convention in your model. A {\tt
Convention} attribute can be specified for the following node types in
the model tree, which all correspond to an external component:
\begin{itemize}
\item the main model (used for the end-user interface or as default for all other external components),
\item a mathematical program,
\item a file (also when used to refer to a DLL containing a library of external procedures and functions used by AIMMS), and
\item a database table or procedure.
\end{itemize}
The value of the {\tt Convention} attribute can be a specific
convention declared in your model, or a string or element parameter
referring to a particular unit convention.

\paragraph{Convention semantics}
\index{convention!semantics}

For data exchange with all aforementioned external components AIMMS
will select a unit convention in the following order.
\begin{itemize}
\item If an external component has a nonempty {\tt Convention}
attribute, AIMMS will use that convention.
\item For display in the user interface, or for data exchange with
external components without a {\tt Convention} attribute, AIMMS
will use the convention specified for the main model (see also Section~\ref{sec:module.model}), if present.
\item If the main model and external components have no {\tt
Convention} attribute, AIMMS will use the default convention, i.e.\
use the unit as specified in the declaration of each identifier.
\end{itemize}

\paragraph{Example}

The following declaration of a {\tt File} identifier shows
the use of the {\tt Convention} attribute. All the output to the
file {\tt ResultFile} will be displayed in Anglo-American units.
\begin{example}
File ResultFile {
    Name       : "Output\\result.dat";
    Convention : AngloAmericanUnits;
}
\end{example}
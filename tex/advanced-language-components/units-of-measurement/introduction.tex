\section{Introduction}\label{sec:units.intro}

\paragraph{Units are common}
\index{use of!units}
\index{unit!use of}

Measurement plays a central role in observations of the real world.
Most observed quantities are measured in some unit (e.g.\ dollar, hour,
meter, etc.), and the magnitude of the unit influences the mental
picture that you may have of an object (e.g.\ ounce, kilogram, ton,
etc.). When you combine such objects in a numerical relationship, the
corresponding units must be {\em commensurable}. Without such
consistency, the mathematical relationships become meaningless.

\paragraph{Why units in models}

There are several good reasons to track units throughout a model. The
explicit mentioning of units can enhance the readability of a model,
which is especially helpful when others read and/or maintain your
model. Units provide the AIMMS compiler with additional checking
power to find errors in model formulations. Finally, through the use
of units you can let AIMMS perform the job of unit conversion and
scaling.

\paragraph{Standard units}

The model editor in AIMMS will give you access to a large number of
quantities and units, and in particular to those of the International
System of Units (referred to as SI from the French ``Systeme
Internationale'').  The SI system is an improved metric system adopted
by the Eleventh General Conference of Weights and Measures in 1960.
The entire SI system of measurement is constructed from the atomic base units
associated with the following nine basic quantities.

\begin{aimmstable}
\begin{tabular}{|l|l|l|}
\hline\hline
{\bf Quantity} &    {\bf Atomic Base Unit} & {\bf Text}\\
\hline
length             &  m         &  meter    \\
mass               &  kg        &  kilogram \\
time               &  s         &  second   \\
temperature        &  K         &  kelvin   \\
amount of mass     &  mol       &  mole \\
electric current   &  A         &  ampere   \\
luminous intensity &  cd        &  candela  \\
angle              &  rad       &  radian   \\
solid angle        &  sr        &  steradian    \\
\hline\hline
\end{tabular}
\caption{Basic SI quantities and their base units}\label{table:units.si-basic}
\end{aimmstable}

\paragraph{Derived quan- tities and units}
\index{unit!SI quantities}

All quantities which are not one of the nine basic SI quantities
are called {\em derived} quantities. Each such quantity has a derived base
unit which can be expressed in terms of the atomic base units of the basic SI
quantities. Optionally, a compound unit symbol can be associated with such a derived base unit,
like the symbol {\tt N} for the unit \verb|kg*m/s^2|. The following table illustrates some of the more well-known
derived quantities and their corresponding derived base units. Note that five of them have an associated compound unit symbol. Many other derived quantities are available in AIMMS.

\begin{aimmstable}
\begin{tabular}{|l|l|l|}
\hline\hline
{\bf Quantity}   &  {\bf Derived Base Unit}       &  {\bf Text} \\
\hline
area             &  \verb|m^2|            &  square meter         \\
volume           &  \verb|m^3|            &  cubic meter          \\
force            &  \verb|N  = kg*m/s^2|      &  newton               \\
pressure         &  \verb|Pa = kg/m*s^2|      &  pascal               \\
energy           &  \verb|J  = kg*m^2/s^2|    &  joule                \\
power            &  \verb|W  = kg*m^2/s^3|    &  watt                 \\
charge           &  \verb|C  = A*s|       &  coulomb              \\
density          &  \verb|kg/m^3|         &  kilogram per cubic meter \\
velocity         &  \verb|m/s|            &  meter per second     \\
angular velocity &  \verb|rad/s|          &  radian per second    \\
\hline\hline
\end{tabular}
\caption{Selected derived SI quantities and their base units}\label{table:units.si-derived}
\end{aimmstable}

\paragraph{Related units}

Aside from the base unit that must be associated with every quantity,
it is also possible to specify a number of {\em related}
units. Related units are those units that can be expressed in terms of
their base unit by means of a linear relationship. A typical example is
the unit {\tt km} which is related to the base unit~{\tt m} by means
of the linear relationship $x$ {\tt km = 1000*$x$ m}. Similarly, the unit
{\tt degC} (degree Celsius) is related to the base unit {\tt K}
through the formula $x$ {\tt degC = ($x$ + 273.15) K}.

\paragraph{Standard unit prefix notation}

Frequently, related units are a multiple of their own base unit, which is reflected through a prefix
notation that indicates the level of scaling. Table~\ref{table:units.unit-mult} shows
the standard SI prefix symbols and their corresponding scaling factor.
Familiar examples are {\tt kton}, {\tt MHz}, {\tt kJ}, etc. Note that
any prefix can be applied to any base unit except the kilogram. The
kilogram takes prefixes as if the base unit were the gram.

\begin{aimmstable}
\begin{tabular}{|c|c|c||c|c|c|}
\hline\hline
{\bf Factor}&{\bf Name}&{\bf Symbol}&{\bf Factor}&{\bf Name}&{\bf Symbol} \\
\hline
   $ 10^1 $  &   deca   &  da   &   $   10^{-1} $       & deci   &    d   \\
   $ 10^2 $  &   hecto  &  h    &   $   10^{-2} $       & centi  &    c   \\
   $ 10^3 $  &   kilo   &  k    &   $   10^{-3} $       & milli  &    m   \\
   $ 10^6 $  &   mega   &  M    &   $   10^{-6} $       & micro  &    mu  \\
   $ 10^9 $  &   giga   &  G    &   $   10^{-9} $       & nano   &    n   \\
   $ 10^{12}$  &   tera   &  T    &   $   10^{-12}$     & pico   &    p   \\
   $ 10^{15}$  &   peta   &  P    &   $   10^{-15}$     & femto  &    f   \\
   $ 10^{18}$  &   exa    &  E    &   $   10^{-18}$     & atto   &    a   \\
   $ 10^{21}$  &   zetta  &  Z    &   $   10^{-21}$     & zepto  &    z   \\
   $ 10^{24}$  &   yotta  &  Y    &   $   10^{-24}$     & yocto  &    y   \\
\hline\hline
\end{tabular}
\caption{Prefixes of the International System}\label{table:units.unit-mult}
\end{aimmstable}

\paragraph{Flexible specification}

To give you maximum freedom to choose your own quantities, units and
naming conventions, AIMMS is not exclusively committed to any
particular standard. However, you are encouraged to use the standard
SI units and prefix symbols to make your model as readable and
maintainable as possible.

\paragraph{Summary of terminology}
\index{quantity!basic}
\index{basic quantity}
\index{quantity!derived}
\index{derived quantity}
\index{unit!base}
\index{unit!atomic}
\index{unit!derived}
\index{base unit}
\index{atomic unit}
\index{derived unit}

Thus far you have encountered {\em basic} quantities (Table~\ref{table:units.si-basic}) and {\em derived}
quantities (Table~\ref{table:units.si-derived}). Each quantity has a {\em base} unit. The base unit
of a basic quantity is defined through a unit symbol, referred to as
an {\em atomic} unit. All other base units are {\em derived} base units.
Such units are defined through an expression in terms of other base units,
which can eventually be translated into an expression of atomic base units.
You have the option to associate a unit symbol with any derived base
unit, which is referred to as a {\em compound} unit symbol. Whenever
you have associated a unit symbol with the base unit of either a basic or derived quantity,
you are also allowed to specify one or more {\em related} unit
symbols by specifying the corresponding linear relationship.

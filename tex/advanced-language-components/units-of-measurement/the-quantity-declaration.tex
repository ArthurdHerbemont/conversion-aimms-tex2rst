\section{The \tttext{Quantity} declaration}\label{sec:units.quantity}

\paragraph{Declaration}
\declindex{Quantity}
\AIMMSlink{quantity}

\syntaxmark{quantity}
In AIMMS, all units are uniquely coupled to declared quantities.
For each declared {\tt Quantity} you must specify an identifier
together with one or more of its attributes listed in
Table~\ref{table:units.attr-quantity}.
\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline
\hline
{\bf Attribute} & {\bf Value-type} & {\bf Mandatory} \\
\hline
\verb|BaseUnit|            & [{\em unit-symbol}] [=] & yes \\
                & [{\em unit-expression}] & \\
\verb|Text|                 & {\em string}           & \\
\verb|Conversion|           & {\em unit-conversion-list} & \\
\verb|Comment|              & {\em comment string} & \\
\hline\hline
\end{tabular}
\caption{Quantity attributes}\label{table:units.attr-quantity}
\end{aimmstable}

\paragraph{The {\tt BaseUnit} attribute}
\declattrindex{quantity}{BaseUnit}
\index{unit!symbol}
\AIMMSlink{quantity.base_unit}

\syntaxmark{unit-symbol}
You must always specify a base unit for each quantity that you
declare.  Its value is either
\begin{itemize}
\item an {\em atomic} unit symbol,
\item a {\em unit expression}, or
\item a {\em compound} unit symbol with unit expression.
\end{itemize}
A unit symbol can be any sequence of the characters
\verb|a|--\verb|z|, the digits \verb|0|--\verb|9|, and the symbols 
\verb|_|, \verb|@|,  \verb|&|, \verb|%|, \verb+|+, 
as well as a currency symbol not starting with a digit,
or one of the special unit symbols {\tt 1} and {\tt -}.
The latter two special unit symbols allow you, for instance, to declare model identifiers
without unit, or to express unitless numerical data in terms of percentages.

% Documentation on currency symbols can be found on the internet:
% - http://unicode.org/charts/PDF/U20A0.pdf
% - http://www.cs.tut.fi/~jkorpela/latin1/ascii-hist.html
% - http://www.xe.com/symbols.htm
% - http://nemesis.lonestar.org/reference/telecom/codes/ascii.html

\paragraph{Currency symbols}

AIMMS supports the currency symbols as
defined by the Unicode committee, see \url{http://unicode.org/charts/PDF/U20A0.pdf}.

\paragraph{Separate namespace}
\index{namespace!units}

AIMMS stores unit symbols in namespaces separate but parallel to the identifier namespaces.
Hence, you are free to choose unit symbols equal to the names of global identifiers within your model.
Namespaces in AIMMS are discussed in full detail in Section~\ref{sec:module.module}.

\paragraph{Backward compatibility}

AIMMS 3.8 and older use only a singleton unit namespace which was a
potential cause of nameclashes when units with the same name are declared
from quantities or unit parameters declared in different namespaces.
In order to obtain the old behaviour one can make sure that all units
are declared within the global namespace or set the option
{\tt singleton\_unit\_namespace} to {\tt on}. This option can be found in the
backward compatibility category.

\paragraph{Example}
The following example illustrates the three types of base units.
\begin{example}
Quantity Length {
    BaseUnit   : {
        m                     ! atomic unit
    }
}
\end{example}
\begin{example}
Quantity Time {
    BaseUnit   : {
        s                     ! atomic unit
    }
}
Quantity Velocity {
    BaseUnit   : {
        m/s                   ! unit expression
    }
}
Quantity Frequency {
    BaseUnit   : {
        Hz = 1/s              ! compound unit symbol with unit expression
    }
}
\end{example}
The atomic unit symbols {\tt m} and {\tt s} are the base units for the
quantities {\tt Length} and {\tt Time}. The unit expression {\tt m/s} is the
base unit for the quantity {\tt Velocity}.  The compound unit symbol {\tt
Hz}, defined by the unit expression {\tt 1/s}, is the base unit of the
quantity {\tt Frequency}.

\paragraph{Derived can be used as basic}

The previous example strictly adheres to the SI standards, and, for
example, defines the base unit of the derived quantity {\tt Frequency} in
terms of the base unit of {\tt Time}. In general, this is not
necessary. If {\tt Time} is not used anywhere else in your model, you
can just provide the base unit {\tt Hz} for {\tt Frequency} without
providing its translation in SI base units. {\tt Frequency} then
becomes a basic quantity, and {\tt Hz} becomes an atomic base unit.

\paragraph{Unit expressions}

The unit expressions that you can enter in the {\tt BaseUnit}
attribute can only consist of
\begin{itemize}
\item unit symbols (base and/or related units),
\item constant factors,
\item the two operators ``{\tt *}'' and ``{\tt
/}'',
\item parentheses, and
\item the power operator ``\verb|^|'' with integer
exponent.
\end{itemize}
The common precedence order of the operators ``{\tt
*}'', ``{\tt /}'' and ``\verb|^|'' is as described in
Section~\ref{sec:expr.oper-prec}. Unit expressions are discussed in
full detail in Section~\ref{sec:units.expr}.

\paragraph{The {\tt Conversion} attribute}
\declattrindex{quantity}{Conversion}
\AIMMSlink{quantity.conversion}
\index{derived unit}
\index{quantity!derived unit}

With the {\tt Conversion} attribute you can declare and define one or
more related unit symbols by specifying the (linear) transformation to the
associated base unit.
%You may also specify the inverse transformation.
%The corresponding inverse transformation of the transformation that
%you specified, which is needed to present output data, is
%automatically determined by AIMMS.
%%In the unusual
%%event that the translation of a base unit into a related unit is not
%%equal to the inverse determined by AIMMS, you may enter your own
%%translation to override the default inverse.  This could be of
%%interest when there are two exchange rates between currencies (one for
%%buying and one for selling).
The conversion syntax is as follows.

\paragraph{Syntax}
\begin{syntax}
\syntaxdiagram{unit-conversion-list}{unit-conv}
\end{syntax}

\paragraph{Unit conversion explained}
\index{quantity!allowed conversion}
\index{unit!symbol}

\syntaxmark{unit-conversion}
A unit conversion must be defined using a linear expression of the
form $(\hbox{\tt \#}\cdot a+b)$ where {\tt \#} is a special token, and
the operator $\cdot$ stands for either multiplication or division.
The coefficients $a$ and $b$ can be either numerical constants or
references to scalar parameters. An example in which the use of scalar
parameters is particularly convenient is the conversion between
currencies parameterized by a varying exchange rate.

\paragraph{Example}
\begin{example}
Quantity Length {
    BaseUnit    : m;
    Conversions : {
        km   -> m    :  # -> # * 1000,
        mile -> m    :  # -> # * 1609
    }
}
Quantity Temperature {
    BaseUnit    : degC;
    Conversions : degC -> degF :  # -> # * 1.8 + 32;
}
Quantity Energy {
    BaseUnit    : J = kg * m^2 / s^2;
    Conversions : {
        kJ  -> J     :  # -> # * 1000 ,
        MJ  -> J     :  # -> # * 1.0e6,
        kWh -> J     :  # -> # * 3.6e6
    }
}
Quantity Currency {
    BaseUnit    : US$;
    Conversion  : {
        DM  -> US$   : # -> # * ExchangeRate('DM') ,
        DFl -> US$   : # -> # * ExchangeRate('DFl')
    }
}
Quantity Unitless {
    BaseUnit    : 1;
    Conversions : % -> 1      : # -> # / 100;
}
\end{example}

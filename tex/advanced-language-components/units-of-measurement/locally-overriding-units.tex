\section{Locally overriding units}\label{sec:units.local-override}

\paragraph{Locally overriding units}
\index{unit!local override}

In some rare occasions the unit specified in the declaration of a particular identifier does not necessarily have to match with the unit of the data for that identifier. In that case, AIMMS allows you just to override the unit of a particular expression locally. Such a local unit override of an expression always takes the simple form
\begin{quote}
{\em (}expression\/{\em ) [}simple-unit-expression{\em ]}
\end{quote}
where {\em expression} is some AIMMS expression, and {\em simple-unit-expression} is a simple unit expression as explained in Section~\ref{sec:units.expr}. If {\em expression} solely consists of a numeric constant, AIMMS allows you to omit the parentheses around it.

\paragraph{Where to use}

You can use local unit overrides in a variety of data I/O related situations.
\begin{itemize}
\item In a {\tt WRITE}, {\tt DISPLAY} or {\tt PUT} statement, you can use a local unit override to specify the particular unit in which data must be written to a file, database table or window.
\item In the {\tt FormatString} function, you can use a local unit override to specify the unit in which a numeric argument corresponding to a {\tt \%n} format specifier must be formatted.
\item On the left side of a data assignment, in the header of a composite table, or in a {\tt READ} statement, you can use a local unit override to specify the unit in which the supplied data to be provided.
\end{itemize}

\paragraph{Commensurate requirement}

In all these data I/O statements and expressions, AIMMS requires that the unit provided in the override is commensurate with the original unit that can be associated with the expression.

\paragraph{Example}

Given the declarations of the examples in the Section~\ref{sec:units.analysis}, the
following data I/O statements locally override the default unit {\tt
[km/h]} of the identifier {\tt Veloci\-tyOfItem} with the commensurate unit {\tt [mph]}.
\begin{itemize}
\item Override per identifier:
\begin{example}
    (VelocityOfItem) [mph] := DATA { car: 55, truck: 45 };
    read (VelocityOfItem) [mph] from table VelocityTable;
    display (VelocityOfItem) [mph];
\end{example}
\item Override per individual entry:
\begin{example}
    put (VelocityOfItem('car')) [mph];
    StringVal := FormatString("Speed in [mph]: %n", (VelocityOfItem('car')) [mph]);
\end{example}
\end{itemize}
Recall that parentheses are always required when you want to override
the default unit in expressions and statements, unless the overridden expression is a simple
numeric constant.

\paragraph{Override for consistency}
\index{unit!consistency override}

In addition to overriding units during a data exchange, you can also override the unit of
a (sub)expression in an assignment with the purpose of enforcing unit
consistency of all terms in the assignment. This is especially useful when there are numeric constants inside your expressions. AIMMS will add the appropriate scale factor if the specified unit override does not match with the corresponding atomic unit expression.

\paragraph{Examples}

The following examples illustrate unit overrides with the purpose of enforcing unit consistency.
\begin{itemize}
\item Consider the assignment
\begin{example}
    SoundIntensity := (10 * log10( SoundLevel / ReferenceLevel )) [dB];
\end{example}
If {\tt SoundIntensity} has an associated unit of {\tt [dB]}, the right hand side of the assignment, which by itself is unitless, must be locally overridden to make the entire assignment unit consistent.
\item Consider the assignment
\begin{example}
    a := b + 10 [km];
\end{example}
where both {\tt a} and {\tt b} are measured in terms of length. As discussed in Section~\ref{sec:units.analysis}, AIMMS
will make no assumption about the unit associated with the numerical constant {\tt 10} in the expression on the right-hand side of the assignment. In order to make the assignment unit consistent, an explicit unit override of the
constant term is required. If the associated base unit is {\tt [m]}, AIMMS will automatically add a scale factor of 1000, whence the assignment will numerically evaluate to {\tt a := b + 10*1000}.
\end{itemize}

\paragraph{Caution is needed}

If you explicitly associate a unit with an expression which already
contains one or more identifiers {\em with} associated units, the
numerical result can be unexpected. This is due to fact that AIMMS, during
expression evaluation, uses the unscaled numerical values with respect to
the associated atomic units of each identifier. To illustrate,
reconsider the assignment
\begin{example}
    a := (b * c) [km];
\end{example}
but now assume that the identifiers {\tt a}, {\tt b}, and {\tt c} have units {\tt [km]},
{\tt [km]}, and {\tt [10*m]}. If the values of {\tt b} and {\tt c} are
{\tt 1 [km]}({\tt=1000 [m]}) and {\tt 50 [10*m]}({\tt=500 [m]}),
respectively, the numerical result of {\tt a} after the assignment
will amount to {\tt (500 * 1000)*1000 [m]= 500000 [km]}, which may not
be the result that you intended.


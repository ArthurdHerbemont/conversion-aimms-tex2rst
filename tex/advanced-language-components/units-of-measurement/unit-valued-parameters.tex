\section{Unit-valued parameters}\label{sec:units.unit-par}

\paragraph{Parametrized units}

In some cases not all entries of an indexed identifier have the same
associated unit. An example is the diet model where the nutritive value of each
nutrient for a single serving of a particular food type is measured
in a different unit.

\paragraph{Unit-valued parameters}
\index{value type!unit}
\index{unit-valued parameter}
\index{parameter!value type!unit}
\AIMMSlink{unit_parameter}

In order to deal with such situations, AIMMS allows the declaration
of (indexed) {\em unit-valued} parameters which you can use in the
unit definition of the other parameters and variables in your model.
In the model tree, unit-valued parameters are available as a special
type of parameter declaration, with attributes as given in Table~\ref{table:units.attr-unit-par}.
\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}               & {\bf See also} \\
                &                                    & {\bf page} \\
\hline
\verb|IndexDomain|   & {\em index-domain}                  & \\
\verb|Quantity|    & {\em quantity}                          & \\
\verb|Default|     & {\em unit-expression}                & \\
\verb|Property|   & \verb|NoSave| & \pageref{attr:par.property} \\
\verb|Text|         & {\em string}                     & \pageref{attr:prelim.text}\\
\verb|Comment|      & {\em comment string}             &
\pageref{attr:prelim.comment}, \pageref{attr:set.comment}\\
\verb|Definition|   & {\em unit-expression}                 & \\
\hline\hline
\end{tabular}
\caption{{\tt UnitParameter} attributes}\label{table:units.attr-unit-par}
\end{aimmstable}

\paragraph{The {\tt Quantity} attribute}
\declattrindex{unit parameter}{Quantity}
\AIMMSlink{unit_parameter.quantity}

You should specify the {\tt Quantity} attribute if all unit values stored in the unit parameter can be associated with a single quantity declared in your model. The effect of specifying a quantity in the {\tt Quantity} attribute of a unit parameter is twofold:
\begin{itemize}
\item during assignments to the unit parameter, AIMMS will verify whether the assigned unit values are commensurate with the base unit of specified quantity, and
\item AIMMS will modify its (compile-time) unit analysis to use the specified quantity rather than an artificial quantity based on the name of the unit parameter (see below).
\end{itemize}

\paragraph{The {\tt Default} and {\tt Definition} attributes}
\declattrindex{unit parameter}{Default}
\AIMMSlink{unit_parameter.default}
\declattrindex{unit parameter}{Definition}
\AIMMSlink{unit_parameter.defintion}

The {\tt Default} and {\tt Definition} attributes of a unit parameter have the same purpose as the {\tt Default} and {\tt Definition} attribute of ordinary parameters, except that the resulting values must be unit expressions (see Section~\ref{sec:units.expr}). If you have specified a quantity in the {\tt Quantity} attribute, AIMMS will verify that these unit expressions are commensurate with the specified quantity.

\paragraph{Allowed unit values}

All unit values read from an external data source, or assigned to a unit parameter, either via an assignment or through its {\tt Definition} attribute, must evaluate to existing unit symbols only. A compile- or runtime error will occur, when a unit value refers to a unit symbol that is not defined in any of the {\tt Quantity} declarations contained in your model.

\paragraph{Use of unit parameters}

With unit parameters you can create, store and manipulate scalar or multidimensional collections of unit values. The unit values stored in a unit parameter can be used, for instance:
\begin{itemize}
\item to associate a parametrized (i.e.\ multidimensional) collection of units with a single multidimensional identifier (through its {\tt Unit} attribute), or
\item to specify a local unit override based on a unit (or collection of units) that is not known a priori.
\end{itemize}

\paragraph{Unit analysis\dots}

When a {\tt Unit} attribute of an identifier contains a reference to a unit parameter, this can, but need not, modify the way in which AIMMS conducts its usual unit analysis. There are two distinct scenarios, both described below.

\paragraph{\dots with associated quantity}

If the unit parameter has an associated quantity (specified through its {\tt Quantity} attribute), all units stored in the unit parameter are known to be commensurate with the base unit of the quantity, although the individual scale factors may be different if the unit parameter is multidimensional. In this case, AIMMS will base its unit analysis on the associated quantity.

\paragraph{\dots without associated quantity}

If there is no associated quantity, AIMMS will introduce an artificial quantity solely on the basis of the symbolic name of the unit parameter (i.e.\ without consideration of its dimension), and base all further unit analysis on this artificial quantity only. If there is unit consistency at the level of these artificial quantities, this automatically ensures, for multidimensional unit parameters, unit consistency at the individual level as well, regardless of the specific individual unit values stored in it.

\clearpage

\paragraph{Example}

Consider the following declarations of unit-valued parameters, where
{\tt f} is an index into the set {\tt Foods} and {\tt n} an index into
the set {\tt Nutrients}.
\begin{example}
UnitParameter NutrientUnit {
    IndexDomain  : n;
}
UnitParameter FoodUnit {
    IndexDomain  : f;
}
\end{example}
With these unit-valued parameters you can specify meaningful indexed
unit expressions for the {\tt Unit} attribute of the following
parameters.
\begin{example}
Parameter NutritiveValue {
    IndexDomain  : (f,n);
    Unit         : NutrientUnit(n)/FoodUnit(f);
}
Parameter NutrientMinimum {
    IndexDomain  : n;
    Unit         : NutrientUnit(n);
}
Variable Serving {
    IndexDomain  : f,
    Unit         : FoodUnit(f);
}
\end{example}
With these declarations, you can now easily verify that all terms in
the definition of the following constraint are unit consistent at the
symbolic level.
\begin{example}
Constraint NutrientRequirement {
    IndexDomain  : n;
    Unit         : NutrientUnit(n);
    Definition   : sum[ f, Servings(f)*NutritiveValue(f,n) ] >=  NutrientMinimum(n);
}
\end{example}

\paragraph{Indexed scaling}

When the {\tt Unit} attribute of an identifier is parametrized by means of indexed unit parameter, AIMMS will correctly scale all data exchange with external components (see Section~\ref{sec:units.scaling}). During data exchange with an external component, AIMMS considers the specified units at the individual (indexed) level, and will determine the proper scaling for every individual index position. In addition, when a unit convention is active, AIMMS will scale all individual entries according to that convention, as applied to the corresponding individual entries of the indexed unit parameter.
As usual, all data of an identifier with a parametrized associated unit will be stored internally in the corresponding atomic unit of every individual index value.

\paragraph{Example revisited}

When AIMMS generates mathematical program which contains the variable {\tt Serving(f)}, each column corresponding to this variable will be scaled according to the scale factor of the particular unit stored in {\tt FoodUnit(f)} with respect to their corresponding atomic unit expressions.
Similarly, AIMMS will scale the columns corresponding to the constraint {\tt NutrientRequirement(n)} according the scale factors of the units stored in {\tt NutrientUnit(n)} with respect to their corresponding atomic unit expressions.

\paragraph{Initializing unit-valued parameters}

You can initialize a unit-valued parameter through lists, tables, and
composite tables like you can initialize any other AIMMS
parameter (see Chapter~\ref{chap:text.data.file}). The values of the individual entries must be valid unit
constants (see Section~\ref{sec:units.expr}), and must be surrounded by square brackets. For compound units constants you can optionally indicate the associated quantity in a similar way as in the unit definition of a
parameter.

\paragraph{Example}
The following list initializes the unit-valued parameter {\tt
NutrientUnit} for a particular set of {\tt Nutrients}.
\begin{example}
    NutrientUnit := DATA { Energy  : [kJ]  ,
                           Protein : [mg]  ,
                           Iron    : [%RDA]  };
\end{example}

\paragraph{Unit parameters and databases}

In addition, AIMMS allows you to read the initial data of a unit parameter from a database table, and write the values of a unit parameter to a database table. The unit values in the database table must be unit constants, and must be stored without square brackets.

\paragraph{Simultaneous unit and data initialization}

When a composite table in a data file, or a table in a database contains both the values of a multidimensional unit parameter, and a corresponding numeric parameter whose {\tt Unit} attribute references that unit parameter, AIMMS allows you to read both identifiers in a single pass. When reading both identifiers, AIMMS will make sure that the numeric values are interpreted with respect to the corresponding unit value that is read simultaneously.

\paragraph{Constant versus parametrized units}

AIMMS even allows you to make assignments from identifiers with a
constant unit to identifier slices of identifiers with a parametrized
unit and vice versa. If AIMMS detects this special situation during compilation of your model, it will postpone the compile unit consistency check whenever necessary, and replace it with a runtime consistency check which is performed every time the assignment is executed. Because all data is stored by AIMMS with respect to atomic units internally, unit consistency again automatically implies scale consistency.

\paragraph{Example}

Given the declarations of the previous example, assume the existence of an additional parameter {\tt EnergyContent(f)} with a constant associated unit, say {\tt Kcal}. Then, AIMMS will postpone the compile unit consistency check for the following two statements, and replace it with a runtime check.
\begin{example}
    NutritiveValue(f,'Energy') := EnergyContent(f);
    EnergyContent(f)           := NutritiveValue(f,'Energy');
\end{example}
The runtime unit consistency check will only succeed, whenever the unit value of the unit parameter {\tt NutrientUnit('Energy')} is commensurate with the constant unit {\tt Kcal}.

\paragraph{Restrictions}

AIMMS will only replace a compile time with a runtime unit consistency check if a unique unit can be associated with the right-hand side of the assignment at compile time. If the assigned expression consists of subexpressions which have different associated unit expressions at compile time, a compile time error will result. This is even the case when, at runtime, these unit expressions evaluate to units that are commensurate with the unit of the left-hand side of the assignment.

\section{\tttext{LibraryModule} declaration and attributes}\label{sec:module.library}
\declindex{LibraryModule}\AIMMSlink{library_module}

\paragraph{{\tt LibraryModule} declaration and attributes}

{\tt LibraryModule} nodes create a separate tree in the model tree along with a separate name\-space for all identifier declarations in that subtree. The model contents associated with a {\tt LibraryModule} is always stored in a separate source file. Contrary to {\tt Section} and {\tt Module} nodes, the name of this source file cannot be specified in the {\tt LibraryModule} declaration. Rather, it is specified when you add the library to your project using the {\bf Library Manager}, as discussed in Section~\ref{UGsec:proj-organization.manager} of the User's Guide. The attributes of {\tt LibraryModule} nodes are listed in Table~\ref{table:module.attr-library}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}  & {\bf See also}\\
&&{\bf page} \\
\hline
\verb|Prefix|       & {\em identifier} & \\
\verb|Interface|       & {\em identifier-list} & \\
\hline
\verb|Property| & {\tt NoSave}  & \pageref{attr:module.property} \\
\hline
\verb|Comment|      & {\em comment string}   & \pageref{attr:prelim.comment}\\
\hline\hline
\end{tabular}
\caption{{\tt LibraryModule} attributes}\label{table:module.attr-library}
\end{aimmstable}

\paragraph{Library modules and namespaces}
\index{namespace} \index{library module!namespace}

Like a normal module, each {\tt LibraryModule} is supplied with a
separate namespace. Compared to normal modules, however, the visibility rules for identifiers in a library modules are different. They are more in line with the intended use of libraries, i.e.\ to enable a single developer to work independently on the model source of a library.

\paragraph{The {\tt Interface} attribute}
\declattrindex{library module}{Interface} \AIMMSlink{library_module.interface}

Through the {\tt Interface} attribute of a {\tt LibraryModule} you can specify the list of identifiers in the module that you want to be part of its public interface. Only identifiers in the library interface can be accessed in model declarations, pages and menu items that are not part of the library at hand.
Library identifiers not in the interface are strictly private to the library, and can never be used outside of the library.

\paragraph{The {\tt Prefix} attribute}
\declattrindex{library module}{Prefix} \AIMMSlink{library_module.prefix}

With the {\em mandatory} {\tt Prefix} attribute of a {\tt LibraryModule} node, you
must specify a module-specific prefix to be used in conjunction with the {\tt
::} operator. The value of the {\tt Prefix} attribute should be a unique name
within the main model.

\paragraph{No propagation to global namespace}

Even though identifiers in the interface of the library are visible outside of the library, AIMMS {\em always} requires the use of the library prefix to reference such identifiers. Library modules do not support the {\tt Public} attribute of ordinary modules to propagate identifiers to the global namespace.

\paragraph{Library initialization and termination}
\procindex{LibraryInitialization}
\procindex{PostLibraryInitialization}
\procindex{LibraryTermination}
\procindex{PreLibraryTermination}

When creating a new library, AIMMS will automatically add {\tt LibraryInitializat\-ion}, {\tt PostLibraryInitialization}, {\tt PreLibraryTermination} and {\tt LibraryTermination} procedures to it. 
These procedures will be executed during the initialization and termination of your model. The distinction between these steps are explained in more detail in Section~\ref{sec:data.init}.

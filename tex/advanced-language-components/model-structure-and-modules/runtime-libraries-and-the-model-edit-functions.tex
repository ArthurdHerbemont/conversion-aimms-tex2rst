\section{Runtime Libraries and the Model Edit Functions}\label{sec:module.runtime}

Runtime libraries and the AIMMS Model Edit Functions permit applications to adapt 
to modern flexibility requirements of the model; at runtime you can create 
identifiers and subsequently use them.  A few use cases, in which 
the need for flexibility in the model grows, are briefly outlined below.

\paragraph{Use case: automating modeling tasks}
You may want to improve the maintainability of your application by 
\begin{itemize}
\item Generating similar statements that act on dynamic selections of identifiers, or
\item Generate necessary parameters and database table identifiers with their mapping attributes
      by querying a relational database schema when setting up a database link with your model.
\end{itemize}

\paragraph{Use case: Cooperative model development}
Another example, in cooperative model development, a model is developed together with the users of that model.
For instance, an existing application framework is demonstrated to the users and, 
subsequently, the suggestions from these users are taken into account. A suggestion might 
be to add structural nodes or arcs, or might be to add a 
particular restriction on existing nodes and arcs.

\paragraph{Use case: Proprietary user knowledge}
Further, not all structural information may be available at the time of model development;  
some users may need to add their proprietary knowledge to the model at runtime.  
Examples of such proprietary knowledge are: 
\begin{itemize}
\item Pricing rules for the valuation of portfolios.
\item Blending rules for the prediction of property values of blends.
\end{itemize}

\paragraph{Use case: ad hoc user queries}
A final example of a modern flexibility requirement is a user who has 
additional questions only when the results are actually presented. 
Such a user wants to question the model in order to understand a particular result. 
This person is only able to formulate the question after the unexpected result presents itself.

\paragraph{Runtime editing of identifiers}
In the above use cases, applications create, manipulate, check, use, and destroy AIMMS identifiers 
at runtime. Such operations are performed by the Model Edit Functions.
Such applications need to:
\begin{enumerate}
\item Have a place to store these AIMMS identifiers and to retrieve them from. 
      Such a place is called an AIMMS runtime library.
\item Have functions and procedures available to create, modify, check, and destroy these AIMMS identifiers.
      Together, these functions and procedures form the Model Edit Functions.
\item Have a way to use these identifiers inside the model.
\item {\em And be able to continue execution in the presence of errors}.
      This fourth requirement is an essential aspect of all the other requirements and 
      is central to the design of the AIMMS Runtime libraries and AIMMS Model 
      Edit Functions. Global and local error handling is described in 
      Section~\ref{sec:exec.error.handling}.
\end{enumerate}


\paragraph{Runtime identifiers and libraries}
The identifiers created, modified, checked, used, and destroyed at runtime are called 
runtime identifiers. These runtime identifiers are declared within a runtime library.
A runtime library is itself also a runtime identifier: it can also be created, modified, checked, used, and
destroyed at runtime. A runtime identifier can have any AIMMS type, except for 
quantity.

\paragraph{Separation between main application and runtime libraries}
Model edit functions are only allowed to operate on runtime identifiers. 
Runtime identifiers exist at runtime but do not yet exist at compile time; the names of
runtime identifiers cannot be used directly in the main model.
This enforces a separation between identifiers in the main application and
runtime identifiers as depicted in Figure~\ref{fig:module.runtime.library}.
On the left side of this architecture there is a main application consisting of a main model and zero, one or more libraries.
On the right there are zero, one or more runtime libraries. 
Compilation errors can occur within runtime libraries at runtime. The identifiers inside the main application  
are not affected by such an error; that is, provided it has local error handling, any procedure inside the main application can continue execution 
in the presence of compilation errors on identifiers in a runtime library.
This is an important advantage of the separation: 
for several of the use cases presented above, this separation enables continuation in the presence of errors. 

\begin{aimmsfigure}
\figscale[0.5]{AimmsRuntimeLibraries}
\caption{Separation between main application and runtime libraries}\label{fig:module.runtime.library}
\end{aimmsfigure}

\paragraph{Example of creating an identifier}
In this example, a runtime procedure {\tt rp} is created and its body specified.
This procedure is created in the runtime library {\tt MyRuntimeLibrary1} with prefix {\tt mrl}.
The purpose of the runtime procedure {\tt rp} is to write out the runtime parameter {\tt P}
declared in the same runtime library. This example assumes that both the runtime library 
{\tt MyRuntimeLibrary1} and the runtime parameter {\tt P} already exist.
\begin{example}
Procedure DisplayDataOfRuntimeIdentifierTabular {
    ElementParameter erp {
        Default    : 'MainExecution';
        Range      : AllIdentifiers;
    }
    StringParameter str;
    ElementParameter err {
        Range      : errh::PendingErrors;
    }
    ElementParameter err2 {
        Range      : errh::PendingErrors;
    }
    Body {
 1      block
 2          erp := me::Create("rp", 'procedure', 'MyRuntimeLibrary1', 0);
 3          me::SetAttribute(erp, 'body', "display { P } ;");
 4          me::Compile(erp);
 5          me::Compile('MyRuntimeLibrary1');
 6          Apply(erp);
 7          me::Delete(erp);
 8      onerror err do
 9          if erp then
10              block
11                  me::Delete(erp);
12              onerror err2 do
13                  if errh::Severity(err2) = 'Severe' then
14                      DialogMessage(errh::Message(err2) +
15                          "; not prepared to handle severe errors " +
16                          "and halting execution");
17                      halt ;
18                  else
19                      errh::MarkAsHandled(err2) ;                
20                  endif ;
21              endblock ;
22              erp := '' ;
23          endif ;
24          errh::MarkAsHandled(err);
25          DialogMessage("Creating and executing rp failed; " + errh::Message(err) );
26      endblock ;
    }
\end{example}

A line by line explanation of this example follows below.
\begin{description}
\item[Lines 1, 8, 25] In order to handle the errors during a group of
              model edit actions, a {\tt BLOCK} statement with an
              {\tt ONERROR} clause is used.
\item[Lines 2 - 7] Contain the calls to the model edit functions. Note that these
              are formulated without any concern for errors because
              these errors are handled in line 9 - 25.
\item[Line 2] Create the procedure {\tt rp} as the final procedure in
              the runtime library {\tt MyRuntimeLibrary1}.
              The prefix of the library will be prefixed to the name of
              the identifier created; and after this statement
              the value of the element parameter {\tt erp} is {\tt 'mrl::rp'}.
\item[Line 3] Sets the contents of the body of that procedure.  Here it is
              to display the parameter {\tt P} in tabular format.
\item[Line 4] Checks the procedure {\tt mrl::rp} for errors.
\item[Line 5] Compiles the entire runtime library {\tt MyRuntimeLibrary1}
              which will make the procedures inside that library runnable.
\item[Line 6] Executes the procedure just created.
\item[Line 7] Delete the procedure just created.
\item[Lines 9 - 23] Try to delete {\tt erp} ({\tt mrl::rp}) if it has not already been deleted.
\item[Lines 13 - 20] Ignore all errors during the deletion except for severe internal errors.
\item[Line 24] Mark the error {\tt err2} as handled.              
\item[Line 25] Finally notifies the application user that something
              has gone wrong.
\end{description}


\paragraph{Model Edit Functions}
Model editing is available from within the language itself with
intrinsic functions and procedures to view, create, modify, move,
rename, compile, and delete identifiers. 
An intrinsic function or procedure that modifies the application is
called a Model Edit Function.
These functions and procedures reside in the predeclared module
{\tt ModelEditFunctions} with the prefix {\tt me}. 
The table below lists the Model Edit Functions and briefly describes them.


\begin{aimmstable}
\funcindexns{me}{CreateLibrary}
\funcindexns{me}{Create}
\procindexns{me}{Delete}
\procindexns{me}{ImportLibrary}
\procindexns{me}{ImportNode}
\procindexns{me}{ExportNode}
\funcindexns{me}{Parent}
\procindexns{me}{Children}
\funcindexns{me}{Type}
\funcindexns{me}{ChildTypeAllowed}
\funcindexns{me}{TypeChangeAllowed}
\procindexns{me}{TypeChange}
\funcindexns{me}{GetAttribute}
\procindexns{me}{SetAttribute}
\funcindexns{me}{AllowedAttribute}
\procindexns{me}{Rename}
\procindexns{me}{Move}
\funcindexns{me}{IsRunnable}
\procindexns{me}{Compile}
\AIMMSlink{me::CreateLibrary}
\AIMMSlink{me::Create}
\AIMMSlink{me::Delete}
\AIMMSlink{me::ImportLibrary}
\AIMMSlink{me::ImportNode}
\AIMMSlink{me::ExportNode}
\AIMMSlink{me::Parent}
\AIMMSlink{me::Children}
\AIMMSlink{me::ChildTypeAllowed}
\AIMMSlink{me::TypeChangeAllowed}
\AIMMSlink{me::TypeChange}
\AIMMSlink{me::GetAttribute}
\AIMMSlink{me::SetAttribute}
\AIMMSlink{me::AllowedAttribute}
\AIMMSlink{me::Rename}
\AIMMSlink{me::Move}
\AIMMSlink{me::IsRunnable}
\AIMMSlink{me::Compile}
\size{9}{11.35pt}\selectfont
\begin{tabular}{|l|}
\hline\hline
{\tt me::CreateLibrary}({\em libraryName}, {\em prefixName})$\to${\tt AllIdentifiers}\\
{\tt me::Create}({\em name}, {\em newType}, {\em parentId}, {\em pos})$\to${\tt AllIdentifiers}\\
{\tt me::Delete}({\em runtimeId})\\
\hline
{\tt me::ImportLibrary}({\em filename}[, {\em password}])$\to${\tt AllIdentifiers}\\
{\tt me::ImportNode}({\em esection}, {\em filename}[, {\em password}])\\
{\tt me::ExportNode}({\em esection}, {\em filename}[, {\em password}])\\
\hline
{\tt me::Parent}({\em runtimeId})$\to${\tt AllIdentifiers}\\
{\tt me::Children}({\em runtimeId}, {\em runtimeChildren(i)})\\
\hline
{\tt me::ChildTypeAllowed}({\em runtimeId}, {\em newType})\\
{\tt me::TypeChangeAllowed}({\em runtimeId}, {\em newType})\\
{\tt me::TypeChange}({\em runtimeId}, {\em newType})\\
\hline
{\tt me::GetAttribute}({\em runtimeId}, {\em attr})\\
{\tt me::SetAttribute}({\em runtimeId}, {\em attr}, {\em txt})\\
{\tt me::AllowedAttribute}({\em runtimeId}, {\em attr})\\
\hline
{\tt me::Rename}({\em runtimeId}, {\em newname})\\
{\tt me::Move}({\em runtimeId}, {\em parentId}, {\em pos})\\
\hline
{\tt me::IsRunnable}({\em runtimeId})\\
\hline
{\tt me::Compile}({\em runtimeId})\\
\hline
\hline\hline
\end{tabular}
\caption{Model Edit Functions for runtime libraries}\label{table:me.library}
\end{aimmstable}

\paragraph{Creating and deleting}
Table~\ref{table:me.library} lists the Model Edit Functions.
A new runtime library can be created using the function {\tt me::CreateLibrary}. 
if successful this function returns the library as an element in {\tt AllSymbols}.
The function {\tt me::Create} creates a new node or identifier with name {\tt name}
of type {\tt type} in section {\tt ep\_sec} at position {\tt pos}.
The return value is an element in {\tt AllSymbols}. If inserted at position $i$ ($i>0$), 
the declarations previously at positions $i$ .. $n$ are moved to positions $i+1$ .. $n+1$.  
If inserted at position 0, the identifier is placed at the end. The procedure {\tt me::Delete} 
can be used to delete both a runtime library and a runtime identifier in a library. 
All subnodes of {\tt ep} in the runtime library are also deleted.

\paragraph{Reading and writing}
The procedure {\tt me::ImportNode} reads a section, module, or library into node {\tt ep}.  If {\tt ep} is a
runtime library, an entire library is read, replacing the existing prefix. {\tt me::ExportNode} writes
the contents of the model editor tree referenced by {\tt ep} to a file.  These two procedures use
the text {\tt .ams} file format.

\paragraph{Inspecting the tree}
The function {\tt me::Parent(ep)} returns the parent of {\tt ep}, or the empty element if {\tt ep} is a root.
The function {\tt me::Children(ep, epc(i))} returns the children of {\tt ep} in {\tt epc(i)} in which {\tt i}
is an index over a subset of {\tt Integers}.

\paragraph{Node types}
The function {\tt me::ChildTypeAllowed(ep, et)} 
returns 1 if an identifier of type {\tt et} is allowed as a child of {\tt ep}. The function {\tt me::TypeChangeAllowed(ep, et)} 
returns 1 if the identifier {\tt ep} is allowed to change into type {\tt et}. The procedure {\tt me::Type\-Change(ep,et)} performs a type change
while attempting to retain as many attributes as possible.

\paragraph{Attributes}
The function {\tt me::GetAttribute(ep, attr)} returns the contents of the attribute {\tt attr} of identifier or node {\tt ep}.
The complementary procedure {\tt me::SetAttribute (ep,attr,str)} specifies these contents. The function {\tt me::AllowedAttribute(ep, attr)} returns 1 if
attribute {\tt attr} of identifier {\tt ep} is allowed to have text.

\paragraph{Changing name or location}
The procedure {\tt me::Rename(ep, newname)} gives {\tt ep} a new name {\tt newname}. 
The text inside the library is adapted, but a corresponding entry in the namechange file is not created.
The procedure {\tt me::Move(ep, ep\_p, pos)} moves an identifier from one location to another. 
When an identifier changes its namespace, this is a change of name, and the text in the runtime library is adapted correspondingly, 
but no entry in the namechange file is created.
Runtime identifiers can not be moved from one runtime library to another. 

\paragraph{Querying runtime library status}
The function {\tt me::IsRunnable(ep)} returns 1 if {\tt ep} is inside a succesfully compiled runtime library.  

\paragraph{Compilation}
The function {\tt me::Compile(ep)} compiles the node {\tt ep} and all its subnodes.  If {\tt ep} is the empty element,
all runtime libraries are compiled.
See also Section~\ref{sec:data.allidentifiers} on working with {\tt AllIdentifiers}.


\paragraph{Runtime identifiers are like data}

To the main application, runtime identifiers are 
like data. Data operations such as creation, 
modification, destruction, read, and write are also 
applicable to runtime identifiers. When saving 
a project, the runtime libraries are \textbf{not} 
saved. Runtime libraries, including the data of 
runtime identifiers, can be saved in two ways: 
as separate files or in cases.

\paragraph{Storing runtime libraries in separate files}

The runtime libraries themselves can be saved in text 
or binary model files using the function {\tt me::ExportNode}.  
They can subsequently be read back using the functions 
{\tt me::ImportLibrary} and {\tt me::ImportNode} (see the 
function reference for more details on these functions).  
The data of the runtime identifiers can be written using a 
{\tt write to file} statement and be read back using a 
{\tt read from file} statement, see also 
Section~\ref{sec:rw.example.simple}.

\paragraph{Storing runtime libraries in cases}

When saving a case, a snapshot of the data in a model, or a
selection thereof (casetype), is saved.  The data of a model 
include the runtime libraries. However, the names of the 
runtime identifiers can vary and therefore they cannot be 
part of a casetype.  Whether runtime libraries are 
saved in a case is controlled by a global option, named 
{\tt Case contains runtime libraries}. When loading a case 
saved with this option switched on, the previously created 
runtime libraries will be first destroyed and then the 
stored runtime libraries will be recreated, both their structure 
and data. When loading a case saved while this option was 
off, or a case saved with AIMMS 3.10 or earlier, any 
existing runtime libraries will be left intact. Datasets 
never contain runtime libraries.

\paragraph{The {\tt NoSave} property}

When the {\tt NoSave} property is specified for a runtime library, 
this runtime library will not be saved in cases.

\paragraph{The AIMMS model explorer}
To the AIMMS model explorer, the runtime libraries are read only;
it can copy runtime identifiers into the main application, but it
cannot modify runtime identifiers.  This is because, if the AIMMS 
model explorer could modify runtime identifiers, the state information
maintained by the main application regarding the runtime identifiers
might become inconsistent with the actual state of these runtime
identifiers.

\paragraph{Visualizing the data of runtime identifiers}
When AIMMS is in developer mode, data pages of the runtime identifiers can be opened, 
just like data pages of ordinary identifiers. The data of runtime identifiers can also
be visualized on the AIMMS pages in the following two ways:
\begin{itemize}
\item The safest way is to create a subset of \PreRef{AllIdentifiers} containing the selected
      runtime identifiers, and use this subset as "implicit identifiers" in a pivot table. If 
      the runtime identifiers referenced in this set do not yet exist, they will 
      simply not be displayed.
\item The runtime identifiers can also be directly visualized in other page objects.  
      Care should then be taken that the visualized runtime identifiers are created with 
      the proper index domain before a page is opened containing these identifiers; if an   
      identifier does not exist, a page containing a reference to such an identifier will 
      not open correctly.
      In order to avoid the inadvertent use of runtime identifiers on pages, they are not 
      selectable using point and click in the identifier selector, but the identifier selector 
      accepts them when typed in.
\end{itemize}


\paragraph{Limitations}
The following limitations apply:
\begin{itemize}
\item Local declarations are not supported; only global identifiers 
      corresponding to elements in \PreRef{AllIdentifiers}.
\item Quantities are not supported.
\item The {\tt source file}, {\tt module code} and {\tt user data} 
      attributes are not supported.
\item The current maximum number of identifiers is thirty thousand.
\end{itemize}



\section{\tttext{Module}~declaration and attributes}\label{sec:module.module}
\declindex{Module}\AIMMSlink{module}

\paragraph{{\tt Module} declaration and attributes}

{\tt Module} nodes create a subtree of the model tree along with a separate
name\-space for all identifier declarations in that subtree. Like {\tt Section}
nodes, the model contents associated with a {\tt Module} node can be stored in
a separate source file.  A {\tt Module} node is always a child of the main {\tt
Model} node, of a {\tt Section} node, or of another {\tt Module} node. The
attributes of {\tt Module} nodes are listed in
Table~\ref{table:module.attr-module}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}  & {\bf See also}\\
&&{\bf page} \\
\hline
\verb|SourceFile| & {\em string}  & \pageref{attr:module.source-file} \\
\hline
\verb|Property| & {\tt NoSave}  & \pageref{attr:module.property} \\
\hline
\verb|Prefix|       & {\em identifier} & \\
\verb|Public|       & {\em identifier-list} & \\
\verb|Protected|    & {\em identifier-list} & \\
\hline
\verb|Comment|      & {\em comment string}   & \pageref{attr:prelim.comment}\\
\hline\hline
\end{tabular}
\caption{{\tt Module} attributes}\label{table:module.attr-module}
\end{aimmstable}

\paragraph{The {\tt SourceFile} attribute}
\declattrindex{module}{SourceFile} \AIMMSlink{module.source_file}

Like with ordinary {\tt Section} nodes, the contents of a {\tt Module} node can
also be stored in a separate source file, dynamically linked into a {\tt
Module} node in your model through the use of the {\tt SourceFile} attribute.

\paragraph{Modules and namespaces}
\index{namespace} \index{module!namespace}

The distinguishing feature of modules is that each module is supplied with a
separate namespace. This means that all identifiers, procedures and functions
declared within a module are, without using the module prefix, only visible within that module. In addition, within a module it is possible to redeclare identifier
names that have already been declared outside the module.

\paragraph{Nested modules}
\index{module!nesting} \index{nested modules}

Modules in an AIMMS model can be nested. This implies that with each
AIMMS model containing one or more {\tt Module} nodes, one can associate a
corresponding tree of nested namespaces. This tree of namespaces starts with
the global namespace of the {\tt Model} node as the root node. As a
consequence, you can associate a path of namespaces with every identifier,
procedure or function declaration in the model tree. This path of namespaces
starts with the global namespace down to the namespace associated with the
module in which the declaration is contained.

\paragraph{Scoping rules}
\index{namespace!scoping rules} \index{scoping rules}

When AIMMS encounters an identifier reference during the compilation of a
procedure or function body or in one of the attributes of an identifier
declaration, AIMMS will search for a declaration of the  identifier at hand
in the following order.
\begin{itemize}
\item If the referenced identifier is declared in the namespace associated with
the {\tt Module} (or {\tt Model}) in which the procedure, function or
identifier is contained, AIMMS will use that particular declaration.
\item If the referenced identifier cannot be found, AIMMS will repeatedly search the next higher namespace
until a declaration for the identifier is found.
\end{itemize}

\paragraph{Consequences}

As a result of these scoping rules, whenever the corresponding identifier name
is referenced within a module, AIMMS has will always to refer to the
identifier declaration within the same module rather than to a possibly
contradicting declaration for an identifier with the same name anywhere higher
up, or sideways, in the model tree. This feature enables multiple developers to
work truly independently on different modules used within a model.

\paragraph{Example}

Consider the following model with two (nested) modules, called {\tt Module1}
and {\tt Module2}.
\begin{aimmsfigure}
\psset{xunit=1cm,yunit=1cm,dimen=middle,linewidth=0.6pt}
\begin{pspicture}(0,0)(10,10.25)
\setbox0\hbox to10cm{\vtop{
\begin{example}
    Model TransportModel {
        ...
        Parameter Distance {
            IndexDomain : (i,j);
        }
        Parameter ShortestDistance;
        ...
        Module Module1 {
            Prefix : m1;
            ...
            Parameter ShortestDistance;
            ...
            Procedure ComputeShortestDistance {
                Body : { 
                    ShortestDistance := 
                        min((i,j), Distance(i,j));
                }
            }
            ...
            Module Module2 {
                Prefix : m2;
                ...
                Parameter Distance {
                    Definition : ShortestDistance;
                }
                ...
            }
            ...
        }
        ...
    }
\end{example}}\hss}
   \rput(4.5,5){\box0}
   \psline{->}(6.3,5.2)(10,5.2)(10,9.5)(3.55,9.5)
   \psline{->}(5.2,5.5)(9.45,5.5)(9.45,6.8)(5,6.8)
   \psline{->}(6.5,2.5)(9.55,2.5)(9.55,6.9)(5,6.9)
   \psline[linewidth=4pt,linecolor=red]{-}(0.1,0.55)(0.1,9.9)
   \psline[linewidth=4pt,linecolor=blue]{-}(0.65,1.15)(0.65,7.6)
   \psline[linewidth=4pt,linecolor=green]{-}(1.15,1.75)(1.15,3.65)
\end{pspicture}
\end{aimmsfigure}
The following can be concluded by applying the scoping rules listed above.
\begin{itemize}
\item The reference to {\tt ShortestDistance} in the procedure {\tt ComputeShortestDist\-ance} in the module {\tt Module1} refers to the declaration {\tt ShortestDistance} within that module, and {\em not} to the declaration {\tt ShortestDistance} in the main model.
\item The reference to {\tt Distance} in the procedure {\tt ComputeShortestDistance} in the module {\tt Module1} refers to the declaration {\tt Distance(i,j)} in the main model, and {\em not} to the scalar declaration {\tt Distance} within the nested module {\tt Module2}.
\item The reference to {\tt ShortestDistance} in the module {\tt Module2} refers to the declaration {\tt ShortestDistance} within the module {\tt Module1}, and {\em not} to the declaration {\tt ShortestDistance} in the main model.
\item The parameter {\tt Distance} in the module {\tt Module2} does not conflict with the declaration of {\tt Distance(i,j)} in the main model, because the former is only visible within the scope of the module {\tt Module2}.
\end{itemize}


\paragraph{Accessing protected identifiers}

The separate namespace of every module actively prevents identifiers within a
module from being ``seen'' outside the module. For this reason, identifiers
declared within a module are also referred to as {\em protected} identifiers.
AIMMS, however, still allows you to reference protected identifiers anywhere
else in your model through the use of the {\em namespace resolution operator}
{\tt ::}. In combination with a module-specific prefix, the {\tt ::} operator
accurately lets you indicate that you are referring to a protected identifier
declared in the particular module associated with the prefix.

\paragraph{The {\tt Prefix} attribute}
\declattrindex{module}{Prefix} \AIMMSlink{module.prefix}

With the {\em mandatory} {\tt Prefix} attribute of a {\tt Module} node, you
must specify a module-specific prefix to be used in conjunction with the {\tt
::} operator. The value of the {\tt Prefix} attribute should be a unique name
within the namespace of the surrounding module (or main model), and will
subsequently be added to this namespace. In conjunction with the {\tt ::}
operator the prefix unambiguously identifies the namespace from which a
particular identifier should be taken.

\paragraph{The {\tt ::} namespace resolution operator}
\operindex{::} \index{operator!namespace resolution} \index{namespace
resolution operator}

With the {\em namespace resolution operator} {\tt ::} you instruct AIMMS to
look for the identifier directly following the {\tt ::} operator within the
module associated with the prefix in front it. The {\tt ::} operator may be
optionally surrounded with spaces.  By stacked use of the {\tt ::} operator you
can indicate that you want to refer to an identifier declared in a nested
module. Each next prefix should refer to the {\tt Prefix} attribute of the
module declared directly within the module associated with the previous prefix.

\paragraph{Using global identifiers in {\tt ModuleS}}

If you want to refer to an identifier in the main model, that is also declared
elsewhere along the path from the current module to the main model, you can use
the {\tt ::} operator {\em without a prefix}. This indicates to AIMMS that
you are interested in an identifier declared in the global namespace associated
with the main model.

\paragraph{Examples}
Consider the model outlined in the example above.
\begin{itemize}
\item Within the main model, a reference {\tt m1::ShortestDistance} would refer to the parameter {\tt ShortestDistance} declared within the module {\tt Module1}, and not to the parameter {\tt ShortestDistance} declared in the main model itself.
\item Within the main model, a reference {\tt m1::m2::Distance} would refer to the parameter {\tt Distance} declared in the module {\tt Module2} nested within the module {\tt Module1}.
\item Within the module {\tt Module1}, a reference to {\tt ::ShortestDistance} would refer to the parameter {\tt ShortestDistance} declared in the main model, and not to the parameter {\tt ShortestDistance} declared in {\tt Module1}.
\item Within the module {\tt Module2}, a reference to {\tt ::Distance} would refer to the parameter {\tt Distance} declared in the main model, and not to the parameter {\tt Distance} declared in {\tt Module2}.
\end{itemize}
The following model outline, which is a variation of the model outline of the
previous example, further illustrates the consequences of the use of the {\tt
::} operator.
\begin{aimmsfigure}
\psset{xunit=1cm,yunit=1cm,dimen=middle,linewidth=0.6pt}
\begin{pspicture}(0,0)(10,10.25)
\setbox0\hbox to10cm{\vtop{
\begin{example}
    Model TransportModel {
        ...
        Parameter Distance {
            IndexDomain : (i,j);
        }
        Parameter ShortestDistance;
        ...
        Module Module1 {
            Prefix : m1;
            ...
            Parameter ShortestDistance;
            ...
            Procedure ComputeShortestDistance {
                Body : {
                    ::ShortestDistance := 
                        min((i,j), m2::Distance(i,j));
                }
            }
            ...
            Module Module2 {
                Prefix : m2;
                ...
                Parameter Distance {
                    Definition : ::ShortestDistance;
                }
                ...
            }
            ...
        }
        ...
    }
\end{example}}\hss}
   \rput(4.5,5){\box0}
   \psline{->}(7.0,5.15)(9,5.15)(9,2.84)(4.7,2.84)
   \psline{->}(5.3,5.45)(9.45,5.45)(9.45,8.45)(4.5,8.45)
   \psline{->}(6.8,2.45)(9.55,2.45)(9.55,8.55)(4.5,8.55)
   \psline[linewidth=4pt,linecolor=red]{-}(0.1,0.55)(0.1,9.9)
   \psline[linewidth=4pt,linecolor=blue]{-}(0.65,1.15)(0.65,7.6)
   \psline[linewidth=4pt,linecolor=green]{-}(1.15,1.75)(1.15,3.65)
\end{pspicture}
\end{aimmsfigure}

\paragraph{The {\tt Public} attribute}
\declattrindex{module}{Public} \AIMMSlink{module.public}

Through the {\tt Public} attribute you can indicate that a set of identifiers
declared within the module is public. These identifiers can then be referenced
without the {\tt ::} operator within the importing module (or main model). The
value of the {\tt Public} attribute must be a constant set expression. You
might consider the identifiers specified in the {\tt Public} attribute as the
public interface of a module. As a result, AIMMS will effectively add the
names of these identifiers to the namespace of the importing module, as if they
were declared within the importing module itself.

\paragraph{Example}

Consider the model outline of the first example, and assume that the
declaration of module {\tt Module2} is augmented as follows.
\begin{example}
            Module Module2 {
                Prefix : m2;
                Public : {
                    data { Distance }
                }
                ...
                Parameter Distance {
                    Definition : ShortestDistance;
                }
                ...
            }
\end{example}
As a result of the {\tt Public} attribute, {\tt Distance} will be added to the
namespace of {\tt Module1}, and the compilation of the procedure {\tt
ComputeShortestDistance} will fail because {\tt Distance} will now refer the
scalar declaration in {\tt Module2} rather than to the 2-dimensional
declaration in the main model. In addition, it is possible, within the main
model, to refer to the parameter {\tt Distance} in {\tt Module2} through the
expression {\tt m1::Distance}, because {\tt Distance} has been effectively
added to the namespace of module {\tt Module1}.

\paragraph{Propagation\\ of public identifiers}

When an identifier is added to the {\tt Public} attribute of an imported
module, it is, as explained above, effectively added to the namespace of the
importing module. This creates the possibility to add a public identifier of an
imported module to the {\tt Public} attribute of the importing module as well.
In this way you can propagate the public character of such an identifier to the
next outer namespace. For example, by adding the identifier {\tt Distance} in
the example above, to the {\tt Public} attribute of the module {\tt Module1} as
well, it would also become public in the main model. Obviously, in this case,
adding {\tt Distance} to the {\tt Public} attribute of {\tt Module1} would
cause a name clash with the global identifier {\tt Distance(i,j)}.

\paragraph{The {\tt Protected} attribute}
\declattrindex{module}{Protected} \AIMMSlink{module.protected}

Once you import a module into an existing AIMMS application, one or more
identifiers in the public interface of the imported module can cause name
clashes with existing identifiers in the application, like {\tt Distance} in
the example of previous paragraph. When you run into such a problem, AIMMS
allows you to override the {\tt Public} status of one or more identifiers of a
module through its {\tt Protected} attribute. The value of the {\tt Protected}
attribute must be a constant set expression, and its contents must be a subset
of the set of identifiers specified in the {\tt Public} attribute. By adding an
identifier to the {\tt Protected} attribute, it is, again, only accessible
outside of the module by using the {\tt ::} operator.

\paragraph{{\tt Public} versus {\tt Protected} responsibilities}

The responsibilities for specifying the {\tt Public} and {\tt Protected}
attributes are substantially different, and result in a different storage of
the values of these attributes. This is similar to the {\tt SouceFile}-related 
attributes discussed earlier in this chapter. The following rules apply.
\begin{itemize}
\item The {\tt Public} attribute is intended for the {\em developer} of a module to define a public interface to the module. If the module is stored in a separate {\tt .amb} file, to be imported by other AIMMS applications, the contents of the {\tt Public} attribute is stored inside the module-specific {\tt .amb} file.
\item The {\tt Protected} attribute is intended for the {\em user} of a module to override the public character of certain identifiers as specified by the developer of the module. As the contents of the {\tt Protected} attribute is not an integral part of the module, but may be specified differently by every user of the module, it is never stored in a module-specific {\tt .amb} file, but rather in the importing module or main model.
\end{itemize}

\paragraph{Unique global representation}

For each identifier in an AIMMS model, there is a unique global
representation. If the identifier is contained in the global namespace of the
main model, the global representation is the identifier name itself. If an
identifier is only contained in the namespace of a particular module, its
unique representation based on the namespace {\tt Prefix} of the module and the
{\tt ::} operator. Thus, for the first example of this section (without {\tt
Public} attributes), the unique global representations of all identifiers are:
\begin{itemize}
\item {\tt Distance(i,j)}
\item {\tt ShortestDistance}
\item {\tt m1::ShortestDistance}
\item {\tt m1::ComputeShortestDistance}
\item {\tt m1::m2::Distance}
\end{itemize}
With the {\tt Public} attribute of {\tt Module2} defined as in the previous
example, the unique global representation of the parameter {\tt Distance} in
{\tt Module2} becomes {\tt m1::Distance}, as it effectively causes {\tt
Distance} to be contained in the namespace of {\tt Module1}.

\paragraph{Display and data transfer}

Whenever AIMMS is requested to {\tt DISPLAY} or {\tt WRITE} the contents of
one or more identifiers in your model, it will use the unique global
representation discussed in the previous paragraph. Also, when you {\tt READ}
data from a file, AIMMS expects all identifiers for which data is provided
in the file to be identified by their unique global representation.
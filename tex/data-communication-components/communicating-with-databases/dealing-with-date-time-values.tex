\section{Dealing with date-time values}\label{sec:db.date-time}%
\index{database!date-time values}%
\index{calendar!communicating with databases}

\paragraph{Mapping date-time values}

Special care is required when you want to read data from or write data
to a database which represents a date, a time, or a time stamp in the database table. The
ODBC technology uses a fixed string format for each of these data types. Most
likely, this format will not coincide with the format that you use to
store dates and times in your modeling application.

\paragraph{Mapped onto calendars}

When a column in a database table containing date-time values maps
onto a {\tt Calendar} in your AIMMS model, AIMMS will
automatically convert the date-time values to the associated time slot
format of the calendar, and store the corresponding values for the
appropriate time slots.

\paragraph{Time zone translation}

By default, AIMMS assumes that the date-time values mapped onto a particular {\tt CALENDAR} are stored in the database according to the same time zone (ignoring daylight saving time) as specified in the {\tt TimeslotFormat} attribute of that calendar (see also Section~\ref{sec:time.format.dst}). In the absence of such a time zone specification, AIMMS will assume the local time zone (without daylight saving time). You can override the time zone through the {\tt TimeslotFormat} attribute of a {\tt Convention}. The use of {\tt Conventions} with respect to {\tt Calendar} is discussed in full detail in Section~\ref{sec:time.tz}.

\paragraph{The {\tt ODBCDateTimeFormat} parameter}%
\preparindex{ODBCDateTimeFormat}

If a date-time column in a database table does not map onto a {\tt
Calendar} in your model, you can still convert the ODBC date-time
format into the date- or time representation of your preference, using
the predefined string parameter {\tt ODBCDateTimeFormat} defined over
the set of {\tt AllIdentifiers}. With it, you can specify, on a per
identifier basis, the particular format that AIMMS should use to
store dates and/or times using the formats discussed in
Section~\ref{sec:time.format}. AIMMS will never perform a time zone conversion for non-calendar data, and will ignore {\tt ODBCDateTimeFormat} when it contains a date-time format specification for a {\tt CALENDAR}.

\paragraph{Unmapped columns}

If you do not specify a date-time format for a particular identifier,
and the column does not map onto a {\tt Calendar}, AIMMS will
assume the fixed ODBC format. These formats are:
\begin{itemize}
\item {\em YYYY-MM-DD hh:mm:ss.tttttt} for date-time columns,
\item {\em YYYY-MM-DD} for date columns, and
\item {\em hh:mm:ss} for time columns.
\end{itemize}
When you are unsure about the specific type of a date/time/date-time
column in the database table during a {\tt WRITE} action, you can
always store the AIMMS data in date-time format, as AIMMS can
convert these to both the date and time format. During a {\tt READ}
action, AIMMS will always translate into the type for the column type.

\paragraph{Example}

A stock ordering model contains the following identifiers:
\begin{itemize}
\item the set {\tt Products} with index {\tt p}, containing all
products kept in stock,
\item an ordinary set {\tt OrderDates} with index {\tt d}, containing
all ordering dates, and
\item a string parameter {\tt ArrivalTime(p,d)} containing the
arrival time of the goods in the warehouse.
\end{itemize}
The order dates should be of the format '{\tt 140319}', whilst the
arrival times should be formatted as '{\tt 12:30 PM}' or '{\tt 9:01
AM}'. Using the time specifiers of Section~\ref{sec:time.format}, you
can accomplish this through the following assignments to the
predefined parameter {\tt ODBCDateTimeFormat}:
\begin{example}
    ODBCDateTimeFormat( 'OrderDates' )  := "%y%m%d";
    ODBCDateTimeFormat( 'ArrivalTime' ) := "%h:%M %p";
\end{example}

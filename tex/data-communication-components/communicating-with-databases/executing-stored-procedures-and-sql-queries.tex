\section{Executing stored procedures and SQL queries}\label{sec:db.stored-procedures}%
\index{database!stored procedure}%
\index{database!SQL query}%
\index{SQL query}%
\index{database procedure}
\index{procedure!database}

\paragraph{Sophisticated control}

When transferring data from or to a database table, you may need more
sophisticated control over the data link than offered by the standard
{\tt DatabaseTable} interface. AIMMS offers you this additional
control by letting you have access to stored procedures as well as
letting you execute SQL statements directly. The following two
paragraphs provide some examples where such control may be useful.

\paragraph{Useful for data processing}

Your application may require its data in a somewhat different form
than is directly available in the database. In this case you may have
to perform some pre-processing of the data in the database. Similarly,
you may want to perform post-processing in the database after writing
data to it. In such circumstances you may call a stored procedure to
perform these tasks for you.

\paragraph{Useful for dynamic access}
\index{use of!stored procedure}
\index{stored procedure!use of}

In some cases, the required data for your application may need to be
the result of a parameterized query of the database, i.e.\ a
database table whose contents is dependent on one or more parameters
which are only known during runtime. Such dynamic tables are usually
obtained as the {\em result set} of a stored procedure or of a
parameterized query. In this case AIMMS will allow you to use a
stored procedure call or a dynamically composed SQL query inside the
{\tt READ} statement as if it were a database table. Please note
that it's currently not possible to read a result set from an Oracle
stored procedure, since Oracle uses a non-standard mechanism for
that (involving so-called {\em ref cursors}).

\paragraph{The {\tt DatabaseProcedure} declaration}
\declindex{DatabaseProcedure}
\AIMMSlink{database_procedure}

Every stored procedure or SQL query that you want to call from within
AIMMS must be declared as a {\tt DatabaseProcedure} within your
application. The attributes of a {\tt DatabaseProcedure} are listed
in Table~\ref{table:db.attr-db-procedure}.
\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value type} & {\bf See also} \\
                &                  & {\bf page} \\
\hline
{\tt DataSource} & {\em string} & \pageref{attr:db.data-source} \\
{\tt Arguments}   & {\em argument-list} & \pageref{sec:intern.ref} \\
{\tt StoredProcedure} & {\em string-expression} & \\
{\tt SQLQuery}        & {\em string-expression} & \\
{\tt Owner}            & {\em string-expression} & \pageref{attr:db.owner}\\
{\tt Property} & {\tt UseResultSet} & \\
{\tt Mapping}  & {\em mapping-list} & \pageref{attr:db.mapping} \\
%\verb|TEXT|             & {\em string}      & \pageref{attr:prelim.text} \\
\verb|Comment|          & {\em comment string} & \pageref{attr:prelim.comment} \\
\verb|Convention|       & {\em convention} &
\pageref{attr:db.convention}, \pageref{sec:units.convention} \\
\hline\hline
\end{tabular}
\caption{{\tt DatabaseProcedure} attributes}\label{table:db.attr-db-procedure}
\end{aimmstable}

\paragraph{SQL query or stored procedure}

A {\tt DatabaseProcedure} in AIMMS can represent either a
(dynamically created) SQL query or a call to a stored procedure.
AIMMS makes the distinction on the basis of the {\tt StoredProcedure} and {\tt SQLQuery} attributes. If the {\tt StoredProcedure} attribute is nonempty, AIMMS assumes that the {\tt
DatabaseProcedure} represents a stored procedure and expects the {\tt
SQLQuery} attribute to be empty, and vice versa.

\paragraph{The {\tt StoredProcedure} attribute}\herelabel{attr:db.proc.actual-name}%
\declattrindex{database procedure}{StoredProcedure}%
\declattrindex{database procedure}{Owner}
\AIMMSlink{database_procedure.stored_procedure}
\AIMMSlink{database_procedure.owner}

With the {\tt StoredProcedure} attribute you can specify the name of
the stored procedure within the ODBC data source that you want to be
called. The {\tt Stored\-Procedure} wizard will let you select any
stored procedure name available within the specified ODBC data source.
If the stored procedure that you want to call is not owned by
yourself, or if there are name conflicts, you should specify the owner
with the {\tt Owner} attribute.

\paragraph{The {\tt SQLQuery} attribute}%
\declattrindex{database procedure}{SQLQuery}
\AIMMSlink{database_procedure.sql_query}

You can use the {\tt SQLQuery} attribute to specify the SQL query
that you want to be executed when the {\tt DatabaseProcedure} is
called. The value of this attribute can be any string expression,
allowing you to generate a dynamic SQL query using the arguments of
the {\tt DatabaseProcedure}.

\paragraph{The {\tt Arguments} attribute}\herelabel{attr:db.proc.arguments}%
\declattrindex{database procedure}{Arguments}
\AIMMSlink{database_procedure.arguments}

With the {\tt Arguments} attribute you can indicate the list of {\em
scalar} arguments of the database procedure. The specified arguments
must have a matching declaration in a declaration section local to the
{\tt DatabaseProcedure}. If the {\tt Database\-Procedure} represents a
stored procedure, the argument list is interpreted as the argument
list of the stored procedure. When you use the {\tt StoredProcedure}
wizard, AIMMS will automatically enter the argument list, including
their AIMMS prototype, for you. For a {\tt DatabaseProcedure}
representing an SQL query, you can use the arguments in composing the
SQL query string.

\paragraph{Input-output type}
\index{database procedure!input-output type}

For SQL queries all arguments must be {\tt Input} arguments, as the
query cannot modify them. For stored procedures, the {\tt StoredProcedure} wizard will by default set the input-output type of each
argument equal to its SQL input-output type. However, if you want to
discard the result of any output argument, you can change its type to
{\tt Input}.

\paragraph{The {\tt Property} attribute}\herelabel{attr:db.proc.property}%
\declattrindex{database procedure}{Property}%
\declpropindex{database procedure}{UseResultSet}
\AIMMSlink{database_procedure.property}
\AIMMSlink{useresultset}

With the {\tt Property} attribute of a {\tt DatabaseProcedure} you
can indicate the intended use of the procedure.
\begin{itemize}
\item When you do not specify the property {\tt UseResultSet}, AIMMS
lets you call the {\tt DatabaseProcedure} as if it were an AIMMS
procedure.
\item When you do specify the property {\tt UseResultSet}, AIMMS
lets you use the {\tt DatabaseProcedure} as a parameterized table in
the {\tt READ} statement. In that case, you can also provide a {\tt
Mapping} attribute to specify the mapping from column names in the
result set onto the corresponding AIMMS identifiers.
\end{itemize}

\paragraph{Stored procedure examples}%
\index{database procedure!example of use}%
\index{procedure!database}
\index{stored procedure!example of use}%
\subtypindex{statement}{READ}{result of stored procedure}

The following declarations will make two stored procedures contained
in the data source ``{\tt Topological Data}'' available in your
AIMMS application. The local declarations of all arguments are
omitted for the sake of brevity. They are all assumed to be {\tt
Input} arguments.
\begin{example}
DatabaseProcedure StoreSingleTransport {
    DataSource       : "Topological Data";
    StoredProcedure  : "SP_STORE_SINGLE_TRANSPORT";
    Arguments        : (from, to, transport);
}
DatabaseProcedure SelectTransportNetwork {
    DataSource       : "Topological Data";
    StoredProcedure  : "SP_DISTANCE";
    Arguments        : MaxDistance;
    Property         : UseResultSet;
    Mapping          : {
        "from"        --> i,
        "to"          --> j,
        "dist"        --> Distance(i,j),
        ("from","to") --> Routes
    }
}
\end{example}
The procedure {\tt StoreSingleTransport} can be used like any other
AIMMS procedure, as in the following statement.
\begin{example}
    StoreSingleTransport( 'Amsterdam', 'Rotterdam',
                          Transport('Amsterdam', 'Rotterdam') );
\end{example}
The second procedure {\tt SelectTransportNetwork} can be used in a
{\tt READ} statement as if it were a database table, as illustrated
below.
\begin{example}
    read from table SelectTransportNetwork( UserSelectedDistance );
\end{example}

\paragraph{SQL query example}
\index{sql query!example of use}%

The following example illustrates the declaration of a {\tt DatabaseProcedure} representing a direct SQL query. Its aim is to delete those
records in the specified table for which the column {\tt VersionCol}
equals the specified version. Both arguments must be declared as local
{\tt Input} string parameters.
\begin{example}
DatabaseProcedure DeleteTableVersion {
    DataSource  : "Topological Data";
    Arguments   : (DeleteTable, DeleteVersion);
    SQLQuery    : {
        FormatString( "DELETE FROM %s WHERE VersionCol = '%s'",
                      DeleteTable, DeleteVersion )
    }
}
\end{example}

\paragraph{Executing SQL statements directly}

In addition to executing SQL queries through {\tt DatabaseProcedure}, AIMMS also allows you to execute SQL statements
directly within a data source. The interface for this mechanism is
simple, and forms a convenient alternative for a {\tt DatabaseProcedure} when you want to execute a single SQL statement only once.

\paragraph{The procedure {\tt DirectSQL}}%
\procindex{DirectSQL}
\AIMMSlink{directsql}

You can send SQL statements to a data source by calling the
procedure {\tt Direct\-SQL} with the following prototype:
\begin{itemize}
\item {\tt DirectSQL}({\em data-source}, {\em SQL-string})
\end{itemize}
Both arguments of the procedure should be string expressions. Note
that in case the SQL statement also produces a result set, then this
set is ignored by AIMMS.

\paragraph{Example}

The following call to {\tt DirectSQL} drops a table called {\tt
"Temporary\_Table"} from the data source {\tt "Topological Data"}.
\begin{example}
    DirectSQL( "Topological Data",
               "DROP TABLE Temporary_Table" );
\end{example}

\paragraph{Use {\tt FormatString}}%
\funcindex{FormatString}

The procedure {\tt DirectSQL} does not offer direct capabilities for
parameterizing the SQL string with AIMMS data. Instead, you can
use the function {\tt FormatString} to construct symbolic SQL
statements with terms based on AIMMS identifiers.


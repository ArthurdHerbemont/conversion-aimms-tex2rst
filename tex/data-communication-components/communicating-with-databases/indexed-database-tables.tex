\section{Indexed database tables}\label{sec:db.indexed-table}

\paragraph{Exogenous columns in primary key}

While the {\tt Mapping} attribute allows you to map data columns in a
database table onto a slice of a higher-dimensional AIMMS
identifier, a different type of slicing is required when the primary
key of a database table contains {\em exogenous} columns that are of
no interest to your application. Consider, for instance, the following
situations.
\begin{itemize}
\item You are linking to a database table that contains data for a
huge set of cities, but your model only deals with a single city that
is not explicitly part of the model formulation. For your application
the city column is exogenous.
\item A table in a database contains several versions of a particular
data set, where the version number is represented by an additional
version column in the table. For your application the version column
is exogenous.
\end{itemize}

\paragraph{Indexed {DatabaseTables}}%
\index{database table!indexed}\index{indexed database table}
\declattrindex{database table}{IndexDomain}
\AIMMSlink{database_table.index_domain}

In your AIMMS application you can deal with these situations by
partitioning a single table inside the database into a set of {\em
virtual} lesser-dimensional tables indexed by the exogenous
column(s). You can do this by declaring the database table to have an
{\tt IndexDomain} corresponding to the sets that map onto the
exogenous columns. In subsequent {\tt READ} and {\tt WRITE}
statements you can then refer to a particular instance of a virtual
table through a reference to the database table with an explicit set
element or an element parameter.

\paragraph{Example}

The following example assumes that the table {\tt "Route Definition"}
contains several versions of the data, each identified by the value of
an additional column {\tt version}. In the AIMMS model, this column
is associated with a set {\tt TableVersions} given by the following
declaration.
\begin{example}
Set TableVersions {
    Index      : v;
    Parameter  : LatestVersion;
}
\end{example}
The following declaration will provide a number of virtual tables
indexed by {\tt v}.
\begin{example}
DatabaseTable RouteData {
    IndexDomain  :  v;
    DataSource   :  "Topological Data";
    TableName    :  "Route Definition";
    Mapping      : {
        "version"   --> v,
        "from"      --> i,
        "to"        --> j,
        "dist"      --> Distance(i,j),
        "cost"      --> TransportCost(i,j)
    }
}
\end{example}
Note that the index {\tt v} in the index domain is mapped onto the
column {\tt version} in the table.

\paragraph{Data transfer with indexed tables}

In order to obtain the set of {\tt TableVersions} you can follow one
of two strategies:
\begin{itemize}
\item you can obtain the set of the available versions from the table
{\tt "Route Definition"} itself by declaring another {\tt
DatabaseTable} in AIMMS
\begin{example}
DatabaseTable VersionTable {
    DataSource   :  "Topological Data";
    TableName    :  "Route Definition";
    Mapping      : {
        "version"   --> TableVersions
    }
}
\end{example}
\item or, you can obtain the versions from a separate table in a relational
database declared similarly as above.
\end{itemize}
A typical sequence of actions for data transfer with indexed tables
could then be the following.
\begin{itemize}
\item Read the set of all possible versions from {\tt VersionTable}:
\begin{example}
    read TableVersions from table VersionTable ;
\end{example}
\item Obtain the value of {\tt LatestVersion} from within the language
or the graphical user interface.
\item Read the data accordingly:
\begin{example}
    read Distance, TransportCost from RouteData(LatestVersion) ;
\end{example}
\end{itemize}
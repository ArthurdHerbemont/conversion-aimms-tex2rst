\section{The \tttext{DatabaseTable} declaration}\label{sec:db.database-table}%
\declindex{DatabaseTable}
\AIMMSlink{database_table}

\tipparagraph{Database tables}{database-table}

\syntaxmark{database-table}
You can make a database table known to AIMMS by means of a {\tt
DatabaseTable} declaration in your application. Inside this
declaration you can specify the ODBC data source name of the database
and the name of the database table from which you want to read, or to
which you want to write. The list of attributes of a {\tt DatabaseTable} is given in Table~\ref{table:db.attr-db-table}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}  & {\bf See also}\\
&& {\bf page}\\
\hline
{\tt IndexDomain}      & {\em index-domain} & \pageref{attr:par.index-domain} \\
{\tt DataSource}       & {\em string-expression}       & \\
\verb|TableName|       & {\em string-expression}       & \\
\verb|Owner|            & {\em string-expression}       & \\
\verb|Property|         & {\tt ReadOnly} &  \\
\verb|Mapping|          & {\em mapping-list}            & \\
\verb|Text|             & {\em string}                  &
\pageref{attr:prelim.text} \\
\verb|Comment|          & {\em comment string}         & \pageref{attr:prelim.comment} \\
\verb|Convention|          & {\em convention}         & \pageref{sec:units.convention} \\
\hline\hline
\end{tabular}
\caption{{\tt DatabaseTable} attributes}\label{table:db.attr-db-table}
\end{aimmstable}

\paragraph{The {\tt DataSource} attribute}\herelabel{attr:db.data-source}%
\declattrindex{database table}{DataSource}
\AIMMSlink{database_table.data_source}

The mandatory {\tt DataSource} attribute specifies the ODBC data
source name of the database you want to link with. Its value must be a
string or a string parameter. If you are unsure about the data source
name by which a particular database is known, AIMMS will help you.
While completing the declaration form of a database table, AIMMS
will automatically let you choose from the available data sources on
your system using the {\tt DataSource} wizard. AIMMS supports the following data source types:
\begin{itemize}
\item ODBC file data sources ({\tt .dsn} extension, only available on Windows), and
\item ODBC user and system data sources (no extension), and 
\item ODBC connection string.
\end{itemize}
In addition, you can specify the name of an AIMMS string parameter, holding the name of any of the above data source types. If the data source you are looking for is not available in this list, you can set up a link to that database from within the wizard. 

\paragraph{The {\tt TableName} attribute}\herelabel{attr:db.table-name}%
\declattrindex{database table}{TableName}
\AIMMSlink{database_table.table_name}

With the {\tt TableName} attribute you must specify the name of the
table or view within the data source to which the {\tt DatabaseTable} is mapped. Once you have provided the {\tt DataSource}
attribute, the {\tt TableName} wizard will let you select any table
or view available in the specified data source.

\paragraph{Example}

The following declaration illustrates the simplest possible {\tt
DatabaseTable} declaration.
\begin{example}
DatabaseTable RouteData {
    DataSource  :  "Topological Data";
    TableName   :  "Route Definition";
}
\end{example}
It will connect to an ODBC user or system data source called ``{\tt Topological
Data}'', and in that data source search for a table named ``{\tt Route
Definition}''.

\paragraph{The {\tt Owner} attribute}\herelabel{attr:db.owner}%
\declattrindex{database table}{Owner}
\AIMMSlink{database_table.owner}

The {\tt Owner} attribute is for advanced use only. By default, when
connecting to a database server, you will have access to all tables
and stored procedures which are visible to you. In case a table name
appears more than once, but is owned by different users, by default a
connection is made to the table instance owned by yourself. By
specifying the {\tt Owner} attribute you can gain access to the table
instance owned by the indicated user.

\paragraph{The {\tt Property} attribute}\herelabel{attr:db.property}
\declattrindex{database table}{Property}
\AIMMSlink{database_table.property}
\declpropindex{database table}{ReadOnly}
\declpropindex{database table}{No Implicit Mapping}

With the {\tt Property} attribute of a {\tt DatabaseTable} you can
specify whether the declared table is {\tt ReadOnly}. Specifying a
database table as {\tt ReadOnly} will prevent you from inadvertently
modifying its content. If you do not provide this property, the
database table will default to read-write permissions unless the
server does not allow write access.

\paragraph{The {\tt Mapping} attribute}\herelabel{attr:db.mapping}%
\declattrindex{database table}{Mapping}
\AIMMSlink{database_table.mapping}
\index{database table!mapping column names}
\index{mapping column names in databases}

\syntaxmark{column-name}
By default, AIMMS tries to map the column names used in a database
table onto the AIMMS identifiers of the same name. Such an implicit mapping is, of
course, not always possible. When you link to an existing database
that was not specifically designed for your AIMMS application, it
is very likely that the column names do not correspond to the names of
your AIMMS identifiers. Therefore, the {\tt Mapping} attribute lets
you override this default. The database columns explicitly mapped through the {\tt Mapping} attribute are added to the set of implicit mappings constructed by AIMMS.
The column names from the database table
used in a mapping list must be quoted. 
If the implicit mapping is not desirable you can provide the property {\tt No Implicit Mapping}.  

\paragraph{Example}

The following declarations demonstrate the use of mappings in a {\tt
DatabaseTable} declaration. This example assumes the set and parameter
declarations of Section~\ref{sec:rw.example} plus the existence of the
relation {\tt Routes} given by
\begin{example}
Set Routes {
    SubsetOf     : (Cities, Cities);
}
\end{example}
The following mapped database declaration will take care of the
necessary column to identifier mapping.
\begin{example}
DatabaseTable RouteData {
    DataSource   :  "Topological Data";
    TableName    :  "Route Definition";
    Mapping      : {
        "from"        --> i,                              ! name substitution
        "to"          --> j,
        "dist"        --> Distance(i,j),

        "fcost"       --> TransportCost(i,j,'fixed'),     ! slicing
        "vcost"       --> TransportCost(i,j,'variable'),

        ("from","to") --> Routes                          ! mapping to relation
    }
}
\end{example}

\paragraph{Name substitution}

The first three lines of the {\tt Mapping} attribute provide a simple
name translation from a column in the database table to an AIMMS
identifier. You can only use this type of mapping if the structural
form of the database table (i.e.\ the primary key) coincides with the
domain of the AIMMS identifier.

\paragraph{Mapping columns to slices}

If the number of attributes in the primary key of a database table is
lower than the dimension of the intended AIMMS identifier, you can
also map a column name to a {\em slice} of an AIMMS identifier of
the proper dimension, as shown in the {\tt fcost} and {\tt vcost}
mapping. You can do this by replacing one or more of the indices in
the identifier's index space with a reference to a fixed element.

\paragraph{Mapping primary key to relation}

As shown in the last line of the {\tt Mapping} attribute, you can let the
complete primary key in a database table correspond with a simple set, or
with a relation (see Section~\ref{sec:set.relation}) in AIMMS. This
correspondence is specified by mapping the tuple of primary attributes of
the table onto the AIMMS set itself, or onto an index into this set.
The primary attributes in the tuple are mapped in a one-to-one fashion
onto the indices in the relation.

\paragraph[.75]{Syntax}
\operindex{-{}->}

The syntax of the {\tt Mapping} attribute is given by the following diagram.
\vskip.5\baselineskip
\begin{syntax}
\syntaxdiagram{mapping-list}{map-list}
\end{syntax}

\paragraph{The {\tt Convention} attribute}\herelabel{attr:db.convention}%
\declattrindex{database table}{Convention}
\typindex{identifier}{Convention}

With the {\tt Convention} attribute you can indicate to AIMMS that
the external data is stored with the units provided in the specified
convention. If the unit specified in the convention differs from the
unit that AIMMS uses to store its data internally, the data is
scaled at the time of transfer. For the declaration of {\tt
Conventions} you are referred to Section~\ref{sec:units.convention}.

\paragraph{Date conversions}

In addition, you can use {\tt Conventions} to convert calendar data from
the calendar slot format used within your model to the format expected by
the database and vice versa. The use of {\tt Conventions} for this purpose
is discussed in full detail in Section~\ref{sec:time.tz}. For non-calendar
related date-time values you can use the predefined identifier {\tt
OBDCDateTimeFormat} to accomplish this (see
Section~\ref{sec:db.date-time}).

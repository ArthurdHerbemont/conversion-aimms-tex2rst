\section{A basic example}\label{sec:rw.example}

\paragraph{Getting started}
\subtypindex{statement}{READ}{example of use}

The aim of this section is to give you an overview of the {\tt READ}
and {\tt WRITE} statements through a short illustrative example. It
shows how to read data from and write data to text files
and database tables. It is based on the familiar transport
problem with the following input data:
\begin{itemize}
\item the set {\tt Cities},
\item the relation {\tt Routes} from {\tt Cities} to {\tt Cities},
\item the parameters {\tt Supply(i)} and {\tt Demand(i)} for each city
{\tt i}, and
\item the parameters {\tt Distance(i,j)} and {\tt
TransportCost(i,j)} for each route between two cities {\tt i} and {\tt
j}.
\end{itemize}
For the sake of simplicity, it is assumed that there is only a single
output, the actual {\tt Transport(i,j)} along each route.

\paragraph{Format of input data}

The input data can be conveniently given in the form of tables. One
for the identifiers defined over a single city like {\tt Supply} and
{\tt Demand}, and the other for the identifiers defined over a tuple
of cities like {\tt Distance} and {\tt TransportCost}. These tables
can be provided in the form of text files as in
Table~\ref{examp:rw.examp-data} (format explained in
Section~\ref{sec:text.composite}). Alternatively, the data
% can be contained in binary AIMMS case files, or
can be obtained from
particular tables in a database. This example assumes the
following database tables exist:
\begin{itemize}
\item {\tt CityData} for the one-dimensional parameters, and
\item {\tt RouteData} for the two-dimensional parameters.
\end{itemize}
\begin{aimmstable}
\keyindex{COMPOSITE TABLE}

\begin{minipage}[t]{0.35\textwidth}
\begin{example}
COMPOSITE TABLE
    Cities      Supply  Demand
!   ----------  ------  ------
    Amsterdam     50
    Rotterdam    100
    Antwerp       75      25
    Berlin               125
    Paris                 75
;
\end{example}
\end{minipage}

\begin{minipage}[t]{0.54\textwidth}
\begin{example}
COMPOSITE TABLE
    i          j          Distance  TransportCost
!   ---------  ---------  --------  -------------
    Amsterdam  Rotterdam       85        1.00
    Amsterdam  Antwerp        170        2.50
    Amsterdam  Berlin         660       10.00
    Amsterdam  Paris          510        8.25
    Rotterdam  Antwerp        100        1.20
    Rotterdam  Berlin         700       10.00
    Rotterdam  Paris          440        7.50
    Antwerp    Berlin         725       11.00
    Antwerp    Paris          340        5.00
    Berlin     Paris         1050       17.50
;

\end{example}
\end{minipage}

\caption{Example data set for the transport model}\label{examp:rw.examp-data}
\end{aimmstable}

\subsection{Simple data transfer}\label{sec:rw.example.simple}

\paragraph{Simple data initialization}

The simplest use of the {\tt READ} statement is to initialize data
from a fixed name text data file,
% a binary case file,
or a database table. To read all the data from each source, the
following groups of statements will suffice
%, where {\tt
%Selected\-CaseFile} is an element parameter into the set of {\tt
%AllDataFiles}.
\begin{example}
    read from file  "transport.inp" ;

    read from table CityData;
    read from table RouteData;
\end{example}
Such statements are typically found in the body of the predefined
procedure {\tt MainInitialization}.

\paragraph{Reading identifier selections}

When a data source also contains data for identifiers that are of no
interest to your particular application (but may be to others),
AIMMS allows you to restrict the data transfer to a specific
selection of identifiers in that data source. For instance, the
following {\tt READ} statement will only read the identifiers {\tt
Distance} and {\tt TransportCost}, not changing the current contents
of the AIMMS identifiers {\tt Supply} and {\tt Demand}.
\begin{example}
    read Distance, TransportCost from file "transport.inp" ;
\end{example}
Similar identifier selections are possible when reading from
% either a binary case file or
a database table.

\paragraph{Writing the solution}
\subtypindex{statement}{WRITE}{example of use}

After your model has computed the optimal transport, you may want to
write the solution {\tt Transport(i,j)} to an text output file for
future reference. You can do this by calling the {\tt WRITE}
statement, which has equivalent syntax to the {\tt READ} statement.
The transfer of {\tt Transport(i,j)} to the file {\tt transport.out}
is accomplished by the following {\tt WRITE} statement.
\begin{example}
    write Transport to file "transport.out" ;
\end{example}
If you omit an identifier selection, AIMMS will write all model
data to the file. When writing to a database table, AIMMS can of
course only transfer data for those identifiers that are known in the
table that you are writing to.

\paragraph{File name need not be explicit}

File data transfer is not restricted to files with a fixed name. To
choose the name of the data file either during execution or from
within the end-user interface, you have several options:
\begin{itemize}
\item replace the filename string in the {\tt READ} and {\tt WRITE} statements
with a string-valued parameter holding the filename, or
\item use a {\tt File} identifier (for text files only).
\end{itemize}

\subsection{Set initialization and domain checking}\label{sec:rw.example.domain}

\paragraph{Domain restrictions}
\index{initialization!enforcing domain restriction}
\index{data!enforcing domain restriction}

When you are reading the initial data of the transport model from an
external data source several situations can occur:
\begin{itemize}
\item you just want to initialize the set {\tt Cities} from the data source,
\item the set {\tt Cities} has already been initialized, and you want to
retrieve the parametric data for existing cities only, or
\item the set {\tt Cities} has already been initialized, but you want to
extend it on the basis of the data read from the external data source.
\end{itemize}

\paragraph{The {\tt READ} statements}
\subtypindex{statement}{READ}{FILTERING clause@{{\tt FILTERING} clause}}

The following statements impose domain restrictions on the {\tt READ}
statement.
\begin{example}
    read Cities
         from file "transport.inp" ;

    read Supply, Demand
         from file "transport.inp"
         filtering i ;

    read Supply, Demand
         from file "transport.inp" ;
\end{example}

\paragraph{Initializing sets}

The first {\tt READ} statement is a straightforward initialization of
the set {\tt Cities}. By default, AIMMS reads in replace mode,
which implies that any previous contents of the set {\tt Cities} is
overwritten.

\paragraph{Domain checking}
\index{domain!checking}

The second {\tt READ} statement assumes that the set {\tt Cities} has already
been initialized. From all entries of the identifiers {\tt Supply} and {\tt
Demand} it will only read those which correspond to existing elements in the
set {\tt Cities}, and skip over the data from the remaining entries.

\paragraph{Extending domain sets}
\index{domain!extending}

The third {\tt READ} statement differs from the second in that the
clause {\tt FILTERING i} has been omitted. As a result, AIMMS will
not reject data that does not correspond to an existing label in the
set {\tt Cities}, but will read all available {\tt Supply} and {\tt
Demand} data, and extend the set {\tt Cities} accordingly.


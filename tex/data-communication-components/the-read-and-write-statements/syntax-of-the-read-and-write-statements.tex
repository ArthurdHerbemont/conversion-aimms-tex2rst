\section{Syntax of the \tttext{READ} and \tttext{WRITE} statements}\label{sec:rw.read-write}

\paragraph{{\tt READ} and {\tt WRITE} statements}
\statindex{READ}
\statindex{WRITE}
\AIMMSlink{read}
\AIMMSlink{write}

In {\tt READ} and {\tt WRITE} statement you can specify the data
source type, what data will be transferred, and in what mode. The
syntax of the statements reflect these aspects.

\paragraph[0.7]{Syntax}
\begin{syntax}
\syntaxdiagram{read-write-statement}{read-write}
\syntaxdiagram{selection}{record-selection}
\end{syntax}

\paragraph{Data sources}
\subtypindex{statement}{READ}{allowed data source}
\subtypindex{statement}{WRITE}{allowed data source}
\index{data source!of READ and WRITE statements@{of {\tt READ} and
{\tt WRITE} statements}}
\typindex{identifier}{FILE}
\presetindex{AllDataFiles}
\typindex{identifier}{DatabaseTable}
% \index{case!use in READ and WRITE statements@{use in {\tt READ} and
% {\tt WRITE} statements}}

\syntaxmark{data-source}
The data source of a {\tt READ} or {\tt
WRITE} statement in AIMMS can be either
\begin{itemize}
\item a {\tt File} represented by either
\begin{itemize}
\item a {\tt File} identifier,
\item a string constant, or
\item a scalar string reference,
\end{itemize}
%\item a {\tt CASE} is represented by an element into the set {\tt
%AllDataFiles},
\item a {\tt TABLE} represented by either
\begin{itemize}
\item a {\tt DatabaseTable} identifier,
\item an element parameter with a range that is a subset of the predeclared set {\tt
AllDatabaseTables}
\end{itemize}
\end{itemize}
Strings for file data sources refer either to an absolute path or to a
relative path. All relative paths are taken relative to the project
directory.
% The elements of the predefined set {\tt AllDataFiles}
% refer to cases and datasets created in the AIMMS data manager tool.
% AIMMS provides a number of functions that you use within your
% model to associate a case or dataset name with an element of {\tt
% AllDataFiles}, or to create new cases or datasets from within your
% model. A description of these functions can be found in the AIMMS
% User's Guide.

\paragraph{Examples}

Assuming that {\tt UserSelectedFile} is a {\tt File} identifier,
and {\tt UserFilename} a string parameter, then the following statements
illustrate the use of strings and {\tt File} identifiers.
\begin{example}
     read from file "C:\Data\Transport\initial.dat" ;
     read from file "data\initial.dat" ;
     read from file UserFileName ;
     read from file UserSelectedFile ;
\end{example}

\paragraph{Specifying a selection}
\subtypindex{statement}{READ}{identifier selection}
\subtypindex{statement}{WRITE}{identifier selection}
\index{identifier selection!of READ and WRITE statements@{of {\tt READ} and
{\tt WRITE} statements}}

The {\em selection} in a {\tt READ} or {\tt WRITE} statement
determines which data you want to transfer from or to a text file,
or database table. A selection is a list of
references to sets, parameters, variables and constraints. During a
{\tt WRITE} statement, AIMMS accepts certain restrictions on each
reference to restrict the amount of data written (as explained below).
Note, however, that AIMMS does not accept all types of restrictions
which are syntactically allowed by the syntax diagram of the {\tt
READ} and {\tt WRITE} statements.

\paragraph{Default selection}

If you do not specify a selection during a {\tt READ} statement,
AIMMS will transfer the data of all identifiers stored in the table
or file that can be mapped onto identifiers in your model. If you do
not specify a selection for a {\tt WRITE} statement to a text
% or case
file, all identifiers declared in your model will be written.
When writing to a database table, AIMMS will write data for all
columns in the table as long as they can be mapped onto AIMMS
identifiers.

\paragraph{Filtering the selection}
\typindex{clause}{FILTERING}
\typindex{clause}{CHECKING}
\subtypindex{statement}{READ}{clause!FILTERING@{\tt FILTERING}}
\subtypindex{statement}{READ}{clause!CHECKING@{\tt CHECKING}}
\subtypindex{statement}{WRITE}{clause!FILTERING@{\tt FILTERING}}
\subtypindex{statement}{WRITE}{clause!CHECKING@{\tt CHECKING}}
\AIMMSlink{filtering}
\AIMMSlink{checking}

You can apply the following filtering qualifiers on {\tt READ} and {\tt
WRITE} statements to restrict the data selection:
\begin{itemize}
\item the {\tt FILTERING} or {\tt CHECKING} clauses restrict the
domain of all transferred data in both the {\tt READ} and {\tt WRITE}
statements, and
\item an arbitrary logical condition can be imposed on each individual
parameter and variable in a {\tt WRITE} statement.
\end{itemize}

\paragraph{{\tt FILTERING} versus {\tt CHECKING}}

You can use both the {\tt FILTERING} and {\tt CHECKING} clause to
restrict the tuples for which data is transferred between a data
source and AIMMS. During a {\tt WRITE} statement there is no
difference in semantics, and you can use both clauses interchangeably.
During a {\tt READ} statement, however, the {\tt FILTERING} clause
will skip over all data outside of the filtering domain, whereas the
{\tt CHECKING} clause will issue a runtime error when the data source
contains data outside of the filtering domain. This is useful feature
for catching typing errors in text data files.

\paragraph{Examples}

The following examples illustrate filtering and the use of logical
conditions imposed on index domains.
\begin{example}
    read Distance(i,j) from table RouteTable
         filtering i in SourceCities, (i,j) in Routes;

    write Transport( (i,j) | Sum(k, Transport(i,k)) > MinimumTransport )
          to table RouteTable ;
\end{example}

\paragraph{Advanced filtering on records}
\index{stored procedure}
\index{database!use of views for filtering}
\index{database!use of database procedure}

If you need more advanced filtering on the records in a database
table, you can use the database to perform this for you. You can
\begin{itemize}
\item define {\em views} to create temporary tables when the filtering
is based on a non-parameterized condition, or
\item use {\em stored procedures} with arguments to create temporary
tables when the filtering is based on a parameterized condition.
\end{itemize}
The resulting tables can then be read using a simple form of the {\tt
READ} statement.

\paragraph{Merge, replace or backup mode}

\subtypindex{mode}{MERGE}{in READ and WRITE statements@{in {\tt READ}
and {\tt WRITE} statements}}
\subtypindex{mode}{REPLACE}{in READ and WRITE statements@{in {\tt READ}
and {\tt WRITE} statements}}
\subtypindex{mode}{BACKUP}{in READ and WRITE statements@{in {\tt READ}
and {\tt WRITE} statements}}
\subtypindex{statement}{READ}{mode!MERGE@{\tt MERGE}}
\subtypindex{statement}{READ}{mode!REPLACE@{\tt REPLACE}}
\subtypindex{statement}{WRITE}{mode!MERGE@{\tt MERGE}}
\subtypindex{statement}{WRITE}{mode!REPLACE@{\tt REPLACE}}
\subtypindex{statement}{WRITE}{mode!BACKUP@{\tt BACKUP}}
\subtypindex{statement}{WRITE}{mode!INSERT@{\tt INSERT}}
\AIMMSlink{merge}
\AIMMSlink{replace}
\AIMMSlink{backup}
\AIMMSlink{insert}

AIMMS allows you to transfer data from and to a file or a database
table in {\em merge} mode, {\em replace} mode or {\em insert} mode. If you have
not selected a mode in either a {\tt READ} or {\tt WRITE} statement,
AIMMS will transfer the data in replace mode by default, 
with one exception: when reading from a case difference file that was generated by {\tt CaseCreateDifferenceFile} function 
with {\tt diffTypes} argument equal to {\tt elementReplacement}, {\tt elementAddition} or {\tt elementMultiplication}, 
the file is always read in merge mode, so that the {\tt diffTypes} can be applied in a sensible way.
\newline
When you are writing data to a text data file, 
AIMMS also supports a {\em
backup} mode. The {\em insert} mode can speed up writing to databases.

\paragraph{Reading in merge mode}

When AIMMS reads data in merge mode, it will overwrite existing
elements for all read identifiers, and add new elements as necessary.
It is important to remember that in this mode, if there is no data
read for some of the existing elements, they keep their current value.

\paragraph{Writing in merge mode}

When AIMMS writes data in merge mode, the semantics is dependent on
the type of the data source.
\begin{itemize}
\item If the data source is a text file, AIMMS will {\em append} the
newly written data to the end of the file.
\item If the data source is
% an AIMMS case file or
a database
table, AIMMS will merge the new values into the existing values,
creating new records as necessary.
\end{itemize}

\paragraph{Reading in replace mode}

When AIMMS reads data in replace mode, it will empty the existing
data of all identifiers in the identifier selection, and then read in
the new data.

\paragraph{Writing in replace mode}

When AIMMS writes data in replace mode, the semantics is again
dependent on the type of the data source.
\begin{itemize}
\item If the data source is a text file, AIMMS will {\em
overwrite the entire contents} of the file with the newly written
data. Thus, if the file also contained data for identifiers that are
not part of the current identifier selection, their data is lost by
the {\tt WRITE} statement.
%%\item If the data source is an AIMMS case file, AIMMS will
%%overwrite the data of all identifiers in the identifier
%%selection.
\item If the data source is a database table, AIMMS will either empty all
columns in the table that are mapped onto identifiers in the
identifier selection (default, {\tt REPLACE COLUMNS} mode), or will remove all records in the table not written by this write statement ({\tt REPLACE ROWS} mode). The {\tt REPLACE COLUMNS} and {\tt REPLACE ROWS} modes are discussed in more detail in Section~\ref{sec:db.restrictions}).
\end{itemize}

\paragraph{Writing in insert mode}
Writing in insert mode is only applicable when writing to databases. Essentially, what it does is writing the selected data to a database table using SQL {\em INSERT} statements. In other words, it expects that the selection of the data that you write to the table doesn't match any existing primary keys in the database table. If it does, AIMMS will raise an error message about duplicate keys being written. Functionally, the insert mode is equivalent to the replace rows mode, with the non-existing primary keys restriction. Especially when writing to database tables which already contain a lot of rows, the speed advantage of the insert mode becomes more visible.

\paragraph{Writing in backup mode}

When you are transferring data to a text file, AIMMS supports
writing in backup mode in addition to the merge and replace modes. The
backup mode lets you write out files which can serve as a text
backup to a (binary) AIMMS case file. When writing in backup mode,
AIMMS
\begin{itemize}
\item skips all identifiers on the identifier list which possess a nonempty
definition (and, consequently, cannot be read in from a datafile),
\item skips all identifiers for which the property {\tt NoSave} has
been set, and
\item writes the contents of all remaining identifiers in
such an order that, upon reading the data from the file, all domain
sets are read before any identifiers defined over such domain sets.
\end{itemize}
Backup mode is not supported during a {\tt READ} statement, or
when writing to a database.
% or case file.

\paragraph{Writing data in a dense mode}
\subtypindex{mode}{DENSE}{in WRITE statements@{in {\tt WRITE} statements}}

Writing in dense mode is only applicable when writing to databases. Data in AIMMS is stored for non-default values only, and, by default, 
AIMMS only writes these non-default values to a database. 
In order to write the default values as well to the database table at hand, 
you can add the {\em dense} keyword before most of the {\tt WRITE} modes discussed above. 
This will cause AIMMS to write all possible values, including the defaults, for all tuple combinations considered in the {\tt WRITE} statement. 
Care should be taken that writing in {\em dense} mode does not lead to an excessive amount of records being stored in the database.
The mode combination {\em merge} and {\em dense} is not allowed, because it is ambiguous
whether or not a non-default entry in the database should be overwritten by a default value of AIMMS.

\paragraph{Replacing sets}
\statindex{CLEANUP}

Whenever elements in a domain set have been removed by a {\tt READ}
statement in replace mode, AIMMS will {\em not} cleanup all
identifiers defined over that domain. Instead, it will leave it up to
you to use the {\tt CLEANUP} statement to remove the inactive data
that may have been created.

\paragraph{Domain filtering}
\typindex{clause}{FILTERING}
\typindex{clause}{CHECKING}
\subtypindex{statement}{READ}{clause!FILTERING@{\tt FILTERING}}
\subtypindex{statement}{READ}{clause!CHECKING@{\tt CHECKING}}
\subtypindex{statement}{WRITE}{clause!FILTERING@{\tt FILTERING}}
\subtypindex{statement}{WRITE}{clause!CHECKING@{\tt CHECKING}}

For every {\tt READ} and {\tt WRITE} statement you can indicate
whether or not you want domain filtering to take place during the data
transfer. If you want domain filtering to be active, you must indicate
the list of indices, or domain conditions to be filtered in either a
{\tt FILTERING} of {\tt CHECKING} clause. In case of ambiguity which
index position in a parameter you want to have filtered you must
specify indices in the set or parameter reference.

\paragraph{Example}
The following {\tt READ} statements are not accepted because both
{\tt Routes} and {\tt Dist\-ance} are defined over {\tt Cities}
$\times$ {\tt Cities}, and it is unclear to which position the
filtered index {\tt i} refers.
\begin{example}
    read Routes   from table RouteTable filtering i ;
    read Distance from table RouteTable filtering i ;
\end{example}
This ambiguity can be resolved by explicitly adding the relevant
indices as follows.
\begin{example}
    read (i,j) in Routes from table RouteTable filtering i ;
    read Distance(i,j)   from table RouteTable filtering i ;
\end{example}

\paragraph{Semantics of domain filtering}
\index{filtering semantics}

When you have activated domain filtering on an index or index tuple,
AIMMS will limit the transfer of data dependent on further index
restrictions.
\begin{itemize}
\item During a {\tt READ} statement only the data elements for which
the value of the given index (tuple) lies within the specified set
are transfered. If no further index restriction has been specified,
transfer will take place for all elements of the corresponding domain
set.
\item During a {\tt WRITE} statement only those data elements are
transferred for which the index (tuple) is contained in the AIMMS
set given in the (optional) {\tt IN} clause. If no set has been
specified, and the data source is a database table, the transfer is
restricted to only those tuples that are already present in the table.
When the data source is a text file
% or AIMMS case file,
the latter type of domain filtering is not meaningful and therefore
ignored by AIMMS.
\end{itemize}

\paragraph{{\tt READ} example}
\subtypindex{statement}{READ}{example of filtering}

In the following two {\tt READ} statements the data transfer for
elements associated with {\tt i} and ({\tt i},{\tt j}), respectively,
is further restricted through the use of the sets {\tt SourceCities}
and {\tt Routes}.
\begin{example}
    read Distance(i,j) from table RouteTable filtering i in SourceCities ;
    read Distance(i,j) from table RouteTable filtering (i,j) in Routes ;
\end{example}

\paragraph{{\tt WRITE} example}
\subtypindex{statement}{WRITE}{example of filtering}

In the following two {\tt WRITE} statements, the values of the
variable {\tt Transport(i,j)} are written to the database table {\tt
RouteTable} for those tuples that lie in the AIMMS set {\tt
SelectedRoutes}, or for which records in the table {\tt RouteTable}
are already present, respectively.
\begin{example}
    write Transport(i,j) to table RouteTable filtering (i,j) in SelectedRoutes ;
    write Transport(i,j) to table RouteTable filtering (i,j) ;
\end{example}
The {\tt FILTERING} clause in the latter {\tt WRITE} statement would
have been ignored by AIMMS when the data source was a text data
file.
% or an AIMMS case file.

\paragraph{Writing selected suffices using the {\tt WHERE} clause}
\typindex{clause}{WHERE}
\subtypindex{statement}{WRITE}{clause!WHERE@{\tt WHERE}}

Using the {\tt WHERE} clause of the {\tt WRITE} statement you can instruct AIMMS, for all identifiers in the identifier selection, to write the data of either a specified suffix or a set of suffices to file, rather than their level values.
The {\tt WHERE} clause can only be specified during a {\tt WRITE} statement to a {\tt FILE}, and the corresponding set or element expression must refer to a subset of, or element in, the predefined set {\tt AllSuffixNames}.

\paragraph{Example}

The following {\tt WRITE} statement will write the values of the {\tt .Violation} suffix of  to the file {\tt ViolationsReport.txt} for all variables in the project.
\begin{example}
    write AllVariables to file "ViolationsReport.txt" where suffix = 'Violation'; 
\end{example}

\section{Tabular expressions}\label{sec:text.table}%
\index{table}
\index{table!2-dimensional}
\index{tabular expression}
\index{initialization!2-dimensional table}
\keyindex{DATA TABLE}
\AIMMSlink{table}

\paragraph{Tables for initialization}
\attrindex{InitialData}

For multidimensional quantities the table format often provides the
most natural structure for data entry because elements are repeated
less often. Tables can be used in text data files and in the
{\tt InitialData} attribute inside the declaration of an identifier.

\paragraph{Two-dimen- sional views}

A table is a two-dimensional view of a multidimensional quantity. The
index tuple of the quantity is split into two parts: row identifiers
and column identifiers. Indices may not be permuted.

\paragraph{Example}
The following example illustrates a simple example of the table format.
\begin{example}
    Distance(i,j) := DATA TABLE
                    Rotterdam    Antwerp    Berlin    Paris
    !               ---------    -------    ------    -----
        Amsterdam       85          170       660      510
        Rotterdam                   100       700      440
        Antwerp                               725      340
        Berlin                                        1050
    ;
\end{example}
The first line of a table (after the keyword
\verb|DATA TABLE|) contains the column identifiers. Each subsequent
line contains a row identifier followed by the table entries.

\paragraph{Multi- dimensional entries}

Row and column identifiers may be set elements, tuples of elements, or
tuples containing element ranges. As a result, multidimensional
identifiers can still be captured within the two-dimensional framework
of a table.

\paragraph{Proper spacing}

Column identifiers must be separated by at least one space. AIMMS
keeps track of the column width by maintaining the first and last
position used by each column identifier. Any entry must intersect only
one column and is understood to be part of that column. AIMMS will
reject any entry that intersects two columns, or falls between them.

\paragraph{Continuation\\ of tables with {\tt +}}%
\index{table!continuation}\operindex{+}

Even though the table format is a convenient way to enter data, the
number of columns is always restricted by the width of a line.
However, by placing a \verb|+| on a new line you can continue a table
by repeating the table format. Row identifiers and column identifiers
can be repeated in each block separated by the {\tt +} sign, but must
be unique within a block.

\paragraph{Example}
The following table illustrates a valid example of table continuation,
equivalent with the previous example.
\begin{example}
    Distance(i,j) := DATA TABLE
                    Rotterdam    Antwerp   
    !               ---------    -------   
        Amsterdam       85          170    
        Rotterdam                   100    
    +
                    Berlin    Paris
    !               ------    -----
        Amsterdam     660      510 
        Rotterdam     700      440 
        Antwerp       725      340 
        Berlin                1050
    ;
\end{example}

\paragraph{Data and membership tables}
\index{table!membership}
\index{membership table}
\index{set!membership table}

Tables can be used for the initialization of both parameters and sets.
When used for parameter initialization, table entries are either blank
or contain explicit numbers, quoted or unquoted set elements and
quoted strings. Entries in tables used for set initialization are
either blank or contain a ``\verb|*|'' denoting membership.

\paragraph{Syntax}

The detailed syntax of a table constant is given by the following
diagram, where the symbol ``$\backslash$n'' stands for the newline
character.
\vskip1pc
\syntaxdiagram{table}{table}
\begin{minipage}[t]{.5\textwidth}
\syntaxdiagram{table-header}{table-header}
\end{minipage}\begin{minipage}[t]{.5\textwidth}
\syntaxdiagram{table-row}{table-row}
\vskip1pc
\end{minipage}

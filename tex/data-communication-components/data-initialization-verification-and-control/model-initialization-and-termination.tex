\section{Model initialization and termination}\label{sec:data.init}%
\index{initialization} \index{data!initialization} \index{termination}

\paragraph{Separation of model and data}%
\index{separation of model and data}

In general, it is a good strategy to separate the initialization of data from
the specification of your model structure. This is particularly true for large
models. The separation improves the clarity of the model text, but more
importantly, it allows you to use the same model structure with various data
sets.

\paragraph{Supplying initial data}

There are several methods to input the initial data of the identifiers in your
model. AIMMS allows you:
\begin{itemize}
\item to supply initial data for a particular identifier as part of its
declaration,
\item to read in data from various external data sources, such as text
data files, AIMMS cases and databases, and
\item to initialize data by means of algebraic statements.
\end{itemize}

\paragraph{Interactive initialization}%
\index{initialization!interactive}

In an interactive application the end-user often has to enter additional data
or modify existing data before the core of the model can be executed. Thus,
proper data initialization in most cases consists of more steps than just
reading data from external sources. It is the responsibility of the modeler to
make sure that an end-user is guided through all necessary initialization steps
and that the sequence is completed before the model is executed.

\paragraph{The attribute {\tt InitialData}}
\declattrindex{set}{InitialData} \declattrindex{procedure}{InitialData}
\attrindex{InitialData} \AIMMSlink{initial_data}\herelabel{attr:par.initialdata}

Both sets and parameters can have an {\tt InitialData} attribute. You can use
it to supply the initial data of a set or parameter, but only when the set or
parameter does not have a definition as well. In general, the {\tt InitialData} attribute is not recommended when different data sets are used. However,
it can be useful for initializing those identifiers in your model that are
likely to remain constant for all data sets. The contents of the {\tt InitialData} attribute must be a {\em constant expression} (i.e.\ a constant, a
constant enumerated set or a constant list expression) or a {\tt DataTable}.
The table format is explained in Section~\ref{sec:text.table}.

\paragraph{The {\tt Main- Initialization} and {\tt PostMain- Initialization} procedures}
\procindex{MainInitialization}
\procindex{PostMainInitialization}

AIMMS will add the procedures {\tt MainInitialization} and {\tt PostMainInitialization} to a new project
automatically. Initially these are empty, leaving the (optional) specification of
their bodies to you. You can use these procedures to read in data from external
sources and to specify AIMMS statements to compute your model's initial data
in terms of other data. The latter step may even include solving a mathematical
program. Both the {\tt MainInitialization} and {\tt PostMainInitialization} procedure are aimed at initializing your model. The distinction between the two 
becomes apparent in the presence of libraries in your model (cf.\ Section~\ref{sec:module.library}). 

\paragraph{Library initialization}
\procindex{LibraryInitialization}
\procindex{PostLibraryInitialization}

Each library can provide {\tt LibraryInitialization} and {\tt PostLibraryInitialization} procedures. 
The {\tt LibraryInitialization} procedure is aimed at initializing the state of each library, {\em regardless of the state of other libraries}, such as sets and parameters that represent the internal 
state of the library, or, when the library uses an external DLL, initializing such a DLL. The {\tt PostLibraryInitialization} procedures are executed after all {\tt LibraryInitialization} procedures have been executed,
and thus, can rely on the internal state of all other libraries already being initialized to perform tasks on its behalf.

\paragraph{Model initialization sequence}
\index{initialization!sequence}

To initialize the data in your model, AIMMS performs the following actions
directly after compiling the model:
\begin{itemize}
\item AIMMS fills the contents of any global set or parameter with the
contents of its {\tt InitialData} attribute, 
\item {aimms} executes the predefined procedures {\tt MainInitialization},
\item AIMMS executes the predefined procedure {\tt LibraryInitialization} for each library,
\item AIMMS executes the predefined procedure {\tt PostMainInitialization}, and
\item finally AIMMS executes the predefined procedure {\tt PostLibraryInitial\-izat\-ion} for each library.
\end{itemize}
Thus, as a guideline, any model initialization that depends on (other) libraries should go into a {\tt PostLibraryInitialization} or the {\tt PostMainInitialization} procedure, to make
sure that it can be executed successfully.

\paragraph{Library termination}
\procindex{LibraryTermination}
\procindex{PreLibraryTermination}

Similarly to the situation of library initialization, each library can provide {\tt PreLibraryTermination} and {\tt LibraryTermination} procedures. 
The {\tt PreLibrary\-Init\-ialization} procedures are executed before all {\tt LibraryTermination} procedures have been executed, and are aimed at at library termination steps that may still need other libraries to 
be functioning. The {\tt LibraryTermination} procedures are aimed at terminating the state of each library individually, for instance, to deinitialize any external DLLs the library may depend upon.

\paragraph{Model termination sequence}
\index{termination!sequence}

To terminate your model, AIMMS performs the following actions directly prior to closing the project:
\begin{itemize}
\item AIMMS executes the predefined procedure {\tt PreMainTermination},
\item AIMMS executes the predefined procedure {\tt PreLibraryTermination} for each library,
\item {aimms} executes the predefined procedures {\tt MainTermination}, and 
\item finally AIMMS executes the predefined procedure {\tt LibraryTermination} for each library.
\end{itemize}

\subsection{Reading data from external sources}\label{sec:data.init.external}

\paragraph{The {\tt READ} statement}
\statindex{READ}

You can use the {\tt READ} statement to initialize data from the following
external data sources:
\begin{itemize}
\item user-supplied text files containing constant lists and tables,
\item AIMMS-generated binary case files, and
\item external ODBC-compliant databases.
\end{itemize}

\paragraph{Reading from text data files}
\index{initialization!from text files}

With the {\tt READ} statement you can initialize selected model input data from
text files containing explicit data assignments. Only {\tt DataTables} and
constant expressions (i.e.\ a constant, a constant enumerated set or a constant
list expression) are allowed. Since the format of these AIMMS data
assignments is simple, the corresponding files are easily generated by external
programs or by using the AIMMS {\tt DISPLAY} statement.

\paragraph{When useful}

Reading from text files is especially useful when
\begin{itemize}
\item the data must come directly from your end-users, but is not
contained in a formal database,
\item the data is produced by external programs that are not linked or
cannot be linked directly to AIMMS
\end{itemize}

\paragraph{Reading from binary case files}%
\index{initialization!from case files}

The {\tt READ} statement can also initialize data from an AIMMS case file.
You can instruct AIMMS to read either selected identifiers or all
identifiers. The case file data is already in an appropriate format, and
therefore provides a fast medium for data storage and retrieval inside your
application.

\paragraph{When useful}

Reading from case files is especially useful when
\begin{itemize}
\item you want to start up your AIMMS application in the same state
as you left it when you last used it,
\item you want to read from different data sources captured inside
different cases making up your own internal database.
\end{itemize}

\paragraph{Reading from databases}%
\index{initialization!from a database}

A third (and powerful) application of the {\tt READ} statement is the retrieval
of data from any ODBC-compliant database. This form of data
initialization gives you direct access to up-to-date corporate databases.

\paragraph{When useful}

Reading from databases is especially useful when
\begin{itemize}
\item data is shared by several users or applications inside an
organization,
\item data integrity over time in a database plays a crucial role
during the lifetime of your application.
\end{itemize}

\paragraph{Computing initial data}%
\index{initialization!by computation}

After reading initial data from internal and external sources, AIMMS allows
you to compute other identifiers not yet initialized. This feature is very
useful when the external data sources of your model supply only partial initial
data. For instance, after reading in event data which represent tank actions
(when and at what rate do charges and discharges take place), all stock levels
at distinct model time instances can be computed.


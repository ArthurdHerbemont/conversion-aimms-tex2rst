\section{Data control}\label{sec:data.control}
\index{data!control}

\paragraph{Why data control?}

The contents of domain sets in your model may change through running procedures
or performing other actions from within the graphical user interface. When
elements are removed from sets, there may be data for domain elements that are
no longer in the domain sets. In addition, data may exist for intermediate
parameters, which is no longer used in the remainder of your model session. For
these situations, AIMMS offers facilities to eliminate or activate data
elements that fall outside their current domain of definition. This section
provides you with housekeeping data control statements, which can be combined
with ordinary assignments to keep your model data consistent and maintained.

\paragraph{Facilities}

AIMMS offers the following \syntaxmark{data-control-statement} data control
tools:
\begin{itemize}
\item the {\tt EMPTY} statement to remove the contents from all or a
selected number of identifiers,
\item the {\tt CLEANUP} and {\tt CLEANDEPENDENTS} statements to clean
up all, or a selected number of identifiers,
\item the {\tt REBUILD} statement to manually instruct AIMMS to reclaim
unused memory from the internal data structures used to the store the data of a
selected number of identifiers,
\item the procedure {\tt FindUsedElements} to find all elements of a
particular set that are in use in a given collection of indexed model
identifiers, and
\item the procedure {\tt RestoreInactiveElements} to find and
restore all inactive elements of a particular set for which inactive data
exists in a given collection of indexed model identifiers.
\end{itemize}

\paragraph{The {\tt EMPTY} statement}
\statindex{EMPTY} \AIMMSlink{empty}

The {\tt EMPTY} statement can be used to discard the complete contents of all
or selected identifiers in your model. Its syntax follows.

\paragraph{}
\begin{syntax}
\syntaxdiagram{empty-statement}{empty-stmt}
\end{syntax}

\paragraph{Empty AIMMS identifiers}
The {\tt EMPTY} operator operates on a list of references to AIMMS
identifiers and takes the following actions.
\begin{itemize}
\item For parameters, variables (arcs) and constraints (nodes)
AIMMS discards their values plus the contents of all their suffices.
\item For sets, AIMMS will discard their contents plus the contents
of all corresponding subsets. If a set is a domain set, AIMMS will remove
the data from {\em all} parameters and variables that are defined over this set
or any of its subsets.
\item For slices of an identifier, AIMMS will discard all
values associated with the slice.
\item For sections in your model text, AIMMS will discard the
contents of all sets, parameters and variables declared in this section.
\item For a subset of the predefined set {\tt AllIdentifiers},
AIMMS will discard the contents of all identifiers contained in this subset.
\end{itemize}

\paragraph{Use in databases}
\index{database table!EMPTY statement@{\tt EMPTY} statement}
\subtypindex{statement}{EMPTY}{database table}

You can also use the {\tt EMPTY} statement in conjunction with databases. With
the {\tt EMPTY} statement you can either empty single columns in a database
table, or discard the contents of an entire table. This use is discussed in
detail in Section~\ref{sec:db.control}. You should note, however, that applying
the {\tt EMPTY} statement to a subset of {\tt AllIdentifiers} does {\em not}
apply to any database table contained in the subset to avoid inadvertent
deletion of data.

\paragraph{Examples}
The following statements illustrate the use of the {\tt EMPTY} operator.
\begin{itemize}
\item Remove all data of the variable {\tt Transport}.
\begin{example}
    empty Transport ;
\end{example}
\item  Remove all data in the set {\tt Cities}, but also all data
depending on {\tt Cities}, like e.g.\ {\tt Transport}.
\begin{example}
    empty Cities ;
\end{example}
\item Remove all the data of the indicated slice of the variable {\tt
Transport}
\begin{example}
    empty Transport(DiscardedCity, j);
\end{example}
\item Remove all data of all identifiers in the model tree node CityData.
\begin{example}
    empty CityData ;
\end{example}
\end{itemize}

\paragraph{Inactive data}
\index{inactive data} \index{data!inactive} \AIMMSlink{inactive_data}

When you remove some but not all elements from a domain set, AIMMS will not
automatically discard the data associated with those elements for every
identifier defined over the particular domain set. AIMMS will also not
automatically discard data that does not satisfy the current domain restriction
of a given identifier. Instead, it will consider such data as {\em inactive}.
During the execution of your model, no reference will be made to inactive data,
but such data may still be visible in the user interface. In addition, AIMMS
will not directly reclaim the memory that is freed up when the cardinality of a
multidimensional identifier in your model decreases.

\paragraph{When useful}

The facility to create inactive data in AIMMS allows you to temporarily
remove elements from domain sets when this is required by your model. You can
then restore the data after the relevant parts of the model have been executed.

\tipparagraph{Discard inactive data}{cleandependents} \statindex{CLEANUP}
\statindex{CLEANDEPENDENTS} \index{inactive data!discard} \AIMMSlink{cleanup}
\AIMMSlink{cleandependents} \AIMMSlink{rebuild} \statindex{REBUILD} \index{memory!reclaim}

If you want to discard inactive data that has been introduced in a particular
data set, you can apply the {\tt CLEANUP} statement to parameters and
variables, or the {\tt CLEANDEPENDENTS} statement to root sets in your model.
Through the {\tt REBUILD} statement you can instruct AIMMS to reclaim the
unused memory associated with one or more identifiers in your model. The syntax
follows.

\paragraph{}
\begin{syntax}
\syntaxdiagram{cleanup-statement}{cleanup-stmt}
\end{syntax}

\paragraph{Rules}

The following rules apply when you call the {\tt CLEANUP} statement.
\begin{itemize}
\item When you apply the {\tt CLEANDEPENDENTS} statement to a
set, all inactive elements are discarded from the set itself and from all of
its subsets. In addition, AIMMS will discard all inactive data throughout
the model caused by the changes to the set.
\item When you apply the {\tt CLEANUP} statement to a parameter or variable,
all inactive data associated with the identifier is removed. This includes
inactive data that is caused by changes in domain and range sets, as well as
data that has become inactive by changes in the domain condition of the
identifier.
\item When you apply the {\tt CLEANDEPENDENTS},  {\tt CLEANUP}, or {\tt REBUILD}
statement to a section, AIMMS will remove the inactive data of all sets, or
parameters and variables declared in it, respectively.
\end{itemize}
After using the {\tt CLEANUP} or {\tt CLEANDEPENDENTS} statement for a
particular identifier, all its associated inactive data is permanently lost.

\paragraph{Resorting root set elements}
\index{set!resort root ---} \index{root set!resort} \index{resort root set}

In addition to discarding inactive data from your model that is caused by the
existence of inactive elements in a root set, the {\tt CLEANDEPENDENTS}
operator will also completely resort a root set and all data defined of it
whenever possible and necessary. The following rules apply.
\begin{itemize}
\item Resorting will only take place if the current storage order of
a root set differs from its current ordering principle.
\item AIMMS will not resort sets for which explicit elements are
used in the model formulation.
\end{itemize}
As a call to {\tt CLEANDEPENDENTS} requires a complete rebuild of all
identifiers defined over the root sets involved, the {\tt CLEANDEPENDENTS}
statement may take a relatively long time to complete. For a more detailed
description of the precise manner in which root set elements and
multidimensional data is stored in AIMMS refer to
Section~\ref{sec:eff.set.ordering}. This section also explains the benefits of
resorting a root set.

\paragraph{Generated Mathematical Programs}

The {\tt CLEANDEPENDENTS} statement will also check whether any variable or 
constraint is affected; and if so will remove any generated mathematical program
that is generated from such a variable or constraint.

\paragraph{Restricted usage in AIMMS GUI}

You should not call the {\tt CLEANDEPENDENTS} statement in procedures that have been
linked to edit actions in graphical objects in an AIMMS end-user GUI via the 
{\bf Procedures} tab of the object {\bf Properties} dialog box. During these actions, 
AIMMS does not expect the element numbering to change.

\paragraph{Efficiency considerations}

If you want to apply the {\tt CLEANDEPENDENTS} statement to multiple sets,
applying the operation to all sets in a single call of the {\tt
CLEANDEPENDENTS} statement will, in general, be more efficient than using a
separate call for every single set. If an identifier depends on two or more of
the sets to which you want to apply the {\tt CLEANDEPENDENTS} operation, the
data of such an identifier will only be traversed and/or rebuild once, rather
than multiple times.


\paragraph{Examples}
\begin{itemize}
\item The following {\tt CLEANDEPENDENTS} statement will remove all
data from your application that depends on the removed element {\tt
'Amsterdam'}, including, for instance, all previously assigned values to {\tt
Transport} departing from or arriving at {\tt 'Amsterdam'}.
\begin{example}
    Cities -= 'Amsterdam' ;

    cleandependents Cities ;
\end{example}
\item The following {\tt CLEANUP} statement will remove the data of the
identifier {\tt Transport} for all tuples that either lie outside the current
contents of {\tt Cities}, or do not satisfy the domain restriction.
\begin{example}
    cleanup Transport;
\end{example}
\item Consider a parameter {\tt A(i,j)} where {\tt i} is an index into a set {\tt S} and {\tt j} an index
into a set {\tt T}, then
\begin{example}
    cleandependents S,T;
\end{example}
will be more efficient than
\begin{example}
    cleandependents S;
    cleandependents T;
\end{example}
because the latter may require {\tt A(i,j)} to be rebuilt twice.
\end{itemize}

\paragraph{Finding used elements}
\procindex{FindUsedElements} \presetindex{AllIdentifiers}
\AIMMSlink{findusedelements}

When you want to remove the elements in a set that are no longer used in your
application, you first have to make sure which elements are currently in use.
To find these elements easily, AIMMS provides the procedure {\tt
FindUsedElements}. It has the following three arguments:
\begin{itemize}
\item a set {\em SearchSet} for which you want to find the used elements,
\item a subset {\em SearchIdentifiers} of the predefined set {\tt
AllIdentifiers} consisting of all identifiers that you want to be investigated,
and
\item a subset {\em UsedElements} of the set {\em SearchSet}
containing the result of the search.
\end{itemize}
Upon execution, AIMMS will return that subset of {\em SearchSet} for which
the elements are used in the combined data of the identifiers contained in {\em
Search\-Identifiers}. When the identifiers {\em SearchSet} and {\tt
UsedElements} are contained in {\tt SearchIdentifiers} they are ignored.

\paragraph{Example}

The following call to {\tt FindUsedElements} will find the elements of the set
{\tt Cities} that are used in the identifiers {\tt Supply}, {\tt Demand}, and
{\tt Distance}, and store the result in the set {\tt UsedCities}.
\begin{example}
    SearchIdentifiers := DATA { Supply, Demand, Distance };

    FindUsedElements( Cities, SearchIdentifiers, UsedCities );
\end{example}
If these cities are the only ones of interest, you can place them into the set
{\tt Cities}, and thereby overwrite its previous contents. After that you can
cleanup your entire dataset by eliminating data dependent on cities other than
the ones currently contained in the set {\tt Cities}. This process is
accomplished through the following two statements.
\begin{example}
    Cities := UsedCities;

    cleandependents Cities;
\end{example}

\paragraph{Finding and restoring inactive elements}
\index{inactive data!restore} \procindex{RestoreInactiveElements}
\presetindex{AllIdentifiers} \AIMMSlink{restoreinactiveelements}

Inactive data in AIMMS results when elements are removed from (domain) sets.
Such data will be inaccessible, unless the corresponding set elements are
brought back into the set. When this is necessary, you can use the procedure
{\tt RestoreInactiveElements} provided by AIMMS. This procedure has the
following three arguments:
\begin{itemize}
\item a set {\em SearchSet} for which you want to verify whether
inactive data exists,
\item a subset {\em SearchIdentifiers} of the predefined set {\tt
AllIdentifiers} consisting of those identifiers that you want to be
investigated, and
\item a subset {\em InactiveElements} of the set {\em SearchSet} containing
the result of the search.
\end{itemize}
Upon execution AIMMS will find all elements for which inactive data exists
in the identifiers in {\em SearchIdentifiers}. The elements found will not only
be placed in the result set {\em InactiveElements}, but also be added to the
search set. This latter extension of {\em SearchSet} implies that the
corresponding inactive data is restored.

\paragraph{Example}

The following call to {\tt RestoreInactiveElements} will verify whether
inactive data exists for the set {\tt Cities} in {\tt AllIdentifiers}.
\begin{example}
    RestoreInactiveElements( Cities, AllIdentifiers, InactiveCities );
\end{example}
After such a call the set {\tt InactiveCities} could contain the element {\tt
'Amsterdam'}. In this case, the set {\tt Cities} has been extended with {\tt
'Amsterdam'} as well. If you subsequently decide that cleaning up the set {\tt
Cities} is harmless, the following two statements will do the trick.
\begin{example}
    Cities -= InactiveCities;

    cleandependents Cities;
\end{example}

\paragraph{Reclaiming memory}

If the cardinality of a multidimensional identifier in your model decreases,
AIMMS will not automatically reclaim the memory that is freed up because of
the decreased amount of data to store. Instead, it will keep the memory
available to store additional data that is associated with subsequent changes
to the identifier. If the cardinality of an identifier decreases dramatically
during a run the of a model, this may lead to a huge amount of memory getting
stuck up with a single identifier in your model.

\paragraph{Memory fragmentation}
\index{memory!fragmentation}

In addition, if a model is running for a prolonged period of time, and an
identifier has undergone huge amounts of structural changes during that time,
the memory associated with that identifier may become heavily fragmented. In
the long run, memory fragmentation may lead to decreased performance of your
model. Rebuilding the internal data structures associated with such an
identifier will resolve the fragmentation problem.

\paragraph{Automatic reclamation}

Prior to solving a mathematical program, AIMMS will perform a quick check
comparing the total amount of memory used by an identifier to the amount of
unused memory associated with that identifier. By adding to and removing elements
from identifiers, memory may become fragmented and the fraction of unused memory 
may grow. If the fraction of unused memory
compared to the total amount of memory in use becomes too large, AIMMS will
automatically rebuild such an identifier in order to reclaim the unused memory.
AIMMS will also reclaim the memory of an identifier whenever it becomes
empty during the run of a model.

\paragraph{the {\tt REBUILD} statement}

Through the {\tt REBUILD} statement you can manually instruct AIMMS to
rebuild the internal data structures associated with one or more identifiers.
During the {\tt REBUILD} statement AIMMS uses a more thorough check to
verify whether a rebuild of an identifier is worthwhile prior to solving a
mathematical program.


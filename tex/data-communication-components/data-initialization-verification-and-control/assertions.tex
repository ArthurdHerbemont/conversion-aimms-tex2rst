\section{Assertions}\label{sec:data.assert}

\paragraph{Data validity is important}%
\index{initialization!data validity}

In almost all modeling applications it is important to check the validity of
input data prior to its use. For instance, in a transportation model it makes
no sense if the total demand exceeds the total supply. In general, data
consistency checks guard against unexplainable or even infeasible model
results. As a result, these checks are essential to obtain customer acceptance
of your application. In rigorous model-based applications it is not uncommon
that the error consistency checks form a significant part of the total model
text.

\paragraph{{\tt Assertion} declaration and attributes}
\declindex{Assertion} \AIMMSlink{assertion}

To provide you with a mechanism to implement data validity checks, AIMMS
offers a special {\tt Assertion} data type. With it, you can easily specify and
verify logical conditions for all elements in a particular domain, and take
appropriate action when you find an inconsistency. Assertions can be verified
from within the model through the {\tt ASSERT} statement, or automatically upon
data changes by the user from within the graphical user interface. The
attributes of the {\tt Assertion} type are given in
Table~\ref{table:data.attr-assertion}.

\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute}     & {\bf Value-type}            & {\bf See also} \\
                    &                             & {\bf page} \\
\hline \verb|IndexDomain| & {\em index-domain}          &
                    \pageref{attr:par.index-domain},
                    \pageref{attr:var.index-domain}\\
\verb|Text|         & {\em string}                &
                    \pageref{attr:prelim.text}, \pageref{attr:par.text} \\
\verb|Property|     & {\tt WarnOnly}  & \\
\verb|AssertLimit| & {\em integer}               & \\
\verb|Definition|   & {\em logical-expression}    &
\pageref{attr:set.definition}, \pageref{attr:par.definition}\\
\verb|Action|       & {\em statements}            & \\
\verb|Comment|      & {\em comment string}        & \pageref{attr:prelim.comment}\\
\hline\hline
\end{tabular}
\caption{{\tt Assertion} attributes}\label{table:data.attr-assertion}
\end{aimmstable}

\paragraph{The {\tt Definition} attribute}
\declattrindex{assertion}{Definition} \AIMMSlink{assertion.definition}

The {\tt Definition} attribute of an {\tt Assertion} contains the logical
expression that must be satisfied by every element in the index domain. If the
logical expression is not true for a particular element in the index domain,
the specified action will be undertaken.
Examples follow.

\paragraph{Examples}

\begin{example}
Assertion SupplyExceedsDemand {
    Text         :  Error: Total demand exceeds total supply;
    Definition   : { 
        Sum( i in Cities, Supply(i) ) >=
        Sum( i in Cities, Demand(i) )
    }
}
Assertion CheckTransportData {
    IndexDomain  :  (i,j) | Distance(i,j);
    Text         :  Please supply proper transport data for transport (i,j);
    AssertLimit  :  3;
    Definition   : { 
        UnitTransportCost(i,j) and
        MinShipment(i,j) <= MaxShipment(i,j)
    }
}
\end{example}
The assertion {\tt SupplyExceedsDemand} is a global check. The assertion {\tt
CheckTrans\-portData(i,j)} is verified for every pair of cities {\tt i} and
{\tt j} for which {\tt Distance(i,j)} assumes a nonzero value. AIMMS will
terminate further verification when the assertion fails for the third time.

\paragraph{The {\tt Text} attribute}
\declattrindex{assertion}{Text} \AIMMSlink{assertion.text}

The {\tt Text} attribute of an {\tt Assertion} is the text that is used 
as warning or error message when the assertion fails for an element in its domain. If the text
contains indices from the assertions index domain, these are expanded to
identify the elements for which the assertion failed. If you have overridden
the default response by means of the {\tt Action} attribute (see below), then
the text attribute is ignored.

\paragraph{The {\tt Property} attribute}
\declattrindex{assertion}{Property} \AIMMSlink{assertion.property}

The {\tt Property} attribute of an assertion can only assume the value {\tt
Warn\-Only}. With it you indicate that a failed assertion should only result in
a warning being triggered, instead of an error. This attribute is also ignored if the 
{\tt Action} is overridden.

\paragraph{The {\tt AssertLimit} attribute}
\declattrindex{assertion}{AssertLimit} \AIMMSlink{assertion.assert_limit}

By default, AIMMS will verify an assertion for every element in its index
domain, and call the (default) action for every element for which the
assertion fails. With the {\tt AssertLimit} attribute you can limit the number
of verifications that are made. When the number of failed assertions reaches the {\tt
AssertLimit}, AIMMS will stop the verification of any further elemens in the index domain. By
default, the {\tt Assert\-Limit} is set to 1.

\paragraph{The {\tt Action} attribute}
\declattrindex{assertion}{Action} \AIMMSlink{assertion.action}

The default response to a failing assertion is that either an error or a warning is raised, 
based on the {\tt Property} setting. 
You can use the {\tt Action} attribute if you want to specify a nondefault
response to a failed assertion. Like the body of a procedure, the {\tt Action}
attribute can contain multiple statements which together implement the
appropriate response. During the execution of the statements in the {\tt
Action} attribute, the indices occurring in the index domain of the assertion
are bound to the currently offending element. This allows you to control the
interaction with the end-user. For instance, you can request that all detected
errors in the index domain are changed appropriately, or perhaps implement an auto-correct on 
invalid values. 

\paragraph{The {\tt FailCount} operator}
\operindex{FailCount} \index{assertion!FailCount operator@{\tt FailCount}
operator} \statindex{HALT} \AIMMSlink{failcount}

If you raise an error or call the {\tt HALT} statement during the execution of an {\tt Action}
attribute, the current model execution will terminate. When you use it in
conjunction with the predefined {\tt FailCount} operator, you can implement a
more sophisticated version of the {\tt AssertLimit}. The {\tt FailCount}
operator evaluates to the number of failures encountered during the current
execution of the assertion. It cannot be referenced outside the context of an
assertion.

\paragraph{Verifying assertions}
\index{assertion!verifying}

Assertions can be verified in two ways:
\begin{itemize}
\item by explicitly calling the {\tt ASSERT} statement during the
execution of your model, or
\item automatically, from within the graphical user interface, when
the end-user of your application changes input values in particular graphical
objects.
\end{itemize}

\paragraph{The {\tt ASSERT} statement}
\statindex{ASSERT} \AIMMSlink{assert}

With the {\tt ASSERT} statement you verify assertions at specific places during
the execution of your model. Thus, you can use it, for instance, during the
execution of the {\tt MainInitialization} procedure, to verify the consistency
of data that you have read from a database. Or, just prior to solving a
mathematical program, to verify that all currently accrued data modifications
do not result in data inconsistencies. The syntax of the {\tt ASSERT} statement
is simple.

\paragraph{Syntax}
\syntaxdiagram{assert-statement}{assert}

\paragraph{Example}
The following statement illustrates a basic use of the {\tt ASSERT} statement.
\begin{example}
    assert SupplyExceedsDemand, CheckTransportData;
\end{example}
It will verify the assertion {\tt SupplyExceedsDemand}, as well as the {\em
complete} assertion {\tt Check\-TransportData}, i.e.\ checks are performed for
every element ({\tt i},{\tt j}) in its domain.

\paragraph{Sliced verification}
\index{assertion!sliced verification}

AIMMS allows you to explicitly supply a binding domain for an indexed
assertion. By doing so, you can limit the assertion verification to the
elements in that binding domain. This is useful when you know a priori that the
data for only a small subset of the elements in a large index domain has
changed. You can use such sliced verification, for instance, during the
execution of a procedure that is called upon a single data change in a
graphical object on a page.

\paragraph{Example}

Assume that {\tt CurrentCity} takes the value of the city for which an end-user
has made a specific data change in the graphical user interface. Then the
following {\tt ASSERT} statement will verify the assertion {\tt
CheckTransportData} for only this specific city.
\begin{example}
    assert CheckTransportData(CurrentCity,j),
           CheckTransportData(i,CurrentCity);
\end{example}


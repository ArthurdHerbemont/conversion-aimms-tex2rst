\section{Working with the set \texttt{AllIdentifiers}}\label{sec:data.allidentifiers}
\presetindex{AllIdentifiers}

\paragraph{Working with {\tt AllIdentifiers}}

Throughout your model you can use the predefined set {\tt AllIdentifiers} to
construct and work with dynamic collections of identifiers in your model.
Several operators in AIMMS support the use of a subset of {\tt
AllIdentifiers} instead of an explicit list of identifier names, while other
operators support the use of an index into {\tt AllIdentifiers} instead of a
single explicit identifier name.

\paragraph{Constructing identifier sets}

AIMMS offers a number of constructs that can help you to construct a
meaningful subset of {\tt AllIdentifiers}. They are:
\begin{itemize}
\item set algebra with other predefined identifier subsets, and
\item dynamic selection based on model query functions.
\end{itemize}

\paragraph{Predefined identifier sets}

When compiling your model, AIMMS automatically creates an identifier set for
every section in your model. Each such set contains all the identifier names
that are declared in the corresponding section. In addition, for every
identifier type, AIMMS fills a predeclared set {\tt All}{\em IdentifierType}
(e.g. {\tt AllParameters}, {\tt AllSets}) with all the identifiers of that
type. The complete list of identifier type related sets defined by AIMMS can
be found in the AIMMS Function Reference. You can use both type of sets to
perform set algebra to construct particular identifier subsets of interest to
your model.

\paragraph{Example}

If your model contains a section {\tt Unit Model}, you can assign the
collection of all parameters in that section to a subset {\tt
UnitModelParameters} of {\tt AllIdentifiers} through the assignment
\begin{example}
    UnitModelParameters := Unit_Model * AllParameters;
\end{example}

\paragraph{Model query functions}

Another method to construct meaningful subsets of {\tt AllIdentifiers} consists of
using the functions provided to query aspects of those identifiers. Selected examples are:
\begin{itemize}
\item the function {\tt IdentifierDimension} returning the dimension of the identifier,
\item the function {\tt IdentifierType} returning the type of the identifier as an element of {\tt AllIdentifierTypes},
\item the function {\tt IdentifierText} returning the contents of the {\tt TEXT} attribute, and
\item the function {\tt IdentifierUnit} returning the contents of the {\tt UNIT} attribute.
\end{itemize}

These functions take as argument an element in the set {\tt AllIdentifiers}.

\paragraph{Functions accepting identifier index}

In addition to the functions lists above, the functions {\tt Card} and {\tt
ActiveCard} also accept an index into the set {\tt AllIdentifiers}. They will
then return the cardinality of the identifier represented by the index, or the
cardinality of the active elements of that identifier, respectively. You can
also use these functions to dynamically construct a subset of {\tt
AllIdentifiers}.

\paragraph{Example}

The set expression
\begin{example}
    { IndexIdentifiers in UnitModelParameters | 
            IdentifierDimension( IndexIdentifier ) = 3 }
\end{example}
refers to the collection of all 3-dimensional parameter in the section {\tt
Unit Model}.

\paragraph{Working with identifier sets}

The following operators in AIMMS support identifier subsets to represent a
collection of individual identifiers:
\begin{itemize}
\item the {\tt READ} and {\tt WRITE} operators,
\item the {\tt EMPTY}, {\tt CLEANUP}, {\tt CLEANDEPENDENTS}, and {\tt REBUILD} operators.
\end{itemize}
If you are interested in the contents of an identifier subset, you can use the
{\tt DISPLAY} operator, which will just print the identifier names contained in
the set, rather than the contents of the identifiers referred to in the
identifier set as is the case for the {\tt WRITE} statement.

\paragraph{Functions accepting identifier sets}

In addition to the operators above, the following AIMMS functions also
operate on subsets of {\tt AllIdentifiers}:
\begin{itemize}
\item {\tt GenerateXML},
\item {\tt CaseCompareIdentifier},
\item {\tt CaseCreateDifferenceFile},
\item {\tt IdentifierMemory},
\item {\tt GMP::Solution::SendToModelSelection},
\item {\tt VariableConstraints},
\item {\tt ConstraintVariables},
\item {\tt ScalarValue},
\item {\tt SectionIdentifiers},
\item {\tt AttributeToString},
\item {\tt IdentifierAttributes}.
\end{itemize}

See also Section "Model Query Functions" on page~\ref{FRchap:model.query.functions} of AIMMS the Function Reference.

\section{The \tttext{File} declaration}\label{sec:report.file}

\tipparagraph{{\tt File} declaration}{file.decl} \declindex{File}
\AIMMSlink{file} \AIMMSlink{file_identifier}

\syntaxmark{file-identifier}
External file names that you want to use for reporting must be linked
to AIMMS identifiers in your model. In this way, external file
names become data. Whenever you want to send output to a particular
external file, you must refer to its associated identifier. This
linking is achieved using a
\verb|File| declaration, the attributes of which are given in
Table~\ref{table:report.attr-file}.
\begin{aimmstable}
\begin{tabular}{|l|l|c|}
\hline\hline
{\bf Attribute} & {\bf Value-type}  & {\bf See also} \\
&& {\bf page}\\
\hline
\verb|Name|         & {\em string-expression}       &      \\
\verb|Device|       & {\tt disk}, {\tt window}, {\tt void}. & \\
\verb|Mode|         & {\tt replace}, {\tt merge} & \\
\verb|Encoding|     & an element in {\tt AllCharacterEncodings} & \pageref{text.file.encoding} \\
\verb|Text|         & {\em string}             & \pageref{attr:prelim.text}\\
\verb|Comment|      & {\em comment string}     &
\pageref{attr:prelim.comment}\\
\verb|Convention|   & {\em convention} & \pageref{attr:db.convention},
\pageref{sec:units.convention} \\
\hline\hline
\end{tabular}
\caption{{\tt File} attributes}\label{table:report.attr-file}
\end{aimmstable}

\paragraph{The {\tt Name} attribute}
\declattrindex{file}{Name}
\AIMMSlink{file.name}

With the {\tt Name} attribute you can specify the actual name of the
disk file or window that you want to refer to. If the file identifier
refers to a disk file, the {\tt Name} will be the file name on disk.
If it refers to a window the {\tt Name} attribute will serve as the
title of the window. If you do not specify a name, AIMMS will
construct a default name, using the internal identifier name as the
root and ``\verb|.put|'' as the extension.

\paragraph{The {\tt Device} attribute}
\declattrindex{file}{Device}
\AIMMSlink{file.device}
\AIMMSlink{disk}
\AIMMSlink{window}
\AIMMSlink{void}

The {\tt Device} attribute can have three values
\begin{itemize}
\item {\tt disk} (default),
\item {\tt window}, and
\item {\tt void}.
\end{itemize}
You can use it to indicate whether the output should be directed to an
external file on disk, a window in the graphical user interface, or
whether no output should be generated at all. This latter {\tt void}
device is very convenient, for instance, to hide output statements in
your code that are useful during the development of your model but
should not be displayed in an end-user version. 

\paragraph{The {\tt Mode} attribute}
\declattrindex{file}{Mode}
\AIMMSlink{file.mode}
\subtypindex{mode}{MERGE}{in FILE declaration@{in {\tt File} declaration}}
\subtypindex{mode}{REPLACE}{in FILE declaration@{in {\tt FILE} declaration}}

You can use the {\tt Mode} attribute to specify whether the file or
window should be overwritten ({\tt replace} mode, default), or
appended to ({\tt merge} mode). The graphical window in the user
interface differs from a file in that it can be closed manually by the
user. In this case, its contents are lost and AIMMS starts writing
to a new instance regardless of the mode.

\paragraph{Example}

The following {\tt File} declarations illustrate the declaration of a
link to the external file ``{\tt result.dat}'' in the {\tt Output} subdirectory of the project directory, and a text window that
will appear with the title ``{\tt Model results}''. The contents of
{\tt ResultFile} will be overwritten whenever it is opened, while the
window {\tt ResultWindow} will be appended to whenever possible.
\begin{example}
File ResultFile {
    Name       : "Output\\result.dat";
    Device     : disk;
    Mode       : replace;
}
File ResultWindow {
    Name       : "Model results";
    Device     : window;
    Mode       : merge;
}
\end{example}


\paragraph{The {\tt Encoding} attribute}
\declattrindex{file}{Encoding}
\AIMMSlink{file.encoding}
\herelabel{attr.file.encoding}

In the {\tt Encoding} attribute of a file, a specific character encoding can be specified for that file,
either as a specific element of the set {\tt AllCharacterEncodings} or as an element parameter with the set {\tt AllCharacterEncodings} as its range.
Encodings are explained in Paragraph {\em Text files} on Page~\pageref{text.file.encoding}.
In the example below, the attribute {\tt Encoding}   
states that code page {\tt WINDOWS-1252} should be used for the file {\tt WindmillLocations.txt}.  
This code page is not uncommon in the Netherlands.
\begin{example}
File WindMillLocs {
    Name       :  "WindmillLocations.txt";
    Encoding   :  'WINDOWS-1252';
}
\end{example}
The statement {\tt Write to file WindMillLocs ;} will subsequently write the 
file {\tt "WindmillLocations.txt"} using the character encoding {\tt WINDOWS-1252}.
When the {\tt Encoding} attribute is not specified, the statements {\tt Read} {\tt from} {\tt file} 
and {\tt Write} {\tt to} {\tt file} will use the encodings specified by the options
{\tt default\_input\_character\_\-en\-coding} and 
{\tt default\_output\_character\_encoding} respectively.
The default of these options is the preferred encoding {\tt UTF8}. 
The {\tt Encoding} attribute is ignored when reading from files which start with a Unicode BOM (Byte Order Mask).


\paragraph{The {\tt Convention} attribute}\herelabel{attr:file.convention}
\declattrindex{file}{Convention}
\AIMMSlink{file.convention}

With the {\tt Convention} attribute you can indicate that AIMMS
must assume that the data in the file is to be stored according to the
units provided in the specified convention. If the unit specified in
the convention differs from the unit in which AIMMS stores its data
internally, the data is scaled just prior to data transfer. For the
declaration of {\tt Conventions} you are referred to
Section~\ref{sec:units.convention}.

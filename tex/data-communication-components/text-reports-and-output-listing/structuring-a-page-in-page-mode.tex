\section{Structuring a page in page mode}\label{sec:report.page-mode}

\paragraph{Page-based files}
\index{page mode}
\index{stream mode}
\index{file!page versus stream mode}
\subtypindex{statement}{PUT}{page versus stream mode}

In addition to the continuous stream mode of operation of the {\tt
PUT} statement discussed in the previous section, AIMMS also
provides a page-based file format. AIMMS divides a page-based file
into pages of a specified length, each consisting of a header, a body,
and a footer. Figure~\ref{fig:report.page-mode} gives an overview of a page
in a page-based report.
\begin{aimmsfigure}
\psset{xunit=0.25cm,yunit=0.25cm,dimen=middle,dash=2.5pt 1.5pt}
\begin{pspicture}(-7,-3)(30,32)
\psframe[linewidth=1.2pt](0,0)(20,30)
\psframe[linewidth=0.6pt](1,1)(19,4.9)\rput[l](4,3){{\tt PUTFT} statement}
\psframe[linewidth=0.6pt](1,5.1)(19,24.9)\rput[l](4,15){{\tt PUT} statement}
\psframe[linewidth=0.6pt](1,25.1)(19,29)\rput[l](4,27){{\tt PUTHD} statement}

\psline[linestyle=dashed](19,1)(22,1)
\psline[linestyle=dashed](19,4.9)(22,4.9)
\psline[linestyle=dashed](19,5.1)(22,5.1)
\psline[linestyle=dashed](19,24.9)(22,24.9)
\psline[linestyle=dashed](19,25.1)(22,25.1)
\psline[linestyle=dashed](19,29)(22,29)
\psline[linestyle=dashed](-2,1)(2,1)
\psline[linestyle=dashed](-2,29)(2,29)
\psline[linestyle=dashed](1,1)(1,-2)
\psline[linestyle=dashed](19,1)(19,-2)

\psline{<->}(21,1)(21,4.9)
\rput[l](23,3){{\tt .FooterSize} suffix}

%\psline{<->}(21,5)(21,7)
%\rput[l](23,6){{\tt .BottomMargin} suffix}

\psline{<->}(21,5.1)(21,24.9)
\rput[l](23,15){{\tt .BodySize} suffix}

%\psline{<->}(21,23)(21,25)
%\rput[l](23,24){{\tt .TopMargin} suffix}

\psline{<->}(21,25.1)(21,29)
\rput[l](23,27){{\tt .HeaderSize} suffix}

\psline{<->}(-1,1)(-1,29)
\rput[r](-3,15){{\tt .PageSize} suffix}

\psline{<->}(1,-1)(19,-1)
\rput[t](10,-1.5){{\tt .PageWidth} suffix}

\end{pspicture}
\caption{Overview of a page in a page based report}\label{fig:report.page-mode}
\end{aimmsfigure}

\paragraph{Switching to page mode}
\index{page mode!switch to}
\index{stream mode!switch to}
\suffindex{file}{PageMode}

You can switch between page and stream by setting the {\tt .PageMode}
suffix of a file identifier to {\tt 'on'} or {\tt 'off'} (the elements
of the predefined set {\tt OnOff}), respectively, as in the statement
\verb|ResultFile.PageMode := 'on'|. The value of the
{\tt .PageMode} suffix is {\tt 'off'} by default. When switching to
another mode AIMMS will begin with a new page or close the last
page.

\paragraph{Page size and width}
\index{page mode!page size and width}
\suffindex{file}{PageSize}
\suffindex{file}{PageWidth}

The default page size is 60 lines. You can overwrite this default by
setting the \verb|.PageSize| suffix of the file identifier to another
positive integer value. For instance,
\verb|ResultFile.PageSize := 10| will give short pages with only ten
lines per page. The default page width is 132 columns. You can change
this default by setting the {\tt .PageWidth} suffix of the file
identifier.

\paragraph{Headers and footers}
\index{page mode!header and footer}
\index{header (page mode)}
\index{footer (page mode)}

The header and footer of a document can be specified by using the
\verb|PUTHD| and \verb|PUTFT| statements. They are equivalent to the
\verb|PUT| statement but write in the header and footer area instead
of in the page body. The size of the header and footer is not preset,
but is determined by the contents of the \verb|PUTHD| and
\verb|PUTFT| statements.  The header and footer keep their contents
from page to page.

\paragraph{Margins}

There are no specific attributes for either the top, bottom, left or
right margins of a page. You essentially control these margins by
either resizing the header or footer of a page, or by positioning the
{\tt PUT} items in a starting column of your choice using the {\tt @}
operator of the {\tt PUT} statement.

\paragraph{Page structure}

Table~\ref{table:report.file-suffix} summarizes the file attributes
for structuring pages. With the exception of the page body size (read
only) you can modify their defaults by using assignment statements.
\begin{aimmstable}
\suffindex{file}{PageMode}
\suffindex{file}{PageSize}
\suffindex{file}{PageWidth}
\suffindex{file}{PageNumber}
\suffindex{file}{BodyCurrentColumn}
\suffindex{file}{BodyCurrentRow}
\suffindex{file}{BodySize}
\suffindex{file}{HeaderCurrentColumn}
\suffindex{file}{HeaderCurrentRow}
\suffindex{file}{HeaderSize}
\suffindex{file}{FooterCurrentColumn}
\suffindex{file}{FooterCurrentRow}
\suffindex{file}{FooterSize}
\AIMMSlink{pagemode}
\AIMMSlink{pagesize}
\AIMMSlink{pagewidth}
\AIMMSlink{pagenumber}
\AIMMSlink{bodycurrentcolumn}
\AIMMSlink{bodycurrentrow}
\AIMMSlink{bodysize}
\AIMMSlink{headercurrentcolumn}
\AIMMSlink{headercurrentrow}
\AIMMSlink{headersize}
\AIMMSlink{footercurrentcolumn}
\AIMMSlink{footercurrentrow}
\AIMMSlink{footersize}
\begin{tabular}{|l||l|c|}
\hline\hline
{\bf Suffix} & {\bf Description} & {\bf Default} \\
\hline
{\tt .PageMode} &  Mode         & {\tt 'off'} \\
{\tt .PageSize} & Page size    & 60 \\
{\tt .PageWidth} & Page width   & 132 \\
{\tt .PageNumber}   & Current page number& 1 \\
\hline
{\tt .BodyCurrentColumn}   & Body current column & -- \\
{\tt .BodyCurrentRow}      & Body current row & -- \\
{\tt .BodySize}  & Body size & -- \\
\hline
{\tt .HeaderCurrentColumn} & Header current column & -- \\
{\tt .HeaderCurrentRow}    & Header current row & -- \\
{\tt .HeaderSize}   & Header size & -- \\
\hline
{\tt .FooterCurrentColumn} & Footer current column & -- \\
{\tt .FooterCurrentRow}     & Footer current row & -- \\
{\tt .FooterSize}    & Footer size & -- \\
\hline\hline
\end{tabular}
\caption{Page structure attributes}\label{table:report.file-suffix}
\end{aimmstable}

\paragraph{Positioning in page mode}
\index{page mode!cursor positioning}

The positioning operators \verb|@|, \verb|#|, and \verb|/| explained
in Section~\ref{sec:report.put} are also applicable in page mode. However,
AIMMS offers you additional file attributes for positioning items in a
page-based file.

\paragraph{Current row and column}

Whenever you \verb|PUT| an item into a header, footer, or page body,
there is a current row and a current column. AIMMS keeps track of
which row and column are current through the suffices {\tt
.BodyCurrentRow} and {\tt .BodyCurrentColumn} of the {\tt File}
identifier. You can either read or overwrite these values using
assignment statements. Similar suffices also exist for the header and
the footer area.

\paragraph{Modifying size of page sections}

After having specified the header, footer, or page body, you may want
to change their size at some stage during the process of writing
pages. By specifying the \verb|.BodySize|, {\tt .HeaderSize} and {\tt
.FooterSize} suffices you can modify the size (or empty) the page
body, the header, or the footer. The value of the {\tt .BodySize}
suffix can be at most the value of the {\tt .PageSize} suffix minus
the value of the {\tt .HeaderSize} and {\tt .FooterSize} suffices.

\paragraph{Printing the page number}
\index{page mode!page number}

Whenever you write the contents of the {\tt .PageNumber} suffix of a
{\tt File} identifier in its header or the footer area, AIMMS will
replace it with the current page number whenever it prints a page of
a page based report. By default, the first page will be numbered 1,
but you can override this by assigning another value to the {\tt
.PageNumber} suffix.


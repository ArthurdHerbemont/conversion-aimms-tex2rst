\section{The \tttext{DISPLAY} statement}\label{sec:report.display}

\paragraph{Output in AIMMS format}
\statindex{DISPLAY}
\AIMMSlink{display}

You can use the \verb|DISPLAY| statement to print the data associated
with sets, parameters and variables to a file or window in AIMMS
format. As this format is also very easy to read, the {\tt DISPLAY}
statement is an excellent alternative for printing indexed
identifiers.

\paragraph[0.8]{Syntax}
\operindex{:}

\begin{syntax}
\syntaxdiagram{display-statement}{display}
\syntaxdiagram{display-format}{display-spec}
\end{syntax}

\paragraph{Display format}
\subtypindex{statement}{DISPLAY}{default format}

All data selections of a {\tt DISPLAY} statement are printed by
AIMMS in the form of a data assignment.
\begin{itemize}
\item Sets are printed in the form of a set assignment with an {\em
enumerated set} on the right-hand side.
\item (Slices of) parameters and variables are printed in the form of
data assignments, which can be either a table format, a list format,
or a composite table.
\end{itemize}
For indexed parameters and variables AIMMS uses a default display
format which is dependent on the dimension.

\paragraph{Overriding the display format}
\subtypindex{statement}{DISPLAY}{format specification}
\index{format specification!in DISPLAY statement@{in {\tt DISPLAY} statement}}

\syntaxmark{format-specifier}
You can override the default AIMMS format by specifying a {\em
display format}, consisting of one or more format specifications, in the {\tt WHERE} clause.
AIMMS supports the following format specifiers:
\keyindex{DECIMALS}
\keyindex{ROWDIM}
\keyindex{COLDIM}
\keyindex{COLSPERLINE}
\begin{itemize}
\item {\tt DECIMALS}: the number of decimals to be printed for each entry,
\item {\tt ROWDIM}: the dimension of the row space,
\item {\tt COLDIM}: the dimension of the column space, and
\item {\tt COLSPERLINE}: the desired numbers of columns per line.
\end{itemize}
When a format specifier is not specified, AIMMS will use the system default.

\paragraph{Number of decimals}

All format specifications in a {\tt WHERE} clause are applied to the entire collection of data selections printed in the {\tt DISPLAY} statement. By specifying a {\tt DECIMAL} format specifier for a particular data selection in the {\tt DISPLAY} statement, you can also override the number of decimals printed for each data selection individually. You cannot specify other format specifiers for individual data selections.

\paragraph{Obtaining lists and tables}
\index{table!created by DISPLAY statement@{created by {\tt
DISPLAY} statement}}
\index{list!created by DISPLAY statement@{created by {\tt
DISPLAY} statement}}

If you have set the dimension of either the row or column space to
zero, AIMMS will print the identifier in list format. If both the
dimension of the row and column space are greater than zero, AIMMS
will print the identifier as a table. AIMMS will honor your request
to print the desired number of columns per line if the resulting width
does not exceed the default page width. In the latter case, AIMMS
will reduce the number of columns until they fit within the requested
page width. The default page width can be set as an option within your
project.

\paragraph{Outer indices for slicing}

If the sum of the dimensions of the row and column space is less than
the dimension of the parameter or variable to be displayed, AIMMS
will display the identifiers as slices of the requested format, where
the slices are taken by fixing the first indices in the domain.

\paragraph{Composite tables}
\index{composite table!created by DISPLAY statement@{created by {\tt
DISPLAY} statement}}

When all arguments of the {\tt DISPLAY} statement have the same domain
and you enclose them by braces, AIMMS will print their values as a
single composite table. In this case, you can only specify the
precision with which each column must be printed. AIMMS will ignore
any of the other
display options in combination with the composite table format.

\paragraph{Example}
\subtypindex{statement}{DISPLAY}{example of use}

The following statements illustrate the use of the {\tt DISPLAY}
statement and its various display options.
\begin{itemize}
\item The following statement will display the data of the variable {\tt
Transport} with 2 decimals and in the default format.
\begin{example}
    display Transport where decimals := 2;
\end{example}
The execution of this statement results in the following output being
generated.
\begin{example}
    Transport :=
    data table
             Amsterdam   Rotterdam  'Den Haag'
         !  ----------  ----------  ----------
 Amsterdam        2.50        2.50        5.00
 Rotterdam        2.50        5.00        5.00
'Den Haag'                    2.50        5.00
    ;
\end{example}
\item The following statement displays the subselection of the slice
of the variable {\tt Transport} consisting of all transports departing
from the set {\tt LargeSupplyCities}.
\begin{example}
    display Transport(i in LargeSupplyCities,j) where decimals := 2;
\end{example}
This statement will result in the following table, assuming that
{\tt Large\-SupplyCities} contains only {\tt Amsterdam} and {\tt
Rotterdam}.
\begin{example}
    Transport :=
    data table
             Amsterdam   Rotterdam  'Den Haag'
         !  ----------  ----------  ----------
 Amsterdam        2.50        2.50        5.00
 Rotterdam        2.50        5.00        5.00
    ;
\end{example}
\item The following {\tt DISPLAY} statement displays {\tt Transport}
with no rows, two columns (i.e.\ in list format), and two entries
per line.
\begin{example}
    display Transport where decimals:=2, rowdim:=0, coldim:=2, colsperline:=2;
\end{example}
The resulting output looks as follows.
\begin{example}
    Transport := data
    { ( Amsterdam , Amsterdam  ) : 2.50,  ( Amsterdam , Rotterdam  ) : 2.50,
      ( Amsterdam , 'Den Haag' ) : 5.00,  ( Rotterdam , Amsterdam  ) : 2.50,
      ( Rotterdam , Rotterdam  ) : 5.00,  ( Rotterdam , 'Den Haag' ) : 5.00,
      ( 'Den Haag', Rotterdam  ) : 2.50,  ( 'Den Haag', 'Den Haag' ) : 5.00 } ;
\end{example}
\item In the following {\tt DISPLAY} statement the row and column display
dimensions do not add up to the dimension of {\tt Transport}.
\begin{example}
    display Transport where decimals:=2, rowdim:=0, coldim:=1, colsperline:=3;
\end{example}
As a result AIMMS considers the indices corresponding to the
dimension deficit as outer, and displays {\tt Transport} by means of
three one-dimensional displays, each of the requested dimension.
\begin{example}
    Transport('Amsterdam', j) := data
    { Amsterdam  : 2.50,  Rotterdam  : 2.50,  'Den Haag' : 5.00 } ;

    Transport('Rotterdam', j) := data
    { Amsterdam  : 2.50,  Rotterdam  : 5.00,  'Den Haag' : 5.00 } ;

    Transport('Den Haag', j) := data
    { Rotterdam  : 2.50,  'Den Haag' : 5.00 } ;
\end{example}
\item The following {\tt DISPLAY} statement illustrates how a
composite table can be obtained for identifiers defined over the same
domain, with a different number of decimals for each identifier.
\begin{example}
    display { Supply decimals := 2, Demand decimals := 3 };
\end{example}
Execution of this statement results in the creation of the following
one-dimensional composite table.
\begin{example}
Composite table:
    i             Supply   Demand
!   ----------    ------  -------
    Amsterdam      10.00    5.000
    Rotterdam      12.50   10.000
    'Den Haag'      7.50   15.000
    ;
\end{example}
\end{itemize}

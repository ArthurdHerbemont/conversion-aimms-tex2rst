from datetime import datetime
import fitz
import LRConvertTEX2RST as c
import openpyxl
import os
import sys

# Directories
rst_directory = os.path.abspath(r'rst')
tex_directory = os.path.abspath(r'tex')

# List of functions
with open('list_of_functions.txt', 'r', encoding='latin-1') as f:
    funcs = f.read().split()

# Define path to directory containing syntax diagrams
path_to_syntax = os.path.abspath('syntax')

# Define path to TeX-file
#tex_file = os.path.join(tex_directory, r'preliminaries\language-preliminaries\lexical-conventions.tex')

if len(sys.argv)>1:
  tex_file = sys.argv[1]
else:
  tex_file = "debug.tex"

# Convert
# try:
function_code = c.convert(tex_file, funcs, path_to_syntax)
print(function_code)
# rst_file = tex_file.replace('\\tex\\', '\\rst\\').replace(r'.tex', r'.rst')
# c.write_rst(function_code, rst_file)
# except Exception as e:
#     print(e)

# # Create list of all relevant files present in the specified directory and its subdirectories
# all_tex_files = []
# for root, dirs, files in os.walk(tex_directory):
#     for file in files:
#         if file.endswith('.tex'):
#             all_tex_files.append(os.path.join(root, file))
#
# start = datetime.now()
# print(str(start) + ' Conversion started...\n\n')
#
# for tex_file in all_tex_files:
#     try:
#         rst_code = c.convert(tex_file, funcs, path_to_syntax)
#         rst_file = tex_file.replace('\\tex\\', '\\rst\\').replace(r'.tex', r'.rst')
#         c.write_rst(rst_code, rst_file)
#     except Exception as err:
#         print('File did not pass: ' + tex_file)
#         print(err), print('\n')
#         pass
#
# end = datetime.now()
#
# print('\n' + str(end) + ' Conversion completed!\n')
# print('\nTotal time elapsed = ' + str(end - start) + ' \n')

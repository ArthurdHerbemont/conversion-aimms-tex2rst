import html
import os
import pdb
import pypandoc
import re
from rst_crossrefs import add_crossrefs
import string
from textwrap import dedent, indent


def pandoc_tex2rst(strng):
    return pypandoc.convert_text(strng, 'rst', format='latex')


def remove_tex(code):
    """Remove TeX-commands and other TeX-elements from TeX-code.

    Parameters
    ----------
    code : str
        TeX-code.

    Returns
    -------
    code : str
        Modified TeX-code.

    """
    # Remove comments preceded by '%', only if '%' is not located in 'example' environments and not preceded by '\'
    def comment(matchobj):
        if matchobj.group(1):
            return matchobj.group(1)
        elif matchobj.group(2):
            return matchobj.group(2)
        else:
            return ''
    code = re.sub(r'(\\begin{example}[\s\S]+?\\end{example})|'
                  r'(\\verb(?P<verb>\W).*?(?P=verb))|'
                  r'(?<!\\)(%.*)', comment, code)

    if r'\def' in code:
        code = re.sub(r'\\def\\.+?{[^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*}', r'', code)

    if r'\hbox' in code:
        code = re.sub(r'(?:\\\\\s?)?\\hbox to ?[\w.]+{\\hfill}', r'', code)
        code = re.sub(r'(?:\\hbox{)?\\hbox to ?[\w.]+\\textwidth{\\vtop{', r'', code)

    if r'\hss' in code:
        code = re.sub(r'}\\hss}}?', r'', code)

    # Remove commands without argument
    commands_noarg = [r'\begin{center}', r'\end{center}', r'\examplefont\em', r'\pbsr', r'\PBS', r'\selectfont',
                      r'\syntaxcommment', r'\Varr', r'\vskip', r'{\allowbreak}']
    for command in commands_noarg:
        if command in code:
            if command == r'\vskip':
                code = re.sub(r'\\vskip[\w.]*', r'', code)
            else:
                code = re.sub(re.escape(command) + r'\b', r'', code)

    # Remove commands with one argument, e.g. \command{arg1}
    commands_arg = [r'\AL', r'\attrindex', r'\declindex', r'\DFI', r'\dfuncindex', r'\efuncindex', r'\eliterindex',
                    r'\fcstatindex', r'\FI', r'\funcindex', r'\index', r'\IndexRef', r'\input', r'\iterindex',
                    r'\keyindex', r'\logiterindex', r'\operindex', r'\preparindex', r'\PreRef', r'\presetindex',
                    r'\procindex', r'\propindex', r'\setiterindex', r'\sfuncindex', r'\statindex', r'\statiterindex',
                    r'\strfuncindex', r'\sufindex']
    commands_arg_regex = r'{[^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*}'
    for command in commands_arg:
        if command in code:
            code = re.sub(re.escape(command) + commands_arg_regex, r'', code)

    # Remove commands with two arguments, e.g. \command{arg1}{arg2}
    commands_2arg = [r'\declattr', r'\declattrindex', r'\declpropindex', r'\fontsize', r'\funcindexns', r'\newcommand',
                     r'\procindexns', r'\size', r'\suffindex', r'\typindex']
    commands_2arg_regex = 2 * commands_arg_regex
    for command in commands_2arg:
        if command in code:
            code = re.sub(re.escape(command) + commands_2arg_regex, r'', code)

    # Remove commands with three arguments, e.g. \command{arg1}{arg2}{arg3}
    commands_3arg = [r'\subtypindex']
    commands_3arg_regex = 3 * commands_arg_regex
    for command in commands_3arg:
        if command in code:
            code = re.sub(re.escape(command) + commands_3arg_regex, r'', code)

    # Remove commands with one argument, e.g. \command{arg1}, but keep argument
    commands_keeparg = [r'\mathit', r'\r']
    commands_keeparg_regex = r'{([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)}'
    for command in commands_keeparg:
        if command in code:
            code = re.sub(re.escape(command) + commands_keeparg_regex, r'\1', code)

    # Remove commands with in-between decimal arguments, e.g. \command0.75\command
    commands_in_between = [r'\tabcolsep']
    for command in commands_in_between:
        if command in code:
            code = re.sub(re.escape(command) + r'\d+[.,]?\d+' + re.escape(command), r'', code)

    return code


def tex_replace_tex(code, funcs, file):
    """Replace TeX-elements with alternative, Pandoc-convertible TeX-elements in TeX-code.

    Parameters
    ----------
    code : str
        TeX-code.
    funcs : list
        List of functions in the AIMMS Function Reference (drawn from Intersphinx).
    file : str
        Path to TeX-file.

    Returns
    -------
    code : str
        Modified TeX-code.

    """
    # Inline markup
    if r'\mbox{\bf' in code:
        for mboxbf_object in re.finditer(r'\\mbox{\\bf(\s.*?)}', code):
            mboxbf_content = re.sub(r'\s', r'\:', mboxbf_object.group(1).strip())
            code = code.replace(mboxbf_object.group(0), r'\mathbf{' + mboxbf_content + r'}')

    if r'\mbox{\em' in code:
        for mboxem_object in re.finditer(r'\\mbox{\\em(\s.*?)}', code):
            mboxem_content = re.sub(r'\s', r'\:', mboxem_object.group(1).strip())
            code = code.replace(mboxem_object.group(0), r'\mathit{' + mboxem_content + r'}')

    if r'\mbox{\tt' in code:
        for mboxtt_object in re.finditer(r'\\mbox{\\tt(\s.*?)}', code):
            mboxtt_content = re.sub(r'\s', r'\:', mboxtt_object.group(1).strip())
            code = code.replace(mboxtt_object.group(0), r'\mathtt{' + mboxtt_content + r'}')

    if r'\hbox{\rm ' in code:
        for hboxrm_object in re.finditer(r'\\hbox{\\rm(\s.*?)}', code):
            hboxrm_content = re.sub(r'\s', r'\:', hboxrm_object.group(1).strip())
            code = code.replace(hboxrm_object.group(0), r'\mathrm{' + hboxrm_content + r'}')

    if r'\bftt' in code:
        code = code.replace(r'\bftt', r'\bf')

    if r'{\bf' in code:
        for bf_object in re.finditer(r'{\\bf(?:{}\s*|\s+)', code):
            code = code.replace(bf_object.group(0), r'\textbf{')

    if r'{\em' in code:
        for em_object in re.finditer(r'{\\em(?:{}\s*|\s+)', code):
            code = code.replace(em_object.group(0), r'\textit{')

    if r'\em\tt' in code:
        code = code.replace(r'\em\tt', r'\tt')

    if r'{\it' in code:
        for it_object in re.finditer(r'{\\it(?:{}\s*|\s+)', code):
            code = code.replace(it_object.group(0), r'\textit{')

    if r'\tt' in code:
        for tt_object in re.finditer(r'\\tt(?:{}\s*|\s+)([^}{]*(?:{[^}{]*}[^}{]*)*?)}', code):
            code = code.replace(tt_object.group(0), r'\texttt{' + tt_object.group(1).strip() + r'}}')

    if r'\tt\em' in code:
        code = code.replace(r'\tt\em', r'\tt')

    if r'\tttext' in code:
        code = code.replace(r'\tttext{', r'\texttt{')

    if r'\texttt' in code:
        for tt_object in re.finditer(r'([^\s{])(\\texttt{)', code):
            if tt_object.group(1):
                code = code.replace(tt_object.group(0), tt_object.group(1) + ' ' + tt_object.group(2))

    if r'{\sc' in code:
        def uppercase(matchobj):
            return matchobj.group(1).upper()

        code = re.sub(r'{\\sc ([^}{]*(?:{[^}{]*}[^}{]*)*)}', uppercase, code)

    if r'{\sf\em ' in code:
        code = code.replace(r'{\sf\em ', r'{\em ')

    if r'\sf\it' in code:
        code = code.replace(r'\sf\it', r'\em')

    if r'\underline' in code:
        def underline(matchobj):
            return '\\textbf' if matchobj.group(5) else matchobj.group(0)
        code = re.sub(r'(?s)'
                      r'(\$?\$.*?\$\$?)|'
                      r'(\\[\[(].*?\\[\])])|'
                      r'(\\begin{equation\*?}.*?\\end{equation\*?})|'
                      r'(\\begin{(?:display)?math}.*?\\end{(?:display)?math})|'
                      r'(\\underline)', underline, code)

    if r'\ExtFRef' in code:
        code = code.replace(r'\ExtFRef{', r'\texttt{')

    # Math
    if r'{align' in code:
        code = re.sub(r'{align[\w*]+?}(?:{\d*})?', r'{align}', code)

    if r'\Cond' in code:
        code = re.sub(r'\\Cond\b', r'\; | \;', code)

    if r'\MININF' in code:
        code = re.sub(r'\\MININF\b', r'-\\infty', code)

    if r'\PLUSINF' in code:
        code = re.sub(r'\\PLUSINF\b', r'\\infty', code)

    if r'\R' in code:
        for mathenv_object in re.finditer(r'(\$?\$.*\\R\b.*?\$\$?)|'
                                          r'(\\\[.*?\\R\b.*?\\\])|'
                                          r'(\\begin{equation\*?}.*?\\R\b.*?\\end{equation\*?})|'
                                          r'(\\begin{displaymath}.*?\\R\b.*?\\end{displaymath})', code, re.DOTALL):
            mathenv_modified = re.sub(r'\\R\b', r'\\mathbb{R}', mathenv_object.group(0))
            code = code.replace(mathenv_object.group(0), mathenv_modified)

    if r'\sign' in code:
        code = code.replace(r'\sign', r'\mathrm{sign}')

    # Other
    if r'\afterpage{' in code and r'\end{aimmstable}}' in code:
        code = code.replace(r'\afterpage{', '').replace(r'\end{aimmstable}}', r'\end{aimmstable}')

    if r'\aimms' in code:
        def aimms(matchobj):
            if matchobj.group(1) == r'~':
                if matchobj.group(0)[0] == r'{':
                    return r'{AIMMS '
                else:
                    return r'AIMMS '
            else:
                return r'AIMMS'
        code = re.sub(r'{?\\aimms([}~])?', aimms, code)

    if r' \bdiv ' in code:
        code = code.replace(r' \bdiv ', r'\;\mathrm{div}\;')

    if r'\gmp' in code:
        def gmp(matchobj):
            if matchobj.group(1) == r'~':
                if matchobj.group(0)[0] == r'{':
                    return r'{GMP '
                else:
                    return r'GMP '
            else:
                return r'GMP'
        code = re.sub(r'{?\\(?:gmp|GMP)([}~])?', gmp, code)

    if r'\win32' in code:
        def win32(matchobj):
            if matchobj.group(1) == r'~':
                if matchobj.group(0)[0] == r'{':
                    return '{\\texttt{Win32} '
                else:
                    return '\\texttt{Win32} '
            else:
                return '\\texttt{Win32}'
        code = re.sub(r'{?\\win32([}~])?', win32, code)

    if r'\guidefig' in code:
        def guidefig_png(matchobj):
            return r'\includegraphics{{{}.png}}'.format(matchobj.group(1))
        code = re.sub(r'(?:\\hskip\.75cm\\vbox{\\hbox{)?\\guidefig{(.+?)}(?:}})?', guidefig_png, code)

    if r'\begin{minipage}' in code:
        code = re.sub(r'(?s)\\begin{minipage}(?:\[\S*?\])*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*})?(.*?)\\end{minipage}',
                      r'\1', code)

    if r'\begin{table}' in code:
        code = code.replace(r'\begin{table}', r'\begin{aimmstable}')
        code = code.replace(r'\end{table}', r'\end{aimmstable}')

    if r'\url{www' in code:
        code = code.replace(r'\url{www', r'\url{http://www')

    if 'aimmsfigure' in code:
        code = code.replace(r'\begin{aimmsfigure}', r'\begin{figure}' + '\n' + r'\centering')
        code = code.replace(r'\end{aimmsfigure}', r'\end{figure}')

    if r'\newline' in code:
        code = code.replace(r'\newline', r'\\')

    if r'\char`\\' in code:
        code = code.replace(r'\char`\\', '\\')

    if r'\CLANGUAGE/' in code:
        code = code.replace(r'\CLANGUAGE/', r'\texttt{C}')

    if r'{\C}++' in code:
        code = code.replace(r'{\C}++', r'\texttt{C++}')

    if r'{\C}' in code:
        code = code.replace(r'{\C}', r'\texttt{C}')

    if r'\C~' in code:
        code = code.replace(r'\C~', r'\texttt{C} ')

    if 'C' in code:
        def monospace_c(matchobj):
            if matchobj.group(2):
                return matchobj.group(2).replace(r'C', r'\texttt{C}')
            elif matchobj.group(1):
                return matchobj.group(1)
            else:
                return matchobj.group(0)
        code = re.sub(r'(?s)(\\begin{example}.+?\\end{example})|((?:^| )C[^\w}:])', monospace_c, code)

    if r'\fortran' in code:
        code = code.replace(r'{\fortran}', r'\fortran').replace(r'\fortran', r'\texttt{FORTRAN}')

    if r'\PASCAL/' in code:
        code = code.replace(r'\PASCAL/', r'\texttt{Pascal}')

    if r'\pascal' in code:
        code = code.replace(r'{\pascal}', r'\pascal').replace(r'\pascal', r'\texttt{PASCAL}')

    if r'\figscale' in code:
        code = re.sub(r'\\figscale\[\S*\]', r'\\includegraphics', code)

    # Cut \AIMMSlinks from TeX-environments and paste them before the environment
    for env_obj in re.finditer(r'\\begin{(?P<env>\w+)}([\s\S]*?)\\end{(?P=env)}', code):
        if r'\AIMMSlink' in env_obj.group(0):
            # Define regular expression
            re_aimmslinks = r'\\AIMMSlink{.+?}\n?'
            # Create list of \AIMMSlinks
            aimms_link_list = re.findall(re_aimmslinks, env_obj.group(0))
            # Remove \AIMMSlinks from TeX-environment
            replacement = re.sub(re_aimmslinks, r'', env_obj.group(0))
            # Append \AIMMSlinks before the environment, with a potential '-LR' postfix
            for al in aimms_link_list:
                aimmslink_name = al.split('{')[1].split('}')[0]
                if aimmslink_name.lower() in [fun.lower() for fun in funcs]:
                    replacement = al.replace(aimmslink_name, aimmslink_name + '-LR') + '\n' + replacement
                else:
                    replacement = al + '\n' + replacement
            # Replace TeX-environment
            code = code.replace(env_obj.group(0), replacement)

    if r'\breaksubsection' in code:
        code = re.sub(r'\\breaksubsection\[[\d.]+?\]', r'\\subsection', code)

    if r'\begin{syntax}' in code:
        for syntax_object in re.finditer(r'\\begin{syntax}.*?\\end{syntax}', code, re.DOTALL):
            if syntax_object:
                # Define text element
                text_element = syntax_object.group(0)
                # Define placeholder for text element replacement
                text_element_replacement = []
                # Extract important content from text element
                for syntax_object_content in re.finditer(r'\\syntaxdiagram{.*?}{.*?}', text_element):
                    if syntax_object_content:
                        text_element_replacement.append(syntax_object_content.group(0))
                # Replace text element
                code = code.replace(text_element, '\n'.join(text_element_replacement))

    if r'\begin{enumerate}' in code:
        for enumerate_obj in re.finditer(r'\\begin{enumerate}(.*?)\\end{enumerate}', code, re.DOTALL):
            if enumerate_obj:
                # Define environment
                enumerate_env = enumerate_obj.group(0)
                # If all items contain an argument
                if all(r'\item[' in it_obj.group(0) for it_obj in re.finditer(r'\\item\S*', enumerate_obj.group(1))):
                    # Change the original enumerate environment to a description environment
                    code = code.replace(enumerate_env, enumerate_env.replace('enumerate', 'description'))

    # Iterate over math objects that contain code elements and check whether they are AIMMS functions
    for math_obj in re.finditer(r'(?<!\\)\$([^$]*?\\(?:math|text)tt{[^}{]*(?:{[^}{]*}[^}{]*)*}[\s\S]*?)(?<!\\)\$|'
                                r'(?<!\\)\$[\s\S]*?(?<!\\)\$|'
                                r'\\verb(?P<verb>\W).*?(?P=verb)|'
                                r'\\begin{example}[\s\S]*?\\end{example}', code):
        if math_obj.group(1):
            math_content = math_obj.group(1).strip().replace('\n', '')
            replacement = ''
            for element in re.finditer(r'(\\(?:math|text)tt{([^}{]*(?:{[^}{]*}[^}{]*)*)})|(.)', math_content):
                if element.group(1):
                    if element.group(2) in funcs:
                        replacement += r'\texttt{' + element.group(2) + r'}'
                    else:
                        replacement += r'$' + element.group(1) + r'$'
                else:
                    replacement += r'$' + element.group(3) + r'$'
            replacement = replacement.replace(r'$$', '')
            code = code.replace(math_obj.group(0), replacement)

    if r'\begin{tabular}' in code:
        # Fix (multiline) top row in a table

        # Regex to extract the column declaration (group(1)) and the top row (group(2)) of the 'tabular' environment
        re_tabular = r'(?s)\\begin{tabular}{(\S+?)}\s*\\hline\s*\\hline\s*(.+?)\s*\\hline'
        # Regex to extract each column title
        re_titles = r'(?s)(.*?)(&|\\\\)'

        # Iterate over 're_tabular' matches
        for match in re.finditer(re_tabular, code):
            # Check if the top row contains \multicolumn (if it does, the table will be manually processed)
            if r'\multicolumn' not in match.group(2):
                # Check if the top row contains a column separator
                if r'&' in match.group(2):
                    # Count the number of columns
                    number_of_col_titles = len(re.findall(r'\w', re.sub(r'{.+?}', r'', match.group(1))))
                    # Prepare a list of merged titles, avoiding linebreaks that are not supported by Pandoc
                    col_titles_merged = [''] * number_of_col_titles
                    # Find and list all column titles
                    col_titles = re.findall(re_titles, match.group(2))
                    # Iterate over column titles
                    for i, col_title in enumerate(col_titles):
                        col_titles_merged[i % number_of_col_titles] += ' ' + col_title[0].replace('\n', ' ').strip()
                    # Rebuild the top row
                    top_row_repl = ''
                    for j in col_titles_merged:
                        if j == col_titles_merged[-1]:
                            top_row_repl += j.strip() + r' \\'
                        else:
                            top_row_repl += j.strip() + ' & '
                    # Remove \textbf commands from top row
                    for col_title in re.finditer(r'\\textbf{([^}{]*(?:{[^}{]*}[^}{]*)*)}', top_row_repl):
                        top_row_repl = top_row_repl.replace(col_title.group(0), col_title.group(1).strip())
                    # Replace top row
                    intermediate_repl = match.group(0).replace(match.group(2), top_row_repl)
                    # Replace original 're_tabular' match
                    code = code.replace(match.group(0), intermediate_repl)
            # else:
            #     raise Exception('File contains table(s) with multicolumn top rows.')

    if r'\begin{model}' in code:

        # Reformulate optimisation models
        re_model = r'\\begin{model}\n?\\direction{([\w:]+)}\n?\\begin{\w+}\n?([\S\s]+?)\n\\end{\w+}\n?' \
                   r'\\subjecttomath\n?\\begin{\w+}\n?([\S\s]+?)\n?\\end{\w+}\n?\\end{model}'
        re_modelline = r'{([^}{]*(?:{[^}{]*}[^}{]*)*)}{([^}{]*(?:{[^}{]*}[^}{]*)*)}{([^}{]*(?:{[^}{]*}[^}{]*)*)}'

        for match in re.finditer(re_model, code):
            verb = match.group(1).replace(':', '').lower()
            objective = re.findall(re_modelline, match.group(2).replace(r'\modelline{}', ''))
            constraints = re.findall(re_modelline, match.group(3).replace(r'\modelline{}', ''))
            objective_tex = objective[0][0] + ' ' * (objective[0][1] != '') + objective[0][1]
            objective_tex += r' & & ' * (objective[0][2] != '') + objective[0][2]
            top_constraint_tex = constraints[0][0] + ' ' * (constraints[0][1] != '') + constraints[0][1]
            top_constraint_tex += r' & &' + ' ' * (constraints[0][2] != '') + constraints[0][2]
            model_repl = r'\begin{equation}' + '\n'
            model_repl += r'\begin{align}' + '\n'
            model_repl += r'& \text{' + verb + r'} & & ' + objective_tex + r' \\' + '\n'
            model_repl += r'& \text{subject to} & & ' + top_constraint_tex + r' \\' + '\n'
            for constraint in constraints[1:]:
                model_repl += r'&&& ' + constraint[0] + ' ' * (constraint[1] != '') + constraint[1]
                model_repl += r' & &' + ' ' * (constraint[2] != '') + constraint[2] + r' \\ ' + '\n'
            model_repl += r'\end{align}' + '\n'
            model_repl += r'\end{equation}'
            code = code.replace(match.group(0), model_repl)

        # Reformulate declaration models
        re_model = r'\\begin{model}[\s\S]+?\\item\[[\w:]+\][\s\S]+?\\declline[\s\S]+?\\end{model}'
        re_item = r'\\item\[([\w:]+)\].+'
        re_declline = r'\\declline({[^}{]*(?:{[^}{]*}[^}{]*)*})({[^}{]*(?:{[^}{]*}[^}{]*)*})'

        for match in re.finditer(re_model, code):
            model_repl = match.group(0).replace(r'\begin{model}', r'\begin{equation}' + '\n' + r'\begin{align}')
            model_repl = model_repl.replace(r'\end{model}', r'\end{align} ' + ' \n' + r'\end{equation}')
            for item in re.finditer(re_item, match.group(0)):
                model_repl = model_repl.replace(item.group(0), r'& \textbf{' + item.group(1) + r'} \\')
            for declaration in re.finditer(re_declline, match.group(0)):
                declline_replacement = r'&&& \text' + declaration.group(1) + r' & & \text' + declaration.group(2) + ' '
                model_repl = model_repl.replace(declaration.group(0), declline_replacement)
            code = code.replace(match.group(0), model_repl)

    if r'\begin{align}' in code:
        # Put standalone 'align' environments inside 'equation' environment
        # Define regular expressions
        re_align = r'(?s)(\\begin{equation}.+?\\end{equation})|(\\begin{align}(.+?)\\end{align})'
        re_modelline = r'{([^}{]*(?:{[^}{]*}[^}{]*)*)}\s*?' \
                       r'{([^}{]*(?:{[^}{]*}[^}{]*)*)}\s*?' \
                       r'{([^}{]*(?:{[^}{]*}[^}{]*)*)}'

        # Iterate over matches
        for align_obj in re.finditer(re_align, code):

            if align_obj.group(2):

                align_env = align_obj.group(2)
                align_content = align_obj.group(3)

                if r'\modelline{}' in align_content:
                    modellines = re.findall(re_modelline, align_content.replace(r'\modelline{}', ''))
                    align_repl = r'\begin{equation}' + '\n'
                    align_repl += r'\begin{align}' + '\n'
                    for modelline in modellines:
                        align_repl += modelline[0] + ' & ' + modelline[1] + ' & & ' + modelline[2] + r' \\' + '\n'
                    align_repl += r'\end{align}' + '\n'
                    align_repl += r'\end{equation}'
                    code = code.replace(align_env, align_repl)

                else:

                    align_repl = r'\begin{equation}' + '\n'
                    align_repl += r'\begin{align}' + '\n'
                    align_repl += align_content.strip() + '\n'
                    align_repl += r'\end{align}' + '\n'
                    if r'\label' in align_repl:
                        align_label = re.search(r'\\label{.+?}', align_repl).group(0)
                        align_repl = align_repl.replace(align_label, '')
                        align_repl += align_label + '\n'
                    align_repl += r'\end{equation}'
                    code = code.replace(align_env, align_repl)

    if r'\DistributionExt' in code or r'\DistributionRem' in code:

        # Define regular expression
        dist_regex = r'\\Distribution(?:Ext|Rem)' \
                     r'\s*{([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)}' \
                     r'\s*{([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)}' \
                     r'\s*{([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)}' \
                     r'\s*{([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)}' \
                     r'\s*{([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)}' \
                     r'\s*{([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)}' \
                     r'\s*{([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)}' \
                     r'(?:\s*{([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)})?\s?'

        def reform(strng):
            return ' '.join(strng.strip().split())

        # Iterate over matches
        for dist_obj in re.finditer(dist_regex, code):
            if dist_obj:
                dist_repl = '\n\n' + r'\begin{aimmstable}' + '\n'
                dist_repl += r'\begin{tabular}{ l l }' + '\n'
                dist_repl += r'Input parameters & {} \\'.format(reform(dist_obj.group(1).replace(r'\\', ', '))) + '\n'
                dist_repl += r'Input check & \( {} \) \\'.format(reform(dist_obj.group(2))) + '\n'
                dist_repl += r'Permitted values & \( {} \) \\'.format(reform(dist_obj.group(3))) + '\n'
                dist_repl += r'{} & \( {} \) \\'.format(reform(dist_obj.group(4)), reform(dist_obj.group(5))) + '\n'
                dist_repl += r'Mean & \( {} \) \\'.format(reform(dist_obj.group(6))) + '\n'
                dist_repl += r'Variance & \( {} \) \\'.format(reform(dist_obj.group(7))) + '\n'
                if dist_obj.group(8):
                    dist_repl += r'Remarks & {}'.format(reform(dist_obj.group(8).replace(r'\\', ', '))) + '\n'
                dist_repl += r'\end{tabular}' + '\n'
                dist_repl += r'\end{aimmstable}' + '\n\n'
                code = code.replace(dist_obj.group(0), dist_repl)

    if r'\StatIter' in code:

        # Define regular expression
        statiter_regex = r'\\StatIter' \
                         r'{[^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*}\s*' \
                         r'{([^}{]*(?:{[^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*}[^}{]*)*)}\s*' \
                         r'{([^}{]*' \
                         r'(?:{[^}{]*' \
                         r'(?:{[^}{]*' \
                         r'(?:{[^}{]*' \
                         r'(?:{[^}{]*' \
                         r'(?:{[^}{]*}[^}{]*)*' \
                         r'}[^}{]*)*' \
                         r'}[^}{]*)*' \
                         r'}[^}{]*)*' \
                         r'}[^}{]*)*' \
                         r')}'

        def reform(strng):
            return ' '.join(strng.strip().split())

        # Iterate over matches
        for statiter_obj in re.finditer(statiter_regex, code):
            if statiter_obj:
                statiter_repl = '\n\n' + r'\begin{aimmstable}' + '\n'
                statiter_repl += r'\begin{tabular}{ l l }' + '\n'
                statiter_repl += r'\textbf{{Operator}} & {} \\'.format(reform(statiter_obj.group(1))) + '\n'
                statiter_repl += r'\textbf{{Formula}} & \( {} \) \\'.format(reform(statiter_obj.group(2))) + '\n'
                statiter_repl += r'\end{tabular}' + '\n'
                statiter_repl += r'\end{aimmstable}' + '\n\n'
                code = code.replace(statiter_obj.group(0), statiter_repl)

    if r'\chapter' in code or r'\section' in code or r'\subsection' in code or r'\subsubsection' in code:

        # Titlecase titles
        def titlecase(matchobj):
            no_cap_list = ['a', 'about', 'above', 'after', 'along', 'amid', 'among', 'an', 'and', 'anti', 'as', 'at',
                           'below', 'but', 'by', 'down', 'for', 'from', 'in', 'into', 'like', 'minus', 'near', 'nor',
                           'of', 'off', 'on', 'onto', 'or', 'over', 'past', 'per', 'plus', 'round', 'since', 'so',
                           'than', 'the', 'till', 'to', 'under', 'until', 'up', 'upon', 'via', 'with', 'yet']
            new_title = ''
            for i, w in enumerate(matchobj.group(1).split(), 1):
                if i == 1:
                    if w[0].isupper() or '\\' in w:
                        new_title += w
                    else:
                        new_title += w.title()
                else:
                    new_title += ' '
                    if not w[0].isupper() and '\\' not in w and w not in no_cap_list:
                        if '\'' in w:
                            new_title += w.capitalize()
                        else:
                            new_title += w.title()
                    else:
                        new_title += w
            return matchobj.group(0).replace(matchobj.group(1), new_title.strip())

        titlecase_regex = r'\\(?:chapter|(?:sub){,2}section){([^}{]+(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)}'
        code = re.sub(titlecase_regex, titlecase, code)

    return code


def dummy_replace_tex(code, path_to_syntax, funcs, file_basename):
    """Replace TeX-commands and TeX-environments not properly handled by Pandoc by plain-text dummies, to be replaced at
    the end by appropriate RST-code.

    Parameters
    ----------
    code : str
        TeX-code.
    path_to_syntax : str
        Path to directory containing syntax diagrams.
    funcs : list
        List of functions in the AIMMS Function Reference (drawn from Intersphinx).
    file_basename : str
        Base name of the TeX-file path

    Returns
    -------
    code : str
        Modified TeX-code.
    dummy_dict : dict
        Dictionary mapping plain-text dummies to appropriate RST-code

    """
    # Define dummy dictionary
    dummy_dict = {}

    if r'\ref{' in code or r'\pageref{' in code:

        # Define list of reference formats
        ref_formats = ['appendix', 'appendices', 'binding', 'bindings', 'chapter', 'chapters', 'figure', 'figures',
                       'page', 'pages', 'section', 'sections', 'subsection', 'subsections', 'table', 'tables']

        # Iterate over matches
        for ref_obj in re.finditer(r'(?P<preamble>\w+~)?(?P<par>\()?\\(?:page)?ref{(?P<label>\S+?)}(?(par)\))', code):

            # Extract reference label
            ref_label = ref_obj.group('label')

            # Determine type of reference
            if 'table:' in ref_label or 'fig:' in ref_label:
                rst_repl = ':numref:`' + ref_label + '`'
                text_dummy = 'NumRef' + max((len(rst_repl) - 8), 0) * 'f'
            elif 'eq:' in ref_label:
                rst_repl = ':eq:`' + ref_label + '`'
                text_dummy = 'Eq' + max((len(rst_repl) - 4), 0) * 'q'
            else:
                rst_repl = ':ref:`' + ref_label + '`'
                text_dummy = 'Ref' + max((len(rst_repl) - 5), 0) * 'f'
            text_dummy += ''.join(['0' if len(dummy_dict) < 10 else '', str(len(dummy_dict))])

            # Add key and value to dummy dictionary
            dummy_dict[text_dummy] = rst_repl

            # Replace full match by plain-text dummy
            tex_repl = ''
            if ref_obj.group('preamble'):
                ref_preamble = ref_obj.group('preamble')
                if ref_preamble.replace('~', '').lower() not in ref_formats:
                    tex_repl += ref_preamble.replace('~', ' ')
            tex_repl += text_dummy
            code = code.replace(ref_obj.group(0), tex_repl)

    if r'\cite' in code:

        # Iterate over matches
        for cite_obj in re.finditer(r'\\cite{(\S+)}', code):
            # Create RST citation and text dummy
            rst_repl = r':cite:`' + cite_obj.group(1) + r'`'
            text_dummy = 'Citation' + ''.join(['0' if len(dummy_dict) < 10 else '', str(len(dummy_dict))])

            # Add key and value to dummy dictionary
            dummy_dict[text_dummy] = rst_repl

            # Replace full match by plain-text dummy
            code = code.replace(cite_obj.group(0), text_dummy)

    if r'\AIMMSlink' in code:

        # Iterate over matches
        for AIMMSLink_obj in re.finditer(r'\\AIMMSlink{(.*?)}', code):

            # Create RST anchor and text dummy
            if AIMMSLink_obj.group(1).lower() in [fun.lower() for fun in funcs]:
                rst_repl = '\n' + r'.. _' + AIMMSLink_obj.group(1) + '-LR' + ':\n\n'
            else:
                rst_repl = '\n' + r'.. _' + AIMMSLink_obj.group(1) + ':\n\n'
            text_dummy = 'AIMMSLink' + ''.join(['0' if len(dummy_dict) < 10 else '', str(len(dummy_dict))])

            # Add key and value to dummy dictionary
            dummy_dict[text_dummy] = rst_repl

            # Replace full match by plain-text dummy
            code = code.replace(AIMMSLink_obj.group(0), '\n' + text_dummy + '\n')

    if r'\syntaxmark' in code:

        # Iterate over matches
        for syntaxmark_obj in re.finditer(r'(.*?)\\syntaxmark{(.*?)} ?', code):
            # Create RST anchor and text dummy
            rst_repl = '\n' + r'.. _' + syntaxmark_obj.group(2) + ':\n\n'
            text_dummy = 'SyntaxMark' + ''.join(['0' if len(dummy_dict) < 10 else '', str(len(dummy_dict))])

            # Add key and value to dummy dictionary
            dummy_dict[text_dummy] = rst_repl

            # Replace full match by plain-text dummy
            if syntaxmark_obj.group(1):
                code = code.replace(syntaxmark_obj.group(0), '\n' + text_dummy + '\n\n' + syntaxmark_obj.group(1))
            else:
                code = code.replace(syntaxmark_obj.group(0), '\n' + text_dummy + '\n\n')

    if r'\inlinefig' in code:

        # Iterate over matches
        for inlinefig_obj in re.finditer(r'\\inlinefig{(.*?)}', code):
            # Create RST elements and text dummies
            rst_repl1 = r'|{}|'.format(inlinefig_obj.group(1))
            rst_repl2 = r'.. |{}| image:: {}.png'.format(inlinefig_obj.group(1), inlinefig_obj.group(1))
            text_dummy1 = 'InlineFig' + ''.join(['0' if len(dummy_dict) < 10 else '', str(len(dummy_dict))])
            text_dummy2 = 'InlineFigSub' + ''.join(['0' if len(dummy_dict) < 10 else '', str(len(dummy_dict))])

            # Add keys and values to dummy dictionary
            dummy_dict[text_dummy1] = rst_repl1
            dummy_dict[text_dummy2] = rst_repl2

            # Replace full match by plain-text dummy and add extra dummy
            code = code.replace(inlinefig_obj.group(0), text_dummy1) + '\n\n' + text_dummy2

    if r'\begin{pspicture}' in code:

        # Define regular expression
        pspic_regex = r'(\\begin{figure}.*?{pspicture}.*?\\end{figure})|' \
                      r'((?:\\ps\w+{[^}{]*(?:{[^}{]*}[^}{]*)*}(.*?))*\\begin{pspicture}.*?\\end{pspicture})'

        # Iterate over matches
        for i, pspic_obj in enumerate(re.findall(pspic_regex, code, re.DOTALL), 1):

            # Define placeholders
            pspic_preamble = ''
            pspic_caption = ''
            pspic_label = ''

            if pspic_obj[0]:
                # 'pspicture'-environment in 'figure'-environment
                tex_element = pspic_obj[0]
            else:
                # Standalone 'pspicture'-environment
                tex_element = pspic_obj[1]

            if pspic_obj[2] and r'\begin' in pspic_obj[2]:
                pspic_preamble += pspic_obj[2] + '\n'

            # If the figure has a caption, extract caption
            if r'\caption' in tex_element:
                caption_obj = re.search(r'\\caption{([^}{]*(?:{[^}{]*}[^}{]*)*)}', tex_element)
                if caption_obj:
                    pspic_caption = pandoc_tex2rst(caption_obj.group(1))

            # If the figure has a label, extract label
            if r'\label' in tex_element:
                label_obj = re.search(r'\\label{([^}{]*(?:{[^}{]*}[^}{]*)*)}', tex_element)
                if label_obj:
                    pspic_label = label_obj.group(1)

            # Create RST figure directive and text dummy
            rst_repl = '.. figure:: ' + file_basename.replace('.tex', '-pspic{}.svg'.format(i))
            if pspic_label:
                rst_repl += '\n' + 3 * ' ' + ':name: ' + pspic_label
            if pspic_caption:
                rst_repl += '\n\n' + 3 * ' ' + pspic_caption
            text_dummy = 'PSPicture' + ''.join(['0' if len(dummy_dict) < 10 else '', str(len(dummy_dict))])

            # Add key and value to dummy dictionary
            dummy_dict[text_dummy] = rst_repl

            # Replace full match by plain-text dummy
            code = code.replace(tex_element, pspic_preamble + '\n\n' + text_dummy + '\n\n')

    if r'\paragraph' in code or r'\tipparagraph' in code:

        def par_replace(par_regex, code):

            # Iterate over matches
            for par_obj in re.finditer(par_regex, code):

                if par_obj:

                    # Extract paragraph title
                    par_title_tex = par_obj.group(1).replace(r'- ', r'').replace('\\\\', ' ')

                    # If paragraph title exists
                    if par_title_tex:

                        # Convert paragraph title
                        par_title_rst = pandoc_tex2rst(par_title_tex)
                        if 'PSPicture' in par_title_rst:
                            par_pspic = ''
                            for par_pspic_obj in re.finditer(r'PSPicture\d+', par_title_rst):
                                par_pspic += par_pspic_obj.group(0) + '\n\n'
                                par_title_rst = par_title_rst.replace(par_pspic_obj.group(0), '')
                        else:
                            par_pspic = ''
                        par_title_rst = ' '.join(par_title_rst.strip().split())

                        # Create RST rubric directive and text dummy
                        rst_repl = '\n\n.. rubric:: {}\n'.format(par_title_rst)
                        # If there is label, add label
                        if par_obj.group(2):
                            rst_repl += 3 * ' ' + ':name: ' + par_obj.group(2) + '\n'
                        text_dummy = 'Paragraph' + ''.join(['0' if len(dummy_dict) < 10 else '', str(len(dummy_dict))])

                        # Add key and value to dummy dictionary
                        dummy_dict[text_dummy] = rst_repl

                        # Replace full match by plain-text dummy
                        code = code.replace(par_obj.group(0), text_dummy + '\n\n' + par_pspic + '\n\n')

                    else:

                        # Remove full match from code
                        code = code.replace(par_obj.group(0), '')

            return code

        if r'\paragraph' in code:
            # Define regular expression
            par_regex = r'\\paragraph(?:\[.+?\]|){([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)}' \
                        r'(?:\\(?:here)?label{(\S+?)})?'

            # Replace TeX paragraph
            code = par_replace(par_regex, code)

        if r'\tipparagraph' in code:
            # Define regular expression
            par_regex = r'\\tipparagraph(?:\[.+?\])?{([^}{]*(?:{[^}{]*}[^}{]*)*)}(?:{([^}{]*(?:{[^}{]*}[^}{]*)*)})?'

            # Replace TeX paragraph
            code = par_replace(par_regex, code)

    if r'\syntaxdiagram' in code:

        # Iterate over matches
        for syndia_obj in re.finditer(r'\\syntaxdiagram{(.*?)}{(.*?)}', code):

            if syndia_obj:
                # Extract syntax diagram label
                syndia_label = syndia_obj.group(1)

                # Load .svg-file
                with open(os.path.join(path_to_syntax, syndia_obj.group(2)) + '.svg') as f:
                    syndia_svg = f.read()

                # Define syntax diagram preamble
                syndia_preamble = '\n\n' + '.. _' + syndia_label + ':\n\n*' + syndia_label + ':*\n\n'

                # Build raw html directive
                syndia_html = '.. raw:: html\n\n'
                syndia_html += '\t<div class="svg-container" style="overflow: auto;">'
                syndia_html += indent(syndia_svg, '\t') + '</div>'

                # Create RST syntax diagram directive and text dummy
                rst_repl = syndia_preamble + syndia_html + '\n\n'
                text_dummy = 'SynDia' + ''.join(['0' if len(dummy_dict) < 10 else '', str(len(dummy_dict))])

                # Add key and value to dummy dictionary
                dummy_dict[text_dummy] = rst_repl

                # Replace full match by plain-text dummy
                code = code.replace(syndia_obj.group(0), '\n\n' + text_dummy + '\n\n')

    if r'\begin{example}' in code:

        # Iterate over matches
        for example_obj in re.finditer(r'(?s)\\begin{example}(.+?)\\end{example}', code):

            if example_obj:
                # Create RST code block and text dummy
                rst_repl = '\n' + '.. code-block:: aimms' + '\n'
                rst_repl += indent(dedent(example_obj.group(1).replace('\t', 4 * ' ')), '\t') + '\n'
                text_dummy = 'ExampleEnv' + ''.join(['0' if len(dummy_dict) < 10 else '', str(len(dummy_dict))])

                # Add key and value to dummy dictionary
                dummy_dict[text_dummy] = rst_repl

                # Replace full match by plain-text dummy
                code = code.replace(example_obj.group(0), '\n\n' + text_dummy + '\n\n')

    if r'\begin{aimmstable}' in code:

        # Iterate over 'aimmstable' environments
        for aimmstable_obj in re.finditer(r'\\begin{aimmstable}([\s\S]*?)\\end{aimmstable}', code):

            if aimmstable_obj:

                # Extract 'aimmstable' content
                table_content = aimmstable_obj.group(1)

                # Predefine variables
                rst_repl = ''
                table_caption_rst = ''
                table_label = ''

                # If the table has a caption, extract caption
                if r'\caption' in table_content:
                    caption_obj = re.search(r'\\caption{([^}{]*(?:{[^}{]*}[^}{]*)*)}', table_content)
                    if caption_obj:
                        table_caption = caption_obj.group(1)
                        table_caption_rst = pandoc_tex2rst(table_caption).strip()
                        # Remove \caption from table content
                        table_content = table_content.replace(caption_obj.group(0), '')

                # If the table has a label, extract label
                if r'\label' in table_content:
                    label_obj = re.search(r'\\label{([^}{]*(?:{[^}{]*}[^}{]*)*)}', table_content)
                    if label_obj:
                        table_label = label_obj.group(1)
                        # Remove \label from table content
                        table_content = table_content.replace(label_obj.group(0), '')

                # If there are 'tabular' environments in the 'aimmstable' environment
                if r'\begin{tabular}' in table_content:
                    # Define placeholder
                    tabular_envs = {}
                    # Iterate over 'tabular' environments
                    for tabular_obj in re.finditer(r'\\begin{tabular}.*?\\end{tabular}', table_content, re.DOTALL):
                        if tabular_obj:
                            # Define 'tabular' text element
                            tabular_text_element = tabular_obj.group(0)
                            # Replace \verb by \texttt
                            if r'\verb' in tabular_text_element:
                                def verb2texttt(matchobj):
                                    verb_content = matchobj.group(2)
                                    if r'~' in verb_content:
                                        verb_content = verb_content.replace(r'~', r'\textasciitilde')
                                    if r'^' in verb_content:
                                        verb_content = verb_content.replace(r'^', r'\textasciicircum')
                                    if '\\' in verb_content:
                                        verb_content = verb_content.replace('\\', r'\textbackslash')
                                    verb_content = re.sub(r'[&%$#_{}]', r'\\\g<0>', verb_content)
                                    return '\\texttt{{{}}}'.format(verb_content)
                                tabular_text_element = re.sub(r'\\verb(?P<verb>\W)(.*?)(?P=verb)',
                                                              verb2texttt,
                                                              tabular_text_element)
                            # Replace \( ... \) math elements in 'tabular' environment by dummies
                            math_dict = {}
                            for math_obj in re.finditer(r'\\\(.*?\\\)', tabular_text_element, re.DOTALL):
                                if math_obj:
                                    math_rst = pandoc_tex2rst(math_obj.group(0)).replace('\r\n', '')
                                    math_dummy = 'MathDum' + max((len(math_rst) - 9), 0) * 'm'
                                    math_dummy += ''.join(['0' if len(math_dict) < 10 else '', str(len(math_dict))])
                                    math_dict[math_dummy] = math_rst
                                    tabular_text_element = tabular_text_element.replace(math_obj.group(0), math_dummy)
                            # First convert TeX table to HTML table to account for empty cells
                            tabular_env_html = pypandoc.convert_text(tabular_text_element,
                                                                     'html',
                                                                     format='latex',
                                                                     extra_args=['--mathjax'])
                            # Fill empty cells in table with 'n/a'
                            tabular_env_html = re.sub(r'(<t[dh][^<>]+>)(</t[dh]>)', r'\1n/a\2', tabular_env_html)
                            # Iterate over math elements in HTML table
                            for math_obj in re.finditer(r'<span class=\"math inline\">\\\((.+?)\\\)</span>',
                                                        tabular_env_html):
                                if math_obj:
                                    math_rst = r':math:`' + html.unescape(math_obj.group(1)) + r'`'
                                    math_dummy = 'MathDum' + max((len(math_rst) - 9), 0) * 'm'
                                    math_dummy += ''.join(['0' if len(math_dict) < 10 else '', str(len(math_dict))])
                                    math_dict[math_dummy] = math_rst
                                    tabular_env_html = tabular_env_html.replace(math_obj.group(0), math_dummy)
                            # Convert 'tabular' environment to RST
                            tabular_env_rst = pypandoc.convert_text(tabular_env_html,
                                                                    'rst',
                                                                    format='html',
                                                                    extra_args=['--wrap=none'])
                            # Replace dummy math elements by RST math elements
                            if math_dict:
                                for math_dummy, math_rst in math_dict.items():
                                    tabular_env_rst = tabular_env_rst.replace(math_dummy, math_rst)
                            # Define dummy
                            tabular_dummy = 'TabularEnv' + str(len(tabular_envs))
                            # Add key and value to dummy dictionary
                            tabular_envs[tabular_dummy] = tabular_env_rst
                            # Replace 'tabular' text element
                            table_content = table_content.replace(tabular_obj.group(0), tabular_dummy)

                    # Define RST table preamble
                    if table_label:
                        table_env_preamble = '\n.. _' + table_label + ':\n\n.. table:: ' + table_caption_rst + '\n\n'
                    else:
                        table_env_preamble = '\n.. table:: ' + table_caption_rst + '\n\n'

                    # Define RST table environment
                    rst_repl = pandoc_tex2rst(table_content)
                    if tabular_envs:
                        # Iterate over converted 'tabular' environments
                        for i, dummy in enumerate(tabular_envs, 1):
                            if i == 1:
                                tabular_env_rst = table_env_preamble + '\t'
                                tabular_env_rst += tabular_envs[dummy].replace('\r\n', '\n\t')
                            else:
                                tabular_env_rst = '\n.. table:: \n\n\t' + tabular_envs[dummy].replace('\r\n', '\n\t')
                            # Replace plain-text dummy
                            rst_repl = rst_repl.replace(dummy, tabular_env_rst)

                # If there are no 'tabular' environments in the 'aimmstable' environment
                else:
                    # Check if there is a code-block in the 'aimmstable' environment
                    if 'ExampleEnv' in table_content:
                        example_env_obj = re.search(r'\bExampleEnv\d+\b', table_content)
                        if example_env_obj:
                            dummy = example_env_obj.group(0)
                            example_env = dummy_dict[dummy]
                            # Redefine code-block preamble
                            if '.. code-block:: aimms' in example_env:
                                if table_caption_rst and table_label:
                                    preamble_new = '.. code-block:: aimms\n'
                                    preamble_new += 3 * ' ' + ':caption: ' + table_caption_rst + '\n'
                                    preamble_new += 3 * ' ' + ':name: ' + table_label
                                elif table_caption_rst:
                                    preamble_new = '.. code-block:: aimms\n'
                                    preamble_new += 3 * ' ' + ':caption: ' + table_caption_rst
                                elif table_label:
                                    preamble_new = '.. code-block:: aimms\n'
                                    preamble_new += 3 * ' ' + ':name: ' + table_label
                                else:
                                    preamble_new = '.. code-block:: aimms'
                                # Replace code-block preamble
                                dummy_dict[dummy] = example_env.replace('.. code-block:: aimms', preamble_new)
                                # Define RST table environment
                                rst_repl = pandoc_tex2rst(table_content)

                # If none of the above
                if not rst_repl:
                    rst_repl = pandoc_tex2rst(table_content)

                # Create text dummy
                text_dummy = 'TableEnv' + ''.join(['0' if len(dummy_dict) < 10 else '', str(len(dummy_dict))])

                # Add key and value to dummy dictionary
                dummy_dict[text_dummy] = rst_repl

                # Replace full match by plain-text dummy
                code = code.replace(aimmstable_obj.group(0), '\n\n' + text_dummy + '\n\n')

    return code, dummy_dict


def convert(file, funcs, path_to_syntax):
    """Convert TeX-code to RST-code.

    Parameters
    ----------
    file : str
        Path to TeX-file.
    funcs : list
        List of functions in the AIMMS Function Reference (drawn from Intersphinx).
    path_to_syntax : str
        Path to directory containing syntax diagrams.

    Returns
    -------
    code : str
        RST-code.

    """
    # Read code from TeX-file
    with open(file, 'r', encoding='latin-1') as f:
        code = f.read()

    # Removals and TeX-by-TeX replacements
    code = tex_replace_tex(remove_tex(code), funcs, file)

    # TeX-by-dummy replacements
    code, dummy_dict = dummy_replace_tex(code, path_to_syntax, funcs, os.path.basename(file))

    # Convert remaining TeX-code using Pandoc
    code = pandoc_tex2rst(code)

    # Dummy replacements
    if dummy_dict:
        # Iterate over dummies in reverse order
        for dummy, rst_repl in reversed(dummy_dict.items()):
            dummy_obj = re.search(r'(?m)(^[\t ]+)?' + dummy, code)
            if dummy_obj:
                # Check leading whitespace
                if dummy_obj.group(1) and dummy_obj.group(1) == ' ':
                    code = code.replace(dummy_obj.group(0), rst_repl)
                elif dummy_obj.group(1) and dummy_obj.group(1) != ' ':
                    code = code.replace(dummy, rst_repl.replace('\n', '\n' + dummy_obj.group(1)))
                else:
                    code = code.replace(dummy, rst_repl)

    # Character replacements
    code = code.replace('\xa0', ' ')
    code = code.replace('\r\n', '\n')
    code = code.replace('’', '\'').replace('‘', '\'')
    code = code.replace('“', '\"').replace('”', '\"')
    code = code.replace('…', '...')
    code = code.replace('–', '-')
    code = code.replace('—', '-')
    code = code.replace('—', '-')

    # Programming language denotation
    code = code.replace(r'``Fortran``', r'``fortran``').replace(r'``fortran``', r'``FORTRAN``')

    # Add links
    if 'Function Reference' in code:
        code = code.replace('Function Reference',
                            '`Function Reference <https://documentation.aimms.com/functionreference/>`__')
    if 'User\'s Guide' in code:
        code = code.replace('User\'s Guide',
                            '`User\'s Guide <https://documentation.aimms.com/_downloads/AIMMS_user.pdf>`__')

    # Check if math directives need labelling
    if '.. math::' in code:
        for mathdir_obj in re.finditer(r'([\t ]*)\.\. math::\s+(?: {3}.*\n)+', code):
            if mathdir_obj:
                math_dir = mathdir_obj.group(0)
                if r'\label' in math_dir:
                    math_indent = mathdir_obj.group(1) + 3 * ' '
                    math_label_obj = re.search(r'\\label{(\S+?)}', math_dir)
                    math_label = math_label_obj.group(1)
                    math_dir_new_preamble = '.. math::' + '\n' + math_indent + ':label: ' + math_label
                    math_dir_new = math_dir.replace('.. math::', math_dir_new_preamble)
                    math_dir_new = math_dir_new.replace(math_label_obj.group(0), '')
                    code = code.replace(math_dir, math_dir_new)

    # Remove redundant whitespace from code
    code = re.sub(r'(?:[\t ]*\n){3,}', '\n\n', code)

    # Add cross-references to Function Reference
    code = add_crossrefs(code, funcs)

    # Remove 'n/a' from tables
    def new_table(matchobj):
        return matchobj.group(0).replace('n/a', 3 * ' ')
    code = re.sub(r'\.\. table::(?: .*)?(?:\n {3}:[\w]+:.*)*\s{2,}(?:(?:\t| {3}).*\n)+', new_table, code)

    # Place labels before rubric titles
    def rubric_ref(matchobj):
        return matchobj.group(2).strip() + '\n\n' + matchobj.group(1)
    code = re.sub(r'(\.\. rubric::.*)((?:\n\n\.\. _\S*:)+)', rubric_ref, code)

    # Remove leading and trailing whitespace
    code = code.strip()

    return code


def write_rst(rst_code, rst_file):
    if rst_file.endswith('index.rst'):
        with open(rst_file, 'r', encoding='latin-1') as f:
            toc_tree = re.search(r'\.\. toctree::\n(?: {3}:maxdepth: \d*\n[\t ]*)?(?:\n[\t ]+\S+)+', f.read()).group(0)
        with open(rst_file, 'w', encoding='latin-1') as f:
            if ':maxdepth:' not in toc_tree:
                f.write(rst_code + '\n\n' + toc_tree.replace('.. toctree::', '.. toctree::\n   :maxdepth: 1'))
            else:
                f.write(rst_code + '\n\n' + toc_tree)
    else:
        with open(rst_file, 'w') as f:
            f.write(rst_code)

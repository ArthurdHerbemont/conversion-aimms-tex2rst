# AIMMS TEX2RST Conversion

Python scripts to assist AIMMS in our transformation to a new documentation system (PDF to Sphinx).

# Requirements

- Python 3
- [Pandoc](https://pandoc.org/installing.html)
- ``python -mpip install PyMuPDF openpyxl pypandoc``

# Usage

Open a command prompt in the folder where the script is located, and go for:

``` console
python RunAll.py Your_TEX_File.tex
```

The output .rst file will be outputted in the console.
